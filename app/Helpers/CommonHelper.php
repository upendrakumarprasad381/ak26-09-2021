<?php

namespace App\Helpers;

use App\Database;
use Config;
use Session;
use DateTime;
use Illuminate\Support\Facades\Auth;

class CommonHelper {

    public static function autoupdate() {
        $updatejson = !empty($_POST['updatejson']) ? json_decode($_POST['updatejson'], true) : [];
        $condjson = !empty($_POST['condjson']) ? json_decode($_POST['condjson'], true) : [];
        $dbtable = !empty($_POST['dbtable']) ? $_POST['dbtable'] : '';
        if (!empty($dbtable) && is_array($updatejson) && is_array($condjson)) {
            Database::updates($dbtable, $updatejson, $condjson);
            die(json_encode(array('status' => true, 'message' => 'successfully')));
        }
    }

    public static function autodelete() {
        $condjson = !empty($_POST['condjson']) ? json_decode($_POST['condjson'], true) : [];
        $dbtable = !empty($_POST['dbtable']) ? $_POST['dbtable'] : '';
        if (!empty($dbtable) && is_array($condjson)) {
            Database::deletes($dbtable, $condjson);
            $filepath = !empty($_POST['filepath']) ? $_POST['filepath'] : '';
            if (is_file($filepath)) {
                unlink($filepath);
            }
            die(json_encode(array('status' => true, 'message' => 'successfully')));
        }
    }

    public static function DecimalAmount($Amount = 0) {
        return sprintf('%0.2f', $Amount);
    }

    public static function GetSessionuser() {
        $sArray = Auth::user();
        if (!empty($sArray)) {
            if ($sArray->hasRole('user') || $sArray->hasRole('guest')) {
                return $sArray;
            }
        }

//        $sArray = Session::get('userlogin');
//        return $sArray;
    }

    public static function eventPlannerEventstatus($statusId = '') {
        $status = 'In-process';
        if (empty($statusId)) {
            $status = "Awaiting Quotation";
        }
        return $status;
    }

    public static function GettodolistById($Id = '') {
        $Sql = "SELECT * FROM `todo_list` WHERE  id='$Id'";
        $list = \App\Database::selectSingle($Sql);
        return $list;
    }

    public static function GeteventList($occasionsId = '', $vendorId = '') {
        $occasionsId = !empty($_POST['occasionsId']) ? $_POST['occasionsId'] : $occasionsId;
        $vendorId = !empty($_POST['vendorId']) ? $_POST['vendorId'] : $vendorId;

        $Sql = "SELECT id,event_name,occasion_id FROM `vendor_events` WHERE status=0 AND occasion_id='$occasionsId' AND vendor_id='$vendorId'";
        $list = \App\Database::select($Sql);
        return $list;
    }

    public static function GetsubList($listId = '') {
        $Sql = "SELECT * FROM `todo_filter_sublist` WHERE status=0 AND list_id='$listId'";
        $list = \App\Database::select($Sql);
        return $list;
    }

    public static function GetguestlistinvitationById($Id = '') {
        $Sql = "SELECT * FROM `guest_list_invitation` WHERE id='$Id'";
        $list = \App\Database::selectSingle($Sql);
        return $list;
    }

    public static function updateMycalender($calenderId = NULL, $type, $eventId, $primaryId, $title, $eventDate, $createdBy = '') {
        $update['type'] = $type;
        $update['event_id'] = $eventId;
        $update['primary_id'] = $primaryId;
        $update['title'] = $title;
        $update['event_date'] = date('Y-m-d', strtotime($eventDate));
        $update['google_update'] = '0';
        $update['updated_at'] = date('Y-m-d H:i:s');
        if (empty($calenderId)) {
            $update['created_at'] = $update['updated_at'];
            if (empty($createdBy)) {
                $createdBy = Auth::user()->id;
            }
            $update['created_by'] = $createdBy;
            $calenderId = Database::insert('calender_events', $update);
        } else {
            Database::updates('calender_events', $update, ['id' => $calenderId]);
        }
        return $calenderId;
    }

    public static function sendWhatsappMessage($mobile_no = '', $message = "") {
        $curl = curl_init();
        $data['phone'] = $mobile_no;
        $data['body'] = $message;
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.chat-api.com/instance344532/sendMessage?token=gujsy6k68i3a34zo",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
        ));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        echo $response;
    }

    public static function send_sms($mobile_no, $messagqwe = '') {
        header('Content-Type: text/html; charset=utf-8');
        $USER_NAME = 'MadEvents';
        $SenderId = 'ASK DEEMA';
        $SMS_PASSWORD = 'Mad2250';

        $messagqwe1 = $messagqwe;
        $mobile_no = $mobile_no;
        $string = "http://sms.bulk-sms-cloud.me/API_SendSMS.aspx?User=$USER_NAME&passwd=$SMS_PASSWORD&mobilenumber=$mobile_no&message=$messagqwe1&sid=$SenderId&mtype=N&DR=N";
        $url = preg_replace("/ /", "%20", $string);
        $sendSms = file_get_contents($url);
        return $sendSms;
    }

}
