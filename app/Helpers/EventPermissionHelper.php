<?php

namespace App\Helpers;

use App\Models\Permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EventPermissionHelper {

    static function attachPermissions($permissions,$id)
    {
        foreach ($permissions as $key => $value) {
            DB::table('permission_vendor_event')->insert([
                  'permission_id' =>  (int)$value,
                  'vendor_event_id' => (int)$id,
                  'vendor_event_type' => 'VendorEvent'
            ]);
        }
    }

    static function getPermissions($event)
    {
       if($event->request_id || Auth::user()->hasRole('event_planner') || Auth::user()->hasRole('vendor'))
       {
          $permissions = Permission::where('module_id',6)->where('name', 'like', 'view'.'%')->orderBy('id','ASC')->pluck('name');
       }
       else
       {
          $permissions = DB::table('permission_vendor_event')->join('permissions','permissions.id','permission_vendor_event.permission_id')->where('vendor_event_id',$event->id)->pluck('name');
       }

       return $permissions;
    }

    static function getEventpermissions()
    {
        return Permission::where('module_id',6)->where('name', 'like', 'view'.'%')->orderBy('id','ASC')->get();
    }

}
