<?php

namespace App\Helpers;

use App\Database;
use Config;
use Session;
use Illuminate\Support\Facades\Auth;

class HtmlHelper {

    public static function homepageSearchLoad() {
        ob_start();
        $catId = !empty($_POST['catId']) ? $_POST['catId'] : '';
        $catAr = LibHelper::GetcategoriesById($catId);
        $showsubcat = ['EVENT_PLANER', '1'];
        $showNoofGuest = ['EVENT_PLANER', '1'];
        $typeOfEvent = ['2', '5', '6', '1', '3'];
        ?>
        <div class="wd100 __tbzfmzwap">
            <div class="row">
                <?php if (in_array($catId, $typeOfEvent)) { ?>
                    <div class="__tfdsyl col-lg-3 col-md-6 col-sm-12 ">
                        <select class="form-select " placeholder="Type of event"  aria-label="Default select example">
                            <option value="">Type of event</option>
                            <option value="0">Indoor</option>
                            <option value="1">Outdoor</option>
                        </select>
                    </div>
                <?php } ?>

                <?php if (in_array($catId, $showsubcat)) { ?>
                    <div class="__tfdsyl col-lg-3 col-md-6 col-sm-12 ">
                        <select class="form-select select2" id="vendor_category" data-placeholder="Select Category" multiple="multiple" aria-label="Default select example">
                            <?php
                            if ($catId == 'EVENT_PLANER') {
                                $Sql = "SELECT * FROM `categories` WHERE parent_id IS null AND status='1'";
                                $cateList = Database::select($Sql);
                            } else {
                                $Sql = "SELECT * FROM `categories` WHERE parent_id='$catId' AND status='1'";
                                $cateList = Database::select($Sql);
                                $Sql = "SELECT * FROM `categories` WHERE parent_id > 0 AND status='1'";
                                $cateAllList = Database::select($Sql);
                                $cateList = empty($cateList) ? $cateAllList : $cateList;
                            }
                            for ($i = 0; $i < count($cateList); $i++) {
                                $d = $cateList[$i];
                                ?>
                                <option value="<?= $d->id ?>"><?= $d->name ?></option>
                            <?php } ?>

                        </select>
                    </div>
                    <?php
                }
                ?>
                <div class="__tfdsyl col-lg-2 col-md-6 col-sm-12 ">
                    <select class="form-select select2" data-placeholder="Type of Occasion" id="occasions" aria-label="Default select example">
                         <option value="">Occasion</option>
                        <?php
                        $Sql = "SELECT * FROM `occasions` WHERE  status='1'";
                        $cateList = Database::select($Sql);
                        for ($i = 0; $i < count($cateList); $i++) {
                            $d = $cateList[$i];
                            ?>
                            <option value="<?= $d->id ?>"><?= $d->name ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="__tfdsyl __location_fld col-lg-2 col-md-6 col-sm-12 ">
                    <select class="form-select select2" data-placeholder="Select Location" id="location"  aria-label="Default select example">
                        <option value="">Location</option>
                        <?php
                        $Sql = "SELECT * FROM `emirates`";
                        $data = Database::select($Sql);
                        for ($i = 0; $i < count($data); $i++) {
                            $d = $data[$i];
                            ?>
                            <option value="<?= $d->id ?>"><?= $d->emirate ?></option>
                        <?php } ?>
                    </select>
                </div>
                <?php
                if ($catId == '4') {
                    ?>
                    <div class="__tfdsyl col-lg-3 col-md-6 col-sm-12 ">
                        <input type="number" class="form-control" placeholder="Focus">
                    </div>
                    <div class="__tfdsyl col-lg-3 col-md-6 col-sm-12 ">
                        <input type="number" class="form-control" id="budget" placeholder="Budget">
                    </div>
                    <?php
                }
                if (in_array($catId, $showNoofGuest)) {
                    ?>
                    <div class="__tfdsyl col-lg-2 col-md-6 col-sm-12">
                        <select class="form-select select2" id="noofguest" data-placeholder="Select no of Guests" aria-label="Default select example">
                            <option value="">No of guests</option>
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                            <option value="500">500</option>
                            <option value="750">750</option>
                            <option value="1000">1000</option>
                            <option value="1000+">1000+</option>
                        </select>
                    </div>
                <?php } ?>
            </div>
            <div class="row">
                <div class="  __srfilsctm col">
                    <input type="search" class="form-control" id="search" placeholder="Search <?= !empty($catAr->name) ? $catAr->name : 'keyword' ?>">
                </div>
                <div class="col-2  __srcfld">
                    <button type="button" class="btn btn-primary __search_btn" id="homepageSearchBtn"><img src="<?= url('/user') ?>/images/search_icon.svg" />SEARCH</button>
                </div>
            </div>
        </div>
        <?php
        $html = ob_get_clean();
        die(json_encode(array('status' => true, 'HTML' => $html)));
    }

    public static function loadHTMLToDoListAssignContacts($Id) {
        ob_start();
        ?>
        <div class="todolisGrWrp__selctd wd100 mt-5" >
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="font-weight-bolder mt-2 mb-2 mr-5">Assigned Contact</h3>
                </div>
            </div>
            <div class="row __cumScroll" id="addedlist">
                <?php
                if (!empty($Id)) {
                    $selUser = "SELECT user_id FROM `todo_list_guest` WHERE todo_id IN ($Id)";
                    $join = "LEFT JOIN role_user RU ON RU.user_id=U.id LEFT JOIN vendor_details VD ON VD.user_id=U.id";
                    $Sql = "SELECT U.id,U.first_name,U.phone,U.email,VD.logo,VD.address FROM `users` U $join WHERE U.id IN ($selUser)";
                    $baseDir = Config::get('constants.HOME_DIR');
                    $data = Database::select($Sql);
                    for ($i = 0; $i < count($data); $i++) {
                        $d = $data[$i];

                        $baseFile = "storage/" . $d->logo;
                        if (is_file($baseDir . $baseFile)) {
                            $d->logo = url($baseFile);
                        } else {
                            $d->logo = url(Config::get('constants.DEFAULT_LOGO_VENDOR'));
                        }
                        $d->phone = !empty($d->phone) ? $d->phone : '+971456789012';
                        $d->address = !empty($d->address) ? $d->address : '';
                        ?>

                        <div class="col-lg-3 col-md-6 col-sm-6 addedlistguest" userId="<?= $d->id ?>" id="guestId-<?= $d->id ?>">
                            <div class="__grcadbz wd100">
                                <div class="__selcBntWrp">
                                    <div class="__selcBn">
                                        <i class="fas fa-check-circle"></i>
                                    </div>
                                    <div class="__CloseBn" userId="<?= $d->id ?>" onclick="ASSIGN.removeGuestlistForToDo(this);">
                                        <i class="far fa-times-circle"></i>
                                    </div>
                                </div>
                                <div class="__grcadbzImg">
                                    <img class="img-fluid" src="<?= $d->logo ?>">
                                </div>
                                <div class="wd100">
                                    <h4 class="mt-2"><a href="javascript:void(0)"><?= $d->first_name ?></a></h4>
                                    <h5 class="__todLilcotin mb-2"><a href="javascript:void(0)"><?= $d->address ?></a> </h5>
                                    <div class="wd100 __todoCall __open_sans ">
                                        <a href="javascript:void(0)"><?= $d->phone ?></a>
                                    </div>
                                    <div class="wd100 __todoEmail __open_sans">
                                        <a href="javascript:void(0)"><?= $d->email ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>




                        <?php
                    }
                }
                ?>

            </div>
        </div>
        <div class="__todlistInrWrp wd100">

            <div class="__todlistInrFlter d-flex">
                <!--begin::Page Title-->

                <!--end::Page Title-->
                <div class="__todLifilFrm  d-flex" style="justify-content: unset;">
                    <div class="col-lg-5 col-md-5 col-sm-12 mb-5">
                        <input type="text" class="form-control" id="mod_search" placeholder="Search name or email ">
                    </div>

                    <div class="col-md-auto pr-0">
                        <button type="button" onclick="ASSIGN.getGuestlistForToDo();" class="btn btn-secondary">Search</button>

                        <button type="button" onclick="ASSIGN.openModaladdContact();"  class="btn btn-secondary">Add Contact</button>
                    </div>
                </div>
            </div>

            <div class="todolisGrWrp wd100 mt-5">
                <div id="assingLoader" class="row" style="margin-left: 45%;display: none;">
                    <img src="https://icons8.com/preloaders/preloaders/1484/Circles-menu-3.gif">
                </div>
                <div class="row __cumScroll" id="assignList" style="display: none;">

                </div>
            </div>







        </div>

        <div class="modal fade" id="addguestcontactpoup" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Add New Contact</h5>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                                <label class="form-label">First Name</label>
                                <input name="json[first_name]" id="mod_first_name" class="form-control form-control-lg" type="text" autocomplete="off" required>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                                <label class="form-label">Last Name</label>
                                <input name="json[last_name]" id="mod_last_name" class="form-control form-control-lg" type="text" autocomplete="off" required>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                                <label class="form-label">Email</label>
                                <input name="json[email]" id="mod_email" class="form-control form-control-lg" type="text" autocomplete="off" required>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                                <label class="form-label">Phone</label>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <select class="form-control" id="mod_phone_code">
                                            <?php
                                            $Sql = "SELECT countryname,dial_code FROM `country` GROUP BY dial_code ORDER BY dial_code ASC";
                                            $data = Database::select($Sql);
                                            for ($i = 0; $i < count($data); $i++) {
                                                $d = $data[$i];
                                                ?>
                                                <option <?= $d->dial_code == '971' ? 'selected' : '' ?> value="<?= $d->dial_code ?>">+<?= $d->dial_code ?></option>
                                            <?php } ?>

                                        </select>
                                    </div>
                                    <div class="col-sm-9">
                                        <input name="json[phone]" id="mod_phone" class="form-control form-control-lg" type="number" autocomplete="off" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="addnewcontact" onclick="ASSIGN.addnewcontact();"  class="btn btn-primary">Submit</button>
                    </div>

                </div>
            </div>
        </div>
        <?php
        $html = ob_get_clean();
        return $html;
    }

    public static function showRFQModal($vendorId, $categoryId, $eventId) {
        ob_start();
        $event = LibHelper::GetvendoreventsById($eventId);
        $cat = LibHelper::GetcategoriesById($categoryId);
        ?>
        <div class="modal fade __sendrfqPop" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <input type="hidden" id="vendor_id" value="<?= $vendorId ?>">
            <input type="hidden" id="event_id" value="<?= $eventId ?>">
            <input type="hidden" id="category_id" value="<?= $categoryId ?>">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content ">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"> </h5>

                        <!--<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>-->
                    </div>
                    <div class="modal-body">
                        <h5>Send RFQ</h5>
                        <div class="row">

                            <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label for="exampleFormControlInput1" class="form-label">Event Name</label>
                                <input type="email" class="form-control" id="rfq_event_name" readonly value="<?= $event->event_name ?>" placeholder="">
                            </div>


                            <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label for="exampleFormControlInput1" class="form-label">Event Date</label>
                                <input type="date" class="form-control" id="rfq_wedding_date" value="<?= $event->event_date ?>" placeholder="Wedding Data">
                            </div>
                            <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label for="exampleFormControlInput1" class="form-label">Venue</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" value="Yes, I have picked a venue" <?= $event->picked_a_venue != 'No, I am still looking for a venue' ? 'checked' : '' ?> disabled  name="venue_picked" id="venuepicked">
                                    <label class="form-check-label" for="venuepicked">
                                        Yes, I have picked a venue
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" value="No, I am still looking for a venue" <?= $event->picked_a_venue == 'No, I am still looking for a venue' ? 'checked' : '' ?> disabled name="venue_picked" id="venuepicked-1">
                                    <label class="form-check-label" for="venuepicked-1">
                                        No, I'm still looking for a venue
                                    </label>
                                </div>
                            </div>

                            <div class="mb-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" id="venue_name_div" style="display: <?= $event->picked_a_venue == 'No, I am still looking for a venue' ? 'none' : '' ?>">
                                <label for="exampleFormControlInput1" class="form-label"> Venue Name</label>
                                <input type="text" class="form-control" id="venue_name" value="<?= $event->event_location ?>" readonly placeholder="Venue Name">
                            </div>


                            <div class="mb-3 col-lg-6 col-md-12 col-sm-12 col-xs-12">

                                <label for="exampleFormControlInput1" class="form-label">How long do you need wedding <?= $cat->name ?>?</label>
                                <?php
                                $time = date('H:i', strtotime($event->event_time));
                                ?>
                                <select class="form-control" id="start_time" aria-label="Default select example">
                                    <option value="">Event Start Time</option>
                                    <option <?= $time == '10:00' ? 'selected' : '' ?> value="10:00">10:00</option>
                                    <option <?= $time == '11:00' ? 'selected' : '' ?> value="11:00">11:00</option>
                                    <option <?= $time == '12:00' ? 'selected' : '' ?> value="12:00">12:00</option>
                                    <option <?= $time == '13:00' ? 'selected' : '' ?> value="13:00">13:00</option>
                                    <option <?= $time == '14:00' ? 'selected' : '' ?> value="14:00">14:00</option>
                                    <option <?= $time == '15:00' ? 'selected' : '' ?> value="15:00">15:00</option>
                                    <option <?= $time == '16:00' ? 'selected' : '' ?> value="16:00">16:00</option>

                                </select>
                            </div>
                            <div class="mb-3 col-lg-6 col-md-12 col-sm-12 col-xs-12">

                                <label for="exampleFormControlInput1" class="form-label">&nbsp;</label>
                                <select class="form-control" id="no_of_hours" aria-label="Default select example">
                                    <option value="" >Number Of Hours</option>
                                    <option <?= $event->number_of_hours == '5' ? 'selected' : '' ?> value="5">05</option>
                                    <option <?= $event->number_of_hours == '6' ? 'selected' : '' ?> value="6">06</option>
                                    <option <?= $event->number_of_hours == '7' ? 'selected' : '' ?> value="7">07</option>
                                    <option <?= $event->number_of_hours == '8' ? 'selected' : '' ?> value="8">08</option>
                                    <option <?= $event->number_of_hours == '9' ? 'selected' : '' ?> value="9">09</option>
                                    <option <?= $event->number_of_hours == '10' ? 'selected' : '' ?> value="10">10 </option>
                                </select>
                            </div>



                            <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label for="exampleFormControlInput1" class="form-label"> No Of Guest</label>
                                <input type="number" class="form-control" id="rfq_no_of_guests" value="<?= $event->number_of_attendees ?>" readonly placeholder="Number Of Guests">
                            </div>

                            <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label for="exampleFormControlInput1" class="form-label">Estimated Budget :</label>

                                <div class="form-check">
                                    <input class="form-check-input" minbudget="7000" maxbudget="0" name="budget" type="radio" checked value="1" id="flexCheckDefault11a">
                                    <label class="form-check-label" for="flexCheckDefault11a">
                                        Over AED 7,000
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" minbudget="6000" maxbudget="7000" name="budget" type="radio" value="2" id="flexCheckDefault12b">
                                    <label class="form-check-label" for="flexCheckDefault12b">
                                        AED 6,000 - AED 7,000
                                    </label>
                                </div>


                                <div class="form-check">
                                    <input class="form-check-input" minbudget="5000" maxbudget="6000" name="budget" type="radio" value="3" id="flexCheckDefault12c">
                                    <label class="form-check-label" for="flexCheckDefault12c">
                                        AED 5,000 - AED 6,000
                                    </label>
                                </div>


                                <div class="form-check">
                                    <input class="form-check-input" minbudget="4000" maxbudget="5000" name="budget" type="radio" value="4" id="flexCheckDefault12d">
                                    <label class="form-check-label" for="flexCheckDefault12d">
                                        AED 4,000 - AED 5,000
                                    </label>
                                </div>



                                <div class="form-check">
                                    <input class="form-check-input" minbudget="3000" maxbudget="4000" name="budget" type="radio" value="5" id="flexCheckDefault12e">
                                    <label class="form-check-label" for="flexCheckDefault12e">
                                        AED 3,000 - AED 4,000
                                    </label>
                                </div>


                                <div class="form-check">
                                    <input class="form-check-input" minbudget="2000" maxbudget="3000" name="budget" type="radio" value="6" id="flexCheckDefault12f">
                                    <label class="form-check-label" for="flexCheckDefault12f">
                                        AED 2,000 - AED 3,000
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" minbudget="0" maxbudget="2000" name="budget" type="radio" value="7" id="flexCheckDefault12o">
                                    <label class="form-check-label" for="flexCheckDefault12o">
                                        Less than AED 2,000
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" minbudget="0" maxbudget="0" name="budget" type="radio" value="8" id="flexCheckDefault12x">
                                    <label class="form-check-label" for="flexCheckDefault12x">
                                        I'm not sure yet
                                    </label>
                                </div>

                            </div>

                            <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label for="exampleFormControlInput1" class="form-label">Would you like to meet this vendor over video chat?</label>

                                <div class="form-check">
                                    <input class="form-check-input" name="video_chat" type="radio" value="Yes please" id="flexCheckDefault1x1">
                                    <label class="form-check-label" for="flexCheckDefault1x1">
                                        Yes please!
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" name="video_chat" checked type="radio" value="Maybe later" id="flexCheckDefault1x2">
                                    <label class="form-check-label" for="flexCheckDefault1x2">
                                        Maybe later
                                    </label>
                                </div>
                            </div>
                            <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label for="exampleFormControlInput1" class="form-label">Note</label>
                                <textarea class="form-control" id="rfq_quick_note" placeholder="Hello! My partner and I are getting married and we're interested in learning more about pricing and services." rows="3"></textarea>
                            </div>


                        </div>



                    </div>
                    <div class="modal-footer">

                        <button onclick="sendRFQ();" type="button" class="btn btn-primary __outlinbnt">Submit</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $html = ob_get_clean();
        return $html;
    }

}
