<?php

namespace App\Helpers;

use App\Database;
use Config;
use Session;

class LibHelper {

    public static function GetvendorCategory() {
        $Sql = "SELECT * FROM `categories` WHERE parent_id IS null AND status='1'";
        $servc = Database::select($Sql);
        return $servc;
    }

    public static function GetbannermanagementBybannerId($bannerId = '') {
        $Sql = "SELECT SC.* FROM `banner_management` SC WHERE SC.banner_id='$bannerId'";
        $dArray = Database::selectSingle($Sql);

        return $dArray;
    }

    public static function GetcmsBycmsId($cmsId = '') {
        $Sql = "SELECT SC.* FROM `general_cms` SC WHERE SC.cms_id='$cmsId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GetoriginalchecklistById($Id = '') {
        $Sql = "SELECT SC.* FROM `original_checklist` SC WHERE SC.id='$Id'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GetwebpagedesigneventId($eventId = '') {
        $Sql = "SELECT SC.* FROM `web_page_design_event` SC WHERE SC.event_id='$eventId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GetwebpagedesignId($Id = '') {
        $Sql = "SELECT SC.* FROM `web_page_design` SC WHERE SC.id='$Id'";
        $dArray = Database::selectSingle($Sql);
        if (!empty($dArray)) {
            $dArray->logo_url = url("web-pages/$dArray->logo");
        }
        return $dArray;
    }

    public static function Getoccasions() {
        $Sql = "SELECT * FROM `occasions` WHERE status='1'";
        $servc = Database::select($Sql);
        return $servc;
    }

    public static function GetusersBy($userId = '') {
        $select=",RU.role_id";
        $join="LEFT JOIN role_user RU ON RU.user_id=U.id";
        $Sql = "SELECT U.* $select FROM `users` U $join  WHERE U.id='$userId'";
        $servc = Database::selectSingle($Sql);
        if (!empty($servc)) {
            $baseDir = Config::get('constants.HOME_DIR');

            $baseFile = "storage/" . $servc->profile_picture;
            if (is_file($baseDir . $baseFile)) {
                $servc->profile_picture_logo = url($baseFile);
            } else {
                $servc->profile_picture_logo = url(Config::get('constants.DEFAULT_LOGO_VENDOR'));
            }
        }
        return $servc;
    }

    public static function GetvendordetailsBy($userId = '') {
        $Sql = "SELECT * FROM `vendor_details` WHERE user_id='$userId'";
        $servc = Database::selectSingle($Sql);
        return $servc;
    }

    public static function GetcategoriesById($Id = '') {
        $Sql = "SELECT * FROM `categories` WHERE id='$Id'";
        $servc = Database::selectSingle($Sql);
        return $servc;
    }
 public static function GetrsvpdesignById($Id = '') {
        $Sql = "SELECT * FROM `rsvp_design` WHERE id='$Id'";
        $servc = Database::selectSingle($Sql);
        return $servc;
    }
    public static function GetvendoreventsById($Id = '') {
        $select = ",O.name AS occasions_name";
        $join = " LEFT JOIN occasions O ON O.id=VE.occasion_id";
        $Sql = "SELECT VE.* $select FROM `vendor_events` VE $join WHERE VE.id='$Id'";
        $servc = Database::selectSingle($Sql);
        if (!empty($servc)) {
            $baseDir = Config::get('constants.HOME_DIR');

            $baseFile = "storage/" . $servc->document;
            if (is_file($baseDir . $baseFile)) {
                $servc->event_logo = url($baseFile);
            } else {
                $servc->event_logo = url(Config::get('constants.DEFAULT_LOGO_VENDOR'));
            }
        }
        return $servc;
    }

    public static function getNewRequestno() {
        $Sql = "SELECT MAX(id) AS max_num FROM `customer_request`";
        $dArray = Database::selectSingle($Sql);
        $max_num = !empty($dArray->max_num) ? ($dArray->max_num + 1) : '1';
        $max_num = 'R-' . str_pad($max_num, 5, "0", STR_PAD_LEFT);
        return $max_num;
    }

    public static function GetguestlisteventsById($Id = '') {
        $Sql = "SELECT A.*,RD.layout_file_name FROM `guest_list_events` A LEFT JOIN rsvp_design RD ON RD.id=A.layout_id WHERE A.id='$Id'";
        $servc = Database::selectSingle($Sql);
        return $servc;
    }

    public static function GeteventByuserId($userId = '') {
        $cond = "";
        $user = LibHelper::GetusersBy($userId);
        $roleId = $user->role_id;
        $fromEPRequest = "";
        $customerReq = " UNION ALL SELECT CR.event_id AS id,VE.event_name FROM `customer_request` CR LEFT JOIN vendor_events VE ON VE.id=CR.event_id WHERE CR.vendor_id='$userId' AND CR.status='2'";
        if ($roleId == '2') {
            $fromEPRequest = " UNION ALL SELECT EPR.event_id AS id,VE.event_name FROM `event_planner_request` EPR LEFT JOIN vendor_events VE ON VE.id=EPR.event_id WHERE EPR.vendor_id='$userId'";
        }

        if ($roleId == '2' || $roleId == '3') {
            $cond = $cond . " AND vendor_id='$userId'";
        } else {
            $cond = $cond . " AND user_id='$userId'";
        }
        $Sql = "SELECT id,event_name FROM `vendor_events` WHERE status=0  $cond $customerReq $fromEPRequest";
        $mainSQl = "SELECT M.* FROM ($Sql) AS M GROUP BY M.id";


        $data = \App\Database::select($mainSQl);
       return $data;
    }

}
