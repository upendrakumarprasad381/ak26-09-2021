<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Imports\VendorsImport;
use App\Jobs\SendEmailJob;
use App\Models\Admin\Master\Category;
use App\Models\Admin\Master\Occasion;
use App\Models\Admin\Master\VendorFilter;
use App\Models\Admin\Master\VendorFilterItem;
use App\Models\User;
use App\Models\Vendor\VendorDetail;
use App\Models\Vendor\VendorFilterValue;
use App\Notifications\UserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Notification;

class ManageVendorsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the event planners list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function eventPlannersIndex()
    {
        return view('admin.vendorsManagement.index')->with([
            'type' => 'Event Planners',
            'rows' => User::whereRoleIs('event_planner')->where('parent_id', null)->orderBy('id', 'DESC')->get(),
            'slug' => 'event-planner',
            'page_title' => 'Manage Vendors'
        ]);
    }

    /**
     * Show the vendors list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function vendorsIndex()
    {
        return view('admin.vendorsManagement.index')->with([
            'type' => 'Vendors',
            'rows' => User::whereRoleIs('vendor')->where('parent_id', null)->orderBy('id', 'DESC')->get(),
            'slug' => 'vendor',
            'page_title' => 'Manage Vendors'
        ]);
    }

    /**
     * Show the event planner.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function eventPlannerShow($id)
    {
        return view('admin.vendorsManagement.show')->with([
            'type' => 'Event Planner',
            'item' => User::find($id),
            'slug' => 'event-planners',
            'page_title' => 'Event Planner View',
            'categories' => Category::where('status', 1)->get(),
            'occasions' => Occasion::where('status', 1)->get(),
            'page_title' => 'Event Planners'
        ]);
    }

    /**
     * Show the vendor.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function vendorShow($id)
    {
        return view('admin.vendorsManagement.show')->with([
            'type' => 'Vendor',
            'item' => User::find($id),
            'slug' => 'vendors',
            'page_title' => 'Vendor View',
            'categories' => Category::where('status', 1)->get(),
            'occasions' => Occasion::where('status', 1)->get(),
            'page_title' => 'Vendors'
        ]);
    }

    /**
     * Approval for Vendors.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function vendorApproval($id)
    {
        $vendor = User::find($id);
        $vendor->status = 0;
        $vendor->save();

        $details['email'] = $vendor->email;
        $details['subject'] = 'Ask Deema Registration';
        $details['file'] = 'emails.email';
        $details['name'] = $vendor->first_name . ' ' . $vendor->last_name;
        $details['message_html'] = '<p>Welcome to Askdeema.</p>
        <p>This email is to confirm that your <b>Ask Deema</b> Account has been approved.</p>
        <p>Login Url : <a href="' . URL::to('/') . '/login">' . URL::to('/') . '/login</a></p>
        <p>Thank You</p>
        <p>Ask Deema</p>';

        dispatch(new SendEmailJob($details));

        $notification['text'] = 'Your Ask Deema Account has been approved.';
        $notification['link'] = null; //if no link given Null

        Notification::send(User::find($vendor->id), new UserNotification($notification));

        return response()->json(array('status' => true, 'message' => 'Vendor is Approved.', 'slug' => str_replace('_', '-', $vendor->roles[0]->name) . 's'), 200);
    }

    public function updateVendor($id)
    {
        $request = request();
        $vendor = User::find($id);

        if ($request->has('trade_license')) {
            $trade_license = $this->fileUpload($request, 'trade_license', 'vendor/documents/' . $id);
            VendorDetail::where('user_id', $id)->update(['trade_license' => $trade_license]);
        }
        if ($request->has('identity_proof')) {
            $identity_proof = $this->fileUpload($request, 'identity_proof', 'vendor/documents/' . $id);
            VendorDetail::where('user_id', $id)->update(['identity_proof' => $identity_proof]);
        }
        if ($request->has('passport')) {
            $passport = $this->fileUpload($request, 'passport', 'vendor/documents/' . $id);
            VendorDetail::where('user_id', $id)->update(['passport' => $passport]);
        }
        if ($request->has('status')) {
            $vendor->status = $request->status;
            $vendor->save();
        }
        if ($request->occasions) {
            DB::table('vendor_occasions')->where('user_id', $vendor->id)->delete();
            foreach ($request->occasions as $key => $value) {
                DB::table('vendor_occasions')->insert(
                    array(
                        'occasion_id' => $value,
                        'user_id' => $vendor->id
                    )
                );
            }
        }
        if ($request->emirates) {
            DB::table('vendor_emirate')->where('user_id', $vendor->id)->delete();
            foreach ($request->emirates as $key => $value) {
                DB::table('vendor_emirate')->insert(
                    array(
                        'emirate_id' => $value,
                        'user_id' => $vendor->id
                    )
                );
            }
        }
        if ($request->categories) {
            DB::table('vendor_category')->where('user_id', $vendor->id)->delete();
            foreach ($request->categories as $key => $value) {
                DB::table('vendor_category')->insert(
                    array(
                        'category_id' => $value,
                        'user_id' => $vendor->id,
                        'updated_at' => date('Y-m-d')
                    )
                );
            }
        }

        return response()->json(array('status' => true, 'message' => 'Vendor Data Updated.', 'slug' => str_replace('_', '-', $vendor->roles[0]->name) . 's'), 200);
    }

    public function updateBusinessInformation()
    {
        $request = request();
        // $request->validate([
        //     'location' => 'required',
        //     'address' => 'required',
        // ]);
        $vendor = Auth::user();
        VendorDetail::where('user_id', $vendor->id)->update([
            'emirates' => $request->location,
            'business_website' => $request->business_website,
            'social_media_link' => $request->social_media_link,
            'address' => $request->address,
            'focus' => $request->focus,
            'instagram' => $request->instagram,
            'facebook' => $request->facebook,
            'linkedin' => $request->linkedin,
            'twiter' => $request->twiter,
            'email_2' => $request->email_2,
            'emirates_availability' => $request->emirates_availability,
            'city' => $request->city,
            'country' => $request->country,
            // 'cuisine_focus' => $request->cuisine_focus,
            // 'menu_type' => $request->menu_type,
            // 'experience_focus' => $request->experience_focus,
            'min_price' => json_decode($request->price_range)[0],
            'max_price' => json_decode($request->price_range)[1],
            // 'capacity' => $request->capacity,
            'about' => $request->about,
            'whatsapp' => $request->whatsapp
        ]);
        if ($request->occasions) {
            DB::table('vendor_occasions')->where('user_id', $vendor->id)->delete();
            foreach ($request->occasions as $key => $value) {
                DB::table('vendor_occasions')->insert(
                    array(
                        'occasion_id' => $value,
                        'user_id' => $vendor->id
                    )
                );
            }
        }
        if ($request->categories) {
            DB::table('vendor_category')->where('user_id', $vendor->id)->delete();
            foreach ($request->categories as $key => $value) {
                DB::table('vendor_category')->insert(
                    array(
                        'category_id' => $value,
                        'user_id' => $vendor->id,
                        'updated_at' => date('Y-m-d')
                    )
                );
            }
        }
        if ($request->emirates) {
            DB::table('vendor_emirate')->where('user_id', $vendor->id)->delete();
            foreach ($request->emirates as $key => $value) {
                DB::table('vendor_emirate')->insert(
                    array(
                        'emirate_id' => $value,
                        'user_id' => $vendor->id
                    )
                );
            }
        }
        $vendor_details = VendorDetail::where('user_id', $vendor->id)->first();
        if ($request->has('logo')) {
            $this->fileDelete($vendor_details->logo);
            $logo = $this->fileUpload($request, 'logo', 'vendor/logo');
            VendorDetail::where('user_id', $vendor->id)->update(['logo' => $logo]);
        } else if (!$request->old_logo) {
            $this->fileDelete($vendor_details->logo);
            VendorDetail::where('user_id', $vendor->id)->update(['logo' => null]);
        }

        if ($request->has('trade_license')) {
            $trade_license = $this->fileUpload($request, 'trade_license', 'vendor/documents/' . $vendor->id);
            VendorDetail::where('user_id', $vendor->id)->update(['trade_license' => $trade_license]);
        }
        if ($request->has('identity_proof')) {
            $identity_proof = $this->fileUpload($request, 'identity_proof', 'vendor/documents/' . $vendor->id);
            VendorDetail::where('user_id', $vendor->id)->update(['identity_proof' => $identity_proof]);
        }
        if ($request->has('passport')) {
            $passport = $this->fileUpload($request, 'passport', 'vendor/documents/' . $vendor->id);
            VendorDetail::where('user_id', $vendor->id)->update(['passport' => $passport]);
        }
        DB::table('vendor_filter_values')->where('vendor_id', $vendor->id)->delete();
        if ($request->filters) {
            foreach ($request->filters as $key => $value) {
                if ($value) {
                    $filter = VendorFilter::find($key);
                    if ($filter->type == 'Text Box') {
                        $filter_value = new VendorFilterValue();
                        $filter_value->vendor_id = $vendor->id;
                        $filter_value->filter_id = $filter->id;
                        $filter_value->value = $value;
                        $filter_value->save();
                    } else if ($filter->type == 'Radio Button') {
                        $filter_value = new VendorFilterValue();
                        $filter_value->vendor_id = $vendor->id;
                        $filter_value->filter_id = $filter->id;
                        $filter_value->filter_item_id = $value[0];
                        $filter_value->value = VendorFilterItem::find($value[0])->text;
                        $filter_value->save();
                    } else {
                        foreach ($value as $key_ => $value_) {
                            $filter_value = new VendorFilterValue();
                            $filter_value->vendor_id = $vendor->id;
                            $filter_value->filter_id = $filter->id;
                            $filter_value->filter_item_id = $value_;
                            $filter_value->value = VendorFilterItem::find($value_)->text;
                            $filter_value->save();
                        }
                    }
                }
            }
        }

        return response()->json(array('status' => true, 'message' => 'Vendor Data Updated.', 'slug' => str_replace('_', '-', $vendor->roles[0]->name) . 's'), 200);
    }

    public function importVendors()
    {
        return view('admin.vendorsManagement.import')->with([
            'page_title' => 'Import Vendors'
        ]);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function importExcel(Request $request)
    {
        $request->validate([
            'file' => 'required',
        ]);
        \Excel::import(new VendorsImport, $request->file);

        $data['message'] = 'Your file is imported successfully in database.';

        return $data;
    }

    public function getCities()
    {
        $request = request();
        return DB::table('cities')->where('country_id', $request->country_id)->get();
    }
}
