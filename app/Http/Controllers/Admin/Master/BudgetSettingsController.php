<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\Controller;
use App\Models\Admin\Master\BudgetSettings;
use App\Models\Admin\Master\Category;
use App\Models\Admin\Master\Occasion;
use App\Models\Vendor\VendorBudgeter;
use Illuminate\Http\Request;

class BudgetSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.master.budgeterSettings.index')->with([
            'rows' => Occasion::all(),
            'page_title' => 'Budget Settings'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type_of_event = Occasion::find(request()->type_of_event);
        $budgets = BudgetSettings::where('occasion_id', $type_of_event->id)->get();
        if (count($budgets) == 0) {
            $budgets = Category::where('type', 'Vendor')->orWhere('type', 'Event Planner')->orderBy('type', 'Desc')->get();
        }

        return view('admin.master.budgeterSettings.create')->with([
            'rows' => $budgets,
            'type_of_event' => $type_of_event,
            'page_title' => 'Budget Settings'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->category as $key => $value) {
            $exist = BudgetSettings::where('occasion_id', $request->occasion_id)->where('category_id', $value)->first();
            if ($exist) {
                $exist->budget = $request->budget[$key];
                $exist->budget_percentage = $request->percentage[$key];
                $exist->save();
            } else {
                $new = new BudgetSettings();
                $new->occasion_id = $request->occasion_id;
                $new->category_id = $value;
                $new->budget = $request->budget[$key];
                $new->budget_percentage = $request->percentage[$key];
                $new->save();
            }
        }

        $data['message'] = 'Budget Succesfully Updated.';
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Master\BudgetSettings  $budgetSettings
     * @return \Illuminate\Http\Response
     */
    public function show(BudgetSettings $budgetSettings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Master\BudgetSettings  $budgetSettings
     * @return \Illuminate\Http\Response
     */
    public function edit(BudgetSettings $budgetSettings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Master\BudgetSettings  $budgetSettings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BudgetSettings $budgetSettings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Master\BudgetSettings  $budgetSettings
     * @return \Illuminate\Http\Response
     */
    public function destroy(BudgetSettings $budgetSettings)
    {
        //
    }
}
