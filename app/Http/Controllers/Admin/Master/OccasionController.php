<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Master\OccasionRequest;
use App\Models\Admin\Master\Occasion;
use Illuminate\Http\Request;

class OccasionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.master.occasions.index')->with([
            'occasions' => Occasion::all(),
            'page_title' => 'Occasions'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master.occasions.create')->with([
            'page_title' => 'Occasions'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OccasionRequest $request)
    {
        $occasion = new Occasion();
        $occasion->name = $request->name;
        $occasion->image = $this->fileUpload($request,'image','master/occasions');
        $occasion->total_budget = 1000;
        $occasion->status = $request->status;
        $occasion->save();

        $data['message'] = 'Occasion succesfully created.';
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Master\Occasion  $occasion
     * @return \Illuminate\Http\Response
     */
    public function show(Occasion $occasion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Master\Occasion  $occasion
     * @return \Illuminate\Http\Response
     */
    public function edit(Occasion $occasion)
    {
        return view('admin.master.occasions.edit')->with([
            'occasion' => $occasion,
            'page_title' => 'Occasions'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Master\Occasion  $occasion
     * @return \Illuminate\Http\Response
     */
    public function update(OccasionRequest $request, Occasion $occasion)
    {
        $occasion->name = $request->name;
        $occasion->total_budget = 1000;
        if($request->has('image'))
        {
            $this->fileDelete($occasion->image);
            $occasion->image = $this->fileUpload($request,'image','master/occasions');
        }
        else if(!$request->old_image)
        {
            $this->fileDelete($occasion->image);
            $occasion->image = null;
        }
        $occasion->status = $request->status;
        $occasion->save();

        $data['message'] = 'Occasion succesfully updated.';
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Master\Occasion  $occasion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Occasion $occasion)
    {
        $occasion->delete();
    }
}
