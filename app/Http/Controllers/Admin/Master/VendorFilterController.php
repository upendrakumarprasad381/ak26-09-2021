<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\Controller;
use App\Models\Admin\Master\CategoryFilter;
use App\Models\Admin\Master\VendorFilter;
use App\Models\Admin\Master\VendorFilterItem;
use App\Models\Vendor\VendorBudget;
use App\Models\Vendor\VendorFilterValue;
use Auth;
use DB;
use Illuminate\Http\Request;

class VendorFilterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.master.vendorFilters.index')->with([
            'rows' => VendorFilter::all(),
            'page_title' => 'Vendor Filters'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master.vendorFilters.create')->with([
            'page_title' => 'Vendor Filters'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $filter = new VendorFilter();
        $filter->name = $request->name;
        $filter->type = $request->type;
        $filter->status = $request->status;
        $filter->save();
        if ($filter && $request->value) {
            foreach ($request->value as $key => $value_) {
                $filter_item = new VendorFilterItem();
                $filter_item->vendor_filter_id = $filter->id;
                $filter_item->text = $value_;
                $filter_item->save();
                $filter_item->value = $filter_item->id;
                $filter_item->save();
            }
        }
        $data['message'] = 'Filter succesfully created.';
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Master\VendorFilter  $vendorFilter
     * @return \Illuminate\Http\Response
     */
    public function show(VendorFilter $vendorFilter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Master\VendorFilter  $vendorFilter
     * @return \Illuminate\Http\Response
     */
    public function edit(VendorFilter $vendorFilter)
    {
        return view('admin.master.vendorFilters.edit')->with([
            'editable' => $vendorFilter,
            'page_title' => 'Vendor Filters'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Master\VendorFilter  $vendorFilter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VendorFilter $vendorFilter)
    {
        $vendorFilter->name = $request->name;
        $vendorFilter->type = $request->type;
        $vendorFilter->status = $request->status;
        $vendorFilter->save();
        $ids = [];
        if ($vendorFilter && $request->value) {
            foreach ($request->value as $key => $value) {
                $exist = VendorFilterItem::find($request->value_id[$key] ?? null);
                if ($exist) {
                    $filter_item = $exist;
                    array_push($ids, $exist->id);
                } else {
                    $filter_item = new VendorFilterItem();
                }
                $filter_item->vendor_filter_id = $vendorFilter->id;
                $filter_item->text = $value;
                $filter_item->save();
                $filter_item->value = $filter_item->id;
                $filter_item->save();
                if (!$exist) {
                    array_push($ids, $filter_item->id);
                }
            }
            VendorFilterItem::where('vendor_filter_id',$filter_item->id)->whereNotIn('id', $ids)->delete();
        }
        $data['message'] = 'Filter succesfully updated.';
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Master\VendorFilter  $vendorFilter
     * @return \Illuminate\Http\Response
     */
    public function destroy(VendorFilter $vendorFilter)
    {

    }
    public function getFilters()
    {
        $request = request();
        $filters = [];
        if($request->categories)
        {
            $filters = CategoryFilter::whereIn('category_id',$request->categories)->with('filter','filter.items')->groupBy('filter_id')->get();
            foreach ($filters as $key => $value) {
                $filetr_value = VendorFilterValue::where('vendor_id',Auth::user()->id)->where('filter_id',$value->filter_id)->get();
                $filters[$key]['filter_value'] = $filetr_value;
            }
        }
        return $filters;
    }
}
