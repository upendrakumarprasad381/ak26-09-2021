<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Master\VendorItemRequest;
use App\Models\Admin\Master\Category;
use App\Models\Admin\Master\VendorItem;
use Illuminate\Http\Request;

class VendorItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.master.vendorItems.index')->with([
            'items' => VendorItem::all(),
            'page_title' => 'Vendor Items'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('parent_id',null)->get();
        return view('admin.master.vendorItems.create')->with(
            [
                'categories' => $categories,
                'page_title' => 'Vendor Items'
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorItemRequest $request)
    {
        $item = new VendorItem();
        $item->name = $request->name;
        $item->category_id = $request->category_id;
        $item->save();

        $data['message'] = 'Item succesfully created.';
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Master\VendorItem  $vendorItem
     * @return \Illuminate\Http\Response
     */
    public function show(VendorItem $vendorItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Master\VendorItem  $vendorItem
     * @return \Illuminate\Http\Response
     */
    public function edit(VendorItem $vendorItem)
    {
        $categories = Category::where('parent_id',null)->get();
        return view('admin.master.vendorItems.edit')->with(
            [
                'categories' => $categories,
                'item' => $vendorItem,
                'page_title' => 'Vendor Items'
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Master\VendorItem  $vendorItem
     * @return \Illuminate\Http\Response
     */
    public function update(VendorItemRequest $request, VendorItem $vendorItem)
    {
        $vendorItem->name = $request->name;
        $vendorItem->category_id = $request->category_id;
        $vendorItem->save();

        $data['message'] = 'Item succesfully updated.';
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Master\VendorItem  $vendorItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(VendorItem $vendorItem)
    {
        $vendorItem->delete();
    }
}
