<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Master\VendorTaxRequest;
use App\Models\Admin\Master\VendorTax;
use Illuminate\Http\Request;

class VendorTaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.master.vendorTaxes.index')->with([
            'items' => VendorTax::all(),
            'page_title' => 'Vendor Taxes'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master.vendorTaxes.create')->with([
            'page_title' => 'Vendor Taxes'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorTaxRequest $request)
    {
        $tax = new VendorTax();
        $tax->name = $request->name;
        $tax->value = $request->value;
        $tax->save();

        $data['message'] = 'Tax succesfully created.';
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Master\VendorTax  $vendorTax
     * @return \Illuminate\Http\Response
     */
    public function show(VendorTax $vendorTax)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Master\VendorTax  $vendorTax
     * @return \Illuminate\Http\Response
     */
    public function edit(VendorTax $vendorTax)
    {
        return view('admin.master.vendorTaxes.edit')->with([
            'item' => $vendorTax,
            'page_title' => 'Vendor Taxes'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Master\VendorTax  $vendorTax
     * @return \Illuminate\Http\Response
     */
    public function update(VendorTaxRequest $request, VendorTax $vendorTax)
    {
        $vendorTax->name = $request->name;
        $vendorTax->value = $request->value;
        $vendorTax->save();

        $data['message'] = 'Tax succesfully created.';
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Master\VendorTax  $vendorTax
     * @return \Illuminate\Http\Response
     */
    public function destroy(VendorTax $vendorTax)
    {
        $vendorTax->delete();
    }
}
