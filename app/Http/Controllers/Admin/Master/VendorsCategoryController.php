<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Master\Category\CategoryRequests;
use App\Models\Admin\Master\Category;
use App\Models\Admin\Master\VendorFilter;
use DB;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class VendorsCategoryController extends Controller
{
    private $type = 'Vendor';
    private $slug = 'vendors-categories';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.master.categories.categories')->with([
            'categories' => Category::where('type',$this->type)->get(),
            'type' => $this->type,
            'slug' => $this->slug,
            'page_title' => 'Categories'
        ]);
    }

    /**
     * Get all data.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData()
    {
        $categries = Category::where('type',$this->type);
        return DataTables::of($categries)
                ->addIndexColumn()
                ->addColumn('parent', function($row){
                        return $row->parent?$row->parent->name:'';
                })
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parent_categories = Category::where('type',$this->type)->where('parent_id',null)->get();
        return view('admin.master.categories.create')->with(
            [
                'parent_categories' => $parent_categories,
                'type' => $this->type,
                'slug' => $this->slug,
                'page_title' => 'Categories',
                'filters' => VendorFilter::where('status',1)->get(),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequests $request)
    {
        $category = new Category();
        $category->name = $request->name;
        $category->slug = $request->slug;
        $category->parent_id = $request->parent;
        $category->status = $request->status;
        $category->type = $request->type;
        $category->qty_label = $request->qty_label;
        $category->icon = $this->fileUpload($request,'icon','category/icons');
        $category->banner_image = $this->fileUpload($request,'banner_image','category/banner_images');

        $category->save();

        if($request->filters)
        {
            foreach ($request->filters as $key => $value) {
                DB::table('category_filters')->insert(
                    array(
                        'category_id' => $category->id,
                        'filter_id' => $value
                    )
                );
            }
        }

        $data['message'] = 'Category succesfully created.';
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Master\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Master\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $parent_categories = Category::where('type',$this->type)->where('parent_id',null)->where('id','!=',$id)->get();
        return view('admin.master.categories.edit')->with(
            [
                'parent_categories' => $parent_categories,
                'category' =>  $category,
                'slug' => $this->slug,
                'page_title' => 'Categories',
                'filters' => VendorFilter::where('status',1)->get(),
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Master\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequests $request, $id)
    {
        $category = Category::find($id);
        $category->name = $request->name;
        $category->slug = $request->slug;
        $category->parent_id = $request->parent;
        $category->status = $request->status;
        $category->type = $request->type;
        $category->qty_label = $request->qty_label;
        if($request->has('icon'))
        {
            $this->fileDelete($category->icon);
            $category->icon = $this->fileUpload($request,'icon','category/icons');
        }
        else if(!$request->old_icon)
        {
            $this->fileDelete($category->icon);
            $category->icon = null;
        }

        if($request->has('banner_image'))
        {
            $this->fileDelete($category->banner_image);
            $category->banner_image = $this->fileUpload($request,'banner_image','category/banner_images');
        }
        else if(!$request->old_banner_image)
        {
            $this->fileDelete($category->banner_image);
            $category->banner_image = null;
        }

        if($request->filters)
        {
            DB::table('category_filters')->where('category_id',$category->id)->delete();
            foreach ($request->filters as $key => $value) {
                DB::table('category_filters')->insert(
                    array(
                        'category_id' => $category->id,
                        'filter_id' => $value
                    )
                );
            }
        }

        $category->save();

        $data['message'] = 'Category succesfully updated.';
        return $data;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Master\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
    }

    public function getSubServices($id)
    {
        return Category::where('parent_id',$id)->get();
    }
}
