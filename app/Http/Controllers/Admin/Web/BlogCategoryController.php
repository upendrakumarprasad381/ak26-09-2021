<?php

namespace App\Http\Controllers\Admin\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Web\BlogCategoryRequest;
use App\Models\Admin\Web\BlogCategory;
use Illuminate\Http\Request;

class BlogCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.web.blogs.categories.index')->with([
            'categories' => BlogCategory::all(),
            'page_title' => 'Blog Categories'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.web.blogs.categories.create')->with([
            'page_title' => 'Blog Categories'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogCategoryRequest $request)
    {
        $blogCategory = new BlogCategory();
        $blogCategory->name = $request->name;
        $blogCategory->slug = $request->slug;
        $blogCategory->status = $request->status;
        $blogCategory->image = $this->fileUpload($request,'image','blog_category/images');

        $blogCategory->save();
        $data['message'] = 'Category succesfully created.';
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Web\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function show(BlogCategory $blogCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Web\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blogCategory = BlogCategory::find($id);
        return view('admin.web.blogs.categories.edit')->with([
            'category' => $blogCategory,
            'page_title' => 'Blog Categories'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Web\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function update(BlogCategoryRequest $request, $id)
    {
        $blogCategory = BlogCategory::find($id);
        $blogCategory->name = $request->name;
        $blogCategory->slug = $request->slug;
        $blogCategory->status = $request->status;
        if($request->has('image'))
        {
            $this->fileDelete($blogCategory->image);
            $blogCategory->image = $this->fileUpload($request,'image','blog_category/images');
        }
        else if(!$request->old_image)
        {
            $this->fileDelete($blogCategory->image);
            $blogCategory->image = null;
        }

        $blogCategory->save();
        $data['message'] = 'Category succesfully updated.';
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Web\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blogCategory = BlogCategory::find($id);
        $blogCategory->delete();
    }
}
