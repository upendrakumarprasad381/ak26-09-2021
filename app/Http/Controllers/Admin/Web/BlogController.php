<?php

namespace App\Http\Controllers\Admin\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Web\BlogRequest;
use App\Models\Admin\Web\Blog;
use App\Models\Admin\Web\BlogCategory;
use App\Models\Admin\Web\BlogTag;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.web.blogs.index')->with([
            'blogs' => Blog::orderBy('id','DESC')->get(),
            'page_title' => 'Blogs'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.web.blogs.create')->with([
            'categories' => BlogCategory::where('status',1)->get(),
            'tags' => BlogTag::where('status',1)->get(),
            'page_title' => 'Blogs'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        $blog = new Blog();
        $blog->title = $request->title;
        $blog->slug = $request->slug;
        $blog->description = $request->description;
        $blog->content = $request->content;
        $blog->category_id = $request->category;
        $blog->tags = json_encode($request->tags);
        $blog->image = $this->fileUpload($request,'image','web/blogs');
        $blog->seo_description = $request->seo_description;
        $blog->seo_keywords = $request->seo_keywords;
        $blog->status = $request->status;
        $blog->save();

        if($request->tags)
            $this->updateTags($request->tags);

        $data['message'] = 'Blog succesfully created.';
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Web\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Web\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        return view('admin.web.blogs.edit')->with([
            'blog' => $blog,
            'categories' => BlogCategory::where('status',1)->get(),
            'tags' => BlogTag::where('status',1)->get(),
            'page_title' => 'Blogs'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Web\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(BlogRequest $request, Blog $blog)
    {
        $blog->title = $request->title;
        $blog->slug = $request->slug;
        $blog->description = $request->description;
        $blog->content = $request->content;
        $blog->category_id = $request->category;
        $blog->tags = json_encode($request->tags);
        if($request->has('image'))
        {
            $this->fileDelete($blog->image);
            $blog->image = $this->fileUpload($request,'image','web/blogs');
        }
        else if(!$request->old_image)
        {
            $this->fileDelete($blog->image);
            $blog->image = null;
        }

        $blog->seo_description = $request->seo_description;
        $blog->seo_keywords = $request->seo_keywords;
        $blog->status = $request->status;
        $blog->save();

        if($request->tags)
            $this->updateTags($request->tags);

        $data['message'] = 'Blog succesfully updated.';
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Web\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        $blog->delete();
    }
    public function updateTags($tags)
    {
        foreach ($tags as $key => $value) {
            $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $value);
            $tag = BlogTag::where('name',$value)->first();
            if(!$tag)
            {
               $tags = new BlogTag();
               $tags->name = $value;
               $tags->slug = $slug;
               $tags->save();
            }
        }
    }
}
