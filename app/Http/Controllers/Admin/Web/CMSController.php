<?php

namespace App\Http\Controllers\Admin\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Database;
use Config;
use Redirect;
use App\Helpers\LibHelper;

class CMSController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('admin.web.ourteams.index')->with([
        ]);
    }

    public function banner() {
        return view('admin.web.cms.banner')->with([
        ]);
    }

    public function addbanner($bannerId = '') {
        if (isset($_POST['submitBtn'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $bannerTitle = $json;
            $json['created_at'] = date('Y-m-d H:i:s');
            $file = !empty($_FILES['banner_file']) ? $_FILES['banner_file'] : [];
            if (!empty($file['name'])) {
                $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "image-list/" . $fileName;
                if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                    $json['image'] = $fileName;
                }
            }
            if (empty($bannerId)) {
                Database::insert('banner_management', $json);
            } else {
                Database::updates('banner_management', $json, ['banner_id' => $bannerId]);
            }
//            $Sql = "SELECT * FROM `banner_management`";
//            $banner = Database::select($Sql);
//            for ($i = 0; $i < count($banner); $i++) {
//                $d = $banner[$i];
//                Database::updates('banner_management', $bannerTitle, ['banner_id' => $d->banner_id]);
//            }
            return Redirect::to(url('/admin/cms/banner'));
        }
        return view('admin.web.cms.addbanner')->with([
                    'bannerId' => $bannerId,
        ]);
    }

    public function bannerRemove($bannerId = '') {
        $banner = LibHelper::GetbannermanagementBybannerId($bannerId);
        $baseDir = Config::get('constants.HOME_DIR') . "image-list/" . $banner->image;
        if (is_file($baseDir)) {
            unlink($baseDir);
        }
        Database::deletes('banner_management', ['banner_id' => $bannerId]);
    }

    public function contactus() {
        if (isset($_POST['submitBtn'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            Database::updates('general_cms', $json, array('cms_id' => 'CONTACT_US_DETAILS'));
        }
        return view('admin.web.cms.contactus')->with([
        ]);
    }

}
