<?php

namespace App\Http\Controllers\Admin\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Web\OurTeamRequest;
use App\Models\Admin\Web\OurTeam;
use Illuminate\Http\Request;

class OurTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.web.ourteams.index')->with([
            'our_teams' => OurTeam::all(),
            'page_title' => 'Our Teams'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.web.ourteams.create')->with([
            'page_title' => 'Our Teams'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OurTeamRequest $request)
    {
        $ourTeam = new OurTeam();
        $ourTeam->first_name = $request->first_name;
        $ourTeam->last_name = $request->last_name;
        $ourTeam->designation = $request->designation;
        $ourTeam->status = $request->status;

        $ourTeam->image = $this->fileUpload($request,'image','web/our_teams');

        $ourTeam->save();
        $data['message'] = 'Team member succesfully created.';
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Web\OurTeam  $ourTeam
     * @return \Illuminate\Http\Response
     */
    public function show(OurTeam $ourTeam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Web\OurTeam  $ourTeam
     * @return \Illuminate\Http\Response
     */
    public function edit(OurTeam $ourTeam)
    {
        return view('admin.web.ourteams.edit')->with([
            'our_team' => $ourTeam,
            'page_title' => 'Our Teams'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Web\OurTeam  $ourTeam
     * @return \Illuminate\Http\Response
     */
    public function update(OurTeamRequest $request, OurTeam $ourTeam)
    {
        $ourTeam->first_name = $request->first_name;
        $ourTeam->last_name = $request->last_name;
        $ourTeam->designation = $request->designation;
        $ourTeam->status = $request->status;

        if($request->has('image'))
        {
            $this->fileDelete($ourTeam->image);
            $ourTeam->image = $this->fileUpload($request,'image','web/our_teams');
        }
        else if(!$request->old_image)
        {
            $this->fileDelete($ourTeam->image);
            $ourTeam->image = null;
        }

        $ourTeam->save();
        $data['message'] = 'Team member succesfully updated.';
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Web\OurTeam  $ourTeam
     * @return \Illuminate\Http\Response
     */
    public function destroy(OurTeam $ourTeam)
    {
        $ourTeam->delete();
    }
}
