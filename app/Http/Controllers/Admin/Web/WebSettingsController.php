<?php

namespace App\Http\Controllers\Admin\Web;

use App\Http\Controllers\Controller;
use App\Models\Admin\Web\WebSettings;
use Illuminate\Http\Request;

class WebSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.web.contactUs.index')->with([
        'data' => WebSettings::latest()->first(),
        'page_title' => 'Contact Us'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $exist = WebSettings::latest()->first();
        if($exist)
        {
            $exist->address = $request->address;
            $exist->email = $request->email;
            $exist->phone = $request->phone;
            $exist->save();
        }
        else
        {
            $settings = new WebSettings();
            $settings->address = $request->address;
            $settings->email = $request->email;
            $settings->phone = $request->phone;
            $settings->save();
        }
        return response()->json(array('status' => true, 'message' => 'Web Data Updated.'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Web\WebSettings  $webSettings
     * @return \Illuminate\Http\Response
     */
    public function show(WebSettings $webSettings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Web\WebSettings  $webSettings
     * @return \Illuminate\Http\Response
     */
    public function edit(WebSettings $webSettings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Web\WebSettings  $webSettings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebSettings $webSettings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Web\WebSettings  $webSettings
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebSettings $webSettings)
    {
        //
    }
}
