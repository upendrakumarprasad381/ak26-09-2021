<?php

namespace App\Http\Controllers\Admin\Web;

use App\Http\Controllers\Controller;
use App\Models\Admin\Web\WebSlider;
use Illuminate\Http\Request;

class WebSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.web.slider.index')->with([
            'sliders' => WebSlider::all(),
            'page_title' => 'Sliders'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.web.slider.create')->with([
            'page_title' => 'Sliders'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required',
        ]);

        $slider = new WebSlider();
        $slider->title = $request->title;
        $slider->status = $request->status;
        $slider->image = $this->fileUpload($request,'image','web/sliders');
        $slider->save();
        $data['message'] = 'Slider succesfully created.';
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Web\WebSlider  $webSlider
     * @return \Illuminate\Http\Response
     */
    public function show(WebSlider $webSlider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Web\WebSlider  $webSlider
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $webSlider = WebSlider::find($id);
        return view('admin.web.slider.edit')->with([
            'slider' => $webSlider,
            'page_title' => 'Sliders'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Web\WebSlider  $webSlider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            // 'image' => 'required',
        ]);
        $webSlider = WebSlider::find($id);
        $webSlider->title = $request->title;
        $webSlider->status = $request->status;
        if($request->has('image'))
        {
            $this->fileDelete($webSlider->image);
            $webSlider->image = $this->fileUpload($request,'image','web/sliders');
        }
        else if(!$request->old_image)
        {
            $this->fileDelete($webSlider->image);
            $webSlider->image = null;
        }

        $webSlider->save();
        $data['message'] = 'Slider succesfully updated.';
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Web\WebSlider  $webSlider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $webSlider = WebSlider::find($id);
        $webSlider->delete();
    }
}
