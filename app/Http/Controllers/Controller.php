<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function fileUpload($request,$file,$path)
    {
        if($request->has($file))
        {
            $fileName = time().'.'.$request->$file->extension();
            $request->$file->storeAs('public/'.$path,$fileName);
            return $path.'/'.$fileName;
        }
        return null;
    }
    public function fileDelete($path)
    {
        $path = 'public/'.$path;
        if(Storage::exists($path)){
            Storage::delete($path);
        }
    }
}
