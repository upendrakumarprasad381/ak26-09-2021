<?php

namespace App\Http\Controllers\EventPlanner;

use App\Http\Controllers\Controller;
use App\Models\Admin\Master\Category;
use App\Models\Vendor\VendorBudget;
use App\Models\Vendor\VendorBudgetItem;
use App\Models\Vendor\VendorEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ManageBudgetController extends Controller
{
    private $user;
    private $event_id;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->parent_id != null ? Auth::user()->parent : Auth::user();
            $this->leads_counts = 1;
            $path = request()->path();
            $this->event_id = explode('/', $path)[2];
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $event = VendorEvent::find($this->event_id);
        $exist = VendorBudget::where('event_id', $this->event_id)->where('vendor_id', $this->user->id)->first();
        if ($event->final_budget != 0) {
            if ($exist) {
                $categories = Category::where('type', 'Vendor')->orWhere('type', 'Event Planner')->orderBy('type', 'DESC')->get();
                $budgets = VendorBudget::where('vendor_id', $this->user->id)->where('event_id', $this->event_id)->get();
                $budget_ = VendorBudget::where('vendor_id', $this->user->id)->where('event_id', $this->event_id)->where('is_default', 1)->first();
                if(!$budget_ && count($budgets) > 0)
                {
                    $upd = VendorBudget::where('vendor_id', $this->user->id)->where('event_id', $this->event_id)->first();
                    $upd->is_default = 1;
                    $upd->save();
                }
                $budget = VendorBudget::where('vendor_id', $this->user->id)->where('event_id', $this->event_id)->where('is_default', 1)->first();
                $details['total_budget'] = $event->final_budget;
                $details['actual_cost'] = $budget->items->sum('actual_cost');
                $details['actual_paid'] = $budget->items->sum('actual_paid');
                $details['total_paid'] = VendorBudgetItem::where('vendor_budget_id', $budget->id)->sum('actual_paid');

                return view('eventPlanner.budgeters.index')->with([
                    'categories' => $categories,
                    'page_title' => 'My Budgets',
                    'event' => $event,
                    'budgets' => $budgets,
                    'details' => $details,
                ]);
            } else {
                return $this->create();
            }
        } else {
            return redirect()->back()->with('show_buget_model', true);
        }
    }

    public function budgeterEntry()
    {
        $event = VendorEvent::find($this->event_id);
    }

    public function saveFinalBudget()
    {
        $request = request();
        $vendor_event = VendorEvent::find($this->event_id);
        $vendor_event->final_budget = $request->final_amount;
        $vendor_event->save();
        // $exist = VendorBudget::where('event_id', $this->event_id)->where('vendor_id', $this->user->id)->first();
        // if (!$exist) {
        //     $budget = new VendorBudget();
        //     $budget->event_id = $this->event_id;
        //     $budget->vendor_id = $this->user->id;
        //     $budget->final_amount = $request->total_budget;
        // }
        return $this->create();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event = VendorEvent::find($this->event_id);
        $categories = Category::where('type', 'Vendor')->orWhere('type', 'Event Planner')->orderBy('type', 'DESC')->get();
        $default_budget = [];
        foreach ($categories as $key => $value) {
            $percentage = $value->budget($event->occasion_id) ? $value->budget($event->occasion_id)['budget_percentage'] : 0;
            $default_budget[$key]['category'] = $value->name;
            $default_budget[$key]['category_id'] = $value->id;
            $default_budget[$key]['budget'] = ($event->final_budget / 100) * $percentage;
        }

        return view('eventPlanner.budgeters.create')->with([
            'categories' => $categories,
            'page_title' => 'Create Budget',
            'event' => $event,
            'rows' => [],
            'default_budget' => $default_budget,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = VendorEvent::find($this->event_id);
        $budget = new VendorBudget();
        $budget->event_id = $this->event_id;
        $budget->vendor_id = $this->user->id;
        $budget->total_budget = $event->final_budget;
        $budget->save();
        if ($request->budget_items) {
            foreach ($request->budget_items as $key => $value) {
                $item = new VendorBudgetItem();
                $item->vendor_budget_id = $budget->id;
                $item->category_id = $value['item_id'];
                $item->category_name = $value['item_name'];
                $item->budget = $value['budget'];
                $item->actual_cost = 0;
                $item->variance = 0;
                $item->save();
            }
        }
        return response()->json(array('status' => true, 'message' => 'Your Budgets Is Saved'), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function BudgetItemStore(Request $request)
    {
        $budget = VendorBudgetItem::where('vendor_budget_id', $request->budget_id)->where('category_name', $request->item_name)->first();
        if ($budget == null) {
            $budget = new VendorBudgetItem();
        }

        $budget->category_id = $request->item_id;
        $budget->category_name = $request->item_name;
        $budget->budget = $request->item_budget;
        $budget->actual_cost = $request->item_actual_cost;
        $budget->actual_paid = $request->item_actual_paid;
        $budget->variance = $request->item_variance;
        $budget->payment_status = $request->item_payment_status;
        $budget->budget_status = $request->item_variance >= 0 ? 'On Budget' : 'Over Budget';
        $budget->remarks = $request->item_remarks;
        $budget->save();

        return response()->json(array('status' => true, 'message' => 'Budget Item Updated.'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor\VendorBudget  $vendorBudget
     * @return \Illuminate\Http\Response
     */
    public function show($event_id,$id)
    {
        $request = request();
        $budget = VendorBudget::find($id);
        $budget_items = VendorBudgetItem::where('vendor_budget_id',$id);
        if($request->q)
        {
             $budget_items = $budget_items->where('category_name', 'LIKE', "%{$request->q}%");
        }
        if($request->order)
        {
            if($request->order == 'alpha')
            {
                $budget_items = $budget_items->orderBy('category_name','ASC');
            }
            else if($request->order == 'amount')
            {
                $budget_items = $budget_items->orderBy('budget','DESC');
            }
        }
        $categories = Category::where('type', 'Vendor')->orWhere('type', 'Event Planner')->orderBy('type', 'DESC')->get();
        $details['total_budget'] = $budget->event->final_budget;
        $details['actual_cost'] = $budget->items->sum('actual_cost');
        $details['actual_paid'] = $budget->items->sum('actual_paid');
        $details['total_paid'] = VendorBudgetItem::where('vendor_budget_id', $budget->id)->where('payment_status', 'Paid')->sum('actual_cost');

        return view('eventPlanner.budgeters.own_budget')->with([
            'categories' => $categories,
            'page_title' => 'My Budget',
            'event' => $budget->event,
            'budget' => $budget,
            'details' => $details,
            'request' => $request,
            'budget_items' => $budget_items->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor\VendorBudget  $vendorBudget
     * @return \Illuminate\Http\Response
     */
    public function edit(VendorBudget $vendorBudget)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor\VendorBudget  $vendorBudget
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VendorBudget $vendorBudget)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor\VendorBudget  $vendorBudget
     * @return \Illuminate\Http\Response
     */
    public function destroy($event_id, $id)
    {
        $vendorBudgeter = VendorBudgetItem::find($id);
        $vendorBudgeter->delete();
        return response()->json(array('status' => true, 'message' => 'Budget Item Removed.'), 200);
    }

    public function setAsDefault($id)
    {
        $budget = VendorBudget::find($id);
        VendorBudget::where('event_id',$budget->event_id)->update(['is_default' => 0]);
        $budget->is_default = 1;
        $budget->save();
    }
}
