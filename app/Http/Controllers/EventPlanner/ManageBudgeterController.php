<?php

namespace App\Http\Controllers\EventPlanner;

use App\Http\Controllers\Controller;
use App\Models\Admin\Master\Category;
use App\Models\Vendor\VendorBudgeter;
use App\Models\Vendor\VendorEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ManageBudgeterController extends Controller
{
    private $leads_counts = [];
    private $test;


    private $user;
    private $event_id;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->parent_id != null?Auth::user()->parent:Auth::user();
            $this->leads_counts = 1;
            $path = request()->path();
            $this->event_id = explode('/',$path)[2];
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $event = VendorEvent::find($this->event_id);
        $categories = Category::where('type','Vendor')->orWhere('type','Event Planner')->orderBy('type','DESC')->get();
        $budgets = VendorBudgeter::where('vendor_id',$this->user->id)->where('event_id',$this->event_id)->where('type',0)->get();
        if(count($budgets) == 0)
        {
           foreach ($categories as $key => $value) {
                  $percentage = $value->budget($event->occasion_id)?$value->budget($event->occasion_id)['budget_percentage']:0;

                  $new_row = new VendorBudgeter();
                  $new_row->vendor_id = $this->user->id;
                  $new_row->event_id = $this->event_id;
                  $new_row->category_id = $value->id;
                  $new_row->budget = ($event->final_budget / 100) * $percentage;
                  $new_row->payment_status = 'Pending';
                  $new_row->budget_status = 'On Budget';
                  $new_row->save();
           }
           $budgets = VendorBudgeter::where('vendor_id',$this->user->id)->where('event_id',$this->event_id)->where('type',0)->get();
        }
        $details['total_budget'] = $event->final_budget;
        $details['actual_cost'] = $budgets->sum('actual_cost');
        $details['total_paid'] = VendorBudgeter::where('vendor_id',$this->user->id)->where('event_id',$this->event_id)->where('payment_status','Paid')->where('type',0)->sum('actual_cost');
        return view('eventPlanner.budgeters.default_budget')->with([
            'categories' => $categories,
            'page_title' => 'Ask Deema Budget',
            'event' => $event,
            'rows' => $budgets,
            'details' => $details
        ]);
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ownBudgets()
    {
        $event = VendorEvent::find($this->event_id);
        $categories = Category::where('type','Vendor')->orWhere('type','Event Planner')->orderBy('type','DESC')->get();
        $budgets = VendorBudgeter::where('vendor_id',$this->user->id)->where('event_id',$this->event_id)->where('type',1)->get();

        $details['total_budget'] = $event->final_budget;
        $details['actual_cost'] = $budgets->sum('actual_cost');
        $details['total_paid'] = VendorBudgeter::where('vendor_id',$this->user->id)->where('event_id',$this->event_id)->where('payment_status','Paid')->where('type',1)->sum('actual_cost');
        return view('eventPlanner.budgeters.own_budget')->with([
            'categories' => $categories,
            'page_title' => 'My Own Budget',
            'event' => $event,
            'rows' => $budgets,
            'details' => $details,
            'request' => request()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $budget = VendorBudgeter::where('event_id',$this->event_id)->where('vendor_id',$this->user->id)->where('category_id',$request->item_name)->where('type',$request->type)->first();
        if($budget == null)
        {
            $budget = new VendorBudgeter();
        }
        $budget->event_id =$this->event_id;
        $budget->vendor_id = $this->user->id;
        $budget->category_id = $request->item_name;
        $budget->budget = $request->item_budget;
        $budget->actual_cost = $request->item_actual_cost;
        $budget->variance = $request->item_variance;
        $budget->payment_status = $request->item_payment_status;
        $budget->budget_status = $request->item_budget_status;
        $budget->remarks = $request->item_remarks;
        $budget->type = $request->type;
        $budget->save();

        $data['message'] = 'Budget Item Updated.';
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor\VendorBudgeter  $vendorBudgeter
     * @return \Illuminate\Http\Response
     */
    public function show(VendorBudgeter $vendorBudgeter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor\VendorBudgeter  $vendorBudgeter
     * @return \Illuminate\Http\Response
     */
    public function edit(VendorBudgeter $vendorBudgeter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor\VendorBudgeter  $vendorBudgeter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VendorBudgeter $vendorBudgeter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor\VendorBudgeter  $vendorBudgeter
     * @return \Illuminate\Http\Response
     */
    public function destroy($event_id,$id)
    {

        $vendorBudgeter = VendorBudgeter::find($id);
        $vendorBudgeter->delete();
        $data['message'] = 'Budget Item Removed.';
        return $data;
    }
}
