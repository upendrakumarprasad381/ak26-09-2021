<?php

namespace App\Http\Controllers\EventPlanner;

use App\Http\Controllers\Controller;
use App\Http\Requests\Vendor\VendorContractsRequest;
use App\Jobs\SendEmailJob;
use App\Models\Admin\Master\VendorTax;
use App\Models\User;
use App\Models\Vendor\VendorContract;
use App\Models\Vendor\VendorContractItems;
use App\Models\Vendor\VendorEvent;
use App\Models\Vendor\VendorProposal;
use App\Notifications\UserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Notification;
use PDF;

class ManageContractsController extends Controller
{
    private $counts = [];
    private $user;
    private $event_id;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->parent_id != null ? Auth::user()->parent : Auth::user();
            $this->leads_counts = 1;
            $path = request()->path();
            $this->event_id = explode('/', $path)[2];

            $this->counts = VendorContract::where([['party1_id', $this->user->id], ['event_id', $this->event_id]])
                ->orWhere([['party2_id', $this->user->id], ['event_id', $this->event_id]])
                ->select(
                    DB::raw("COALESCE(SUM(CASE WHEN status = 'Pending' THEN 1 ELSE 0 END),0) AS Pending"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 'Signed' THEN 1 ELSE 0 END),0) AS Signed"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 'Declined' THEN 1 ELSE 0 END),0) AS Declined"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 'On Hold' THEN 1 ELSE 0 END),0) AS OnHold"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 'Ammended' THEN 1 ELSE 0 END),0) AS Ammended")
                )->first();

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = request();

        $query = new VendorContract();
        for ($i = 1; $i <= 2; $i++) {
            $where = $i == 1 ? 'where' : 'orWhere';
            $query = $query->$where(function ($qr) use ($request, $i) {
                $qr->where('party' . $i . '_id', $this->user->id);
                $qr->where('event_id', $this->event_id);
                if ($request->status) {
                    if ($request->status == 'Declined/On Hold') {
                        $qr->where('status', $request->status);
                    } else {
                        $qr->where('status', $request->status);
                    }
                }
                if ($request->occasion) {
                    $qr->with('event')
                        ->whereHas('event', function ($q) use ($request) {
                            $q->where('vendor_events.occasion_id', $request->occasion);
                        });
                }
                if ($request->q) {
                    $qr->with('event')
                        ->whereHas('event', function ($q) use ($request) {
                            $q->where('vendor_events.event_name', 'LIKE', "%{$request->q}%");
                        });
                }
            });
        }

        return view('eventPlanner.contracts.index')->with([
            'page_title' => $request->status ? $request->status . ' Contracts' : 'Contracts',
            'event' =>  VendorEvent::find($this->event_id),
            'contracts' => $query->get(),
            'request' => $request,
            'counts' => $this->counts
        ]);
    }

    /**
     * Display a listing of the signed contracts.
     *
     * @return \Illuminate\Http\Response
     */
    public function signedContracts()
    {
        $contracts = VendorContract::where([['party1_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Signed']])
            ->orWhere([['party2_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Signed']])->get();

        return view('eventPlanner.contracts.index')->with([
            'page_title' => 'Signed Contracts',
            'event' =>  VendorEvent::find($this->event_id),
            'contracts' => $contracts,
            'counts' => $this->counts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($event_id, $proposal_id = null)
    {
        $create_from_proposal = 0;
        $proposals = VendorProposal::with('items.category')->where([['party1_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Accepted'], ['contract_status', 0]])
            ->orWhere([['party2_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Accepted'], ['contract_status', 0]])->get();

        if ($proposal_id) {
            $proposals = VendorProposal::with('items.category')->where('contract_status', 0)->where('id', $proposal_id)->get();
            $create_from_proposal = 1;
        }
        return view('eventPlanner.contracts.create')->with([
            'page_title' => 'Prepare Contract',
            'event' => VendorEvent::find($this->event_id),
            'proposals' => $proposals,
            'taxes' => VendorTax::all(),
            'create_from_proposal' => $create_from_proposal
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorContractsRequest $request)
    {
        $contract = new VendorContract();
        $contract->contract_name = $request->contract_name;
        $contract->event_id = $request->event_id;
        $contract->proposal_id = $request->proposal_id;
        // $contract->request_id = $request->request_id;
        $contract->party1_id = $request->party1_id;
        $contract->party2_id = $request->party2_id;
        $contract->date = date("Y-m-d", strtotime($request->date));
        $contract->approved_digital_written_signature = $request->approved_digital_written_signature ?: 0;
        $contract->type = $request->type ? 'Digital' : 'Written';

        $contract->signature_1 = $this->fileUpload($request, 'signature_1', 'event_planner/contracts/' . $request->event_id . '/sign');
        $contract->signature_1_type = $request->type ? 'Digital' : 'Written';
        $contract->signature_2 = $this->fileUpload($request, 'signature_2', 'event_planner/contracts/' . $request->event_id . '/sign');
        $contract->signature_2_type = $request->type ? 'Digital' : 'Written';
        if ($request->signature_1_esign != null) {
            $base64_str = substr($request->signature_1_esign, strpos($request->signature_1_esign, ",") + 1);
            $image = base64_decode($base64_str);
            $path = 'event_planner/contracts/' . $request->event_id . '/sign/' . $request->event_id . '-' . $contract->date . '.png';
            Storage::disk('public')->put($path, $image);
            $contract->signature_1 = $path;
        }
        if ($request->signature_2_esign != null) {
            $base64_str = substr($request->signature_2_esign, strpos($request->signature_2_esign, ",") + 1);
            $image = base64_decode($base64_str);
            $path = 'event_planner/contracts/' . $request->event_id . '/sign/' . $request->event_id . '-' . $contract->date . '.png';
            Storage::disk('public')->put($path, $image);
            $contract->signature_2 = $path;
        }
        $contract->terms_of_agreement = $request->terms_of_agreement;
        $contract->save();

        if ($request->items) {
            foreach (json_decode($request->items) as $row) {
                $item = new VendorContractItems();
                $item->contract_id = $contract->id;
                $item->item_name = $row->event_name;
                $item->service_type_id = $row->service_type;
                $item->description = $row->description;
                $item->amount = $row->amount;
                $item->tax = $row->tax;
                $item->quantity = $row->quantity;
                $item->attachment = $row->file;
                $item->remarks = $row->remarks;
                $item->sub_total = $row->amount * $row->quantity;
                $item->tax_total = ($item->sub_total / 100) * $row->tax;
                $item->total = $item->sub_total + $item->tax_total;
                $item->save();
            }
        }

        $proposal = VendorProposal::find($contract->proposal_id);
        $proposal->contract_status = 1;
        $proposal->save();


        $mailable_user = User::find($contract->party2_id);
        $details_['email'] = $mailable_user->email;
        $details_['subject'] = 'Ask Deema Contract';
        $details_['file'] = 'emails.email';
        $details_['name'] = $mailable_user->first_name . ' ' . $mailable_user->last_name;
        $details_['message_html'] = '
                    <p>You have Received the Contract for' . $contract->event['event_name'] . ' From ' . Auth::user()->first_name . ' ' . Auth::user()->last_name . '.Please login to review.</p>
                    <p>Thankyou</p>
                    <p>Ask Deema</p>';

        dispatch(new SendEmailJob($details_));

        $notification['text'] = 'You have Received the Contract for' . $contract->event['event_name'] . ' From ' . Auth::user()->first_name . ' ' . Auth::user()->last_name;
        $notification['link'] = null; //if no link given Null
        Notification::send(User::find($contract->party2_id), new UserNotification($notification));


        $data['message'] = 'Contract successfully created.';
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor\VendorContract  $vendorContract
     * @return \Illuminate\Http\Response
     */
    public function show(VendorContract $vendorContract)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor\VendorContract  $vendorContract
     * @return \Illuminate\Http\Response
     */
    public function edit($event_id, $id)
    {
        $contract = VendorContract::find($id);
        $proposals = VendorProposal::where('id', $this->event_id)->where('party2_id', $this->user->id)->where('contract_status', 0)->orWhere('id', $contract->proposal_id)->get();
        return view('eventPlanner.contracts.edit')->with([
            'page_title' => $contract->status == 'Signed' ? 'View Contract' : 'Edit Contract',
            'event' => VendorEvent::find($this->event_id),
            'proposals' => $proposals,
            'contract' => $contract,
            'editable' => $contract->party1_id == $this->user->id ? ($contract->status == 'Signed' ? 0 : 1) : 0,
            'taxes' => VendorTax::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor\VendorContract  $vendorContract
     * @return \Illuminate\Http\Response
     */
    public function update(VendorContractsRequest $request, $event_id, $id)
    {
        $contract = VendorContract::find($id);
        if ($request->editable == 1) {
            $contract->date = date("Y-m-d", strtotime($request->date));
            $contract->approved_digital_written_signature = $request->approved_digital_written_signature ?: 0;
            $contract->type = $request->type ? 'Digital' : 'Written';
            if ($request->has('signature_1')) {
                $this->fileDelete($contract->signature_1);
                $contract->signature_1_type = $request->type ? 'Digital' : 'Written';
                $contract->signature_1 = $this->fileUpload($request, 'signature_1', 'vendor/contracts/' . $request->event_id . '/sign');
            } else if (!$request->old_signature_1) {
                $this->fileDelete($contract->signature_1);
                $contract->signature_1 = null;
            }

            if ($request->has('signature_2')) {
                $this->fileDelete($contract->signature_2);
                $contract->signature_2_type = $request->type ? 'Digital' : 'Written';
                $contract->signature_2 = $this->fileUpload($request, 'signature_2', 'vendor/contracts/' . $request->event_id . '/sign');
            } else if (!$request->old_signature_2) {
                $this->fileDelete($contract->signature_2);
                $contract->signature_2 = null;
            }

            if ($request->signature_1_esign != null) {
                $base64_str = substr($request->signature_1_esign, strpos($request->signature_1_esign, ",") + 1);
                $image = base64_decode($base64_str);
                $path = 'vendor/contracts/' . $request->event_id . '/sign/' . $request->event_id . '-' . $contract->date . '.png';
                Storage::disk('public')->put($path, $image);
                $contract->signature_1 = $path;
                $contract->signature_1_type = $request->type ? 'Digital' : 'Written';
            }
            if ($request->signature_2_esign != null) {
                $base64_str = substr($request->signature_2_esign, strpos($request->signature_2_esign, ",") + 1);
                $image = base64_decode($base64_str);
                $path = 'vendor/contracts/' . $request->event_id . '/sign/' . $request->event_id . '-' . $contract->date . '.png';
                Storage::disk('public')->put($path, $image);
                $contract->signature_2 = $path;
                $contract->signature_2_type = $request->type ? 'Digital' : 'Written';
            }
            $contract->terms_of_agreement = $request->terms_of_agreement;
        } else {
            if ($request->has('signature_2')) {
                $this->fileDelete($contract->signature_2);
                $contract->signature_2 = $this->fileUpload($request, 'signature_2', 'vendor/contracts/' . $request->event_id . '/sign');
            } else if (!$request->old_signature_2) {
                $this->fileDelete($contract->signature_2);
                $contract->signature_2 = null;
                $contract->signature_2_type = $request->type ? 'Digital' : 'Written';
            }

            if ($request->signature_2_esign != null) {
                $base64_str = substr($request->signature_2_esign, strpos($request->signature_2_esign, ",") + 1);
                $image = base64_decode($base64_str);
                $path = 'vendor/contracts/' . $request->event_id . '/sign/' . $request->event_id . '-' . $contract->date . '.png';
                Storage::disk('public')->put($path, $image);
                $contract->signature_2 = $path;
                $contract->signature_2_type = $request->type ? 'Digital' : 'Written';
            }
            $contract->status = 'Signed';
        }

        $contract->save();
        if ($request->items && $request->editable == 1) {
            VendorContractItems::where('contract_id', $contract->id)->delete();
            foreach (json_decode($request->items) as $row) {
                $item = new VendorContractItems();
                $item->contract_id = $contract->id;
                $item->item_name = $row->event_name;
                $item->service_type_id = $row->service_type;
                $item->description = $row->description;
                $item->amount = $row->amount;
                $item->tax = $row->tax;
                $item->quantity = $row->quantity;
                $item->attachment = $row->file;
                $item->remarks = $row->remarks;

                $item->sub_total = $row->amount * $row->quantity;
                $item->tax_total = ($item->sub_total / 100) * $row->tax;
                $item->total = $item->sub_total + $item->tax_total;

                $item->save();
            }
        }
        $notification['text'] = 'A Contract Updated Against Your Proposal By ' . $contract->party1['first_name'] . '.';
        $notification['link'] = null; //if no link given Null
        Notification::send(User::find($contract->party2_id), new UserNotification($notification));

        $data['message'] = 'Contract successfully updated.';
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor\VendorContract  $vendorContract
     * @return \Illuminate\Http\Response
     */
    public function destroy(VendorContract $vendorContract)
    {
        //
    }

    public function ammendContract()
    {
        $contract = VendorContract::find(request()->id);
        $ammended = $contract->replicate();
        $ammended->status = 'Pending';
        $ammended->ammended_id = $contract->id;
        $ammended->save();

        foreach (VendorContractItems::where('contract_id', $contract->id)->get() as $key => $value) {
            $ammendedItem =  $value->replicate();
            $ammendedItem->contract_id = $ammended->id;
            $ammendedItem->save();
        }
        $contract->status = 'Ammended';
        $contract->save();

        return $ammended->id;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generatePDF()
    {
        $request = request();
        $data['event'] = VendorEvent::find($request->event_id);
        $data['party1'] = User::find($request->party1_id);
        $data['party2'] = User::find($request->party2_id);
        $data['items'] = json_decode($request->items);
        $data['terms'] = json_decode($request->terms_of_agreement);
        $data['request'] = $request;
        if ($request->contract) {
            $data['contract'] = VendorContract::find($request->contract);
        }
        // dd($data['items']);
        $pdf = PDF::loadView('eventPlanner.contracts.pdf', $data);
        $path = Storage::put('public/pdf/contract.pdf', $pdf->output());
        return $path;
    }
}
