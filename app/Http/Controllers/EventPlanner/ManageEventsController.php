<?php

namespace App\Http\Controllers\EventPlanner;

use App\Helpers\EventPermissionHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Vendor\VendorEventsRequest;
use App\Jobs\SendEmailJob;
use App\Models\Admin\Master\Category;
use App\Models\Admin\Master\Occasion;
use App\Models\Permission;
use App\Models\User;
use App\Models\Vendor\CustomerRequest;
use App\Models\Vendor\Emirate;
use App\Models\Vendor\EventPlannerRequest;
use App\Models\Vendor\NoOfAttendees;
use App\Models\Vendor\VendorContract;
use App\Models\Vendor\VendorEvent;
use App\Models\Vendor\VendorEventCategory;
use App\Models\Vendor\VendorInvoice;
use App\Models\Vendor\VendorProposal;
use App\Notifications\UserNotification;
use Dompdf\Dompdf;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Notification;
use URL;
use PDF;
use Illuminate\Support\Facades\Storage;
use iio\libmergepdf\Merger;
use iio\libmergepdf\Pages;

class ManageEventsController extends Controller
{
    private $event_counts = [];
    private $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            $this->user = Auth::user()->parent_id != null ? Auth::user()->parent : Auth::user();

            $this->event_counts = VendorEvent::where('vendor_id', $this->user->id)
                ->select(
                    DB::raw("COUNT(id) AS total"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 0 THEN 1 ELSE 0 END),0) AS active"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END),0) AS proposed"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 2 THEN 1 ELSE 0 END),0) AS archived")
                )->first();

            $event_by_event_planner = VendorEvent::with('eventPlannerRFQ')
                ->whereHas('eventPlannerRFQ', function ($q) {
                    $q->where('event_planner_request.vendor_id', $this->user->id)->where('suggest_to', null);;
                })->select(
                    DB::raw("COUNT(id) AS total"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 0 THEN 1 ELSE 0 END),0) AS active"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END),0) AS proposed"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 2 THEN 1 ELSE 0 END),0) AS archived")
                )->first();

            $event_by_customer = VendorEvent::with('customerRFQ')
                ->whereHas('customerRFQ', function ($q) {
                    $q->where('customer_request.vendor_id', $this->user->id)->where('suggest_to', null);;
                })->select(
                    DB::raw("COUNT(id) AS total"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 0 THEN 1 ELSE 0 END),0) AS active"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END),0) AS proposed"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 2 THEN 1 ELSE 0 END),0) AS archived")
                )->first();

            $event_by_event_planner_sugg = VendorEvent::with('eventPlannerRFQ')
                ->whereHas('eventPlannerRFQ', function ($q) {
                    $q->where('event_planner_request.suggest_to', $this->user->id);
                })->select(
                    DB::raw("COUNT(id) AS total"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 0 THEN 1 ELSE 0 END),0) AS active"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END),0) AS proposed"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 2 THEN 1 ELSE 0 END),0) AS archived")
                )->first();

            $event_by_customer_sugg = VendorEvent::with('customerRFQ')
                ->whereHas('customerRFQ', function ($q) {
                    $q->where('customer_request.suggest_to', $this->user->id);
                })->select(
                    DB::raw("COUNT(id) AS total"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 0 THEN 1 ELSE 0 END),0) AS active"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END),0) AS proposed"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 2 THEN 1 ELSE 0 END),0) AS archived")
                )->first();

            $event_by_own = VendorEvent::where('vendor_id', $this->user->id)->orderBy('id', 'DESC')
                ->select(
                    DB::raw("COUNT(id) AS total"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 0 THEN 1 ELSE 0 END),0) AS active"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END),0) AS proposed"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 2 THEN 1 ELSE 0 END),0) AS archived")
                )->first();

            $this->event_counts['active'] = $event_by_event_planner_sugg['active'] + $event_by_customer_sugg['active'] + $event_by_event_planner['active'] + $event_by_customer['active'] + $event_by_own['active'];
            $this->event_counts['proposed'] = $event_by_event_planner_sugg['proposed'] + $event_by_customer_sugg['proposed'] + $event_by_event_planner['proposed'] + $event_by_customer['proposed'] + $event_by_own['proposed'];
            $this->event_counts['archived'] = $event_by_event_planner_sugg['archived'] + $event_by_customer_sugg['archived'] + $event_by_event_planner['archived'] + $event_by_customer['archived'] + $event_by_own['archived'];

            $this->event_counts['total'] = $this->event_counts['active'] + $this->event_counts['proposed'] + $this->event_counts['archived'];

            return $next($request);
        });
    }

    /**
     * Show the vendor leads dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getEvents()
    {
        $request = request();

        // $query = new VendorEvent();

        // $query = $query->where(function ($qr) use ($request) {
        //     $qr->where('vendor_id', $this->user->id);
        //     if ($request->occasion) {
        //         $qr->where('occasion_id', $request->occasion);
        //     }
        //     if ($request->type) {
        //         $qr->where('type_of_event', $request->type);
        //     }
        //     if ($request->daterange) {
        //         $request->daterange = explode(',', $request->daterange);
        //         $old = $request->daterange[0] . ' / ' . $request->daterange[1];
        //         $qr->whereBetween('event_date', [date("Y-m-d", strtotime($request->daterange[0])), date("Y-m-d", strtotime($request->daterange[1]))]);
        //         $request->daterange = $old;
        //     }
        // });

        // dd($query->get());

        $event_by_event_planner = VendorEvent::with('eventPlannerRFQ')
            ->whereHas('eventPlannerRFQ', function ($q) {
                $q->where('event_planner_request.vendor_id', $this->user->id)->where('event_planner_request.status', 2)->where('suggest_to', null);
            })->where(function ($qr) use ($request) {
                if ($request->occasion) {
                    $qr->where('occasion_id', $request->occasion);
                }
                if ($request->type) {
                    $qr->where('type_of_event', $request->type);
                }
                if ($request->attendees) {
                    $range = explode(',', $request->attendees);
                    if ($range[1] == 0) {
                        $qr->where('number_of_attendees', '>=', $range[0]);
                    } else {
                        $qr->whereBetween('number_of_attendees', [$range[0], $range[1]]);
                    }
                }
                if ($request->daterange) {
                    $request->daterange = explode(',', $request->daterange);
                    $qr->whereBetween('event_date', [date("Y-m-d", strtotime($request->daterange[0])), date("Y-m-d", strtotime($request->daterange[1]))]);
                    // $request->daterange = $old;
                }
            })->get();

        $event_by_customer = VendorEvent::with('customerRFQ')
            ->whereHas('customerRFQ', function ($q) {
                $q->where('customer_request.vendor_id', $this->user->id)->where('customer_request.status', 2)->where('suggest_to', null);
            })->where(function ($qr) use ($request) {
                if ($request->occasion) {
                    $qr->where('occasion_id', $request->occasion);
                }
                if ($request->type) {
                    $qr->where('type_of_event', $request->type);
                }
                if ($request->attendees) {
                    $range = explode(',', $request->attendees);
                    if ($range[1] == 0) {
                        $qr->where('number_of_attendees', '>=', $range[0]);
                    } else {
                        $qr->whereBetween('number_of_attendees', [$range[0], $range[1]]);
                    }
                }
                if ($request->daterange) {
                    $qr->whereBetween('event_date', [date("Y-m-d", strtotime($request->daterange[0])), date("Y-m-d", strtotime($request->daterange[1]))]);
                }
            })->get();

        $event_by_event_planner_suggested = VendorEvent::with('eventPlannerRFQ')
            ->whereHas('eventPlannerRFQ', function ($q) {
                $q->where('event_planner_request.suggest_to', $this->user->id)->where('event_planner_request.status', 2);
            })->where(function ($qr) use ($request) {
                if ($request->occasion) {
                    $qr->where('occasion_id', $request->occasion);
                }
                if ($request->type) {
                    $qr->where('type_of_event', $request->type);
                }
                if ($request->attendees) {
                    $range = explode(',', $request->attendees);
                    if ($range[1] == 0) {
                        $qr->where('number_of_attendees', '>=', $range[0]);
                    } else {
                        $qr->whereBetween('number_of_attendees', [$range[0], $range[1]]);
                    }
                }
                if ($request->daterange) {
                    $request->daterange = explode(',', $request->daterange);
                    $qr->whereBetween('event_date', [date("Y-m-d", strtotime($request->daterange[0])), date("Y-m-d", strtotime($request->daterange[1]))]);
                    // $request->daterange = $old;
                }
            })->get();

        $event_by_customer_suggested = VendorEvent::with('customerRFQ')
            ->whereHas('customerRFQ', function ($q) {
                $q->where('customer_request.suggest_to', $this->user->id)->where('customer_request.status', 2);
            })->where(function ($qr) use ($request) {
                if ($request->occasion) {
                    $qr->where('occasion_id', $request->occasion);
                }
                if ($request->type) {
                    $qr->where('type_of_event', $request->type);
                }
                if ($request->attendees) {
                    $range = explode(',', $request->attendees);
                    if ($range[1] == 0) {
                        $qr->where('number_of_attendees', '>=', $range[0]);
                    } else {
                        $qr->whereBetween('number_of_attendees', [$range[0], $range[1]]);
                    }
                }
                if ($request->daterange) {
                    $qr->whereBetween('event_date', [date("Y-m-d", strtotime($request->daterange[0])), date("Y-m-d", strtotime($request->daterange[1]))]);
                }
            })->get();

        $event_by_own = VendorEvent::where('vendor_id', $this->user->id)->where(function ($qr) use ($request) {
            if ($request->occasion) {
                $qr->where('occasion_id', $request->occasion);
            }
            if ($request->type) {
                $qr->where('type_of_event', $request->type);
            }
            if ($request->attendees) {
                $range = explode(',', $request->attendees);
                if ($range[1] == 0) {
                    $qr->where('number_of_attendees', '>=', $range[0]);
                } else {
                    $qr->whereBetween('number_of_attendees', [$range[0], $range[1]]);
                }
            }
            if ($request->daterange) {
                $old = $request->daterange[0] . ' / ' . $request->daterange[1];
                $qr->whereBetween('event_date', [date("Y-m-d", strtotime($request->daterange[0])), date("Y-m-d", strtotime($request->daterange[1]))]);
                $request->daterange = $old;
            }
        })->orderBy('id', 'DESC')->get();



        $leads = $event_by_customer->merge($event_by_event_planner);
        $leads = $leads->merge($event_by_event_planner_suggested);
        $leads = $leads->merge($event_by_customer_suggested);
        $leads = $leads->merge($event_by_own);
        $leads = $leads->sortBy('created_at');
        return $leads;
    }
    public function index()
    {
        return view('eventPlanner.myevents.dashboard')->with([
            'page_title' => 'My Events',
            'event_counts' => $this->event_counts,
            'events' => $this->getEvents(),
            'occasions' => Occasion::all(),
            'request' => request()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('eventPlanner.myevents.create_new_event')->with([
            'page_title' => 'Create a Event',
            'occasions' => Occasion::all(),
            'customers' => User::whereRoleIs('user')->get(),
            'categories' => Category::where('type', 'Vendor')->where('status', 1)->where('parent_id', null)->get(),
            'permissions' => EventPermissionHelper::getEventpermissions(),
            'attendees' => NoOfAttendees::all(),
            'emirates' => Emirate::all()
        ]);
    }

    /**
     * Show the vendor leads show.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showEvent($id)
    {
        $event = VendorEvent::where('id', $id)->first();

        $permissions = EventPermissionHelper::getPermissions($event);
        $events = $this->getEvents();
        $chat_list = [];
        $vendor = $this->user->roles[0]->name == 'vendor' ? 'event_planner' : 'vendor';
        foreach ($event->eventPlannerRFQ as $key => $value) {
            if (!isset($chat_list[$value['id']])) {
                $chat_list[$value->$vendor['id']]['type'] = $value->category_id;
                $chat_list[$value->$vendor['id']]['name'] = $value->$vendor['first_name'] . ' ' . $value->$vendor['last_name'];
                $chat_list[$value->$vendor['id']]['image'] = $value->$vendor['profile_picture'];
                $chat_list[$value->$vendor['id']]['location'] = $value->$vendor->bussinessDetails['emirates'];
            }
        }
        return view('eventPlanner.myevents.event_dashboard')->with([
            'event' => $event,
            'page_title' => $event->event_name,
            'event_counts' => $this->event_counts,
            'permissions' => $permissions->toArray(),
            'events' => $events,
            'chat_list' => $chat_list
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorEventsRequest $request)
    {
        $vendor_event = new VendorEvent();
        $vendor_event->vendor_id = $this->user->id;
        $vendor_event->user_id = $request->user_id;
        $vendor_event->occasion_id = $request->occasion;
        $vendor_event->event_name = $request->event_name;
        $vendor_event->type_of_event = $request->type_of_event;
        if ($request->price_range) {
            $vendor_event->min_budget = json_decode($request->price_range)[0];
            $vendor_event->max_budget = json_decode($request->price_range)[0];
        }
        $vendor_event->final_budget = $request->final_budget ?: 0;
        $vendor_event->number_of_attendees = $request->number_of_attendees;
        $vendor_event->event_location = $request->event_location;
        $vendor_event->event_date = date("Y-m-d", strtotime($request->event_date));
        $vendor_event->planning_date = date("Y-m-d", strtotime($request->planning_date));
        $vendor_event->event_time = $request->event_time;
        $vendor_event->number_of_hours = 0;
        $vendor_event->quick_note = '';
        $vendor_event->type = 'Event Planner';
        $vendor_event->image = $this->fileUpload($request, 'image', 'event_planner/events/images');
        $vendor_event->document = $this->fileUpload($request, 'document', 'event_planner/events/documents');
        $vendor_event->save();

        if ($request->categories) {
            foreach ($request->categories as $key => $value) {
                DB::table('vendor_event_category')->insert([
                    'vendor_event_id' => $vendor_event->id,
                    'category_id' => $value
                ]);
            }
        }
        if ($request->event_permissions)
            EventPermissionHelper::attachPermissions($request->event_permissions, $vendor_event->id);

        \App\Helpers\CommonHelper::updateMycalender(null, 'EVENT', $vendor_event->id, null, $vendor_event->event_name, $vendor_event->event_date);
        $data['message'] = 'Event succesfully created.';

        if ($vendor_event->user_id) {
            $mailable_user = User::find($vendor_event->user_id);
            $details['email'] = $mailable_user->email;
            $details['subject'] = 'Ask Deema Event Registration';
            $details['file'] = 'emails.email';
            $details['name'] = $mailable_user->first_name . ' ' . $mailable_user->last_name;
            $details['message_html'] = '
            <p>A new event ' . $vendor_event->event_name . ' created by ' . $vendor_event->vendor['first_name'] . '</p>
            <p>Ask Deema</p>';
            dispatch(new SendEmailJob($details));

            $notification['text'] = 'A new event ' . $vendor_event->event_name . ' created by ' . $vendor_event->vendor['first_name'] . '';
            $notification['link'] = null; //if no link given Null
            Notification::send(User::find($mailable_user->id), new UserNotification($notification));
        }

        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor\VendorEvent  $vendorEvent
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendor_event = VendorEvent::find($id);
        $categories_ids = array();
        foreach ($vendor_event->categories as $key => $value) {
            array_push($categories_ids, $value->category_id);
        }
        return view('eventPlanner.myevents.edit')->with([
            'page_title' => 'Edit Event',
            'event' => $vendor_event,
            'occasions' => Occasion::all(),
            'customers' => User::whereRoleIs('user')->get(),
            'categories' => Category::where('type', 'Vendor')->where('status', 1)->where('parent_id', null)->get(),
            'categories_ids' => $categories_ids,
            'permissions' => EventPermissionHelper::getEventpermissions(),
            'attendees' => NoOfAttendees::all(),
            'emirates' => Emirate::all(),
            'event_permissions' => DB::table('permission_vendor_event')->join('permissions', 'permissions.id', 'permission_vendor_event.permission_id')->where('vendor_event_id', $id)->pluck('permissions.id')->toArray()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor\VendorEvent  $vendorEvent
     * @return \Illuminate\Http\Response
     */
    public function update(VendorEventsRequest $request, $id)
    {
        $vendorEvent = VendorEvent::find($id);
        $vendorEvent->user_id = $request->user_id;
        $vendorEvent->occasion_id = $request->occasion;
        $vendorEvent->event_name = $request->event_name;
        $vendorEvent->type_of_event = $request->type_of_event;
        if ($request->price_range) {
            $vendorEvent->min_budget = json_decode($request->price_range)[0];
            $vendorEvent->max_budget = json_decode($request->price_range)[0];
        }
        $vendorEvent->number_of_attendees = $request->number_of_attendees;
        $vendorEvent->event_location = $request->event_location;
        $vendorEvent->event_date = date("Y-m-d", strtotime($request->event_date));
        $vendorEvent->planning_date = date("Y-m-d", strtotime($request->planning_date));
        $vendorEvent->event_time = $request->event_time;
        $vendorEvent->number_of_hours = 0;
        $vendorEvent->quick_note = '';
        $vendorEvent->type = 'Event Planner';
        if ($request->has('document')) {
            $vendorEvent->document = $this->fileUpload($request, 'document', 'event_planner/events/documents');
        }
        if ($request->has('image')) {
            $this->fileDelete($vendorEvent->image);
            $vendorEvent->image = $this->fileUpload($request, 'image', 'event_planner/events/images');
        } else if (!$request->old_image) {
            $this->fileDelete($vendorEvent->image);
            $vendorEvent->image = null;
        }
        $vendorEvent->save();


        if ($request->categories) {
            DB::table('vendor_event_category')->where('vendor_event_id', $vendorEvent->id)->delete();
            foreach ($request->categories as $key => $value) {
                DB::table('vendor_event_category')->insert([
                    'vendor_event_id' => $vendorEvent->id,
                    'category_id' => $value
                ]);
            }
        }
        if ($request->event_permissions)
            EventPermissionHelper::attachPermissions($request->event_permissions, $vendorEvent->id);

        if ($vendorEvent->user_id) {
            $mailable_user = User::find($vendorEvent->user_id);
            $details['email'] = $mailable_user->email;
            $details['subject'] = 'Ask Deema Event';
            $details['file'] = 'emails.email';
            $details['name'] = $mailable_user->first_name . ' ' . $mailable_user->last_name;
            $details['message_html'] = '
            <p>Your event ' . $vendorEvent->event_name . ' updated by ' . $vendorEvent->vendor['first_name'] . '</p>
            <p>Ask Deema</p>';
            dispatch(new SendEmailJob($details));

            $notification['text'] = 'Your event ' . $vendorEvent->event_name . ' updated by ' . $vendorEvent->vendor['first_name'] . '';
            $notification['link'] = null; //if no link given Null
            Notification::send(User::find($mailable_user->id), new UserNotification($notification));
        }
        $data['message'] = 'Event succesfully updated.';
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor\VendorEvent  $vendorEvent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendorEvent = VendorEvent::find($id);
        if (count($vendorEvent->proposals) == 0) {
            $vendorEvent->delete();
            return response()->json(array('status' => true, 'message' => "Successfully deleted event."), 200);
        } else {
            return response()->json(array('status' => false, 'type' => 'warning', 'message' => "Can't delete this event."), 422);
        }
    }

    public function getPermissions($event)
    {
        if ($event->request_id || $this->user->hasRole('event_planner') || $this->user->hasRole('vendor')) {
            $permissions = Permission::where('module_id', 6)->where('name', 'like', 'view' . '%')->orderBy('id', 'ASC')->pluck('name');
        } else {
            $permissions = DB::table('permission_vendor_event')->join('permissions', 'permissions.id', 'permission_vendor_event.permission_id')->where('vendor_event_id', $event->id)->pluck('name');
        }

        return $permissions;
    }

    public function getEventpermissions()
    {
        return Permission::where('module_id', 6)->where('name', 'like', 'view' . '%')->orderBy('id', 'ASC')->get();
    }

    public function createDuplicate($event_id)
    {
        $event = VendorEvent::find($event_id);
        $duplicated = $event->replicate();
        $duplicated->event_name = $event->event_name . '- Copy';
        $duplicated->vendor_id = $this->user->id;
        $duplicated->save();

        foreach (VendorEventCategory::where('vendor_event_id', $event->id)->get() as $key => $value) {
            $duplicated_category =  $value->replicate();
            $duplicated_category->vendor_event_id = $duplicated->id;
            $duplicated_category->save();
        }

        \App\Helpers\CommonHelper::updateMycalender(null, 'EVENT', $duplicated->id, null, $duplicated->event_name, $duplicated->event_date);

        return response()->json(array('status' => true, 'message' => "Created new event.", 'event_id' => $duplicated->id), 200);
    }

    public function exportEvent($event_id)
    {
        $html = '';
        $event = VendorEvent::where('id', $event_id)->with('customer', 'occasion', 'categoriesModel')->first();
        $proposals = $this->eventQueryBuilder(new VendorProposal(), $event_id);
        $contracts = $this->eventQueryBuilder(new VendorContract(), $event_id);
        $invoices = $this->eventQueryBuilder(new VendorInvoice(), $event_id);
        $data['event'] = $event;
        $data['proposals'] = $proposals;
        $data['contracts'] = $contracts;
        $data['invoices'] = $invoices;
        $view = view('eventPlanner.myevents.export.event', $data);
        $html .= $view->render();
        $pdf = PDF::loadHTML($html);
        $sheet = $pdf->setPaper('a4', 'portrait');
        // Storage::delete('public/pdf/event-export-' . $this->user->id . '.pdf');
        $path = Storage::put('public/pdf/event-export-' . $this->user->id . '.pdf', $sheet->output());
        return 'event-export-' . $this->user->id . '.pdf';
    }

    private function eventQueryBuilder($query, $event_id)
    {
        for ($i = 1; $i <= 2; $i++) {
            $where = $i == 1 ? 'where' : 'orWhere';
            $query = $query->$where(function ($qr) use ($event_id, $i) {
                $qr->where('party' . $i . '_id', $this->user->id);
                $qr->where('event_id', $event_id);
            });
        }
        return $query->get();
    }
}
