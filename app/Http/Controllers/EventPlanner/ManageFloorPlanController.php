<?php

namespace App\Http\Controllers\EventPlanner;

use App\Http\Controllers\Controller;
use App\Models\Vendor\VendorFloorPlan;
use Illuminate\Http\Request;

class ManageFloorPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view('eventPlanner.floorPlans.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor\VendorFloorPlan  $vendorFloorPlan
     * @return \Illuminate\Http\Response
     */
    public function show(VendorFloorPlan $vendorFloorPlan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor\VendorFloorPlan  $vendorFloorPlan
     * @return \Illuminate\Http\Response
     */
    public function edit(VendorFloorPlan $vendorFloorPlan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor\VendorFloorPlan  $vendorFloorPlan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VendorFloorPlan $vendorFloorPlan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor\VendorFloorPlan  $vendorFloorPlan
     * @return \Illuminate\Http\Response
     */
    public function destroy(VendorFloorPlan $vendorFloorPlan)
    {
        //
    }
}
