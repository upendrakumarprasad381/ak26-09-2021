<?php

namespace App\Http\Controllers\EventPlanner;

use App\Http\Controllers\Controller;
use App\Http\Requests\Vendor\VendorInvoicesRequest;
use App\Models\Admin\Master\VendorTax;
use App\Models\User;
use App\Models\Vendor\VendorContract;
use App\Models\Vendor\VendorEvent;
use App\Models\Vendor\VendorInvoice;
use App\Models\Vendor\VendorInvoiceItem;
use App\Notifications\UserNotification;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Notification;

class ManageInvoiceController extends Controller
{
    private $counts = [];
    private $user;
    private $event_id;


    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->parent_id != null ? Auth::user()->parent : Auth::user();
            $query = new VendorInvoice();
            for ($i = 1; $i <= 2; $i++) {
                $where = $i == 1 ? 'where' : 'orWhere';
                $query = $query->$where(function ($qr) use ($request, $i) {
                    $qr->where('party' . $i . '_id', $this->user->id);
                    $qr->where('event_id', $this->event_id);
                });
            }
            $this->counts = $query->select(
                    DB::raw("COALESCE(SUM(CASE WHEN status = 'Pending' THEN 1 ELSE 0 END),0) AS Pending"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 'Accepted' THEN 1 ELSE 0 END),0) AS Accepted"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 'Refunded' THEN 1 ELSE 0 END),0) AS Refunded"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 'Rejected' THEN 1 ELSE 0 END),0) AS Rejected")
                )->first();
            $path = request()->path();
            $this->event_id = explode('/', $path)[2];
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $request = request();
        $query = new VendorInvoice();
        for ($i = 1; $i <= 2; $i++) {
            $where = $i == 1 ? 'where' : 'orWhere';
            $query = $query->$where(function ($qr) use ($request, $i) {
                $qr->where('party' . $i . '_id', $this->user->id);
                $qr->where('event_id', $this->event_id);
                if ($request->status) {
                    if ($request->status == 'Paid') {
                        $qr->where('payment_status', $request->status);
                    } else {
                        $qr->where('status', $request->status);
                    }
                }
                if ($request->occasion) {
                    $qr->with('event')
                        ->whereHas('event', function ($q) use ($request) {
                            $q->where('vendor_events.occasion_id', $request->occasion);
                        });
                }
                if ($request->q) {
                    $qr->with('event')
                        ->whereHas('event', function ($q) use ($request) {
                            $q->where('vendor_events.event_name', 'LIKE', "%{$request->q}%");
                        });
                }
            });
        }

        return view('eventPlanner.invoices.index')->with([
            'page_title' => $request->status ? $request->status . ' Invoices' : 'Invoices',
            'event' =>  VendorEvent::find($this->event_id),
            'invoices' => $query->get(),
            'request' => $request,
            'counts' => $this->counts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($event_id, $proposal_id = null)
    {
        $create_from_contract = 0;
        $contracts = VendorContract::with('items.category')->where([['party1_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Signed'], ['invoice_status', 0]])
            ->orWhere([['party2_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Signed'], ['invoice_status', 0]])->get();
        if ($proposal_id) {
            $contracts = VendorContract::with('items.category')->where('invoice_status', 0)->where('id', $proposal_id)->get();
            $create_from_contract = 1;
        }
        $last_sn = VendorInvoice::latest()->pluck('invoice_sn')->first();
        $next_sn = $last_sn ? (int)str_replace('ASK', '', $last_sn) : 1000;
        return view('eventPlanner.invoices.create')->with([
            'page_title' => 'Add Invoice',
            'event' => VendorEvent::find($this->event_id),
            'contracts' => $contracts,
            'taxes' => VendorTax::all(),
            'next_sn' => $next_sn + 1,
            'create_from_contract' => $create_from_contract
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorInvoicesRequest $request)
    {
        $invoice = new VendorInvoice();
        $invoice->event_id = $request->event_id;
        $invoice->invoice_sn = $request->invoice_sn;
        $invoice->contract_id = $request->contract_id;
        $invoice->party1_id = $this->user->id;
        $invoice->status = $request->status;
        $invoice->party2_id = $request->party2_id;
        $invoice->date = date("Y-m-d", strtotime($request->date));
        $invoice->due_date = date("Y-m-d", strtotime($request->due_date));
        $invoice->approved_digital_written_signature = $request->approved_digital_written_signature ?: 0;
        $invoice->type = $request->type ? 'Digital' : 'Written';
        $invoice->send_notification_date = date("Y-m-d", strtotime($request->send_notification_date));
        $invoice->reminder_on_date = date("Y-m-d", strtotime($request->reminder_on_date));
        $invoice->weekly_reminder = $request->weekly_reminder ?: 0;
        $invoice->daily_reminder = $request->daily_reminder ?: 0;

        $invoice->signature_1 = $this->fileUpload($request, 'signature_1', 'event_planner/invoices/' . $request->event_id . '/sign');
        // $invoice->signature_1 = $this->fileUpload($request,'signature_1','event_planner/invoices/'.$request->event_id.'/sign');
        if ($request->signature_1_esign != null) {
            $base64_str = substr($request->signature_1_esign, strpos($request->signature_1_esign, ",") + 1);
            $image = base64_decode($base64_str);
            $path = 'event_planner/invoices/' . $request->event_id . '/sign/' . $request->event_id . '-' . $invoice->date . '.png';
            Storage::disk('public')->put($path, $image);
            $invoice->signature_1 = $path;
        }

        $invoice->terms_of_agreement = $request->terms_of_agreement;
        $invoice->total_amount = $request->payment_total;
        $invoice->total_payable = $request->payment_amount;
        $invoice->total_paid = $request->payment_paid;
        $invoice->save();

        if ($request->items) {
            foreach (json_decode($request->items) as $row) {
                $item = new VendorInvoiceItem();
                $item->invoice_id = $invoice->id;
                $item->item_name = $row->event_name;
                $item->service_type_id = $row->service_type;
                $item->description = $row->description;
                $item->amount = $row->amount;
                $item->tax = $row->tax;
                $item->quantity = $row->quantity;
                $item->attachment = $row->file;
                $item->remarks = $row->remarks;
                $item->sub_total = $row->amount * $row->quantity;
                $item->tax_total = ($item->sub_total / 100) * $row->tax;
                $item->total = $item->sub_total + $item->tax_total;
                $item->save();
            }
        }

        if($request->payment_balance == 0)
        {
            $contract = VendorContract::find($invoice->contract_id);
            $contract->invoice_status = 2; //FUll Paid
            $contract->save();
        }
        else
        {
            $contract = VendorContract::find($invoice->contract_id);
            $contract->invoice_status = 1; //partially paid
            $contract->save();
        }

        $notification['text'] = 'New Invoice Created By '.$invoice->party1['first_name'].'.';
        $notification['link'] = null; //if no link given Null
        Notification::send(User::find($invoice->party2_id), new UserNotification($notification));
        $data['message'] = 'Invoice Send.';
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor\VendorInvoice  $vendorInvoice
     * @return \Illuminate\Http\Response
     */
    public function show(VendorInvoice $vendorInvoice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor\VendorInvoice  $vendorInvoice
     * @return \Illuminate\Http\Response
     */
    public function edit($event_id, $id)
    {
        $invoice = VendorInvoice::find($id);
        $contracts = VendorContract::where('id', $this->event_id)->where('party2_id', $this->user->id)->where('invoice_status', 0)->orWhere('id', $invoice->contract_id)->get();

        return view('eventPlanner.invoices.edit')->with([
            'page_title' => $invoice->status == 'Paid' ? 'View Invoice' : 'Edit Invoice',
            'event' => VendorEvent::find($this->event_id),
            'contracts' => $contracts,
            'invoice' => $invoice,
            'editable' => $invoice->party1_id == $this->user->id ? 1 : 0,
            'taxes' => VendorTax::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor\VendorInvoice  $vendorInvoice
     * @return \Illuminate\Http\Response
     */
    public function update(VendorInvoicesRequest $request,  $event_id, $id)
    {
        $invoice = VendorInvoice::find($id);
        if ($request->editable == 1) {
            $invoice->invoice_sn = $request->invoice_sn;
            $invoice->contract_id = $request->contract_id;
            $invoice->party1_id = $this->user->id;
            $invoice->party2_id = $request->party2_id;
            $invoice->status = $request->status;
            $invoice->date = date("Y-m-d", strtotime($request->date));
            $invoice->due_date = date("Y-m-d", strtotime($request->due_date));
            $invoice->approved_digital_written_signature = $request->approved_digital_written_signature ?: 0;
            $invoice->type = $request->type ? 'Digital' : 'Written';
            $invoice->send_notification_date = date("Y-m-d", strtotime($request->send_notification_date));
            $invoice->reminder_on_date = date("Y-m-d", strtotime($request->reminder_on_date));
            $invoice->weekly_reminder = $request->weekly_reminder ?: 0;
            $invoice->daily_reminder = $request->daily_reminder ?: 0;

            $invoice->total_amount = $request->payment_total;
            $invoice->total_payable = $request->payment_amount;
            $invoice->total_paid = $request->payment_paid;

            if ($request->has('signature_1')) {
                $this->fileDelete($invoice->signature_1);
                $invoice->signature_1 = $this->fileUpload($request, 'signature_1', 'event_planner/invoices/' . $request->event_id . '/sign');
            } else if (!$request->old_signature_1) {
                $this->fileDelete($invoice->signature_1);
                $invoice->signature_1 = null;
            }

            if ($request->signature_1_esign != null) {
                $base64_str = substr($request->signature_1_esign, strpos($request->signature_1_esign, ",") + 1);
                $image = base64_decode($base64_str);
                $path = 'event_planner/invoices/' . $request->event_id . '/sign/' . $request->event_id . '-' . $invoice->date . '.png';
                Storage::disk('public')->put($path, $image);
                $invoice->signature_1 = $path;
            }
            $invoice->terms_of_agreement = $request->terms_of_agreement;
        } else {
            if ($request->has('signature_1')) {
                $this->fileDelete($invoice->signature_1);
                $invoice->signature_1 = $this->fileUpload($request, 'signature_1', 'event_planner/invoices/' . $request->event_id . '/sign');
            } else if (!$request->old_signature_1) {
                $this->fileDelete($invoice->signature_1);
                $invoice->signature_1 = null;
            }

            if ($request->signature_1_esign != null) {
                $base64_str = substr($request->signature_1_esign, strpos($request->signature_1_esign, ",") + 1);
                $image = base64_decode($base64_str);
                $path = 'event_planner/invoices/' . $request->event_id . '/sign/' . $request->event_id . '-' . $invoice->date . '.png';
                Storage::disk('public')->put($path, $image);
                $invoice->signature_1 = $path;
            }
            $invoice->status = 'Paid';
        }

        $invoice->save();

        if ($request->items && $request->editable == 1) {
            VendorInvoiceItem::where('invoice_id', $invoice->id)->delete();
            foreach (json_decode($request->items) as $row) {
                $item = new VendorInvoiceItem();
                $item->invoice_id = $invoice->id;
                $item->item_name = $row->event_name;
                $item->service_type_id = $row->service_type;
                $item->description = $row->description;
                $item->amount = $row->amount;
                $item->tax = $row->tax;
                $item->quantity = $row->quantity;
                $item->attachment = $row->file;
                $item->remarks = $row->remarks;

                $item->sub_total = $row->amount * $row->quantity;
                $item->tax_total = ($item->sub_total / 100) * $row->tax;
                $item->total = $item->sub_total + $item->tax_total;

                $item->save();
            }
        }

        if($request->payment_balance == 0)
        {
            $contract = VendorContract::find($invoice->contract_id);
            $contract->invoice_status = 2; //FUll Paid
            $contract->save();
        }
        else
        {
            $contract = VendorContract::find($invoice->contract_id);
            $contract->invoice_status = 1; //partially paid
            $contract->save();
        }

        $data['message'] = 'Invoice successfully updated.';
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor\VendorInvoice  $vendorInvoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(VendorInvoice $vendorInvoice)
    {
        //
    }
}
