<?php

namespace App\Http\Controllers\EventPlanner;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmailJob;
use App\Models\User;
use App\Models\Vendor\CustomerRequest;
use App\Models\Vendor\EventPlannerRequest;
use App\Models\Vendor\VendorEvent;
use App\Notifications\UserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Notification;

class ManageLeadsController extends Controller
{
    private $leads_counts = [];
    private $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->parent_id != null?Auth::user()->parent:Auth::user();

            $customer = CustomerRequest::where('vendor_id',$this->user->id)
            ->select(DB::raw("COALESCE(SUM(CASE WHEN status = 0 THEN 1 ELSE 0 END),0) AS received"),
            DB::raw("COALESCE(SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END),0) AS opened"),
            DB::raw("COALESCE(SUM(CASE WHEN status = 2 THEN 1 ELSE 0 END),0) AS ongoing"),
            DB::raw("COALESCE(SUM(CASE WHEN status = 3 THEN 1 ELSE 0 END),0) AS declined"))->first();

            $vendor = EventPlannerRequest::where('vendor_id',$this->user->id)
            ->select(DB::raw("COALESCE(SUM(CASE WHEN status = 0 THEN 1 ELSE 0 END),0) AS received"),
            DB::raw("COALESCE(SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END),0) AS opened"),
            DB::raw("COALESCE(SUM(CASE WHEN status = 2 THEN 1 ELSE 0 END),0) AS ongoing"),
            DB::raw("COALESCE(SUM(CASE WHEN status = 3 THEN 1 ELSE 0 END),0) AS declined"))->first();

            $this->leads_counts['received'] = $customer['received'] + $vendor['received'];
            $this->leads_counts['opened'] = $customer['opened'] + $vendor['opened'];
            $this->leads_counts['ongoing'] = $customer['ongoing'] + $vendor['ongoing'];
            $this->leads_counts['declined'] = $customer['declined'] + $vendor['declined'];
            return $next($request);
        });
    }

    /**
     * Show the vendor leads dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('eventPlanner.leads.dashboard')->with([
            'page_title' => 'Leads',
            'leads_counts' => $this->leads_counts,
        ]);
    }

    /**
     * Show the vendor leads services.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function recievedLeads()
    {
        $recieved_leads = CustomerRequest::where('vendor_id',$this->user->id)->where('status',0)->get();
        return view('eventPlanner.leads.received_leads')->with([
            'recieved_leads' => $recieved_leads,
            'page_title' => 'Received Leads',
            'leads_counts' => $this->leads_counts,
        ]);
    }

    /**
     * Show the vendor leads show.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showRcievedLead($id)
    {
        $recieved_lead = CustomerRequest::where('id',$id)->first();

        return view('eventPlanner.leads.received_lead_details')->with([
            'recieved_lead' => $recieved_lead,
            'page_title' => 'Received Leads',
            'leads_counts' => $this->leads_counts,
        ]);
    }

    /**
     * Show the vendor opened services.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function ongoingLeads()
    {
        $recieved_leads = CustomerRequest::where('vendor_id',$this->user->id)->where('status',2)->get();
        return view('eventPlanner.leads.received_leads')->with([
            'recieved_leads' => $recieved_leads,
            'page_title' => 'Ongoing Leads',
            'leads_counts' => $this->leads_counts,
        ]);
    }
     /**
     * Show the vendor opened services.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function declinedLeads()
    {
        $event_planner_leads = EventPlannerRequest::where('vendor_id',$this->user->id)->where('status',3)->get();
        $recieved_leads = CustomerRequest::where('vendor_id',$this->user->id)->where('status',3)->get();
        $leads = $event_planner_leads->merge($recieved_leads);
        $leads = $leads->sortBy('created_at');
        return view('eventPlanner.leads.received_leads')->with([
            'recieved_leads' => $leads,
            'page_title' => 'Declined Leads',
            'leads_counts' => $this->leads_counts,
        ]);
    }

    public function acceptLead()
    {
        $request = request();
        $recieved_lead = CustomerRequest::find($request->id);
        if($recieved_lead->status == 2)
        {
            return response()->json(array('status' =>true,'type' => 'warning','message' => 'Lead Already Accepted'),200);
        }
        $recieved_lead->status = 2;
        $recieved_lead->save();

        $mailable_user = User::find($recieved_lead->user_id);
        $details_['email'] = $mailable_user->email;
        $details_['subject'] = 'Ask Deema Lead Accepted';
        $details_['file'] = 'emails.email';
        $details_['name'] = $mailable_user->first_name . ' ' . $mailable_user->last_name;
        $details_['message_html'] = '
                <p>Your lead accepted by ' . Auth::user()->first_name . ' ' . Auth::user()->last_name . '.  Please login to view details.</p>
                <p>Thankyou</p>
                <p>Ask Deema</p>';

        dispatch(new SendEmailJob($details_));

        $notification['text'] = 'Your lead accepted by ' . Auth::user()->first_name . ' ' . Auth::user()->last_name . '';
        $notification['link'] = null; //if no link given Null

        Notification::send(User::find($mailable_user->id), new UserNotification($notification));

        $message['status'] = true;
        $message['message'] = 'Lead Accepted.';

        return $message;
    }

    public function declineLead()
    {
        $request = request();
        $recieved_lead = CustomerRequest::find($request->id);
        if($recieved_lead->status == 3)
        {
            return response()->json(array('status' =>true,'type' => 'warning','message' => 'Lead Already Declined'),200);
        }
        $recieved_lead->status = 3;
        $recieved_lead->save();

        $mailable_user = User::find($recieved_lead->user_id);
        $details_['email'] = $mailable_user->email;
        $details_['subject'] = 'Ask Deema Lead Rejected';
        $details_['file'] = 'emails.email';
        $details_['name'] = $mailable_user->first_name . ' ' . $mailable_user->last_name;
        $details_['message_html'] = '
                <p>Your lead rejected by ' . Auth::user()->first_name . ' ' . Auth::user()->last_name . '.  Please login to view details.</p>
                <p>Thankyou</p>
                <p>Ask Deema</p>';

        dispatch(new SendEmailJob($details_));

        $notification['text'] = 'Your lead rejected by ' . Auth::user()->first_name . ' ' . Auth::user()->last_name . '';
        $notification['link'] = null; //if no link given Null

        Notification::send(User::find($mailable_user->id), new UserNotification($notification));

        $message['status'] = true;
        $message['message'] = 'Lead Declined.';

        return $message;
    }

}
