<?php

namespace App\Http\Controllers\EventPlanner;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmailJob;
use App\Models\Admin\Master\VendorTax;
use App\Models\ProposalTermsNegotiaion;
use App\Models\User;
use App\Models\Vendor\EventPlannerRequest;
use App\Models\Vendor\VendorEvent;
use App\Models\Vendor\VendorProposal;
use App\Models\Vendor\VendorProposalItem;
use App\Models\Vendor\VendorProposalItemNegotiation;
use App\Notifications\UserNotification;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Notification;

class ManageProposalsController extends Controller
{
    private $proposal_counts = [];
    private $user;
    private $event_id;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->parent_id != null ? Auth::user()->parent : Auth::user();
            $path = request()->path();
            $this->event_id = explode('/', $path)[2];
            $query = new VendorProposal();
            for ($i = 1; $i <= 2; $i++) {
                $where = $i == 1 ? 'where' : 'orWhere';
                $query = $query->$where(function ($qr) use ($i) {
                    $qr->where('party' . $i . '_id', $this->user->id);
                    $qr->where('event_id', $this->event_id);
                });
            }

            $this->proposal_counts = $query->select(
                DB::raw("COALESCE(SUM(CASE WHEN status = 'Send/Recieved' THEN 1 ELSE 0 END),0) AS send"),
                DB::raw("COALESCE(SUM(CASE WHEN status = 'Accepted' THEN 1 ELSE 0 END),0) AS accepted"),
                DB::raw("COALESCE(SUM(CASE WHEN status = 'Pending' THEN 1 ELSE 0 END),0) AS pending"),
                DB::raw("COALESCE(SUM(CASE WHEN status = 'Declined' THEN 1 ELSE 0 END),0) AS declined"),
                DB::raw("COALESCE(SUM(CASE WHEN status = 'On Hold' THEN 1 ELSE 0 END),0) AS hold"),
                DB::raw("COALESCE(SUM(CASE WHEN status = 'Draft' AND party1_id = " . $this->user->id . " THEN 1 ELSE 0 END),0) AS draft"),
            )->first();
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $request = request();
        $query = new VendorProposal();
        for ($i = 1; $i <= 2; $i++) {
            $where = $i == 1 ? 'where' : 'orWhere';
            $query = $query->$where(function ($qr) use ($request, $i) {
                $qr->where('party' . $i . '_id', $this->user->id);
                $qr->where('event_id', $this->event_id);
                if ($request->status) {
                    if ($request->status == 'Declined/On Hold') {
                        $qr->where('status', $request->status);
                    } else {
                        $qr->where('status', $request->status);
                    }
                }
                if ($request->occasion) {
                    $qr->with('event')
                        ->whereHas('event', function ($q) use ($request) {
                            $q->where('vendor_events.occasion_id', $request->occasion);
                        });
                }
                if ($request->q) {
                    $qr->with('event')
                        ->whereHas('event', function ($q) use ($request) {
                            $q->where('vendor_events.event_name', 'LIKE', "%{$request->q}%");
                        });
                }
                if ($i == 2) {
                    $qr->where('status', '!=', 'Draft');
                }
            });
        }

        return view('eventPlanner.proposals.index')->with([
            'page_title' => $request->status ? $request->status . ' Proposals' : 'Proposals',
            'event' =>  VendorEvent::find($this->event_id),
            'proposals' => $query->get(),
            'request' => $request,
            'count' => $this->proposal_counts
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function acceptedProposals()
    {
        $proposals = VendorProposal::where([['party1_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Accepted']])
            ->orWhere([['party2_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Accepted']])->get();

        return view('eventPlanner.proposals.index')->with([
            'page_title' => 'Accepted Proposals',
            'event' =>  VendorEvent::find($this->event_id),
            'proposals' => $proposals,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function pendingProposals()
    {
        $proposals = VendorProposal::where([['party1_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Pending']])
            ->orWhere([['party2_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Pending']])->get();

        return view('eventPlanner.proposals.index')->with([
            'page_title' => 'Pending Proposals',
            'event' =>  VendorEvent::find($this->event_id),
            'proposals' => $proposals,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $requests = EventPlannerRequest::where('event_id', $this->event_id)->where('vendor_id', $this->user->id)->where('proposal_status', 0)->get();

        return view('eventPlanner.proposals.create')->with([
            'page_title' => 'Add Proposal',
            'event' => VendorEvent::find($this->event_id),
            'requests' => $requests,
            'taxes' => VendorTax::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $proposal = new VendorProposal();
        $proposal->event_id = $request->event_id;
        $proposal->request_id = $request->request_id;
        $proposal->party1_id = $request->party1_id;
        $proposal->party2_id = $request->party2_id;
        $proposal->date = date("Y-m-d", strtotime($request->date));
        $proposal->approved_digital_written_signature = $request->approved_digital_written_signature ?: 0;
        $proposal->type = $request->type ? 'Digital' : 'Written';

        $proposal->signature_1 = $this->fileUpload($request, 'signature_1', 'event_planner/proposals/' . $request->party1_id . '/sign');
        $proposal->signature_2 = $this->fileUpload($request, 'signature_2', 'event_planner/proposals/' . $request->party2_id . '/sign');
        if ($request->signature_1_esign != null) {
            $base64_str = substr($request->signature_1_esign, strpos($request->signature_1_esign, ",") + 1);
            $image = base64_decode($base64_str);
            $path = 'event_planner/proposals/' . $request->party1_id . '/sign/' . $request->event_id . '-' . $proposal->date . '.png';
            Storage::disk('public')->put($path, $image);
            $proposal->signature_1 = $path;
        }
        if ($request->signature_2_esign != null) {
            $base64_str = substr($request->signature_2_esign, strpos($request->signature_2_esign, ",") + 1);
            $image = base64_decode($base64_str);
            $path = 'event_planner/proposals/' . $request->party2_id . '/sign/' . $request->event_id . '-' . $proposal->date . '.png';
            Storage::disk('public')->put($path, $image);
            $proposal->signature_2 = $path;
        }
        $proposal->terms_of_agreement = $request->terms_of_agreement;
        $proposal->save();
        if ($request->items) {
            foreach (json_decode($request->items) as $row) {
                $item = new VendorProposalItem();
                $item->proposal_id = $proposal->id;
                $item->item_name = $row->event_name;
                $item->service_type_id = $row->service_type;
                $item->description = $row->description;
                $item->amount = $row->amount;
                $item->tax = $row->tax;
                $item->quantity = $row->quantity;
                $item->attachment = $row->file;
                $item->remarks = $row->remarks;
                $item->sub_total = $row->amount * $row->quantity;
                $item->tax_total = ($item->sub_total / 100) * $row->tax;
                $item->total = $item->sub_total + $item->tax_total;
                $item->save();
            }
        }

        $event_planner_req = EventPlannerRequest::find($proposal->request_id);
        $event_planner_req->status = 2;
        $event_planner_req->proposal_status = 1;
        $event_planner_req->save();

        $data['message'] = 'Proposal Send.';
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($event_id, $id)
    {
        $proposal = VendorProposal::find($id);
        $requests = EventPlannerRequest::Where('id', $proposal->request_id)->get();
        return view('eventPlanner.proposals.view')->with([
            'page_title' => 'View Proposal',
            'event' => VendorEvent::find($this->event_id),
            'requests' => $requests,
            'proposal' => $proposal,
            'editable' => $proposal->party1_id == $this->user->id ? 1 : 0,
            'taxes' => VendorTax::all()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($event_id, $id)
    {
        $proposal = VendorProposal::find($id);
        $items = [];
        foreach ($proposal->items as $key => $value) {
            $items[$key]['id'] = $value->id;
            $items[$key]['item_name'] = $value->item_name;
            $items[$key]['service_type'] = $value->category['name'];
            $items[$key]['service_type_id'] = $value->service_type_id;
            $items[$key]['sub_service'] = $value->subCategory?$value->subCategory['name']:null;
            $items[$key]['sub_service_id'] = $value->sub_service_id;
            $items[$key]['description'] = $value->description;
            $items[$key]['amount'] = $value->amount;
            $items[$key]['tax'] = $value->tax;
            $items[$key]['qty'] = $value->quantity;
            $items[$key]['sub_total'] = $value->sub_total;
            $items[$key]['tax_amount'] = $value->tax_total;
            $items[$key]['total'] = $value->total;
            $items[$key]['image_url'] = $value->attachment;
            $items[$key]['remark'] = $value->remarks;
            if (count($value->negotiations) > 0) {
                $items[$key]['negotiation']['amount'] =  $value->negotiations[0]['amount'];
                $items[$key]['negotiation']['sub_total'] =  $value->negotiations[0]['amount'] * $value->quantity;
                $items[$key]['negotiation']['tax_total'] =  $items[$key]['negotiation']['sub_total'] / 100 * $value->tax;
                $items[$key]['negotiation']['total'] =  $items[$key]['negotiation']['tax_total'] + $items[$key]['negotiation']['sub_total'];
                $items[$key]['negotiation']['note'] =  $value->negotiations[0]['note'];
                $items[$key]['negotiation']['status'] =  $value->negotiations[0]['status'];
            }
        }
        $requests = EventPlannerRequest::where('id', $this->event_id)->where('vendor_id', $this->user->id)->where('status', 0)->orWhere('id', $proposal->request_id)->get();
        if ($proposal->party1_id != $this->user->id) {
            $requests = $requests = EventPlannerRequest::Where('id', $proposal->request_id)->get();
            $page_title = 'View Proposal';
        }
        $negotiation = request()->negotiation?:0;
        return view('eventPlanner.proposals.edit')->with([
            'page_title' => $page_title ?: 'Edit Proposal',
            'event' => VendorEvent::find($this->event_id),
            'requests' => $requests,
            'proposal' => $proposal,
            'editable' => $proposal->party1_id == $this->user->id ? 1 : 0,
            'taxes' => VendorTax::all(),
            'items' => $items,
            'negotiation' => $negotiation
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $event_id, $id)
    {
        $proposal = VendorProposal::find($id);
        if ($request->editable == 1) {
            $proposal->date = date("Y-m-d", strtotime($request->date));
            $proposal->approved_digital_written_signature = $request->approved_digital_written_signature ?: 0;
            $proposal->type = $request->type ? 'Digital' : 'Written';
            if ($request->has('signature_1')) {
                $this->fileDelete($proposal->signature_1);
                $proposal->signature_1 = $this->fileUpload($request, 'signature_1', 'vendor/proposals/' . $request->party1_id . '/sign');
            } else if (!$request->old_signature_1) {
                $this->fileDelete($proposal->signature_1);
                $proposal->signature_1 = null;
            }

            if ($request->has('signature_2')) {
                $this->fileDelete($proposal->signature_2);
                $proposal->signature_2 = $this->fileUpload($request, 'signature_2', 'vendor/proposals/' . $request->party2_id . '/sign');
            } else if (!$request->old_signature_2) {
                $this->fileDelete($proposal->signature_2);
                $proposal->signature_2 = null;
            }

            if ($request->signature_1_esign != null) {
                $base64_str = substr($request->signature_1_esign, strpos($request->signature_1_esign, ",") + 1);
                $image = base64_decode($base64_str);
                $path = 'vendor/proposals/' . $request->party1_id . '/sign/' . $request->event_id . '-' . $proposal->date . '.png';
                Storage::disk('public')->put($path, $image);
                $proposal->signature_1 = $path;
            }
            if ($request->signature_2_esign != null) {
                $base64_str = substr($request->signature_2_esign, strpos($request->signature_2_esign, ",") + 1);
                $image = base64_decode($base64_str);
                $path = 'vendor/proposals/' . $request->party2_id . '/sign/' . $request->event_id . '-' . $proposal->date . '.png';
                Storage::disk('public')->put($path, $image);
                $proposal->signature_2 = $path;
            }
            $proposal->terms_of_agreement = $request->terms_of_agreement;
        } else {
            if ($request->negotiation == 1) {
                if (count(json_decode($request->items)) != 0) {
                    foreach (json_decode($request->items) as $row) {
                        if (isset($row->negotiation)) {
                            $item = new VendorProposalItemNegotiation();
                            $item->item_id = $row->id;
                            $item->amount = $row->negotiation->amount;
                            $item->remarks = $row->negotiation->note;
                            $item->status = 1;
                            $item->save();
                        }
                    }

                    $proposal->status = 'Pending';
                    $notification['text'] = 'Your Proposal Has a Negotiation By ' . $proposal->party2['first_name'] . '.';
                    $notification['link'] = null; //if no link given Null
                    Notification::send(User::find($proposal->party1_id), new UserNotification($notification));
                    $proposal->negotiation_status = 1;
                    $proposal->save();
                } else {
                    $item_ids = $proposal->items->pluck('id')->toArray();
                    VendorProposalItemNegotiation::whereIn('item_id', $item_ids)->update(['status' => 0]);
                }

                if ($request->neg_tc_items) {
                    $item = new ProposalTermsNegotiaion();
                    $item->vendor_id = $this->user->id;
                    $item->proposal_id = $proposal->id;
                    $item->terms = $request->neg_tc_items;
                    $item->status = 1;
                    $item->save();
                    $proposal->negotiation_status = 1;
                    $proposal->save();
                } else {
                    ProposalTermsNegotiaion::where('proposal_id', $proposal->id)->update(['status' => 0]);
                }
            } else {
                $proposal->status = 'Accepted';
                $mailable_user = User::find($proposal->party1_id);
                $details_['email'] = $mailable_user->email;
                $details_['subject'] = 'Ask Deema Proposal';
                $details_['file'] = 'emails.email';
                $details_['name'] = $mailable_user->first_name . ' ' . $mailable_user->last_name;
                $details_['message_html'] = '
                    <p>Congratulations!</p>
                    <p>'.$proposal->event['event_name'].' Proposal has been approved By '.Auth::user()->first_name.' '.Auth::user()->last_name.'.</p>
                    <p>Thankyou</p>
                    <p>Ask Deema</p>';

                dispatch(new SendEmailJob($details_));

                $notification['text'] = $proposal->event['event_name'].' Proposal has been approved By '.Auth::user()->first_name.' '.Auth::user()->last_name;
                $notification['link'] = null; //if no link given Null
                Notification::send(User::find($proposal->party1_id), new UserNotification($notification));

            }
        }

        $proposal->save();
        if ($request->items && $request->editable == 1) {
            VendorProposalItem::where('proposal_id', $proposal->id)->delete();
            foreach (json_decode($request->items) as $row) {
                $item = new VendorProposalItem();
                $item->proposal_id = $proposal->id;
                $item->item_name = $row->event_name;
                $item->service_type_id = $row->service_type;
                $item->description = $row->description;
                $item->amount = $row->amount;
                $item->tax = $row->tax;
                $item->quantity = $row->quantity;
                $item->attachment = $row->file;
                $item->remarks = $row->remarks;
                $item->sub_total = $row->amount * $row->quantity;
                $item->tax_total = ($item->sub_total / 100) * $row->tax;
                $item->total = $item->sub_total + $item->tax_total;
                $item->save();
            }
        }

        $data['message'] = 'Proposal Updated.';
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
