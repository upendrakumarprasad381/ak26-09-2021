<?php

namespace App\Http\Controllers\EventPlanner;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmailJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Notifications\UserNotification;
use Notification;

class VendorManagerController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index($eventId, $categoryId) {

        return view('eventPlanner.vendorManager.eventPlaning')->with([
                    'eventId' => $eventId,
                    'categoryId' => $categoryId,
                    'page_title' => 'Vendor List',
        ]);
    }

    public function proposals($eventId) {
        return view('eventPlanner.vendorManager.proposals')->with([
                    'eventId' => $eventId,
        ]);
    }

    public function vendordetails($vendorId, $categoryId, $eventId) {
        if (isset($_POST['showRFQModal']) && !empty($_POST['showRFQModal'])) {
            $html = \App\Helpers\HtmlHelper::showRFQModal($vendorId, $categoryId, $eventId);
            die(json_encode(array('status' => true, 'HTML' => $html)));
        }
        return view('eventPlanner.vendorManager.vendordetails')->with([
                    'vendorId' => $vendorId,
                    'eventId' => $eventId,
                    'categoryId' => $categoryId,
                    'page_title' => 'Vendor Details',
        ]);
    }

    public function saveRFQ(Request $request) {
        if ($request->ajax()) {
            $data = $request->all();
            $json = $data['json'];
            $Sql = "SELECT * FROM `event_planner_request` WHERE event_id='" . $json['event_id'] . "' AND category_id='" . $json['category_id'] . "' AND vendor_id='" . $json['vendor_id'] . "'";
            $list = \App\Database::select($Sql);
            if (!empty($list)) {
                return response()->json(array('status' => false, 'message' => 'You have already sent RFQ.'), 200);
            }
            $json['event_planner_id'] = Auth::user()->id;
            $json['created_at'] = date('Y-m-d H:i:s');
            $json['updated_at'] = date('Y-m-d H:i:s');

            $mailable_user = User::find($json['vendor_id']);
            $details_['email'] = $mailable_user->email;
            $details_['subject'] = 'Ask Deema Leads';
            $details_['file'] = 'emails.email';
            $details_['name'] = $mailable_user->first_name . ' ' . $mailable_user->last_name;
            $details_['message_html'] = '
                <p>You received a new lead from '.Auth::user()->first_name.' '.Auth::user()->last_name.'.  Please login to view details.</p>
                <p>Thankyou</p>
                <p>Ask Deema</p>';

            dispatch(new SendEmailJob($details_));

            $notification['text'] = 'You received a new lead from '.Auth::user()->first_name.' '.Auth::user()->last_name.'.';
            $notification['link'] = null; //if no link given Null

            Notification::send(User::find($mailable_user->id), new UserNotification($notification));

            \App\Database::insert('event_planner_request', $json);
            return response()->json(array('status' => true, 'message' => 'RFQ send succesfully.'), 200);
        }
    }

    public function viewallrequest($eventId) {
        if (isset($_POST['sendReminderallrequest'])) {
            $msg = !empty($_POST['msg']) ? $_POST['msg'] : '';
            $receiverId = !empty($_POST['receiverId']) ? $_POST['receiverId'] : '';



            $notification['text'] = $msg;
            $notification['link'] = url('vendor/my-events'); //if no link given Null
            Notification::send(User::where('id', $receiverId)->first(), new UserNotification($notification));
            return response()->json(array('status' => true, 'message' => 'Send succesfully.'), 200);
        }
        return view('eventPlanner.vendorManager.viewallrequest')->with([
                    'eventId' => $eventId,
                    'page_title' => 'RFQ List',
        ]);
    }

}
