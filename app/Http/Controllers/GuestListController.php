<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use Redirect;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Jobs\SendEmailJob;
use Illuminate\Support\Facades\URL;
use Config;
use Excel;
use App\Exports\GuestListExport;

class GuestListController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index($eventId = '') {


        $Sql = "SELECT * FROM `guest_list_events` WHERE event_id='$eventId'";
        $eventList = \App\Database::select($Sql);
        if (empty($eventList)) {
            $event = \App\Helpers\LibHelper::GetvendoreventsById($eventId);
            $insert['event_id'] = $eventId;
            $insert['name'] = $event->event_name;
            $insert['created_at'] = date('Y-m-d H:i:s');
            $insert['updated_at'] = date('Y-m-d H:i:s');
            $insert['layout_id'] = '1';
            \App\Database::insert('guest_list_events', $insert);
        }

        return view('GuestList.index')->with([
                    'eventId' => $eventId,
                    'page_title' => 'Guest List',
        ]);
    }

    public function createevent($eventId = '') {
        if (isset($_POST['createEventguest'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json['event_id'] = $eventId;
            $json['updated_at'] = date('Y-m-d H:i:s');
            $json['created_at'] = $json['updated_at'];
            \App\Database::insert('guest_list_events', $json);
            return Redirect::to(url("/guest-list/$eventId"));
        }
        return view('GuestList.createevent')->with([
                    'eventId' => $eventId
        ]);
    }

    public function subeventExport($eventId = '', $subeventId = '') {
        $data[] = ['Name of Guest', 'Email', 'Phone No.', 'Alternate No.', 'Number Of Attendes', 'Answer Of Question', 'Answer Of Question', 'Any Remarks'];
        $Sql = "SELECT * FROM `guest_list_invitation` WHERE event_id='$eventId' AND guest_list_subevent_id='$subeventId'";
        $SqlData = \App\Database::select($Sql);
        for ($i = 0; $i < count($SqlData); $i++) {
            $d = $SqlData[$i];
            $data[] = array(
                $d->name,
                $d->email,
                $d->phone,
                $d->alternateno,
                $d->noof_attendes,
                $d->question,
                $d->date_of_rsvp,
                $d->remarks,
            );
        }
        $printData = new GuestListExport($data);
        return Excel::download($printData, 'invoices.xlsx');
    }

    public function subevent($eventId = '', $subeventId = '') {
        $pendingHTML = '<span style="background-color: #673ab7;" class="label label-warning label-inline mr-2">Pending</span>';
        $confirmfHTML = '<span style="background-color: #8bc34a;" class="label label-success label-inline mr-2">Coming</span>';

        if (isset($_POST['updateguestListstatus'])) {
            $primaryid = !empty($_POST['primaryid']) ? $_POST['primaryid'] : '';
            $iscompleted = !empty($_POST['iscompleted']) ? $_POST['iscompleted'] : '0';
            $html = empty($iscompleted) ? $pendingHTML : $confirmfHTML;
            \App\Database::updates('guest_list_invitation', ['status' => $iscompleted, 'updated_at' => date('Y-m-d H:i:s')], ['id' => $primaryid]);
            die(json_encode(array('status' => true, 'HTML' => $html)));
        }
        if (isset($_POST['daeleteGuest'])) {
            $primaryid = !empty($_POST['primaryid']) ? $_POST['primaryid'] : '';
            \App\Database::deletes('guest_list_invitation', ['id' => $primaryid]);
            die(json_encode(array('status' => true, 'HTML' => false)));
        }
        if (isset($_POST['sendreminderGuest'])) {
            $primaryid = !empty($_POST['primaryid']) ? $_POST['primaryid'] : '';
            $this->sendInvitation($primaryid);
            $html = ob_get_clean();
            die(json_encode(array('status' => true, 'HTML' => false)));
        }
        if (isset($_POST['importguestpoupBtn'])) {
            $file = !empty($_FILES['import_guest']) ? $_FILES['import_guest'] : [];
            if (!empty($file['name'])) {
                $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "image-list/" . $fileName;
                if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                    $data = \Excel::toArray(false, $baseDir);
                    $guestlist = !empty($data[0]) ? $data[0] : [];
                    for ($i = 1; $i < count($guestlist); $i++) {
                        $d = $guestlist[$i];
                        $inset['event_id'] = $eventId;
                        $inset['guest_list_subevent_id'] = $subeventId;
                        $inset['name'] = !empty($d['0']) ? $d['0'] : '';
                        $inset['email'] = !empty($d['1']) ? $d['1'] : '';
                        $inset['phone'] = !empty($d['2']) ? $d['2'] : '';
                        $inset['alternateno'] = !empty($d['3']) ? $d['3'] : '';
                        $inset['noof_attendes'] = !empty($d['4']) ? $d['4'] : '';
                        $inset['question'] = !empty($d['5']) ? $d['5'] : '';
                        $inset['date_of_rsvp'] = !empty($d['6']) ? date('Y-m-d', strtotime($d['6'])) : '';
                        $inset['remarks'] = !empty($d['7']) ? $d['7'] : '';
                        $inset['created_at'] = date('Y-m-d');
                        $inset['updated_at'] = $inset['created_at'];
                        $invtId = \App\Database::insert('guest_list_invitation', $inset);
                        $this->sendInvitation($invtId);
                    }
                    unlink($baseDir);
                }
            }
        }


        if (isset($_POST['autoload'])) {
            ob_start();
            $search = !empty($_POST['search']) ? $_POST['search'] : '';
            $tabId = !empty($_POST['tabId']) ? $_POST['tabId'] : '';
            $cond = "";
            if ($tabId == 'pending') {
                $cond = $cond . " AND G.status=0";
            }if ($tabId == 'confirmed') {
                $cond = $cond . " AND G.status=1";
            }if ($tabId == 'not-coming') {
                $cond = $cond . " AND G.status=2";
            }

            if (!empty($search)) {
                $cond = $cond . " AND (G.name LIKE '%$search%' || G.email LIKE '%$search%' || G.phone LIKE '%$search%')";
            }
            $Sql = "SELECT G.* FROM `guest_list_invitation` G WHERE G.event_id='$eventId' AND G.guest_list_subevent_id='$subeventId'  $cond ORDER BY G.id DESC";

            $list = \App\Database::select($Sql);
            if (!empty($list)) {
                for ($i = 0; $i < count($list); $i++) {
                    $d = $list[$i];
                    ?>
                    <tr>
                        <td class="text-center"><?= $i + 1 ?></td>
                        <td><?= $d->name ?></td>
                        <td class="text-center">
                            <div class="__statusChkWrap __nowrap">
                                <?php
                                if (empty($d->status) || $d->status == '1') {
                                    ?>
                                    <statusId<?= $d->id ?>>
                                        <?= empty($d->status) ? $pendingHTML : $confirmfHTML; ?>
                                    </statusid<?= $d->id ?>>
                                    <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
                                        <input  <?= !empty($d->status) ? 'checked' : '' ?> onclick="updateguestListstatus(this);" primaryId="<?= $d->id ?>"   type="checkbox" name="checkedId-<?= $i ?>">
                                        <span></span>
                                        &nbsp;
                                    </label>
                                    <?php
                                } else {
                                    echo '<span style="background-color: #e91e63;" class="label label-danger label-inline mr-2">Not Coming</span>';
                                }
                                ?>
                            </div>
                        </td>
                        <td class="text-center"><?= $d->question ?></td>
                        <td class="text-center"><?= $d->noof_attendes ?></td>
                        <td class="text-center"><?= $d->phone ?></td>
                        <td class="text-center">
                            <a class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary" href="<?= url("guest-list/$eventId/sub-event/$subeventId/create/$d->id") ?>"><i class="far fa-edit"></i></a>

                            <a  target="_blank" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary" href="<?= url("guest-list/$d->id/event-invitation-confirmation") ?>"><i class="fas fa-external-link-alt"></i></a>

                            <a onclick="daeleteGuest(this);" primaryId="<?= $d->id ?>" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary "    title="Remove"><i class="fas fa-trash-alt"></i></a>
                            <a onclick="sendreminderGuest(this);" primaryId="<?= $d->id ?>" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary "    title="Reminder"><i class="fas fa-bell"></i></a>
                        </td>

                    </tr>
                    <?php
                }
            } else {
                ?><tr><td colspan="8">No data found.</td></tr><?php
            }
            $html = ob_get_clean();
            die(json_encode(array('status' => true, 'HTML' => $html)));
        }



        return view('GuestList.subevent')->with([
                    'eventId' => $eventId,
                    'subeventId' => $subeventId,
                    'page_title' => 'Guest List Details',
        ]);
    }

    public function sendInvitation($Id = '') {
        $Invitation = \App\Helpers\CommonHelper::GetguestlistinvitationById($Id);
        $event = \App\Helpers\LibHelper::GetvendoreventsById($Invitation->event_id);
        $subevent = \App\Helpers\LibHelper::GetguestlisteventsById($Invitation->guest_list_subevent_id);

        $confirmUrl = url("guest-list/$Id/event-invitation-confirmation");
        $whatAppMsg = "";
        $whatAppMsg = $whatAppMsg . "Dear - $Invitation->name" . PHP_EOL . PHP_EOL;
        $whatAppMsg = $whatAppMsg . "Good Day," . PHP_EOL;
        $whatAppMsg = $whatAppMsg . "Hope you are doing well." . PHP_EOL . PHP_EOL;
        $whatAppMsg = $whatAppMsg . 'It is my immense pleasure to inform you that ' . $event->occasions_name . ' on  ' . date('d M Y', strtotime($event->event_date)) . '! I would love for all of you to be present at my ' . $event->occasions_name . ' and grace the day. I look forward to seeing you there. Please find the venue details and other information below: ' . PHP_EOL;
        $whatAppMsg = $whatAppMsg . 'Wedding ceremony: ' . date('d M Y', strtotime($event->event_date)) . PHP_EOL;
        $whatAppMsg = $whatAppMsg . 'Please RSVP to this email! :' . $confirmUrl . PHP_EOL . PHP_EOL;
        $whatAppMsg = $whatAppMsg . 'Thank You' . PHP_EOL;
        $whatAppMsg = $whatAppMsg . 'Ask Deema' . PHP_EOL;
        \App\Helpers\CommonHelper::sendWhatsappMessage($Invitation->phone, $whatAppMsg);

        $details['email'] = $Invitation->email;
        $details['subject'] = 'Ask Deema Invitation ' . $subevent->name;
        $details['file'] = 'emails.email';
        $details['name'] = $Invitation->name;
        $details['message_html'] = '<p>It is my immense pleasure to inform you that ' . $event->occasions_name . ' on  ' . date('d M Y', strtotime($event->event_date)) . '! I would love for all of you to be present at my ' . $event->occasions_name . ' and grace the day. I look forward to seeing you there. Please find the venue details and other information below:</p>

' . (!empty($event->event_location) ? '<p>' . $event->event_location . '</p>' : '') . '
        <p>Wedding ceremony: ' . date('d M Y', strtotime($event->event_date)) . '</p>
        <p>Please RSVP to this email! : <a href="' . $confirmUrl . '">Clic Here !</a></p>
        <p>Thank You</p>
        <p>Ask Deema</p>';

        dispatch(new SendEmailJob($details));
    }

    public function subeventcreate($eventId = '', $subeventId = '', $id = '') {
        if (isset($_POST['createguestlistBtn'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json['date_of_rsvp'] = date('Y-m-d', strtotime($json['date_of_rsvp']));
            $json['event_id'] = $eventId;
            $json['guest_list_subevent_id'] = $subeventId;
            $json['updated_at'] = date('Y-m-d H:i:s');

            if (empty($json['id'])) {
                $json['created_at'] = $json['updated_at'];
                $json['id'] = \App\Database::insert('guest_list_invitation', $json);
                $this->sendInvitation($json['id']);
            } else {
                \App\Database::updates('guest_list_invitation', $json, ['id' => $json['id']]);
                $this->sendInvitation($json['id']);
            }
            return Redirect::to(url("/guest-list/$eventId/sub-event/$subeventId"));
        }
        return view('GuestList.subeventcreate')->with([
                    'eventId' => $eventId,
                    'subeventId' => $subeventId,
                    'Id' => $id,
                    'page_title' => 'Guest List Create',
        ]);
    }

    public function layoutsettings($eventId = '', $subeventId = '') {
        return view('GuestList.layoutsettings')->with([
                    'eventId' => $eventId,
                    'subeventId' => $subeventId,
                    'page_title' => 'Guest List Layout Settings',
        ]);
    }

    public function layoutsettingscontent($eventId = '', $subeventId = '', $layoutId = '') {
        $layout = \App\Helpers\LibHelper::GetrsvpdesignById($layoutId);
        $mylayoutFolder = 'layout1';
        if (!empty($layout)) {
            $mylayoutFolder = $layout->layout_file_name;
        }
        if (!empty($_POST['submitMyDatacontent'])) {
            $update['layout_id'] = $layoutId;
            $update['content'] = !empty($_POST['content']) ? $_POST['content'] : '';
            $update['updated_at'] = date('Y-m-d H:i:s');

            $file = !empty($_FILES['logo']) ? $_FILES['logo'] : [];
            if (!empty($file['name'])) {
                $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "image-list/" . $fileName;
                if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                    $update['logo'] = $fileName;
                }
            }


            \App\Database::updates('guest_list_events', $update, ['id' => $subeventId]);

            $Sql = "SELECT *  FROM `guest_list_invitation` WHERE `guest_list_subevent_id` = '$subeventId'";
            $invitation = \App\Database::selectSingle($Sql);
            $url = url("guest-list/$eventId/sub-event/$subeventId");
            if (!empty($invitation->id)) {
                $url = url("guest-list/$invitation->id/event-invitation-confirmation");
            }
            return response()->json(array('type' => 'success', 'message' => 'updated', 'url' => $url));
        }
        if (!empty($_POST['resetMyLayou'])) {
            $update['layout_id'] = $layoutId;
            $update['content'] = '';
            $update['updated_at'] = date('Y-m-d H:i:s');

            \App\Database::updates('guest_list_events', $update, ['id' => $subeventId]);
            return response()->json(array('type' => 'success', 'message' => 'updated', 'url' => ''));
        }
        $html = view("GuestList.RSVP_Layout.$mylayoutFolder.defaultHTML")->with([
            'eventId' => $eventId,
            'subeventId' => $subeventId,
            'layoutId' => $layoutId,
            'page_title' => 'Guest List Layout Settings',
        ]);
//echo $html;exit;
        return view('GuestList.layoutsettingscontent')->with([
                    'eventId' => $eventId,
                    'subeventId' => $subeventId,
                    'layoutId' => $layoutId,
                    'dafaultHTML' => $html,
                    'page_title' => 'Guest List Layout Settings',
        ]);
    }

}
