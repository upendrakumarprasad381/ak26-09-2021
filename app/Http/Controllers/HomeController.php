<?php

namespace App\Http\Controllers;

use App\Http\Requests\Admin\Master\VendorItemRequest;
use App\Jobs\SendEmailJob;
use App\Models\Admin\Master\Category;
use App\Models\Admin\Master\Occasion;
use App\Models\Admin\Master\VendorItem;
use App\Models\User;
use App\Models\Vendor\Emirate;
use App\Models\Vendor\VendorAward;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_role = Auth::user()->roles[0];
        $slug = str_replace('_', '-', $user_role->name);
        if ($user_role->name == 'superadministrator') {
            return redirect('/admin');
        } else {
            return redirect('/' . $slug.'/dashboard');
        }
        return '/';
    }

    static function vendorDetailsPercentage()
    {
        $user = Auth::user();
        $vendor_details = Auth::user()->bussinessDetails;
        $fields = [
            'first_name' => 'user',
            'last_name' => 'user',
            'phone' => 'user',
            // 'phone_office' => 'user',
            'email' => 'user',
            'password' => 'user',
            'profile_picture' => 'user',
            'emirates' => 'user_details',
            'logo' => 'user_details',
            // 'business_website' => 'user_details',
            // 'address' => 'user_details',
            'trade_license' => 'user_details',
            'identity_proof' => 'user_details',
            'passport' => 'user_details',
            // 'instagram' => 'user_details',
            'city' => 'user_details',
            'country' => 'user_details',
            // 'min_price' => 'user_details',
            // 'max_price' => 'user_details',
            'capacity'  => 'user_details'
        ];
        $count = count($fields);

        $un_filled_count = 0;
        foreach ($fields as $key => $value) {
            if ($value == 'user_details') {
                if ($vendor_details->$key == '' || $vendor_details->$key == null) {
                    $un_filled_count++;
                }
            } else {
                if ($user->$key == '' || $user->$key == null) {
                    $un_filled_count++;
                }
            }
        }

        $percentage =  (($count - $un_filled_count) / $count) * 100;

        return  (int)$percentage;
    }

    /**
     * Show the personalInformation page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function personalInformation()
    {
        return view('admin.accountSettings.personal_information')->with([
            'user' => Auth::user(),
            'page_title' => 'Personal Informations',
            'percentage' => $this->vendorDetailsPercentage()
        ]);
    }

    /**
     * Show the personalInformation page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function businessInformation()
    {
        $user = Auth::user();
        return view('admin.accountSettings.business_information')->with([
            'user' => $user,
            'occasions' => Occasion::all(),
            'emirates' => Emirate::all(),
            'categories' => Category::where('type', 'Vendor')->get(),
            'page_title' => 'Business Informations',
            'percentage' => $this->vendorDetailsPercentage(),
            'countries' => DB::table('countries')->get(),
            'cities' => $user->bussinessDetails['country'] != 0 ? DB::table('cities')->where('country_id',$user->bussinessDetails['country'])->get() : []
        ]);
    }

    public function awardsAffliations()
    {
        return view('admin.accountSettings.awards')->with([
            'user' => Auth::user(),
            'awards' => VendorAward::where('user_id', Auth::user()->id)->get(),
            'page_title' => 'Business Informations',
            'percentage' => $this->vendorDetailsPercentage()
        ]);
    }

    /**
     * Show the passwordChange page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function changePassword()
    {
        return view('admin.accountSettings.change_password')->with([
            'user' => Auth::user(),
            'page_title' => 'Change Password',
            'percentage' => $this->vendorDetailsPercentage()
        ]);
    }

    /**
     * Save user informations.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function updateUserInformation(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'email' => 'required',
        ]);
        $user = User::find(auth()->user()->id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->phone = $request->phone;
        $user->phone_office = $request->phone_office;
        $user->email = $request->email;
        if ($request->has('profile_picture')) {
            $this->fileDelete($user->profile_picture);
            $user->profile_picture = $this->fileUpload($request, 'profile_picture', 'user/profile_picture');
        } else if (!$request->old_profile_picture) {
            $this->fileDelete($user->profile_picture);
            $user->profile_picture = null;
        }
        $user->save();


        $data['status'] = true;
        $data['message'] = 'User informations updated.';

        return $data;
    }

    /**
     * Update user password.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function updateUserPassword(Request $request)
    {
        $request->validate([
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $user = User::find(auth()->user()->id);
        $user->password = Hash::make($request->password);
        $user->save();

        $data['status'] = true;
        $data['message'] = 'User password updated.';

        return $data;
    }

    /**
     * Create Customer.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function createCustomer(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => 'required'
        ]);

        $user = new User();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $password = Str::random(8);
        $user->password = Hash::make($password);
        $user->save();
        $user->attachRole(5);

        $details['email'] = $request->email;
        $details['file'] = 'emails.email';
        $details['subject'] = 'Askdeema Account';
        $details['name'] = $request->first_name . ' ' . $request->last_name;
        $details['message_html'] = '<p> Your Askdeema account is created . <br> <br>
                                <b>Username : </b>' . $request->email . ' <br>
                                <b>Password : </b>' . $password . '</p>';

        dispatch(new SendEmailJob($details));


        $data['status'] = true;
        $data['user'] = $user;
        $data['message'] = 'User created.';

        return $data;
    }

    public function uploadFile(Request $request)
    {
        return $this->fileUpload($request, 'file', $request->path);
    }

    public function createVendorItem(VendorItemRequest $request)
    {
        $item = new VendorItem();
        $item->name = $request->name;
        $item->category_id = $request->category_id;
        $item->vendor_id = Auth::user()->id;
        $item->save();

        return $item;
        //    $data['message'] = 'Item created.';
        //    $data['name'] = $item->name;
        //    return $data;
    }
}
