<?php

namespace App\Http\Controllers;

use App\Models\Vendor\MemberDesignation;
use Illuminate\Http\Request;

class MemberDesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor\MemberDesignation  $memberDesignation
     * @return \Illuminate\Http\Response
     */
    public function show(MemberDesignation $memberDesignation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor\MemberDesignation  $memberDesignation
     * @return \Illuminate\Http\Response
     */
    public function edit(MemberDesignation $memberDesignation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor\MemberDesignation  $memberDesignation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MemberDesignation $memberDesignation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor\MemberDesignation  $memberDesignation
     * @return \Illuminate\Http\Response
     */
    public function destroy(MemberDesignation $memberDesignation)
    {
        //
    }
}
