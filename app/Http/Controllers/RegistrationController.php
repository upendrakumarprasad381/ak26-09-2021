<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use App\Database;
Use Redirect;
use App\Jobs\SendEmailJob;
use App\Models\Role;

class RegistrationController extends Controller {

    public function __construct() {

    }

    public function test1() {
        \App\Helpers\CommonHelper::setSessionuser();
    }

    public function test2() {
        $sArray = Session::all();


        //  $sArray = \App\Helpers\CommonHelper::GetSessionuser();
        return Redirect::to(url('/'));
    }

    public function customerloginTest(Request $request) {

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {





            if (Auth::user()->hasRole('user') || Auth::user()->hasRole('guest')) {
                if (\Request::session()->has("URL_PWD")) {
                    return redirect(\Request::session()->pull("URL_PWD"));
                } else {
                    return Redirect::to(url('/user/dashboard'));
                }
            } else {
                Auth::logout();
                return Redirect::back()->with('message', 'Only for customer/guest login.');
            }
        } else {
            return Redirect::back()->with('message', 'Invalid username or password.');
        }
    }

    public function customerlogin(Request $request) {

        if (isset($_POST['json']) && is_array($_POST['json'])) {

            $json = $_POST['json'];
            $user = User::where('email', $json['email'])->first();
            if (empty($user)) {
                $_POST['message'] = "Invalid username and password";
            }
            if (!empty($user)) {

                if (!$user->hasRole(['user'])) {
                    $_POST['message'] = "Invalid username and password";
                    return view('user.view.customerlogin')->with('posts', $_POST);
                }
            } else {
                $_POST['message'] = "Invalid username and password";
            }
            $credentials['email'] = $json['email'];
            $credentials['password'] = $json['password'];
            if ($token = Auth::attempt($credentials)) {
                $token = $token;





                $sesAr = \App\Helpers\LibHelper::GetusersBy($user->id);
            } else {
                $_POST['message'] = "Invalid username and password";
            }
        }
        return view('user.view.customerlogin')->with('posts', $_POST);
    }

    public function customerlogout() {
        session()->forget('userlogin');
        session()->save();
        Auth::logout();
        return Redirect::to(url('/'));
    }

    public function customersignup() {
        if (isset($_POST['json']) && is_array($_POST['json'])) {
            $json = $_POST['json'];
            $isValidate = !empty($_POST['isValidate']) ? $_POST['isValidate'] : '';
            $results = User::where('email', $json['email'])->first();
            if (!empty($results)) {
                die(json_encode(array('status' => false, 'messages' => 'Email id already registered')));
            }
            if (empty($isValidate)) {
                $otp = mt_rand(1000, 9999);
                $otp = '1111';
                $messagqwe = "You have one new OTP code: $otp";
                \App\Helpers\CommonHelper::send_sms($json['phone'], $messagqwe);

                $details['email'] = $json['email'];
                $details['subject'] = 'Ask Deema OTP Code';
                $details['file'] = 'emails.email';
                $details['name'] = $json['first_name'] . ' ' . $json['last_name'];
                $details['message_html'] = '<p>You have one new OTP Code: ' . $otp . '</p>
                <p>Thank you for choosing AskDeema.</p>
                <p>Ask Deema</p>';
                dispatch(new SendEmailJob($details));

                die(json_encode(array('status' => true, 'messages' => 'OTP send to your email or phone.', 'otp' => base64_encode($otp + 777))));
            }

            $json['password'] = Hash::make($json['password']);
            $json['status'] = '1';
            $user = User::Create($json);
            $user->attachRole(5);


            $details_['email'] = $user->email;
            $details_['subject'] = 'Ask Deema Registration';
            $details_['file'] = 'emails.email';
            $details_['name'] = $user->first_name . ' ' . $user->last_name;
            $details_['message_html'] = '<p>Congratulations!</p>
            <p>Registration successfully</p>
            <p>Thank you for choosing AskDeema.</p>
            <p>Ask Deema</p>';
            dispatch(new SendEmailJob($details_));


            die(json_encode(array('status' => true, 'messages' => 'Successfully registered')));
        }
        return view('user.view.customersignup');
    }

    public function vendorsignup() {






        if (isset($_POST['json']) && is_array($_POST['json'])) {
            $json = $_POST['json'];
            $details = !empty($_POST['details']) && is_array($_POST['details']) ? $_POST['details'] : [];
            $rollId = !empty($_POST['roll_id']) ? $_POST['roll_id'] : '2';
            $isValidate = !empty($_POST['isValidate']) ? $_POST['isValidate'] : '';
            $vendor_category = !empty($json['vendor_category']) ? explode(',', $json['vendor_category']) : [];
            $vendor_category = !empty($vendor_category) && is_array($vendor_category) ? $vendor_category : [];

            $vendor_occasions = !empty($json['vendor_occasions']) ? explode(',', $json['vendor_occasions']) : [];
            $vendor_occasions = !empty($vendor_occasions) && is_array($vendor_occasions) ? $vendor_occasions : [];

            $emirates = !empty($details['emirates']) ? explode(',', $details['emirates']) : [];
            $emirates = !empty($emirates) && is_array($emirates) ? $emirates : [];

            $results = User::where('email', $json['email'])->first();
            $passwordlogin = $json['password'];





//            Auth::login($results);
//
//            echo 'dasdas';exit;

            if (!empty($results)) {
                return response()->json(array('status' => false, 'messages' => 'Email id already registered'));
            }

            if (empty($isValidate)) {
                $otp = mt_rand(1000, 9999);
                $otp = '1111';
                $messagqwe = "You have one new OTP code: $otp";
                \App\Helpers\CommonHelper::send_sms($json['phone'], $messagqwe);



                $details_['email'] = $json['email'];
                $details_['subject'] = 'Ask Deema OTP Code';
                $details_['file'] = 'emails.email';
                $details_['name'] = $json['first_name'] . ' ' . $json['last_name'];
                $details_['message_html'] = '<p>You have one new OTP Code: ' . $otp . '</p>
            <p>Thank you for choosing AskDeema.</p>
            <p>Ask Deema</p>';
                dispatch(new SendEmailJob($details_));
                return response()->json(array('status' => true, 'messages' => 'OTP send to your email or phone.', 'otp' => base64_encode($otp + 777)));
            }

            $json['password'] = Hash::make($json['password']);
            $json['status'] = '1';
            unset($json['vendor_category'], $json['vendor_occasions'], $details['emirates']);
            $user = User::Create($json);
            $user->attachRole($rollId);
            if ($rollId == '3') {
                for ($i = 0; $i < count($vendor_occasions); $i++) {
                    $cat = array(
                        'occasion_id' => $vendor_occasions[$i],
                        'user_id' => $user->id,
                        'updated_at' => date('Y-m-d H:i:s'),
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    Database::insert('vendor_occasions', $cat);
                }
            } else {
                for ($i = 0; $i < count($vendor_category); $i++) {
                    $cat = array(
                        'category_id' => $vendor_category[$i],
                        'user_id' => $user->id,
                        'updated_at' => date('Y-m-d H:i:s'),
                    );
                    Database::insert('vendor_category', $cat);
                }
            }

            for ($i = 0; $i < count($emirates); $i++) {
                $em = array(
                    'emirate_id' => $emirates[$i],
                    'user_id' => $user->id,
                    'updated_at' => date('Y-m-d H:i:s'),
                );
                Database::insert('vendor_emirate', $em);
            }


            $details['role_id'] = $rollId;
            $details['user_id'] = $user->id;
            $details['updated_at'] = date('Y-m-d H:i:s');
            Database::updateOrinsert('vendor_details', $details, $matches = array('user_id' => $user->id));

            $details_['email'] = $user->email;
            $details_['subject'] = 'Ask Deema Registration';
            $details_['file'] = 'emails.email';
            $details_['name'] = $user->first_name . ' ' . $user->last_name;
            $details_['message_html'] = '<p>Congratulations!</p>
            <p>Thank you for choosing AskDeema. we will review your application and shall get in touch with you shortly.</p>
            <p>Registration successfully</p>
            <p>Ask Deema</p>';
            dispatch(new SendEmailJob($details_));

            $user->status = true;
            $user->messages = 'Registered successfully.';
            $user->GO_URL = url('/dashboard');
            if (Auth::attempt(['email' => $json['email'], 'password' => $passwordlogin])) {
                return $user;
            }
        }
        return view('user.view.vendorsignup');
    }

    public function helper() {
        $function = !empty($_POST['function']) ? $_POST['function'] : '';
        $helper = !empty($_POST['helper']) ? $_POST['helper'] : '';
        if ($helper == 'Common') {
            \App\Helpers\CommonHelper::$function();
        } else if ($helper == 'Lib') {
            \App\Helpers\LibHelper::$function();
        } else if ($helper == 'Html') {
            \App\Helpers\HtmlHelper::$function();
        } else {
            die(json_encode(array('status' => false, 'message' => 'Invalid helper name')));
        }
    }

}
