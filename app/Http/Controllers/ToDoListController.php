<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Config;
use App\Jobs\SendEmailJob;

class ToDoListController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        if (isset($_POST['assignedcontactSave']) && !empty($_POST['assignedcontactSave'])) {
            $guestId = !empty($_POST['guestId']) && is_array($_POST['guestId']) ? $_POST['guestId'] : [];
            $todoId = !empty($_POST['todoId']) ? $_POST['todoId'] : '';

            \App\Database::deletes('todo_list_guest', ['todo_id' => $todoId]);
            for ($i = 0; $i < count($guestId); $i++) {
                $d = $guestId[$i];
                $insert['todo_id'] = $todoId;
                $insert['user_id'] = $guestId[$i];
                $insert['created_at'] = date('Y-m-d H:i:s');
                \App\Database::insert('todo_list_guest', $insert);
            }
            die(json_encode(array('status' => true, 'message' => 'updated', 'HTML' => false)));
        }
        if (isset($_POST['modalforAssignContact']) && !empty($_POST['modalforAssignContact'])) {
            $todoId = !empty($_POST['todoId']) ? $_POST['todoId'] : '';

            $HTML = \App\Helpers\HtmlHelper::loadHTMLToDoListAssignContacts($todoId);
            die(json_encode(array('status' => true, 'message' => 'updated', 'HTML' => $HTML)));
        }
        if (isset($_POST['toDoASComplite']) && !empty($_POST['toDoASComplite'])) {
            $iscompleted = !empty($_POST['iscompleted']) ? $_POST['iscompleted'] : '';
            $comment = !empty($_POST['comment']) ? $_POST['comment'] : '';
            $type = !empty($_POST['type']) ? $_POST['type'] : '';
            $primaryid = !empty($_POST['primaryid']) ? $_POST['primaryid'] : '';
            if ($type == '1') {
                \App\Database::updates('original_checklist', ['completed' => $iscompleted, 'comment' => $comment, 'updated_at' => date('Y-m-d H:i:s')], ['id' => $primaryid]);
            } else {
                \App\Database::updates('todo_list', ['completed' => $iscompleted, 'comment' => $comment, 'updated_at' => date('Y-m-d H:i:s')], ['id' => $primaryid]);
            }
            return response()->json(array('type' => 'success', 'message' => 'updated', 'dataCount' => false, 'HTML' => false));
        }
        if (isset($_POST['autoload'])) {
            ob_start();
            $tabId = !empty($_POST['tabId']) ? $_POST['tabId'] : '';
            $eventId = !empty($_POST['eventId']) ? $_POST['eventId'] : '';
            $fromDays = !empty($_POST['fromDays']) ? $_POST['fromDays'] : '';


            $occasions = !empty($_POST['occasions']) && is_array($_POST['occasions']) ? $_POST['occasions'] : [];
            $occasions = implode(' || ', $occasions);

            $filter_list = !empty($_POST['filter_list']) && is_array($_POST['filter_list']) ? $_POST['filter_list'] : [];
            $filter_list = implode(' || ', $filter_list);

            $vendorCategory = !empty($_POST['vendorCategory']) && is_array($_POST['vendorCategory']) ? $_POST['vendorCategory'] : [];
            $vendorCategory = implode(',', $vendorCategory);

            $cond = '';
            $cond2 = '';

            if (!empty($eventId)) {
                $cond = $cond . " AND a.event_id='$eventId'";
            }

            if ($tabId == 'critical') {
                $cond = $cond . " AND DATEDIFF(a.date ,'" . date('Y-m-d') . "') <= 3 AND DATEDIFF(a.date ,'" . date('Y-m-d') . "') >= 0 AND a.completed NOT IN (1)";
            }
            if ($tabId == 'delayed') {
                $cond = $cond . " AND DATEDIFF(a.date ,'" . date('Y-m-d') . "') < 0 AND a.completed NOT IN (1)";
            }
            if ($tabId == 'upcoming') {
                $cond = $cond . " AND DATEDIFF(a.date ,'" . date('Y-m-d') . "') >= 4 AND DATEDIFF(a.date ,'" . date('Y-m-d') . "') <= 8 AND a.completed NOT IN (1)";
            }
            if ($tabId == 'coming') {
                $cond = $cond . " AND DATEDIFF(a.date ,'" . date('Y-m-d') . "') > 8 AND a.completed NOT IN (1)";
            }

            if (!empty($fromDays)) {
                $fromDays = date('Y-m-d', strtotime($fromDays));
                if ($fromDays > date('Y-m-d')) {
                    $cond = $cond . " AND a.date BETWEEN '" . date('Y-m-d') . "' AND '$fromDays'";
                } else {
                    $cond = $cond . " AND a.date BETWEEN '$fromDays' AND '" . date('Y-m-d') . "'";
                }
            }
            if (!empty($occasions)) {
                $cond = $cond . " AND $occasions";
            }
            if (!empty($filter_list)) {
                $cond2 = $cond2 . " AND $filter_list";
            }
            if (!empty($vendorCategory)) {
                $cond = $cond . " AND VE.id IN (SELECT vendor_event_id FROM `vendor_event_category` WHERE category_id IN ($vendorCategory))";
            }
            if (!empty($_POST['search'])) {
                $cond = $cond . " AND a.item_name LIKE '%" . $_POST['search'] . "%'";
            }
            $contact = "(SELECT GROUP_CONCAT(U.first_name) FROM `todo_list_guest` TLG LEFT JOIN users U ON U.id=TLG.user_id WHERE todo_id=TL.id)";
            $contact_ = "(SELECT GROUP_CONCAT(U.first_name) FROM `todo_list_guest` TLG LEFT JOIN users U ON U.id=TLG.user_id WHERE todo_id=original_checklist.id)";

            $addedBysomeOne = " UNION ALL SELECT TL.id,TL.event_id,'2' AS type,TFL.list_name,date,TL.items_name AS item_name, '' AS contact,TL.completed FROM todo_list_guest TLG LEFT JOIN `todo_list` TL ON TL.id=TLG.todo_id LEFT JOIN todo_filter_list TFL ON TFL.id=TL.list_id WHERE TLG.user_id='" . Auth::user()->id . "' $cond2";


            $todaay = date('Y-m-d');
            $qry1 = "SELECT TL.id,TL.event_id,'0' AS type,TFL.list_name,date,TL.items_name AS item_name,$contact AS contact,TL.completed FROM `todo_list` TL LEFT JOIN todo_filter_list TFL ON TFL.id=TL.list_id WHERE  TL.vendor_id='" . Auth::user()->id . "' $cond2";
            $qry2 = "UNION ALL SELECT id,event_id,'1' AS type,'Checklist' AS list_name,end AS date,item_name,$contact_ AS contact,completed FROM `original_checklist` WHERE vendor_id='" . Auth::user()->id . "'";
            $join = " LEFT JOIN vendor_events VE ON VE.id=a.event_id";

            $select = ",VE.event_name,IF(a.completed=0,DATEDIFF(`date`, CURDATE()),9) AS diffrent";
            $Sql = "SELECT a.* $select FROM ($qry1 $qry2 $addedBysomeOne) AS a $join WHERE 1 $cond ORDER BY diffrent ASC,date ASC";


            $list = \App\Database::select($Sql);
            if (!empty($list)) {
                for ($i = 0; $i < count($list); $i++) {
                    $d = $list[$i];



                    $isValid = $d->date > date('Y-m-d');
                    ?>
                    <tr>
                        <td class="text-center"><?= $i + 1 ?></td>
                        <td class="text-left"><?= $d->item_name ?> </td>
                        <td class="text-left">

                                <a data-toggle="tooltip" todoId="<?= $d->id ?>" onclick="ASSIGN.openModalforAssignContact(this);" data-placement="top" title="<?= $d->contact ?>" href="javascript:void(0)"><i style="font-size: 12px;" class="fas fa-plus"></i> <?= substr($d->contact, 0, 12) ?><?= !empty($d->contact) ? '..' : '' ?></a>

                        </td>
                        <td class="text-left"><?= $d->list_name ?><?= !empty($d->event_name) ? ' - '.$d->event_name : '' ?></td>
                        <td class="text-center"><?= date('d/m/Y', strtotime($d->date)) ?> </td>



                        <td class="text-center">
                            <div class="__statusChkWrap __nowrap">

                                <statusId<?= $d->id ?>>
                                    <?php
                                    $mystatus = '';
                                    // if (!empty($isValid)) {
                                        if ($d->diffrent >= 0 && $d->diffrent <= 3) {
                                            $mystatus = '<span class="label label-info label-inline mr-2 critical" style="background-color: orange !important;">Critical</span>';
                                        } elseif ($d->diffrent >= 4 && $d->diffrent <= 8) {
                                            $mystatus = '<span class="label label-dark  label-inline mr-2 upcoming">Upcoming</span>';
                                        } elseif ($d->diffrent < 0) {
                                            $mystatus = '<span class="label label-danger label-inline mr-2 delayed">Delayed</span>';
                                        } else {
                                            $mystatus = '<span class="label label-info label-inline mr-2 nextupcoming">Coming</span>';
                                        }
                                    // } else {
                                    //     $mystatus = '<span class="label label-danger label-inline mr-2  delayed">Delayed</span>';
                                    // }

                                    if (empty($d->completed)) {
                                        echo $mystatus;
                                    } else {
                                        echo '<span  class="label label-success label-inline mr-2 completed">Completed</span>';
                                    }
                                    ?>
                                </statusid<?= $d->id ?>>

                                <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
                                    <input mystatus='<?= $mystatus ?>' <?= !empty($d->completed) ? 'checked' : '' ?> onclick="updateMyToDoASComplite(this);" primaryId="<?= $d->id ?>" atype="<?= $d->type ?>"  type="checkbox" name="checkedId-<?= $i ?>">
                                    <span></span>
                                    &nbsp;
                                </label>



                            </div>
                        </td>
                        <td class="text-center">
                            <div class="__nowrap">



                                <a class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary autoupdate" atype="<?= $d->type ?>" todoId='<?= $d->id ?>' onclick="deleteTodo(this);" title="Remove"><i class="fas fa-trash-alt"></i></a>
                                <?php if (empty($d->type)) { ?>
                                    <a class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary" href="<?= url('to-do-list/create/' . $d->id) ?>"><i class="far fa-edit"></i></a>

                                <?php } else {
                                    ?><a class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary " target="_blank" href="<?= url("checklist/$d->event_id") ?>"  title="View more"><i class="fas fa-external-link-alt"></i></a><?php }
                                ?>

                            </div>
                        </td>

                    </tr>
                    <?php
                }
            } else {
                ?><tr><td colspan="8">No data found.</td></tr><?php
            }
            $html = ob_get_clean();
            die(json_encode(array('status' => true, 'dataCount' => count($list), 'HTML' => $html)));
        }
        return view('ToDoList.index')->with([
                    'page_title' => 'To Do List',
        ]);
    }

    public function create($Id = '') {
        return view('ToDoList.create')->with([
                    'Id' => $Id,
                    'page_title' => 'To Do Create',
        ]);
    }

    public function addnewcontact() {
        if (isset($_POST['json'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];



            $user = User::where('email', $json['email'])->first();
            if (!empty($user)) {
                die(json_encode(array('status' => false, 'message' => 'Email id is already registered')));
            }




            $json['password'] = Hash::make('guest@123');
            $user = User::Create($json);
            $user->attachRole(6);



            $details_['email'] = $json['email'];
            $details_['subject'] = 'Ask Deema Guest Invitation';
            $details_['file'] = 'emails.email';
            $details_['name'] = $json['first_name'] . ' ' . $json['last_name'];
            $details_['message_html'] = '<p>User Name : ' . $json['email'] . '</p>
                       <p>Password: guest@123</p>
                         <p><a href="' . url('customer-login') . '">click here to login</a></p>
             <p>Thank you for choosing AskDeema.</p>
            <p>Ask Deema</p>';
            dispatch(new SendEmailJob($details_));
            die(json_encode(array('status' => true, 'message' => 'Registered successfully')));
        }
    }

    public function store(Request $request) {
        $json = $request->json;
        $guestId = !empty($request->guestId) ? explode(',', $request->guestId) : [];
        $guestId = !empty($guestId) && is_array($guestId) ? $guestId : [];



        $file = !empty($_FILES['attachmentData']) ? $_FILES['attachmentData'] : [];
        if (!empty($file['name'])) {
            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
            $baseDir = Config::get('constants.HOME_DIR') . "image-list/" . $fileName;
            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                $json['attachment'] = $fileName;
            }
        }


        $json['vendor_id'] = Auth::user()->id;
        $json['date'] = date('Y-m-d', strtotime($json['date']));

        $json['updated_at'] = date('Y-m-d H:i:s');
        if (empty($json['id'])) {
            $json['created_at'] = $json['updated_at'];
            $json['id'] = \App\Database::insert('todo_list', $json);
        } else {
            \App\Database::updates('todo_list', $json, ['id' => $json['id']]);
        }
        \App\Database::deletes('todo_list_guest', ['todo_id' => $json['id']]);
        for ($i = 0; $i < count($guestId); $i++) {
            $d = $guestId[$i];
            $insert['todo_id'] = $json['id'];
            $insert['user_id'] = $guestId[$i];
            $insert['created_at'] = date('Y-m-d H:i:s');
            \App\Database::insert('todo_list_guest', $insert);
        }
        $todo = \App\Helpers\CommonHelper::GettodolistById($json['id']);

        $calenderId = \App\Helpers\CommonHelper::updateMycalender($todo->calender_id, $type = 'TODO_LIST', $todo->event_id, $primaryId = $json['id'], $title = $todo->items_name, $eventDate = $todo->date);
        \App\Database::updates('todo_list', ['calender_id' => $calenderId], ['id' => $json['id']]);

        $roleId = Auth::user()->roles()->first()->id;
        $url = url('/to-do-list');
        if ($roleId == '5' || $roleId == '6') {
            $url = url('/user/to-do-list');
        }


        die(json_encode(array('status' => true, 'updated successfully', 'url' => $url)));
    }

    public function getsubList(Request $request) {
        $Sql = "SELECT S.* FROM `todo_filter_sublist` S LEFT JOIN todo_filter_list M ON M.id=S.list_id WHERE S.status=0 AND S.list_id='$request->list_id' AND M.created_by='" . Auth::user()->id . "'";

        $data = \App\Database::select($Sql);
        die(json_encode(['success' => 'successfully', 'data' => $data]));
    }

    public function deletetodo(Request $request) {
        $atype = !empty($_POST['type']) ? $_POST['type'] : '';

        if ($atype == '1') {
            \App\Database::deletes('original_checklist', ['id' => $request->todo_id]);
        } else {
            \App\Database::deletes('todo_list', ['id' => $request->todo_id]);
        }
        die(json_encode(['success' => 'successfully', 'status' => true]));
    }

    public function getGuestlistForToDo() {
        $user = Auth::user()->parent_id != null ? Auth::user()->parent : Auth::user();
        $search = !empty($_POST['search']) ? $_POST['search'] : '';
        $cond = "";
        if (!empty($search)) {
            $cond = $cond . " AND (CONCAT(U.first_name,U.last_name) LIKE '%$search%' || U.email LIKE '%$search%')";
        }
        $join = "LEFT JOIN role_user RU ON RU.user_id=U.id LEFT JOIN vendor_details VD ON VD.user_id=U.id";
        $Sql = "SELECT U.id,U.first_name,U.phone,U.email,U.parent_id,VD.logo,VD.address FROM `users` U $join $cond LIMIT 8";
        $data = \App\Database::select($Sql);

        $baseDir = Config::get('constants.HOME_DIR');
        $html = "";
        for ($i = 0; $i < count($data); $i++) {
            $d = $data[$i];
            $baseFile = "storage/" . $d->logo;
            if (is_file($baseDir . $baseFile)) {
                $d->logo = url($baseFile);
            } else {
                $d->logo = url(Config::get('constants.DEFAULT_LOGO_VENDOR'));
            }
            $d->phone = !empty($d->phone) ? $d->phone : '+971456789012';
            $d->address = !empty($d->address) ? $d->address : '';
            $html = $html . ' <div class="col-lg-3 col-md-6 col-sm-6 venlist" userId="' . $d->id . '" json="' . base64_encode(json_encode($d)) . '" onclick="ASSIGN.setGuestlistForToDo(this);">
                             <div class="__grcadbz wd100">
                                <div class="__grcadbzImg">
                                    <img class="img-fluid"  src="' . $d->logo . '">
                                </div>
                                <div class="wd100">
                                    <h4 class="mt-2"><a href="javascript:void(0)">' . $d->first_name . '</a></h4>
                                    <h5 class="__todLilcotin mb-4"><a href="javascript:void(0)">' . $d->address . '</a> </h5>
                                    <div class="wd100 __todoCall __open_sans ">
                                        <a href="javascript:void(0)">' . $d->phone . '</a>
                                    </div>
                                    <div class="wd100 __todoEmail __open_sans">
                                        <a href="javascript:void(0)">' . $d->email . '</a>
                                    </div>
                                </div>
                            </div>
                        </div>';
        }
        die(json_encode(['status' => true, 'HTML' => $html]));
    }

}
