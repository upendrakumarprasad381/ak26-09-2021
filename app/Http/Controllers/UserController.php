<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmailJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use URL;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Notifications\UserNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Notification;

class UserController extends Controller {

    public function __construct() {

    }

    public function index() {

        return view('user.view.index');
    }

    public function vendorsportal() {
        return view('user.view.vendorsportal');
    }

    public function vendorsportalsummary() {
        return view('user.view.vendorsportalsummary');
    }

    public function helper() {
        $function = !empty($_POST['function']) ? $_POST['function'] : '';
        $helper = !empty($_POST['helper']) ? $_POST['helper'] : '';
        if ($helper == 'Common') {
            \App\Helpers\CommonHelper::$function();
        } else if ($helper == 'Lib') {
            \App\Helpers\LibHelper::$function();
        } else if ($helper == 'Html') {
            \App\Helpers\HtmlHelper::$function();
        } else {
            die(json_encode(array('status' => false, 'message' => 'Invalid helper name')));
        }
    }

    public function vendorslist() {

        return view('user.view.vendorslist');
    }

    public function vendordetails() {
        $urlRed = url(\Request::getRequestUri());
        if (empty(Auth::user()->id)) {
            \Request::session()->put("URL_PWD", $urlRed);
        }

        if (isset($_POST['json']) && is_array($_POST['json'])) {
            $json = $_POST['json'];
            $additional_services = !empty($_POST['additional_services']) && is_array($_POST['additional_services']) ? $_POST['additional_services'] : [];

            $catId = !empty($_POST['catId']) ? explode(',', $_POST['catId']) : [];
            $catId = !empty($catId) && is_array($catId) ? $catId : [];

            $session = \App\Helpers\CommonHelper::GetSessionuser();

            $json['user_id'] = $session->id;
            $json['created_at'] = date('Y-m-d H:i:s');
            $json['updated_at'] = $json['created_at'];

            $requestId = \App\Database::insert('vendor_events', $json);
            for ($i = 0; $i < count($catId); $i++) {
                $insert = array(
                    'vendor_event_id' => $requestId,
                    'category_id' => $catId[$i],
                    'created_at' => $json['updated_at'],
                    'updated_at' => $json['updated_at'],
                );
                \App\Database::insert('vendor_event_category', $insert);
            }
            for ($i = 0; $i < count($additional_services); $i++) {
                $d = $additional_services[$i];
                $d['request_id'] = $requestId;
                $d['updated_at'] = $json['created_at'];
                \App\Database::insert('request_additional_services', $d);
            }
            $url = url('/');
            die(json_encode(array('status' => true, 'messages' => 'Event created successfully.', 'url' => '')));
        }
        return view('user.view.vendordetails');
    }

    public function myservices() {
        return view('user.view.myservices');
    }

    public function aboutus() {
        return view('user.view.aboutus');
    }

    public function contactus() {
          $user = User::where('id', 459)->first();

     if (Auth::attempt(['email' => 'ep10@alwafaagroup.com', 'password' => 'Admin'])) {
            return $user;
        }

        return view('user.view.contactus');
    }

    public function privacypolicy() {
        return view('user.view.privacypolicy');
    }

    public function senduserRFQ() {
        if (isset($_POST['json']) && is_array($_POST['json'])) {
            $json = $_POST['json'];
            $eventId = !empty($json['event_id']) ? $json['event_id'] : '';
            $event = \App\Helpers\LibHelper::GetvendoreventsById($eventId);
            $session = \App\Helpers\CommonHelper::GetSessionuser();
            $json['request_no'] = \App\Helpers\LibHelper::getNewRequestno();
            $json['event_id'] = $eventId;
            $json['vendor_id'] = $json['vendor_id'];
            $json['user_id'] = $event->user_id;
            $json['event_name'] = $event->event_name;
            $json['type_of_event'] = $event->type_of_event == 'Indoor' ? '0' : 1;
            $json['first_name'] = $session->first_name;
            $json['second_name'] = $session->last_name;
            $json['email'] = $session->email;
            $json['wedding_date'] = $event->event_date;
            $json['flexible_wedding_date'] = $event->date_flexible == 'No' ? '0' : 1;
            $json['no_of_guests'] = $event->number_of_attendees;
            $json['venue_picked'] = $event->picked_a_venue == 'Yes, I have picked a venue' ? '1' : '0';
            $json['venue_name'] = $event->event_location;
            $json['start_time'] = $event->event_time;
            $json['no_of_hours'] = $event->number_of_hours;
            $json['minbudget'] = $event->min_budget;
            $json['maxbudget'] = $event->max_budget;
            $json['video_chat'] = $event->picked_a_venue == 'Maybe later' ? '0' : '1';
            $json['created_at'] = date('Y-m-d H:i:s');
            $json['updated_at'] = $json['created_at'];
            $requestId = \App\Database::insert('customer_request', $json);
            $Sql = "SELECT * FROM `vendor_event_category` WHERE vendor_event_id='$eventId'";
            $cat = \App\Database::select($Sql);
            for ($i = 0; $i < count($cat); $i++) {
                $d = $cat[$i];
                $insert['request_id'] = $requestId;
                $insert['category_id'] = $d->category_id;
                $insert['created_at'] = $json['created_at'];
                $insert['updated_at'] = $json['created_at'];
                \App\Database::insert('customer_request_category', $insert);
            }

            $mailable_user = User::find($json['vendor_id']);
            $details_['email'] = $mailable_user->email;
            $details_['subject'] = 'Ask Deema Customer Lead';
            $details_['file'] = 'emails.email';
            $details_['name'] = $mailable_user->first_name . ' ' . $mailable_user->last_name;
            $details_['message_html'] = '
                <p>You received a new lead from '.Auth::user()->first_name.' '.Auth::user()->last_name.'.  Please login to view details.</p>
                <p>Thankyou</p>
                <p>Ask Deema</p>';

            dispatch(new SendEmailJob($details_));

            $notification['text'] = 'You received a new lead from '.Auth::user()->first_name.' '.Auth::user()->last_name.'.  Please login to view details.';
            $notification['link'] = null; //if no link given Null

            Notification::send(User::find($mailable_user->id), new UserNotification($notification));

            $url = url('/');
            die(json_encode(array('status' => true, 'messages' => 'Request sent successfully.', 'url' => $url)));
        }
    }

    public function event() {
        return view('user.view.event');
    }

    public function eventdetails($eventId) {
        return view('user.view.eventdetails')->with([
                    'eventId' => $eventId,
        ]);
    }

    public function blog() {
        return view('user.view.blog');
    }

    public function blogdetails($blogId) {
        return view('user.view.blogdetails')->with([
                    'blogId' => $blogId,
        ]);
    }

    public function todolist() {
        if (isset($_POST['autoload'])) {
            ob_start();
            $tabId = !empty($_POST['tabId']) ? $_POST['tabId'] : '';


            $cond = '';
            $cond2 = '';
            if ($tabId == 'completed') {
                $cond = $cond . " AND a.status=1";
            }
            if ($tabId == 'upcoming') {
                $cond = $cond . " AND a.date > '" . date('Y-m-d') . "'";
            }
            if ($tabId == 'todays-overdue') {
                $cond = $cond . " AND a.date <= '" . date('Y-m-d') . "'";
            }

            if (!empty($_POST['search'])) {
                $cond = $cond . " AND a.item_name LIKE '%" . $_POST['search'] . "%'";
            }
            $userId = Auth::user()->id;
            $roleId = Auth::user()->roles()->first()->id;

            $addedByVenodr = " UNION ALL SELECT TL.id,TL.event_id,'2' AS type,TFL.list_name,date,TL.items_name AS item_name,TL.completed FROM todo_list_guest TLG LEFT JOIN `todo_list` TL ON TL.id=TLG.todo_id LEFT JOIN todo_filter_list TFL ON TFL.id=TL.list_id WHERE TLG.user_id='$userId' $cond2";
            //   echo $addedByVenodr;exit;


            $qry1 = " UNION ALL SELECT TL.id,TL.event_id,'0' AS type,TFL.list_name,date,TL.items_name AS item_name,TL.completed FROM `todo_list` TL LEFT JOIN todo_filter_list TFL ON TFL.id=TL.list_id WHERE TL.vendor_id='$userId' $cond2";
            $qry2 = "SELECT OC.id,event_id,'1' AS type,'checklist' AS list_name,OC.end AS date,OC.item_name,OC.completed FROM `original_checklist` OC  WHERE OC.vendor_id='$userId' ";
            $join = " LEFT JOIN vendor_events VE ON VE.id=a.event_id";
            $select = ",VE.event_name";
            $Sql = "SELECT a.* $select FROM ($qry2 $qry1 $addedByVenodr) AS a $join WHERE 1 $cond";


            $list = \App\Database::select($Sql);
            if (!empty($list)) {
                for ($i = 0; $i < count($list); $i++) {
                    $d = $list[$i];
                    ?>
                    <tr>
                        <td class="text-center"><?= $i + 1 ?></td>
                        <td><?= $d->event_name ?></td>
                        <td><?= $d->list_name ?></td>
                        <td class="text-center"><?= date('d/m/Y', strtotime($d->date)) ?></td>
                        <td class="text-center"><?= $d->item_name ?> </td>

                        <td class="text-center">
                            <?php
                            if (empty($d->status)) {
                                ?><span class="label label-warning label-inline mr-2">In-process</span><?php
                            } else {
                                ?>
                                <span class="label label-success label-inline mr-2">Completed</span>
                            <?php } ?>
                        </td>
                        <td class="text-center">
                            <?php if (empty($d->type)) { ?>
                                <a class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary" href="<?= url('to-do-list/create/' . $d->id) ?>"><i class="far fa-edit"></i></a>
                                <a class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary autoupdate" todoId='<?= $d->id ?>' onclick="deleteTodo(this);" title="Remove"><i class="fas fa-trash-alt"></i></a>
                            <?php } else if ($d->type == '1') {
                                ?><a class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary " target="_blank" href="<?= url("checklist/$d->event_id") ?>"  title="View more"><i class="fas fa-external-link-alt"></i></a><?php }
                            ?>
                        </td>

                    </tr>
                    <?php
                }
            } else {
                ?><tr><td colspan="8">No data found.</td></tr><?php
            }
            $html = ob_get_clean();
            die(json_encode(array('status' => true, 'HTML' => $html)));
        }
        return view('ToDoList.index')->with([
                    'page_title' => 'To Do List',
        ]);
    }

    public function eventInvitationConfirmation($invitationId) {
        $invitation = \App\Helpers\CommonHelper::GetguestlistinvitationById($invitationId);
        $subEvents = \App\Helpers\LibHelper::GetguestlisteventsById($invitation->guest_list_subevent_id);
        $mylayoutFolder = 'layout1';
        if (!empty($subEvents)) {
            $mylayoutFolder = $subEvents->layout_file_name;
        }

        if (isset($_POST['UpdateInvitationConfirmation'])) {
            $json['status'] = !empty($_POST['status']) ? $_POST['status'] : '';
            $json['answer'] = !empty($_POST['answer']) ? $_POST['answer'] : '';
            $json['updated_at'] = date('Y-m-d H:i:s');
            \App\Database::updates('guest_list_invitation', $json, ['id' => $invitationId]);
            die(json_encode(array('status' => true, 'msg' => 'updated successfully')));
        }
        $defaultHTML = view("GuestList.RSVP_Layout.$mylayoutFolder.defaultHTML")->with([
            'eventId' => $invitation->event_id,
            'subeventId' => $invitation->guest_list_subevent_id,
            'layoutId' => 1,
            'page_title' => 'Guest List Layout Settings',
        ]);
        return view("GuestList.RSVP_Layout.$mylayoutFolder.view")->with([
                    'invitationId' => $invitationId,
                    'defaultHTML' => $defaultHTML,
        ]);
//        return view('GuestList.eventInvitationConfirmation')->with([
//                    'invitationId' => $invitationId,
//        ]);
    }

}
