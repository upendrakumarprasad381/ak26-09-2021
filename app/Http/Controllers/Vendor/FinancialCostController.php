<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Models\Vendor\FinancialCost;
use App\Models\Vendor\FinancialCostItem;
use App\Models\Vendor\VendorContract;
use App\Models\Vendor\VendorEvent;
use Auth;
use Illuminate\Http\Request;

class FinancialCostController extends Controller
{
    private $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->parent_id != null ? Auth::user()->parent : Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('vendor.financialTool.cost')->with([
            'page_title' => 'Financial Cost',
            'data' => FinancialCost::where('vendor_id', $this->user->id)->orderBy('id', 'DESC')->get()
        ]);
    }

    public function getContracts()
    {
        $request = request();
        $query = new VendorContract();
        for ($i = 1; $i <= 2; $i++) {
            $where = $i == 1 ? 'where' : 'orWhere';
            $query = $query->$where(function ($qr) use ($request, $i) {
                $qr->where('party' . $i . '_id', $this->user->id);
                $qr->where('event_id', $request->event_id);
            });
        }
        return $query->get();
    }

    public function getContractItems()
    {
        $request = request();
        $result = VendorContract::find($request->contract_id);

        return $result->items;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event_by_event_planner = VendorEvent::with('eventPlannerRFQ')
            ->whereHas('eventPlannerRFQ', function ($q) {
                $q->where('event_planner_request.vendor_id', $this->user->id)->where('event_planner_request.status', 2);
            })->get();
        $event_by_customer = VendorEvent::with('customerRFQ')
            ->whereHas('customerRFQ', function ($q) {
                $q->where('customer_request.vendor_id', $this->user->id)->where('customer_request.status', 2);
            })->get();

        $event_by_own = VendorEvent::where('vendor_id', $this->user->id)->orderBy('id', 'DESC')->get();
        $events = $event_by_customer->merge($event_by_event_planner);
        $events = $events->merge($event_by_own);
        $events = $events->sortBy('created_at');

        return view('vendor.financialTool.create-cost')->with([
            'page_title' => 'Create Cost',
            'events' => $events
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cost = new FinancialCost();
        $cost->vendor_id = $this->user->id;
        $cost->event_id = $request->event_id;
        $cost->contract_id = $request->contract_id;
        $cost->date =  date('Y-m-d', strtotime($request->date));
        $cost->actual_cost = 0.00;
        $cost->invoice_amount = 0.00;
        $cost->save();
        $actual_cost = 0.00;
        $invoice_amount = 0.00;
        if ($request->item_name) {
            foreach ($request->item_name as $key => $value) {
                $cost_ = new FinancialCostItem();
                $cost_->cost_id = $cost->id;
                $cost_->item_name = $value;
                $cost_->actual_cost = $request->actual_cost[$key];
                $cost_->invoice_amount = $request->invoice_amount[$key];
                $cost_->save();
                $actual_cost += $request->actual_cost[$key];
                $invoice_amount += $request->invoice_amount[$key];
            }
            $cost->actual_cost = $actual_cost;
            $cost->invoice_amount = $invoice_amount;
            $cost->save();
        }

        return response()->json(array('status' => true, 'message' => 'Cost Saved.'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor\FinancialCost  $financialCost
     * @return \Illuminate\Http\Response
     */
    public function show(FinancialCost $financialCost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor\FinancialCost  $financialCost
     * @return \Illuminate\Http\Response
     */
    public function edit(FinancialCost $financialCost)
    {
        $event_by_event_planner = VendorEvent::with('eventPlannerRFQ')
            ->whereHas('eventPlannerRFQ', function ($q) {
                $q->where('event_planner_request.vendor_id', $this->user->id)->where('event_planner_request.status', 2);
            })->get();
        $event_by_customer = VendorEvent::with('customerRFQ')
            ->whereHas('customerRFQ', function ($q) {
                $q->where('customer_request.vendor_id', $this->user->id)->where('customer_request.status', 2);
            })->get();

        $event_by_own = VendorEvent::where('vendor_id', $this->user->id)->orderBy('id', 'DESC')->get();
        $events = $event_by_customer->merge($event_by_event_planner);
        $events = $events->merge($event_by_own);
        $events = $events->sortBy('created_at');
        $query = new VendorContract();
        for ($i = 1; $i <= 2; $i++) {
            $where = $i == 1 ? 'where' : 'orWhere';
            $query = $query->$where(function ($qr) use ($financialCost, $i) {
                $qr->where('party' . $i . '_id', $this->user->id);
                $qr->where('event_id', $financialCost->event_id);
            });
        }
        $contracts = $query->get();
        return view('vendor.financialTool.create-cost')->with([
            'page_title' => 'Create Cost',
            'events' => $events,
            'contracts' => $contracts,
            'editable' => $financialCost
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor\FinancialCost  $financialCost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FinancialCost $financialCost)
    {
        $cost = $financialCost;
        $cost->event_id = $request->event_id;
        $cost->contract_id = $request->contract_id;
        $cost->date = $request->date;
        $cost->actual_cost = 0.00;
        $cost->invoice_amount = 0.00;
        $cost->save();
        $actual_cost = 0.00;
        $invoice_amount = 0.00;
        if ($request->item_name) {
            FinancialCostItem::where('cost_id', $cost->id)->delete();
            foreach ($request->item_name as $key => $value) {
                $cost_ = new FinancialCostItem();
                $cost_->cost_id = $cost->id;
                $cost_->item_name = $value;
                $cost_->actual_cost = $request->actual_cost[$key];
                $cost_->invoice_amount = $request->invoice_amount[$key];
                $cost_->save();
                $actual_cost += $request->actual_cost[$key];
                $invoice_amount += $request->invoice_amount[$key];
            }
            $cost->actual_cost = $actual_cost;
            $cost->invoice_amount = $invoice_amount;
            $cost->save();
        }

        return response()->json(array('status' => true, 'message' => 'Cost Saved.'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor\FinancialCost  $financialCost
     * @return \Illuminate\Http\Response
     */
    public function destroy(FinancialCost $financialCost)
    {
        $financialCost->delete();
    }
}
