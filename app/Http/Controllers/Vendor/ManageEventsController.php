<?php

namespace App\Http\Controllers\Vendor;

use App\Helpers\EventPermissionHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Vendor\VendorEventsRequest;
use App\Models\Admin\Master\Category;
use App\Models\Admin\Master\Occasion;
use App\Models\User;
use App\Models\Vendor\EventPlannerRequest;
use App\Models\Vendor\VendorEvent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ManageEventsController extends Controller
{
    private $event_counts = [];
    private $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->parent_id != null?Auth::user()->parent:Auth::user();
            $this->event_counts = VendorEvent::where('vendor_id',$this->user->id)
            ->select(DB::raw("COUNT(id) AS total"),
            DB::raw("COALESCE(SUM(CASE WHEN status = 0 THEN 1 ELSE 0 END),0) AS active"),
            DB::raw("COALESCE(SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END),0) AS proposed"),
            DB::raw("COALESCE(SUM(CASE WHEN status = 2 THEN 1 ELSE 0 END),0) AS archived"))->first();
            return $next($request);
        });
    }

    /**
     * Show the vendor leads dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $event_by_event_planner = VendorEvent::with('eventPlannerRFQ')
                                    ->whereHas('eventPlannerRFQ' , function($q)
                                    {
                                        $q->where('event_planner_request.vendor_id',$this->user->id)->where('event_planner_request.status',2);
                                    })->get();
        $event_by_customer = VendorEvent::with('customerRFQ')
                                    ->whereHas('customerRFQ' , function($q)
                                    {
                                        $q->where('customer_request.vendor_id',$this->user->id)->where('customer_request.status',2);
                                    })->get();

        $event_by_own = VendorEvent::where('vendor_id',$this->user->id)->orderBy('id','DESC')->get();
        $leads = $event_by_customer->merge($event_by_event_planner);
        $leads = $leads->merge($event_by_own);
        $leads = $leads->sortBy('created_at');

        return view('vendor.myevents.dashboard')->with([
            'page_title' => 'My Events',
            'event_counts' => $this->event_counts,
            'events' =>$leads
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('vendor.myevents.create_new_event')->with([
           'page_title' => 'Create a Event',
           'occasions' => Occasion::all(),
           'customers' => User::whereRoleIs('user')->get(),
           'categories' => Category::where('type','Vendor')->where('status',1)->where('parent_id',null)->get(),
           'permissions' => EventPermissionHelper::getEventpermissions(['view-vendor-manager'])
       ]);
    }

    /**
     * Show the vendor leads show.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showEvent($id)
    {
        $event = VendorEvent::where('id',$id)->first();
        $permissions = EventPermissionHelper::getPermissions($event);
        return view('vendor.myevents.event_dashboard')->with([
            'event' => $event,
            'page_title' => $event->event_name,
            'event_counts' => $this->event_counts,
            'permissions' => $permissions->toArray()
        ]);
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorEventsRequest $request)
    {
        $vendor_event = new VendorEvent();
        $vendor_event->vendor_id = $this->user->id;
        $vendor_event->user_id = $request->user_id;
        $vendor_event->occasion_id = $request->occasion;
        $vendor_event->event_name = $request->event_name;
        $vendor_event->type_of_event = $request->type_of_event;
        if ($request->price_range) {
            $vendor_event->min_budget = json_decode($request->price_range)[0];
            $vendor_event->max_budget = json_decode($request->price_range)[0];
        }
        $vendor_event->final_budget = $request->final_budget ?: 0;
        $vendor_event->number_of_attendees = $request->number_of_attendees;
        $vendor_event->event_location = $request->event_location;
        $vendor_event->event_date = $request->event_date;
        $vendor_event->event_time = $request->event_time;
        $vendor_event->number_of_hours = 0;
        $vendor_event->quick_note = '';
        $vendor_event->type = 'Event Planner';
        $vendor_event->image = $this->fileUpload($request, 'image', 'event_planner/events/images');
        $vendor_event->document = $this->fileUpload($request, 'document', 'event_planner/events/documents');
        $vendor_event->save();

        if ($request->categories) {
            foreach ($request->categories as $key => $value) {
                DB::table('vendor_event_category')->insert([
                    'vendor_event_id' => $vendor_event->id,
                    'category_id' => $value
                ]);
            }
        }
        if ($request->event_permissions)
            EventPermissionHelper::attachPermissions($request->event_permissions, $vendor_event->id);

        \App\Helpers\CommonHelper::updateMycalender(null, 'EVENT', $vendor_event->id, null, $vendor_event->event_name, $vendor_event->event_date);
        $data['message'] = 'Event succesfully created.';
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor\VendorEvent  $vendorEvent
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendor_event = VendorEvent::find($id);
        $categories_ids = array();
        foreach ($vendor_event->categories as $key => $value) {
            array_push($categories_ids, $value->category_id);
        }
        return view('eventPlanner.myevents.edit')->with([
            'page_title' => 'Edit Event',
            'event' => $vendor_event,
            'occasions' => Occasion::all(),
            'customers' => User::whereRoleIs('user')->get(),
            'categories' => Category::where('type', 'Vendor')->where('status', 1)->where('parent_id', null)->get(),
            'categories_ids' => $categories_ids,
            'permissions' => EventPermissionHelper::getEventpermissions(),
            'event_permissions' => DB::table('permission_vendor_event')->join('permissions', 'permissions.id', 'permission_vendor_event.permission_id')->where('vendor_event_id', $id)->pluck('permissions.id')->toArray()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor\VendorEvent  $vendorEvent
     * @return \Illuminate\Http\Response
     */
    public function update(VendorEventsRequest $request, $id)
    {
        $vendorEvent = VendorEvent::find($id);
        $vendorEvent->user_id = $request->user_id;
        $vendorEvent->occasion_id = $request->occasion;
        $vendorEvent->event_name = $request->event_name;
        $vendorEvent->type_of_event = $request->type_of_event;
        if ($request->price_range) {
            $vendorEvent->min_budget = json_decode($request->price_range)[0];
            $vendorEvent->max_budget = json_decode($request->price_range)[0];
        }
        $vendorEvent->number_of_attendees = $request->number_of_attendees;
        $vendorEvent->event_location = $request->event_location;
        $vendorEvent->event_date = date("Y-m-d", strtotime($request->event_date));
        $vendorEvent->event_time = $request->event_time;
        $vendorEvent->number_of_hours = 0;
        $vendorEvent->quick_note = '';
        $vendorEvent->type = 'Vendor';
        if ($request->has('document')) {
            $vendorEvent->document = $this->fileUpload($request, 'document', 'event_planner/events/documents');
        }
        if ($request->has('image')) {
            $this->fileDelete($vendorEvent->image);
            $vendorEvent->image = $this->fileUpload($request, 'image', 'event_planner/events/images');
        } else if (!$request->old_image) {
            $this->fileDelete($vendorEvent->image);
            $vendorEvent->image = null;
        }
        $vendorEvent->save();


        if ($request->categories) {
            DB::table('vendor_event_category')->where('vendor_event_id', $vendorEvent->id)->delete();
            foreach ($request->categories as $key => $value) {
                DB::table('vendor_event_category')->insert([
                    'vendor_event_id' => $vendorEvent->id,
                    'category_id' => $value
                ]);
            }
        }
        if ($request->event_permissions)
            EventPermissionHelper::attachPermissions($request->event_permissions, $vendorEvent->id);

        $data['message'] = 'Event succesfully updated.';
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor\VendorEvent  $vendorEvent
     * @return \Illuminate\Http\Response
     */
    public function destroy(VendorEvent $vendorEvent)
    {
        //
    }






}
