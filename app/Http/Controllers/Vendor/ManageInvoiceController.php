<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Http\Requests\Vendor\VendorInvoicesRequest;
use App\Jobs\SendEmailJob;
use App\Models\Admin\Master\VendorTax;
use App\Models\User;
use App\Models\Vendor\VendorBudget;
use App\Models\Vendor\VendorBudgetItem;
use App\Models\Vendor\VendorContract;
use App\Models\Vendor\VendorEvent;
use App\Models\Vendor\VendorInvoice;
use App\Models\Vendor\VendorInvoiceItem;
use App\Notifications\UserNotification;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Notification;

class ManageInvoiceController extends Controller
{
    private $leads_counts = [];
    private $user;
    private $event_id;


    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->parent_id != null ? Auth::user()->parent : Auth::user();
            $path = request()->path();
            $this->event_id = explode('/', $path)[2];
            $query = new VendorInvoice();
            for ($i = 1; $i <= 2; $i++) {
                $where = $i == 1 ? 'where' : 'orWhere';
                $query = $query->$where(function ($qr) use ($request, $i) {
                    $qr->where('party' . $i . '_id', $this->user->id);
                    $qr->where('event_id', $this->event_id);
                });
            }
            $this->counts = $query->select(
                DB::raw("COALESCE(SUM(CASE WHEN status = 'Pending' THEN 1 ELSE 0 END),0) AS Pending"),
                DB::raw("COALESCE(SUM(CASE WHEN status = 'Accepted' THEN 1 ELSE 0 END),0) AS Accepted"),
                DB::raw("COALESCE(SUM(CASE WHEN payment_status = 'Paid' THEN 1 ELSE 0 END),0) AS Paid"),
                DB::raw("COALESCE(SUM(CASE WHEN status = 'Refunded' THEN 1 ELSE 0 END),0) AS Refunded"),
                DB::raw("COALESCE(SUM(CASE WHEN status = 'Rejected' THEN 1 ELSE 0 END),0) AS Rejected")
            )->first();
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = request();
        $query = new VendorInvoice();
        for ($i = 1; $i <= 2; $i++) {
            $where = $i == 1 ? 'where' : 'orWhere';
            $query = $query->$where(function ($qr) use ($request, $i) {
                $qr->where('party' . $i . '_id', $this->user->id);
                $qr->where('event_id', $this->event_id);
                if ($request->status) {
                    if ($request->status == 'Paid') {
                        $qr->where('payment_status', $request->status);
                    } else {
                        $qr->where('status', $request->status);
                    }
                }
                if ($request->occasion) {
                    $qr->with('event')
                        ->whereHas('event', function ($q) use ($request) {
                            $q->where('vendor_events.occasion_id', $request->occasion);
                        });
                }
                if ($request->q) {
                    $qr->with('event')
                        ->whereHas('event', function ($q) use ($request) {
                            $q->where('vendor_events.event_name', 'LIKE', "%{$request->q}%");
                        });
                }
            });
        }

        return view('vendor.invoices.index')->with([
            'page_title' => $request->status ? $request->status . ' Invoices' : 'Invoices',
            'event' =>  VendorEvent::find($this->event_id),
            'invoices' => $query->get(),
            'request' => $request,
            'counts' => $this->counts
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function acceptedInvoices()
    {
        $invoices = VendorInvoice::where([['party1_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Accepted']])
            ->orWhere([['party2_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Accepted']])->get();

        return view('vendor.invoices.index')->with([
            'page_title' => 'Accepted Invoices',
            'event' =>  VendorEvent::find($this->event_id),
            'invoices' => $invoices,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function paidInvoices()
    {
        $invoices = VendorInvoice::where([['party1_id', $this->user->id], ['event_id', $this->event_id], ['payment_status', 'Paid']])
            ->orWhere([['party2_id', $this->user->id], ['event_id', $this->event_id], ['payment_status', 'Paid']])->get();

        return view('vendor.invoices.index')->with([
            'page_title' => 'Paid Invoices',
            'event' =>  VendorEvent::find($this->event_id),
            'invoices' => $invoices,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function rejectedInvoices()
    {
        $invoices = VendorInvoice::where([['party1_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Rejected']])
            ->orWhere([['party2_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Rejected']])->get();

        return view('vendor.invoices.index')->with([
            'page_title' => 'Rejected Invoices',
            'event' =>  VendorEvent::find($this->event_id),
            'invoices' => $invoices,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function allInvoices()
    {
        $invoices = VendorInvoice::where([['party1_id', $this->user->id], ['event_id', $this->event_id]])
            ->orWhere([['party2_id', $this->user->id], ['event_id', $this->event_id]])->get();

        return view('vendor.invoices.index')->with([
            'page_title' => 'All Invoices',
            'event' =>  VendorEvent::find($this->event_id),
            'invoices' => $invoices,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($event_id, $contract_id = null)
    {
        $create_from_contract = 0;
        $contracts = VendorContract::with('items.category')->where([['party1_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Signed'], ['invoice_status', '!=', 2]])
            ->orWhere([['party2_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Signed'], ['invoice_status', '!=', 2]])->get();
        if ($contract_id) {
            $contracts = VendorContract::with('items.category')->where('invoice_status', '!=', 2)->where('id', $contract_id)->get();
            $create_from_contract = 1;
        }
        $last_sn = VendorInvoice::latest()->pluck('invoice_sn')->first();
        $next_sn = $last_sn ? (int)str_replace('ASK', '', $last_sn) : 1000;
        return view('vendor.invoices.create')->with([
            'page_title' => 'Add Invoice',
            'event' => VendorEvent::find($this->event_id),
            'contracts' => $contracts,
            'taxes' => VendorTax::all(),
            'next_sn' => $next_sn + 1,
            'create_from_contract' => $create_from_contract
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorInvoicesRequest $request)
    {
        $invoice = new VendorInvoice();
        $invoice->event_id = $request->event_id;
        $invoice->invoice_sn = $request->invoice_sn;
        $invoice->contract_id = $request->contract_id;
        $invoice->party1_id = $this->user->id;
        $invoice->status = $request->status;
        $invoice->party2_id = $request->party2_id;
        $invoice->date = date("Y-m-d", strtotime($request->date));
        $invoice->due_date = date("Y-m-d", strtotime($request->due_date));
        $invoice->approved_digital_written_signature = $request->approved_digital_written_signature ?: 0;
        $invoice->type = $request->type ? 'Digital' : 'Written';
        $invoice->send_notification_date = date("Y-m-d", strtotime($request->send_notification_date));
        $invoice->reminder_on_date = date("Y-m-d", strtotime($request->reminder_on_date));
        $invoice->weekly_reminder = $request->weekly_reminder ?: 0;
        $invoice->daily_reminder = $request->daily_reminder ?: 0;

        $invoice->signature_1 = $this->fileUpload($request, 'signature_1', 'vendor/invoices/' . $request->event_id . '/sign');
        // $invoice->signature_1 = $this->fileUpload($request,'signature_1','vendor/invoices/'.$request->event_id.'/sign');
        if ($request->signature_1_esign != null) {
            $base64_str = substr($request->signature_1_esign, strpos($request->signature_1_esign, ",") + 1);
            $image = base64_decode($base64_str);
            $path = 'vendor/invoices/' . $request->event_id . '/sign/' . $request->event_id . '-' . $invoice->date . '.png';
            Storage::disk('public')->put($path, $image);
            $invoice->signature_1 = $path;
        }
        $invoice->total_amount = $request->payment_total;
        $invoice->total_payable = $request->payment_amount;
        $invoice->total_paid = $request->payment_paid;


        $invoice->terms_of_agreement = VendorContract::find($request->contract_id)->terms_of_agreement;
        $invoice->save();

        if ($request->items) {
            foreach (json_decode($request->items) as $row) {
                $item = new VendorInvoiceItem();
                $item->invoice_id = $invoice->id;
                $item->item_name = $row->event_name;
                $item->service_type_id = $row->service_type;
                $item->description = $row->description;
                $item->amount = $row->amount;
                $item->tax = $row->tax;
                $item->quantity = $row->quantity;
                $item->attachment = $row->file;
                $item->remarks = $row->remarks;
                $item->sub_total = $row->amount * $row->quantity;
                $item->tax_total = ($item->sub_total / 100) * $row->tax;
                $item->total = $item->sub_total + $item->tax_total;
                $item->save();
            }
        }

        if ($request->payment_balance == 0) {
            $contract = VendorContract::find($invoice->contract_id);
            $contract->invoice_status = 2; //FUll Paid
            $contract->save();
        } else {
            $contract = VendorContract::find($invoice->contract_id);
            $contract->invoice_status = 1; //partially paid
            $contract->save();
        }

        $mailable_user = User::find($invoice->party2_id);
        $details_['email'] = $mailable_user->email;
        $details_['subject'] = 'Ask Deema Invoice';
        $details_['file'] = 'emails.email';
        $details_['name'] = $mailable_user->first_name . ' ' . $mailable_user->last_name;
        $details_['message_html'] = '
                    <p>You received a invoice for ' . $invoice->event['event_name'] . ' from ' . Auth::user()->first_name . ' ' . Auth::user()->last_name . '.  Please Login and Review this Invoice.</p>
                    <p>Thankyou</p>
                    <p>Ask Deema</p>';

        dispatch(new SendEmailJob($details_));

        $notification['text'] = 'You received a invoice for ' . $invoice->event['event_name'] . ' from ' . Auth::user()->first_name . ' ' . Auth::user()->last_name . '';
        $notification['link'] = null; //if no link given Null
        Notification::send(User::find($invoice->party2_id), new UserNotification($notification));


        $data['message'] = 'Invoice Send.';
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor\VendorInvoice  $vendorInvoice
     * @return \Illuminate\Http\Response
     */
    public function show($event_id, $id)
    {
        $invoice = VendorInvoice::find($id);
        $contracts = VendorContract::where('id', $invoice->contract_id)->get();
        return view('vendor.invoices.view')->with([
            'page_title' => 'View Invoice',
            'event' => VendorEvent::find($this->event_id),
            'contracts' => $contracts,
            'invoice' => $invoice,
            'editable' => $invoice->party1_id == $this->user->id ? 1 : 0,
            'taxes' => VendorTax::all()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor\VendorInvoice  $vendorInvoice
     * @return \Illuminate\Http\Response
     */
    public function edit($event_id, $id)
    {
        $invoice = VendorInvoice::find($id);
        $contracts = VendorContract::where('id', $this->event_id)->where('party2_id', $this->user->id)->where('invoice_status', 0)->orWhere('id', $invoice->contract_id)->get();
        return view('vendor.invoices.edit')->with([
            'page_title' => $invoice->status == 'Paid' ? 'View Invoice' : 'Edit Invoice',
            'event' => VendorEvent::find($this->event_id),
            'contracts' => $contracts,
            'invoice' => $invoice,
            'editable' => $invoice->party1_id == $this->user->id ? ($invoice->status == 'Paid' ? 0 : 1) : 0,
            'taxes' => VendorTax::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor\VendorInvoice  $vendorInvoice
     * @return \Illuminate\Http\Response
     */
    public function update(VendorInvoicesRequest $request,  $event_id, $id)
    {
        $invoice = VendorInvoice::find($id);
        if ($request->editable == 1) {
            $invoice->invoice_sn = $request->invoice_sn;
            $invoice->contract_id = $request->contract_id;
            $invoice->party1_id = $this->user->id;
            $invoice->party2_id = $request->party2_id;
            $invoice->status = $request->status;
            $invoice->date = date("Y-m-d", strtotime($request->date));
            $invoice->due_date = date("Y-m-d", strtotime($request->due_date));
            $invoice->approved_digital_written_signature = $request->approved_digital_written_signature ?: 0;
            $invoice->type = $request->type ? 'Digital' : 'Written';
            $invoice->send_notification_date = date("Y-m-d", strtotime($request->send_notification_date));
            $invoice->reminder_on_date = date("Y-m-d", strtotime($request->reminder_on_date));
            $invoice->weekly_reminder = $request->weekly_reminder ?: 0;
            $invoice->daily_reminder = $request->daily_reminder ?: 0;

            if ($request->has('signature_1')) {
                $this->fileDelete($invoice->signature_1);
                $invoice->signature_1 = $this->fileUpload($request, 'signature_1', 'vendor/invoices/' . $request->event_id . '/sign');
            } else if (!$request->old_signature_1) {
                $this->fileDelete($invoice->signature_1);
                $invoice->signature_1 = null;
            }

            if ($request->signature_1_esign != null) {
                $base64_str = substr($request->signature_1_esign, strpos($request->signature_1_esign, ",") + 1);
                $image = base64_decode($base64_str);
                $path = 'vendor/invoices/' . $request->event_id . '/sign/' . $request->event_id . '-' . $invoice->date . '.png';
                Storage::disk('public')->put($path, $image);
                $invoice->signature_1 = $path;
            }
            $invoice->terms_of_agreement = VendorContract::find($request->contract_id)->terms_of_agreement;

            $invoice->total_amount = $request->payment_total;
            $invoice->total_payable = $request->payment_amount;
            $invoice->total_paid = $request->payment_paid;
        } else {
            if ($request->has('signature_1')) {
                $this->fileDelete($invoice->signature_1);
                $invoice->signature_1 = $this->fileUpload($request, 'signature_1', 'vendor/invoices/' . $request->event_id . '/sign');
            } else if (!$request->old_signature_1) {
                $this->fileDelete($invoice->signature_1);
                $invoice->signature_1 = null;
            }

            if ($request->signature_1_esign != null) {
                $base64_str = substr($request->signature_1_esign, strpos($request->signature_1_esign, ",") + 1);
                $image = base64_decode($base64_str);
                $path = 'vendor/invoices/' . $request->event_id . '/sign/' . $request->event_id . '-' . $invoice->date . '.png';
                Storage::disk('public')->put($path, $image);
                $invoice->signature_1 = $path;
            }
            $invoice->status = 'Paid';
        }

        $invoice->save();

        if ($request->items && $request->editable == 1) {
            VendorInvoiceItem::where('invoice_id', $invoice->id)->delete();
            foreach (json_decode($request->items) as $row) {
                $item = new VendorInvoiceItem();
                $item->invoice_id = $invoice->id;
                $item->item_name = $row->event_name;
                $item->service_type_id = $row->service_type;
                $item->description = $row->description;
                $item->amount = $row->amount;
                $item->tax = $row->tax;
                $item->quantity = $row->quantity;
                $item->attachment = $row->file;
                $item->remarks = $row->remarks;

                $item->sub_total = $row->amount * $row->quantity;
                $item->tax_total = ($item->sub_total / 100) * $row->tax;
                $item->total = $item->sub_total + $item->tax_total;

                $item->save();
            }
        }

        if ($request->payment_balance == 0) {
            $contract = VendorContract::find($invoice->contract_id);
            $contract->invoice_status = 2; //FUll Paid
            $contract->save();
        } else {
            $contract = VendorContract::find($invoice->contract_id);
            $contract->invoice_status = 1; //partially paid
            $contract->save();
        }

        $data['message'] = 'Invoice successfully updated.';
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor\VendorInvoice  $vendorInvoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(VendorInvoice $vendorInvoice)
    {
        //
    }

    public function rejectInvoice($event_id, $id)
    {
        $invoice = VendorInvoice::find($id);
        $invoice->status = 'Rejected';
        $invoice->save();

        $notification['text'] = 'Your Invoice Rejected By ' . $invoice->party2['first_name'] . '.';
        $notification['link'] = null; //if no link given Null
        Notification::send(User::find($invoice->party1_id), new UserNotification($notification));

        return response()->json(array('status' => true, 'message' => 'Invoice ' . $invoice->invoice_sn . 'Is Rejected.'), 200);
    }

    public function acceptInvoice($event_id, $id)
    {
        $request = request();
        $invoice = VendorInvoice::find($id);
        $invoice->payment_status = $request->payment_status;
        $invoice->status = 'Accepted';
        $invoice->save();

        $category_id = $invoice->contract->proposal->request->category_id;
        $budgets = VendorBudget::where('vendor_id', $this->user->id)->where('event_id', $this->event_id)->get();
        foreach ($budgets as $key => $value) {
            $item = VendorBudgetItem::where('vendor_budget_id', $value->id)->where('category_id', $category_id)->first();
            $item->actual_paid = $item->actual_paid + $invoice->total_payable;
            $item->save();
        }
        $notification['text'] = 'Your Invoice Accepted ' . $request->payment_status . ' By ' . $invoice->party2['first_name'] . '.';
        $notification['link'] = null; //if no link given Null
        Notification::send(User::find($invoice->party1_id), new UserNotification($notification));

        return response()->json(array('status' => true, 'message' => 'Invoice ' . $invoice->invoice_sn . 'Is Accepted.'), 200);
    }
    public function invoiceCollectStatus($event_id, $id)
    {
        $request = request();
        $invoice = VendorInvoice::find($id);
        $invoice->collected_status = $request->collect_status;
        $invoice->save();

        return response()->json(array('status' => true, 'message' => 'Invoice ' . $invoice->invoice_sn . ' Payment ' . $invoice->collected_status . '.'), 200);
    }
}
