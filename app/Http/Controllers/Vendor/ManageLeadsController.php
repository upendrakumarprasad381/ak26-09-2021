<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmailJob;
use App\Models\User;
use App\Models\Vendor\CustomerRequest;
use App\Models\Vendor\EventPlannerRequest;
use App\Models\Vendor\VendorEvent;
use App\Models\Vendor\VendorEvents;
use App\Notifications\UserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Notification;

class ManageLeadsController extends Controller
{
    private $leads_counts = [];
    private $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->parent_id != null ? Auth::user()->parent : Auth::user();

            $customer = CustomerRequest::where('vendor_id', $this->user->id)->where('suggest_to',null)
                ->select(
                    DB::raw("COALESCE(SUM(CASE WHEN status = 0 THEN 1 ELSE 0 END),0) AS received"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END),0) AS opened"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 2 THEN 1 ELSE 0 END),0) AS ongoing"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 3 THEN 1 ELSE 0 END),0) AS declined")
                )->first();

            $vendor = EventPlannerRequest::where('vendor_id', $this->user->id)->where('suggest_to',null)
                ->select(
                    DB::raw("COALESCE(SUM(CASE WHEN status = 0 THEN 1 ELSE 0 END),0) AS received"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END),0) AS opened"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 2 THEN 1 ELSE 0 END),0) AS ongoing"),
                    DB::raw("COALESCE(SUM(CASE WHEN status = 3 THEN 1 ELSE 0 END),0) AS declined")
                )->first();
            $this->leads_counts['received'] = $customer['received'] + $vendor['received'];
            $this->leads_counts['opened'] = $customer['opened'] + $vendor['opened'];
            $this->leads_counts['ongoing'] = $customer['ongoing'] + $vendor['ongoing'];
            $this->leads_counts['declined'] = $customer['declined'] + $vendor['declined'];

            return $next($request);
        });
    }

    /**
     * Show the vendor leads dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('vendor.leads.dashboard')->with([
            'page_title' => 'Leads',
            'leads_counts' => $this->leads_counts,
        ]);
    }


    /**
     * Show the vendor leads services.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function recievedLeads()
    {
        $event_planner_leads = EventPlannerRequest::where('vendor_id', $this->user->id)->where('status', 0)->where('suggest_to',null)->get();
        $recieved_leads = CustomerRequest::where('vendor_id', $this->user->id)->where('status', 0)->where('suggest_to',null)->get();
        $leads = $event_planner_leads->merge($recieved_leads);
        $leads = $leads->sortBy('created_at');

        return view('vendor.leads.received_leads')->with([
            'recieved_leads' => $leads,
            'page_title' => 'Received Leads',
            'leads_counts' => $this->leads_counts,
        ]);
    }

    /**
     * Show the vendor leads services.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function suggestedLeads()
    {
        $event_planner_leads = EventPlannerRequest::where('vendor_id', $this->user->id)->where('suggest_to', '!=', null)->get();
        $recieved_leads = CustomerRequest::where('vendor_id', $this->user->id)->where('suggest_to', '!=', null)->get();
        $leads = $event_planner_leads->merge($recieved_leads);
        $leads = $leads->sortBy('created_at');

        return view('vendor.leads.suggested_dashboard')->with([
            'recieved_leads' => $leads,
            'page_title' => 'Suggested Leads',
        ]);
    }

    /**
     * Show the vendor leads services.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function lookForLeads()
    {
        $event_planner_leads = EventPlannerRequest::where('suggest_to', $this->user->id)->get();
        $recieved_leads = CustomerRequest::where('suggest_to', $this->user->id)->get();
        $leads = $event_planner_leads->merge($recieved_leads);
        $leads = $leads->sortBy('created_at');

        return view('vendor.leads.suggested_dashboard')->with([
            'recieved_leads' => $leads,
            'page_title' => 'Look For Leads',
        ]);
    }

    /**
     * Show the vendor leads show.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showRcievedLead($id)
    {
        $recieved_lead = CustomerRequest::where('id', $id)->first();
        return view('vendor.leads.received_lead_details')->with([
            'recieved_lead' => $recieved_lead,
            'page_title' => 'Received Leads',
            'leads_counts' => $this->leads_counts,
            'vendors' => User::whereRoleIs('vendor')->where('id', '!=', $this->user->id)->get()
        ]);
    }

    /**
     * Show the vendor opened services.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function ongoingLeads()
    {
        $event_planner_leads = EventPlannerRequest::where('vendor_id', $this->user->id)->where('status', 2)->where('suggest_to',null)->get();
        $recieved_leads = CustomerRequest::where('vendor_id', $this->user->id)->where('status', 2)->where('suggest_to',null)->get();
        $leads = $event_planner_leads->merge($recieved_leads);
        $leads = $leads->sortBy('created_at');
        return view('vendor.leads.received_leads')->with([
            'recieved_leads' => $leads,
            'page_title' => 'Ongoing Leads',
            'leads_counts' => $this->leads_counts,
        ]);
    }

    /**
     * Show the vendor opened services.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function declinedLeads()
    {
        $event_planner_leads = EventPlannerRequest::where('vendor_id', $this->user->id)->where('status', 3)->where('suggest_to',null)->get();
        $recieved_leads = CustomerRequest::where('vendor_id', $this->user->id)->where('status', 3)->where('suggest_to',null)->get();
        $leads = $event_planner_leads->merge($recieved_leads);
        $leads = $leads->sortBy('created_at');
        return view('vendor.leads.received_leads')->with([
            'recieved_leads' => $leads,
            'page_title' => 'Declined Leads',
            'leads_counts' => $this->leads_counts,
        ]);
    }

    public function acceptLead()
    {
        $request = request();
        $recieved_lead = CustomerRequest::find($request->id);
        if ($recieved_lead->status == 2) {
            return response()->json(array('status' => true, 'type' => 'warning', 'message' => 'Lead Already Accepted'), 200);
        }
        $recieved_lead->status = 2;
        $recieved_lead->save();

        \App\Helpers\CommonHelper::updateMycalender(null, 'EVENT', $recieved_lead->event_id, $recieved_lead->id, $recieved_lead->event['event_name'],  $recieved_lead->event['event_date']);

        $mailable_user = User::find($recieved_lead->user_id);
        $details_['email'] = $mailable_user->email;
        $details_['subject'] = 'Ask Deema Lead Accepted';
        $details_['file'] = 'emails.email';
        $details_['name'] = $mailable_user->first_name . ' ' . $mailable_user->last_name;
        $details_['message_html'] = '
                <p>Your lead accepted by ' . Auth::user()->first_name . ' ' . Auth::user()->last_name . '.  Please login to view details.</p>
                <p>Thankyou</p>
                <p>Ask Deema</p>';

        dispatch(new SendEmailJob($details_));

        $notification['text'] = 'Your lead accepted by ' . Auth::user()->first_name . ' ' . Auth::user()->last_name . '.';
        $notification['link'] = null; //if no link given Null

        Notification::send(User::find($mailable_user->id), new UserNotification($notification));

        $message['status'] = true;
        $message['message'] = 'Lead Accepted.';

        return $message;
    }
    public function declineLead()
    {
        $request = request();
        $recieved_lead = CustomerRequest::find($request->id);
        if ($recieved_lead->status == 3) {
            return response()->json(array('status' => true, 'type' => 'warning', 'message' => 'Lead Already Declined'), 200);
        }
        $recieved_lead->status = 3;
        $recieved_lead->save();

        $mailable_user = User::find($recieved_lead->user_id);
        $details_['email'] = $mailable_user->email;
        $details_['subject'] = 'Ask Deema Lead Rejected';
        $details_['file'] = 'emails.email';
        $details_['name'] = $mailable_user->first_name . ' ' . $mailable_user->last_name;
        $details_['message_html'] = '
                <p>Your lead rejected by ' . Auth::user()->first_name . ' ' . Auth::user()->last_name . '.  Please login to view details.</p>
                <p>Thankyou</p>
                <p>Ask Deema</p>';

        dispatch(new SendEmailJob($details_));

        $notification['text'] = 'Your lead rejected by ' . Auth::user()->first_name . ' ' . Auth::user()->last_name . '.';
        $notification['link'] = null; //if no link given Null

        Notification::send(User::find($mailable_user->id), new UserNotification($notification));

        $message['status'] = true;
        $message['message'] = 'Lead Declined.';

        return $message;
    }

    /**
     * Show the vendor leads show.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showEventPlannerRcievedLead($id)
    {
        $recieved_lead = EventPlannerRequest::where('id', $id)->first();
        return view('vendor.leads.received_lead_details')->with([
            'recieved_lead' => $recieved_lead,
            'page_title' => 'Received Leads',
            'leads_counts' => $this->leads_counts,
            'vendors' => User::whereRoleIs('vendor')->where('id', '!=', $this->user->id)->get()
        ]);
    }

    public function acceptEventPlannerLead()
    {
        $request = request();
        $recieved_lead = EventPlannerRequest::find($request->id);
        if ($recieved_lead->status == 2) {
            return response()->json(array('status' => true, 'type' => 'warning', 'message' => 'Lead Already Accepted'), 200);
        }
        $recieved_lead->status = 2;
        $recieved_lead->save();

        $mailable_user = User::find($recieved_lead->event_planner_id);
        $details_['email'] = $mailable_user->email;
        $details_['subject'] = 'Ask Deema Lead Accepted';
        $details_['file'] = 'emails.email';
        $details_['name'] = $mailable_user->first_name . ' ' . $mailable_user->last_name;
        $details_['message_html'] = '
                <p>Your lead acceted by ' . Auth::user()->first_name . ' ' . Auth::user()->last_name . '.  Please login to view details.</p>
                <p>Thankyou</p>
                <p>Ask Deema</p>';

        dispatch(new SendEmailJob($details_));

        $notification['text'] = 'Your Lead Accepted By ' . $recieved_lead->vendor['first_name'] . '.';
        $notification['link'] = null; //if no link given Null
        Notification::send(User::find($recieved_lead->event_planner_id), new UserNotification($notification));
        \App\Helpers\CommonHelper::updateMycalender(null, 'EVENT', $recieved_lead->event_id, $recieved_lead->id, $recieved_lead->event['event_name'],  $recieved_lead->event['event_date']);
        $message['status'] = true;
        $message['message'] = 'Lead Accepted.';

        return $message;
    }

    public function declineEventPlannerLead()
    {
        $request = request();
        $recieved_lead = EventPlannerRequest::find($request->id);
        if ($recieved_lead->status == 3) {
            return response()->json(array('status' => true, 'type' => 'warning', 'message' => 'Lead Already Declined'), 200);
        }
        $recieved_lead->status = 3;
        $recieved_lead->save();

        $mailable_user = User::find($recieved_lead->event_planner_id);
        $details_['email'] = $mailable_user->email;
        $details_['subject'] = 'Ask Deema Lead Rejected';
        $details_['file'] = 'emails.email';
        $details_['name'] = $mailable_user->first_name . ' ' . $mailable_user->last_name;
        $details_['message_html'] = '
                <p>Your lead Rejected by ' . Auth::user()->first_name . ' ' . Auth::user()->last_name . '.  Please login to view details.</p>
                <p>Thankyou</p>
                <p>Ask Deema</p>';

        dispatch(new SendEmailJob($details_));

        $notification['text'] = 'Your Lead Rejected By ' . $recieved_lead->vendor['first_name'] . '.';
        $notification['link'] = null; //if no link given Null
        Notification::send(User::find($recieved_lead->event_planner_id), new UserNotification($notification));

        $message['status'] = true;
        $message['message'] = 'Lead Declined.';

        return $message;
    }

    public function suggestLead()
    {
        $request = request();
        $request->validate([
            'vendor' => 'required',
        ]);
        if ($request->type == 'Event Planner') {
            $lead = EventPlannerRequest::find($request->request_id);
            $mailable_user = User::find($lead->event_planner_id);
        } else {
            $lead = CustomerRequest::find($request->request_id);
            $mailable_user = User::find($lead->user_id);
        }
        $lead->suggest_to = $request->vendor;
        $lead->suggest_note = $request->note;
        $lead->save();

        $details_['email'] = $mailable_user->email;
        $details_['subject'] = 'Ask Deema Lead Suggestion';
        $details_['file'] = 'emails.email';
        $details_['name'] = $mailable_user->first_name . ' ' . $mailable_user->last_name;
        $details_['message_html'] = '
                <p>You have a new lead Suggestion by ' . Auth::user()->first_name . ' ' . Auth::user()->last_name . '.  Please login to view details.</p>
                <p>Thankyou</p>
                <p>Ask Deema</p>';

        dispatch(new SendEmailJob($details_));

        $notification['text'] = 'You have a new lead Suggestion by ' .Auth::user()->first_name . ' ' . Auth::user()->last_name . '.';
        $notification['link'] = null; //if no link given Null
        Notification::send(User::find($request->vendor), new UserNotification($notification));

        $message['status'] = true;
        $message['message'] = 'Lead Suggested to ' . User::find($request->vendor)->first_name . '.';

        return $message;
    }
}
