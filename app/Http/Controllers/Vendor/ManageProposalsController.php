<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Http\Requests\Vendor\VendorProposalsRequest;
use App\Jobs\SendEmailJob;
use App\Models\Admin\Master\VendorItem;
use App\Models\Admin\Master\VendorTax;
use App\Models\ProposalTermsNegotiaion;
use App\Models\User;
use App\Models\Vendor\EventPlannerRequest;
use App\Models\Vendor\VendorEvent;
use App\Models\Vendor\VendorProposal;
use App\Models\Vendor\VendorProposalItem;
use App\Models\Vendor\VendorProposalItemNegotiation;
use App\Notifications\UserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Notification;

class ManageProposalsController extends Controller
{

    private $proposal_counts = [];
    private $user;
    private $event_id;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->parent_id != null ? Auth::user()->parent : Auth::user();
            $path = request()->path();
            $this->event_id = explode('/', $path)[2];
            $query = new VendorProposal();
            for ($i = 1; $i <= 2; $i++) {
                $where = $i == 1 ? 'where' : 'orWhere';
                $query = $query->$where(function ($qr) use ($i) {
                    $qr->where('party' . $i . '_id', $this->user->id);
                    $qr->where('event_id', $this->event_id);
                });
            }

            $this->proposal_counts = $query->select(
                DB::raw("COALESCE(SUM(CASE WHEN status = 'Send/Recieved' THEN 1 ELSE 0 END),0) AS send"),
                DB::raw("COALESCE(SUM(CASE WHEN status = 'Accepted' THEN 1 ELSE 0 END),0) AS accepted"),
                DB::raw("COALESCE(SUM(CASE WHEN status = 'Pending' THEN 1 ELSE 0 END),0) AS pending"),
                DB::raw("COALESCE(SUM(CASE WHEN status = 'Declined' THEN 1 ELSE 0 END),0) AS declined"),
                DB::raw("COALESCE(SUM(CASE WHEN status = 'On Hold' THEN 1 ELSE 0 END),0) AS hold"),
                DB::raw("COALESCE(SUM(CASE WHEN status = 'Draft' AND party1_id = " . $this->user->id . " THEN 1 ELSE 0 END),0) AS draft"),
            )->first();

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $request = request();
        $query = new VendorProposal();
        for ($i = 1; $i <= 2; $i++) {
            $where = $i == 1 ? 'where' : 'orWhere';
            $query = $query->$where(function ($qr) use ($request, $i) {
                $qr->where('party' . $i . '_id', $this->user->id);
                $qr->where('event_id', $this->event_id);
                if ($request->status) {
                    if ($request->status == 'Declined/On Hold') {
                        $qr->where('status', $request->status);
                    } else {
                        $qr->where('status', $request->status);
                    }
                }
                if ($request->occasion) {
                    $qr->with('event')
                        ->whereHas('event', function ($q) use ($request) {
                            $q->where('vendor_events.occasion_id', $request->occasion);
                        });
                }
                if ($request->q) {
                    $qr->with('event')
                        ->whereHas('event', function ($q) use ($request) {
                            $q->where('vendor_events.event_name', 'LIKE', "%{$request->q}%");
                        });
                }
                if ($i == 2) {
                    $qr->where('status', '!=', 'Draft');
                }
            });
        }

        return view('vendor.proposals.index')->with([
            'page_title' => $request->status ? $request->status . ' Proposals' : 'Proposals',
            'event' =>  VendorEvent::find($this->event_id),
            'proposals' => $query->get(),
            'request' => $request,
            'count' => $this->proposal_counts
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function acceptedProposals()
    {
        $proposals = VendorProposal::where([['party1_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Accepted']])
            ->orWhere([['party2_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Accepted']])->get();

        return view('vendor.proposals.index')->with([
            'page_title' => 'Accepted Proposals',
            'event' =>  VendorEvent::find($this->event_id),
            'proposals' => $proposals,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function pendingProposals()
    {
        $proposals = VendorProposal::where([['party1_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Pending']])
            ->orWhere([['party2_id', $this->user->id], ['event_id', $this->event_id], ['status', 'Pending']])->get();

        return view('vendor.proposals.index')->with([
            'page_title' => 'Pending Proposals',
            'event' =>  VendorEvent::find($this->event_id),
            'proposals' => $proposals,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $requests = EventPlannerRequest::where('event_id', $this->event_id)->where('vendor_id', $this->user->id)->where('suggest_to', null)->where('proposal_status', 0)->get();
        $requests_suggest = EventPlannerRequest::where('event_id', $this->event_id)->where('suggest_to', $this->user->id)->where('proposal_status', 0)->get();
        $requests = $requests->merge($requests_suggest);

        return view('vendor.proposals.create')->with([
            'page_title' => 'Add Proposal',
            'event' => VendorEvent::find($this->event_id),
            'requests' => $requests,
            'taxes' => VendorTax::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorProposalsRequest $request)
    {
        $proposal = new VendorProposal();
        $proposal->event_id = $request->event_id;
        $proposal->request_id = $request->request_id;
        $proposal->party1_id = $request->party1_id;
        $proposal->party2_id = $request->party2_id;
        $proposal->date = date("Y-m-d", strtotime($request->date));
        $proposal->approved_digital_written_signature = $request->approved_digital_written_signature ?: 0;
        $proposal->type = $request->type ? 'Digital' : 'Written';
        $proposal->status = $request->status;
        $proposal->signature_1 = $this->fileUpload($request, 'signature_1', 'vendor/proposals/' . $request->party1_id . '/sign');
        $proposal->signature_2 = $this->fileUpload($request, 'signature_2', 'vendor/proposals/' . $request->party2_id . '/sign');
        if ($request->signature_1_esign != null) {
            $base64_str = substr($request->signature_1_esign, strpos($request->signature_1_esign, ",") + 1);
            $image = base64_decode($base64_str);
            $path = 'vendor/proposals/' . $request->party1_id . '/sign/' . $request->event_id . '-' . $proposal->date . '.png';
            Storage::disk('public')->put($path, $image);
            $proposal->signature_1 = $path;
        }
        if ($request->signature_2_esign != null) {
            $base64_str = substr($request->signature_2_esign, strpos($request->signature_2_esign, ",") + 1);
            $image = base64_decode($base64_str);
            $path = 'vendor/proposals/' . $request->party2_id . '/sign/' . $request->event_id . '-' . $proposal->date . '.png';
            Storage::disk('public')->put($path, $image);
            $proposal->signature_2 = $path;
        }
        $proposal->terms_of_agreement = $request->terms_of_agreement;
        $proposal->save();
        if ($request->items) {
            foreach (json_decode($request->items) as $row) {
                $item = new VendorProposalItem();
                $item->proposal_id = $proposal->id;
                $item->item_name = $row->item_name;
                $item->service_type_id = $row->service_type_id;
                $item->sub_service_id = $row->sub_service_id;
                $item->description = $row->description;
                $item->amount = $row->amount;
                $item->tax = $row->tax;
                $item->quantity = $row->qty;
                $item->attachment = $row->image_url;
                $item->remarks = $row->remark;
                $item->sub_total = $row->sub_total;
                $item->tax_total = $row->tax_amount;
                $item->total = $row->total;
                $item->save();
            }
        }
        if ($request->draft) {
            $proposal->status = 'Draft';
            $proposal->save();
            $data['message'] = 'Proposal Saved in Draft.';
        } else {
            $event_planner_req = EventPlannerRequest::find($proposal->request_id);
            $event_planner_req->status = 2;
            $event_planner_req->proposal_status = 1;
            $event_planner_req->save();

            $mailable_user = User::find($request->party2_id);
            $details_['email'] = $mailable_user->email;
            $details_['subject'] = 'Ask Deema Proposal';
            $details_['file'] = 'emails.email';
            $details_['name'] = $mailable_user->first_name . ' ' . $mailable_user->last_name;
            $details_['message_html'] = '
                <p>You received a proposal for '.$proposal->event['event_name'].' from '.Auth::user()->first_name.' '.Auth::user()->last_name.'.  Please Login and Review this Proposal.</p>
                <p>Thankyou</p>
                <p>Ask Deema</p>';

            dispatch(new SendEmailJob($details_));

            $notification['text'] = 'You received a proposal for '.$proposal->event['event_name'].' from '.Auth::user()->first_name.' '.Auth::user()->last_name.'';
            $notification['link'] = null; //if no link given Null
            Notification::send(User::find($request->party2_id), new UserNotification($notification));

            $data['message'] = 'Proposal Send.';
        }


        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($event_id, $id)
    {
        $proposal = VendorProposal::find($id);
        $requests = EventPlannerRequest::Where('id', $proposal->request_id)->get();
        return view('vendor.proposals.view')->with([
            'page_title' => 'View Proposal',
            'event' => VendorEvent::find($this->event_id),
            'requests' => $requests,
            'proposal' => $proposal,
            'editable' => $proposal->party1_id == $this->user->id ? 1 : 0,
            'taxes' => VendorTax::all()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($event_id, $id)
    {
        $proposal = VendorProposal::find($id);
        $items = [];
        foreach ($proposal->items as $key => $value) {
            $items[$key]['id'] = $value->id;
            $items[$key]['item_name'] = $value->item_name;
            $items[$key]['service_type'] = $value->category['name'];
            $items[$key]['service_type_id'] = $value->service_type_id;
            $items[$key]['sub_service'] = $value->subCategory?$value->subCategory['name']:null;
            $items[$key]['sub_service_id'] = $value->sub_service_id;
            $items[$key]['description'] = $value->description;
            $items[$key]['amount'] = $value->amount;
            $items[$key]['tax'] = $value->tax;
            $items[$key]['qty'] = $value->quantity;
            $items[$key]['sub_total'] = $value->sub_total;
            $items[$key]['tax_amount'] = $value->tax_total;
            $items[$key]['total'] = $value->total;
            $items[$key]['image_url'] = $value->attachment;
            $items[$key]['remark'] = $value->remarks;
            if (count($value->negotiations) > 0) {
                $items[$key]['negotiation']['amount'] =  $value->negotiations[0]['amount'];
                $items[$key]['negotiation']['sub_total'] =  $value->negotiations[0]['amount'] * $value->quantity;
                $items[$key]['negotiation']['tax_total'] =  $items[$key]['negotiation']['sub_total'] / 100 * $value->tax;
                $items[$key]['negotiation']['total'] =  $items[$key]['negotiation']['tax_total'] + $items[$key]['negotiation']['sub_total'];
                $items[$key]['negotiation']['note'] =  $value->negotiations[0]['note'];
                $items[$key]['negotiation']['status'] =  $value->negotiations[0]['status'];
            }
        }

        $proposal_tc = $proposal->terms_of_agreement;
        $tc_negotiations = count($proposal->terms_negotiations) > 0 ? $proposal->terms_negotiations[0]['terms'] : null;
        $requests = EventPlannerRequest::where('id', $this->event_id)->where('vendor_id', $this->user->id)->where('status', 0)->orWhere('id', $proposal->request_id)->get();
        return view('vendor.proposals.edit')->with([
            'page_title' => 'Edit Proposal',
            'event' => VendorEvent::find($this->event_id),
            'requests' => $requests,
            'proposal' => $proposal,
            'editable' => $proposal->party1_id == $this->user->id ? ($proposal->status == 'Accepted' ? 0 : 1) : 0,
            'taxes' => VendorTax::all(),
            'items' => $items,
            'tc_negotiations' => $tc_negotiations,
            'proposal_tc' => $proposal_tc
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VendorProposalsRequest $request, $event_id, $id)
    {

        $proposal = VendorProposal::find($id);
        if ($request->editable == 1) {
            $proposal->date = date("Y-m-d", strtotime($request->date));
            $proposal->approved_digital_written_signature = $request->approved_digital_written_signature ?: 0;
            $proposal->type = $request->type ? 'Digital' : 'Written';
            if ($request->has('signature_1')) {
                $this->fileDelete($proposal->signature_1);
                $proposal->signature_1 = $this->fileUpload($request, 'signature_1', 'vendor/proposals/' . $request->party1_id . '/sign');
            } else if (!$request->old_signature_1) {
                $this->fileDelete($proposal->signature_1);
                $proposal->signature_1 = null;
            }

            if ($request->has('signature_2')) {
                $this->fileDelete($proposal->signature_2);
                $proposal->signature_2 = $this->fileUpload($request, 'signature_2', 'vendor/proposals/' . $request->party2_id . '/sign');
            } else if (!$request->old_signature_2) {
                $this->fileDelete($proposal->signature_2);
                $proposal->signature_2 = null;
            }

            if ($request->signature_1_esign != null) {
                $base64_str = substr($request->signature_1_esign, strpos($request->signature_1_esign, ",") + 1);
                $image = base64_decode($base64_str);
                $path = 'vendor/proposals/' . $request->party1_id . '/sign/' . $request->event_id . '-' . $proposal->date . '.png';
                Storage::disk('public')->put($path, $image);
                $proposal->signature_1 = $path;
            }
            if ($request->signature_2_esign != null) {
                $base64_str = substr($request->signature_2_esign, strpos($request->signature_2_esign, ",") + 1);
                $image = base64_decode($base64_str);
                $path = 'vendor/proposals/' . $request->party2_id . '/sign/' . $request->event_id . '-' . $proposal->date . '.png';
                Storage::disk('public')->put($path, $image);
                $proposal->signature_2 = $path;
            }
            $proposal->status = $request->status;
            $proposal->terms_of_agreement = $request->terms_of_agreement;
        } else {
            if ($request->has('signature_2')) {
                $this->fileDelete($proposal->signature_2);
                $proposal->signature_2 = $this->fileUpload($request, 'signature_2', 'vendor/proposals/' . $request->party2_id . '/sign');
            } else if (!$request->old_signature_2) {
                $this->fileDelete($proposal->signature_2);
                $proposal->signature_2 = null;
            }

            if ($request->signature_2_esign != null) {
                $base64_str = substr($request->signature_2_esign, strpos($request->signature_2_esign, ",") + 1);
                $image = base64_decode($base64_str);
                $path = 'vendor/proposals/' . $request->party2_id . '/sign/' . $request->event_id . '-' . $proposal->date . '.png';
                Storage::disk('public')->put($path, $image);
                $proposal->signature_2 = $path;
            }
            $proposal->status = 'Accepted';
        }

        $proposal->save();
        if ($request->items && $request->editable == 1) {
            $ids = [];

            foreach (json_decode($request->items) as $row) {
                $item = VendorProposalItem::where('proposal_id', $proposal->id)->where('item_name', $row->item_name)->first();
                if (!$item) {
                    $item = new VendorProposalItem();
                }
                $item->proposal_id = $proposal->id;
                $item->item_name = $row->item_name;
                $item->service_type_id = $row->service_type_id;
                $item->sub_service_id = $row->sub_service_id;
                $item->description = $row->description;
                $item->amount = $row->amount;
                $item->tax = $row->tax;
                $item->quantity = $row->qty;
                $item->attachment = $row->image_url;
                $item->remarks = $row->remark;
                $item->sub_total = $row->sub_total;
                $item->tax_total = $row->tax_amount;
                $item->total = $row->total;
                $item->save();
                array_push($ids, $item->id);
            }
            if ($proposal->status == 'Draft' && $request->editable == 1) {
                $proposal->status = 'Send/Recieved';
                $proposal->save();
                $event_planner_req = EventPlannerRequest::find($proposal->request_id);
                $event_planner_req->status = 2;
                $event_planner_req->proposal_status = 1;
                $event_planner_req->save();

                $mailable_user = User::find($proposal->party2_id);
                $details_['email'] = $mailable_user->email;
                $details_['subject'] = 'Ask Deema Proposal';
                $details_['file'] = 'emails.email';
                $details_['name'] = $mailable_user->first_name . ' ' . $mailable_user->last_name;
                $details_['message_html'] = '
                    <p>You received a proposal for '.$proposal->event['event_name'].' from '.Auth::user()->first_name.' '.Auth::user()->last_name.'.  Please Login and Review this Proposal.</p>
                    <p>Thankyou</p>
                    <p>Ask Deema</p>';

                dispatch(new SendEmailJob($details_));

                $notification['text'] = 'You received a proposal for '.$proposal->event['event_name'].' from '.Auth::user()->first_name.' '.Auth::user()->last_name.'';
                $notification['link'] = null; //if no link given Null
                Notification::send(User::find($proposal->party2_id), new UserNotification($notification));

                $data['message'] = 'Proposal Send.';
            } else {
                VendorProposalItemNegotiation::whereIn('item_id', $ids)->update(['status' => 0]);
                ProposalTermsNegotiaion::where('proposal_id',$proposal->id)->update(['status' => 0]);
                if($request->negotiation)
                {
                    $proposal->reviced_no = $proposal->reviced_no + 1;
                    $proposal->negotiation_status = 0;
                    $proposal->save();
                }
                $notification['text'] = 'Your Proposal Has Updated By ' . $proposal->party1['first_name'] . '.';
                $notification['link'] = null; //if no link given Null
                Notification::send(User::find($proposal->party2_id), new UserNotification($notification));
                $data['message'] = 'Proposal Updated.';
            }
            VendorProposalItem::where('proposal_id', $proposal->id)->whereNotIn('id', $ids)->delete();
        }

        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
