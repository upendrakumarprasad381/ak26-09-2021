<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use App\Models\Admin\Master\Category;
use App\Models\Admin\Master\Occasion;
use App\Models\Vendor\Emirate;
use Auth;
use DB;
use Illuminate\Http\Request;

class ManageShopFrontController extends Controller
{
    private $user;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->parent_id != null ? Auth::user()->parent : Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        return view('vendor.shop_front.business_information')->with([
            'user' => $this->user,
            'occasions' => Occasion::all(),
            'emirates' => Emirate::all(),
            'categories' => Category::where('type', 'Vendor')->get(),
            'page_title' => 'Business Informations',
            'countries' => DB::table('countries')->get(),
            'cities' => $this->user->bussinessDetails['country'] != 0 ? DB::table('cities')->where('country_id',$this->user->bussinessDetails['country'])->get() : []
        ]);
    }

    /**
     * Show the personalInformation page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function basicInformation()
    {
        return view('vendor.shop_front.personal_information')->with([
            'user' => $this->user,
            'page_title' => 'Basic Informations',
            'percentage' => HomeController::vendorDetailsPercentage()
        ]);
    }

}
