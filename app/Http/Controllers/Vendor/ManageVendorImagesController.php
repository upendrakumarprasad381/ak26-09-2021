<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Models\Vendor\VendorImage;
use Auth;
use Illuminate\Http\Request;

class ManageVendorImagesController extends Controller
{
    private $user;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->parent_id != null ? Auth::user()->parent : Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('vendor.shop_front.gallary.index')->with([
            'rows' => VendorImage::where('user_id', $this->user->id)->get(),
            'page_title' => 'Gallary',
            'user' =>$this->user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendor.shop_front.gallary.create')->with([
            'page_title' => 'Gallary',
            'user' =>$this->user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required',
        ]);
        $terms = new VendorImage();
        $terms->user_id = $this->user->id;
        $terms->image = $this->fileUpload($request, 'image', 'vendor/images');
        $terms->save();
        return response()->json(array('status' => true, 'message' => 'Image Created.'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $VendorImage = VendorImage::find($id);
        return view('vendor.shop_front.gallary.create')->with([
            'page_title' => 'Gallary',
            'editable' => $VendorImage,
            'user' =>$this->user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $VendorImage = VendorImage::find($id);
        $request->validate([
            'image' => 'required',
        ]);

        $VendorImage->user_id = $this->user->id;
        if ($request->has('image')) {
            $this->fileDelete($VendorImage->image);
            $VendorImage->image = $this->fileUpload($request, 'image', 'vendor/images');
        } else if (!$request->old_image) {
            $this->fileDelete($VendorImage->image);
            $VendorImage->image = null;
        }
        $VendorImage->save();

        return response()->json(array('status' => true, 'message' => 'Image Updated.'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $VendorImage = VendorImage::find($id);
        $VendorImage->delete();
    }
}
