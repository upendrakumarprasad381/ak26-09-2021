<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Models\Admin\Master\VendorTax;
use App\Models\Vendor\VendorDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MasterSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('vendor.masterSettings.tax')->with([
            'data' => Auth::user()->bussinessDetails,
            'taxes' => VendorTax::all(),
            'page_title' => 'Default Tax'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function defaultTax()
    {
        return view('vendor.masterSettings.tax')->with([
            'data' => Auth::user()->bussinessDetails,
            'taxes' => VendorTax::all(),
            'page_title' => 'Default Tax'
        ]);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->terms_conditions)
        {
            VendorDetail::where('user_id',Auth::user()->id)
            ->update(array('proposal_terms_condition' => $request->terms_conditions));
        }
        if($request->tax)
        {
            VendorDetail::where('user_id',Auth::user()->id)
            ->update(array('default_tax' => $request->tax));
        }

        return response()->json(array('status' =>true,'message' => 'Data updated..'),200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
