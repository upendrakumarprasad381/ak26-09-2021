<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Models\Vendor\NoOfAttendees;
use Auth;
use Illuminate\Http\Request;

class NoOfAttendeesController extends Controller
{
    private $user;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->parent_id != null?Auth::user()->parent:Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('vendor.masterSettings.noOfAttendees.index')->with([
            'rows' => NoOfAttendees::where('vendor_id',$this->user->id)->get(),
            'page_title' => 'No of Attendees'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendor.masterSettings.noOfAttendees.create')->with([
            'page_title' => 'No of Attendees'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'value' => 'required',
        ]);
        $terms = new NoOfAttendees();
        $terms->vendor_id = $this->user->id;
        $terms->value = $request->value;
        $terms->save();
        return response()->json(array('status' => true, 'message' => 'No of Attendees value Created.'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor\NoOfAttendees  $NoOfAttendees
     * @return \Illuminate\Http\Response
     */
    public function show(NoOfAttendees $NoOfAttendees)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor\NoOfAttendees  $NoOfAttendees
     * @return \Illuminate\Http\Response
     */
    public function edit(NoOfAttendees $NoOfAttendees)
    {
        return view('vendor.masterSettings.noOfAttendees.create')->with([
            'page_title' => 'No Of Attendees',
            'editable' => $NoOfAttendees
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor\NoOfAttendees  $NoOfAttendees
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NoOfAttendees $NoOfAttendees)
    {
        $request->validate([
            'value' => 'required',
        ]);

        $NoOfAttendees->vendor_id = $this->user->id;
        $NoOfAttendees->value = $request->value;
        $NoOfAttendees->save();

        return response()->json(array('status' => true, 'message' => 'No of Attendees Updated.'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor\NoOfAttendees  $NoOfAttendees
     * @return \Illuminate\Http\Response
     */
    public function destroy(NoOfAttendees $NoOfAttendees)
    {
        $NoOfAttendees->delete();
    }
}
