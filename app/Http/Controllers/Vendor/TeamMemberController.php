<?php

namespace App\Http\Controllers\Vendor;

use App\Helpers\EventPermissionHelper;
use App\Http\Controllers\Controller;
use App\Jobs\SendEmailJob;
use App\Models\Permission;
use App\Models\User;
use App\Models\Vendor\MemberDesignation;
use App\Notifications\UserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Notification;

class TeamMemberController extends Controller
{
    private $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->parent_id != null?Auth::user()->parent:Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.accountSettings.teamMembers.index')->with([
           'members' => User::where('parent_id',$this->user->id)->get(),
           'user' => $this->user,
           'page_title' => 'Team Members'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.accountSettings.teamMembers.create')->with([
            'user' => $this->user,
            'permissions' => Permission::where([['module_id',6]])->orderBy('id','ASC')->get(),
            'page_title' => 'Team Members',
            'designations' => MemberDesignation::where('vendor_id',$this->user->id)->get()
         ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);

        $user = new User();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->status = 0;
        $user->designation = $request->designation;
        $user->profile_picture = $this->fileUpload($request,'profile_picture','user/profile_picture');
        $password = Str::random(8);
        $user->password = Hash::make($password);
        $user->parent_id = $this->user->id;
        $user->save();
        $user->attachRole($this->user->roles[0]->id);

        foreach ($request->permissions as $key => $value) {
            $user->attachPermission($value);
        }

        $details['email'] = $request->email;
        $details['subject'] = 'Askdeema Account Registration';
        $details['file'] = 'emails.email';
        $details['name'] = $request->first_name.' '.$request->last_name;
        $details['message_html'] = '<p> Your Askdeema account is created by '.$this->user['first_name'].' '.$this->user['last_name'] .' . <br>
        Please login using this credentials. <br>
                                <b>Username : </b>'.$request->email.' <br>
                                <b>Password : </b>'.$password.'</p>';

        dispatch(new SendEmailJob($details));

        $notification['text'] = 'Your Askdeema account is created by '.$this->user['first_name'].' '.$this->user['last_name'];
        $notification['link'] = null; //if no link given Null
        Notification::send($user, new UserNotification($notification));

        $user->save();
        return response()->json(array('status' => true,'password' => $password, 'message' => 'New Team Member Created.'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editable = User::find($id);
        return view('admin.accountSettings.teamMembers.create')->with([
            'user' => $this->user,
            'editable' => $editable,
            'permissions' => Permission::where('module_id',6)->orderBy('id','ASC')->get(),
            'page_title' => 'Team Members',
            'designations' => MemberDesignation::where('vendor_id',$this->user->id)->get()
         ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$id],
        ]);

        $user = User::find($id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->designation = $request->designation;
        if($request->has('profile_picture'))
        {
            $this->fileDelete($user->profile_picture);
            $user->profile_picture = $this->fileUpload($request,'profile_picture','user/profile_picture');
        }
        else if(!$request->old_profile_picture)
        {
            $this->fileDelete($user->profile_picture);
            $user->profile_picture = null;
        }

        $user->parent_id = $this->user->id;
        $user->save();

        DB::table('permission_user')->where('user_id',$id)->delete();
        if($request->permissions)
        {
            foreach ($request->permissions as $key => $value) {
                $user->attachPermission($value);
            }

        }

        $user->save();
        return response()->json(array('status' => true, 'message' => 'New Team Member Updated.'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
    }
}
