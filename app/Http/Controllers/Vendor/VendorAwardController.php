<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Models\Vendor\VendorAward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VendorAwardController extends Controller
{

    private $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->parent_id != null?Auth::user()->parent:Auth::user();
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.accountSettings.awards.index')->with([
            'user' => Auth::user(),
            'awards' => VendorAward::where('user_id', $this->user->id)->get(),
            'page_title' => 'Business Informations',
            'percentage' => HomeController::vendorDetailsPercentage()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.accountSettings.awards.create')->with([
            'user' => $this->user,
            'page_title' => 'Business Informations',
            'percentage' => HomeController::vendorDetailsPercentage()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'thumbnail' => 'required',
        ]);

        $award = new VendorAward();
        $award->user_id = $this->user->id;
        $award->title = $request->title;
        $award->description = $request->description;
        $award->attachment = $this->fileUpload($request, 'attachment', 'vendor/awards/attachment');
        $award->thumbnail = $this->fileUpload($request, 'thumbnail', 'vendor/awards/thumbnail');
        $award->save();
        return response()->json(array('status' => true, 'message' => 'Award Is Saved.'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor\VendorAward  $vendorAward
     * @return \Illuminate\Http\Response
     */
    public function show(VendorAward $vendorAward)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor\VendorAward  $vendorAward
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendorAward = VendorAward::find($id);
        return view('admin.accountSettings.awards.create')->with([
            'user' => $this->user,
            'editable' => $vendorAward,
            'percentage' => HomeController::vendorDetailsPercentage()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor\VendorAward  $vendorAward
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $award = VendorAward::find($id);
        $request->validate([
            'title' => 'required',
        ]);

        $award->user_id = $this->user->id;
        $award->title = $request->title;
        $award->description = $request->description;
        if ($request->has('thumbnail')) {
            $this->fileDelete($award->thumbnail);
            $award->thumbnail = $this->fileUpload($request, 'thumbnail', 'vendor/awards/thumbnail');
        } else if (!$request->old_thumbnail) {
            // $this->fileDelete($award->thumbnail);
            // $award->thumbnail = null;
        }
        if ($request->has('attachment')) {
            $award->attachment = $this->fileUpload($request, 'attachment', 'vendor/awards/attachment');
        }
        $award->save();
        return response()->json(array('status' => true, 'message' => 'Award Is Saved.'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor\VendorAward  $vendorAward
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendorAward = VendorAward::find($id);
        $vendorAward->delete();
    }
}
