<?php

namespace App\Http\Controllers\vendor;

use App\Http\Controllers\Controller;
use App\Models\Vendor\CustomerRequest;
use App\Models\Vendor\EventPlannerRequest;
use App\Models\Vendor\FinancialCost;
use App\Models\Vendor\VendorContract;
use App\Models\Vendor\VendorInvoice;
use App\Models\Vendor\VendorProposal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class VendorHomeController extends Controller
{
    private $user;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->parent_id != null ? Auth::user()->parent : Auth::user();
            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total_leads = CustomerRequest::where('vendor_id', $this->user->id)->count('id') + EventPlannerRequest::where('vendor_id', $this->user->id)->count('id');
        $total_proposals = VendorProposal::where('party1_id', $this->user->id)->orWhere('party2_id', $this->user->id)->count('id');
        $total_contracts = VendorContract::where('party1_id', $this->user->id)->orWhere('party2_id', $this->user->id)->count('id');
        $total_invoices = VendorInvoice::where('party1_id', $this->user->id)->orWhere('party2_id', $this->user->id)->count('id');

        // $customer = CustomerRequest::where('vendor_id', $this->user->id)
        //     ->select(
        //         DB::raw("COALESCE(SUM(CASE WHEN status = 0 THEN 1 ELSE 0 END),0) AS received"),
        //         DB::raw("COALESCE(SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END),0) AS opened"),
        //         DB::raw("COALESCE(SUM(CASE WHEN status = 2 THEN 1 ELSE 0 END),0) AS ongoing"),
        //         DB::raw("COALESCE(SUM(CASE WHEN status = 3 THEN 1 ELSE 0 END),0) AS declined")
        //     )->first();

        // $vendor = EventPlannerRequest::where('vendor_id', $this->user->id)
        //     ->select(
        //         DB::raw("COALESCE(SUM(CASE WHEN status = 0 THEN 1 ELSE 0 END),0) AS received"),
        //         DB::raw("COALESCE(SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END),0) AS opened"),
        //         DB::raw("COALESCE(SUM(CASE WHEN status = 2 THEN 1 ELSE 0 END),0) AS ongoing"),
        //         DB::raw("COALESCE(SUM(CASE WHEN status = 3 THEN 1 ELSE 0 END),0) AS declined")
        //     )->first();

        // $leads['total'] = $total_leads;
        // $leads['received'] = $customer['received'] + $vendor['received'];
        // $leads['opened'] = $customer['opened'] + $vendor['opened'];
        // $leads['ongoing'] = $customer['ongoing'] + $vendor['ongoing'];
        // $leads['declined'] = $customer['declined'] + $vendor['declined'];


        // $proposals = VendorProposal::where('vendor_id', $this->user->id)
        //     ->select(
        //         DB::raw("COALESCE(SUM(CASE WHEN status = 'Send/Recieved' THEN 1 ELSE 0 END),0) AS Send/Recieved"),
        //         DB::raw("COALESCE(SUM(CASE WHEN status = 'Pending' THEN 1 ELSE 0 END),0) AS Pending"),
        //         DB::raw("COALESCE(SUM(CASE WHEN status = Declined THEN 1 ELSE 0 END),0) AS Declined"),
        //         DB::raw("COALESCE(SUM(CASE WHEN status = Accepted THEN 1 ELSE 0 END),0) AS Accepted")
        //     )->first();
        // $proposal['total'] = $total_proposals;
        // $proposal['Send'] = $proposals['Send'];
        // $proposal['Pending'] = $proposals['Pending'];
        // $proposal['Declined'] = $proposals['Declined'];
        // $proposal['Accepted'] = $proposals['Accepted'];

        // $contracts = VendorContract::where('vendor_id', $this->user->id)
        //     ->select(
        //         DB::raw("COALESCE(SUM(CASE WHEN status = Pending THEN 1 ELSE 0 END),0) AS Pending"),
        //         DB::raw("COALESCE(SUM(CASE WHEN status = Signed THEN 1 ELSE 0 END),0) AS Signed"),
        //         DB::raw("COALESCE(SUM(CASE WHEN status = Declined THEN 1 ELSE 0 END),0) AS Declined"),
        //         DB::raw("COALESCE(SUM(CASE WHEN status = On Hold THEN 1 ELSE 0 END),0) AS On Hold")
        //     )->first();

        // $contracts['total'] = $total_contracts;
        // $contracts['Pending'] = $contracts['Pending'];
        // $contracts['Signed'] = $contracts['Signed'];
        // $contracts['Declined'] = $contracts['Declined'];
        // $contracts['On Hold'] = $contracts['On Hold'];

        // $invoices = VendorContract::where('vendor_id', $this->user->id)
        //     ->select(
        //         DB::raw("COALESCE(SUM(CASE WHEN status = Pending THEN 1 ELSE 0 END),0) AS Pending"),
        //         DB::raw("COALESCE(SUM(CASE WHEN status = Signed THEN 1 ELSE 0 END),0) AS Signed"),
        //         DB::raw("COALESCE(SUM(CASE WHEN status = Declined THEN 1 ELSE 0 END),0) AS Declined"),
        //         DB::raw("COALESCE(SUM(CASE WHEN status = On Hold THEN 1 ELSE 0 END),0) AS On Hold")
        //     )->first();

        // $invoices['total'] = $total_invoices;
        // $invoices['Pending'] = $invoices['Pending'];
        // $invoices['Signed'] = $invoices['Signed'];
        // $invoices['Declined'] = $invoices['Declined'];
        // $invoices['On Hold'] = $invoices['On Hold'];



        $data['total_leads'] = $total_leads;
        $data['total_proposals'] = $total_proposals;
        $data['total_contracts'] =  $total_contracts;
        $data['total_invoices'] =  $total_invoices;
        $data['page_title'] = 'Dashboard';

        return view('vendor.dashboard')->with($data);
    }

    public function allProposals()
    {
        $request = request();
        $query = new VendorProposal();
        for ($i = 1; $i <= 2; $i++) {
            $where = $i == 1 ? 'where' : 'orWhere';
            $query = $query->$where(function ($qr) use ($request, $i) {
                $qr->where('party' . $i . '_id', $this->user->id);
                if ($request->status) {
                    if ($request->status == 'Declined/On Hold') {
                        $qr->where('status', $request->status);
                    } else {
                        $qr->where('status', $request->status);
                    }
                }
                if ($request->occasion) {
                    $qr->with('event')
                        ->whereHas('event', function ($q) use ($request) {
                            $q->where('vendor_events.occasion_id', $request->occasion);
                        });
                }
                if ($request->q) {
                    $qr->with('event')
                        ->whereHas('event', function ($q) use ($request) {
                            $q->where('vendor_events.event_name', 'LIKE', "%{$request->q}%");
                        });
                }
            });
        }

        return view('vendor.proposals.all_proposals')->with([
            'page_title' => $request->status ? $request->status . ' Proposals' : 'Proposals',
            'proposals' => $query->get(),
            'role' => $this->user->roles[0]['name'],
            'request' => $request
        ]);
    }

    public function allContracts()
    {
        $request = request();
        if ($request->status) {
            $contracts = VendorContract::where([['party1_id', $this->user->id], ['status', $request->status]])
                ->orWhere([['party2_id', $this->user->id], ['status', $request->status]])->get();
        } else {
            $contracts = VendorContract::where([['party1_id', $this->user->id]])
                ->orWhere([['party2_id', $this->user->id]])->get();
        }

        return view('vendor.contracts.all_contracts')->with([
            'page_title' => $request->status ? $request->status . ' Contracts' : 'Contracts',
            'contracts' => $contracts,
            'role' => $this->user->roles[0]['name']
        ]);
    }

    public function allInvoices()
    {
        $request = request();
        $query = new VendorInvoice();
        for ($i = 1; $i <= 2; $i++) {
            $where = $i == 1 ? 'where' : 'orWhere';
            $query = $query->$where(function ($qr) use ($request, $i) {
                $qr->where('party' . $i . '_id', $this->user->id);
                if ($request->status) {
                    if ($request->status == 'Paid') {
                        $qr->where('payment_status', $request->status);
                    } else {
                        $qr->where('status', $request->status);
                    }
                }
                if ($request->occasion) {
                    $qr->with('event')
                        ->whereHas('event', function ($q) use ($request) {
                            $q->where('vendor_events.occasion_id', $request->occasion);
                        });
                }
                if ($request->q) {
                    $qr->with('event')
                        ->whereHas('event', function ($q) use ($request) {
                            $q->where('vendor_events.event_name', 'LIKE', "%{$request->q}%");
                        });
                }
            });
        }

        if($this->user->hasRole('vendor'))
        {
            $query_ =  VendorInvoice::where('party1_id', $this->user->id);
        }else
        {
            $query_ =  VendorInvoice::where('party2_id', $this->user->id);
        }

        $query_total = clone $query;
        $query_paid = clone $query;
        $query_pending = clone $query;
        $query_pending_ = clone $query;
        $query_rejected_ = clone $query;

        $invoice_count['total'] = $query_total->where('status','!=', 'Rejected')->sum('total_payable');
        $invoice_count['paid'] = $query_paid->where('status', 'Accepted')->where('payment_status', 'Paid')->sum('total_payable');
        $invoice_count['pending'] = $query_pending->where('status', 'Accepted')->where('payment_status', 'Un Paid')->sum('total_payable') + $query_pending_->where('status', 'Pending')->sum('total_payable');
        $invoice_count['rejected'] = $query_rejected_->where('status', 'Rejected')->sum('total_payable');

        return view('vendor.invoices.all_invoices')->with([
            'page_title' => $request->status ? $request->status . ' Invoices' : 'Invoices',
            'invoices' => $query->get(),
            'role' => $this->user->roles[0]['name'],
            'invoice_count' => $invoice_count,
            'request' => $request
        ]);
    }

    public function financialTool()
    {
        if($this->user->hasRole('vendor'))
        {
            $query =  VendorInvoice::where('party1_id', $this->user->id);
        }else
        {
            $query =  VendorInvoice::where('party2_id', $this->user->id);
        }

        $query_total = clone $query;
        $query_paid = clone $query;
        $query_pending = clone $query;
        $query_pending_ = clone $query;

        $invoice_count['total'] = $query_total->where('status', '!=','Rejected')->sum('total_payable');
        $invoice_count['paid'] = $query_paid->where('status', 'Accepted')->where('payment_status','=','Paid')->sum('total_payable');
        $invoice_count['pending'] = $query_pending->where('status', 'Accepted')->where('payment_status', '=','Un Paid')->sum('total_payable')
        + $query_pending_->where('status', 'Pending')->sum('total_payable');

        $months = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
        $monthly_report = [];
        $monthly_report_ = [];
        $monthly_report__ = [];
        $monthly_report___ = [];
        foreach ($months as $key => $value) {
            $query_ = clone  $query;
            array_push($monthly_report_, $query_->whereMonth('date', $value)->where('status', '!=','Rejected')->sum('total_payable'));
            $query__ = clone  $query;
            array_push($monthly_report__, $query__->whereMonth('date', $value)->where('payment_status', 'Paid')->sum('total_payable'));
            $query___ = clone  $query;
            $query____ = clone  $query;
            array_push($monthly_report___, $query___->whereMonth('date', $value)->where('payment_status', 'Un Paid')->sum('total_payable') + $query____->whereMonth('date', $value)->where('status', 'Pending')->sum('total_payable'));
        }
        $monthly_report['total'] = $monthly_report_;
        $monthly_report['paid'] = $monthly_report__;
        $monthly_report['unpaid'] = $monthly_report___;




        $profit_report_query = FinancialCost::where('vendor_id', $this->user->id);
        $profit_monthly_report = [];
        $profit_monthly_report_ = [];
        $profit_monthly_report__ = [];
        $profit_monthly_report___ = [];
        foreach ($months as $key => $value) {
            $profit_query_ = clone  $profit_report_query ;
            array_push($profit_monthly_report_, $profit_query_->whereMonth('date', $value)->sum('actual_cost'));
            $profit_query__ = clone  $profit_report_query ;
            array_push($profit_monthly_report__, $profit_query__->whereMonth('date', $value)->sum('invoice_amount'));
            array_push($profit_monthly_report___, ($profit_monthly_report__[$key] - $profit_monthly_report_[$key]));
        }
        $profit_monthly_report['actual_cost'] = $profit_monthly_report_;
        $profit_monthly_report['invoice_amount'] = $profit_monthly_report__;
        $profit_monthly_report['profit'] = $profit_monthly_report___;
        // dd($profit_monthly_report);
        return view('vendor.financialTool.index')->with([
            'page_title' => 'Financial Tool',
            'invoice_count' => $invoice_count,
            'financial_report' => json_encode($monthly_report),
            'profit_report' => json_encode($profit_monthly_report)
        ]);
    }
    public function createFinancialTool()
    {
        return view('vendor.financialTool.cost')->with([
            'page_title' => 'Create Financial Tool'
        ]);
    }
}
