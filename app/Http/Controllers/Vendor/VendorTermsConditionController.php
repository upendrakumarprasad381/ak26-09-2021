<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\Models\Vendor\VendorTermsCondition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VendorTermsConditionController extends Controller
{
    private $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->parent_id != null?Auth::user()->parent:Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('vendor.masterSettings.termsCondition.index')->with([
            'rows' => VendorTermsCondition::where('user_id',$this->user->id)->get(),
            'page_title' => 'Terms & Conditions'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendor.masterSettings.termsCondition.create')->with([
            'page_title' => 'Terms & Conditions'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'terms_and_condition' => 'required',
            'type' => 'required',
            'name' => 'required'
        ]);
        $terms = new VendorTermsCondition();
        $terms->user_id = $this->user->id;
        $terms->name = $request->name;
        $terms->text = $request->terms_and_condition;
        $terms->type = $request->type;
        $terms->default = $request->default;
        $terms->save();
        return response()->json(array('status' => true, 'message' => 'Terms & Conditions Is Saved.'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor\VendorTermsCondition  $vendorTermsCondition
     * @return \Illuminate\Http\Response
     */
    public function show(VendorTermsCondition $vendorTermsCondition)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor\VendorTermsCondition  $vendorTermsCondition
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendorTermsCondition = VendorTermsCondition::find($id);
        return view('vendor.masterSettings.termsCondition.create')->with([
            'page_title' => 'Terms & Conditions',
            'editable' => $vendorTermsCondition
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor\VendorTermsCondition  $vendorTermsCondition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $terms = VendorTermsCondition::find($id);
        $request->validate([
            'terms_and_condition' => 'required',
            'name' => 'required'
        ]);
        $terms->text = $request->terms_and_condition;
        $terms->name = $request->name;
        $terms->type = $request->type;
        $terms->default = $request->default;
        $terms->save();
        return response()->json(array('status' => true, 'message' => 'Terms & Conditions Is Saved.'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor\VendorTermsCondition  $vendorTermsCondition
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $terms = VendorTermsCondition::find($id);
        $terms->delete();
    }

    public function getTC()
    {
        $request = request();
        $tc = VendorTermsCondition::where('user_id',$this->user->id)->where('type',$request->type)->get();
        return $tc;
    }
}
