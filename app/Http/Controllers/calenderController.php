<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use Redirect;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class calenderController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index($eventId = '') {
        // \App\Helpers\CommonHelper::sendWhatsappMessage($mobile_no='+971529347678', $message='igone this message');

        return view('calender.index')->with([
                    'eventId' => $eventId,
                    'page_title' => 'Calender',
        ]);
    }

    public function settings() {
        if (isset($_POST['submitcalender-settings'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json['updated_at'] = date('Y-m-d H:i:s');
            \App\Database::updates('vendor_details', $json, ['user_id' => Auth::user()->id]);
            return Redirect::to(url("/calender"));
        }
        return view('calender.settings')->with([
                    'page_title' => 'Calender Settings',
        ]);
    }

    public function eventList($eventId = '') {
        $ar = [];
        $cond = "";
        $start = !empty($_REQUEST['start']) ? date('Y-m-d', strtotime($_REQUEST['start'])) : '';
        $end = !empty($_REQUEST['end']) ? date('Y-m-d', strtotime($_REQUEST['end'])) : '';

        if (!empty($eventId)) {
            $cond = $cond . " AND (event_id=$eventId || event_id='0')";
        }
        if (!empty($start) && !empty($end)) {
            $cond = $cond . " AND event_date BETWEEN '$start' AND '$end'";
        }
        $cond = $cond . " AND created_by='" . Auth::user()->id . "'";

        $Sql = "SELECT title,event_date AS start,event_date AS end,event_id,primary_id,type FROM `calender_events` WHERE 1 $cond";
        $data = \App\Database::select($Sql);
        for ($i = 0; $i < count($data); $i++) {
            $d = $data[$i];
            if ($d->type == 'TODO_LIST') {
                $d->url = url("to-do-list/create/$d->primary_id");
            } else if ($d->type == 'CHECK_LIST') {
                $d->url = url("checklist/$d->event_id");
            }
        }
        die(json_encode($data));
    }

    public function googleUpdatelist() {

        $join = " LEFT JOIN vendor_events VE ON VE.id=M.event_id ";
        $endDate = "UNION ALL SELECT id,event_id,item_name,1 AS dateType, end AS date,google_id_end AS googleEventId FROM `original_checklist` WHERE google_update_end=0 AND vendor_id='" . Auth::user()->id . "'";
        $Sql = "SELECT M.* FROM (SELECT id,event_id,item_name,0 AS dateType, start AS date,google_id_start AS googleEventId FROM `original_checklist` WHERE google_update_start=0 AND vendor_id='" . Auth::user()->id . "' $endDate ) AS M $join WHERE 1 LIMIT 1";
        $Sql = "SELECT * FROM `calender_events` WHERE google_update=0 AND created_by='" . Auth::user()->id . "' LIMIT 1";
        $data = \App\Database::selectSingle($Sql);
        if (empty($data)) {
            die(json_encode(array('status' => false, 'No check list found.')));
        }

        $Event = array(
            'summary' => $data->title,
            'description' => $data->title,
            'start' => array(
                'date' => $data->event_date,
            ),
            'end' => array(
                'date' => $data->event_date,
            ),
        );
        $list = array(
            'primaryId' => $data->id,
            'googleEventId' => $data->google_event_id
        );
       
        die(json_encode(array('status' => true, 'data' => $list, 'Event' => $Event, 'message' => 'updated successfully')));
    }

    public function googleUpdateEventId() {
        $primaryId = !empty($_POST['primaryId']) ? $_POST['primaryId'] : '';
        $googleEventId = !empty($_POST['googleEventId']) ? $_POST['googleEventId'] : '';
   
        if (!empty($primaryId)) {
            $update['updated_at'] = date('Y-m-d H:i:s');
            $update['google_update'] = '1';
            $update['google_event_id'] = $googleEventId;
            \App\Database::updates('calender_events', $update, ['id' => $primaryId]);
        }
        
        die(json_encode(array('status' => true, 'data' => false, 'Event' => false, 'message' => 'updated successfully')));
    }

}
