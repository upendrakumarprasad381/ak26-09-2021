<?php

namespace App\Http\Controllers;

use App\Models\Admin\Master\Occasion;
use Illuminate\Http\Request;
Use Redirect;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Vendor\VendorEvent;
use DateTime;
use DB;

class checklistController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index($eventId = '', $filterId = '') {
        return view('checklist.index')->with([
                    'eventId' => $eventId,
                    'filterId' => $filterId,
                    'page_title' => 'Checklist',
        ]);
    }

    public function originalchecklist($eventId = '') {
        if (isset($_POST['original-checklist'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json = array_values($json);

            for ($i = 0; $i < count($json); $i++) {
                $d = $json[$i];
                $d['start'] = date('Y-m-d', strtotime($d['start']));
                $d['end'] = date('Y-m-d', strtotime($d['end']));
                $d['event_id'] = $eventId;
                $d['updated_at'] = date('Y-m-d H:i:s');
                $d['created_at'] = $d['updated_at'];
                $d['vendor_id'] = Auth::user()->id;
                $d['role_id'] = Auth::user()->roles()->first()->id;
                $primaryId = \App\Database::insert('original_checklist', $d);

                $calender_id_start = \App\Helpers\CommonHelper::updateMycalender($calenderId = NULL, $type = 'CHECK_LIST', $eventId, $primaryId, $title = $d['item_name'], $eventDate = $d['start']);
                $calender_id_end = \App\Helpers\CommonHelper::updateMycalender($calenderId = NULL, $type = 'CHECK_LIST', $eventId, $primaryId, $title = $d['item_name'], $eventDate = $d['end']);
                \App\Database::updates('original_checklist', ['calender_id_start' => $calender_id_start, 'calender_id_end' => $calender_id_end], ['id' => $primaryId]);
            }
            return Redirect::to(url("/checklist/$eventId"));
        }
        return view('checklist.originalchecklist')->with([
                    'eventId' => $eventId,
                    'page_title' => 'Original Checklist',
        ]);
    }

    public function customizedchecklist($eventId = '') {
        if (isset($_POST['json'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json = array_values($json);


            for ($i = 0; $i < count($json); $i++) {
                $d = $json[$i];
                $d['start'] = date('Y-m-d', strtotime($d['start']));
                $d['end'] = date('Y-m-d', strtotime($d['end']));

                $datetime1 = new DateTime($d['start']);
                $datetime2 = new DateTime($d['end']);
                $interval = $datetime1->diff($datetime2);
                $workingDays = $interval->format('%a');
                $d['no_of_days'] = $workingDays + 1;
                $d['event_id'] = $eventId;
                $d['is_add'] = '1';
                $d['updated_at'] = date('Y-m-d H:i:s');
                $d['created_at'] = $d['updated_at'];
                $d['vendor_id'] = Auth::user()->id;
                $d['role_id'] = Auth::user()->roles()->first()->id;
                $primaryId = \App\Database::insert('original_checklist', $d);

                $calender_id_start = \App\Helpers\CommonHelper::updateMycalender($calenderId = NULL, $type = 'CHECK_LIST', $eventId, $primaryId, $title = $d['item_name'], $eventDate = $d['start']);
                $calender_id_end = \App\Helpers\CommonHelper::updateMycalender($calenderId = NULL, $type = 'CHECK_LIST', $eventId, $primaryId, $title = $d['item_name'], $eventDate = $d['end']);
                \App\Database::updates('original_checklist', ['calender_id_start' => $calender_id_start, 'calender_id_end' => $calender_id_end], ['id' => $primaryId]);
            }
            die(json_encode(array('status' => true, 'updated successfully')));
            // return Redirect::to(url("/checklist/$eventId"));
        }
        // if($eventId)
        // {
        //     $occasions = Occasion::all();
        // }
        return view('checklist.customizedchecklist')->with([
                    'eventId' => $eventId,
                    'page_title' => 'Customized Checklist',
                    'occasions' => Occasion::all()
        ]);
    }

    public function getChecklists($occasion_id)
    {
        return DB::table('original_checklist_master')->where('occasion_id',$occasion_id)->get();
    }

}
