<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use Redirect;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Helpers\LibHelper;
use Config;

class createWebPageController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index($eventId = '') {
        if (isset($_POST['type'])) {
           
            $themeId = !empty($_POST['themeId']) ? $_POST['themeId'] : '';
            $Sql = "SELECT * FROM `web_page_design_event` WHERE event_id='$eventId'";
            $data = \App\Database::select($Sql);
            $add = array(
                'event_id' => $eventId,
                'web_page_id' => $themeId,
                'slug' => !empty($_POST['slug']) ? $_POST['slug'] : '',
                'updated_at' => date('Y-m-d H:i:s'),
            );
            if (empty($data)) {
                $add['created_at'] = $add['updated_at'];
                \App\Database::insert('web_page_design_event', $add);
            } else {
                \App\Database::updates('web_page_design_event', $add, ['event_id' => $eventId]);
            }
            return response()->json(['type' => 'success', 'message' => 'Selected successfully.']);
        }
        return view('createWebPage.index')->with([
                    'eventId' => $eventId,
              'page_title' => 'Web Pages',
        ]);
    }

    public function webPagesettings($eventId = '', $webpageId = '') {
        if (isset($_POST['uploadImage'])) {
            $json['images'] = '';
            $id="";
            $HOME_DIR = Config::get('constants.HOME_DIR');
            $form = !empty($_FILES['uploadimg']['name']) ? $_FILES['uploadimg'] : '';
            if (!empty($form)) {
                $FileName = uniqid() . '.' . pathinfo($form['name'], PATHINFO_EXTENSION);
                if (move_uploaded_file($form['tmp_name'], $HOME_DIR . 'web-pages/images/' . $FileName)) {
                    $json['created_at'] = date('Y-m-d H:i:s');
                    $json['images'] = $FileName;
                    $json['event_id'] = $eventId;
                   $id= \App\Database::insert('web_page_design_event_images', $json);
                }
            }
            return response()->json(['type' => 'success', 'message' => 'Update successfully.','pId'=>$id, 'fullpath' => $HOME_DIR.('web-pages/images/' .$json['images']),'src' => url('web-pages/images/' .$json['images'])]);
        }
        if (isset($_POST['removeImage'])) {
            $fullpath = !empty($_POST['fullpath']) ? $_POST['fullpath'] : '';
            $removeImage = !empty($_POST['removeImage']) ? $_POST['removeImage'] : '';
            \App\Database::deletes('web_page_design_event_images', ['id' => $removeImage]);
            if (is_file($fullpath)) {
                unlink($fullpath);
            }
            return response()->json(['type' => 'success', 'message' => 'Update successfully.']);
        }
        if (isset($_POST['json'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json['list']=!empty($json['list']) && is_array($json['list']) ? json_encode($json['list']) : '';
           
            $webpage = LibHelper::GetwebpagedesigneventId($eventId);
            $HOME_DIR = Config::get('constants.HOME_DIR');
            $form = !empty($_FILES['logo']['name']) ? $_FILES['logo'] : '';
            if (!empty($form)) {
                $FileName = uniqid() . '.' . pathinfo($form['name'], PATHINFO_EXTENSION);
                if (move_uploaded_file($form['tmp_name'], $HOME_DIR . 'web-pages/images/' . $FileName)) {
                    $oldImg = $HOME_DIR . 'web-pages/images/' . $webpage->logo;
                    if (is_file($oldImg)) {
                        unlink($oldImg);
                    }
                    $json['logo'] = $FileName;
                }
            }
            $json['updated_at'] = date('Y-m-d H:i:s');
            \App\Database::updates('web_page_design_event', $json, ['event_id' => $eventId]);
            return response()->json(['type' => 'success', 'message' => 'Update successfully.']);
        }
        return view('createWebPage.webPageSettings')->with([
                    'eventId' => $eventId,
                    'webpageId' => $webpageId,
              'page_title' => 'Web Pages Settings',
        ]);
    }

    public function webPagepreview($eventId = '') {
        $Sql = "SELECT E.event_id,D.layout_file_name FROM `web_page_design_event` E LEFT JOIN web_page_design D ON D.id=E.web_page_id WHERE E.slug='$eventId'";
        $data = \App\Database::selectSingle($Sql);
        if (empty($data)) {
            return Redirect::to(url('/'));
        }
        return view("createWebPage.layout.$data->layout_file_name")->with([
                    'eventId' => $data->event_id
        ]);
    }

}
