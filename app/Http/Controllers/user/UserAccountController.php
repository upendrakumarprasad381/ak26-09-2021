<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmailJob;
use App\Models\User;
use App\Notifications\UserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Notification;

class UserAccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the personalInformation page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function personalInformation()
    {
        return view('user.accountSettings.personal_information');
    }

    /**
     * Show the passwordChange page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function changePassword()
    {
        return view('user.accountSettings.change_password');
    }

     /**
     * Save user informations.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function updateUserInformation(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);
        $user = User::find(auth()->user()->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        $data['status'] = true;
        $data['message'] = 'User informations updated.';

        return $data;
    }

    /**
     * Update user password.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function updateUserPassword(Request $request)
    {
        $request->validate([
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $user = User::find(auth()->user()->id);
        $user->password = Hash::make($request->password);
        $user->save();

        $data['status'] = true;
        $data['message'] = 'User password updated.';

        $details_['email'] = $user->email;
        $details_['subject'] = 'Ask Deema Password Changed';
        $details_['file'] = 'emails.email';
        $details_['name'] = $user->first_name . ' ' . $user->last_name;
        $details_['message_html'] = '<p>Success</p>
        <p>Your password changed successfully</p>
        <p>Ask Deema</p>';
        dispatch(new SendEmailJob($details_));

        $notification['text'] = 'Your password changed successfully.';
        $notification['link'] = null; //if no link given Null
        Notification::send(User::find($user->id), new UserNotification($notification));

        return $data;
    }
}
