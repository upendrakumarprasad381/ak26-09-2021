<?php

namespace App\Http\Requests\Admin\Web;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id =  request()->blog_id ? request()->blog_id: '';
        return [
            'title' => 'required',
            'description' => 'required',
            'content' => 'required',
            'image' => 'required',
            'category' => 'required',
            'slug' => 'required|unique:blogs,slug,'.$id
        ];
    }
}
