<?php

namespace App\Http\Requests\Vendor;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class VendorContractsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user()->parent_id != null ? Auth::user()->parent : Auth::user();
        if ($this->party1_id == $user->id) {
            return [
                'signature_1_esign' => $this->type == 'Digital' && $this->old_signature_1 == null ? 'required' : '',
                'signature_1' => $this->type != 'Digital' && $this->old_signature_1 == null ? 'required' : '',
                'event_id' => 'required',
                'proposal_id' => 'required',
                'party1_id' => 'required',
                'party2_id' => 'required',
                'date' => 'required',
                'items.*' => 'required'
            ];
        } else {
            return [
                'signature_2_esign' => $this->type == 'Digital' && $this->old_signature_2 == null ? 'required' : '',
                'signature_2' => $this->type != 'Digital' && $this->old_signature_2 == null ? 'required' : '',
                'event_id' => 'required',
                'proposal_id' => 'required',
                'party1_id' => 'required',
                'party2_id' => 'required',
                'date' => 'required',
                'items.*' => 'required'
            ];
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'signature_1_esign.required' => 'Signature is required',
            'signature_1.required' => 'Signature is required',
        ];
    }
}
