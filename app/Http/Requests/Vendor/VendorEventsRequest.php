<?php

namespace App\Http\Requests\Vendor;

use Illuminate\Foundation\Http\FormRequest;

class VendorEventsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'event_name' => 'required|min:4',
            'type_of_event' => 'required',
            'event_date' => 'required',
            'number_of_attendees' => 'required | numeric',
            'occasion' => 'required',
            'categories.*' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
        'occasion.required' => 'The type of event field is required.',
        'categories.*.required' => 'The categories field is required'
        ];
    }
}
