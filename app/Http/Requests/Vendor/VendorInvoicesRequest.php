<?php

namespace App\Http\Requests\Vendor;

use Illuminate\Foundation\Http\FormRequest;

class VendorInvoicesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'signature_1_esign' => $this->type == 'Digital' ? 'required' : '',
            'signature_1' => $this->type != 'Digital' ? 'required' : '',
            'event_id' => 'required',
            'contract_id' => 'required',
            'party2_id' => 'required',
            'date' => 'required',
            'items.*' => 'required',
            'payment_amount' => 'required|numeric|gt:0'
        ];
    }
}
