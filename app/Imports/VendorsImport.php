<?php

namespace App\Imports;

use App\Models\Admin\Master\Category;
use App\Models\User;
use App\Models\Vendor\VendorDetail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;

class VendorsImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $type = 'Vendor';
        $category = 0;
        $user = '';
        if ($row[1] != 'Category') {
            if ($row[1] == 'Event Planners') {
                $type = 'Event Planner';
            } else {
                $type = 'Vendor';
                $exist_category = Category::where('type', 'Vendor')->where('name', $row[1])->first();
                if ($exist_category) {
                    $category = $exist_category->id;
                } else {
                    $new_category = new Category();
                    $new_category->name = $row[1];
                    $new_category->slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $row[1]);
                    $new_category->save();
                    $category = $new_category->id;
                }
            }
            if ($row[6] != '') {
                if (!User::where('email', $row[6])->first()) {

                    $user = new User();
                    $user->first_name = $row[3];
                    $user->email = $row[6];
                    $user->phone = $row[8];
                    $user->status = 0;
                    $user->phone_office = $row[9];
                    $user->password = Hash::make('Askdeema##123');
                    $user->save();

                    $user->attachRole($type=='Vendor'?2:3);

                    VendorDetail::insert([
                        'user_id' => $user->id,
                        'role_id' => $type=='Vendor'?2:3,
                        'emirates' => $row[11]?:'',
                        'business_website' => $row[4]?:'',
                        'social_media_link' => $row[4]?:'',
                        'address' => '',
                        'focus' => $row[2],
                        'instagram' => $row[5],
                        'email_2' => $row[7],
                        'emirates_availability' => $row[10],
                        'city' => $row[12],
                        'country' => $row[13],
                        'cuisine_focus' => $row[14],
                        'menu_type' => $row[15],
                        'experience_focus' => $row[16],
                        'capacity' => $row[18],
                        'updated_at' => date('Y-m-d')
                    ]);
                    DB::table('vendor_category')->insert(array(
                        'category_id' => $category,
                        'user_id' => $user->id,
                        'updated_at' => date('Y-m-d')
                    ));

                    if($row[0])
                    {
                        $array = explode(',',$row[0]);
                        foreach ($array as $key => $value) {
                           DB::table('vendor_images')->insert(array(
                               'user_id' => $user->id,
                               'image' => 'vendor/images/'.$value
                           ));
                        }
                    }


                    return $user;
                }
            }
        }
    }
}
