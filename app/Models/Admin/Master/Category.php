<?php

namespace App\Models\Admin\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory;
    public function parent()
    {
        return $this->belongsTo(Category::class,'parent_id');
    }
    public function childs()
    {
        return $this->hasMany(Category::class,'parent_id');
    }
    public function items()
    {
        return $this->hasMany(VendorItem::class,'category_id');
    }
    public function budget(int $occasion_id)
    {
        return $this->hasOne(BudgetSettings::class, 'category_id')->where('occasion_id', $occasion_id)->first();
    }
    public function filters()
    {
        return $this->hasMany(CategoryFilter::class,'category_id');
    }
}
