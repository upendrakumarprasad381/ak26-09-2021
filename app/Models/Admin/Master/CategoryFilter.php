<?php

namespace App\Models\Admin\Master;

use App\Models\Vendor\VendorFilterValue;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryFilter extends Model
{
    use HasFactory;
    public function filter()
    {
        return $this->belongsTo(VendorFilter::class,'filter_id');
    }
}
