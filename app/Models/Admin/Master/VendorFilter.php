<?php

namespace App\Models\Admin\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorFilter extends Model
{
    use HasFactory;
    public function items()
    {
        return $this->hasMany(VendorFilterItem::class,'vendor_filter_id');
    }
}
