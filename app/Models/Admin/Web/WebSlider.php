<?php

namespace App\Models\Admin\Web;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebSlider extends Model
{
    use HasFactory;
}
