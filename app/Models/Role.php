<?php

namespace App\Models;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    public $guarded = [];
    const ADMIN_ROLE = 1;
    const MEMBER_ROLE = 2;

    const MEMBER_ROLE_NAME = 'Member';
    const ADMIN_ROLE_NAME = 'Admin';
}
