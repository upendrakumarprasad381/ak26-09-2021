<?php

namespace App\Models\Vendor;

use App\Models\Admin\Master\Category;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerRequest extends Model
{
    use HasFactory;
    protected $table="customer_request";
    protected $fillable =['status'];

    public function event()
    {
        return $this->belongsTo(VendorEvent::class,'event_id');
    }

    public function categories()
    {
        return $this->hasMany(CustomerRequestCategory::class,'request_id');
    }

    public function categoriesList()
    {
        return $this->belongsToMany(Category::class,'customer_request_category','request_id','category_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function additionalServices()
    {
        return $this->hasMany(RequestAdditionalService::class,'request_id');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Models\User','vendor_id');
    }
    public function suggested_to()
    {
        return $this->belongsTo('App\Models\User','suggest_to');
    }
}
