<?php

namespace App\Models\Vendor;

use App\Models\Admin\Master\Category;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerRequestCategory extends Model
{
    use HasFactory;
    protected $table="customer_request_category";
    public function category()
    {
        return $this->belongsTo(Category::class,'category_id');
    }
}
