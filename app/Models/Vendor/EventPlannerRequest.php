<?php

namespace App\Models\Vendor;

use App\Models\Admin\Master\Category;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventPlannerRequest extends Model
{
    use HasFactory;
    protected $table="event_planner_request";
    public function event()
    {
        return $this->belongsTo(VendorEvent::class,'event_id');
    }
    public function category()
    {
        return $this->belongsTo(Category::class,'category_id');
    }
    public function proposal()
    {
        return $this->hasOne(VendorProposal::class,'request_id');
    }
    public function event_planner()
    {
        return $this->belongsTo('App\Models\User','event_planner_id');
    }
    public function vendor()
    {
        return $this->belongsTo('App\Models\User','vendor_id');
    }
    public function suggested_to()
    {
        return $this->belongsTo('App\Models\User','suggest_to');
    }


}
