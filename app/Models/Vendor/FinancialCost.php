<?php

namespace App\Models\Vendor;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialCost extends Model
{
    use HasFactory;
    public function event()
    {
        return $this->belongsTo(VendorEvent::class, 'event_id');
    }

    public function items()
    {
        return $this->hasMany(FinancialCostItem::class, 'cost_id');
    }
}
