<?php

namespace App\Models\Vendor;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestAdditionalService extends Model
{
    use HasFactory;
    protected $table="request_additional_services";
}
