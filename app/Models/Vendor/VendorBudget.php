<?php

namespace App\Models\Vendor;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorBudget extends Model
{
    use HasFactory;
    use SoftDeletes;
    public function items()
    {
        return $this->hasMany(VendorBudgetItem::class);
    }
    public function event()
    {
        return $this->belongsTo(VendorEvent::class);
    }
}
