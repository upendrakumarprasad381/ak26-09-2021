<?php

namespace App\Models\Vendor;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class VendorContract extends Model
{
    use HasFactory;
    use SoftDeletes;
    public function items()
    {
        return $this->hasMany(VendorContractItems::class,'contract_id');
    }

    public function event()
    {
        return $this->belongsTo(VendorEvent::class,'event_id');
    }

    public function request()
    {
        return $this->belongsTo(EventPlannerRequest::class,'request_id');
    }

    public function proposal()
    {
        return $this->belongsTo(VendorProposal::class,'proposal_id');
    }

    public function party1()
    {
        return $this->belongsTo('App\Models\User','party1_id');
    }

    public function party2()
    {
        return $this->belongsTo('App\Models\User','party2_id');
    }

    public function invoice_payments()
    {
        $payment = VendorInvoice::where('contract_id',$this->id)->select(DB::raw("coalesce(SUM(total_payable),0.00) as paid"))->where('payment_status','!=','Rejected')->first();
        return $payment;
    }

}
