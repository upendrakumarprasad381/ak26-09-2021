<?php

namespace App\Models\Vendor;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorContractItems extends Model
{
    use HasFactory;
    public function category()
    {
        return $this->belongsTo('App\Models\Admin\Master\Category','service_type_id');
    }
}
