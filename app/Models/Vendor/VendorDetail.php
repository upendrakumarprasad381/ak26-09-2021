<?php

namespace App\Models\Vendor;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorDetail extends Model
{
    use HasFactory;
    public function country()
    {
        return $this->belongsTo('App\Models\Country','country');
    }
    public function city()
    {
        return $this->belongsTo('App\Models\City','city');
    }
}
