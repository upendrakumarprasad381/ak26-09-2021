<?php

namespace App\Models\Vendor;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorEmirate extends Model
{
    use HasFactory;
    protected $table = 'vendor_emirate';
}
