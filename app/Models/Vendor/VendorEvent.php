<?php

namespace App\Models\Vendor;

use App\Models\Admin\Master\Category;
use App\Models\Admin\Master\Occasion;
use App\Models\Permission;
use Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class VendorEvent extends Model
{
    use SoftDeletes;
    use HasFactory;

    public function categories()
    {
        return $this->hasMany(VendorEventCategory::class,'vendor_event_id');
    }

    public function proposals()
    {
        return $this->hasMany(VendorProposal::class,'event_id','id');
    }

    public function categoriesModel()
    {
        return $this->belongsToMany(Category::class,'vendor_event_category','vendor_event_id','category_id');
    }

    public function occasions()
    {
        return $this->hasMany(VendorOccasion::class,'occasion_id');
    }

    public function occasionsModel()
    {
        return $this->belongsToMany(Occasion::class,'vendor_occasions','user_id','occasion_id');
    }

    public function eventPlannerRFQ()
    {
        return $this->hasMany(EventPlannerRequest::class,'event_id');
    }

    public function customerRFQ()
    {
        return $this->hasMany(CustomerRequest::class,'event_id');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Models\User','vendor_id');
    }
    public function customer()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function occasion()
    {
        return $this->belongsTo('App\Models\Admin\Master\Occasion','occasion_id');
    }

    public function additionalServices()
    {
        return $this->hasMany(RequestAdditionalService::class,'request_id');
    }

    public function counts()
    {
        $user = Auth::user()->parent_id != null?Auth::user()->parent:Auth::user();
        $proposals = VendorProposal::where([['party1_id', $user->id], ['event_id', $this->id]])
        ->orWhere([['party2_id', $user->id], ['event_id', $this->id]])->count('id');
        $invoices = VendorInvoice::where([['party1_id', $user->id], ['event_id', $this->id]])
        ->orWhere([['party2_id', $user->id], ['event_id', $this->id]])->count('id');
        $contracts = VendorContract::where([['party1_id', $user->id], ['event_id', $this->id]])
        ->orWhere([['party2_id', $user->id], ['event_id', $this->id]])->count('id');
        $ep_requests = EventPlannerRequest::where([['event_planner_id', $user->id], ['event_id', $this->id]])
        ->count('id');
        return ['proposals' => $proposals, 'invoices' => $invoices,'contracts' => $contracts,'ep_requests' => $ep_requests];
    }





}
