<?php

namespace App\Models\Vendor;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorEventCategory extends Model
{
    use HasFactory;
    protected $table = "vendor_event_category";
    public function category()
    {
        return $this->belongsTo('App\Models\Admin\Master\Category','category_id');
    }
}
