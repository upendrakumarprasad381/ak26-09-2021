<?php

namespace App\Models\Vendor;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorInvoice extends Model
{
    use HasFactory;
    use SoftDeletes;
    public function items()
    {
        return $this->hasMany(VendorInvoiceItem::class,'invoice_id');
    }

    public function event()
    {
        return $this->belongsTo(VendorEvent::class,'event_id');
    }

    public function contract()
    {
        return $this->belongsTo(VendorContract::class,'contract_id');
    }

    public function party1()
    {
        return $this->belongsTo('App\Models\User','party1_id');
    }

    public function party2()
    {
        return $this->belongsTo('App\Models\User','party2_id');
    }
}
