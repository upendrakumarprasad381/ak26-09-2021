<?php

namespace App\Models\Vendor;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorInvoiceItem extends Model
{
    use HasFactory;
    use SoftDeletes;
    public function category()
    {
        return $this->belongsTo('App\Models\Admin\Master\Category','service_type_id');
    }
}
