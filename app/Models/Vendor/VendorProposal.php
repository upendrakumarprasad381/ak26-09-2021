<?php

namespace App\Models\Vendor;

use App\Models\Admin\Master\Category;
use App\Models\ProposalTermsNegotiaion;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorProposal extends Model
{
    use HasFactory;
    use SoftDeletes;
    public function items()
    {
        return $this->hasMany(VendorProposalItem::class,'proposal_id');
    }

    public function event()
    {
        return $this->belongsTo(VendorEvent::class,'event_id');
    }

    public function request()
    {
        return $this->belongsTo(EventPlannerRequest::class,'request_id');
    }

    public function party1()
    {
        return $this->belongsTo('App\Models\User','party1_id');
    }

    public function party2()
    {
        return $this->belongsTo('App\Models\User','party2_id');
    }

    public function terms_negotiations()
    {
        return $this->hasMany(ProposalTermsNegotiaion::class,'proposal_id')->where('status',1);
    }
}
