<?php

namespace App\Models\Vendor;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorProposalItem extends Model
{
    use HasFactory;
    public function category()
    {
        return $this->belongsTo('App\Models\Admin\Master\Category','service_type_id');
    }
    public function subCategory()
    {
        return $this->belongsTo('App\Models\Admin\Master\Category','sub_service_id');
    }
    public function negotiations()
    {
        return $this->hasMany(VendorProposalItemNegotiation::class,'item_id')->where('status',1);
    }
}
