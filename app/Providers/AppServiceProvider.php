<?php

namespace App\Providers;

use Auth;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //compose all the views....
        view()->composer('*', function ($view) {
            if (Auth::user()) {
                $url = str_replace('_', '-', Auth::user()->roles[0]->name);
                $notifications = Auth::user()->unreadNotifications;
            } else {
                $url = '';
                $notifications = [];
            }
            $view->with('url', $url);
            $view->with('notifications', $notifications);
        });
        Schema::defaultStringLength(191);
    }
}
