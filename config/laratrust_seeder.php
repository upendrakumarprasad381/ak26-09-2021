<?php

return [
    /**
     * Control if the seeder should create a user per role while seeding the data.
     */
    'create_users' => true,

    /**
     * Control if all the laratrust tables should be truncated before running the seeder.
     */
    'truncate_tables' => true,

    'roles_structure' => [
        'superadministrator' => [
            'users' => 'c,r,u,d'
        ],
        'vendor' => [
            'users' => 'c,r,u,d'
        ],
        'event_planner' => [
            'users' => 'c,r,u,d'
        ],
        'shop_admin' => [
            'users' => 'c,r,u,d'
        ],
        'user' => [
            'users' => 'c,r,u,d'
        ]
    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
