<?php
// Aside menu
return [
    'items' => [
        // Dashboard
        [
            'title' => 'Home',
            'root' => true,
            'icon' => 'img/menu/home.svg', // or can be 'flaticon-home' or any flaticon-*
            'page' => '/dashboard',
        ],
        // Super Admin Menu
        // [
        //     'section' => 'Vendors Management',
        //     'role' => 'superadministrator'
        // ],
        [
            'title' => 'Vendors Management',
            'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'superadministrator',
            'submenu' => [
                [
                    'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                    'title' => 'Event Planners',
                    'role' => 'superadministrator',
                    'root' => true,
                    'page' => 'admin/event-planners',
                ],
                [
                    'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                    'title' => 'Vendors',
                    'role' => 'superadministrator',
                    'root' => true,
                    'page' => 'admin/vendors',
                ],
                [
                    'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                    'title' => 'Import Vendors',
                    'role' => 'superadministrator',
                    'root' => true,
                    'page' => 'admin/import-vendors',
                ],
            ]
        ],



        // [
        //     'section' => 'Master',
        //     'role' => 'superadministrator'
        // ],
        [
            'title' => 'Master',
            'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'superadministrator',
            'submenu' => [
                [
                    'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                    'title' => 'Vendors Categories',
                    'root' => false,
                    'role' => 'superadministrator',
                    'page' => 'admin/master/vendors-categories',
                ],
                // [
                //     'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                //     'title' => 'Event Planners Categories',
                //     'root' => true,
                //     'role' => 'superadministrator',
                //     'page' => 'admin/master/event-planners-categories',
                // ],
                [
                    'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                    'title' => 'Shop Categories',
                    'root' => false,
                    'role' => 'superadministrator',
                    'page' => 'admin/master/shop-admins-categories',
                ],
                [
                    'title' => 'Occasions',
                    'root' => false,
                    'role' => 'superadministrator',
                    'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                    'page' => 'admin/master/occasions',
                ],
                [
                    'title' => 'Budget Settings',
                    'root' => false,
                    'role' => 'superadministrator',
                    'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                    'page' => 'admin/master/budget-settings',
                ],
                [
                    'title' => 'Vendors Items',
                    'root' => false,
                    'role' => 'superadministrator',
                    'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                    'page' => 'admin/master/vendor-items',
                ],
                [
                    'title' => 'Vendors Taxes',
                    'root' => false,
                    'role' => 'superadministrator',
                    'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                    'page' => 'admin/master/vendor-taxes',
                ],
                [
                    'title' => 'Vendor Filters',
                    'root' => false,
                    'role' => 'superadministrator',
                    'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                    'page' => 'admin/master/vendor-filters',
                ]
            ],
            [
                'title' => 'Account Settings',
                'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                'bullet' => 'line',
                'root' => false,
                'role' => 'superadministrator',
                'page' => 'personal_information',
            ],

        ],
        [
            'title' => 'Website Settings',
            'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'superadministrator',
            'submenu' => [
                //         [
                //     'title' => 'Sliders',
                //     'root' => true,
                //     'role' => 'superadministrator',
                //     'page' => 'admin/web/sliders',
                //     'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                // ],
                [
                    'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                    'title' => 'Blogs',
                    'role' => 'superadministrator',
                    'root' => true,
                    'page' => 'admin/web/blogs',
                ],
                [
                    'title' => 'Blog Categories',
                    'root' => true,
                    'role' => 'superadministrator',
                    'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                    'page' => 'admin/web/blogs-categories'
                ],
                [
                    'title' => 'Our Teams',
                    'root' => true,
                    'role' => 'superadministrator',
                    'page' => 'admin/web/our-teams',
                    'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                ],
                [
                    'title' => 'Contact Us',
                    'root' => true,
                    'role' => 'superadministrator',
                    // 'page' => 'admin/web/web-settings',
                    'page' => 'admin/cms/contactus',
                    'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                ],
                [
                    'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
                    'title' => 'Banner',
                    'role' => 'superadministrator',
                    'root' => true,
                    'page' => 'admin/cms/banner',
                ],
            ],
        ],
        // Vendor Menu
        // [
        //     'section' => 'Vendor Menu',
        //     'role' => 'vendor'
        // ],

        [
            'title' => 'My Events',
            'icon' => 'img/menu/My Events-2.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'vendor',
            'permission' => 'view-my-events',
            'page' => 'vendor/my-events'
        ],
        [
            'title' => 'Calender',
            'icon' => 'img/menu/calender.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'vendor',
            'permission' => 'view-calender',
            'page' => '/calender'
        ],

        [
            'title' => 'Leads',
            'icon' => 'img/menu/leads.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'vendor',
            'permission' => 'view-leads',
            'page' => 'vendor/leads'
        ],
        [
            'title' => 'Proposals',
            'icon' => 'img/menu/proposals.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'vendor',
            'permission' => 'view-proposals',
            'page' => 'vendor/proposals'
        ],
        [
            'title' => 'To Do List',
            'icon' => 'img/menu/report.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'vendor',
            'permission' => 'view-proposals',
            'page' => '/to-do-list'
        ],
        [
            'title' => 'Invoices',
            'icon' => 'img/menu/invoices.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'vendor',
            'permission' => 'view-invoices',
            'page' => 'vendor/invoices'
        ],
        [
            'title' => 'Financial Tool',
            'icon' => 'img/menu/Financial Tool-2.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'vendor',
            'permission' => 'view-invoices',
            'page' => 'vendor/financial-tool'
        ],

        [
            'title' => 'Templetes',
            'icon' => 'img/menu/templates.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'vendor',
            'permission' => 'view-templetes',
            'page' => '/vendor'
        ],
        [
            'title' => 'Shop Front',
            'icon' => 'img/menu/shop_front.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'vendor',
            'permission' => 'view-shop-front',
            'page' => '/vendor/shop-front/basic-information'
        ],
        [
            'title' => 'Account Settings',
            'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
            'root' => false,
            'role' => 'vendor',
            'permission' => 'view-account-settings',
            'page' => 'personal_information',
        ],
        [
            'title' => 'Master Settings',
            'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
            'root' => true,
            'page' => 'vendor/master-settings',
            'permission' => 'view-master-settings',
            'role' => 'vendor',
        ],


        // Event Planner Menu
        // [
        //     'section' => 'Event Planner Menu',
        //     'role' => 'event_planner'
        // ],
        [
            'title' => 'My Events',
            'icon' => 'img/menu/My Events-2.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'event_planner',
            'permission' => 'view-my-events',
            'page' => 'event-planner/my-events'
        ],
        [
            'title' => 'Calender',
            'icon' => 'img/menu/calender.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'event_planner',
            'permission' => 'view-calendar',
            'page' => '/calender'
        ],

        [
            'title' => 'Leads',
            'icon' => 'img/menu/leads.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'event_planner',
            'permission' => 'view-calendar',
            'page' => 'event-planner/leads'
        ],
        [
            'title' => 'Proposals',
            'icon' => 'img/menu/proposals.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'event_planner',
            'permission' => 'view-proposals',
            'page' => 'event-planner/proposals'
        ],
        [
            'title' => 'To Do List',
            'icon' => 'img/menu/report.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'event_planner',
            'permission' => 'view-proposals',
            'page' => '/to-do-list'
        ],
        [
            'title' => 'Invoices',
            'icon' => 'img/menu/invoices.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'event_planner',
            'permission' => 'view-invoices',
            'page' => 'event-planner/invoices'
        ],
        [
            'title' => 'Financial Tool',
            'icon' => 'img/menu/Financial Tool-2.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'event_planner',
            'permission' => 'view-invoices',
            'page' => 'event-planner/financial-tool'
        ],

        [
            'title' => 'Templetes',
            'icon' => 'img/menu/templates.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'event_planner',
            'permission' => 'view-templetes',
            'page' => '/event-planner'
        ],
        [
            'title' => 'Shop Front',
            'icon' => 'img/menu/shop_front.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'event_planner',
            'permission' => 'view-shop-front',
            'page' => '/event-planner/shop-front/basic-information'
        ],
        [
            'title' => 'Account Settings',
            'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
            'root' => false,
            'role' => 'event_planner',
            'permission' => 'view-account-settings',
            'page' => 'personal_information',
        ],
        [
            'title' => 'Master Settings',
            'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
            'root' => true,
            'page' => 'event-planner/master-settings',
            'role' => 'event_planner',
            'permission' => 'view-master-settings',
        ],
        // Shop Admin Menu
        [
            'section' => 'Shop Admin Menu',
            'role' => 'shop_admin'
        ],
        [
            'title' => 'Applications',
            'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'shop_admin',
            'submenu' => [
                [
                    'title' => 'Users',
                    'bullet' => 'dot',
                    'submenu' => [
                        [
                            'title' => 'List - Default',
                            'page' => 'test',
                        ],
                    ]
                ]
            ]
        ],

        [
            'title' => 'Event',
            'icon' => 'img/menu/calender.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'user',
            'page' => 'user/event'
        ],
        [
            'title' => 'To Do List',
            'icon' => 'img/menu/proposals.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'user',
            'page' => 'user/to-do-list'
        ],
[
            'title' => 'Event',
            'icon' => 'img/menu/calender.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'guest',
            'page' => 'user/event'
        ],
        [
            'title' => 'To Do List',
            'icon' => 'img/menu/proposals.svg',
            'bullet' => 'line',
            'root' => true,
            'role' => 'guest',
            'page' => 'user/to-do-list'
        ],


    ]

];
