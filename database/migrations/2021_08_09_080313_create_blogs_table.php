<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug');
            $table->longText('description')->nullable();
            $table->longText('content');
            $table->unsignedInteger('category_id');
            $table->string('tags')->nullable();
            $table->string('image');
            $table->text('seo_keywords')->nullable();
            $table->text('seo_description')->nullable();
            $table->boolean('status')->default(1);
            $table->string('author')->nullable();
            $table->boolean('approved')->default(0);
            $table->dateTime('approved_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
