<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_request', function (Blueprint $table) {
            $table->id();
            $table->string('request_no');
            $table->bigInteger('vendor_id');
            $table->bigInteger('user_id');
            $table->string('first_name');
            $table->string('second_name');
            $table->string('email');
            $table->date('wedding_date');
            $table->integer('flexible_wedding_date');
            $table->integer('no_of_guests');
            $table->integer('venue_picked');
            $table->integer('status');
          
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_request');
    }
}
