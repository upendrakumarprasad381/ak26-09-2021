<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_events', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vendor_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('request_id')->nullable();
            $table->string('event_name');
            $table->unsignedBigInteger('occasion_id')->nullable();
            $table->string('document')->nullable();
            $table->double('min_budget');
            $table->double('max_budget');
            $table->integer('number_of_attendees');
            $table->string('event_location')->nullable();
            $table->date('event_date');
            $table->time('event_time')->nullable();
            $table->enum('type_of_event',['Indoor','Outdoor']);
            $table->enum('type',['Vendor','Event Planner']);
            $table->tinyInteger('status')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_events');
    }
}
