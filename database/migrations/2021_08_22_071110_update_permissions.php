<?php

use App\Models\Module;
use App\Models\Permission;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdatePermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('modules')->truncate();
        Module::insert([
            ['id' => 1,'name' => 'superadministrator', 'display_name' => 'Super Administrator'],
            ['id' => 2,'name' => 'vendor', 'display_name' => 'Vendor'],
            ['id' => 3,'name' => 'event-planner', 'display_name' => 'Event Planner'],
            ['id' => 4,'name' => 'shop-admin', 'display_name' => 'Shop Admin'],
            ['id' => 5,'name' => 'user', 'display_name' => 'User'],
            ['id' => 6,'name' => 'events', 'display_name' => 'Events'],
            ['id' => 7,'name' => 'leads', 'display_name' => 'Leads'],
            ['id' => 8,'name' => 'common', 'display_name' => 'Common Permissions']
        ]);

        DB::table('permissions')->truncate();
        Permission::insert([
            ['name' => 'view-my-events', 'display_name' => 'View My Events', 'module_id' => 6],
            ['name' => 'edit-my-events', 'display_name' => 'Edit My Events', 'module_id' => 6],

            ['name' => 'view-vendor-manager', 'display_name' => 'View Vendor Manager', 'module_id' => 6],
            ['name' => 'edit-vendor-manager', 'display_name' => 'Edit Vendor Manager', 'module_id' => 6],

            ['name' => 'view-calendar', 'display_name' => 'View Calendar', 'module_id' => 6],
            ['name' => 'edit-calendar', 'display_name' => 'Edit Calendar', 'module_id' => 6],

            ['name' => 'view-budgeter', 'display_name' => 'View Budgeter', 'module_id' => 6],
            ['name' => 'edit-budgeter', 'display_name' => 'Edit Budgeter', 'module_id' => 6],

            ['name' => 'view-contracts', 'display_name' => 'View Contracts', 'module_id' => 6],
            ['name' => 'edit-contracts', 'display_name' => 'Edit Contracts', 'module_id' => 6],

            ['name' => 'view-checklist', 'display_name' => 'View Checklist', 'module_id' => 6],
            ['name' => 'edit-checklist', 'display_name' => 'Edit Checklist', 'module_id' => 6],

            ['name' => 'view-leads', 'display_name' => 'View Leads', 'module_id' => 6],
            ['name' => 'edit-leads', 'display_name' => 'Edit Leads', 'module_id' => 6],

            ['name' => 'view-proposals', 'display_name' => 'View Proposals', 'module_id' => 6],
            ['name' => 'edit-proposals', 'display_name' => 'Edit Proposals', 'module_id' => 6],

            ['name' => 'view-create-a-web_page', 'display_name' => 'View Create A Web Page', 'module_id' => 6],
            ['name' => 'edit-create-a-web_page', 'display_name' => 'Edit Create A Web Page', 'module_id' => 6],

            ['name' => 'view-floor-plans', 'display_name' => 'View Floor Plans', 'module_id' => 6],
            ['name' => 'edit-floor-plans', 'display_name' => 'Edit Floor Plans', 'module_id' => 6],

            ['name' => 'view-invoices', 'display_name' => 'View Invoices', 'module_id' => 6],
            ['name' => 'edit-invoices', 'display_name' => 'Edit Invoices', 'module_id' => 6],

            ['name' => 'view-conversations', 'display_name' => 'View Conversations', 'module_id' => 6],
            ['name' => 'edit-conversations', 'display_name' => 'Edit Conversations', 'module_id' => 6],

            ['name' => 'view-guest-list', 'display_name' => 'View Guest List', 'module_id' => 6],
            ['name' => 'edit-guest-list', 'display_name' => 'Edit Guest List', 'module_id' => 6],

            ['name' => 'view-form-builder', 'display_name' => 'View Form Builder', 'module_id' => 6],
            ['name' => 'edit-form-builder', 'display_name' => 'Edit Form Builder', 'module_id' => 6],

            ['name' => 'view-account-settings', 'display_name' => 'View Account Settings', 'module_id' => 8],
            ['name' => 'edit-account-settings', 'display_name' => 'Edit Account Settings', 'module_id' => 8],

            ['name' => 'view-master-settings', 'display_name' => 'View Master Settings', 'module_id' => 8],
            ['name' => 'edit-master-settings', 'display_name' => 'Edit Master Settings', 'module_id' => 8],

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
