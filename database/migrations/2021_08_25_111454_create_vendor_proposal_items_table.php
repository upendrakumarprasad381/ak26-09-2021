<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorProposalItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_proposal_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('proposal_id');
            $table->string('item_name');
            $table->unsignedBigInteger('service_type_id');
            $table->text('description')->nullable();
            $table->double('amount');
            $table->string('tax');
            $table->integer('quantity');
            $table->string('attachment')->nullable();
            $table->boolean('is_sub_item')->default(0);
            $table->unsignedBigInteger('item_id')->nullable();
            $table->text('remarks')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_proposal_items');
    }
}
