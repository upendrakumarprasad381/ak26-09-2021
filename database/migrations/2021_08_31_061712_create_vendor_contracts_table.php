<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_contracts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('event_id');
            $table->unsignedBigInteger('request_id')->nullable();
            $table->unsignedBigInteger('proposal_id')->nullable();
            $table->unsignedBigInteger('party1_id');
            $table->unsignedBigInteger('party2_id');
            $table->date('date');
            $table->string('signature_1')->nullable();
            $table->string('signature_2')->nullable();
            $table->boolean('approved_digital_written_signature')->default(0);
            $table->enum('type',['Written','Digital']);
            $table->enum('status',['Pending','Signed','Declined','On Hold']);
            $table->longText('terms_of_agreement')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_contracts');
    }
}
