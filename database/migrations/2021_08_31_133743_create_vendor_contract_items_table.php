<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorContractItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_contract_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('contract_id');
            $table->string('item_name');
            $table->unsignedBigInteger('service_type_id');
            $table->text('description')->nullable();
            $table->double('amount');
            $table->string('tax');
            $table->integer('quantity');
            $table->double('sub_total')->default(0);
            $table->double('tax_total')->default(0);
            $table->double('total')->default(0);
            $table->string('attachment')->nullable();
            $table->text('remarks')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_contractitems');
    }
}
