<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTaxAmountToVendorProposalItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_proposal_items', function (Blueprint $table) {
            $table->double('sub_total')->after('quantity')->default(0);
            $table->double('tax_total')->after('sub_total')->default(0);
            $table->double('total')->after('tax_total')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_proposal_items', function (Blueprint $table) {
            $table->dropColumn('sub_total');
            $table->dropColumn('tax_total');
            $table->dropColumn('total');
        });
    }
}
