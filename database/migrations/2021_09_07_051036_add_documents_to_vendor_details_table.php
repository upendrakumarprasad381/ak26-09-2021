<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDocumentsToVendorDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_details', function (Blueprint $table) {
            $table->string('trade_license')->after('proposal_terms_condition')->nullable();
            $table->string('identity_proof')->after('trade_license')->nullable();
            $table->string('passport')->after('identity_proof')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_details', function (Blueprint $table) {
            $table->dropColumn('trade_license');
            $table->dropColumn('identity_proof');
            $table->dropColumn('passport');
        });
    }
}
