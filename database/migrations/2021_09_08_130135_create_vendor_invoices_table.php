<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_invoices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('event_id');
            $table->unsignedBigInteger('contract_id')->nullable();
            $table->string('invoice_sn');
            $table->unsignedBigInteger('party1_id')->nullable();
            $table->unsignedBigInteger('party2_id')->nullable();
            $table->date('date');
            $table->date('due_date');
            $table->string('contract_number')->nullable();
            $table->enum('status',['Pending','Paid','Refunded','Due Today or Passed']);
            $table->string('signature_1')->nullable();
            $table->string('signature_2')->nullable();
            $table->boolean('approved_digital_written_signature')->default(0);
            $table->enum('type',['Written','Digital']);
            $table->longText('terms_of_agreement')->nullable();
            $table->date('send_notification_date')->nullable();
            $table->date('reminder_on_date')->nullable();
            $table->boolean('weekly_reminder')->default(0);
            $table->boolean('daily_reminder')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_invoices');
    }
}
