<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBudgetSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_settings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('occasion_id');
            $table->unsignedBigInteger('category_id');
            $table->double('budget')->default(0.00);
            $table->double('budget_percentage')->default(0.00);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budget_settings');
    }
}
