<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFocusToVendorDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_details', function (Blueprint $table) {
            $table->string('focus')->nullable()->after('passport');
            $table->string('instagram')->nullable()->after('focus');
            $table->string('email_2')->nullable()->after('instagram');
            $table->string('emirates_availability')->nullable()->after('email_2');
            $table->string('city')->nullable()->after('emirates_availability');
            $table->string('country')->nullable()->after('city');
            $table->string('cuisine_focus')->nullable()->after('country');
            $table->string('menu_type')->nullable()->after('cuisine_focus');
            $table->string('experience_focus')->nullable()->after('menu_type');
            $table->string('price_range')->nullable()->after('experience_focus');
            $table->string('capacity')->nullable()->after('price_range');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_details', function (Blueprint $table) {
            //
        });
    }
}
