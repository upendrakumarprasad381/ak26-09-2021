<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaymentToVendorInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_invoices', function (Blueprint $table) {
            $table->double('total_amount')->after('daily_reminder')->default(0.00);
            $table->double('total_payable')->after('daily_reminder')->default(0.00);
            $table->double('total_paid')->after('total_amount')->default(0.00);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_invoices', function (Blueprint $table) {
            $table->dropColumn('total_amount');
            $table->dropColumn('total_paid');
            $table->dropColumn('total_payable');
        });
    }
}
