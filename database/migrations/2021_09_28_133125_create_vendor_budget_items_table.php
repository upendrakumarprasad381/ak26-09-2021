<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorBudgetItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_budget_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vendor_budget_id');
            $table->unsignedBigInteger('category_id')->nullable();
            $table->string('category_name')->nullable();
            $table->double('budget');
            $table->double('actual_cost');
            $table->double('variance');
            $table->enum('payment_status',['Pending','Partially Paid','Paid']);
            $table->enum('budget_status',['On Budget','Over Budget']);
            $table->string('remarks')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_budget_items');
    }
}
