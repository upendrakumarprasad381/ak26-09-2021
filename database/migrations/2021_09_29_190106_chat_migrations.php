<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChatMigrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('users', function (Blueprint $table) {
        //     $table->string('name');
        //     $table->timestamp('last_seen')->nullable();
        //     $table->tinyInteger('is_online')->default(0)->nullable();
        //     $table->tinyInteger('is_active')->default(0)->nullable();
        //     $table->text('about')->nullable();
        //     $table->string('photo_url')->nullable();
        //     $table->string('activation_code')->nullable();
        //     $table->tinyInteger('is_system')->default(0)->nullable();
        // });

        // Schema::create('conversations', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->unsignedInteger('from_id')->nullable();
        //     $table->unsignedInteger('to_id')->nullable();
        //     $table->text('message');
        //     $table->tinyInteger('status')->default(0)->comment('0 for unread,1 for seen');
        //     $table->tinyInteger('message_type')->default(0)->comment('0- text message, 1- image, 2- pdf, 3- doc, 4- voice');
        //     $table->text('file_name')->nullable();
        //     $table->timestamps();
        //     $table->foreign('from_id')
        //         ->references('id')->on('users')
        //         ->onUpdate('cascade')
        //         ->onDelete('cascade');
        //     $table->foreign('to_id')
        //         ->references('id')->on('users')
        //         ->onUpdate('cascade')
        //         ->onDelete('cascade');
        // });
        // Schema::create('message_action', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->integer('conversation_id');
        //     $table->integer('deleted_by');
        //     $table->timestamps();
        // });
        // Schema::table('users', function (Blueprint $table) {
        //     $table->string('player_id')->unique()->nullable()->comment('One signal user id');
        //     $table->boolean('is_subscribed')->nullable();
        // });
        // Schema::create('blocked_users', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->unsignedInteger('blocked_by');
        //     $table->unsignedInteger('blocked_to');
        //     $table->timestamps();

        //     $table->foreign('blocked_by')->references('id')->on('users')
        //         ->onDelete('cascade')
        //         ->onUpdate('cascade');

        //     $table->foreign('blocked_to')->references('id')->on('users')
        //         ->onDelete('cascade')
        //         ->onUpdate('cascade');
        // });
        // Schema::table('message_action', function (Blueprint $table) {
        //     $table->tinyInteger('is_hard_delete')->default(0);
        // });
        // Schema::create('groups', function (Blueprint $table) {
        //     $table->uuid('id')->primary();
        //     $table->string('name');
        //     $table->string('description')->nullable();
        //     $table->string('photo_url')->nullable();
        //     $table->integer('privacy');
        //     $table->integer('group_type')->comment('1 => Open (Anyone can send message), 2 => Close (Only Admin can send message) ');
        //     $table->unsignedInteger('created_by');
        //     $table->timestamps();

        //     $table->foreign('created_by')
        //         ->references('id')->on('users')
        //         ->onUpdate('cascade')
        //         ->onDelete('cascade');
        // });
        // Schema::create('group_users', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('group_id');
        //     $table->unsignedInteger('user_id');
        //     $table->integer('role')->default(1);
        //     $table->unsignedInteger('added_by');
        //     $table->unsignedInteger('removed_by')->nullable();
        //     $table->dateTime('deleted_at')->nullable();
        //     $table->timestamps();

        //     $table->foreign('group_id')
        //         ->references('id')->on('groups')
        //         ->onUpdate('cascade')
        //         ->onDelete('cascade');

        //     $table->foreign('user_id')
        //         ->references('id')->on('users')
        //         ->onUpdate('cascade')
        //         ->onDelete('cascade');

        //     $table->foreign('removed_by')
        //         ->references('id')->on('users')
        //         ->onUpdate('cascade')
        //         ->onDelete('cascade');

        //     $table->foreign('added_by')
        //         ->references('id')->on('users')
        //         ->onUpdate('cascade')
        //         ->onDelete('cascade');
        // });

        // Schema::table('conversations', function (Blueprint $table) {
        //     $table->dropForeign('conversations_to_id_foreign');
        //     $table->dropIndex('conversations_to_id_foreign');
        // });

        // Schema::table('conversations', function (Blueprint $table) {
        //     $table->string('to_id')->change();
        //     $table->string('to_type')->default(\App\Models\Conversation::class)->after('to_id')->comment('1 => Message, 2 => Group Message');
        // });

        // Schema::create('group_message_recipients', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->unsignedInteger('user_id');
        //     $table->unsignedInteger('conversation_id');
        //     $table->string('group_id');
        //     $table->dateTime('read_at')->nullable();
        //     $table->timestamps();

        //     $table->foreign('user_id')
        //         ->references('id')->on('users')
        //         ->onUpdate('cascade')
        //         ->onDelete('cascade');

        //     $table->foreign('conversation_id')
        //         ->references('id')->on('conversations')
        //         ->onUpdate('cascade')
        //         ->onDelete('cascade');
        // });
        // Schema::create('last_conversations', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('group_id');
        //     $table->unsignedInteger('conversation_id');
        //     $table->unsignedInteger('user_id');
        //     $table->text('group_details');

        //     $table->index('group_id');

        //     $table->foreign('group_id')
        //         ->references('id')->on('groups')
        //         ->onUpdate('cascade')
        //         ->onDelete('cascade');

        //     $table->foreign('user_id')
        //         ->references('id')->on('users')
        //         ->onUpdate('cascade')
        //         ->onDelete('cascade');

        //     $table->foreign('conversation_id')
        //         ->references('id')->on('conversations')
        //         ->onUpdate('cascade')
        //         ->onDelete('cascade');
        //     $table->timestamps();
        // });
        // Schema::table('conversations', function (Blueprint $table) {
        //     $table->unsignedInteger('reply_to')->nullable();

        //     $table->foreign('reply_to')->references('id')->on('conversations')
        //         ->onDelete('cascade')
        //         ->onUpdate('cascade');
        // });
        // Schema::create('notifications', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('owner_id');
        //     $table->string('owner_type');
        //     $table->text('notification');
        //     $table->unsignedBigInteger('to_id');
        //     $table->boolean('is_read')->default(0);
        //     $table->dateTime('read_at')->nullable();
        //     $table->tinyInteger('message_type')->default(0);
        //     $table->text('file_name')->nullable();
        //     $table->softDeletes();
        //     $table->timestamps();
        // });
        // Schema::table('conversations', function (Blueprint $table) {
        //     $table->text('url_details');
        // });
        // Schema::create('archived_users', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('owner_id');
        //     $table->string('owner_type');
        //     $table->unsignedInteger('archived_by');
        //     $table->timestamps();

        //     $table->foreign('archived_by')
        //         ->references('id')->on('users')
        //         ->onUpdate('cascade')
        //         ->onDelete('cascade');
        // });
        // Schema::table('users', function (Blueprint $table) {
        //     $table->integer('privacy')->default(1);
        // });
        // Schema::table('users', function (Blueprint $table) {
        //     $table->integer('gender')->nullable();
        // });
        // Schema::table('conversations', function (Blueprint $table) {
        //     $table->text('url_details')->nullable()->change();
        // });
        // Schema::create('chat_request', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->unsignedInteger('from_id')->nullable();
        //     $table->string('owner_id')->nullable();
        //     $table->string('owner_type')->nullable();
        //     $table->integer('status')->default(0);
        //     $table->timestamps();

        //     $table->foreign('from_id')
        //         ->references('id')->on('users')
        //         ->onUpdate('cascade')
        //         ->onDelete('cascade');
        // });
        // Schema::create('user_status', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->unsignedInteger('user_id');
        //     $table->string('emoji');
        //     $table->string('emoji_short_name');
        //     $table->string('status');
        //     $table->timestamps();
        // });
        // Schema::create('reported_users', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->unsignedInteger('reported_by');
        //     $table->unsignedInteger('reported_to');
        //     $table->longText('notes');
        //     $table->timestamps();

        //     $table->foreign('reported_by')
        //         ->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        //     $table->foreign('reported_to')
        //         ->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        // });
        // Schema::create('settings', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('key');
        //     $table->string('value');
        //     $table->timestamps();
        // });
        // Schema::table('users', function (Blueprint $table) {
        //     $table->softDeletes();
        // });

        // Schema::table('users', function (Blueprint $table) {
        //     $table->index(['name']);
        //     $table->index(['email']);
        //     $table->index(['phone']);
        // });
        // Schema::table('reported_users', function (Blueprint $table) {
        //     $table->index(['created_at']);
        // });
        // Schema::table('roles', function (Blueprint $table) {
        //     $table->index(['name']);
        // });
        // Schema::table('conversations', function (Blueprint $table) {
        //     $table->index(['created_at']);
        // });
        // Schema::table('notifications', function (Blueprint $table) {
        //     $table->index(['created_at']);
        // });
        // Schema::table('groups', function (Blueprint $table) {
        //     $table->index(['name']);
        // });
        // Schema::table('group_users', function (Blueprint $table) {
        //     $table->index(['role']);
        // });
        // Schema::create('user_devices', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->unsignedInteger('user_id');
        //     $table->string('player_id');
        //     $table->timestamps();

        //     $table->foreign('user_id')->references('id')->on('users')
        //         ->onUpdate('cascade')
        //         ->onDelete('cascade');
        // });

        // Schema::table('users', function (Blueprint $table) {
        //     $table->string('language')->default('en');
        // });
        // Schema::table('failed_jobs', function (Blueprint $table) {
        //     $table->string('uuid')->after('id')->nullable()->unique();
        // });

        // DB::table('failed_jobs')->whereNull('uuid')->cursor()->each(function ($job) {
        //     DB::table('failed_jobs')
        //         ->where('id', $job->id)
        //         ->update(['uuid' => (string) Str::uuid()]);
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('last_seen');
            $table->dropColumn('is_online');
            $table->dropColumn('is_active');
            $table->dropColumn('photo_url');
            $table->dropColumn('activation_code');
            $table->dropColumn('is_system');
        });
        Schema::dropIfExists('conversations');
        Schema::dropIfExists('message_action');
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('player_id');
            $table->dropColumn('is_subscribed');
        });
        Schema::dropIfExists('blocked_users');
        Schema::table('message_action', function (Blueprint $table) {
            $table->dropColumn('is_hard_delete');
        });
        Schema::dropIfExists('groups');
        Schema::dropIfExists('group_users');
        Schema::table('conversations', function (Blueprint $table) {
            $table->dropColumn('to_type');
        });
        Schema::dropIfExists('group_message_recipients');
        Schema::dropIfExists('last_conversations');
        Schema::table('conversations', function (Blueprint $table) {
            $table->dropForeign('conversations_reply_to_foreign');
            $table->dropColumn('reply_to');
        });
        Schema::dropIfExists('notifications');
        Schema::table('conversations', function (Blueprint $table) {
            $table->dropColumn('url_details');
        });
        Schema::dropIfExists('archived_users');
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('privacy');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('gender');
        });
        Schema::table('conversations', function (Blueprint $table) {
            $table->dropColumn('url_details');
        });
        Schema::dropIfExists('chat_request');
        Schema::dropIfExists('user_status');
        Schema::dropIfExists('reported_users');
        Schema::dropIfExists('settings');
        Schema::table('users', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropIndex(['name']);
            $table->dropIndex(['email']);
            $table->dropIndex(['phone']);
        });
        Schema::table('reported_users', function (Blueprint $table) {
            $table->dropIndex(['created_at']);
        });
        Schema::table('roles', function (Blueprint $table) {
            $table->dropIndex(['name']);
        });
        Schema::table('conversations', function (Blueprint $table) {
            $table->dropIndex(['created_at']);
        });
        Schema::table('notifications', function (Blueprint $table) {
            $table->dropIndex(['created_at']);
        });
        Schema::table('groups', function (Blueprint $table) {
            $table->dropIndex(['name']);
        });
        Schema::table('group_users', function (Blueprint $table) {
            $table->dropIndex(['role']);
        });
        Schema::dropIfExists('user_devices');
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('language');
        });
        Schema::table('failed_jobs', function (Blueprint $table) {
            $table->dropColumn('uuid');
        });
    }
}
