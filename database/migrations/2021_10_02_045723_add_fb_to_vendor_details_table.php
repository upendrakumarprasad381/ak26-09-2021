<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFbToVendorDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_details', function (Blueprint $table) {
            $table->string('facebook')->after('instagram')->nullable();
            $table->string('linkedin')->after('facebook')->nullable();
            $table->string('twiter')->after('linkedin')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_details', function (Blueprint $table) {
            $table->dropColumn('facebook');
            $table->dropColumn('linkedin');
            $table->dropColumn('twiter');
        });
    }

}
