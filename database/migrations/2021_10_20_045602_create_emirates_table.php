<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmiratesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emirates', function (Blueprint $table) {
            $table->id();
            $table->string('emirate');
            $table->timestamps();
        });

        DB::table('emirates')->insert([
            array(
                'emirate' => 'Abu Dhabi'
            ),
            array(
                'emirate' => 'Dubai'
            ),
            array(
                'emirate' => 'Sharjah'
            ),
            array(
                'emirate' => 'Ajman'
            ),
            array(
                'emirate' => 'Umm Al Quwain'
            ),
            array(
                'emirate' => 'Ras Al Khaimah'
            ),
            array(
                'emirate' => 'Fujairah'
            )
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emirates');
    }
}
