<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeToVendorTermsConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_terms_conditions', function (Blueprint $table) {
            $table->enum('type',['Proposal','Contract','Invoice'])->default('Proposal')->after('default');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_terms_conditions', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
