<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDefaultToVendorBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_budgets', function (Blueprint $table) {
            $table->boolean('is_default')->default(0)->after('status');
            $table->unsignedDouble('sub_event_id')->nullable()->after('event_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_budgets', function (Blueprint $table) {
            $table->dropColumn('is_default');
        });
    }
}
