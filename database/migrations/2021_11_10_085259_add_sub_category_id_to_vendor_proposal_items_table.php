<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSubCategoryIdToVendorProposalItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_proposal_items', function (Blueprint $table) {
            $table->unsignedBigInteger('sub_service_id')->after('service_type_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_proposal_items', function (Blueprint $table) {
            $table->dropColumn('sub_service_id');
        });
    }
}
