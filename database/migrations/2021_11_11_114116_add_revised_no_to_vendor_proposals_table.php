<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRevisedNoToVendorProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_proposals', function (Blueprint $table) {
            $table->tinyInteger('reviced_no')->default(0)->after('terms_of_agreement');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_proposals', function (Blueprint $table) {
            $table->dropColumn('reviced_no');
        });
    }
}
