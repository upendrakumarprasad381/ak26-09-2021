<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNegotiationStatusToVendorProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_proposals', function (Blueprint $table) {
            $table->boolean('negotiation_status')->default(0)->after('reviced_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_proposals', function (Blueprint $table) {
            $table->dropColumn('negotiation_status');
        });
    }
}
