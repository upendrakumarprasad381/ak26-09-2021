<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSignature1TypeToVendorContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_contracts', function (Blueprint $table) {
            $table->enum('signature_1_type',['Digital','Written'])->after('signature_1')->default('Digital');
            $table->enum('signature_2_type',['Digital','Written'])->after('signature_2')->default('Digital');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_contracts', function (Blueprint $table) {
            $table->dropColumn('signature_1_type');
            $table->dropColumn('signature_2_type');
        });
    }
}
