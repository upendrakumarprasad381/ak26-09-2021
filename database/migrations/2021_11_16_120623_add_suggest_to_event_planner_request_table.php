<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSuggestToEventPlannerRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_planner_request', function (Blueprint $table) {
            $table->unsignedBigInteger('suggest_to')->after('vendor_id')->nullable();
            $table->text('suggest_note')->after('suggest_to')->nullable();
        });

        Schema::table('customer_request', function (Blueprint $table) {
            $table->unsignedBigInteger('suggest_to')->after('vendor_id')->nullable();
            $table->text('suggest_note')->after('suggest_to')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_planner_request', function (Blueprint $table) {
            $table->dropColumn('suggest_to');
            $table->dropColumn('suggest_note');
        });
        Schema::table('customer_request', function (Blueprint $table) {
            $table->dropColumn('suggest_to');
            $table->dropColumn('suggest_note');
        });
    }
}
