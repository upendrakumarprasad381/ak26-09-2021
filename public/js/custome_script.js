$(document).ajaxComplete(function myErrorHandler(event, xhr, ajaxOptions, thrownError) {
    var response = xhr.responseJSON;
    switch (xhr.status) {
        case 422:
            if (response.type == 'warning') {
                toastr.warning(response.message, 'Warning');
            }
            else {
                $.each(response.errors, function (key, errors) {
                    var input = $('[name="' + key + '"]');
                    var wrapper = input.parent();
                    wrapper.find('.invalid-feedback').attr('hidden', true);
                    input.addClass('is-invalid');
                    wrapper.append('<div class="invalid-feedback">' + errors[0] + '</div>');
                });
                toastr.error(response.message, 'Error');
            }
            break;
        case 200:
            if (response.type == 'warning') {
                toastr.warning(response.message, 'Warning');
            }
            else {
                if (response.message) {
                    Swal.fire({
                        text: response.message,
                        icon: "success",
                    });
                }
            }
            break;
        case 500:
            toastr.error('Some thing went wrong!', 'Warning');
            break;
    }
});

$('.form-control').on("change", function () {
    var field = $(this);
    field.removeClass('is-invalid');
    field.parent().find('.invalid-feedback').hide();
});

//text box validation for numerics
$(".numeric").on("keypress keyup blur", function (event) {
    $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
});
function jsFieldsValidator(fields) {
    valid = true;
    fields.forEach(field => {
        $("input[name='" + field + "']")
            .map(function () {
                if ($(this).val() == "") {
                    valid = false;
                    var wrapper = $(this).parent();
                    wrapper.find('.invalid-feedback').attr('hidden', true);
                    $(this).addClass('is-invalid');
                    wrapper.append('<div class="invalid-feedback">The ' + field.replace('_', ' ') + ' is required</div>');
                }
            });
        $("select[name='" + field + "']")
            .map(function () {
                if ($(this).val() == "") {
                    valid = false;
                    var wrapper = $(this).parent();
                    wrapper.find('.invalid-feedback').attr('hidden', true);
                    $(this).addClass('is-invalid');
                    wrapper.append('<div class="invalid-feedback">The ' + field.replace('_', ' ') + ' is required</div>');
                }
            });
    });
    if (!valid)
        toastr.error('Validation failure.', 'Error');
    return valid;
}

//image upload
$(function () {
    var names = [];
    $('body').on('change', '.picupload', function (event) {
        var getAttr = $(this).attr('click-type');
        var parent_tag = $(this).parent().parent();
        var files = event.target.files;
        var output = document.getElementById("media-list");
        var z = 0
        if (getAttr == 'single') {

            // $('#media-list').html('');
            // $('#media-list').html('<li class="myupload"><span><i class="fa fa-plus" aria-hidden="true"></i><input type="file" click-type="type2" id="picupload" class="picupload"></span></li>');
            // $('#hint_brand').modal('show');

            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                names.push($(this).get(0).files[i].name);
                if (file.type.match('image')) {
                    var picReader = new FileReader();
                    picReader.fileName = file.name
                    picReader.addEventListener("load", function (event) {
                        var picFile = event.target;
                        var div = document.createElement("li");
                        div.innerHTML = "<img src='" + picFile.result + "'" +
                            "title='" + picFile.name + "'/><div  class='post-thumb'><div class='inner-post-thumb'><a href='javascript:void(0);' data-id='" + event.target.fileName + "' class='remove-pic'><i class='fa fa-times' aria-hidden='true'></i></a><div></div>" + event.target.fileName + "";

                        parent_tag.parent().prepend(div);
                        parent_tag.hide();
                    });
                } else {
                    var picReader = new FileReader();
                    picReader.fileName = file.name
                    picReader.addEventListener("load", function (event) {
                        var picFile = event.target;
                        var div = document.createElement("li");
                        div.innerHTML = "<img src='/assets/icons/docs.png'" +
                            "title='" + picFile.name + "'/><div  class='post-thumb'><div class='inner-post-thumb'><a href='javascript:void(0);' data-id='" + event.target.fileName + "' class='remove-pic'><i class='fa fa-times' aria-hidden='true'></i></a><div></div>" + event.target.fileName + "";
                        $("#media-list").prepend(div);
                        parent_tag.parent().prepend(div);
                        parent_tag.hide();
                    });

                }
                picReader.readAsDataURL(file);
            }
            console.log(names);
        } else if (getAttr == 'multiple') {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                names.push($(this).get(0).files[i].name);
                if (file.type.match('image')) {

                    var picReader = new FileReader();
                    picReader.fileName = file.name
                    picReader.addEventListener("load", function (event) {

                        var picFile = event.target;

                        var div = document.createElement("li");

                        div.innerHTML = "<img src='" + picFile.result + "'" +
                            "title='" + picFile.name + "'/><div  class='post-thumb'><div class='inner-post-thumb'><a href='javascript:void(0);' data-id='" + event.target.fileName + "' class='remove-pic'><i class='fa fa-times' aria-hidden='true'></i></a><div></div>";

                        $("#media-list").prepend(div);

                    });
                } else {
                    var picReader = new FileReader();
                    picReader.fileName = file.name
                    picReader.addEventListener("load", function (event) {

                        var picFile = event.target;

                        var div = document.createElement("li");

                        div.innerHTML = "<video src='" + picFile.result + "'" +
                            "title='" + picFile.name + "'></video><div class='post-thumb'><div  class='inner-post-thumb'><a href='javascript:void(0);' data-id='" + event.target.fileName + "' class='remove-pic'><i class='fa fa-times' aria-hidden='true'></i></a><div></div>";

                        $("#media-list").prepend(div);

                    });
                }
                picReader.readAsDataURL(file);

            }
            // return array of file name
            console.log(names);
        }

    });

    $('body').on('click', '.remove-pic', function () {
        $(this).parent().parent().parent().parent().find('.myupload').show();
        $(this).parent().parent().parent().remove();
        var removeItem = $(this).attr('data-id');
        var yet = names.indexOf(removeItem);

        if (yet != -1) {
            names.splice(yet, 1);
        }
        // return array of file name

        console.log(names);
    });
    $('#hint_brand').on('hidden.bs.modal', function (e) {
        names = [];
        z = 0;
    });
});
//Digital signature
(function () {
    window.requestAnimFrame = (function (callback) {
        return window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimaitonFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();

    var canvas = document.getElementById("sig-canvas");
    var ctx = canvas.getContext("2d");
    ctx.strokeStyle = "#222222";
    ctx.lineWidth = 4;

    var drawing = false;
    var mousePos = {
        x: 0,
        y: 0
    };
    var lastPos = mousePos;

    canvas.addEventListener("mousedown", function (e) {
        drawing = true;
        lastPos = getMousePos(canvas, e);
    }, false);

    canvas.addEventListener("mouseup", function (e) {
        drawing = false;
    }, false);

    canvas.addEventListener("mousemove", function (e) {
        mousePos = getMousePos(canvas, e);
    }, false);

    // Add touch event support for mobile
    canvas.addEventListener("touchstart", function (e) {

    }, false);

    canvas.addEventListener("touchmove", function (e) {
        var touch = e.touches[0];
        var me = new MouseEvent("mousemove", {
            clientX: touch.clientX,
            clientY: touch.clientY
        });
        canvas.dispatchEvent(me);
    }, false);

    canvas.addEventListener("touchstart", function (e) {
        mousePos = getTouchPos(canvas, e);
        var touch = e.touches[0];
        var me = new MouseEvent("mousedown", {
            clientX: touch.clientX,
            clientY: touch.clientY
        });
        canvas.dispatchEvent(me);
    }, false);

    canvas.addEventListener("touchend", function (e) {
        var me = new MouseEvent("mouseup", {});
        canvas.dispatchEvent(me);
    }, false);

    function getMousePos(canvasDom, mouseEvent) {
        var rect = canvasDom.getBoundingClientRect();
        return {
            x: mouseEvent.clientX - rect.left,
            y: mouseEvent.clientY - rect.top
        }
    }

    function getTouchPos(canvasDom, touchEvent) {
        var rect = canvasDom.getBoundingClientRect();
        return {
            x: touchEvent.touches[0].clientX - rect.left,
            y: touchEvent.touches[0].clientY - rect.top
        }
    }

    function renderCanvas() {
        if (drawing) {
            ctx.moveTo(lastPos.x, lastPos.y);
            ctx.lineTo(mousePos.x, mousePos.y);
            ctx.stroke();
            lastPos = mousePos;
        }
    }

    // Prevent scrolling when touching the canvas
    document.body.addEventListener("touchstart", function (e) {
        if (e.target == canvas) {
            e.preventDefault();
        }
    }, false);
    document.body.addEventListener("touchend", function (e) {
        if (e.target == canvas) {
            e.preventDefault();
        }
    }, false);
    document.body.addEventListener("touchmove", function (e) {
        if (e.target == canvas) {
            e.preventDefault();
        }
    }, false);

    (function drawLoop() {
        requestAnimFrame(drawLoop);
        renderCanvas();
    })();

    function clearCanvas() {
        canvas.width = canvas.width;
    }

    // Set up the UI
    var sigText = document.getElementById("sig-dataUrl");
    var sigImage = document.getElementById("sig-image");
    var clearBtn = document.getElementById("sig-clearBtn");
    var submitBtn = document.getElementById("sig-submitBtn");
    clearBtn.addEventListener("click", function (e) {
        clearCanvas();
    }, false);
    submitBtn.addEventListener("click", function (e) {
        var dataUrl = canvas.toDataURL();

        var div = document.createElement("li");
        div.innerHTML = "<img src='" + dataUrl + "'" +
            "title=''/><div  class='post-thumb'><div class='inner-post-thumb'><a href='javascript:void(0);' data-id='" + dataUrl + "' class='remove-pic'><i class='fa fa-times' aria-hidden='true'></i></a><div></div>";
        parent_tag.parent().prepend(div);
        parent_tag.parent().parent().find('.esign').val(dataUrl)
        parent_tag.hide();

    }, false);

})();


//Terms and Conditions Scripts

$('.btnApplyDflt').on('click', function () {
    var type = $('.btnApplyDflt').data('type');
    $.ajax({
        type: "GET",
        enctype: 'multipart/form-data',
        url: "/vendor/get_vendor_terms_conditions?type=" + type + "",
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
            $('#defaultTCModel').modal('show');
            var tc_items = JSON.parse(localStorage["tc_items"]);
            var row = `<label class="checkbox">
            <input type="checkbox" class="check_all">
            <span></span>Check All</label>`;
            $('.default_tc_list').empty();
            data.forEach(element => {
                var checked = '';
                if (tc_items.length > 0) {
                    tc_items.forEach(element_ => {
                        if (element_.text == element.text) {
                            checked = 'checked';
                        }
                    });
                }
                else {
                    checked = element.default == 1 ? 'checked="checked"' : '';
                }
                row += `<label class="checkbox default_tc">
                <input type="checkbox" `+ checked + ` name="default_tc" value="` + element.text + `">
                <span></span>`+ element.text + `</label>`;
            });
            $('.default_tc_list').append(row);
            $('.check_all').on('change', function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    $('input[name="default_tc"]').each(function () {
                        this.checked = 'checked';
                    });
                } else {
                    $('input[name="default_tc"]').each(function () {
                        this.checked = '';
                    });
                }
            });
            $('input[name="default_tc"]').on('change', function () {
                var isChecked = $(this).is(":checked");
                if (isChecked) {
                    var checked_all = true;
                    $('input[name="default_tc"]').each(function () {
                        if ($(this).is(":checked")) {
                        }
                        else {
                            checked_all = false;
                        }
                    });
                    if (checked_all) {
                        $('.check_all').prop('checked', 'checked');
                    }
                } else {
                    $('.check_all').prop('checked', '');
                }
            });
            // var tc_items = [];
            // data.forEach(element => {
            //     tc_items.push({
            //         text: element.text,
            //     });
            // });
            // localStorage["tc_items"] = JSON.stringify(tc_items);
            // updateTCRows();
        },
        error: function (e) {
            $("#btnNewItemSave").prop("disabled", false);
        }
    });
});
$('#btnAddDefaultTC').on('click', function () {
    var tc_items = [];
    $("input:checkbox[name=default_tc]:checked").each(function () {
        tc_items.push({
            text: $(this).val(),
        });
    });

    localStorage["tc_items"] = JSON.stringify(tc_items);
    updateTCRows();
    $('#defaultTCModel').modal('hide');
});

function updateTCRows() {
    $('#tc_list').empty();
    var tc_items = JSON.parse(localStorage["tc_items"]);
    var neg_tc_items = localStorage["neg_tc_items"] ? JSON.parse(localStorage["neg_tc_items"]) : [];
    row = 0;
    tc_items.forEach(element => {
        var rows = '';
        rows += '<li data-text="' + element.text + '" data-row="' + row + '">' + element.text + ' &nbsp';
        if (neg_tc_items.length > 0) {
            neg_tc_items.forEach(element_ => {
                if (element_.row == row) {
                    if (element_.row == row) {
                        rows += '<span class="neg_text_' + row + '" style="color: red;">' + element_.text + '</span>';
                    }
                    // else {
                    //     rows += '<span class="neg_text_' + row + '" style="color: red;"></span>';
                    // }
                }
                // else {
                //     rows += '<span class="neg_text_' + row + '" style="color: red;">' + element_.text + '</span>';
                // }
            });
        }
        rows +=
            '<a href="javascript:;"class="btn btn-sm btn-clean btn-icon TCeditBtn" title="Edit"><i class="la la-pencil"></i></a>';
        rows +=
            '<a href="javascript:;" class="btn btn-sm btn-clean btn-icon TCdeleteBtn"title="Delete"><i class="la la-trash"></i></a>';
        rows += '</li>';
        $('#tc_list').append(rows);
        row++;
    });
    tc_editable = '';
    tc_editable_row = '';
    $('.TCeditBtn').on('click', function () {
        $('#btnAddTCItem').text('Update Item');
        tc_editable = $(this).parent().data('text');
        tc_editable_row = $(this).parent().data('row');
        $('textarea#terms_and_condition').val($(this).parent().data('text'));
        $('#addTCItemModel').modal('show');
    });
    $('.TCdeleteBtn').on('click', function () {
        var tc_items = JSON.parse(localStorage["tc_items"]);
        array_index = $(this).parent().data('row');

        Swal.fire({
            title: "Are you sure?",
            text: "You wont be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                tc_items.splice(array_index, 1);
                localStorage["tc_items"] = JSON.stringify(tc_items);
                updateTCRows();
            } else if (result.dismiss === "cancel") {
                Swal.fire(
                    "Cancelled",
                    "Your imaginary file is safe :)",
                    "error"
                )
            }
        });
    });
}
$('.btnTCAddNew').on('click', function () {
    $('#btnAddTCItem').text('Add Item');
    tc_editable = '';
    $('#addTCItemModel').modal('show');
});

$('#btnAddTCItem').on('click', function () {
    var tc_items = JSON.parse(localStorage["tc_items"]);
    var text = $('textarea#terms_and_condition').val();
    var tc_item = {
        text: text
    }
    if (tc_items.length > 0) {
        var exist = 0;
        tc_items.forEach(element => {
            if (element.text == tc_editable) {
                exist = 1;
                element.text = text
            }
        });
        if (exist == 0) {
            tc_items.push(tc_item);
        }
    } else {
        tc_items.push(tc_item);
    }
    localStorage["tc_items"] = JSON.stringify(tc_items);
    updateTCRows();
    $('#addTCItemModel').modal('hide');
    $('textarea#terms_and_condition').val('');
});

function getSubService(type) {
    $.ajax({
        type: "GET",
        enctype: 'multipart/form-data',
        url: "/vendor/get-sub-services/" + type + "",
        success: function (data) {
            row = '<option value>Select Sub Service</option>';
            data.forEach(element => {
                row += '<option value=' + element.id + '>' + element.name + '</option>';
            });
            $('#item_sub_service').append(row);
        },
        error: function (e) {
        }
    });
}

