
function ajaxpost(form, url) {
    var res = '';
    var xhr = new XMLHttpRequest();
    xhr.open('POST', base_url + url, false);
    xhr.onload = function () {
        res = xhr.responseText;
    };
    xhr.send(form);
    return res;
}
function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test($email);
}
function alertSimple(message) {
    $.dialog({
        title: false,
        content: message,
    });
}
