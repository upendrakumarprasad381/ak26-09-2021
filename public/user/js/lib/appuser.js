window.addEventListener("resize", function () {
    "use strict";
    //window.location.reload();
});
$(document).ready(function () {
      if (location.protocol !== "https:") {
        location.protocol = "https:";
    }
    $('.select2').select2();
});
document.addEventListener("DOMContentLoaded", function () {
    // make it as accordion for smaller screens
    if (window.innerWidth < 992) {
        // close all inner dropdowns when parent is closed
        document.querySelectorAll('.navbar .dropdown').forEach(function (everydropdown) {
            everydropdown.addEventListener('hidden.bs.dropdown', function () {
                // after dropdown is hidden, then find all submenus
                this.querySelectorAll('.submenu').forEach(function (everysubmenu) {
                    // hide every submenu as well
                    everysubmenu.style.display = 'none';
                });
            })
        });
        document.querySelectorAll('.dropdown-menu a').forEach(function (element) {
            element.addEventListener('click', function (e) {

                let nextEl = this.nextElementSibling;
                if (nextEl && nextEl.classList.contains('submenu')) {
                    // prevent opening link if link needs to open dropdown
                    e.preventDefault();

                    if (nextEl.style.display == 'block') {
                        nextEl.style.display = 'none';
                    } else {
                        nextEl.style.display = 'block';
                    }

                }
            });
        })
    }
    // end if innerWidth

});
if(method=='index'){
   var swiper = new Swiper('.catpro', {
        slidesPerView: 6
        , spaceBetween: 10
        , loop: true, // init: false,
        pagination: {
            el: '.swiper-pagination'
            , clickable: true
            , }
        , navigation: {
            nextEl: '.catpro-next'
            , prevEl: '.catpro-prev'
            , }
        , breakpoints: {
            1024: {
                slidesPerView: 5
                , spaceBetween: 15
                , }
            , 992: {
                slidesPerView: 5
                , spaceBetween: 15
                , }
            , 768: {
                slidesPerView: 3
                , spaceBetween: 1
                , }
            , 640: {
                slidesPerView: 2
                , spaceBetween: 1
                , }
            , 320: {
                slidesPerView: 1
                , spaceBetween: 1
                , }
        }
        , autoplay: {
            delay: 1500,
        }
    });

    var swiper = new Swiper('.__epdSlr', {
        slidesPerView: 5,
        spaceBetween: 15,
        loop: true,
        // init: false,

        navigation: {
            nextEl: '.__epdSlr-next',
            prevEl: '.__epdSlr-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 2,
                spaceBetween: 15,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 15,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10,
            }
        },
        autoplay: {
            delay: 2000,
        }
    });

     var swiper = new Swiper(".mySwiper", {
        spaceBetween: 15,
        loop: true,
        pagination: {
            el: ".swiper-pagination",
            dynamicBullets: true,
        },
        autoplay: {
            delay: 1500,
        }
    });
    var myCarousel = document.querySelector('#carouselExampleCaptions')
    var carousel = new bootstrap.Carousel(carouselExampleCaptions, {
        interval: 2000,
    })
    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 400);
            return false;
        });
    });
}
