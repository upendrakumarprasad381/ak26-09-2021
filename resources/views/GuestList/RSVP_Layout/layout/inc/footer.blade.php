<?php
$invitation = \App\Helpers\CommonHelper::GetguestlistinvitationById($invitationId);
if (empty($invitation->status)) {
?>
<div class="__rsvpFomWrp wd100">
    <div class="mb-3"> 
        <textarea class="form-control" id="answer" rows="3" placeholder="<?= $invitation->question ?>"></textarea>
    </div>
</div>
<div class="__revpBtnWrp wd100"> 
    <a href="javascript:void(0)" btype="red" status="2" msg="Are you sure you are not coming ?" onclick="doThisAction(this);" class="__rsBtn">I Am Not Coming</a>
    <a href="javascript:void(0)" btype="red" status="0" msg="Are you sure you want to update later ?" onclick="doThisAction(this);" class="__rsBtn">I Will Update Later</a>
    <a href="javascript:void(0)" btype="green" status="1"  msg="Are you sure want to submit ?" onclick="doThisAction(this);" class="__rsBtn">I Am Coming</a> 
</div>
<?php } ?>
</div>
</section>
<footer class="footer wd100">
    <div class="container">
        <div class="copy_right">
            © <?= date('Y') ?> AskDeema.com.  All Rights Reserved.  	
        </div>
</footer>
<script src="<?= url('user/js/jquery.min.js') ?>"></script>
<script src="<?= url('user/js/bootstrap.bundle.js') ?>"></script>
<script src="{{asset('user/js/lib/jquery-confirm.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('user/js/lib/jquery-confirm.min.css')}}">
<script>
        function doThisAction(e) {
            var status = $(e).attr('status');
            var answer = $('#answer').val();
            if ($('#answer').val() == '' && status != 0) {
                $('#answer').focus();
                return false;
            }
            var data = {status: status, UpdateInvitationConfirmation: true, answer: answer};
            data['_token'] = '{{ csrf_token() }}';
            $.confirm({
                title: $(e).attr('msg'),
                content: false,
                type: $(e).attr('btype'),
                typeAnimated: true,
                buttons: {
                    confirm: {
                        text: 'Submit',
                        btnClass: 'btn-green',
                        action: function () {
                            $.ajax({
                                url: "",
                                type: 'POST',
                                data: data,
                                async: false,
                                success: function (data) {
                                    try {
                                        var json = jQuery.parseJSON(data);
                                        if (json.status == true) {
                                            $.dialog({
                                                title: false,
                                                content: json.msg,
                                            });
                                            setTimeout(function () {
                                                window.location = '';
                                            }, 3000);


                                        }
                                    } catch (e) {
                                        alert(e);
                                    }

                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        btnClass: 'btn-warning',
                        action: function () {
                        }
                    },
                }
            });
        }
</script>