<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="<?= url('user/css/bootstrap.css') ?>">
        <link rel="stylesheet" href="<?= url('web-pages/rsvp/style1.css') ?>">
        <title>RSVP</title>
        <link rel="apple-touch-icon" sizes="57x57" href="<?= url('') ?>/user/images/favicons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= url('') ?>/user/images/favicons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= url('') ?>/user/images/favicons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= url('') ?>/user/images/favicons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= url('') ?>/user/images/favicons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= url('') ?>/user/images/favicons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= url('') ?>/user/images/favicons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= url('') ?>/user/images/favicons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= url('') ?>/user/images/favicons/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?= url('') ?>user/images/favicons/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= url('') ?>/user/images/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= url('') ?>/user/images/favicons/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= url('') ?>/user/images/favicons/favicon-16x16.png">
        <link rel="manifest" href="<?= url('user/images/favicons/manifest.json') ?>">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="images/favicons/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <script src="https://kit.fontawesome.com/9cea2b8228.js" crossorigin="anonymous"></script>
    </head>

    <body >

     	<section class="section __bgcover" style="background: url(<?= url('') ?>/web-pages/rsvp/1bg.gif) no-repeat top #e5ecde;">
          


                <?php
                $invitation = \App\Helpers\CommonHelper::GetguestlistinvitationById($invitationId);
                $event = App\Helpers\LibHelper::GetvendoreventsById($invitation->event_id);
                $subEvents = \App\Helpers\LibHelper::GetguestlisteventsById($invitation->guest_list_subevent_id);
                
                $logo = !empty($subEvents->logo) ? "image-list/" . $subEvents->logo : '';
                if (is_file(Config::get('constants.HOME_DIR') . $logo)) {
                    $logo = url($logo);
                } else {
                    $logo = url('web-pages/rsvp/layout2DefaultImage.jpg');
                }
                
                
                if (empty($invitation->status)) {
                    echo (!empty($subEvents->content) ? $subEvents->content : $defaultHTML );
                    ?>
                    <div class="container"> 
                          <div class="__rsvpFomWrp wd100">
                        <div class="mb-3">
                            <textarea class="form-control" rows="3" id="answer" placeholder="<?= $invitation->question ?>"></textarea>
                        </div>
                        <div class="__revpBtnWrp wd100">
                            <a href="javascript:void(0)" btype="red" status="2" msg="Are you sure you are not coming ?" onclick="doThisAction(this);" class="__rsBtn">I Am Not Coming</a>
                            <a href="javascript:void(0)" btype="red" status="0" msg="Are you sure you want to update later ?" onclick="doThisAction(this);" class="__rsBtn">I Will Update Later</a>
                            <a href="javascript:void(0)" btype="green" status="1"  msg="Are you sure want to submit ?" onclick="doThisAction(this);" class="__rsBtn">I Am Coming</a> 
                        </div>
                          </div>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="" style="text-align: center;margin-top: 38px;">
                        <div class="">
                            <h1> This link will no longer available. </h1>
                        </div>
                    </div>
                    <?php
                }
                ?>

            
           <div class="container">
			<div class="copy_right wd100">
			 	© 2021 AskDeema.com.  All Rights Reserved.  	
		 	</div>
		</div>
        </section>

        <style>
            .chss{
                width: 69%!important; 
            }
        </style>
    </body>
    <script src="<?= url('user/js/jquery.min.js') ?>"></script>
    <script src="<?= url('user/js/bootstrap.bundle.js') ?>"></script>
    <script src="{{asset('user/js/lib/jquery-confirm.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('user/js/lib/jquery-confirm.min.css')}}">
    <script>
        function doThisAction(e) {
            var status = $(e).attr('status');
            var answer = $('#answer').val();
            if ($('#answer').val() == '' && status != 0) {
                $('#answer').focus();
                return false;
            }
            var data = {status: status, UpdateInvitationConfirmation: true, answer: answer};
            data['_token'] = '{{ csrf_token() }}';
            $.confirm({
                title: $(e).attr('msg'),
                content: false,
                type: $(e).attr('btype'),
                typeAnimated: true,
                buttons: {
                    confirm: {
                        text: 'Submit',
                        btnClass: 'btn-green',
                        action: function () {
                            $.ajax({
                                url: "",
                                type: 'POST',
                                data: data,
                                async: false,
                                success: function (data) {
                                    try {
                                        var json = jQuery.parseJSON(data);
                                        if (json.status == true) {
                                            $.dialog({
                                                title: false,
                                                content: json.msg,
                                            });
                                            setTimeout(function () {
                                                window.location = '';
                                            }, 3000);


                                        }
                                    } catch (e) {
                                        alert(e);
                                    }

                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Cancel',
                        btnClass: 'btn-warning',
                        action: function () {
                        }
                    },
                }
            });
        }
        $('.el').css('background-image', 'url(' + '<?= $logo ?>' + ')');
    
    </script>
</html>