<div class="container">
    <div class="__tphdsc wd100">
        <h1>RSVP</h1>
        <h5>Kindly reply by Sun 31 Oct, 2021</h5>
    </div>

    <div class="__evtWrap wd100">
        <h4>Event Date</h4>

        <div class="__evtwWrdtCr">


            <div class="evtRvdate">30</div>

            <div class="__evtRvYr">

                Sunday<br />
                January <br />
                2022

            </div>
        </div>
        <h6 class="__NexaRegular">Venue: Jumeirah - Dubai</h6>
        <h6 class="__NexaRegular">Number of Attendes: 2</h6>
    </div>

    <div class="__evRsvpPtg wd100">
        <h6>Dear Deepak S Alwafaa, </h6>
        <p>It’s a Happiness to make you a part of the big step ahead. Your presence will greatly bless this holy union.
            It is our hope that you shall be present on this auspicious occasion with you and your family. We request
            you to kindly confirm your presence to attend the ceremony
        </p>
    </div>
</div>
