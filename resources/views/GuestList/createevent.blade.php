{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')
<!--begin::Entry-->

<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->

        <div class="card card-custom __gutAdpg">
            <h3 class="text-dark-100 font-weight-bolder mb-5 ">Create Guest List Event</h3> 
            <form method="post" >
                @csrf
                <div class="row"> 
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Name</label>
                        <input type="text" class="form-control form-control-lg" name="json[name]" required autocomplete="off" placeholder="">
                    </div>
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Notes</label>
                        <textarea class="form-control form-control-lg" name="json[notes]" required rows="3" placeholder=""></textarea> 
                    </div> 
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right mb-2">
                        <button type="submit" name="createEventguest" class="btn btn-primary __guestLisBnt btn-lg">Submit</button>
                    </div>

                </div>
            </form>
        </div> 


        <!--end::Container-->
    </div>
</div>
<!--begin::Entry-->
<!-- Button trigger modal -->


<!-- Modal -->

@endsection

{{-- Scripts Section --}}
@section('scripts')

@endsection
