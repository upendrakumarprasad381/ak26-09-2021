<?php
$invitation = \App\Helpers\CommonHelper::GetguestlistinvitationById($invitationId);
$event = App\Helpers\LibHelper::GetvendoreventsById($invitation->event_id);
$subEvents = \App\Helpers\LibHelper::GetguestlisteventsById($invitation->guest_list_subevent_id);

$defaultHTML = view('GuestList.RSVP_Layout.layout.index')->with([
    'eventId' => $invitation->event_id,
    'subeventId' => $invitation->guest_list_subevent_id,
    'layoutId' => 1,
    'page_title' => 'Guest List Layout Settings',
        ]);
?>
@include('GuestList.RSVP_Layout.layout.inc.header')
<?php
if (empty($invitation->status)) {
    echo (!empty($subEvents->content) ? $subEvents->content : $defaultHTML );
} else {
    ?>
    <div class="" style="text-align: center;margin-top: 38px;">
        <div class="">
            <h1> This link will no longer available. </h1>
        </div>
    </div>

    <?php
}
?>


@include('GuestList.RSVP_Layout.layout.inc.footer')




<?php
/*
  ?>
  <!--begin::Entry-->
  <!DOCTYPE html>
  <html lang="en">
  <head>
  <meta charset="utf-8" />
  <title>Ask Deema Wedding Invite Form  </title>
  <meta name="description" content="" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

  <link rel="stylesheet" href="{{asset('user/js/lib/jquery-confirm.min.css')}}">
  <link href="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/wedding_theme/img/favicon.ico" rel="icon">
  <link rel="stylesheet" href="https://demo.softwarecompany.ae/ask_deema/css/bootstrap.css">
  <link href="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/assets/css/wedding_invite.css" rel="stylesheet" type="text/css"/>
  </head>


  <body>
  <?php
  $invitation = \App\Helpers\CommonHelper::GetguestlistinvitationById($invitationId);
  $event = App\Helpers\LibHelper::GetvendoreventsById($invitation->event_id);
  //        echo '<pre>';
  //        print_r($invitation);
  //        print_r($event);
  //        exit;
  ?>

  <div class="container">


  <?php if (empty($invitation->status)) { ?>
  <div class="__invsBg">
  <div class="__invsBgIbzo">
  <div class="__surwhomwrp">
  <div class="__tozlrbozLt">
  <h1> RSVP </h1>
  <h2>Kindly reply by <?= date('D d M, Y', strtotime($invitation->date_of_rsvp)) ?></h2>
  </div>
  <div class="__tozlrbozrt">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  <td  class="__tdtiz">Event Date: <strong><?= date('D d M, Y', strtotime($event->event_date)) ?></strong></td>
  </tr>
  <tr>
  <td  class="__tdtiz">Venue: <strong><?= $event->event_location ?></strong>
  </td>
  </tr>
  <tr>
  <td  class="__tdtiz"> Number of Attendes:  <strong><?= $invitation->noof_attendes ?></strong></td>
  </tr>
  </table>
  </div>
  </div>
  <div class="__wctxt">
  <b>Dear <?= $invitation->name ?>,</b> <br/>
  It’s a Happiness to make you a part of the big step ahead. Your presence will greatly bless this holy union. It is our hope that you shall be present on this auspicious occasion with you and your family. We request you to kindly confirm your presence to attend the ceremony
  </div>
  <div class="__infrwd wd100">

  <div class="row">

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-3">
  <label><?= $invitation->question ?></label>
  <textarea class="form-control" id="answer" rows="3"></textarea>
  </div>

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-3  ">

  <button style="margin-left: 22px;" status="1" btype="green" msg="Are you sure want to submit ?" onclick="doThisAction(this);" type="button" class="btn btn-success float-end __ntfgdfg">I Am Coming</button>
  <button type="button" btype="red" status="2" msg="Are you sure you are not coming ?" onclick="doThisAction(this);" class="btn btn-danger float-end __ntfgdfg">I Am Not Coming</button>

  <button style="margin-right: 20px;" type="button" btype="red" status="0" msg="Are you sure you want to update later ?" onclick="doThisAction(this);" class="btn btn-warning float-end __ntfgdfg">I Will Update Later</button>
  </div>

  </div>

  </div>
  </div>
  </div>
  <?php } else {
  ?>
  <div class="" style="text-align: center;margin-top: 38px;">
  <div class="">
  <h1> This link will no longer available. </h1>
  </div>
  </div>

  <?php }
  ?>




  </div>


  </body>

  <script src="{{asset('user/js/jquery.min.js')}}"></script>
  <script src="{{asset('user/js/lib/jquery-confirm.min.js')}}"></script>
  <script>
  function doThisAction(e) {
  var status = $(e).attr('status');
  var answer = $('#answer').val();
  if ($('#answer').val() == '' && status != 0) {
  $('#answer').focus();
  return false;
  }
  var data = {status: status, UpdateInvitationConfirmation: true, answer: answer};
  data['_token'] = '{{ csrf_token() }}';
  $.confirm({
  title: $(e).attr('msg'),
  content: false,
  type: $(e).attr('btype'),
  typeAnimated: true,
  buttons: {
  confirm: {
  text: 'Submit',
  btnClass: 'btn-green',
  action: function () {
  $.ajax({
  url: "",
  type: 'POST',
  data: data,
  async: false,
  success: function (data) {
  try {
  var json = jQuery.parseJSON(data);
  if (json.status == true) {
  $.dialog({
  title: false,
  content: json.msg,
  });
  setTimeout(function () {
  window.location = '';
  }, 3000);


  }
  } catch (e) {
  alert(e);
  }

  }
  });
  }
  },
  cancel: {
  text: 'Cancel',
  btnClass: 'btn-warning',
  action: function () {
  }
  },
  }
  });
  }
  </script>
  </html>
 */
?>



