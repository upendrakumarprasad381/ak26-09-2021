{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')
<?php
$event = App\Helpers\LibHelper::GetvendoreventsById($eventId);
?>
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/event/' . $eventId . '') }}" class="text-muted">{{ $event->event_name }}</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="<?= url("guest-list/$eventId") ?>" class="text-muted">Guest List</a>
</li>

@endsection
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->


        <div class="__guestListWrp wd100">
            <div class="row">
                <?php
                $select = ",VE.event_location,VE.event_date,VE.document";
                $join = " LEFT JOIN vendor_events VE ON VE.id=GLE.event_id";
                $count = ",(SELECT SUM(noof_attendes)  FROM `guest_list_invitation` WHERE `guest_list_subevent_id` = GLE.id AND event_id='$eventId') AS total_g";
                $Sql = "SELECT GLE.* $select $count FROM `guest_list_events` GLE $join WHERE GLE.event_id='$eventId'";

                $baseDir = Config::get('constants.HOME_DIR');
                $data = \App\Database::select($Sql);
                for ($i = 0; $i < count($data); $i++) {
                    $d = $data[$i];

                    $baseFile = "storage/" . $d->document;
                    if (is_file($baseDir . $baseFile)) {
                        $d->document = url($baseFile);
                    } else {
                        $d->document = url(Config::get('constants.DEFAULT_LOGO_VENDOR'));
                    }

                    $url = url("guest-list/$eventId/sub-event/$d->id");
                    ?>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="__gltkBz wd100 active"> 
                            <h4><a href="<?= $url ?>"><?= $d->name ?></a></h4>

                            <div class=" __gltkBzImg wd100">
                                <a href="<?= $url ?>"><img class="img-fluid" src="<?= $d->document ?>"></a>
                            </div>
                            <div class="__gltkBzDrcp wd100"> 

                                <div class="__guestDat  wd100">
                                    <?= date('d M Y', strtotime($d->event_date)) ?> 

                                </div>
                                <div class="__guestcoutol wd100">
                                    Total Attendes : <span class="guesttotal"><?= $d->total_g ?></span>
                                </div>
                                <div class="__guestlocation wd100">{{$d->event_location}}</div> 
                            </div> 
                        </div> 
                    </div>
                <?php } ?>
                <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                    <a href="<?= url("guest-list/$eventId/create-event") ?>" style="display: none;">  <button type="button" class="btn btn-primary __gustsbBtn">Create New </button></a>
                </div>
            </div>
        </div>

        <!--end::Container-->
    </div>
</div>
<!--begin::Entry-->


@endsection

{{-- Scripts Section --}}
@section('scripts')

@endsection
