{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')
<!--begin::Entry-->
<?php
$event = App\Helpers\LibHelper::GetvendoreventsById($eventId);
$subEvents = \App\Helpers\LibHelper::GetguestlisteventsById($subeventId);
?>
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/event/' . $eventId . '') }}" class="text-muted">{{ $event->event_name }}</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="<?= url("guest-list/$eventId") ?>" class="text-muted">Guest List</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="<?= url("guest-list/$eventId/sub-event/$subeventId") ?>" class="text-muted">Guest List Dashboard</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="" class="text-muted">Layout Settings</a>
</li>
@endsection

<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->

        <div class="__tmxDesignWrp wd100">


            <h3 class="mb-5">Find Your Design</h3>

            <div class="row">
                <?php
                $Sql = "SELECT * FROM `rsvp_design` WHERE status=0 ORDER BY id ASC";
                $list = App\Database::select($Sql);


                for ($i = 0; $i < count($list); $i++) {
                    $d = $list[$i];
                    $basePath = url("web-pages/$d->logo");
                    $url = url("guest-list/$eventId/layout-settings-content/$subeventId/$d->id");
                    ?>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 __tmxDesignbZ">
                        <div class="wd100 __tmxDesigWrz">
                            <div class="__tmxDesiimg wd100">
                                <img src="<?= $basePath ?>" class="img-fluid">
                            </div>
                            <div class="__tmxDesiTex wd100 ">
                                <h5><?= $d->title ?></h5>
                                <h6><?= $d->description ?> <?= !empty($subEvents->layout_id) && $subEvents->layout_id == $d->id ? '<a class="selicon"> <i class="fas fa-check"></i></a>' : '' ?> </h6>



                            </div>
                            <div class="wd100 __temWebBnt_item">
                                <a href="<?= $url ?>" > <button class="btn btn-secondary __temWebBnt">Update</button></a>
                            </div>
                        </div>
                    </div>
                <?php } ?>








            </div>





        </div>



        <!--end::Container-->
    </div>
</div>
<!--begin::Entry-->
<style>
    .selicon{
        float: right;
    }
    .selicon i{
        color: #4caf50;
        font-size: 23px;
    }
</style>

@endsection

{{-- Scripts Section --}}
@section('scripts')

@endsection
