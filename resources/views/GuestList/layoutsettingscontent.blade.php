{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')
<!--begin::Entry-->
<?php
$event = App\Helpers\LibHelper::GetvendoreventsById($eventId);
$subEvents = \App\Helpers\LibHelper::GetguestlisteventsById($subeventId);
?>
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/event/' . $eventId . '') }}" class="text-muted">{{ $event->event_name }}</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="<?= url("guest-list/$eventId") ?>" class="text-muted">Guest List</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="<?= url("guest-list/$eventId/sub-event/$subeventId") ?>" class="text-muted">Guest List Dashboard</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="" class="text-muted">Layout Settings</a>
</li>
@endsection

<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->

        <div class="__tmxDesignWrp wd100">


            <h3 class="mb-5">Layout Content Update</h3>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                    <label class="form-label">Logo</label>
                    <input type="file" id="logo" class="form-control form-control-lg" placeholder="" >
                </div>


                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                    <label class="form-label">Content</label>
                    <textarea class="form-control form-control-lg summernote" id="content" name="json[content]"  required="" rows="3" placeholder="" spellcheck="false"><?= !empty($subEvents->content) ? $subEvents->content : $dafaultHTML ?></textarea> 
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                    <button type="button" onclick="submitMyDatacontent();" class="btn btn-primary">Submit</button>
                    <button type="button" onclick="resetMyLayou();" class="btn btn-primary">Reset Content</button>
                </div>
            </div>





        </div>



        <!--end::Container-->
    </div>
</div>
<!--begin::Entry-->
<style>
    .selicon{
        float: right;
    }
    .selicon i{
        color: #4caf50;
        font-size: 23px;
    }
</style>

@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
    function resetMyLayou() {
        var data = {resetMyLayou: '1', content: '', _token: "{{ csrf_token() }}"};
        $.ajax({
            url: "",
            cache: false,
            data: data,
            async: false,
            type: 'POST',
            success: function (res) {
                if (res.type == "success") {
                    window.location = '';
                }
                console.log(res);
            }
        });
    }
    function submitMyDatacontent() {
        var form = new FormData();
        var content = $('#content').val();
        form.append('content', content);
        form.append('submitMyDatacontent', '1');
        form.append('_token', "{{ csrf_token() }}");

        var fileInput = document.querySelector('#logo');
        form.append('logo', fileInput.files[0]);


        var json = ajaxpost(form, "");
        json = jQuery.parseJSON(json);

        if (json.type == "success") {
            window.location = json.url;
        }

    }
</script>
@endsection
