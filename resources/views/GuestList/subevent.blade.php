{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')

<?php
$event = App\Helpers\LibHelper::GetvendoreventsById($eventId);
?>
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/event/' . $eventId . '') }}" class="text-muted">{{ $event->event_name }}</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="<?= url("guest-list/$eventId") ?>" class="text-muted">Guest List</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="<?= url("guest-list/$eventId/sub-event/$subeventId") ?>" class="text-muted">Guest List Dashboard</a>
</li>
@endsection
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->

       	<div class="__guestListWrp wd100"> 

            <div class="row __sublk">

                <?php
                $count = ",(SELECT COUNT(id)  FROM `guest_list_invitation` WHERE `guest_list_subevent_id` = '$subeventId' AND event_id='$eventId') AS total_g";
                $confirm = ",(SELECT SUM(noof_attendes)  FROM `guest_list_invitation` WHERE `guest_list_subevent_id` = '$subeventId' AND event_id='$eventId' AND status=1) AS confirm";
                $not_coming = ",(SELECT COUNT(id)  FROM `guest_list_invitation` WHERE `guest_list_subevent_id` = '$subeventId' AND event_id='$eventId' AND status=2) AS not_coming";
                $Sql = "SELECT COUNT(id)  AS pending $count $confirm $not_coming FROM `guest_list_invitation` WHERE `guest_list_subevent_id` = '$subeventId' AND event_id='$eventId' AND status=0";


                $data = \App\Database::selectSingle($Sql);
                ?>


                <div class="col-lg-3 col-md-4 col-sm-12 mb-5"> 
                    <a href="javascript:void(0)" tabId="" class="card card-custom card-stretch clckEvent __active">
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans"><?= !empty($data->total_g) ? $data->total_g : '0' ?></span>
                            </div>
                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">All Guests</div>
                        </div> 
                    </a>
                </div>


                <div class="col-lg-3 col-md-4 col-sm-12 mb-5"> 
                    <a href="javascript:void(0)" tabId="pending" class="card card-custom clckEvent card-stretch ">
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans"><?= !empty($data->pending) ? $data->pending : '0' ?></span>
                            </div>
                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Pending</div>
                        </div> 
                    </a>
                </div>


                <div class="col-lg-3 col-md-4 col-sm-12 mb-5"> 
                    <a href="javascript:void(0)" tabId="confirmed" class="card card-custom clckEvent card-stretch">
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans"><?= !empty($data->confirm) ? $data->confirm : '0' ?></span>
                            </div>
                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Coming</div>
                        </div> 
                    </a>
                </div>


                <div class="col-lg-3 col-md-4 col-sm-12 mb-5"> 
                    <a href="javascript:void(0)" tabId="not-coming" class="card card-custom clckEvent card-stretch">
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans"><?= !empty($data->not_coming) ? $data->not_coming : '0' ?></span>
                            </div>
                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Not Coming</div>
                        </div> 
                    </a>
                </div>





















            </div>


            <div class="wd100 __tpExpBtnWrp">
                <div class="d-flex">

                    <div class="__ppserch d-flex align-items-center py-3 py-sm-0 px-sm-3 " style="    width: 74%;">

                        <input type="text" class="form-control border-0 font-weight-bold pl-2" id="search" placeholder="Search...">
                        <span class="svg-icon svg-icon-lg">

                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"></rect>
                            <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                            <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"></path>
                            </g>
                            </svg>

                        </span>
                    </div>


                    <div class=" __topSwitchBtn">
                        <button class="btn btn-secondary " onclick="loadGuest();" type="button" >Search</button>
                        <a href="<?= url("guest-list/$eventId/layout-settings/$subeventId/") ?>"> <button class="btn btn-secondary "  type="button" >Layout Settings </button></a>

                    </div>



                </div>
            </div> 


            <div class="row _gstListSbbnwrp">
                <div class="__vtrTpBtnWrp d-flex wd100"> 
                    <a data-toggle="modal" data-target="#importguestpoup" href="javascript:void(0)" class="__vrttbtn ">Import Guest List</a>
                    <a href="<?= url("guest-list/$eventId/sub-event/$subeventId/export") ?>" class="__vrttbtn">Export Guest List  </a>

                    <a href="<?= url("guest-list/$eventId/sub-event/$subeventId/create") ?>" class="__vrttbtn ">Add Guest</a> 

                </div>
            </div>



            <div class="__guestListTavw __scopeWorkTbvw  wd100"> 
                <div class="table-responsive"> 
                    <table class="table" id="tableThis">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col" class="text-center __brdsLt">S.N</th>
                                <th scope="col">Guest Name</th>
                                <th scope="col" class="text-center">Status</th>
                                <th scope="col" class="text-center">Question</th>
                                <th scope="col" class="text-center">No Of Attendees</th>
                                <th scope="col" class="text-center">Contact No</th>
                                <th scope="col" class="text-center __brdsRt">Action</th> 
                            </tr>
                        </thead>
                        <tbody>


                        </tbody>
                    </table> 

                </div>
            </div>

            <div class="wd100 text-right mt-5">
                <a href="<?= url("guest-list/$eventId/sub-event/$subeventId/create") ?>"><button type="button" class="btn btn-primary __guestLisBnt">Add Guest</button></a>
            </div>



        </div>


        <!--end::Container-->
    </div>
</div>
<!--begin::Entry-->
<!-- Button trigger modal -->


<!-- Modal -->



<div class="modal fade" id="importguestpoup" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Import Guest List</h5>
            </div>
            <form method="post" action="" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label" style="width: 100%;">File <a href="<?= url('img/guest-list-import-layout.xlsx') ?>" style="float: right;">Download Formate</a></label>
                            <input name="import_guest" class="form-control form-control-lg" type="file" autocomplete="off" required>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="importguestpoupBtn"  class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
    function updateguestListstatus(e) {
        var iscompleted = $(e).is(':checked') ? 1 : 0;
        var data = {_token: "{{ csrf_token() }}"};
        data['iscompleted'] = iscompleted;
        data['primaryid'] = $(e).attr('primaryid');
        data['updateguestListstatus'] = '1';
        ad();
        function ad() {
            $.ajax({
                url: "",
                cache: false,
                data: data,
                async: false,
                type: 'POST',
                success: function (res) {
                    res = jQuery.parseJSON(res);
                    if (res.status == true) {
                        $('statusId' + data.primaryid).html(res.HTML);
                    }
                }
            });
        }
    }
    function daeleteGuest(e) {
        var data = {_token: "{{ csrf_token() }}"};
        data['primaryid'] = $(e).attr('primaryid');
        data['daeleteGuest'] = '1';

        Swal.fire({
            title: "Are you sure?",
            text: false,
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                ad();
            } else if (result.dismiss === "cancel") {
                Swal.fire(
                        "Cancelled",
                        false,
                        "error"
                        )
            }
        });
        function ad() {
            $.ajax({
                url: "",
                cache: false,
                data: data,
                async: false,
                type: 'POST',
                success: function (res) {
                    res = jQuery.parseJSON(res);
                    if (res.status == true) {
                        location.reload();
                    }
                }
            });
        }
    }
    function sendreminderGuest(e) {
        var data = {_token: "{{ csrf_token() }}"};
        data['primaryid'] = $(e).attr('primaryid');
        data['sendreminderGuest'] = '1';

        Swal.fire({
            title: "Are you sure?",
            text: false,
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, send it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                ad();
            } else if (result.dismiss === "cancel") {
                Swal.fire(
                        "Cancelled",
                        false,
                        "error"
                        )
            }
        });
        function ad() {
            $.ajax({
                url: "",
                cache: false,
                data: data,
                async: false,
                type: 'POST',
                success: function (res) {
                    res = jQuery.parseJSON(res);
                    if (res.status == true) {
                        Swal.fire(
                                "Successfully sent",
                                false,
                                "success"
                                )
                    }
                }
            });
        }
    }
    $('.clckEvent').click(function () {
        $('.clckEvent').removeClass('__active');
        $(this).addClass('__active');
        loadGuest();
    });
    function loadGuest() {
        var tabId = $('.clckEvent.__active').attr('tabId');
        var data = {_token: "{{ csrf_token() }}", autoload: true, tabId: tabId};
        data['search'] = $('#search').val();
        $.ajax({
            url: "",
            cache: false,
            data: data,
            async: false,
            type: 'POST',
            success: function (res) {
                res = jQuery.parseJSON(res);
                $('#tableThis tbody').html(res.HTML);
            }
        });
    }
    loadGuest();
</script>
@endsection
