{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')
<!--begin::Entry-->
<?php
$dArray = \App\Helpers\CommonHelper::GetguestlistinvitationById($Id);

$event = App\Helpers\LibHelper::GetvendoreventsById($eventId);
?>
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/event/' . $eventId . '') }}" class="text-muted">{{ $event->event_name }}</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="<?= url("guest-list/$eventId") ?>" class="text-muted">Guest List</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="<?= url("guest-list/$eventId/sub-event/$subeventId") ?>" class="text-muted">Guest List Dashboard</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="" class="text-muted">Create</a>
</li>
@endsection
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->

        <div class="card card-custom __gutAdpg">


            <h3 class="text-dark-100 font-weight-bolder mb-5 ">Add a Guest</h3> 
            <form method="post">
                @csrf
                <div class="row"> 
                    <input type="hidden" name="json[id]" value="<?= !empty($dArray->id) ? $dArray->id : '' ?>">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Name of Guest</label>
                        <input type="text" name="json[name]" value="<?= !empty($dArray->name) ? $dArray->name : '' ?>" class="form-control form-control-lg" required autocomplete="off" placeholder="">
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Email</label>
                        <input type="email" class="form-control form-control-lg" name="json[email]" value="<?= !empty($dArray->email) ? $dArray->email : '' ?>" required autocomplete="off" placeholder="">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Phone No.</label>
                        <input type="text" name="json[phone]"  value="<?= !empty($dArray->phone) ? $dArray->phone : '' ?>" class="form-control form-control-lg" required autocomplete="off" placeholder="+971563965086">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Alternate No.</label>
                        <input type="text" name="json[alternateno]" value="<?= !empty($dArray->alternateno) ? $dArray->alternateno : '' ?>" class="form-control form-control-lg" required autocomplete="off" placeholder="">
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Number Of Attendes <small>(Max Allowed : 100)</small></label>
                        <input type="text" name="json[noof_attendes]" value="<?= !empty($dArray->noof_attendes) ? $dArray->noof_attendes : '' ?>" value="1" class="form-control form-control-lg" required autocomplete="off" placeholder="">
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Question For Guest<small>(Special Songs to add ?)</small></label>
                        <input type="text" name="json[question]" list="browsers" value="<?= !empty($dArray->question) ? $dArray->question : '' ?>" class="form-control form-control-lg" autocomplete="off" placeholder="">
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Date To RSVP  </label>
                        <div class="input-group date" >
                            <input type="text" id="date_of_rsvp" value="<?= !empty($dArray->date_of_rsvp) ? date('Y-m-d', strtotime($dArray->date_of_rsvp)) : '' ?>" class="form-control datepickerYMD" autocomplete="off"  name="json[date_of_rsvp]" required readonly />
                            <div class="input-group-append" >
                                <span class="input-group-text">
                                    <i class="ki ki-calendar"></i>
                                </span>
                            </div>
                        </div>

                    </div> 
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Any Remarks</label>
                        <textarea class="form-control form-control-lg" name="json[remarks]"  rows="3" autocomplete="off" placeholder=""><?= !empty($dArray->remarks) ? $dArray->remarks : '' ?></textarea> 
                    </div> 
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right mb-2">
                        <button name="createguestlistBtn" type="submit" class="btn btn-primary __guestLisBnt btn-lg">Send Invitation</button>
                    </div>
                </div>
            </form>
        </div> 


        <!--end::Container-->
    </div>
</div>
<!--begin::Entry-->
<!-- Button trigger modal -->
<datalist id="browsers">
    <?php
    $Sql = "SELECT question FROM `guest_list_invitation` WHERE question!='' GROUP BY question";
   $data= \App\Database::select($Sql);
   for($i=0;$i<count($data);$i++){
     ?><option value="<?= $data[$i]->question ?>"><?php
   }
    ?>
    
</datalist>

<!-- Modal -->

@endsection

{{-- Scripts Section --}}
@section('scripts')

@endsection
