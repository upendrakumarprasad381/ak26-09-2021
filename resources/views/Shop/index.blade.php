@include('user.view.includes.header')



<!--Inner Banner -->

<section class="inner-banner wd100">
    <div class="breadcrumb-area" style="background-image: url(<?= url('/img/vendors-bg.jpg') ?>)" data-overlay="dark"data-opacity="7">
        <div class="container pt-150 pb-150 position-relative">
            <div class="row">
                <div class="col-xl-5">
                    <div class="breadcrumb-title">
                        <h3 class="title">Ask Deema’s Shop
                            For Everyone</h3>
                        <span class="sub-title">Ask Deema shop is created to support event home businesses and 
                            minimize their hassle to join the ecommerce world. All you need as 
                            features to manage and run an ecommerce shop are provided by 
                            Askdeema's You can review the detailed features below:</span>
                        <div class="button-ask">
                            <div class="button">
                                <a style="    cursor: none;" class="btn-one ask-ad" href="javascript:void(0)">Coming Soon 
                                    <!--Start Your Journy With AD-->
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="breadcrumb-nav">
                <ul>
                    <li><a href="<?= url('/') ?>">Home</a></li>
                    <li class="active">Shop Login</li>
                </ul>
            </div>

        </div>
    </div>
</section>


<!-- End Inner Banner -->

<!-- Event Planners-->
<section class="eventplanner-one eventplanner--one__home-two thm-gray-bg service-one__service-page">
    <div class="vendor-one__block">
        <!-- <div class="block-title text-center">
          <h2 class="block-title__title text-uppercase">Top Wedding Vendor Categories</h2>
            <div class="block-title__line"></div>
        </div> -->
        <div class="container">
            <div class="row">

                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-finance"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Finance Tool</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->

                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-calendar"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Calender</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-relationship"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Customer Relationships</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-shopping-bag"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Products</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-store"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Shop Front</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-report"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Reports</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-shipped"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Delivery Cycle </a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->

                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-receipt"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">My Orders</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-chat"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Conversations</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-market"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Commercial</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-contract"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Contracts</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.service-one__block -->
</section><!-- /.service-one -->

<!-- End event Planner Sign -->




@include('user.view.includes.footer')
