{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')
<!--begin::Entry-->
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="{{ url('/to-do-list') }}" class="text-muted">To Do List</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="" class="text-muted">Create</a>
</li>

@endsection
<?php
//\App\Helpers\CommonHelper::updateMycalender($calenderId = 3, $type='TODO_LIST', $eventId='20', $primaryId='1', $title='Test 1 2', $eventDate='2021-09-17');
//exit;
if (isset($_POST['addListBtn'])) {
    $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
    $json['created_by'] = Auth::user()->id;
    $json['updated_at'] = date('Y-m-d H:i:s');
    $json['created_at'] = $json['updated_at'];
    \App\Database::insert('todo_filter_list', $json);
}
if (isset($_POST['addsubListBtn'])) {
    $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];

    $json['updated_at'] = date('Y-m-d H:i:s');
    $json['created_at'] = $json['updated_at'];
    \App\Database::insert('todo_filter_sublist', $json);
}
$todo = \App\Helpers\CommonHelper::GettodolistById($Id);
?>
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->

        <div class="card card-custom __gutAdpg">

            <?php if (!empty($message)) { ?>
                <div class="alert alert-warning" role="alert">
                    {{$message}}
                </div>
            <?php } ?>
            <h3 class="text-dark-100 font-weight-bolder mb-5 ">Create To Do List</h3> 
            <form method="post" action="<?= url('to-do-list/store') ?>" id="formaddingevent" onsubmit="return false;">
                @csrf
                <div class="row"> 
                    <input type="hidden" name="json[id]" id="pid" value="<?= !empty($todo->id) ? $todo->id : '' ?>">


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Items Name</label>
                        <textarea class="form-control form-control-lg" name="json[items_name]" id="items_name" required rows="3" placeholder=""><?= !empty($todo->items_name) ? $todo->items_name : '' ?></textarea> 
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Event</label>
                        <select name="json[event_id]" id="event_id"  class="form-control form-control-lg" required>
                            <option value="0">--select--</option>
                            <?php
                            $event_id = !empty($todo->event_id) ? $todo->event_id : '';
                           $data= App\Helpers\LibHelper::GeteventByuserId(Auth::user()->id);
                            for ($i = 0; $i < count($data); $i++) {
                                $d = $data[$i];
                                ?><option <?= $event_id == $d->id ? 'selected' : '' ?> value="<?= $d->id ?>"><?= $d->event_name ?></option><?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">List Name <a href="javascript:void(0)" onclick="$('#staticBackdrop').modal('show');"><span class="label label-success label-inline mr-2">Add New</span></a></label>
                        <select class="form-control form-control-lg" name="json[list_id]" id="list_id" required placeholder="">
                            <option value="">--select--</option>
                            <?php
                            $list_id = !empty($todo->list_id) ? $todo->list_id : '';
                            $Sql = "SELECT * FROM `todo_filter_list` WHERE status=0 AND created_by='" . Auth::user()->id . "'";
                            $list = \App\Database::select($Sql);
                            for ($i = 0; $i < count($list); $i++) {
                                $d = $list[$i];
                                ?>  <option <?= $list_id == $d->id ? 'selected' : '' ?> value="<?= $d->id ?>"><?= $d->list_name ?></option><?php
                            }
                            ?>
                        </select>
                    </div>

                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Sub list name <a href="javascript:void(0)" onclick="$('#addsublistmodal').modal('show');"><span class="label label-success label-inline mr-2">Add New</span></a></label>
                        <select class="form-control form-control-lg" name="json[sub_list]" id="sub_list"  placeholder="">
                            <option value="0">--select--</option>
                            <?php
                            if (!empty($list_id)) {
                                $data = \App\Helpers\CommonHelper::GetsubList($list_id);
                                for ($i = 0; $i < count($data); $i++) {
                                    $d = $data[$i];
                                    ?>  <option <?= $todo->sub_list == $d->id ? 'selected' : '' ?> value="<?= $d->id ?>"><?= $d->sub_list_name ?></option><?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4 ">

                        <label class="form-label">Date</label>

                        <div class="input-group">
                            <input  class="form-control form-control-lg datepickerYMD" value="<?= !empty($todo->date) ? date('Y-m-d', strtotime($todo->date)) : '' ?>" id="date" type="text" autocomplete="off" >

                        </div>
                    </div>

                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label style="width: 100%;" class="form-label">Attachment <small>(.pdf,.docx,.png,.jpg)</small>

                        </label>
                        <input  class="form-control form-control-lg" id="attachmentData" type="file" autocomplete="off" >

                        <div class="wd100">
                            <div class="__clthubk">
                                <?php
                                $basePath = "image-list/" . (!empty($todo->attachment) ? $todo->attachment : '');
                                if (is_file(Config::get('constants.HOME_DIR') . $basePath)) {
                                    ?> 


                                    <a class="__clthubkBoz" target="_blank" href="<?= url($basePath) ?>"> <i class="fas fa-file-alt"></i>
                                        <br/> View File</a><?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>









                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right mb-2">
                        <button type="button" id="addsubListBtn" onclick="validateForm();" class="btn btn-primary __guestLisBnt btn-lg"><?= empty($Id) ? 'Create' : 'Update' ?></button>
                        <button type="button"  id="plzwait" style="display: none;" class="btn btn-primary">Please wait..</button>
                    </div>




                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" <?= !empty($todo->assign_contact) ? 'checked' : '' ?>   onclick="ASSIGN.assigncontactBoxAction();" id="assigncontactBox">
                            <label class="form-check-label" for="assigncontactBox">
                                <h4> Assign Contacts</h4>
                            </label>
                        </div>
                    </div>




                    <div class="__liXZe col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-12" style="display: <?= !empty($todo->assign_contact) ? 'block' : 'none' ?>" id="allreadcontectlist">
                        <?= App\Helpers\HtmlHelper::loadHTMLToDoListAssignContacts($Id); ?>
                    </div>















                </div>
            </form>
        </div> 



        <!--end::Container-->
    </div>
</div>
<!--begin::Entry-->
<!-- Button trigger modal -->

<style>
    .venlist{
        cursor: pointer;
    }
</style>
<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Add New List</h5>
            </div>
            <form method="post" action="">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Name</label>
                            <input name="json[list_name]" class="form-control form-control-lg" type="text" autocomplete="off" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="addListBtn" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="addsublistmodal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Add New Sub List</h5>
            </div>
            <form method="post" action="">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">List Name <a href="javascript:void(0)" onclick="$('#staticBackdrop').modal('show');"><span class="label label-success label-inline mr-2">Add New</span></a></label>
                            <select class="form-control form-control-lg" name="json[list_id]" required placeholder="">
                                <option value="">--select--</option>
                                <?php
                                $Sql = "SELECT * FROM `todo_filter_list` WHERE status=0 AND created_by='" . Auth::user()->id . "'";
                                $list = \App\Database::select($Sql);
                                for ($i = 0; $i < count($list); $i++) {
                                    $d = $list[$i];
                                    ?>  <option value="<?= $d->id ?>"><?= $d->list_name ?></option><?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Name</label>
                            <input name="json[sub_list_name]" class="form-control form-control-lg" type="text" autocomplete="off" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="addsubListBtn"  class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>








@endsection

{{-- Scripts Section --}}
@section('scripts')

<script>
    var ASSIGN = (function () {
        var fn = {};
        fn.assigncontactBoxAction = function () {
            if ($('#assigncontactBox').is(':checked')) {
                $('#allreadcontectlist').show();
            } else {
                $('#allreadcontectlist').hide();
                $('#addedlist').html('');
            }
        }
        fn.getGuestlistForToDo = function () {
            $('#assingLoader').show();
            $('#assignList').hide();
            setTimeout(function () {
                var data = {_token: "{{ csrf_token() }}"};
                data['search'] = $('#mod_search').val();

                data['categoryId'] = $('#categoryId').val();
                $.ajax({
                    url: "{{url('/to-do-list/getGuestlistForToDo')}}",
                    cache: false,
                    data: data,
                    async: true,
                    type: 'POST',
                    success: function (res) {
                        res = jQuery.parseJSON(res);
                        if (res.status == true) {
                            $('#assingLoader').hide();
                            $('#assignList').show();
                            $('#assignList').html(res.HTML);
                        }
                    }
                });
            });
        }
        fn.setGuestlistForToDo = function (e) {
            var json = $(e).attr('json');
            json = jQuery.parseJSON(atob(json));
            var html = '';

            html = html + '                  <div class="col-lg-3 col-md-6 col-sm-6 addedlistguest" userId="' + json.id + '" id="guestId-' + json.id + '">';
            html = html + '                        <div class="__grcadbz wd100">';
            html = html + '                            <div class="__selcBntWrp">';
            html = html + '                                <div class="__selcBn">';
            html = html + '                                    <i class="fas fa-check-circle"></i>';
            html = html + '                                </div>';
            html = html + '                                <div class="__CloseBn" userId="' + json.id + '" onclick="ASSIGN.removeGuestlistForToDo(this);">';
            html = html + '                                    <i class="far fa-times-circle"></i>';
            html = html + '                                </div> ';
            html = html + '                            </div>';
            html = html + '                            <div class="__grcadbzImg">';
            html = html + '                                <img class="img-fluid" src="' + json.logo + '">';
            html = html + '                            </div>';
            html = html + '                            <div class="wd100">';
            html = html + '                                <h4 class="mt-2"><a href="javascript:void(0)">' + json.first_name + '</a></h4>';
            html = html + '                                <h5 class="__todLilcotin mb-2"><a href="javascript:void(0)">' + json.address + '</a> </h5>';
            html = html + '                                <div class="wd100 __todoCall __open_sans ">';
            html = html + '                                    <a href="javascript:void(0)">' + json.phone + '</a>';
            html = html + '                                </div>';
            html = html + '                                <div class="wd100 __todoEmail __open_sans">';
            html = html + '                                    <a href="javascript:void(0)">' + json.email + '</a>';
            html = html + '                                </div>';
            html = html + '                            </div>';
            html = html + '                        </div>';
            html = html + '                    </div>';
            $('#guestId-' + json.id).remove();
            $('#addedlist').append(html);
            $('html, body').animate({
                scrollTop: (parseInt($("#addedlist").offset().top) - 200)
            }, 0);
        }
        fn.removeGuestlistForToDo = function (e) {
            var userId = $(e).attr('userId');
            $('#guestId-' + userId).remove();
        }
        fn.openModalforAssignContact = function (e) {
            var data = {_token: "{{ csrf_token() }}"};
            data['todoId'] = $(e).attr('todoid');
            data['modalforAssignContact'] = '1';
            $.ajax({
                url: "{{ url('/to-do-list') }}",
                cache: false,
                data: data,
                async: true,
                type: 'POST',
                success: function (res) {
                    res = jQuery.parseJSON(res);
                    if (res.status == true) {
                        $('#assigncontst').html(res.HTML);
                        $('#opentodoId').val($(e).attr('todoid'));
                        $('#exampleModalconactd').modal('show');
                    }
                }
            });
        }
        fn.openModaladdContact = function () {
            //   $('#exampleModalconactd').modal('hide');
            setTimeout(function () {
                $('#addguestcontactpoup').modal('show');
            }, 100);
        }
        fn.addnewcontact = function () {
            var form = new FormData();
            form.append('_token', '{{ csrf_token() }}');
            var phoneWithcode = $('#mod_phone_code').val() + $("#mod_phone").val();

            form.append('json[first_name]', $('#mod_first_name').val());
            form.append('json[last_name]', $('#mod_last_name').val());
            form.append('json[email]', $('#mod_email').val());
            form.append('json[phone]', phoneWithcode);




            if ($('#mod_first_name').val() == '') {
                $('#mod_first_name').css('border-color', 'red');
                $('#mod_first_name').focus();
                return false;
            } else {
                $('#mod_first_name').css('border-color', '');
            }
            if (!validateEmail($('#mod_email').val()) || $('#mod_email').val() == '') {
                $('#mod_email').css('border-color', 'red');
                $('#mod_email').focus();
                return false;
            } else {
                $('#mod_email').css('border-color', '');
            }
            if ($('#mod_phone').val() == '') {
                $('#mod_phone').css('border-color', 'red');
                $('#mod_phone').focus();
                return false;
            } else {
                $('#mod_phone').css('border-color', '');
            }

            var json = ajaxpost(form, "/to-do-list/addnewcontact");
            try {
                var json = jQuery.parseJSON(json);
                alertSimple(json.message);
                if (json.status == true) {
                    $('#addguestcontactpoup').modal('hide');
                }
            } catch (e) {
                alert(e);
            }
        }
        fn.assignedcontactSave = function () {
            var data = {_token: "{{ csrf_token() }}"};
            data['todoId'] = $('#opentodoId').val();
            data['assignedcontactSave'] = '1';
            var guestId = [];
            $('.addedlistguest').each(function () {
                guestId.push($(this).attr('userId'));
            });
            data['guestId'] = guestId;
            $.ajax({
                url: "{{ url('/to-do-list') }}",
                cache: false,
                data: data,
                async: true,
                type: 'POST',
                success: function (res) {
                    res = jQuery.parseJSON(res);
                    if (res.status == true) {
                        $('#exampleModalconactd').modal('hide');
                        $('.nav-to.active').click();
                    }
                }
            });
        }

        return fn;
    })();

    //ASSIGN.getGuestlistForToDo();

    $(document).ready(function () {

        $("#list_id").change(function () {
            var list_id = $('#list_id').val();
            var data = {_token: "{{ csrf_token() }}", list_id: list_id};
            $.ajax({
                url: "{{url('/to-do-list/getsubList')}}",
                cache: false,
                data: data,
                async: false,
                type: 'POST',
                success: function (res) {
                    res = jQuery.parseJSON(res);
                    var data = res.data;
                    var html = '<option value="0">--select--</option>';
                    for (var i = 0; i < data.length; i++) {
                        var d = data[i];
                        html = html + '<option value="' + d.id + '">' + d.sub_list_name + '</option>';
                    }
                    $('#sub_list').html(html);
                }
            });
        });
    });

</script>
<script>
    function validateForm() {



        var form = new FormData();

        if ($('#items_name').val() == '') {
            $('#items_name').focus();
            $('#items_name').css('border-color', 'red');
            return false;
        } else {
            $('#items_name').css('border-color', '');

        }
        if ($('#list_id').val() == '') {
            $('#list_id').focus();
            $('#list_id').css('border-color', 'red');
            return false;
        } else {
            $('#list_id').css('border-color', '');

        }
        if ($('#date').val() == '') {
            $('#date').focus();
            $('#date').css('border-color', 'red');
            return false;
        } else {
            $('#date').css('border-color', '');

        }

        var guestId = [];
        $('.addedlistguest').each(function () {
            guestId.push($(this).attr('userId'));
        });

        form.append('json[event_id]', $('#event_id').val());
        form.append('json[list_id]', $('#list_id').val());
        form.append('json[sub_list]', $('#sub_list').val());
        form.append('json[date]', $('#date').val());
        form.append('json[items_name]', $('#items_name').val());
        form.append('json[id]', $('#pid').val());


        form.append('json[assign_contact]', ($('#assigncontactBox').is(':checked') ? 1 : '0'));


        var fileInput = document.querySelector('#attachmentData');
        form.append('attachmentData', fileInput.files[0]);


        form.append('_token', '{{ csrf_token() }}');
        form.append('guestId', guestId);

      
        $('#addsubListBtn').hide();
        $('#plzwait').show();
        setTimeout(function () {
            var json = ajaxpost(form, "{{url('/to-do-list/store')}}");
            try {
                var json = jQuery.parseJSON(json);
                if (json.status == true) {
                    window.location = json.url;
                }
            } catch (e) {
                alert(e);
            }
        }, 500);
    }
</script>
@endsection
