{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')
<?php
if (isset($_POST['addListBtn'])) {
    $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
    $json['created_by'] = Auth::user()->id;
    $json['updated_at'] = date('Y-m-d H:i:s');
    $json['created_at'] = $json['updated_at'];
    \App\Database::insert('todo_filter_list', $json);
}
?>
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="{{ url('/to-do-list') }}" class="text-muted">To Do List</a>
</li>


@endsection
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->

       	<div class="__toListWrp wd100">
            <input id="filter_eventId" value="<?= !empty($_REQUEST['eventId']) ? $_REQUEST['eventId'] : '' ?>" type="hidden">
            <input id="filter_tabId" value="<?= !empty($_REQUEST['tabId']) ? $_REQUEST['tabId'] : '' ?>" type="hidden">
            <input id="filter_fromdate" value="<?= !empty($_REQUEST['fromdate']) ? $_REQUEST['fromdate'] : '' ?>" type="hidden">
            <?php
            $roleId = Auth::user()->roles()->first()->id;
            $hideFor = ['5', '6'];

            $vendor = \App\Helpers\LibHelper::GetvendordetailsBy(Auth::user()->id);
            $checkbox = "";
            $occasions = \App\Helpers\LibHelper::Getoccasions();
            if (!in_array($roleId, $hideFor)) {
                for ($i = 0; $i < count($occasions); $i++) {
                    $d = $occasions[$i];
                    $checkbox = $checkbox . ' <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
                                                <input cond="VE.occasion_id=' . "'$d->id'" . '" onclick="autoLoad();" class="occasions" type="checkbox" name="Checkboxes16"/>
                                                <span></span>
                                                ' . $d->name . '
                                            </label>';
                }
            }
            ?>

            <div class="wd100 __tpExpBtnWrp" style="margin-top: 0;">
                <form method="post" onsubmit="return false;">
                    @csrf

                    <div class="d-flex __tpExto_list">
                        <div class="__tpExtoFld d-flex align-items-center">
                            <input name="search" id="search" type="text" class="form-control font-weight-bold pl-2" required placeholder="Search...">
                            <button onclick="autoLoad();" name="searcthis" class="btn btn-primary __nowrap" type="button" >
                                Serach
                            </button>
                        </div>

                        <!--<div class="dropdown __topSwitchBtn __dvflez">-->

                        <!--</div>-->

                        <div class="dropdown __topSwitchBtn __flez">
                            <a href="javascript:void(0)" onclick="$('#staticBackdrop').modal('show');"><button class="btn btn-secondary __nowrap" type="button" >Add Sub List </button></a>
                            <a href="<?= url('to-do-list/create') ?>"><button class="btn btn-secondary __nowrap" type="button" >Add Item </button></a>
                            <div class="btn btn-primary __todoLifTogle" onclick="filterclic();"><i class="fas fa-filter"></i> <span>Filter</span></div>

                        </div>

                    </div>


                </form>
            </div>


            <?php
            $Sql = "SELECT * FROM `todo_filter_list` WHERE  status=0 AND created_by='" . Auth::user()->id . "'";
            $dAry = \App\Database::select($Sql);
            for ($i = 0; $i < count($dAry); $i++) {
                $d = $dAry[$i];
                $checkbox = $checkbox . ' <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
                                                <input cond="TFL.id=' . "'$d->id'" . '" onclick="autoLoad();" type="checkbox" class="filter_list" name="Checkboxes16"/>
                                                <span></span>
                                                ' . $d->list_name . '
                                            </label>';
            }
            if (!in_array($roleId, $hideFor)) {
                $dAry = App\Helpers\LibHelper::GetvendorCategory();
                for ($i = 0; $i < count($dAry); $i++) {
                    $d = $dAry[$i];
                    $checkbox = $checkbox . ' <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
                                                <input   cond="' . "'$d->id'" . '" onclick="autoLoad();" class="vendorCategory" type="checkbox" name="Checkboxes16"/>
                                                <span></span>
                                                ' . $d->name . '
                                            </label>';
                }
            }
            ?>






            <div class="_todolist-1" style="margin-top: 50px;">



                <div class="__todolist_TabWrp wd100">

                    <div class="row">
                        <ul class="nav nav-tabs nav nav-pills nav-fill alltab">
                            <li><a  class="nav-to active" onclick="autoLoad();" tabId='all' data-toggle="tab" id="li-all" href="#home">All - <span class="font-weight-bolder mr-2 __open_sans" tabId="all" id="all-activities">0</span></a></li>
                            <li><a class="nav-to" onclick="autoLoad();" tabId='delayed' id="li-delayed" data-toggle="tab" href="#menu1">Delayed - <span class="font-weight-bolder mr-2 __open_sans" tabId='delayed' id="delayed">0</span></a></li>
                            <li><a class="nav-to" onclick="autoLoad();" tabId='critical' id="li-critical" data-toggle="tab" href="#menu4">Critical - <span class="font-weight-bolder mr-2 __open_sans" tabId='critical' id="critical">0</span></a></li>
                            <li><a class="nav-to" onclick="autoLoad();" tabId='upcoming' id="li-upcoming" data-toggle="tab" href="#menu2">Upcoming - <span class=" font-weight-bolder mr-2 __open_sans" tabId='upcoming' id="upcoming">0</span></a></li>
                            <li><a class="nav-to" onclick="autoLoad();" tabId='coming' id="li-coming" data-toggle="tab" href="#menu3">Coming - <span class=" font-weight-bolder mr-2 __open_sans" tabId='coming' id="coming">0</span></a></li>
                        </ul>

                    </div>

                    <div class="__todoleftFiltrNewWrp wd100" style="display: none;">
                        <div class="__todoleftFiltr" >
                            <div class="__ftCose" onclick="filterclic();"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
                            <h5 class="font-weight-bolder">Filters</h5>
                            <div class="wd100 col-form-label mt-0 pt-0">
                                <div class="checkbox-list">
                                    <?php
                                    echo $checkbox;
                                    ?>
                                </div>
                            </div>
                            <div class="__clear_all" onclick="clearall();"><i class="fas fa-times"></i> Clear all</div>
                        </div>
                    </div>

                    <div class="tab-content wd100">
                        <div id="home" class="tab-pane fade in active show">
                            <div class="__srvBzWrp __todSubRwp">


                                <div class="__toderptrep">
                                    <div class="__toListTavw __scopeWorkTbvw  wd100">
                                        <?php
                                        $cond = '';
                                        ddlist($type = 'all');
                                        ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="menu4" class="tab-pane fade">
                            <div class="  __srvBzWrp __todSubRwp">

                                <div class="__toderptrep">
                                    <div class="__toListTavw __scopeWorkTbvw  wd100">
                                        <?php
                                        ddlist($type = 'critical');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <div class="  __srvBzWrp __todSubRwp">

                                <div class="__toderptrep">
                                    <div class="__toListTavw __scopeWorkTbvw  wd100">
                                        <?php
                                        ddlist($type = 'delayed');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                            <div class="  __srvBzWrp __todSubRwp">


                                <div class="__toderptrep">
                                    <div class="__toListTavw __scopeWorkTbvw  wd100">
                                        <?php
                                        ddlist($type = 'upcoming');
                                        ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="menu3" class="tab-pane fade">
                            <div class="__srvBzWrp __todSubRwp">

                                <div class="__toderptrep">
                                    <div class="__toListTavw __scopeWorkTbvw  wd100">
                                        <?php
                                        ddlist($type = 'coming');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>










        </div>



        <!--end::Container-->
    </div>
</div>

<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Add New List</h5>
            </div>
            <form method="post" action="">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Name</label>
                            <input name="json[list_name]" class="form-control form-control-lg" type="text" autocomplete="off" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="addListBtn" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--begin::Entry-->
<?php

function ddlist($type) {
    ?>
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col" class="text-center __brdsLt">S.N</th>
                    <th scope="col" class="text-left">Items Name</th>
                    <th  scope="col" class="text-left">Contact</th>
                    <th scope="col" class="text-left">List Name</th>
                    <th scope="col" class="text-center">Date</th>


                                                                                                    <!--<th scope="col" class="text-center">Duration</th>-->


                    <th scope="col" class="text-center" width="20%">Status</th>
                    <th scope="col" class="text-center __brdsRt">Action</th>
                </tr>
            </thead>
            <tbody id="tbody-<?= $type ?>">


            </tbody>
        </table>
    </div>
    <?php
}
?>
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModalconactd">
    <div class="modal-dialog" role="document" style="max-width: 75%;">
        <input type="hidden" id="opentodoId">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Assign Contacts</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row" >
                    <div class="col-sm-12" id="assigncontst">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" onclick="ASSIGN.assignedcontactSave();" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')











<script>
    var ASSIGN = (function () {
        var fn = {};
        fn.assigncontactBoxAction = function () {
            if ($('#assigncontactBox').is(':checked')) {
                $('#allreadcontectlist').show();
            } else {
                $('#allreadcontectlist').hide();
                $('#addedlist').html('');
            }
        }
        fn.getGuestlistForToDo = function () {
            $('#assingLoader').show();
            $('#assignList').hide();
            setTimeout(function () {
                var data = {_token: "{{ csrf_token() }}"};
                data['search'] = $('#mod_search').val();

                data['categoryId'] = $('#categoryId').val();
                $.ajax({
                    url: "{{url('/to-do-list/getGuestlistForToDo')}}",
                    cache: false,
                    data: data,
                    async: true,
                    type: 'POST',
                    success: function (res) {
                        res = jQuery.parseJSON(res);
                        if (res.status == true) {
                            $('#assingLoader').hide();
                            $('#assignList').show();
                            $('#assignList').html(res.HTML);
                        }
                    }
                });
            });
        }
        fn.setGuestlistForToDo = function (e) {
            var json = $(e).attr('json');
            json = jQuery.parseJSON(atob(json));
            var html = '';

            html = html + '                  <div class="col-lg-3 col-md-6 col-sm-6 addedlistguest" userId="' + json.id + '" id="guestId-' + json.id + '">';
            html = html + '                        <div class="__grcadbz wd100">';
            html = html + '                            <div class="__selcBntWrp">';
            html = html + '                                <div class="__selcBn">';
            html = html + '                                    <i class="fas fa-check-circle"></i>';
            html = html + '                                </div>';
            html = html + '                                <div class="__CloseBn" userId="' + json.id + '" onclick="ASSIGN.removeGuestlistForToDo(this);">';
            html = html + '                                    <i class="far fa-times-circle"></i>';
            html = html + '                                </div> ';
            html = html + '                            </div>';
            html = html + '                            <div class="__grcadbzImg">';
            html = html + '                                <img class="img-fluid" src="' + json.logo + '">';
            html = html + '                            </div>';
            html = html + '                            <div class="wd100">';
            html = html + '                                <h4 class="mt-2"><a href="javascript:void(0)">' + json.first_name + '</a></h4>';
            html = html + '                                <h5 class="__todLilcotin mb-2"><a href="javascript:void(0)">' + json.address + '</a> </h5>';
            html = html + '                                <div class="wd100 __todoCall __open_sans ">';
            html = html + '                                    <a href="javascript:void(0)">' + json.phone + '</a>';
            html = html + '                                </div>';
            html = html + '                                <div class="wd100 __todoEmail __open_sans">';
            html = html + '                                    <a href="javascript:void(0)">' + json.email + '</a>';
            html = html + '                                </div>';
            html = html + '                            </div>';
            html = html + '                        </div>';
            html = html + '                    </div>';
            $('#guestId-' + json.id).remove();
            $('#addedlist').append(html);
            $('html, body').animate({
                scrollTop: (parseInt($("#addedlist").offset().top) - 200)
            }, 0);
        }
        fn.removeGuestlistForToDo = function (e) {
            var userId = $(e).attr('userId');
            $('#guestId-' + userId).remove();
        }
        fn.openModalforAssignContact = function (e) {
            var data = {_token: "{{ csrf_token() }}"};
            data['todoId'] = $(e).attr('todoid');
            data['modalforAssignContact'] = '1';
            $.ajax({
                url: "{{ url('/to-do-list') }}",
                cache: false,
                data: data,
                async: true,
                type: 'POST',
                success: function (res) {
                    res = jQuery.parseJSON(res);
                    if (res.status == true) {
                        $('#assigncontst').html(res.HTML);
                        $('#opentodoId').val($(e).attr('todoid'));
                        $('#exampleModalconactd').modal('show');
                    }
                }
            });
        }
        fn.openModaladdContact = function () {
            //   $('#exampleModalconactd').modal('hide');
            setTimeout(function () {
                $('#addguestcontactpoup').modal('show');
            }, 100);
        }
        fn.addnewcontact = function () {
            var form = new FormData();
            form.append('_token', '{{ csrf_token() }}');
            var phoneWithcode = $('#mod_phone_code').val() + $("#mod_phone").val();

            form.append('json[first_name]', $('#mod_first_name').val());
            form.append('json[last_name]', $('#mod_last_name').val());
            form.append('json[email]', $('#mod_email').val());
            form.append('json[phone]', phoneWithcode);




            if ($('#mod_first_name').val() == '') {
                $('#mod_first_name').css('border-color', 'red');
                $('#mod_first_name').focus();
                return false;
            } else {
                $('#mod_first_name').css('border-color', '');
            }
            if (!validateEmail($('#mod_email').val()) || $('#mod_email').val() == '') {
                $('#mod_email').css('border-color', 'red');
                $('#mod_email').focus();
                return false;
            } else {
                $('#mod_email').css('border-color', '');
            }
            if ($('#mod_phone').val() == '') {
                $('#mod_phone').css('border-color', 'red');
                $('#mod_phone').focus();
                return false;
            } else {
                $('#mod_phone').css('border-color', '');
            }

            var json = ajaxpost(form, "/to-do-list/addnewcontact");
            try {
                var json = jQuery.parseJSON(json);
                alertSimple(json.message);
                if (json.status == true) {
                    $('#addguestcontactpoup').modal('hide');
                }
            } catch (e) {
                alert(e);
            }
        }
        fn.assignedcontactSave = function () {
            var data = {_token: "{{ csrf_token() }}"};
            data['todoId'] = $('#opentodoId').val();
            data['assignedcontactSave'] = '1';
            var guestId = [];
            $('.addedlistguest').each(function () {
                guestId.push($(this).attr('userId'));
            });
            data['guestId'] = guestId;
            $.ajax({
                url: "{{ url('/to-do-list') }}",
                cache: false,
                data: data,
                async: true,
                type: 'POST',
                success: function (res) {
                    res = jQuery.parseJSON(res);
                    if (res.status == true) {
                        $('#exampleModalconactd').modal('hide');
                        $('.nav-to.active').click();
                    }
                }
            });
        }

        return fn;
    })();
    function updateMyToDoASComplite(e) {
        var iscompleted = $(e).is(':checked') ? 1 : 0;
        var mystatus = $(e).attr('mystatus');
        var compliteHTML = '<span class="label label-info label-inline mr-2 completed">Completed</span>';
        var data = {_token: "{{ csrf_token() }}"};
        data['toDoASComplite'] = '1';
        data['iscompleted'] = iscompleted;
        data['type'] = $(e).attr('atype');
        data['primaryid'] = $(e).attr('primaryid');





        Swal.fire({
            title: 'Comment',
            input: 'textarea'
        }).then(function (result) {
            if (result.value) {
                data['comment'] = result.value;
                ad();
            } else {
                Swal.fire('Comment is required');
                $(e).prop('checked', !iscompleted);
                return false;
            }
        });

        function ad() {
            $.ajax({
                url: "{{ url('/to-do-list') }}",
                cache: false,
                data: data,
                async: false,
                type: 'POST',
                success: function (res) {
                    if (iscompleted == '1') {
                        $('statusId' + data['primaryid']).html(compliteHTML);
                    } else {
                        $('statusId' + data['primaryid']).html(mystatus);
                    }
                    console.log(res);
                }
            });
        }
    }
    function autoLoad() {
        setTimeout(function () {
            var tabId = $('.alltab .active').attr('tabid');
            // if(!tabId)
            // {
            //     tabId = 'all';
            // }
            var loading = '<tr><td colspan="8">Wait data is loading...</td></tr>';
            $('#tbody-' + tabId).html(loading);

            b();
        }, 250);

        function b() {
            var tabId = $('.alltab .active').attr('tabid');
            var data = {_token: "{{ csrf_token() }}", autoload: true, tabId: tabId};
            data['eventId'] = $('#filter_eventId').val();
            data['fromDays'] = $('#filter_fromdate').val();

            var occasions = [];
            $('.occasions:checked').each(function () {
                occasions.push($(this).attr('cond'));
            });
            var filter_list = [];
            $('.filter_list:checked').each(function () {
                filter_list.push($(this).attr('cond'));
            });
            var vendorCategory = [];
            $('.vendorCategory:checked').each(function () {
                vendorCategory.push($(this).attr('cond'));
            });
            data['occasions'] = occasions;
            data['filter_list'] = filter_list;
            data['vendorCategory'] = vendorCategory;
            data['search'] = $('#search').val();
            $.ajax({
                url: "{{ url('/to-do-list') }}",
                cache: false,
                data: data,
                async: false,
                type: 'POST',
                success: function (res) {
                    res = jQuery.parseJSON(res);
                    $('#tbody-' + tabId).html(res.HTML);
                    $('[data-toggle="tooltip"]').tooltip();
                }
            });
        }
    }
    autoLoad();
    function clearall() {
        $('.vendorCategory,.filter_list,.occasions').prop('checked', false);
        autoLoad();
    }
    function deleteTodo(e) {
        Swal.fire({
            title: "Are you sure?",
            text: "You wont be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                var data = {_token: "{{ csrf_token() }}", todo_id: $(e).attr('todoId')};
                data['type'] = $(e).attr('atype');
                $.ajax({
                    url: "{{url('/to-do-list/delete-todo')}}",
                    cache: false,
                    data: data,
                    async: false,
                    type: 'POST',
                    success: function (res) {
                        res = jQuery.parseJSON(res);
                        if (res.status == true) {
                            location.reload();
                        }
                    }
                });
            } else if (result.dismiss === "cancel") {
                Swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                        )
            }
        });
    }
    function filterclic() {
        var vis = $('.__todoleftFiltrNewWrp').is(":hidden");
        if (vis) {
            $('.__todoleftFiltrNewWrp').show(500);
        } else {
            $('.__todoleftFiltrNewWrp').hide(500);
        }
    }



    $(document).ready(function () {
        var filter_tabId = $('#filter_tabId').val();
        if (filter_tabId != '') {
            $('#li-' + filter_tabId).click();
            autoLoad();
        }




        setTimeout(function () {
            activitiesData('all-activities');
            activitiesData('critical');
            activitiesData('delayed');
            activitiesData('upcoming');
            activitiesData('coming');
        }, 500);


    });
    function activitiesData(counterId) {
        var fromDays = '';
        var tabId = $('#' + counterId).attr('tabId');
        var data = {
            _token: "{{ csrf_token() }}",
            autoload: true,
            eventId: '',
            tabId: tabId,
            fromDays: fromDays
        };
        data['eventId'] = $('#filter_eventId').val();
        $.ajax({
            url: "{{ url('/to-do-list') }}",
            cache: false,
            data: data,
            async: true,
            type: 'POST',
            success: function (res) {
                res = jQuery.parseJSON(res);
                if (res.status == true) {
                    $('#' + counterId).html(res.dataCount);
                }
            }
        });

    }
</script>
@endsection
