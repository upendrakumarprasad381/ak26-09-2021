{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="{{url(''.$url.'/personal_information')}}" class="text-muted">Account Settings</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="" class="text-muted">{{$page_title}}</a>
</li>
@endsection
{{-- Content --}}
@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">

    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Profile Personal Information-->
            <div class="d-flex flex-row">

                <!--begin::Aside-->
                <div class="flex-row-auto offcanvas-mobile w-250px w-xxl-350px" id="kt_profile_aside">
                    <!--begin::Profile Card-->
                    <div class="card card-custom card-stretch">
                        <!--begin::Body-->
                        <div class="card-body pt-4">
                            <div class="d-flex align-items-center __piLefCard">
                                <div
                                    class="symbol symbol-60 symbol-xxl-100 mr-5 align-self-start align-self-xxl-center">
                                    <div class="symbol-label"
                                        style="background-image:url('{{ url('/storage') }}/{{ $user->profile_picture }}')">
                                    </div>
                                    @if($user->status == 0)
                                    <i class="symbol-badge bg-success"></i>
                                    @else
                                    <i class="symbol-badge bg-danger"></i>
                                    @endif
                                </div>
                                <div>
                                    <a href="#"
                                        class="font-weight-bolder font-size-h5 text-dark-75 text-hover-primary">{{
                                        auth()->user()->first_name }}
                                        {{ auth()->user()->last_name }}</a>
                                    <div class="text-muted">{{ auth()->user()->roles[0]['display_name'] }}</div>
                                    @if($user->status == 0)
                                    <span class="badge badge-success">Active</span>
                                    @else
                                    <span class="badge badge-danger">Inactive</span>
                                    @endif


                                </div>
                            </div>
                            <!--end::User-->
                            @if($page_title != 'Team Members')
                            <div class="progress __askAcctProgs">
                                <div class="progress-bar" role="progressbar" style="width: {{$percentage}}%;" aria-valuenow="{{$percentage}}" aria-valuemin="0" aria-valuemax="100">{{$percentage}}% Completed.</div>
                            </div>
                            @endif
                            <!--begin::Contact-->
                            <div class="py-9 __askAcctinfoemcl">
                                <div class="d-flex align-items-center justify-content-between mb-2">
                                    <span class="font-weight-bold mr-2">Email:</span>
                                    <a href="#" class="text-muted text-hover-primary">{{ auth()->user()->email }}</a>
                                </div>
                                <div class="d-flex align-items-center justify-content-between mb-2">
                                    <span class="font-weight-bold mr-2">Phone:</span>
                                    <span class="text-muted">{{ auth()->user()->phone }}</span>
                                </div>
                                <div class="d-flex align-items-center justify-content-between">
                                    <span class="font-weight-bold mr-2">Office Number:</span>
                                    <span class="text-muted">{{ auth()->user()->phone_office }}</span>
                                </div>
                            </div>
                            <!--end::Contact-->
                            <!--begin::Nav-->

                            <div class="__askAccttbaNv navi navi-bold navi-hover navi-active navi-link-rounded">
                                <div class="navi-item mb-2">
                                    <a href="{{ url('/personal_information') }}" class="navi-link py-4 @if($page_title == 'Personal Informations') active @endif">
                                        <span class="navi-icon mr-2">
                                            <i class="fas fa-user"></i>
                                        </span>
                                        <span class="navi-text font-size-lg">Personal Information</span>
                                    </a>
                                </div>
                                @if($user->parent_id == null)
                                <div class="navi-item mb-2">
                                    <a href="{{ url('/business_information') }}" class="navi-link py-4 @if($page_title == 'Business Informations') active @endif">
                                        <span class="navi-icon mr-2">
                                            <i class="fas fa-briefcase"></i>
                                        </span>
                                        <span class="navi-text font-size-lg">Business Information</span>
                                    </a>
                                </div>
                                <div class="navi-item mb-2">
                                    <a href="{{ url('/team-members') }}" class="navi-link py-4 @if($page_title == 'Team Members') active @endif">
                                        <span class="navi-icon mr-2">
                                            <i class="fas fa-users"></i>
                                        </span>
                                        <span class="navi-text font-size-lg">Team Members</span>
                                    </a>
                                </div>
                                @endif
                                <div class="navi-item mb-2">
                                    <a href="{{ url('/change_password') }}" class="navi-link py-4 @if($page_title == 'Change Password') active @endif">
                                        <span class="navi-icon mr-2">
                                            <i class="fas fa-key"></i>
                                        </span>
                                        <span class="navi-text font-size-lg">Change Password</span>
                                    </a>
                                </div>
                            </div>
                            <!--end::Nav-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Profile Card-->
                </div>
                <!--end::Aside-->
                @yield('account_settings_content')
            </div>
            <!--end::Profile Personal Information-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

