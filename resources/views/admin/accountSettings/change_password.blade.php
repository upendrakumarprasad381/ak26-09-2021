@extends('admin.accountSettings.account_settings_layout')
@section('account_settings_content')
    <!--begin::Content-->
    <div class="flex-row-fluid ml-lg-8">
        <!--begin::Card-->
        <div class="card card-custom">
            <!--begin::Header-->
            <div class="card-header py-3">
                <div class="card-title align-items-start flex-column">
                    <h3 class="card-label font-weight-bolder text-dark">Change Password</h3>
                    <span class="text-muted font-weight-bold font-size-sm mt-1">Change your account password</span>
                </div>
                <div class="card-toolbar">
                    <button type="reset" id="btnSubmit" class="btn btn-success mr-2">Request</button>
                    <a href="{{ url('/personal_information') }}"><button type="reset"
                            class="btn btn-secondary">Cancel</button></a>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Form-->
            <form class="form" id="my-form">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="form-group validated col-md-12">
                            <label class="form-control-label">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Email" value="{{$user->email}}" readonly/>
                        </div>
                        {{-- <div class="form-group validated col-md-6">
                            <label class="form-control-label">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="New password" />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Verify Password</label>
                            <input type="password" class="form-control" name="password_confirmation"
                                placeholder="Verify password" />
                        </div> --}}
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
    <!--end::Content-->
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#btnSubmit").click(function(event) {
                event.preventDefault();
                var form = $('#my-form')[0];
                var data = new FormData(form);
                $("#btnSubmit").prop("disabled", true);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: "password/email",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(data) {
                        $("#btnSubmit").prop("disabled", false);
                        Swal.fire({
                            text: 'A password reset link was sent. Click the link in the email to create a new password.',
                            icon: "success",
                        });
                        // setTimeout(function() {
                        //     location.reload();
                        // }, 1000);
                    },
                    error: function(e) {
                        $("#btnSubmit").prop("disabled", false);
                    }
                });
            });
        });
    </script>
@endsection
