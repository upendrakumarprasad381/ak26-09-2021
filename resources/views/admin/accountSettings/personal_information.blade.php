@extends('admin.accountSettings.account_settings_layout')
@section('account_settings_content')
    <!--begin::Content-->
    <div class="flex-row-fluid ml-lg-8">
        <!--begin::Card-->
        <div class="card card-custom card-stretch">
            <!--begin::Header-->
            <div class="card-header py-3">
                <div class="card-title align-items-start flex-column">
                    <h3 class="card-label font-weight-bolder text-dark">Personal Information</h3>

                    {{-- <span class="text-muted font-weight-bold font-size-sm mt-1">Update your personal informaiton</span> --}}
                </div>
                <div class="card-toolbar">
                    <button type="submit" id="btnSubmit" class="btn btn-success mr-2">Save Changes</button>
                    <a href="{{ url('/dashboard') }}"><button type="reset" class="btn btn-secondary">Cancel</button></a>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Form-->
            <form class="form" id="form">
                @csrf
                <!--begin::Body-->
                <div class="card-body">
                    <div class="row">
                        <div class="form-group validated col-md-12">
                            <label class="form-control-label">Profile Picture</label>
                            <ul id="media-list" class="clearfix">
                                <li class="myupload" @if ($user->profile_picture)
                                    style="display:none;" @endif>
                                    <span><i class="fa fa-plus" aria-hidden="true"></i>
                                        <input type="file" click-type="single" id="picupload" name="profile_picture"
                                            value="{{ url('/storage') }}/{{ $user->profile_picture }}"
                                            class="picupload">
                                    </span>
                                </li>
                                @if ($user->profile_picture)
                                    <li>
                                        <img src="/storage/{{ $user->profile_picture }}" title="" />
                                        <div class='post-thumb'>
                                            <div class='inner-post-thumb'>
                                                <a href='javascript:void(0);' data-id='' class='remove-pic'>
                                                    <i class='fa fa-times' aria-hidden='true'>
                                                    </i>
                                                </a>
                                            </div>
                                        </div>
                                        <input type="text" name="old_profile_picture" value="{{ $user->profile_picture }}"
                                            style="display: none;">
                                    </li>
                                @endif
                            </ul>
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">First Name</label>
                            <input type="text" class="form-control" name="first_name"
                                value="{{ auth()->user()->first_name }}" />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Last Name</label>
                            <input type="text" class="form-control" name="last_name"
                                value="{{ auth()->user()->last_name }}" />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Phone</label>
                            <input type="number" class="form-control" name="phone" value="{{ auth()->user()->phone }}" />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Office Number</label>
                            <input type="number" class="form-control" name="phone_office"
                                value="{{ auth()->user()->phone_office }}" />
                        </div>
                        <div class="form-group validated col-md-12">
                            <label class="form-control-label">Email</label>
                            <input type="email" class="form-control" name="email" value="{{ auth()->user()->email }}" />
                        </div>
                    </div>
                </div>
                <!--end::Body-->
            </form>
            <!--end::Form-->
        </div>
    </div>
@endsection
{{-- Scripts Section --}}
@section('scripts')
    <script type="text/javascript">
        $("#btnSubmit").click(function(event) {
            event.preventDefault();
            var form = $('#form')[0];
            var data = new FormData(form);
            $("#btnSubmit").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/update_user_information",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    $("#btnSubmit").prop("disabled", false);
                    setTimeout(function() {
                        location.reload();
                    }, 1000);

                },
                error: function(e) {
                    $("#btnSubmit").prop("disabled", false);
                }
            });
        });
    </script>
@endsection
