@extends('admin.accountSettings.account_settings_layout')
@section('account_settings_content')
    <!--begin::Content-->
    <div class="flex-row-fluid ml-lg-8">
        <!--begin::Card-->
        <div class="card card-custom card-stretch">
            <!--begin::Header-->
            <div class="card-header py-3">
                <div class="card-title align-items-start flex-column">
                    <h3 class="card-label font-weight-bolder text-dark">Create Team Member
                    </h3>
                </div>
                <div class="card-toolbar">
                    <button type="submit" id="btnSubmit" class="btn btn-success mr-2">Save</button>
                    {{-- <a href="{{ url('/team-members') }}"><button type="reset" class="btn btn-secondary">Cancel</button></a> --}}
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Form-->
            @php
                $url = '/team-members';
                if (isset($editable)) {
                    $url = '/team-members/' . $editable->id;
                }
            @endphp
            <form class="form" id="form">
                @csrf
                @if (isset($editable))
                    @method('PATCH')
                @endif
                <!--begin::Body-->
                <div class="card-body">
                    <div class="row">
                        <div class="form-group validated col-md-12">
                            <label class="form-control-label">Profile Picture</label>
                            @if (isset($editable))
                                <ul id="media-list" class="clearfix">
                                    <li class="myupload" @if ($editable->profile_picture) style="display:none;" @endif>
                                        <span><i class="fa fa-plus" aria-hidden="true"></i>
                                            <input type="file" click-type="single" id="picupload" name="profile_picture"
                                                value="{{ url('/storage') }}/{{ $editable->profile_picture }}"
                                                class="picupload">
                                        </span>
                                    </li>
                                    @if ($editable->profile_picture)
                                        <li>
                                            <img src="/storage/{{ $editable->profile_picture }}" title="" />
                                            <div class='post-thumb'>
                                                <div class='inner-post-thumb'>
                                                    <a href='javascript:void(0);' data-id='' class='remove-pic'>
                                                        <i class='fa fa-times' aria-hidden='true'>
                                                        </i>
                                                    </a>
                                                </div>
                                            </div>
                                            <input type="text" name="old_profile_picture"
                                                value="{{ $editable->profile_picture }}" style="display: none;">
                                        </li>
                                    @endif
                                </ul>
                            @else
                                <ul id="media-list" class="clearfix">
                                    <li class="myupload">
                                        <span><i class="fa fa-plus" aria-hidden="true"></i>
                                            <input type="file" click-type="single" id="picupload" name="profile_picture"
                                                class="picupload">
                                        </span>
                                    </li>
                                </ul>
                            @endif
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">First Name</label>
                            <input type="text" class="form-control" id="first_name" name="first_name" @isset($editable)
                                value="{{ $editable->first_name }}" @endisset />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Last Name</label>
                            <input type="text" class="form-control" id="last_name" name="last_name" @isset($editable)
                                value="{{ $editable->last_name }}" @endisset />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Email</label>
                            <input type="email" class="form-control" id="email" name="email" @isset($editable)
                                value="{{ $editable->email }}" @endisset />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Phone</label>
                            <input type="text" class="form-control" id="phone" name="phone" @isset($editable)
                                value="{{ $editable->phone }}" @endisset />
                        </div>
                        <div class="form-group validated col-md-12">
                            <label class="form-control-label">Designation</label>
                            {{-- <input type="text" class="form-control" id="designation" name="designation" @isset($editable)
                                value="{{ $editable->designation }}" @endisset /> --}}
                            <select class="form-control select2" id="designation" name="designation">
                                <option value></option>
                                @foreach ($designations as $designation)
                                    <option value="{{ $designation['id'] }}" @isset($editable)@if ($editable->designation == $designation->id) selected @endif
                                        @endisset>
                                        {{ $designation['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <h4>Module Permissions</h4>
                        <br>
                        <br>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @php $text = explode(' ',trim($permission->display_name)); @endphp
                                @if ($text[0] == 'View')
                                    <div class="col-lg-4 col-md-4 col-sm-12 form-group">
                                        <div class="checkbox-inline">
                                            <label class="checkbox checkbox-outline">
                                                <input type="checkbox" value="{{ $permission['id'] }}"
                                                    name="permissions[]" @isset($editable) @if ($editable->hasPermission($permission['name'])) checked @endif
                                                        @endisset @empty($editable) checked @endempty>
                                                    <span></span>
                                                    {{ $permission['display_name'] }}
                                                </label>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <h4>Edit Permissions</h4>
                            <br>
                            <br>
                            <div class="row">
                                @foreach ($permissions as $permission)
                                    @php $text = explode(' ',trim($permission->display_name)); @endphp
                                    @if ($text[0] == 'Edit')
                                        <div class="col-lg-4 col-md-4 col-sm-12 form-group">
                                            <div class="checkbox-inline">
                                                <label class="checkbox checkbox-outline">
                                                    <input type="checkbox" value="{{ $permission['id'] }}"
                                                        name="permissions[]" @isset($editable) @if ($editable->hasPermission($permission['name'])) checked @endif
                                                            @endisset @empty($editable)  @endempty>
                                                        <span></span>
                                                        {{ $permission['display_name'] }}
                                                    </label>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    </form>
                    <!--end::Form-->
                </div>
            </div>
            <!--end::Content-->
        @endsection

        {{-- Scripts Section --}}
        @section('scripts')
            <script type="text/javascript">
                var url = "{{ $url }}";
                $("#btnSubmit").click(function(event) {
                    event.preventDefault();
                    var form = $('#form')[0];
                    var data = new FormData(form);
                    $("#btnSubmit").prop("disabled", true);
                    $.ajax({
                        type: "POST",
                        enctype: 'multipart/form-data',
                        url: url,
                        data: data,
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function(data) {
                            $("#btnSubmit").prop("disabled", false);
                            setTimeout(function() {
                                location.replace('/team-members');
                            }, 1000);

                        },
                        error: function(e) {
                            $("#btnSubmit").prop("disabled", false);
                        }
                    });
                });
                $('#designation').select2({
                    placeholder: "Select Designation",
                });
            </script>
        @endsection
