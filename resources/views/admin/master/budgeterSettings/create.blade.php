{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="" class="text-muted">{{$page_title}}</a>
</li>
@endsection
{{-- Content --}}
@section('content')
    <!--begin::Container-->
    <div class="container">
        <!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="flaticon2-supermarket text-primary"></i>
                    </span>
                    <h3 class="card-label">{{ $type_of_event->name }} Budgets</b></h3>
                </div>

            </div>
            <!--begin::Form-->
            <form class="form" id="form">
                @csrf
                <!--begin::Body-->
                <div class="card-body">
                    <div class="row">
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Category</label>
                        </div>
                        <div class="form-group validated col-md-3">
                            <label class="form-control-label">Budget</label>
                        </div>
                        <div class="form-group validated col-md-3">
                            <label class="form-control-label">Percentage</label>
                        </div>
                    </div>
                    @php $zero = 0 @endphp
                    @foreach ($rows as $row)
                        <div class="row">
                            <div class="form-group validated col-md-6">
                                <input type="text" class="form-control" name="name" value="@if ($row->occasion_id){{ $row->category['name'] }}@else{{ $row['name'] }}@endif"
                                    readonly />
                                <input type="text" class="form-control" name="category[]" value="@if ($row->occasion_id){{ $row->category['id'] }}@else{{ $row['id'] }}@endif"
                                    hidden />
                            </div>
                            <div class="form-group validated col-md-3">
                                <input type="text" class="form-control numeric budget" name="budget[]"
                                    value="@if ($row->occasion_id){{ $row['budget'] }}@else{{ $zero }}@endif" />
                            </div>
                            <div class="form-group validated col-md-3">
                                <input type="text" class="form-control percentage" name="percentage[]"
                                    value="@if ($row->occasion_id){{ $row['budget_percentage'] }}@else{{$zero}}@endif"/>
                            </div>
                        </div>
                    @endforeach
                    @php
                     $other_budget = $row->occasion_id?$type_of_event->total_budget - $rows->sum('budget'):0;
                     $other_percentage = ($other_budget/$type_of_event->total_budget) * 100;
                    @endphp
                    <div class="row">
                        <div class="form-group validated col-md-6">
                            <input type="text" class="form-control" value="Others" readonly />
                        </div>
                        <div class="form-group validated col-md-3">
                            <input type="text" class="form-control numeric budget" id="other_budget" value="@if($other_percentage > 0){{$other_budget}}@else{{$zero}}@endif"/>
                        </div>
                        <div class="form-group validated col-md-3">
                            <input type="text" class="form-control numeric" id="other_percentage" value="@if($other_percentage > 0){{$other_percentage}}@else{{$zero}}@endif" readonly />
                        </div>
                    </div>
                </div>
                <input type="text" name="occasion_id" value="{{ $type_of_event->id }}" hidden />
                <!--end::Body-->
                <div class="card-footer">
                    <button type="submit" id="btnSubmit" class="btn btn-primary mr-2">Save</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
    <!--end::Card-->
    </div>
    <!--end::Container-->
    </div>
    <!--end::Entry-->
    </div>
    <!--end::Content-->
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        $("#btnSubmit").click(function(event) {
            event.preventDefault();
            var form = $('#form')[0];
            var data = new FormData(form);
            $("#btnSubmit").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/admin/master/budget-settings",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    $("#btnSubmit").prop("disabled", false);
                    setTimeout(function() {
                        location.replace('/admin/master/budget-settings');
                    }, 1000);
                },
                error: function(e) {
                    $("#btnSubmit").prop("disabled", false);
                }
            });
        });
        $('.budget').on('input', function() {
            var budget_total = 0.00;
            $("input[name='budget[]']")
                .map(function() {
                    if ($(this).val() != ' ')
                        budget_total += parseFloat($(this).val());
                }).get();
            var total_budget = "{{ $type_of_event->total_budget }}";
            var budget = $(this).val();
            var other_budget = total_budget - budget_total;
            if(other_budget >= 0)
            {
                $('#other_budget').val(other_budget);
                $('#other_percentage').val(((other_budget/total_budget) * 100));
            }
            else
            {
                $('#other_budget').val(0);
                $('#other_percentage').val(0);
            }
            if (budget_total > total_budget) {
                $(this).addClass('is-invalid');
                toastr.error('Budget OverDue', 'Warning');
            } else {
                $('.budget').removeClass('is-invalid');
            }
            var percentage = budget / total_budget * 100;
            $(this).parent().parent().find('.percentage').val(parseInt(percentage));
        });

        $('.percentage').on('input', function() {
            var budget_total = 0.00;
            $("input[name='percentage[]']")
                .map(function() {
                    if ($(this).val() != ' ')
                        budget_total += parseFloat($(this).val());
                }).get();
            var total_budget = "{{ $type_of_event->total_budget }}";
            var budget = $(this).val();
            var other_budget = 100 - budget_total;
            if(other_budget >= 0)
            {
                $('#other_budget').val((total_budget/100) * other_budget);
                $('#other_percentage').val(other_budget);
            }
            else
            {
                $('#other_budget').val(0);
                $('#other_percentage').val(0);
            }
            if (budget_total > 100) {
                $(this).addClass('is-invalid');
                toastr.error('Budget OverDue', 'Warning');
            } else {
                $('.budget').removeClass('is-invalid');
            }
            var percentage = total_budget / 100  * budget;
            $(this).parent().parent().find('.budget').val(percentage.toFixed(2));
        });
    </script>
@endsection
