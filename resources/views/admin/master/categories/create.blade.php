{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
    <li class="breadcrumb-item text-muted">
        <a href="" class="text-muted">{{ $page_title }}</a>
    </li>
@endsection
{{-- Content --}}
@section('content')
    <!--begin::Container-->
    <div class="container">
        <!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="flaticon2-supermarket text-primary"></i>
                    </span>
                    <h3 class="card-label">Add</h3>
                </div>

            </div>
            <!--begin::Form-->
            <form class="form" id="form">
                @csrf
                <!--begin::Body-->
                <div class="card-body">
                    <div class="row">
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="" />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Slug</label>
                            <input type="text" class="form-control" id="slug" name="slug" value="" />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Icon</label>
                            {{-- <input type="file" class="form-control" name="icon" value="" /> --}}
                            <ul id="media-list" class="clearfix">
                                <li class="myupload">
                                    <span><i class="fa fa-plus" aria-hidden="true"></i>
                                        <input type="file" click-type="single" id="picupload" name="icon"
                                            class="picupload">
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Banner Image</label>
                            {{-- <input type="file" class="form-control" name="banner_image" value="" /> --}}
                            <ul id="media-list" class="clearfix">
                                <li class="myupload">
                                    <span><i class="fa fa-plus" aria-hidden="true"></i>
                                        <input type="file" click-type="single" id="picupload" name="banner_image"
                                            class="picupload">
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Parent</label>
                            <select class="form-control" id="parent_category" name="parent">
                                <option value>Select Parent Category</option>
                                @foreach ($parent_categories as $category)
                                    <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group validated col-md-4">
                            <label class="form-control-label">Qty Label</label>
                            <input type="text" class="form-control" id="qty_label" name="qty_label" value="" />
                        </div>
                        <div class="form-group col-md-2">
                            <label class="form-control-label">Active</label>
                            <span class="switch switch-icon">
                                <label>
                                    <input type="hidden" checked="checked" value="0" name="status" />
                                    <input type="checkbox" checked="checked" value="1" name="status" />
                                    <span></span>
                                </label>
                            </span>
                        </div>
                        <input type="hidden" name="type" value="{{ $type }}" />
                    </div>
                    <h4>Filters</h4>
                    <hr>
                    <div class="row">
                        @foreach ($filters as $filter)
                            <div class="col-lg-3 col-md-3 col-sm-12 form-group">
                                <div class="checkbox-inline">
                                    <label class="checkbox checkbox-outline">
                                        <input type="checkbox" value="{{ $filter->id }}" name="filters[]">
                                        <span></span>
                                        {{ $filter->name }}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!--end::Body-->
                <div class="card-footer">
                    <button type="submit" id="btnSubmit" class="btn btn-primary mr-2">Save</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
    <!--end::Card-->
    </div>
    <!--end::Container-->
    </div>
    <!--end::Entry-->
    </div>
    <!--end::Content-->
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        $("#btnSubmit").click(function(event) {
            event.preventDefault();
            var form = $('#form')[0];
            var data = new FormData(form);
            $("#btnSubmit").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/admin/master/{{ $slug }}",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    $("#btnSubmit").prop("disabled", false);
                    setTimeout(function() {
                        location.replace('/admin/master/{{ $slug }}');
                    }, 1000);
                },
                error: function(e) {
                    $("#btnSubmit").prop("disabled", false);
                }
            });
        });
        $('#parent_category').select2({
            placeholder: "Select Category"
        });

        function convertToSlug(Text) {
            return Text
                .toLowerCase()
                .replace(/[^\w ]+/g, '')
                .replace(/ +/g, '-');
        }
        $('#name').on('input', function() {
            var slug = convertToSlug($(this).val());
            $('#slug').val(slug);
        });
    </script>
@endsection
