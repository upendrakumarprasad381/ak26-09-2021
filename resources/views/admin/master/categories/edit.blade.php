{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
    <li class="breadcrumb-item text-muted">
        <a href="" class="text-muted">{{ $page_title }}</a>
    </li>
@endsection
{{-- Content --}}
@section('content')
    <!--begin::Container-->
    <div class="container">
        <!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="flaticon2-supermarket text-primary"></i>
                    </span>

                    <h3 class="card-label">Edit</h3>
                </div>

            </div>
            <!--begin::Form-->
            <form class="form" id="form">
                @csrf
                @method('PATCH')
                <!--begin::Body-->
                <div class="card-body">
                    <div class="row">
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Name</label>
                            <input type="text" class="form-control" name="name" id="name"
                                value="{{ $category->name }}" />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Slug</label>
                            <input type="text" class="form-control" name="slug" id="slug"
                                value="{{ $category->slug }}" />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Icon</label>
                            <ul id="media-list" class="clearfix">
                                <li class="myupload" @if ($category->icon) style="display:none;" @endif>
                                    <span><i class="fa fa-plus" aria-hidden="true"></i>
                                        <input type="file" click-type="single" id="picupload" name="icon"
                                            value="{{ url('/storage') }}/{{ $category->icon }}" class="picupload">
                                    </span>
                                </li>
                                @if ($category->icon)
                                    <li>
                                        <img src="/storage/{{ $category->icon }}" title="" />
                                        <div class='post-thumb'>
                                            <div class='inner-post-thumb'>
                                                <a href='javascript:void(0);' data-id='' class='remove-pic'>
                                                    <i class='fa fa-times' aria-hidden='true'>
                                                    </i>
                                                </a>
                                            </div>
                                        </div>
                                        <input type="text" name="old_icon" value="{{ $category->icon }}"
                                            style="display: none;">
                                    </li>
                                @endif
                            </ul>
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Banner Image</label>
                            <ul id="media-list" class="clearfix">
                                <li class="myupload" @if ($category->banner_image) style="display:none;" @endif>
                                    <span><i class="fa fa-plus" aria-hidden="true"></i>
                                        <input type="file" click-type="single" id="picupload" name="banner_image"
                                            value="{{ url('/storage') }}/{{ $category->banner_image }}"
                                            class="picupload">
                                    </span>
                                </li>
                                @if ($category->banner_image)
                                    <li>
                                        <img src="/storage/{{ $category->banner_image }}" title="" />
                                        <div class='post-thumb'>
                                            <div class='inner-post-thumb'>
                                                <a href='javascript:void(0);' data-id='' class='remove-pic'>
                                                    <i class='fa fa-times' aria-hidden='true'>
                                                    </i>
                                                </a>
                                            </div>
                                        </div>
                                        <input type="text" name="old_banner_image" value="{{ $category->banner_image }}"
                                            style="display: none;">
                                    </li>
                                @endif
                            </ul>
                        </div>

                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Parent</label>
                            <select class="form-control" id="parent_category" name="parent">
                                <option value>Select Parent Category</option>
                                @foreach ($parent_categories as $row)
                                    <option value="{{ $row['id'] }}" @if ($category->parent_id == $row['id']) selected @endif>{{ $row['name'] }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group validated col-md-4">
                            <label class="form-control-label">Qty Label</label>
                            <input type="text" class="form-control" id="qty_label" name="qty_label"
                                value="{{ $category->qty_label }}" />
                        </div>
                        <div class="form-group col-md-2">
                            <label class="form-control-label">Active</label>
                            <span class="switch switch-icon">
                                <label>
                                    <input type="hidden" @if ($category->status == 0) checked="checked" @endif value="0" name="status" />
                                    <input type="checkbox" @if ($category->status == 1) checked="checked" @endif value="1" name="status" />
                                    <span></span>
                                </label>
                            </span>
                        </div>
                        <input type="hidden" name="type" value="{{ $category->type }}" />
                        <input type="hidden" id="category_id" name="category_id" value="{{ $category->id }}" />
                    </div>
                    <h4>Filters</h4>
                    <hr>
                    <div class="row">
                        @foreach ($filters as $filter)
                            <div class="col-lg-3 col-md-3 col-sm-12 form-group">
                                <div class="checkbox-inline">
                                    <label class="checkbox checkbox-outline">
                                        <input type="checkbox" value="{{$filter->id}}" name="filters[]"
                                        @if($category->filters) @if(in_array($filter->id,$category->filters->pluck('filter_id')->toArray())) checked @endif @endif >
                                        <span></span>
                                        {{$filter->name}}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!--end::Body-->
                <div class="card-footer">
                    <button type="submit" id="btnSubmit" class="btn btn-primary mr-2">Save</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
    <!--end::Card-->
    </div>
    <!--end::Container-->
    </div>
    <!--end::Entry-->
    </div>
    <!--end::Content-->
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        $('#parent_category').select2({
            placeholder: "Select Category"
        });
        $("#btnSubmit").click(function(event) {
            event.preventDefault();
            var category_id = $('#category_id').val();
            var form = $('#form')[0];
            var data = new FormData(form);
            $("#btnSubmit").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/admin/master/{{ $slug }}/" + category_id,
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    $("#btnSubmit").prop("disabled", false);
                    setTimeout(function() {
                        location.replace('/admin/master/{{ $slug }}');
                    }, 1000);

                },
                error: function(e) {
                    $("#btnSubmit").prop("disabled", false);
                }
            });
        });

        function convertToSlug(Text) {
            return Text
                .toLowerCase()
                .replace(/[^\w ]+/g, '')
                .replace(/ +/g, '-');
        }
        $('#name').on('input', function() {
            var slug = convertToSlug($(this).val());
            $('#slug').val(slug);
        });
    </script>
@endsection
