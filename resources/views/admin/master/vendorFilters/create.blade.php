{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
    <li class="breadcrumb-item text-muted">
        <a href="" class="text-muted">{{ $page_title }}</a>
    </li>
@endsection
{{-- Content --}}
@section('content')
    <!--begin::Container-->
    <div class="container">
        <!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="flaticon2-supermarket text-primary"></i>
                    </span>
                    <h3 class="card-label">Add</h3>
                </div>

            </div>
            <!--begin::Form-->
            <form class="form" id="form">
                @csrf
                <!--begin::Body-->
                <div class="card-body">
                    <div class="row">
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Label Text</label>
                            <input type="text" class="form-control" id="name" name="name" />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Type</label>
                            <select class="form-control" id="type" name="type">
                                <option value="Text Box">Text Box</option>
                                <option value="Drop Down">Drop Down</option>
                                <option value="Check Box">Check Box</option>
                                <option value="Radio Button">Radio</option>
                            </select>
                        </div>
                        <div class="value-container col-md-12" hidden>
                            <div class="row">
                                <div class="form-group validated col-md-4">
                                    <label class="form-control-label">Value</label>
                                    <input type="text" class="form-control" id="value" name="value[]" />
                                </div>
                                <div class="form-group col-md-3">
                                    <button class="btn btn-primary addNewBtn mt-7">Add New</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-control-label">Active</label>
                            <span class="switch switch-icon">
                                <label>
                                    <input type="hidden" checked="checked" value="0" name="status" />
                                    <input type="checkbox" checked="checked" value="1" name="status" />
                                    <span></span>
                                </label>
                            </span>
                        </div>
                    </div>
                </div>
                <!--end::Body-->
                <div class="card-footer">
                    <button type="submit" id="btnSubmit" class="btn btn-primary mr-2">Save</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
    <!--end::Card-->
    </div>
    <!--end::Container-->
    </div>
    <!--end::Entry-->
    </div>
    <!--end::Content-->
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        $("#type").on("change", function() {
            var value = $('#type :selected').val();
            if (value != 'Text Box') {
                $('.value-container').attr('hidden', false);
            } else {
                $('.value-container').attr('hidden', true);
            }
        });
        $('.addNewBtn').on('click', function(e) {
            e.preventDefault();
            var row = `<div class="row">
                                <div class="form-group validated col-md-4">
                                    <label class="form-control-label">Value</label>
                                    <input type="text" class="form-control" name="value[]" />
                                </div>
                                <div class="form-group col-md-3">
                                    <button class="btn btn-secondary removeBtn mt-7">Remove</button>
                                </div>
                            </div>`;
            $('.value-container').append(row);
            $('.removeBtn').on('click', function(e) {
                e.preventDefault();
                $(this).parent().parent().empty();
            });
        });
        $('.removeBtn').on('click', function(e) {
            e.preventDefault();
            $(this).parent().parent().empty();
        });
        $("#btnSubmit").click(function(event) {
            event.preventDefault();
            var form = $('#form')[0];
            var data = new FormData(form);
            $("#btnSubmit").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/admin/master/vendor-filters",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    $("#btnSubmit").prop("disabled", false);
                    setTimeout(function() {
                        location.replace('/admin/master/vendor-filters');
                    }, 1000);
                },
                error: function(e) {
                    $("#btnSubmit").prop("disabled", false);
                }
            });
        });
    </script>
@endsection
