{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
    <li class="breadcrumb-item text-muted">
        <a href="" class="text-muted">{{ $page_title }}</a>
    </li>
@endsection
{{-- Content --}}
@section('content')
    <!--begin::Container-->
    <div class="container">
        <!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="flaticon2-supermarket text-primary"></i>
                    </span>
                    <h3 class="card-label">Edit</h3>
                </div>

            </div>
            <!--begin::Form-->
            <form class="form" id="form">
                @csrf
                @method('PATCH')
                <!--begin::Body-->
                <div class="card-body">
                    <div class="row">
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Name</label>
                            <input type="text" class="form-control" id="name" name="name"
                                value="{{ $editable->name }}" />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Type</label>
                            <select class="form-control" id="type" name="type">
                                <option value="Text Box" @if ($editable->type == 'Text Box') selected @endif>Text Box</option>
                                <option value="Drop Down" @if ($editable->type == 'Drop Down') selected @endif>Drop Down</option>
                                <option value="Check Box" @if ($editable->type == 'Check Box') selected @endif>Check Box</option>
                                <option value="Radio Button" @if ($editable->type == 'Radio Button') selected @endif>Radio</option>
                            </select>
                        </div>
                        <div class="value-container col-md-12" @if ($editable->type == 'Text Box') hidden @endif>
                            @if(count($editable->items) > 0)
                            @foreach ($editable->items as $item)
                                <div class="row">
                                    <div class="form-group validated col-md-4">
                                        <label class="form-control-label">Value</label>
                                        <input type="text" class="form-control" id="value" name="value[]" value="{{$item->text}}"/>
                                        <input type="hidden" class="form-control" name="value_id[]" value="{{$item->value}}"/>
                                    </div>
                                    <div class="form-group col-md-3">
                                        @if ($loop->iteration == 1)
                                            <button class="btn btn-primary addNewBtn mt-7">Add New</button>
                                        @else
                                            <button class="btn btn-secondary removeBtn mt-7">Remove</button>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                            @else
                            <div class="row">
                                <div class="form-group validated col-md-4">
                                    <label class="form-control-label">Value</label>
                                    <input type="text" class="form-control" id="value" name="value[]"/>
                                    <input type="hidden" class="form-control" name="value_id[]"/>
                                </div>
                                <div class="form-group col-md-3">
                                        <button class="btn btn-primary addNewBtn mt-7">Add New</button>
                                </div>
                            </div>
                            @endif
                        </div>
                        {{-- <div class="form-group validated col-md-6 value-container" @if ($editable->type == 'Text Box') hidden @endif>
                            <label class="form-control-label">Values (If morethan 1 seperate with comma(,))</label>
                            <input type="text" class="form-control" id="value" name="value" value="@foreach ($editable->items as $item)@if ($loop->iteration != 1),@endif{{$item->text}}@endforeach"/>
                        </div> --}}
                        <div class="form-group col-md-6">
                            <label class="form-control-label">Active</label>
                            <span class="switch switch-icon">
                                <label>
                                    <input type="hidden" @if ($editable->status == 0) checked="checked" @endif value="0" name="status" />
                                    <input type="checkbox" @if ($editable->status == 1) checked="checked" @endif value="1" name="status" />
                                    <span></span>
                                </label>
                            </span>
                        </div>
                        <input type="hidden" id="filter_id" name="filter_id" value="{{ $editable->id }}" />
                    </div>
                </div>
                <!--end::Body-->
                <div class="card-footer">
                    <button type="submit" id="btnSubmit" class="btn btn-primary mr-2">Save</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
    <!--end::Card-->
    </div>
    <!--end::Container-->
    </div>
    <!--end::Entry-->
    </div>
    <!--end::Content-->
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        $("#type").on("change", function() {
            var value = $('#type :selected').val();
            if (value != 'Text Box') {
                $('.value-container').attr('hidden', false);
            } else {
                $('.value-container').attr('hidden', true);
            }
        });
        $('.addNewBtn').on('click', function(e) {
            e.preventDefault();
            var row = `<div class="row">
                                <div class="form-group validated col-md-4">
                                    <label class="form-control-label">Value</label>
                                    <input type="text" class="form-control" name="value[]" />
                                </div>
                                <div class="form-group col-md-3">
                                    <button class="btn btn-secondary removeBtn mt-7">Remove</button>
                                </div>
                            </div>`;
            $('.value-container').append(row);
            $('.removeBtn').on('click', function(e) {
                e.preventDefault();
                $(this).parent().parent().empty();
            });
        });
        $('.removeBtn').on('click', function(e) {
            e.preventDefault();
            $(this).parent().parent().empty();
        });
        $("#btnSubmit").click(function(event) {
            event.preventDefault();
            var id = $('#filter_id').val();
            var form = $('#form')[0];
            var data = new FormData(form);
            $("#btnSubmit").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/admin/master/vendor-filters/" + id,
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    $("#btnSubmit").prop("disabled", false);
                    setTimeout(function() {
                        location.replace('/admin/master/vendor-filters');
                    }, 1000);

                },
                error: function(e) {
                    $("#btnSubmit").prop("disabled", false);
                }
            });
        });
    </script>
@endsection
