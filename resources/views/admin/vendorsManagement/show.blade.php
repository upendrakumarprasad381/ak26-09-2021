{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="" class="text-muted">{{$page_title}}</a>
</li>
@endsection
{{-- Content --}}
@section('content')
<!--begin::Container-->
<div class="container">
<!--begin::Card-->
<div class="card card-custom">
    <div class="card-header">
        <div class="card-title">
            <span class="card-icon">
                <i class="flaticon2-user text-primary"></i>
            </span>

            <h3 class="card-label">{{$item->first_name}} {{$item->last_name}} - {{$item->roles[0]['display_name']}}</h3>
        </div>

    </div>
<!--begin::Form-->
<form class="form" id="form">
    @csrf
    <!--begin::Body-->
    <div class="card-body">
        <div class="row">
        <div class="form-group validated col-md-4">
            <label class="form-control-label">First Name</label>
            <input type="text" class="form-control" value="{{$item->first_name}}" readonly/>
        </div>
        <div class="form-group validated col-md-4">
            <label class="form-control-label">Last Name</label>
            <input type="text" class="form-control" value="{{$item->last_name}}" readonly/>
        </div>
        <div class="form-group validated col-md-4">
            <label class="form-control-label">Phone</label>
            <input type="text" class="form-control" value="{{$item->phone}}" readonly/>
        </div>
        <div class="form-group validated col-md-4">
            <label class="form-control-label">Office Phone</label>
            <input type="text" class="form-control" value="{{$item->phone_office}}" readonly/>
        </div>
        <div class="form-group validated col-md-4">
            <label class="form-control-label">Email</label>
            <input type="text" class="form-control" value="{{$item->email}}" readonly/>
        </div>
        <div class="form-group validated col-md-4">
            <label class="form-control-label">Location</label>
            <input type="text" class="form-control" value="@isset($item->bussinessDetails){{$item->bussinessDetails['location']}} @endisset" readonly/>
        </div>
        <div class="form-group validated col-md-4">
            <label class="form-control-label">Website</label>
            <input type="text" class="form-control" value="@isset($item->bussinessDetails){{$item->bussinessDetails['business_website']}} @endisset" readonly/>
        </div>
        <div class="form-group validated col-md-4">
            <label class="form-control-label">Social Media Link</label>
            <input type="text" class="form-control" value="@isset($item->bussinessDetails){{$item->bussinessDetails['social_media_link']}} @endisset" readonly/>
        </div>
        <div class="form-group validated col-md-4">
            <label class="form-control-label">Address</label>
            <input type="text" class="form-control" value="@isset($item->bussinessDetails){{$item->bussinessDetails['address']}} @endisset" readonly/>
        </div>
        <div class="form-group validated col-md-6">
            <label class="form-control-label">Categories</label>
            <select class="form-control select2" multiple="multiple" id="categories_select" name="categories[]">
                <option value></option>
                @foreach ($categories as $category)
                    <option value="{{$category['id']}}" @if(in_array($category['id'],$item->categories->pluck('id')->toArray())) selected @endif>{{$category['name']}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group validated col-md-6">
            <label class="form-control-label">Occasions</label>
            <select class="form-control select2" multiple="multiple" id="occasion_select" name="occasions[]">
                <option value>Select Occasions</option>
                @foreach ($occasions as $occasion)
                    <option value="{{$occasion['id']}}" @if(in_array($occasion['id'],$item->occasions->pluck('id')->toArray())) selected @endif>{{$occasion['name']}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group col-md-4">
            <label class="form-control-label">Approve</label>
            <span class="switch switch-icon">
                <label>
                    <input type="hidden" @if($item->status == 1) checked="checked" @endif value="1" name="status"/>
                    <input type="checkbox" @if($item->status == 0) checked="checked" @endif value="0" name="status"/>
                    <span></span>
                </label>
            </span>
        </div>
        <input type="hidden" id="vendor_id" name="vendor_id" value="{{$item->id}}"/>
        </div>
        @if($item->bussinessDetails)
        <h4>Documents</h4>
        <div class="row">
            <div class="form-group validated col-md-4">
                <label class="form-control-label">Trade License</label>
                <input type="file" class="form-control" value="" name="trade_license"/>
                @if($item->bussinessDetails['trade_license'])
                <a href="/storage/{{$item->bussinessDetails['trade_license']}}" download="{{$item->first_name}}{{$item->last_name}}-Trade License">Download-Trade License</a>
                @endif
            </div>
            <div class="form-group validated col-md-4">
                <label class="form-control-label">Identity Proof</label>
                <input type="file" class="form-control" value="" name="identity_proof"/>
                @if($item->bussinessDetails['identity_proof'])
                <a href="/storage/{{$item->bussinessDetails['identity_proof']}}" download="{{$item->first_name}}{{$item->last_name}}-Identity Proof">Download-Identity Proof</a>
                @endif
            </div>
            <div class="form-group validated col-md-4">
                <label class="form-control-label">Passport</label>
                <input type="file" class="form-control" value="" name="passport"/>
                @if($item->bussinessDetails['passport'])
                <a href="/storage/{{$item->bussinessDetails['passport']}}" download="{{$item->first_name}}{{$item->last_name}}-Passport">Download-Passport</a>
                @endif
            </div>
        </div>
        @endif
    </div>
    <!--end::Body-->
    <div class="card-footer">
        @if($item->status == 1)
        <button type="submit" id="btnSendApproval" class="btn btn-primary mr-2">Approve</button>
        @else
        {{-- <button type="submit" id="btnSave" class="btn btn-primary mr-2">Save</button> --}}
        @endif
        <button type="submit" id="btnSave" class="btn btn-primary mr-2">Save</button>
        <a href="/admin/{{$slug}}"><button type="button" class="btn btn-secondary">Back</button></a>
    </div>
</form>
<!--end::Form-->
    </div>
</div>
<!--end::Card-->
</div>
<!--end::Container-->
</div>
<!--end::Entry-->
</div>
<!--end::Content-->
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
        $('#categories_select').select2({
            placeholder: "Select Categories",
        });
        $('#occasion_select').select2({
            placeholder: "Select Occasions",
        });
    $("#btnSendApproval").click(function (event) {
            event.preventDefault();
            var vendor_id = $('#vendor_id').val();
            var form = $('#form')[0];
            var data = new FormData(form);
            $("#btnSendApproval").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/admin/vendor-approval/"+vendor_id,
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    $("#btnSendApproval").prop("disabled", false);
                    setTimeout(function () {
                        location.reload();
                    }, 1000);

                },
                error: function (e) {
                    $("#btnSendApproval").prop("disabled", false);
                }
            });
        });

        $("#btnSave").click(function (event) {
            event.preventDefault();
            var vendor_id = $('#vendor_id').val();
            var form = $('#form')[0];
            var data = new FormData(form);
            $("#btnSendApproval").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/admin/update-vendor/"+vendor_id,
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    $("#btnSendApproval").prop("disabled", false);
                    setTimeout(function () {
                        location.reload();
                    }, 1000);

                },
                error: function (e) {
                    $("#btnSendApproval").prop("disabled", false);
                }
            });
        });

</script>
@endsection
