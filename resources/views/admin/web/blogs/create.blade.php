{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="" class="text-muted">{{$page_title}}</a>
</li>
@endsection
{{-- Content --}}
@section('content')
<!--begin::Container-->
<div class="container">
<!--begin::Card-->
<div class="card card-custom">
    <div class="card-header">
        <div class="card-title">
            <span class="card-icon">
                <i class="flaticon2-supermarket text-primary"></i>
            </span>
            <h3 class="card-label">Add</h3>
        </div>

    </div>
<!--begin::Form-->
<form class="form" id="form">
    @csrf
    <!--begin::Body-->
    <div class="card-body">
        <div class="row">
        <div class="form-group validated col-md-12">
            <label class="form-control-label">Title</label>
            <input type="text" class="form-control" id="title" name="title" />
        </div>
        <div class="form-group validated col-md-12">
            <label class="form-control-label">Slug</label>
            <input type="text" class="form-control" id="slug" name="slug" />
        </div>
        <div class="form-group validated col-md-12">
            <label class="form-control-label">Descritpion</label>
            <div class="summernote_desc form-control"></div>
        </div>
        <div class="form-group validated col-md-12">
            <label class="form-control-label">Content</label>
            <div class="summernote_content form-control"></div>
        </div>
        <div class="form-group validated col-md-6">
            <label class="form-control-label">Category</label>
            <select class="form-control" id="category" name="category">
                <option value>Select Category</option>
                @foreach ($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group validated col-md-6">
            <label class="form-control-label">Tags</label>
            <select class="form-control" id="tags" name="tags[]" multiple="multiple">
                <option value>Select Tags</option>
                @foreach ($tags as $tag)
                    <option value="{{$tag->name}}">{{$tag->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group validated col-md-6">
            <label class="form-control-label">Image</label>
            {{-- <div class="dropzone dropzone-default dropzone-success" id="file_upload">
                <div class="dropzone-msg dz-message needsclick">
                    <h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>
                    <span class="dropzone-msg-desc">Only image, pdf and psd files are allowed for upload</span>
                </div>
            </div> --}}
            <!--user post text -wrap end-->

            <ul id="media-list" class="clearfix">
                <li class="myupload">
                    <span><i class="fa fa-plus" aria-hidden="true"></i>
                        <input type="file" click-type="single" id="picupload" name="image" class="picupload">
                    </span>
                </li>
            </ul>
            {{-- <input type="file" class="form-control" name="image"/> --}}
        </div>
        <div class="form-group col-md-6">
            <label class="form-control-label">Active</label>
            <span class="switch switch-icon">
                <label>
                    <input type="hidden" checked="checked" value="0" name="status"/>
                    <input type="checkbox" checked="checked" value="1" name="status"/>
                    <span></span>
                </label>
            </span>
        </div>
        <div class="form-group validated col-md-12">
            <label class="form-control-label">SEO - Keywords</label>
            <input type="text" class="form-control" id="seo_keywords" name="seo_keywords" />
        </div>
        <div class="form-group validated col-md-12">
            <label class="form-control-label">SEO - Description</label>
            <textarea class="form-control" id="seo_description" name="seo_description"></textarea>
        </div>
        </div>
    </div>
    <!--end::Body-->
    <div class="card-footer">
        <button type="submit" id="btnSubmit" class="btn btn-primary mr-2">Save</button>
        <button type="reset" class="btn btn-secondary">Cancel</button>
    </div>
</form>
<!--end::Form-->
    </div>
</div>
<!--end::Card-->
</div>
<!--end::Container-->
</div>
<!--end::Entry-->
</div>
<!--end::Content-->
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>

    $('#category').select2({
            placeholder: "Select Category"
    });
    $('#tags').select2({
         placeholder: "Add Tags",
         tags: true
    });
    $('.summernote_desc').summernote({
        height: 100,
    });

    $('.summernote_content').summernote({
        height: 300,
    });
    $("#btnSubmit").click(function (event) {
            event.preventDefault();
            var description = $('.summernote_desc').summernote('code');
            var content = $('.summernote_content').summernote('code');
            var form = $('#form')[0];
            var data = new FormData(form);
            data.append("description",description);
            data.append("content",content);
            $("#btnSubmit").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/admin/web/blogs",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    $("#btnSubmit").prop("disabled", false);
                    setTimeout(function () {
                        location.replace('/admin/web/blogs');
                    }, 1000);
                },
                error: function (e) {
                    $("#btnSubmit").prop("disabled", false);
                }
            });
        });
        function convertToSlug(Text)
        {
            return Text
                .toLowerCase()
                .replace(/[^\w ]+/g,'')
                .replace(/ +/g,'-')
                ;
        }
        $('#title').on('input',function()
        {
           var slug = convertToSlug($(this).val());
           $('#slug').val(slug);
        });
</script>
@endsection
