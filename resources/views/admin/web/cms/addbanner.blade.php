{{-- Extends layout --}}
@extends('layout.default')
{{-- Content --}}
@section('content')
<!--begin::Container-->
<?php
$banner=App\Helpers\LibHelper::GetbannermanagementBybannerId($bannerId);
?>
<div class="container">
    <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
                <span class="card-icon">
                    <i class="flaticon2-supermarket text-primary"></i>
                </span>
                <h3 class="card-label"><strong>Add New Banner</strong></h3>
            </div>

        </div>
        <!--begin::Form-->
        <form class="form" method="post" enctype="multipart/form-data">
            @csrf
            <!--begin::Body-->

            <div class="card-body">
                <div class="row">
                    <div class="form-group validated col-md-6">
                        <label class="form-control-label">Banner Text </label>
                        <input type="text" name="json[title]" class="form-control" value="<?= !empty($banner->title) ? $banner->title : '' ?>"  required/>
                    </div>
                    <div class="form-group validated col-md-6">
                        <label class="form-control-label">Banner Description </label>
                        <input type="text" name="json[description]" class="form-control" value="<?= !empty($banner->description) ? $banner->description : '' ?>"  required/>
                    </div>
                    <div class="form-group validated col-md-6">
                        <label  class="form-control-label">Banner Image <small style="color: red;">Width and height: 1920*700 px</small></label>
                        <input type="file" name="banner_file" class="form-control" value=""  />
                    </div>
                </div>


            </div>

            <!--end::Body-->
            <div class="card-footer">
                <button type="submit" name="submitBtn" class="btn btn-primary mr-2">Save</button>

            </div>
        </form>
        <!--end::Form-->
    </div>
</div>
<!--end::Card-->
</div>
<!--end::Container-->
</div>
<!--end::Entry-->
</div>
<!--end::Content-->
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
    $("#btnSubmit").click(function (event) {
        event.preventDefault();
        var form = $('#form')[0];
        var data = new FormData(form);
        $("#btnSubmit").prop("disabled", true);
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "/admin/master/budget-settings",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                $("#btnSubmit").prop("disabled", false);
                setTimeout(function () {
                    location.replace('/admin/master/budget-settings');
                }, 1000);
            },
            error: function (e) {
                $("#btnSubmit").prop("disabled", false);
            }
        });
    });
    $('.budget').on('input', function () {
        var budget_total = 0.00;
        $("input[name='budget[]']")
                .map(function () {
                    if ($(this).val() != ' ')
                        budget_total += parseFloat($(this).val());
                }).get();
        var total_budget = "";
        var budget = $(this).val();
        var other_budget = total_budget - budget_total;
        if (other_budget >= 0)
        {
            $('#other_budget').val(other_budget);
            $('#other_percentage').val(((other_budget / total_budget) * 100));
        }
        else
        {
            $('#other_budget').val(0);
            $('#other_percentage').val(0);
        }
        if (budget_total > total_budget) {
            $(this).addClass('is-invalid');
            toastr.error('Budget OverDue', 'Warning');
        } else {
            $('.budget').removeClass('is-invalid');
        }
        var percentage = budget / total_budget * 100;
        $(this).parent().parent().find('.percentage').val(parseInt(percentage));
    });

    $('.percentage').on('input', function () {
        var budget_total = 0.00;
        $("input[name='percentage[]']")
                .map(function () {
                    if ($(this).val() != ' ')
                        budget_total += parseFloat($(this).val());
                }).get();
        var total_budget = "";
        var budget = $(this).val();
        var other_budget = 100 - budget_total;
        if (other_budget >= 0)
        {
            $('#other_budget').val((total_budget / 100) * other_budget);
            $('#other_percentage').val(other_budget);
        }
        else
        {
            $('#other_budget').val(0);
            $('#other_percentage').val(0);
        }
        if (budget_total > 100) {
            $(this).addClass('is-invalid');
            toastr.error('Budget OverDue', 'Warning');
        } else {
            $('.budget').removeClass('is-invalid');
        }
        var percentage = total_budget / 100 * budget;
        $(this).parent().parent().find('.budget').val(percentage.toFixed(2));
    });
</script>
@endsection
