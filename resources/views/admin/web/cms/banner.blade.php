{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<!--begin::Container-->
<div class="container">
    <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
                <span class="card-icon">
                    <i class="flaticon2-supermarket text-primary"></i>
                </span>
                <h3 class="card-label">Home Page Banner</h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <a href="{{url('admin/cms/addbanner')}}" class="btn btn-primary font-weight-bolder">
                    <span class="svg-icon svg-icon-md"></span>Add New</a>
                <!--end::Button-->
            </div> 
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-bordered table-hover table-checkable" id="kt_datatable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $basePath = Config::get('constants.HOME_DIR') . "image-list/";
                    $urlPath = url("image-list/");
                    $Sql = "SELECT * FROM `banner_management`";
                    $list = App\Database::select($Sql);
                    for ($i = 0; $i < count($list); $i++) {
                        $d = $list[$i];
                        ?>
                        <tr>
                            <td><?= $i + 1; ?></td>
                            <td><?= $d->title ?></td>
                            <td><?= is_file($basePath . $d->image) ? '<img src="' . $urlPath . '/' . $d->image . '" height="50" width="50">' : '' ?></td>
                            <td nowrap="nowrap">
                                 <a href="<?= url("admin/cms/addbanner/$d->banner_id") ?>" class="btn btn-sm btn-clean btn-icon"  title="Edit"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                <a href="javascript:void(0)"  urlDel="<?= url("admin/cms/bannerRemove/$d->banner_id") ?>" class="btn btn-sm btn-clean btn-icon" onclick="delImg(this);" title="Remove"><i class="fa fa-window-close" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->
</div>
<!--end::Container-->
</div>
<!--end::Entry-->
</div>
<!--end::Content-->
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
    var table = $('#kt_datatable').DataTable();
   function delImg(event){
         var urlDel = $(event).attr('urlDel');
        Swal.fire({
            title: "Are you sure?",
            text: "You wont be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "DELETE",
                    enctype: 'multipart/form-data',
                    url: urlDel,
                    processData: false,
                    contentType: false,
                    cache: false,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function (data) {
                        Swal.fire(
                                "Deleted!",
                                "Your file has been deleted.",
                                "success"
                                )
                        setTimeout(function () {
                              location.reload();
                        }, 1000);
                    },
                    error: function (e) {
                    }
                });
            } else if (result.dismiss === "cancel") {
                Swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                        )
            }
        });
   }
</script>
@endsection
