{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="" class="text-muted">{{$page_title}}</a>
</li>
@endsection
{{-- Content --}}
@section('content')
    <!--begin::Container-->
    <div class="container">
        <!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="flaticon2-supermarket text-primary"></i>
                    </span>
                    <h3 class="card-label">Contact Us</h3>
                </div>

            </div>
            <!--begin::Form-->
            <form class="form" id="form">
                @csrf
                <!--begin::Body-->
                <div class="card-body">
                    <div class="row">
                        <div class="form-group validated col-md-12">
                            <label class="form-control-label">Address</label>
                            <textarea class="form-control" id="address" name="address">@isset($data){{ $data->address }}@endisset</textarea>
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="@isset($data){{ $data->email }}@endisset"/>
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Phone</label>
                            <input type="text" class="form-control numeric" id="phone" name="phone" value="@isset($data){{ $data->phone }}@endisset"/>
                        </div>
                    </div>
                </div>
                <!--end::Body-->
                <div class="card-footer">
                    <button type="submit" id="btnSubmit" class="btn btn-primary mr-2">Save</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
    <!--end::Card-->
    </div>
    <!--end::Container-->
    </div>
    <!--end::Entry-->
    </div>
    <!--end::Content-->
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        $("#btnSubmit").click(function(event) {
            event.preventDefault();
            var form = $('#form')[0];
            var data = new FormData(form);
            $("#btnSubmit").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/admin/web/web-settings",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    $("#btnSubmit").prop("disabled", false);
                    setTimeout(function() {
                        location.replace('/admin/web/web-settings');
                    }, 1000);

                },
                error: function(e) {
                    $("#btnSubmit").prop("disabled", false);
                }
            });
        });
    </script>
@endsection
