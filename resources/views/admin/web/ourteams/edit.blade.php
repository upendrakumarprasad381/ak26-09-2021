{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="" class="text-muted">{{$page_title}}</a>
</li>
@endsection
{{-- Content --}}
@section('content')
<!--begin::Container-->
<div class="container">
<!--begin::Card-->
<div class="card card-custom">
    <div class="card-header">
        <div class="card-title">
            <span class="card-icon">
                <i class="flaticon2-supermarket text-primary"></i>
            </span>
            <h3 class="card-label">Edit</h3>
        </div>

    </div>
<!--begin::Form-->
<form class="form" id="form">
    @csrf
    @method('PATCH')
    <!--begin::Body-->
    <div class="card-body">
        <div class="row">
        <div class="form-group validated col-md-6">
            <label class="form-control-label">First Name</label>
            <input type="text" class="form-control" id="first_name" name="first_name" value="{{$our_team->first_name}}" />
        </div>
        <div class="form-group validated col-md-6">
            <label class="form-control-label">Last Name</label>
            <input type="text" class="form-control" id="last_name" name="last_name" value="{{$our_team->last_name}}" />
        </div>
        <div class="form-group validated col-md-6">
            <label class="form-control-label">Image</label>
            <ul id="media-list" class="clearfix">
                <li class="myupload" @if($our_team->image) style="display:none;" @endif>
                    <span><i class="fa fa-plus" aria-hidden="true"></i>
                        <input type="file" click-type="single" id="picupload" name="image" value="{{url('/storage')}}/{{$our_team->image}}" class="picupload">
                    </span>
                </li>
                @if($our_team->image)
                <li>
                    <img src="/storage/{{$our_team->image}}" title=""/>
                    <div class='post-thumb'>
                        <div class='inner-post-thumb'>
                            <a href='javascript:void(0);' data-id='' class='remove-pic'>
                                <i class='fa fa-times' aria-hidden='true'>
                                </i>
                            </a>
                        </div>
                    </div>
                    <input type="text" name="old_image" value="{{$our_team->image}}" style="display: none;">
                </li>
                @endif
            </ul>
        </div>
        <div class="form-group validated col-md-6">
            <label class="form-control-label">Designation</label>
            <input type="text" class="form-control" name="designation" value="{{$our_team->designation}}" />
        </div>
        <div class="form-group col-md-6">
            <label class="form-control-label">Active</label>
            <span class="switch switch-icon">
                <label>
                    <input type="hidden" @if($our_team->status == 0) checked="checked" @endif value="0" name="status"/>
                    <input type="checkbox" @if($our_team->status == 1) checked="checked" @endif value="1" name="status"/>
                    <span></span>
                </label>
            </span>
        </div>
        <input type="hidden" id="team_id" name="team_id" value="{{$our_team->id}}"/>
        </div>
    </div>
    <!--end::Body-->
    <div class="card-footer">
        <button type="submit" id="btnSubmit" class="btn btn-primary mr-2">Save</button>
        <button type="reset" class="btn btn-secondary">Cancel</button>
    </div>
</form>
<!--end::Form-->
    </div>
</div>
<!--end::Card-->
</div>
<!--end::Container-->
</div>
<!--end::Entry-->
</div>
<!--end::Content-->
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
    $("#btnSubmit").click(function (event) {
            event.preventDefault();
            var id = $('#team_id').val();
            var form = $('#form')[0];
            var data = new FormData(form);
            $("#btnSubmit").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/admin/web/our-teams/"+id,
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    $("#btnSubmit").prop("disabled", false);
                    setTimeout(function () {
                        location.replace('/admin/web/our-teams');
                    }, 1000);

                },
                error: function (e) {
                    $("#btnSubmit").prop("disabled", false);
                }
            });
        });
</script>
@endsection

