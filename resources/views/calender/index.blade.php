{{-- Extends layout --}}
@extends('layout.default')

<?php
$vendor = \App\Helpers\LibHelper::GetvendordetailsBy(Auth::user()->id);
?>
{{-- Content --}}
@section('content')

@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="" class="text-muted">Calender</a>
</li>

@endsection
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->
        <!--<div id='loading'>loading...</div>-->

        <div id='calendar'></div>



        <!--end::Container-->
    </div>
</div>
<!--begin::Entry-->

<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered __calanderpopwrap" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bolder __open_sans" id="exampleModalLongTitle">Date - <eventDate></eventDate></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">X</button>
            </div>
            <div class="modal-body">
                <div class="__calander_btn">

                    <a href="javascript:void(0)" redUrl="<?= url('/event-planner/create-event?') ?>" onclick="gotThisUrl(this);" class="__crtBntPn">
                        <i class="flaticon-plus"></i><br/>
                        New Event
                    </a>

                    <a href="javascript:void(0)" redUrl="<?= url('/to-do-list/create?') ?>" onclick="gotThisUrl(this);" class="__crtBntPn">
                        <i class="flaticon-plus"></i><br/>
                        To Do List
                    </a>

                </div>
            </div>

        </div>
    </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="https://apis.google.com/js/api.js" ></script>
<script>
                        var calendarId = "<?= !empty($vendor->calendar_id) ? $vendor->calendar_id : '' ?>";
                        var calendarEmailId = "<?= !empty($vendor->calendar_email_id) ? $vendor->calendar_email_id : '' ?>";
                        var base_url = "{{url('/')}}";
</script>
<script>
    var pushGoogle;
    document.addEventListener('DOMContentLoaded', function () {
        var calendarEl = document.getElementById('calendar');
        let clickCnt = 0;
        var calendar = new FullCalendar.Calendar(calendarEl, {
            headerToolbar: {
                left: 'prev,next today calender_settings googleActive',
                center: 'title',
                right: 'dayGridMonth,listYear'
            },
            dayMaxEventRows: true, // for all non-TimeGrid views
            customButtons: {
                calender_settings: {
                    text: 'Google Settings',
                    click: function () {
                        window.location = '{{url("calender/settings")}}';
                    }
                },
                googleActive: {
                    text: 'Google Login',
                    click: function () {
                        Google.authenticate();

                    }
                }
            },
            displayEventTime: false, // don't show the time column in list view
            events: {
                url: "{{url('calender/eventList/'.$eventId)}}",
            },
            dateClick: function (info) {
                clickCnt++;
                if (clickCnt === 1) {
                    oneClickTimer = setTimeout(function () {
                        clickCnt = 0;
                    }, 400);
                } else if (clickCnt === 2) {
                    clearTimeout(oneClickTimer);
                    clickCnt = 0;
                    //window.location = "{{url('/')}}" + '/event-planner/create-event?event-date=' + info.dateStr;

                }
                $('eventDate').html(info.dateStr);
                $("#exampleModalCenter").modal("show");
            },
            eventClick: function (info) {
                info.jsEvent.preventDefault(); // don't let the browser navigate
                if (info.event.url) {
                    window.open(info.event.url, "_blank");
                }
            }

        });
        calendar.render();
    });

    function gotThisUrl(e) {
        var redUrl = $(e).attr('redUrl');
        var eventDate = $('eventDate').text();
        window.location = redUrl + 'date=' + eventDate;
    }


    if (calendarId != '') {

        function doSomthing() {

            var data = {};
            $.ajax({
                async: true,
                url: "{{url('calender/googleUpdatelist')}}",
                data: data,
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function (json) {
                    try {
                        json = jQuery.parseJSON(json);
                        if (json.status == true) {
                            if (json.data.googleEventId == '') {
                                Google.addEvent(json.Event, json.data);
                            } else {
                                Google.updateEvent(json.Event, json.data);
                            }
                        }
                    }
                    catch (err) {

                    }
                },
                error: function (e) {
                }
            });
        }
        setTimeout(function () {
            pushGoogle = false;
            Google.validateProfile();
            //doSomthing();
        }, 500);
    } else {
        setTimeout(function () {
            pushGoogle = false;
            $('.fc-googleActive-button').hide();
        }, 50);
    }

</script>
@endsection
