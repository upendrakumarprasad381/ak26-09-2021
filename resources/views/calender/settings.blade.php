{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')
<!--begin::Entry-->
<?php
$vendor=\App\Helpers\LibHelper::GetvendordetailsBy(Auth::user()->id);

?>
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="{{ url('/calender') }}" class="text-muted">Calender</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{ url('/calender/settings') }}" class="text-muted">Settings</a>
</li>
@endsection

<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->
        <div class="col-lg-10 col-md-12 col-sm-12">
            <div class="card card-custom __gutAdpg">
                <h3 class="text-dark-100 font-weight-bolder mb-5 ">Calender settings</h3> 
                <form method="post" action="">
                    @csrf
                 
                    <div class="__steouboz wd100">
                        <div class="row"> 
                            <div class="col-lg-12">
                                <h5>Step 1</h5>
                            </div>
                            
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                                <label style="width: 100%;" class="form-label">Email Id </label>
                                <input type="email" name="json[calendar_email_id]" value="<?= $vendor->calendar_email_id ?>" id="occasions" class="form-control form-control-lg" >
                            </div>
                            
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                                <label style="width: 100%;" class="form-label">Calendar Id
                                
                                <a class="__candbntCrt" target="_blank" href="https://calendar.google.com/calendar/u/0/r/settings/createcalendar?tab=mc">Create New</a></label>
                                
                                <input type="email" name="json[calendar_id]" value="<?= $vendor->calendar_id ?>" id="occasions" class="form-control form-control-lg" >
                            </div>
                            
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right mb-2">
                                <button type="submit" name="submitcalender-settings" class="btn btn-primary __guestLisBnt btn-lg">Update</button>
                            </div>
                        </div>
                    </div>
                        
                    <div class="__steouboz wd100">    
                        <div class="row">  
                        <div class="col-lg-12">
                            <h5>Step 2  - Make your Google Calendar public:</h5>
                        </div>
                        
                        <div class="col-lg-12">
                            <ol>
                                <li>In the Google Calendar interface, locate the “My calendars” area on the left.</li>
                                <li>Hover over the calendar you need and click the downward arrow.</li>
                                <li>A menu will appear. Click “Share this Calendar”.</li>
                                <li>Check “Make this calendar public”.</li>
                                <li>Make sure “see all event details” is <strong>checked</strong>.</li>
                                <li>Click “Save”.</li>
                            </ol>
                        </div>
                        <div class="col-lg-12">
                            
                            <div class="__stepImg">
                               <a href="<?= url('/img/cl-public-config.JPG') ?>" target="_blank"> <img class="img-fluid" src="<?= url('/img/cl-public-config.JPG') ?>"></a>
                            </div>
                            
                        </div>
                    </div>
                    </div>
                        
                    <div class="__steouboz wd100">
                        <div class="row">      
                            <div class="col-lg-12">
                                <h5>Step 3  - Obtain your Google Calendar’s ID:</h5>
                            </div>
                            
                            <div class="col-lg-12">
                                <ol>
                                    <li>In the Google Calendar interface, locate the “My calendars” area on the left.</li>
                                    <li>Hover over the calendar you need and click the downward arrow.</li>
                                    <li>A menu will appear. Click “Calendar settings”.</li>
                                    <li>In the “Calendar Address” section of the screen, you will see your Calendar ID. It will look something like “abcd1234@group.calendar.google.com”.</li>
                                </ol>
                            </div>
                            
                            <div class="col-lg-12">
                                 <div class="__stepImg">
                                <a href="<?= url('/img/cal-id.JPG') ?>" target="_blank"> <img class="img-fluid" src="<?= url('/img/cal-id.JPG') ?>"></a>
                                </div>
                            </div>
                        </div>    
                    </div>
                        
                     
                </form>
            </div>
        </div>
        <!--end::Container-->
    </div>
</div>
<!--begin::Entry-->


<?php
?>

@endsection

{{-- Scripts Section --}}
@section('scripts')

<script src="https://apis.google.com/js/api.js" ></script>
<script>


//Google.validateProfile();

</script>
@endsection
