{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')
    <!--begin::Entry-->

@section('sub-header')
    <?php
    $event = App\Helpers\LibHelper::GetvendoreventsById($eventId);
    ?>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/event/' . $eventId . '') }}" class="text-muted">{{ $event->event_name }}</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('checklist/' . $eventId) }}" class="text-muted">Checklist</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="" class="text-muted">Customized Checklist</a>
    </li>
@endsection
<?php
$Sql = "SELECT SUM(no_of_days) AS no_of_days FROM `original_checklist_master` OC WHERE OC.occasion_id='$event->occasion_id'";
$defaultTotalDays = App\Database::selectSingle($Sql);
$defaultTotalDays = !empty($defaultTotalDays->no_of_days) ? $defaultTotalDays->no_of_days : '0';
?>

<div class="d-flex flex-column-fluid row">
    <div class="d-flex col-md-12">
        <div class="dropdown __topSwitchBtn __addItemDrop __selcDrop">
            <select class="form-control" id="occasions" name="occasions">
                <option value>Select Occasion</option>
                @foreach ($occasions as $occasion)
                    <option value="{{ $occasion->id }}">{{ $occasion->name }}
                    </option>
                @endforeach
            </select>
        </div>
        {{-- <div class="__bud-btn">

            <button class="btn btn-primary" id="btnSubmit" type="button"> Proceed </button>
        </div> --}}
    </div>
    <div class="__budget wd100 mt-10 col-md-5">
        <div class="__budgeter wd100">
            <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Item Name</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody id="checklist_items_table_body">
                </tbody>
            </table>
        </div>
    </div>
    <div class="__budget wd100 mt-10 col-md-7">
        <div class="__budgeter wd100">
            <form method="post" onsubmit="return false;">
                @csrf
                <div class="table-responsive">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th width='10%' scope="col" class="text-center __brdsLt">S.N</th>
                                <th width='30%' scope="col">Item Name</th>
                                <th width='20%' scope="col" class="text-center">From Date</th>
                                <th width='20%' scope="col" class="text-center">To Date</th>
                                <th width='10%' scope="col" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody id="checklist_table_body">

                        </tbody>
                    </table>
                </div>
                <div class="wd100 text-right mt-5">
                    <button type="button" id="customized-checklist" onclick="validateForm();"
                        class="btn btn-primary __guestLisBnt">Save Checklist</button>
                    <button type="button" id="plzwait" style="display: none;" class="btn btn-primary">Please
                        wait..</button>
                    <!--         <button type="button" class="btn btn-primary __guestLisBnt">Share Link</button>-->
                </div>
            </form>
        </div>
    </div>
</div>
{{-- <div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->
        <div class="__guestListWrp wd100">
            <div class="wd100 text-right mt-5">
                <a href="javascript:void(0)" onclick="addRowchecklist();"><button type="button" class="btn btn-primary __guestLisBnt">Add New</button></a>
            </div>
            <form method="post" onsubmit="return false;">
                @csrf
                <div class="__guestListTavw __scopeWorkTbvw  wd100">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-dark">

                                <tr>
                                    <th width='10%' scope="col" class="text-center __brdsLt">S.N</th>
                                    <th width='30%' scope="col">Item Name</th>
                                    <th width='20%' scope="col" class="text-center" >From Date</th>
                                    <th width='20%' scope="col" class="text-center">To Date</th>
                                    <th width='10%' scope="col" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>



                            </tbody>
                        </table>
                    </div>

                </div>

                <div class="wd100 text-right mt-5">
                    <button  type="button" id="customized-checklist" onclick="validateForm();" class="btn btn-primary __guestLisBnt">Save Checklist</button>
                    <button type="button"  id="plzwait" style="display: none;" class="btn btn-primary">Please wait..</button>
                    <!--         <button type="button" class="btn btn-primary __guestLisBnt">Share Link</button>-->
                </div>
            </form>


        </div>



        <!--end::Container-->
    </div>
</div> --}}
<!--begin::Entry-->


<style>
    body {
        counter-reset: section;
    }

    emp h2::before {
        counter-increment: section;
        content: ""counter(section) "";
    }

</style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<!--<script src="https://apis.google.com/js/api.js"></script>-->
<script>
    localStorage.setItem("checklist_items", JSON.stringify([]));

    var customizedlist = $('.customizedlist').length;
    // if (customizedlist == false) {
    //     addRowchecklist();
    // }

    function addRowchecklist(item_name = null, item_id = null, dateFrom = null, dateTo = null) {
        var uniqId = Math.floor((Math.random() * 1000000000) + 1);
        var html = '';
        html = html + '<tr id="trId-' + uniqId + '" uniqId="' + uniqId + '" class="customizedlist">';
        html = html + '    <td class="text-center"><emp><h2 class="this"></h2></emp></td>';
        html = html + '    <td>  <input name="json[' + uniqId + '][item_name]" id="item_name-' + uniqId +
            '" type="text" class="form-control" value="' + item_name + '" data-id="' + item_id + '" required></td>';

        html = html + '<td>';
        // html = html + '    <div class="input-group date" id="from-00-' + uniqId + '" data-target-input="nearest">';
        html = html + '        <input name="json[' + uniqId + '][start]" id="start-' + uniqId +
            '" type="text" class="form-control datepickerYMD"  autocomplete="off" readonly required />';
        //                            html = html + '        <div class="input-group-append" data-target="#from-00-' + uniqId + '" data-toggle="datetimepicker">';
        //                            html = html + '            <span class="input-group-text">';
        //                            html = html + '                <i class="ki ki-calendar"></i>';
        //                            html = html + '            </span>';
        //                            html = html + '        </div>';
        //html = html + '    </div>';
        html = html + '</td>';
        html = html + '<td>';
        // html = html + '    <div class="input-group date" id="to-01-' + uniqId + '" data-target-input="nearest">';
        html = html + '        <input name="json[' + uniqId + '][end]" id="end-' + uniqId +
            '" type="text" class="form-control datepickerYMD" readonly autocomplete="off" required />';
        //  html = html + '        <div class="input-group-append" data-target="#to-01-' + uniqId + '" data-toggle="datetimepicker">';
        // html = html + '            <span class="input-group-text">';
        // html = html + '                <i class="ki ki-calendar"></i>';
        // html = html + '            </span>';
        //html = html + '        </div>';
        //  html = html + '    </div>';

        html = html + '</td>';



        html = html + '    <td class="text-center">';
        html = html + '        <a uniqId="' + uniqId +
            '"itemId="' + item_id +
            '" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary autoupdate" todoid="1" onclick="deleteRowchecklist(this);" title="Remove"><i class="fas fa-trash-alt"></i></a>';
        html = html + '    </td>';
        html = html + '</tr> ';
        $('#checklist_table_body').append(html);
        $('#start-' + uniqId).datetimepicker({
            defaultDate: new Date(dateFrom),
            format: 'YYYY-MM-DD',
        });
        $('#end-' + uniqId).datetimepicker({
            defaultDate: new Date(dateTo),
            format: 'YYYY-MM-DD',
        });

    }
    //  Google.validateProfile();
    function validateForm() {
        $('#customized-checklist').hide();
        $('#plzwait').show();
        setTimeout(function() {
            a();
        }, 300);

        function a() {
            var form = new FormData();
            form.append('_token', '{{ csrf_token() }}');
            var isValid = true;
            $('.customizedlist').each(function(i) {
                var uniqId = $(this).attr('uniqId');
                if ($('#item_name-' + uniqId).val() == '') {
                    $('#item_name-' + uniqId).focus();
                    $('#item_name-' + uniqId).css('border-color', 'red');
                    isValid = false;
                } else {
                    $('#item_name-' + uniqId).css('border-color', '');
                    form.append('json[' + i + '][item_name]', $('#item_name-' + uniqId).val());
                }
                if ($('#start-' + uniqId).val() == '') {
                    $('#start-' + uniqId).focus();
                    $('#start-' + uniqId).css('border-color', 'red');
                    isValid = false;
                } else {
                    $('#start-' + uniqId).css('border-color', '');
                    form.append('json[' + i + '][start]', $('#start-' + uniqId).val());
                }
                if ($('#end-' + uniqId).val() == '') {
                    $('#end-' + uniqId).focus();
                    $('#end-' + uniqId).css('border-color', 'red');
                    isValid = false;
                } else {
                    $('#end-' + uniqId).css('border-color', '');
                    form.append('json[' + i + '][end]', $('#end-' + uniqId).val());
                }
            });
            if (isValid == false) {
                $('#customized-checklist').show();
                $('#plzwait').hide();
                return false;
            }
            var json = ajaxpost(form, "");
            try {
                var json = jQuery.parseJSON(json);
                if (json.status == true) {
                    window.location = "{{ url('/checklist/' . $eventId) }}";
                }
            } catch (e) {
                alert(e);
            }


        }
    }

    function deleteRowchecklist(e) {
        var uniqId = $(e).attr('uniqId');
        var item_id = $(e).attr('itemId');
        var customizedlist = $('.customizedlist').length;
        // if (customizedlist == '1') {
        //     return false;
        // }
        $('#trId-' + uniqId).remove();
        $('.addItemBtn_' + item_id).removeClass('disabled');
    }

    var addDays = 0;

    function updateChecklistItems(data) {
        var row = '';
        var count = 1;
        $('#checklist_items_table_body').empty();
        if (data.length > 0) {
            data.forEach(element => {
                row += `<tr>
                        <th scope="col" class="td-index">` + count + `</th>
                        <td>` + element.name + `</td>
                        <td class="text-center">
                            <a data-id="` + element.id + `" data-item_name="` + element.name + `" data-no_of_days="` +
                    element.no_of_days + `" data-start="` + element.start + `"  data-end="` + element.end + `"
                                id="addItemBtn_` + element.id +
                    `"
                                class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary autoupdate addItemBtn addItemBtn_` +
                    element.id + `"
                                title="Add"><i class="fas fa-plus"></i></a>
                        </td>
                    </tr>`;
                count++;
            });
        } else {
            row += `<tr>
                        <th scope="col" colspan="3" class="td-index">
                            No Data...
                            </th>
                    </tr>`;
        }

        $('#checklist_items_table_body').append(row);
        $('.addItemBtn').on('click', function() {
            var planning_date = new Date("{{ $event->planning_date }}");
            var event_date = new Date("{{ $event->event_date }}");
            var diffTime = Math.abs(planning_date - event_date);
            var workingDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            var defaultTotalDays = "{{ $defaultTotalDays }}";
            var item_name = $(this).data('item_name');
            var item_id = $(this).data('id');
            var start = $(this).data('start');
            var end = $(this).data('end');
            var no_of_days = $(this).data('no_of_days');
            if (no_of_days) {
                var calcDays = no_of_days / defaultTotalDays * workingDays;
                // calcDays = calcDays;
                // start = addDays;
                end = start + calcDays;
            } else {
                // start = addDays;
                end = start;
            }
            if (no_of_days) {
                addDays = addDays + calcDays;
            }

            // var item_element = $('#addItemBtn_'+item_id);
            $(this).addClass('disabled');

            var dateFrom = planning_date;
            var dateFrom = dateFrom.setDate(planning_date.getDate() + start);

            var dateTo = planning_date;
            dateTo = dateTo.setDate(planning_date.getDate() + end);
            addRowchecklist(item_name, item_id, dateFrom, dateTo, );
        });
    }

    $('.addItemBtn').on('click', function() {

        var item_name = 'name';
        var item_id = 'id';
        addRowchecklist(item_name, item_id);
    });

    $('#occasions').on('change', function() {
        var id = $(this).find(':selected').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "GET",
            url: "/vendor/get-checklists/" + id,
            success: function(data) {
                if (data) {
                    updateChecklistItems(data);
                }
            },
            error: function(e) {}
        });
    });
</script>
@endsection
