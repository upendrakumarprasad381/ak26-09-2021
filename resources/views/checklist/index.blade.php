{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')

@section('sub-header')
    <?php
    $event = App\Helpers\LibHelper::GetvendoreventsById($eventId);
    ?>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/event/' . $eventId . '') }}" class="text-muted">{{ $event->event_name }}</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('checklist/' . $eventId) }}" class="text-muted">Checklist</a>
    </li>
@endsection
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->

        <?php
        $vendor = \App\Helpers\LibHelper::GetvendordetailsBy(Auth::user()->id);
        if (isset($_POST['addListBtn'])) {

            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json['start'] = date('Y-m-d', strtotime($json['start']));
            $json['end'] = date('Y-m-d', strtotime($json['end']));


            $json['updated_at'] = date('Y-m-d H:i:s');
            if (empty($json['id'])) {
                $json['event_id'] = $eventId;
                $json['vendor_id'] = Auth::user()->id;
                $json['id'] = \App\Database::insert('original_checklist', $json);
            } else {
                \App\Database::updates('original_checklist', $json, ['id' => $json['id']]);
            }
            $chelist = App\Helpers\LibHelper::GetoriginalchecklistById($json['id']);
            $calender_id_start = \App\Helpers\CommonHelper::updateMycalender($calenderId = $chelist->calender_id_start, $type = 'CHECK_LIST', $chelist->event_id, $primaryId = $json['id'], $title = $chelist->item_name, $eventDate = $json['start']);
            $calender_id_end = \App\Helpers\CommonHelper::updateMycalender($calenderId = $chelist->calender_id_end, $type = 'CHECK_LIST', $chelist->event_id, $primaryId = $json['id'], $title = $chelist->item_name, $eventDate = $json['end']);
        }

        $Sql = "SELECT * FROM `original_checklist` WHERE  vendor_id ='" . Auth::user()->id . "' AND event_id='$eventId'";
        $data = \App\Database::select($Sql);
        if (empty($data)) {
            ?>
        <div class="row _toListSbbnwrp">
            <div class="__vtrTpBtnWrp __tabBnNeUi d-flex wd100">
                <a href="<?= url("checklist/$eventId/original-checklist") ?>" class="__vrttbtn ">
                    <i class="flaticon-clipboard"></i>
                    Original checklist</a>
                <a href="<?= url("checklist/$eventId/customized-checklist") ?>" class="__vrttbtn ">
                    <i class="flaticon-clipboard"></i>
                    Customized checklist</a>
            </div>
        </div>
        <?php } else {
            ?>
        <div class="__guestListWrp wd100">
            <div class="row">
                <div class="__vtrTpBtnWrp d-flex wd100 __guestListMBnt ">
                    <?php
                    $Sql33 = "SELECT COUNT(id) AS total FROM `original_checklist` WHERE  vendor_id ='" . Auth::user()->id . "' AND event_id='$eventId' ";
                    $countAll = \App\Database::selectSingle($Sql33);
                    $Sql22 = $Sql33 . " AND end < '" . date('Y-m-d') . "' AND completed=0";
                    $countoverDue = \App\Database::selectSingle($Sql22);
                    $Sql11 = $Sql33 . '  AND completed=1';
                    $countcomplited = \App\Database::selectSingle($Sql11);

                    $Sql_delay = $Sql33 . " AND DATEDIFF(end ,'" . date('Y-m-d') . "') < 0 AND completed=0";
                    $delay = \App\Database::selectSingle($Sql_delay);

                    $Sql_critical = $Sql33 . " AND DATEDIFF(end ,'" . date('Y-m-d') . "') <= 3 AND DATEDIFF(end ,'" . date('Y-m-d') . "') >= 0 AND  completed=0";
                    $critical = \App\Database::selectSingle($Sql_critical);

                    $Sql_upcoming = $Sql33 . " AND DATEDIFF(end ,'" . date('Y-m-d') . "') >= 4 AND DATEDIFF(end ,'" . date('Y-m-d') . "') <= 8 AND  completed=0";
                    $upcoming = \App\Database::selectSingle($Sql_upcoming);

                    $Sql_coming = $Sql33 . " AND DATEDIFF(end ,'" . date('Y-m-d') . "') > 8 AND  completed=0";
                    $coming = \App\Database::selectSingle($Sql_coming);

                    ?>
                    <a href="<?= url("checklist/$eventId/delayed") ?>"
                        class="__vrttbtn <?= $filterId == 'delayed' ? 'active' : '' ?>">Delayed -
                        <?= $delay->total ?></a>
                    <a href="<?= url("checklist/$eventId/critical") ?>"
                        class="__vrttbtn <?= $filterId == 'critical' ? 'active' : '' ?>">Critical -
                        <?= $critical->total ?> </a>
                    <a href="<?= url("checklist/$eventId/upcoming") ?>"
                        class="__vrttbtn <?= $filterId == 'upcoming' ? 'active' : '' ?>">Upcoming -
                        <?= $upcoming->total ?> </a>
                    <a href="<?= url("checklist/$eventId/coming") ?>"
                        class="__vrttbtn <?= $filterId == 'coming' ? 'active' : '' ?>">Coming -
                        <?= $coming->total ?></a>

                </div>
            </div>
            <div class="wd100 __tpExpBtnWrp">
                <div class="d-flex">
                    <div class="__ppserch d-flex align-items-center py-3 py-sm-0 px-sm-3 " style="width: 86%;">
                        <input type="text" class="form-control border-0 font-weight-bold pl-2" id="myInput"
                            placeholder="Search keyword...">
                    </div>
                    <div class=" __topSwitchBtn">
                        <button class="btn btn-primary  " onclick="showModal(this);" json="" type="button">Add
                            Item</button>
                    </div>

                </div>
            </div>



            <div class="__guestListTavw __scopeWorkTbvw  wd100">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col" class="text-center __brdsLt">S.N</th>
                                <th scope="col">Item Name</th>

                                <th scope="col" class="text-center">Timeline</th>
                                <!--                                    <th scope="col" class="text-center">Days</th>-->
                                <th scope="col" class="text-center">Status</th>

                                <th scope="col" class="text-center __brdsRt">Action</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                            <?php
                                $cond = "";
                                if ($filterId == 'delayed') {
                                    $cond = $cond . " AND DATEDIFF(end ,'" . date('Y-m-d') . "') < 0 AND completed=0";
                                }
                                else if ($filterId == 'critical') {
                                    $cond = $cond . " AND DATEDIFF(end ,'" . date('Y-m-d') . "') <= 3 AND DATEDIFF(end ,'" . date('Y-m-d') . "') >= 0 AND completed=0";
                                }
                                else if ($filterId == 'upcoming') {
                                    $cond = $cond . " AND DATEDIFF(end ,'" . date('Y-m-d') . "') >= 4 AND DATEDIFF(end ,'" . date('Y-m-d') . "') <= 8 AND completed=0";
                                }
                                else if ($filterId == 'coming') {
                                    $cond = $cond . " AND DATEDIFF(end ,'" . date('Y-m-d') . "') > 8 AND completed=0";
                                }
                                 else if ($filterId == 'completed') {
                                    $cond = $cond . "  AND completed=1";
                                }

                                $Sql = "SELECT MIN(start) AS start,MAX(end) AS end FROM `original_checklist` WHERE  vendor_id ='" . Auth::user()->id . "' AND event_id='$eventId' $cond";
                                $minMax = \App\Database::selectSingle($Sql);

                                $datetime1 = new DateTime($minMax->start);
                                $datetime2 = new DateTime($minMax->end);
                                $interval = $datetime1->diff($datetime2);
                                $workingDays = $interval->format('%a');


                                $if9LessThen="IF( DATEDIFF(`end`, CURDATE()) < 9,DATEDIFF(`end`, CURDATE()),9   ) ";
                                $select = ",IF(OC.completed=0,DATEDIFF(`end`, CURDATE()),$if9LessThen) AS diffrent";
                                $join = " ";
                                $Sql = "SELECT OC.* $select FROM `original_checklist` OC WHERE  vendor_id ='" . Auth::user()->id . "' AND event_id='$eventId' $cond ORDER BY diffrent ASC,end ASC";
                                $data = \App\Database::select($Sql);

                                for ($i = 0; $i < count($data); $i++) {
                                    $d = $data[$i];

                                    $d->startYMD = date('d-m-Y', strtotime($d->start));
                                    $d->endYMD = date('d-m-Y', strtotime($d->end));


                                    $isValid = $d->end > date('Y-m-d');
                                    ?>
                            <tr>
                                <td class="text-center"><?= $i + 1 ?></td>
                                <td><?= $d->item_name ?> </td>
                                <td class="text-center">
                                    <?= date('d M Y', strtotime($d->start)) . ' - ' . date('d M Y', strtotime($d->end)) ?>
                                </td>

                                <td class="text-center">
                                    <div class="__statusChkWrap __nowrap">

                                        <statusId<?= $d->id ?>>
                                            <?php
                                            $mystatus = '';
                                            // if (!empty($isValid)) {
                                                if ($d->diffrent >= 0 && $d->diffrent <= 3) {
                                                    $mystatus = '<span class="label label-info label-inline mr-2 critical" style="background-color: orange !important;">Critical</span>';
                                                } elseif ($d->diffrent >= 4 && $d->diffrent <= 8) {
                                                    $mystatus = '<span class="label label-dark  label-inline mr-2 upcoming">Upcoming</span>';
                                                } elseif ($d->diffrent < 0) {
                                                    $mystatus = '<span class="label label-danger label-inline mr-2 delayed">Delayed</span>';
                                                } else {
                                                    $mystatus = '<span class="label label-info label-inline mr-2 nextupcoming">Coming</span>';
                                                }
                                            // } else {
                                            //     // $mystatus = '<span class="label label-danger label-inline mr-2 delayed">Delayed</span>';
                                            // }

                                            if (empty($d->completed)) {
                                                echo $mystatus;
                                            } else {
                                                echo '<span class="label label-success label-inline mr-2 completed">Completed</span>';
                                            }

                                            ?>
                                        </statusid<?= $d->id ?>>

                                        <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input mystatus='<?= $mystatus ?>'
                                                <?= !empty($d->completed) ? 'checked' : '' ?>
                                                onclick="updateMyToDoASComplite(this);" primaryId="<?= $d->id ?>"
                                                atype="1" type="checkbox" name="checkedId-<?= $i ?>">
                                            <span></span>
                                            &nbsp;
                                        </label>

                                    </div>

                                </td>
                                <td class="text-center">
                                    <div class="__nowrap">
                                        <a class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary autoupdate"
                                            atype="1" todoId='<?= $d->id ?>' onclick="deleteTodo(this);"
                                            title="Remove"><i class="fas fa-trash-alt"></i></a>
                                        <a onclick="showModal(this);" json="<?= base64_encode(json_encode($d)) ?>"
                                            class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary"
                                            href="javascript:void(0)"><i class="far fa-edit"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td class="text-center"><?= $i + 1 ?></td>
                                <td></td>
                                <td class="text-center"><?= $event->occasions_name ?> Days</td>
                                <td class="text-center"><?= $workingDays ?> Days</td>
                                <td class="text-center"></td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>

            <div class="wd100 text-right mt-5">
                <!--  <button type="button" class="btn btn-primary __guestLisBnt">Save Checklist</button>
                           <button type="button" class="btn btn-primary __guestLisBnt">Share Link</button>-->
            </div>



        </div>
        <?php
        }
        ?>




        <!--end::Container-->
    </div>
</div>
<!--begin::Entry-->
<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Add New List</h5>
            </div>
            <form method="post" action="">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="json[id]" id="pid" value="">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Items Name</label>
                            <textarea class="form-control form-control-lg" name="json[item_name]" id="item_name"
                                required rows="3" placeholder=""></textarea>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Start</label>
                            <input type="text" id="start" value="" class="form-control datepicker" autocomplete="off"
                                name="json[start]" required readonly />
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">End</label>
                            <input type="text" id="end" value="" class="form-control datepicker" autocomplete="off"
                                name="json[end]" required readonly />
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="addListBtn" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')

<script>
    function deleteTodo(e) {
        Swal.fire({
            title: "Are you sure?",
            text: "You wont be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: true
        }).then(function(result) {
            if (result.value) {
                var data = {
                    _token: "{{ csrf_token() }}",
                    todo_id: $(e).attr('todoId')
                };
                data['type'] = $(e).attr('atype');
                $.ajax({
                    url: "{{ url('/to-do-list/delete-todo') }}",
                    cache: false,
                    data: data,
                    async: false,
                    type: 'POST',
                    success: function(res) {
                        res = jQuery.parseJSON(res);
                        if (res.status == true) {
                            location.reload();
                        }
                    }
                });
            } else if (result.dismiss === "cancel") {
                Swal.fire(
                    "Cancelled",
                    "Your imaginary file is safe :)",
                    "error"
                )
            }
        });
    }

    function updateMyToDoASComplite(e) {
        var iscompleted = $(e).is(':checked') ? 1 : 0;
        var mystatus = $(e).attr('mystatus');
        var compliteHTML = '<span  class="label label-info label-inline mr-2 completed">Completed</span>';
        var data = {
            _token: "{{ csrf_token() }}"
        };
        data['toDoASComplite'] = '1';
        data['iscompleted'] = iscompleted;
        data['type'] = $(e).attr('atype');
        data['primaryid'] = $(e).attr('primaryid');





        Swal.fire({
            title: 'Comment',
            input: 'textarea'
        }).then(function(result) {
            if (result.value) {
                data['comment'] = result.value;
                ad();
            } else {
                Swal.fire('Comment is required');
                $(e).prop('checked', !iscompleted);
                return false;
            }
        });

        function ad() {
            $.ajax({
                url: "{{ url('/to-do-list') }}",
                cache: false,
                data: data,
                async: false,
                type: 'POST',
                success: function(res) {
                    if (iscompleted == '1') {
                        $('statusId' + data['primaryid']).html(compliteHTML);
                    } else {
                        $('statusId' + data['primaryid']).html(mystatus);
                    }
                    console.log(res);
                }
            });
        }
    }
    $(document).ready(function() {
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });

    function showModal(e) {
        var json = $(e).attr('json');
        if (json != '') {
            json = jQuery.parseJSON(atob(json));
            if (json.id) {
                $('#pid').val(json.id);
                $('#start').val(json.startYMD);
                $('#end').val(json.endYMD);
                $('#item_name').val(json.item_name);
            } else {
                setEmpty();
            }
        } else {
            setEmpty();
        }

        function setEmpty() {
            $('#pid').val('');
            $('#start').val('');
            $('#end').val('');
            $('#item_name').val('');
        }

        $('#staticBackdrop').modal('show');
    }
</script>
@endsection
