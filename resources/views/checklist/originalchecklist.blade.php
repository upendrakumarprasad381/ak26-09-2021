{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')
<!--begin::Entry-->
@section('sub-header')
<?php
$event = App\Helpers\LibHelper::GetvendoreventsById($eventId);
?>
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/event/' . $eventId . '') }}" class="text-muted">{{ $event->event_name }}</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{url('checklist/'.$eventId)}}" class="text-muted">Checklist</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="" class="text-muted">Original Checklist</a>
</li>
@endsection
<?php
$Sql = "SELECT SUM(no_of_days) AS no_of_days FROM `original_checklist_master` OC WHERE OC.occasion_id='$event->occasion_id'";
$defaultTotalDays = App\Database::selectSingle($Sql);
$defaultTotalDays = !empty($defaultTotalDays->no_of_days) ? $defaultTotalDays->no_of_days : '0';
?>
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->

        <form method="post">
            @csrf
            <div class="__guestListWrp wd100"> 
                <h4 class="mb-0 font-weight-bolder">Planning Dates <?= date('d-m-Y',  strtotime($event->planning_date)) ?> To <?= date('d-m-Y',  strtotime($event->event_date)) ?></h4>
                <div class="__guestListTavw __orgChecklistTab __scopeWorkTbvw  wd100"> 
  
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col" class="text-center __brdsLt">S.N</th>
                                    <th scope="col" class="text-center">Item Name</th>
                                    <th scope="col" class="text-center">Timeline</th>
                                    <th scope="col" class="text-center">Days</th>
                                    <th scope="col" class="text-center __brdsRt">Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $datetime1 = new DateTime($event->planning_date);
                                $datetime2 = new DateTime($event->event_date);
                                $interval = $datetime1->diff($datetime2);
                                $workingDays = $interval->format('%a') + 1;


                                $select = "";
                                $join = " ";
                                $Sql = "SELECT OC.* FROM `original_checklist_master` OC WHERE OC.occasion_id='$event->occasion_id'";

                                $data = \App\Database::select($Sql);
                                $addDays = 0;
                                if (!empty($data)) {
                                    for ($i = 0; $i < count($data); $i++) {
                                        $d = $data[$i];
                                        $defaultComplite = ($d->end - $d->start) + 1;


                                        if (!empty($d->no_of_days)) {
                                            $calcDays = $d->no_of_days / $defaultTotalDays * $workingDays;
                                            $calcDays = (int) $calcDays;
                                            $start = $addDays;
                                            $end = $start + $calcDays;
                                        } else {
                                            $start = $addDays - $calcDays;
                                            $end = $start + $calcDays;
                                        }
                                        $dateFrom = date('d M Y', strtotime($event->planning_date . " +$start day"));
                                        $dateTo = date('d M Y', strtotime($event->planning_date . " +$end day"));
                                        ?>

                                        <tr id="trId-<?= $i ?>">
                                    <input type="hidden" name="json[<?= $i ?>][item_name]" value="<?= $d->name ?>">
                                    <input type="hidden" name="json[<?= $i ?>][start]" value="<?= $dateFrom ?>">
                                    <input type="hidden" name="json[<?= $i ?>][end]" value="<?= $dateTo ?>">
                                    <input type="hidden" name="json[<?= $i ?>][no_of_days]" value="<?= $calcDays ?>">
                                    <input type="hidden" name="json[<?= $i ?>][is_add]" value="<?= !empty($d->no_of_days) ? '1' : '0' ?>">
                                    <td class="text-center"><p class="mb-0"></p></td>
                                    <td class="text-center"><?= $d->name ?></td>
                                    <td class="text-center"> <span class="__taboutldateFrom"><?= "$dateFrom" . '-' . " $dateTo" ?></span></td>
                                    <td class="text-center"><span class="__taboutldays"><?= $calcDays ?> days to complete</span>  </td>
                                    <td><span class="__taboutldays"> <a href="javascript:void(0)" remvid="trId-<?= $i ?>" onclick="removethisTR(this);">Remove</a></span>     </td>
                                    </tr>  
                                    <?php
                                    if (!empty($d->no_of_days)) {
                                        $addDays = $addDays + $calcDays;
                                    }
                                }
                                ?>
                                <tr>
                                    <td class="text-center"><p class="mb-0"></p></td>
                                    <td class="text-center"><?= $event->occasions_name ?> Day</td>
                                    <td class="text-center"> </td>
                                    <td colspan="2" class="text-center"><span class="__taboutldays"><?= $addDays ?> days</span></td>
                                </tr> 
                                <tr>
                                    <td class="text-center"><p class="mb-0"></p></td>
                                    <td class="text-center">Wedding Date</td>
                                    <td class="text-center"> </td>
                                    <td colspan="2" class="text-center"><span class="__taboutldays"><?= date('d/m/Y', strtotime($event->event_date)) ?> </span></td>
                                </tr> 
                            <?php } else {
                                ?><tr><td colspan="5">No check list master found for <?= $event->occasions_name ?></td></tr><?php }
                            ?>
                            </tbody>
                        </table> 

                    </div>
                </div>

                <div class="wd100 text-right mt-5">
                    <button type="submit" name="original-checklist" class="btn btn-primary __guestLisBnt">Save Checklist</button>
                    <!--         <button type="button" class="btn btn-primary __guestLisBnt">Share Link</button>-->
                </div>



            </div>
        </form>



        <!--end::Container-->
    </div>
</div>
<!--begin::Entry-->
<style>
    body {
        counter-reset: checklistcounter;
    }
    td p::before {
        /* Increment "my-sec-counter" by 1 */
        counter-increment: checklistcounter;
        content: counter(checklistcounter) ;
    }
</style>

@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
    function removethisTR(e) {
        var remvid = $(e).attr('remvid');
        $('#' + remvid).remove();
    }
</script>
@endsection
