{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')
<?php
 $event = App\Helpers\LibHelper::GetvendoreventsById($eventId);
?>
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/event/' . $eventId . '') }}" class="text-muted">{{ $event->event_name }}</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="<?= url("create-web-page/$eventId") ?>" class="text-muted">Create Web Page</a>
</li>
@endsection


<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->

        <div class="__tmxDesignWrp wd100">


            <h3 class="mb-5">Find Your Design</h3>

            <div class="row">
                <?php
                $Sql = "SELECT * FROM `web_page_design` WHERE status=0";
                $list = App\Database::select($Sql);
                $webpage = App\Helpers\LibHelper::GetwebpagedesigneventId($eventId);
               
                for ($i = 0; $i < count($list); $i++) {
                    $d = $list[$i];
                    $basePath = url("web-pages/$d->logo");
                    $url = url("create-web-page/$eventId/$d->id");
                    ?>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 __tmxDesignbZ">
                        <div class="wd100 __tmxDesigWrz">
                            <div class="__tmxDesiimg wd100">
                                <img src="<?= $basePath ?>" class="img-fluid">
                            </div>
                            <div class="__tmxDesiTex wd100 ">
                                <h5><?= $d->title ?></h5>
                                <h6><?= $d->description ?> <?= !empty($webpage->web_page_id) && $webpage->web_page_id == $d->id ? '<a class="selicon"> <i class="fas fa-check"></i></a>' : '' ?> </h6>



                            </div>
                            <div class="wd100 __temWebBnt_item">
                                <?php
                                if (!empty($webpage->web_page_id) && $webpage->web_page_id == $d->id) {
                                    ?> <a  themeId="<?= $d->id ?>" href="<?= url("web-page/settings/$eventId") ?>" > <button class="btn btn-secondary __temWebBnt">Update</button></a><?php
                                } else {
                                    ?><a xhref="<?= $url ?>" slug="<?= $event->event_name ?>" themeId="<?= $d->id ?>" href="javascript:void(0)" msg="<?= empty($webpage) ? 'Are you sure want to use this web page!' : 'Are you sure want to use this web page!. May be you will loss of few of data.' ?>" onclick="delImg(this);"> <button class="btn btn-secondary __temWebBnt">Select </button></a><?php }
                                ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>








            </div>





        </div>



        <!--end::Container-->
    </div>
</div>
<!--begin::Entry-->
<style>
    .selicon{
        float: right;
    }
    .selicon i{
        color: #4caf50;
        font-size: 23px;
    }
</style>

@endsection

{{-- Scripts Section --}}
@section('scripts')
<!--begin::Page Scripts(used by this page)-->


<script>
    function delImg(e) {
        var urlDel = "{{url('create-web-page/'.$eventId)}}";
        var themeId = $(e).attr('themeId');
     
        var slug=convertToSlug($(e).attr('slug'));
        var data = {type: 'updateTheme', themeId: themeId,slug:slug};
          $.ajax({
                    async: false,
                    url: urlDel,
                    data: data,
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function (data) {
                       
                        setTimeout(function () {
                            window.location = "{{url('web-page/settings/'.$eventId)}}";
                        }, 250);
                    },
                    error: function (e) {
                    }
                });
    }
    function convertToSlug(Text)
    {
        return Text
                .toLowerCase()
                .replace(/ /g, '-')
                .replace(/[^\w-]+/g, '')
                ;
    }
</script>
<!--end::Page Scripts-->
@endsection
