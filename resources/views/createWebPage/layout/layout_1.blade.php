<!DOCTYPE html>
<?php
$event = App\Helpers\LibHelper::GetvendoreventsById($eventId);
$webpage = App\Helpers\LibHelper::GetwebpagedesigneventId($eventId);
$theme = App\Helpers\LibHelper::GetwebpagedesignId($webpage->web_page_id);
?>
<html lang="en">
    <!--begin::Head-->
    <head>
        <base href="">
        <meta charset="utf-8" />
        <title><?= $event->event_name ?> | Ask Deema</title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link href="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/wedding_theme/css/style.css" rel="stylesheet">
        <link href="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
        <?php if ($theme->id == '1') { ?>
            <link href="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/assets/css/theme1.css" rel="stylesheet" type="text/css" />
        <?php } else {
            ?><link href="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/assets/css/theme2.css" rel="stylesheet" type="text/css" /><?php }
        ?>
        <link rel="shortcut icon" href="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/assets/media/logos/favicon.ico" />
    </head>

    <body>
        <!-- ======= Header ======= -->
        <header id="header" class="fixed-top  header-transparent ">
            <div class="container d-flex align-items-center justify-content-center">

                <nav id="navbar" class="navbar">
                    <ul>
                        <li><a class="nav-link scrollto active" href="index.html#hero">Home</a></li>
                        <li><a class="nav-link scrollto" href="index.html#story">Our Story</a></li>
                        <li><a class="nav-link scrollto" href="index.html#gallery">Gallery</a></li>
                        <li><a class="nav-link scrollto" href="index.html#wedding_party">Abount The Wedding</a></li>
                        <li class="__noneborder"><a class="nav-link scrollto" href="index.html#travel">Travel</a></li>
                    </ul>
                    <i class="bi bi-list mobile-nav-toggle"></i>
                </nav><!-- .navbar -->

            </div>
        </header><!-- End Header -->
        <section id="hero"  class="__topPrtWrp">
            <div class="container">
                <h1 class="__mName"><?= $webpage->event_title ?></h1>
                <h2 class="__mDate"><?= date('F d-m, Y', strtotime($event->event_date)) ?></h2>
                <h3 class="__mLocation"><?= $webpage->location ?></h3>

                <div class="__mBanner  ">
                    <?php
                    $img = "web-pages/images/$webpage->logo";
                    $img = is_file(Config::get('constants.HOME_DIR') . $img) ? url($img) : url('web-pages/image-addicon.webp');
                    ?>
                    <img class="img-fluid" src="<?= $img ?>" />
                </div>
            </div>
        </section>


        <section id="story" class="__storySect">
            <div class="container">
                <div class="__storyWrp">
                    <h2><?= $webpage->about_title ?></h2>
                    <p><?= $webpage->about ?></p>
                </div>
            </div>
        </section>


        <section id="gallery" class="__gallerySect">
            <div class="container">
                <div class="__galleryWrp    row-cols-2 row-cols-sm-2 row-cols-md-3 row-cols-lg-4">

                    <?php
                    $Sql = "SELECT * FROM `web_page_design_event_images` WHERE event_id='$eventId'";
                    $list = App\Database::select($Sql);
                    for ($i = 0; $i < count($list); $i++) {
                        $d = $list[$i];
                        ?>
                        <div class="__galyBoz">
                            <div class="__galyBpiz">
                                <a data-fslightbox="gallery" href="<?= url('web-pages/images/' . $d->images) ?>">
                                    <img class="img-fluid" src="<?= url('web-pages/images/' . $d->images) ?>" />
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php
        $list = !empty($webpage->list) ? json_decode($webpage->list, true) : '';
        $list = !empty($list) && is_array($list) ? $list : [];
        for ($i = 0; $i < count($list); $i++) {
            $d = $list[$i];
            ?>
            <section id="wedding_party" class="__wedding_partySect">
                <div class="container">
                    <div class="__wedding_partySectWrp">
                        <h2><?= !empty($d['title']) ? $d['title'] : '' ?></h2>
                        <p><?= !empty($d['description']) ? $d['description'] : '' ?></p>
                        <?php
                        if((count($list)-1)==$i){
                            echo '<img  class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/assets/img/theme/wedbom.png"/>';
                        }
                        ?>
                    </div>
                </div>
            </section>
        <?php } ?>

<!--        <section id="travel" class="__TravelSect">
            <div class="container">
                <div class="__TravelSectWrp">
                    <h2>Travel</h2>
                    <p><= $webpage->travel ?></p>
                    <img  class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/assets/img/theme/wedbom.png"/>
                </div>
            </div>
        </section>-->

        <footer>
            <div class="container text-center">
                © <?= date('Y') ?> Copyright AskDeema.com | Designed & Developed by Alwafaa Group
            </div>

        </footer>
    </body>
    <script src="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/assets/js/scripts.bundle.js"></script>
    <script src="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/assets/js/fslightbox.js"></script>
    <script src="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/wedding_theme/js/main.js"></script>
</html>
