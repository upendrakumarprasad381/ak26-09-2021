<!DOCTYPE html>
<?php
$event = App\Helpers\LibHelper::GetvendoreventsById($eventId);
$webpage = App\Helpers\LibHelper::GetwebpagedesigneventId($eventId);
$theme = App\Helpers\LibHelper::GetwebpagedesignId($webpage->web_page_id);
?>
<html lang="en">
    <!--begin::Head-->
    <head>
        <base href="">
        <meta charset="utf-8" />
        <title><?= $event->event_name ?> | Ask Deema</title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link href="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/wedding_theme/img/favicon.ico" rel="icon">
        <link href="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/wedding_theme/css/bootstrap.css" rel="stylesheet">
        <link href="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/wedding_theme/css/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/wedding_theme/css/style.css" rel="stylesheet">
        <?php if ($theme->id == '3') { ?>
            <link href="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/wedding_theme/css/theme3.css" rel="stylesheet" type="text/css" />
        <?php } else {
            ?>  <link href="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/wedding_theme/css/theme4.css" rel="stylesheet" type="text/css" /> <?php }
        ?>

    </head>

    <body>


        <!-- ======= Header ======= -->
        <header id="header" class="fixed-top  header-transparent ">
            <div class="container d-flex align-items-center justify-content-center">

                <nav id="navbar" class="navbar">
                    <ul>
                        <li><a class="nav-link scrollto active" href="index.html#hero">Home</a></li>
                        <li><a class="nav-link scrollto" href="index.html#story">Our Story</a></li>
                        <li><a class="nav-link scrollto" href="index.html#gallery">Gallery</a></li>
                        <li><a class="nav-link scrollto" href="index.html#wedding_party">Abount The Wedding</a></li>
                        <li class="__noneborder"><a class="nav-link scrollto" href="index.html#travel">Travel</a></li>
                    </ul>
                    <i class="bi bi-list mobile-nav-toggle"></i>
                </nav><!-- .navbar -->

            </div>
        </header><!-- End Header -->



        <section id="hero" class="__topPrtWrp">
            <div class="container">

                <?php if ($theme->id == '3') { ?>

                    <div class="__th3NaBowrp">
                        <div class="__monsdatlef">
                            <?= date('F', strtotime($event->event_date)) ?><br/>
                            <span><?= date('d-m, Y', strtotime($event->event_date)) ?></span>
                        </div>
                        <div class="__minNametopCr">
                            <div class="__toparrow">
                                <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/wedding_theme/img/theme3/nameLab_top.svg" />
                            </div>
                            <h1><?= $webpage->event_title ?></h1>
                            <!--                        <div class="namsuand">&</div>
                                                    <h1>Santy</h1>  -->
                            <div class="__toparrow">
                                <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/wedding_theme/img/theme3/nameLab_bottm.svg" />
                            </div>
                        </div>
                        <div class="__loctionright">
                            <?= $webpage->location ?>
                        </div>
                    </div>
                <?php } else {
                    ?>
                    <div class="__th3NaBowrp">
                        <div class="__minNametopCr">
                            <h1><?= $webpage->event_title ?></h1>
                        </div>
                        <div class="__datvw">
                            <div class="__monsdatlef">
                                <?= date('F', strtotime($event->event_date)) ?><br/>
                                <span><?= date('d-m, Y', strtotime($event->event_date)) ?></span>
                            </div>
                            <div class="__loctionright">
                                <?= $webpage->location ?>
                            </div>
                        </div>
                    </div>

                    <?php
                }
                ?>


                <div class="__mBanner">
                    <?php
                    $img = "web-pages/images/$webpage->logo";
                    $img = is_file(Config::get('constants.HOME_DIR') . $img) ? url($img) : url('web-pages/image-addicon.webp');
                    ?>
                    <div class="__mBannerImg wd100">
                        <img class="img-fluid" src="<?= $img ?>" />
                    </div>
                </div>



            </div>
        </section>






        <section id="story" class="__storySect">
            <div class="container">
                <div class="__storyWrp">
                    <h2><?= $webpage->about_title ?></h2>
                    <p><?= $webpage->about ?></p>
                    <?php if ($theme->id == '3') { ?>
                        <div class="wd100 __sec_bottomImg">
                            <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/wedding_theme/img/theme3/sec_bottom.svg" />
                        </div>
                    <?php } else {
                        ?>
                        <div class="wd100 __sec_bottomImg">
                            <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/wedding_theme/img/theme4/sec-bg.png" />
                        </div>
                    <?php }
                    ?>
                </div>
            </div>
        </section>


        <section id="gallery" class="__gallerySect">
            <div class="container">
                <h2>Gallery</h2>
                <div class="__galleryWrp    row-cols-2 row-cols-sm-2 row-cols-md-3 row-cols-lg-4">

                    <?php
                    $Sql = "SELECT * FROM `web_page_design_event_images` WHERE event_id='$eventId'";
                    $list = App\Database::select($Sql);
                    for ($i = 0; $i < count($list); $i++) {
                        $d = $list[$i];
                        ?>
                        <div class="__galyBoz">
                            <div class="__galyBpiz">
                                <a data-fslightbox="gallery" href="<?= url('web-pages/images/' . $d->images) ?>">
                                    <img class="img-fluid" src="<?= url('web-pages/images/' . $d->images) ?>" />
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php if ($theme->id == '3') { ?>
                    <div class="wd100 __sec_bottomImg">
                        <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/wedding_theme/img/theme3/sec_bottom.svg" />
                    </div>
                <?php } ?>
            </div>
        </section>



        <?php
        $list = !empty($webpage->list) ? json_decode($webpage->list, true) : '';
        $list = !empty($list) && is_array($list) ? $list : [];
        for ($i = 0; $i < count($list); $i++) {
            $d = $list[$i];
            ?>
            <section id="travel" class="__TravelSect">
                <div class="container">
                    <div class="__TravelSectWrp">
                        <h2><?= !empty($d['title']) ? $d['title'] : '' ?></h2>
                        <p><?= !empty($d['description']) ? $d['description'] : '' ?></p>
                        <?php if ($theme->id == '3') { ?>
                            <div class="wd100 __sec_bottomImg">
                                <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/wedding_theme/img/theme3/sec_bottom.svg" />
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </section>
        <?php } ?>
        <footer>
            <div class="footerObct">

            </div>
            <div class="container text-center">
                © <?= date('Y') ?> Copyright AskDeema.com | Designed & Developed by Alwafaa Group
            </div>

        </footer>

        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="fas fa-arrow-up"></i></a>
    </body>
    <script src="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/assets/js/scripts.bundle.js"></script>
    <script src="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/assets/js/fslightbox.js"></script>
    <script src="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/wedding_theme/js/main.js"></script>
</html>
