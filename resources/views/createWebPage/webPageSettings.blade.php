{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')
<?php
$event = App\Helpers\LibHelper::GetvendoreventsById($eventId);
$webpage = App\Helpers\LibHelper::GetwebpagedesigneventId($eventId);
$theme = App\Helpers\LibHelper::GetwebpagedesignId($webpage->web_page_id);
?>
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/event/' . $eventId . '') }}" class="text-muted">{{ $event->event_name }}</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="<?= url("create-web-page/$eventId") ?>" class="text-muted">Create Web Page</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="" class="text-muted">Web Pages Settings</a>
</li>
@endsection



<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->

        <!--begin::Dashboard-->

        <div class="__cmPgBlkz __cpgurl wd100 d-flex align-items-center justify-content-between flex-wrap  ">
            <div class="__cmPghdle">
                <h3 class="mb-0 font-weight-bolder">Your Wedding Website</h3>
                <div class="wd100 __urlprt">
                    <span id="p1"><?= url("web/" . $webpage->slug) ?></span>
                    <a class="__copy_btn" onclick="copyToClipboard('#p1')"> <i class="ki ki-copy"></i> Copy</a>
                    <!--<a class="__copy_btn"> <i class="flaticon2-edit"></i> Edit</a>-->
                </div>
            </div>

            <div class="__proslAddRtPrt">
                <!-- <button class="btn btn-primary mr-3 " type="button">Message Guests</button>-->
                <a href="<?= url("web/" . $webpage->slug) ?>" target="_blank"><button class="btn btn-secondary  " type="button"> Preview Site</button></a>
            </div>
        </div>


        <!--        <div class="__cmPgBlkz __registry wd100">
                    <h4 class="mb-0 font-weight-bolder">Let's set up your registry</h4>
                    <p>Share your wish list with guests by linking an existing registry or starting a new one.</p>
                </div>-->


        <div class="wd100 mt-10">
            <div class="row">

                <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12 __webEditeWrap" style="padding-top: unset;">

                    <div class="accordion accordion-light accordion-light-borderless accordion-svg-toggle" id="accordionExample7">

                        <div class="card">

                            <div class="card-header" id="headingOne7">
                                <div class="card-title" data-toggle="collapse" data-target="#collapseOne7">
                                    <span class="svg-icon svg-icon-primary">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                        </g>
                                        </svg>
                                    </span>
                                    <div class="card-label pl-4">Home</div>
                                </div>
                            </div>

                            <div id="collapseOne7XX" class="collapseXX show" data-parent="#accordionExample7">
                                <div class="card-body pl-12 pr-12">


                                    <div class="wd100">


                                        <div class="row">
                                            <div class="mb-12 col-lg-12 col-md-12 col-sm-12 __datewebPg">
                                                <label class="form-label">Event Title</label>
                                                <input type="text"  class="form-control"  id="event_title" placeholder="Wedding Title" value="<?= !empty($webpage->event_title) ? $webpage->event_title : '' ?>"">
                                            </div>
                                        </div>


                                        <div class="__addurWdLoction wd100 ">
                                            <div class="row">
                                                <div class="mb-12 col-lg-12 col-md-12 col-sm-12 __datewebPg" style="margin-bottom: 1rem !important;">



                                                    <label class="form-label">Add a Cover Photo </label>
                                                    <div></div>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="logo"/>
                                                        <label class="custom-file-label" for="logo">Choose file</label>
                                                    </div>


<!--<input type="file" id="logo" class="form-control" >-->
                                                </div>


                                                <div class="  col-lg-12 col-md-12 col-sm-12 __datewebPg_list">
                                                    <?php
                                                    $img = "web-pages/images/$webpage->logo";
                                                    $img = is_file(Config::get('constants.HOME_DIR') . $img) ? url($img) : url('web-pages/image-addicon.webp');
                                                    ?>
                                                    <div class="__codeThum">
                                                        <!--                                                        <div class="__covrClose">
                                                                                                                    <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                                                                                </div>-->
                                                        <img src="{{$img}}">
                                                    </div>
                                                </div>


                                                <div class="mb-6 col-lg-6 col-md-6 col-sm-12 __datewebPg">
                                                    <label class="form-label">Event Date </label>
                                                    <input type="text" readonly class="form-control" value="<?= date('F d, Y', strtotime($event->event_date)) ?>">
                                                </div>



                                                <div class="mb-6 col-lg-6 col-md-6 col-sm-12">
                                                    <label class="form-label">Location</label>
                                                    <input type="text" id="location" class="form-control" placeholder="Dubai-United Arab Emirates" value="<?= !empty($webpage->location) ? $webpage->location : $event->event_location ?>">
                                                </div>
                                            </div>
                                        </div>


                                    </div>


                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="headingTwo7">
                                <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseTwo7">
                                    <span class="svg-icon svg-icon-primary">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                        </g>
                                        </svg>
                                    </span>
                                    <div class="card-label pl-4">About</div>
                                </div>
                            </div>
                            <div id="collapseTwo7XX" class="collapseXX show" data-parent="#accordionExample7">
                                <div class="card-body pl-12 pr-12">
                                    <div class="wd100">
                                        <input class="form-control" placeholder="About title" value="<?= !empty($webpage->about_title) ? $webpage->about_title : '' ?>" id="about_title" >
                                        <br>
                                    </div>
                                    <div class="wd100">
                                        <textarea class="form-control" placeholder="About" id="about" rows="7"><?= !empty($webpage->about) ? $webpage->about : '' ?></textarea>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="headingThree7">
                                <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseThree7">
                                    <span class="svg-icon svg-icon-primary">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                        </g>
                                        </svg>
                                    </span>
                                    <div class="card-label pl-4">Photos</div>
                                </div>
                            </div>
                            <div id="collapseThree7XX" class="collapseXX show" data-parent="#accordionExample7">
                                <input type="file" style="display: none;" id="uploadimg" onchange="uploadImage();">



                                <div class="card-body pl-12 pr-12 wd100">
                                    <div class="wd100">
                                        <div class="__addImage">
                                            <a onclick="document.getElementById('uploadimg').click();" href="javascript:void(0)">
                                                <i class="fa fa-plus" aria-hidden="true"></i> <br/>
                                                Add Image
                                            </a>
                                        </div>
                                    </div>
                                    <div class="wd100" id="imglistt">
                                        <?php
                                        $Sql = "SELECT * FROM `web_page_design_event_images` WHERE event_id='$eventId'";
                                        $list = App\Database::select($Sql);
                                        for ($i = 0; $i < count($list); $i++) {
                                            $d = $list[$i];
                                            ?>
                                            <div class="__codeThum" id="imag-<?= $d->id ?>">
                                                <div class="__covrClose" imgId="<?= $d->id ?>" fullpath="<?= Config::get('constants.HOME_DIR') . 'web-pages/images/' . $d->images ?>" onclick="removeImage(this);">
                                                    <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                </div>
                                                <img src="<?= url('web-pages/images/' . $d->images) ?>"  >
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="card">
                            <div class="card-header" id="headingFour7">
                                <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseFour7">
                                    <span class="svg-icon svg-icon-primary">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                        </g>
                                        </svg>
                                    </span>
                                    <div class="card-label pl-4">Add More Details   <a class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary "  onclick="addnewTitle(this);" title="Remove"><i class="fas fa-plus"></i></a> </div>
                                </div>
                            </div>

                            <div id="collapseFour7XX" class="collapse show" data-parent="#accordionExample7">

                                <div class="card-body pl-12 pr-12" id="listData">
                                    <?php
                                    $list = !empty($webpage->list) ? json_decode($webpage->list, true) : '';
                                    $list = !empty($list) && is_array($list) ? $list : [];
                                    for ($i = 0; $i < count($list); $i++) {
                                        $d = $list[$i];
                                        $uniqId = uniqid();
                                        ?>
                                        <div id="list-<?= $uniqId ?>" uniqid="<?= $uniqId ?>" class="mylist">
                                            <div class="row">
                                                <div class="col-sm-11">
                                                    <div class="wd100">
                                                        <input class="form-control" id="title-<?= $uniqId ?>" placeholder="Title" value="<?= !empty($d['title']) ? $d['title'] : '' ?>" >
                                                        <br>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <a class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary" rmvid="list-<?= $uniqId ?>" onclick="deletetitle(this);" title="Remove"><i class="fas fa-trash-alt"></i></a>
                                                </div>
                                            </div>
                                            <div class="wd100">
                                                <textarea class="form-control" id="description-<?= $uniqId ?>" placeholder="Description" rows="7"><?= !empty($d['description']) ? $d['description'] : '' ?></textarea>
                                                <br>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>


                                </div>
                            </div>
                        </div>








                        <div class="wd100 text-right mt-5 mb-5">
                            <!-- <button type="submit" class="btn btn-secondary">Cancel</button>-->
                            <button type="button" onclick="submitMyform();" style="margin-right: 40px;" class="btn btn-primary">Submit</button>
                        </div>


                    </div>

                </div>



                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="__webEditeWrpRt">
                        <h4 class="font-weight-bolder text-dark">Your Theme</h4>	
                        <div class="wb100 __urShorPix">
                            <img class="img-fluid" src="<?= $theme->logo_url ?>">
                        </div>
                        <h5 class="mt-3 mb-0 font-weight-bolder text-dark">Ask Deema</h5>
                        <h6 class="text-dark"><?= $theme->title ?></h6>
                    </div>
                </div>

            </div>

        </div>




        <!--end::Container-->
    </div>
</div>
<!--begin::Entry-->


@endsection

{{-- Scripts Section --}}
@section('scripts')
<link href="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/assets/plugins/custom/uppy/uppy.bundle.css" rel="stylesheet" type="text/css" />
<script src="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/assets/plugins/custom/uppy/uppy.bundle.js"></script>
<script src="https://demo.softwarecompany.ae/ask_deema/metronic/backend_html/assets/js/pages/crud/file-upload/uppy.js"></script>
<script>
                                var listLeng = $('.mylist').length;
                                if (listLeng == '0') {
                                    addnewTitle();
                                }
                                function addnewTitle() {
                                    var uniqId = Math.floor((Math.random() * 1000000000) + 1);
                                    var html = '';
                                    html = html + '<div id="list-' + uniqId + '" uniqid="' + uniqId + '" class="mylist">';
                                    html = html + '    <div class="row">';
                                    html = html + '        <div class="col-sm-11">';
                                    html = html + '            <div class="wd100">';
                                    html = html + '                <input class="form-control" id="title-' + uniqId + '" placeholder="Title" value="" >';
                                    html = html + '                <br>';
                                    html = html + '            </div>';
                                    html = html + '        </div>';
                                    html = html + '        <div class="col-sm-1">';
                                    html = html + '            <a class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary" rmvid="list-' + uniqId + '" onclick="deletetitle(this);" title="Remove"><i class="fas fa-trash-alt"></i></a>';
                                    html = html + '        </div>';
                                    html = html + '    </div>';
                                    html = html + '    <div class="wd100">';
                                    html = html + '        <textarea class="form-control" id="description-' + uniqId + '" placeholder="Description" rows="7"></textarea>';
                                    html = html + '        <br>';
                                    html = html + '    </div>';
                                    html = html + '</div>';
                                    $('#listData').append(html);
                                }
                                function deletetitle(e) {
                                    var listLeng = $('.mylist').length;
                                    if (listLeng == '1') {
                                        alertSimple('at least one required');
                                        return false;
                                    }
                                    var rmvid = $(e).attr('rmvid');
                                    $('#' + rmvid).remove();
                                }
                                function submitMyform() {
                                    var form = new FormData();
                                    form.append('_token', $('meta[name="csrf-token"]').attr('content'));
                                    form.append('json[event_title]', $('#event_title').val());
                                    form.append('json[location]', $('#location').val());
                                    form.append('json[about_title]', $('#about_title').val());
                                    form.append('json[about]', $('#about').val());



                                    var fileInput = document.querySelector('#logo');
                                    form.append('logo', fileInput.files[0]);
                                    if ($('#event_title').val() == '') {
                                        $('#event_title').focus();
                                        $('#event_title').css('border-color', 'red');
                                        return false;
                                    }
                                    if ($('#location').val() == '') {
                                        $('#location').focus();
                                        $('#location').css('border-color', 'red');
                                        return false;
                                    }
                                    if ($('#about_title').val() == '') {
                                        $('#about_title').focus();
                                        $('#about_title').css('border-color', 'red');
                                        return false;
                                    }
                                    if ($('#about').val() == '') {
                                        $('#about').focus();
                                        $('#about').css('border-color', 'red');
                                        return false;
                                    }
                                    var isValid = true;
                                    $('.mylist').each(function (i) {
                                        var uniqid = $(this).attr('uniqid');
                                        var title = $('#title-' + uniqid);
                                        var description = $('#description-' + uniqid);
                                        if (title.val() == '') {
                                            title.focus();
                                            title.css('border-color', 'red');
                                            isValid = false;
                                        }
                                        if (description.val() == '') {
                                            description.focus();
                                            description.css('border-color', 'red');
                                            isValid = false;
                                        }
                                        form.append("json[list][" + i + "][title]", title.val());
                                        form.append("json[list][" + i + "][description]", description.val());
                                    });
                                    if (isValid == false) {
                                        return false;
                                    }
                                    var json = ajaxpost(form, "{{url('web-page/settings/'.$eventId)}}");
                                    try {
                                        var json = jQuery.parseJSON(json);
                                        if (json) {
                                            window.location = '';
                                        }
                                    } catch (e) {
                                        alert(e);
                                    }
                                }
                                function uploadImage() {
                                    var form = new FormData();

                                    form.append('_token', $('meta[name="csrf-token"]').attr('content'));
                                    form.append('uploadImage', 'data');

                                    var fileInput = document.querySelector('#uploadimg');
                                    form.append('uploadimg', fileInput.files[0]);

                                    var json = ajaxpost(form, "{{url('web-page/settings/'.$eventId)}}");
                                    try {
                                        var json = jQuery.parseJSON(json);
                                        if (json) {
                                            var html = '';
                                            html = html + '<div class="__codeThum" id="imag-' + json.pId + '">';
                                            html = html + '    <div class="__covrClose" imgId="' + json.pId + '" fullpath="' + json.fullpath + '" onclick="removeImage(this);">';
                                            html = html + '        <i class="fa fa-times-circle" aria-hidden="true"></i>';
                                            html = html + '    </div>';
                                            html = html + '    <img src="' + json.src + '"  >';
                                            html = html + '</div>';
                                            $('#imglistt').append(html);
                                        }
                                    } catch (e) {
                                        alert(e);
                                    }
                                }
                                function removeImage(e) {
                                    var form = new FormData();
                                    var imgId = $(e).attr('imgId');
                                    form.append('_token', $('meta[name="csrf-token"]').attr('content'));
                                    form.append('removeImage', imgId);
                                    form.append('fullpath', $(e).attr('fullpath'));

                                    var json = ajaxpost(form, "{{url('web-page/settings/'.$eventId)}}");
                                    try {
                                        var json = jQuery.parseJSON(json);
                                        if (json) {
                                            $('#imag-' + imgId).remove();
                                        }
                                    } catch (e) {
                                        alert(e);
                                    }
                                }
                                function copyToClipboard(element) {
                                    var $temp = $("<input>");
                                    $("body").append($temp);
                                    $temp.val($(element).text()).select();
                                    document.execCommand("copy");
                                    $temp.remove();
                                    alert('copied to clipboard');
                                }
</script>
@endsection
