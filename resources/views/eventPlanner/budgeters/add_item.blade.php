<!-- Modal-->
<div class="modal fade" id="addNewItemModel" role="model" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="new-item-form">
                    @csrf
                    <div class="wd100   __addscpWrkPg">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4 __ctndrofid">
                                <label class="form-label">Item Name</label>
                                <select class="form-control select2 item_name_select" name="item_name" id="item_name">
                                    @foreach ($categories as $item)
                                        <option></option>
                                        @php $budget = $item->budget($event->occasion_id); @endphp
                                        <option value="{{ $item['id'] }}" data-budget="@if($budget){{$budget->budget}}@endif" data-percentage="@if($budget){{$budget->budget_percentage}}@endif" data-total_budget="{{$event->occasion['total_budget']}}">{{ $item['name'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                <label class="form-label">Budget</label>
                                <input type="text" class="form-control numeric" placeholder="Amount" name="item_budget"
                                    id="item_budget">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                <label class="form-label">Actual Cost</label>
                                <input type="text" class="form-control numeric" placeholder="Amount"
                                    name="item_actual_cost" id="item_actual_cost">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                <label class="form-label">Variance</label>
                                <input type="text" class="form-control numeric" value="0"
                                    name="item_variance" id="item_variance" readonly>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                <label class="form-label">Actual Paid</label>
                                <input type="text" class="form-control numeric" value="0"
                                    name="item_actual_paid" id="item_actual_paid">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                <label class="form-label">Payment Status</label>
                                <select class="form-control" name="item_payment_status" id="item_payment_status">
                                    <option value="Pending">Pending</option>
                                    <option value="Partially Paid">Partially Paid</option>
                                    <option value="Paid">Paid</option>
                                </select>
                            </div>
                            {{-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                <label class="form-label">Budget Status</label>
                                <select class="form-control" name="item_budget_status" id="item_budget_status">
                                    <option value="On Budget">On Budget</option>
                                    <option value="Over Budget">Over Budget</option>
                                </select>
                            </div> --}}
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group mb-0 ">
                                <label class="form-label">Remarks</label>
                                <textarea class="form-control" id="item_remarks" rows="3" placeholder="Remarks"
                                    name="item_remarks"></textarea>
                            </div>
                            <input type="hidden" id="item_editable">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="button" id="btnAddItem" class="btn btn-primary font-weight-bold">Update Item</button>
            </div>
        </div>
    </div>
</div>
