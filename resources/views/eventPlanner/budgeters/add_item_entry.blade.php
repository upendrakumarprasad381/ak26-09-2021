<!-- Modal-->
<div class="modal fade" id="addNewItemEntryModel" role="model" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="new-item-form">
                    @csrf
                    <div class="wd100 __addscpWrkPg">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4 __ctndrofid">
                                <label class="form-label">Item Name</label>
                                <select class="form-control select2" name="item_name" id="item_name">
                                    @foreach ($categories as $item)
                                        <option></option>
                                        @php $budget = $item->budget($event->occasion_id); @endphp
                                        <option value="{{ $item['id'] }}" data-budget="@if($budget){{$budget->budget}}@endif" data-percentage="@if($budget){{$budget->budget_percentage}}@endif" data-total_budget="{{$event->final_budget}}">{{ $item['name'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                <label class="form-label">Askdeema Budget</label>
                                <input type="text" class="form-control numeric" placeholder="Amount" readonly name="item_budget"
                                    id="item_budget">
                            </div>
                            <input type="hidden" id="item_editable">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="button" id="btnAddItem" class="btn btn-primary font-weight-bold">Add Item</button>
            </div>
        </div>
    </div>
</div>
