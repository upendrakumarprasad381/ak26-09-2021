@extends('layout.default')
@section('sub-header')
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/event/' . $event->id . '') }}" class="text-muted">{{ $event->event_name }}</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/event/' . $event->id . '/budgeter/create') }}"
            class="text-muted">{{ $page_title }}</a>
    </li>
@endsection
@section('content')
    <div class="d-flex flex-column-fluid row">
        <div class="__budget wd100 mt-10 col-md-7">
            <div class="__budgeter wd100">
                <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Item Name</th>
                            <th scope="col" style="display: none;">#Category id</th>
                            <th scope="col">Budget</th>
                            <th scope="col">Askdeema Budget</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody id="budget_table_body">
                        @php
                            $count = 0;
                            $total = 0;
                            $ids = $event->categoriesModel->pluck('id')->toArray();
                            array_push($ids, 13);
                        @endphp
                        @foreach ($default_budget as $row)
                            @if (in_array($row['category_id'], $ids))
                                @php
                                    $count++;
                                    $total += $row['budget'];
                                @endphp
                                <tr>
                                    <th scope="col" class="td-index">{{ $count }}</th>
                                    <td>{{ $row['category'] }}</td>
                                    <td style="display: none;">{{ $row['category_id'] }}</td>
                                    <td width="33%"><input class="numeric form-control budget-amount-row" type="text"
                                            value="0"></td>
                                    <td><input class="form-control" type="text" value="{{ $row['budget'] }}" readonly>
                                    </td>
                                    <td class="text-center">
                                        {{-- <a class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary editItemBtn"
                                        title="Edit"><i class="far fa-edit"></i></a> --}}
                                        <a data-id="{{ $row['category_id'] }}"
                                            class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary autoupdate removeItemBtn"
                                            title="Remove"><i class="fas fa-trash-alt"></i></a>
                                    </td>
                                </tr>
                            @endif
                        @endforeach

                    </tbody>
                    <tfoot id="budget_table_footer">
                        <tr>
                            <th scope="col" class="td-index">{{ $count + 1 }}</th>
                            <td>Others</td>
                            <td style="display: none;"></td>
                            <td><input class="form-control numeric others-amount" type="text"
                                    value="{{ $event->final_budget }}" readonly></td>
                            <td><input class="form-control ad-others-amount" type="text"
                                    value="{{ $event->final_budget - $total }}" readonly></td>
                            <td class="text-center">
                                {{-- <a data-id=""
                                    class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary autoupdate removeItemBtn"
                                    title="Remove"><i class="fas fa-trash-alt"></i></a> --}}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="__budget wd100 mt-10 col-md-5">
            <div class="__bud-btn">
                <button class="btn btn-primary btn-outline-primary " type="button" data-toggle="modal"
                    data-target="#addNewItemEntryModel"> Add Item </button>
                <button class="btn btn-secondary" id="btnSetDefaultBudget" data-target="#defaultBudgetsModel">
                    AskDeema Budget </button>
                <button class="btn btn-primary" id="btnSubmit" type="button"> Proceed </button>
            </div>
        </div>
    </div>
    @include('eventPlanner.budgeters.default_budgets')
    @include('eventPlanner.budgeters.add_item_entry')
@endsection
@section('scripts')
    <script>
        var budget = [];
        budget = "{{ json_encode($default_budget) }}";

        console.log(budget);
        localStorage.setItem("budget", JSON.stringify([]));
        setIndex();
        $('#item_name').select2({
            tags: true,
            placeholder: "Select Item"
        });
        // $('.editItemBtn').on('click', function(e) {
        //     var row = $(this).closest("tr");
        //     $('#item_name').val(row.find("td:eq(1)").html()).trigger('change');
        //     $('#item_budget').val(row.find("td:eq(2)").html().replace('AED ', ''));
        //     $('#item_actual_cost').val(row.find("td:eq(3)").html().replace('AED ', ''));
        //     $('#item_variance').val(row.find("td:eq(4)").html().replace('AED ', ''));
        //     $('#item_payment_status').val(row.find("td:eq(5)").html());
        //     $('#item_budget_status').val(row.find("td:eq(6)").html());
        //     $('textarea#item_remarks').val(row.find("td:eq(7)").html());
        //     $('#addNewItemModel').modal('show');
        // });
        $("#btnSubmit").click(function(event) {
            event.preventDefault();
            var items = [];
            var total_budget = 0;
            $('#budget_table_body > tr').each(function() {
                var row = {
                    item_name: $(this).find('td').eq(0).text(),
                    item_id: isNaN(parseInt($(this).find('td').eq(1).text())) ? null : parseInt($(this)
                        .find('td').eq(1).text()),
                    budget: parseFloat($(this).find('td').eq(2).find('.numeric').val()),
                }
                total_budget += row.budget;
                items.push(row);
            });
            if (items.length > 0) {
                $("#btnSubmit").prop("disabled", true);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/event-planner/event/{{ $event->id }}/budgeter",
                    data: {
                        budget_items: items
                    },
                    success: function(data) {
                        $("#btnSubmit").prop("disabled", false);
                        setTimeout(function() {
                            location.replace(
                                '/event-planner/event/{{ $event->id }}/budgeter');
                        }, 1000);
                    },
                    error: function(e) {
                        $("#btnSubmit").prop("disabled", false);
                    }
                });
            } else {
                toastr.error('Add items to bidgets.', 'Error');
            }

        });

        $('.budget-amount-row').on('input', function() {
            setIndex();
        });

        function setIndex() {
            var total = 0;
            $(".td-index").each(function(index) {
                $(this).text(++index);
                var row = $(this).closest("tr");
                value = isNaN(parseFloat(row.find('.budget-amount-row').val())) ? 0 : parseFloat(row.find(
                    '.budget-amount-row').val());
                total += value;
                row.find('.removeItemBtn').data('id', index);
            });
            var total_budget = "{{ $event->final_budget }}";
            $('.others-amount').val(parseFloat(total_budget) - total);
        }
        $('.removeItemBtn').on('click', function(e) {
            var row = $(this).closest("tr");
            Swal.fire({
                title: "Are you sure?",
                text: "You wont be able to revert this!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function(result) {
                if (result.value) {
                    row.remove();
                    setIndex();
                } else if (result.dismiss === "cancel") {
                    Swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                    )
                }
            });

        });
        $('#item_name').on('change', function() {
            var total_budget = $(this).find(':selected').data('total_budget');
            var budget = $(this).find(':selected').data('budget');
            var percentage = $(this).find(':selected').data('percentage');

            $('#item_budget').val(((total_budget / 100) * percentage));
        });
        $('#item_actual_cost').on('input', function() {
            var cost = $(this).val();
            var budget = $('#item_budget').val();
            var variance = parseFloat(budget) - parseFloat(cost);
            if (variance >= 0) {
                $('#item_budget_status').val('On Budget').trigger('change');
            } else {
                $('#item_budget_status').val('Over Budget').trigger('change');
            }
            $('#item_variance').val(variance);
        });
        $('#btnAddItem').on('click', function() {
            if (jsFieldsValidator(['item_name'])) {
                var item_name = $('#item_name').find(':selected').text();
                var item_id = $('#item_name').find(':selected').val();
                var budget = $('#item_budget').val();
                if ($('#item_name').find(':selected').data('select2-tag')) {
                    item_id = null;
                }

                var row = `
                    <tr>
                        <th scope="col" class="td-index"></th>
                        <td>` + item_name + `</td>
                        <td style="display: none;">` + item_id + `</td>
                        <td width="33%"><input class="numeric form-control budget-amount-row" type="text" value="0"></td>
                        <td><input class="form-control" type="text" value="` + budget + `" readonly></td>
                        <td class="text-center">
                            <a data-id=""
                                class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary autoupdate removeItemBtn"
                                title="Remove"><i class="fas fa-trash-alt"></i></a>
                        </td>
                    </tr>
                `;
                $('#budget_table_body').append(row);
                $('.budget-amount-row').on('input', function() {
                    setIndex();
                });
                setIndex();
                $('#addNewItemEntryModel').modal('hide');
            }
            $('.removeItemBtn').on('click', function(e) {
                var row = $(this).closest("tr");
                Swal.fire({
                    title: "Are you sure?",
                    text: "You wont be able to revert this!",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                }).then(function(result) {
                    if (result.value) {
                        row.remove();
                        $('')
                        setIndex();
                    } else if (result.dismiss === "cancel") {
                        Swal.fire(
                            "Cancelled",
                            "Your imaginary file is safe :)",
                            "error"
                        )
                    }
                });

            });
        });
        $('#btnSetDefaultBudget').on('click', function() {
            var items = [];
            $('#budget_table_body > tr').each(function() {
                var row = {
                    item_id: isNaN(parseInt($(this).find('td').eq(1).text())) ? null : parseInt($(this)
                        .find('td').eq(1).text()),
                }
                items.push(row);
            });

            var default_budgets = "{{ json_encode($default_budget) }}";
            default_budgets = JSON.parse(default_budgets.replace(/&quot;/g, '"'));
            var final_budget = "{{ $event->final_budget }}";
            $('#budget_table_body').empty();
            $('#budget_table_footer').empty();
            var count = 1;
            var total_budget = 0;
            default_budgets.forEach(element => {
                var need = false;
                items.forEach(element_ => {
                    if (element_.item_id == element.category_id) {
                        need = true;
                    }
                });
                if (element.budget != 0 && need) {
                    var row = `
                    <tr>
                        <th scope="col" class="td-index"></th>
                        <td>` + element.category + `</td>
                        <td style="display: none;">` + element.category_id + `</td>
                        <td width="33%"><input class="numeric form-control budget-amount-row" type="text" value="` +
                        element.budget + `"></td>
                        <td><input class="form-control" type="text" value="` + element.budget + `" readonly></td>
                        <td class="text-center">
                            <a data-id=""
                                class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary autoupdate removeItemBtn"
                                title="Remove"><i class="fas fa-trash-alt"></i></a>
                        </td>
                    </tr>
                `;
                    $('#budget_table_body').append(row);
                    count++;
                    total_budget += element.budget;
                }
            });

            var row = `
                    <tr>
                        <th scope="col" class="td-index"></th>
                        <td>Others</td>
                        <td style="display: none;"></td>
                        <td width="33%"><input class="form-control numeric others-amount" type="text" value="` + (final_budget -
                total_budget) + `"></td>
                        <td><input class="form-control ad-others-amount" type="text" value="` + (final_budget -
                total_budget) + `" readonly></td>
                        <td class="text-center">

                        </td>
                    </tr>
                `;
            $('#budget_table_footer').append(row);
            setIndex();
            $('.removeItemBtn').on('click', function(e) {
                var row = $(this).closest("tr");

                Swal.fire({
                    title: "Are you sure?",
                    text: "You wont be able to revert this!",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                }).then(function(result) {
                    if (result.value) {
                        row.remove();
                        setIndex();
                    } else if (result.dismiss === "cancel") {
                        Swal.fire(
                            "Cancelled",
                            "Your imaginary file is safe :)",
                            "error"
                        )
                    }
                });

            });

            $('.budget-amount-row').on('input', function() {
                setIndex();
            });

            $('#defaultBudgetsModel').modal('hide');
            setIndex();
        });
    </script>
@endsection
