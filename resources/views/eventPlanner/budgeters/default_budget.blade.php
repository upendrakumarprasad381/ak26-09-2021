@extends('layout.default')
@section('content')

<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <div class="row ">
            <div class="__vtrTpBtnWrp d-flex wd100">
                <a href="{{ url('event-planner/event/' . $event->id . '/budgeters') }}" class="__vrttbtn @if($page_title == 'Ask Deema Budget') active @endif">Ask Deema Budget</a>
                <a href="{{ url('event-planner/event/' . $event->id . '/own-budgeters') }}" class="__vrttbtn @if($page_title == 'My Own Budget') active @endif">My Own Budget</a>
            </div>
        </div>
        <!--begin::Dashboard-->
        <div class="__budgeterWrp wd100">
            <div class="row __budgeterFlterWp">
                <div class="col-xl-12">
                    <!--begin::Stats Widget 16-->
                    <div href="#" class="card card-custom card-stretch">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="grid">
                                <section class="grid-one">
                                    <h2>Total Budget</h2>
                                    <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="120"
                                        height="120">
                                        <circle class="circle-chart__background" stroke="#efefef" stroke-width="1"
                                            fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                        <circle class="circle-chart__circle" stroke="#92B8C0" stroke-width="1"
                                            stroke-dasharray="100,100" stroke-linecap="round" fill="none"
                                            cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                        <g class="circle-chart__info">
                                            <text class="circle-chart__percent" x="16.91549431" y="15.5"
                                                alignment-baseline="central" text-anchor="middle"
                                                font-size="6">AED</text>
                                            <text class="circle-chart__subline" x="16.91549431" y="20.5"
                                                alignment-baseline="central" text-anchor="middle" font-size="5">{{
                                                $details['total_budget'] }}</text>
                                        </g>
                                    </svg>
                                </section>

                                <section class="grid-one">
                                    <h2>Actual Cost</h2>
                                    <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="120"
                                        height="120">
                                        <circle class="circle-chart__background" stroke="#efefef" stroke-width="1"
                                            fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                        <circle class="circle-chart__circle circle-chart__circle--cost" stroke="#7A5C66"
                                            stroke-width="1" stroke-dasharray="80,100" stroke-linecap="round"
                                            fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                        <g class="circle-chart__info">
                                            <text class="circle-chart__percent" x="16.91549431" y="15.5"
                                                alignment-baseline="central" text-anchor="middle"
                                                font-size="8">AED</text>
                                            <text class="circle-chart__subline" x="16.91549431" y="20.5"
                                                alignment-baseline="central" text-anchor="middle" font-size="2">{{
                                                $details['actual_cost'] }}</text>
                                        </g>
                                    </svg>
                                </section>
                                <section class="grid-one">
                                    <h2>Total Paid</h2>
                                    <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="120"
                                        height="120">
                                        <circle class="circle-chart__background" stroke="#efefef" stroke-width="1"
                                            fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                        <circle class="circle-chart__circle circle-chart__circle--cost" stroke="#7A5C66"
                                            stroke-width="1" stroke-dasharray="65,100" stroke-linecap="round"
                                            fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                        <g class="circle-chart__info">
                                            <text class="circle-chart__percent" x="16.91549431" y="15.5"
                                                alignment-baseline="central" text-anchor="middle"
                                                font-size="8">AED</text>
                                            <text class="circle-chart__subline" x="16.91549431" y="20.5"
                                                alignment-baseline="central" text-anchor="middle" font-size="2">{{
                                                $details['total_paid'] }}</text>
                                        </g>
                                    </svg>
                                </section>

                                <!-- grid two-->
                                <section class="grid-two">
                                    <h2>To Be Paid</h2>
                                    <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="120"
                                        height="120">
                                        <circle class="circle-chart__background" stroke="#efefef" stroke-width="1"
                                            fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                        <circle class="circle-chart__circle" stroke="#92B8C0" stroke-width="1"
                                            stroke-dasharray="100,100" stroke-linecap="round" fill="none"
                                            cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                        <g class="circle-chart__info">
                                            <text class="circle-chart__percent" x="16.91549431" y="15.5"
                                                alignment-baseline="central" text-anchor="middle"
                                                font-size="6">AED</text>
                                            <text class="circle-chart__subline" x="16.91549431" y="20.5"
                                                alignment-baseline="central" text-anchor="middle" font-size="5">{{
                                                $details['actual_cost'] - $details['total_paid'] }}</text>
                                        </g>
                                    </svg>
                                </section>

                                <section class="grid-two">
                                    <h2>Over Due</h2>
                                    <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="120"
                                        height="120">
                                        <circle class="circle-chart__background" stroke="#efefef" stroke-width="1"
                                            fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                        <circle class="circle-chart__circle circle-chart__circle--cost" stroke="#7A5C66"
                                            stroke-width="1" stroke-dasharray="65,100" stroke-linecap="round"
                                            fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                        <g class="circle-chart__info">
                                            <text class="circle-chart__percent" x="16.91549431" y="15.5"
                                                alignment-baseline="central" text-anchor="middle"
                                                font-size="8">AED</text>
                                            <text class="circle-chart__subline" x="16.91549431" y="20.5"
                                                alignment-baseline="central" text-anchor="middle" font-size="2">{{
                                                $details['total_budget'] - $details['actual_cost'] < 0 ?
                                                    ($details['total_budget'] - $details['actual_cost']) * -1 : 0 }}
                                                    </text>
                                        </g>
                                    </svg>
                                </section>
                                <!--// End Grid -->

                                <!--  <section class="grid-one">-->
                                <!--  <h2>Total Budget</h2>-->
                                <!--  <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="120" height="120">-->
                                <!--    <circle class="circle-chart__background" stroke="#efefef" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />-->
                                <!--    <circle class="circle-chart__circle" stroke="#92B8C0" stroke-width="1" stroke-dasharray="100,100" stroke-linecap="round" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />-->
                                <!--    <g class="circle-chart__info">-->
                                <!--      <text class="circle-chart__percent" x="16.91549431" y="15.5" alignment-baseline="central" text-anchor="middle" font-size="6">AED</text>-->
                                <!--      <text class="circle-chart__subline" x="16.91549431" y="20.5" alignment-baseline="central" text-anchor="middle" font-size="5">50,0000</text>-->
                                <!--    </g>-->
                                <!--  </svg>-->
                                <!--</section>-->

                                <section class="grid-one">
                                    <h2>Balance Amount</h2>
                                    <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="120"
                                        height="120">
                                        <circle class="circle-chart__background" stroke="#efefef" stroke-width="1"
                                            fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                        <circle class="circle-chart__circle circle-chart__circle--cost" stroke="#7A5C66"
                                            stroke-width="1" stroke-dasharray="80,100" stroke-linecap="round"
                                            fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                        <g class="circle-chart__info">
                                            <text class="circle-chart__percent" x="16.91549431" y="15.5"
                                                alignment-baseline="central" text-anchor="middle"
                                                font-size="8">AED</text>
                                            <text class="circle-chart__subline" x="16.91549431" y="20.5"
                                                alignment-baseline="central" text-anchor="middle" font-size="2">{{
                                                $details['total_budget'] - $details['actual_cost'] > 0 ?
                                                $details['total_budget'] - $details['actual_cost'] : 0 }}</text>
                                        </g>
                                    </svg>
                                </section>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 16-->
                </div>
            </div>
            <div class="wd100 __tpExpBtnWrp __ckLstFtre">
                <div class="d-flex">

                    <div class="__ppserch d-flex align-items-center py-3 py-sm-0 px-sm-3 ">

                        <input type="text" class="form-control border-0 font-weight-bold pl-2" placeholder="Search...">
                        <span class="svg-icon svg-icon-lg">

                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"></rect>
                                    <path
                                        d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                        fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                    <path
                                        d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                        fill="#000000" fill-rule="nonzero"></path>
                                </g>
                            </svg>

                        </span>
                    </div>


                    <div class="dropdown __topSwitchBtn __addItemDrop">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Sort&nbsp;By
                        </button>
                        <div class="dropdown-menu " aria-labelledby="dropdownMenuButton">
                            <a href="#" class="dropdown-item"> 1</a>
                            <a href="#" class="dropdown-item"> 1</a>
                            <a href="#" class="dropdown-item"> 54 </a>
                        </div>
                    </div>
                    <div class="dropdown __topSwitchBtn __addItemDrop">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Filter&nbsp;By
                        </button>
                        <div class="dropdown-menu " aria-labelledby="dropdownMenuButton">
                            <a href="#" class="dropdown-item"> 1</a>
                            <a href="#" class="dropdown-item"> 1</a>
                            <a href="#" class="dropdown-item"> 54 </a>
                        </div>
                    </div>




                </div>
            </div>


        </div>
        <!--end::Container-->


        <div class="__budget wd100 mt-10">


            <div class="__budgeter wd100 ">


                <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Item Name</th>
                            <th scope="col" style="display: none;">#Category id</th>
                            <th scope="col">Budget</th>
                            <th scope="col">Actual Cost</th>
                            <th scope="col">Variance</th>
                            <th scope="col">Payment Status</th>
                            <th scope="col">Budget Status</th>
                            <th scope="col">Remark</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($rows as $row)
                        <tr>
                            <th scope="col">{{ $loop->iteration }}</th>
                            <td>{{ $row->category['name'] }}</td>
                            <td style="display: none;">{{ $row->category['id'] }}</td>
                            <td>AED {{ $row['budget'] }}</td>
                            <td>AED {{ $row['actual_cost'] }}</td>
                            <td>AED {{ $row['variance'] }}</td>
                            <td>{{ $row['payment_status'] }}</td>
                            <td>{{ $row['budget_status'] }}</td>
                            <td>{{ $row['remarks'] }}</td>
                            <td class="text-center">
                                <a class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary editItemBtn"
                                    title="Edit"><i class="far fa-edit"></i></a>
                                <a data-id="{{ $row->id }}"
                                    class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary autoupdate removeItemBtn"
                                    title="Remove"><i class="fas fa-trash-alt"></i></a>
                                {{-- <a
                                    class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary autoupdate"
                                    title="Remove"><i class="fas fa-eye"></i></a> --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="__bud-btn">
                    <button class="btn btn-primary btn-outline-primary " type="button" data-toggle="modal"
                        data-target="#addNewItemModel"> Add Item </button>
                </div>
            </div>
        </div>
    </div>
</div>
@include('eventPlanner.budgeters.add_item')
@endsection
@section('scripts')
<script>
    $('#item_name').select2({
        placeholder: "Select a Item",
    });

    $('.editItemBtn').on('click', function (e) {
        var row = $(this).closest("tr");
        $('#item_name').val(row.find("td:eq(1)").html()).trigger('change');
        $('#item_budget').val(row.find("td:eq(2)").html().replace('AED ', ''));
        $('#item_actual_cost').val(row.find("td:eq(3)").html().replace('AED ', ''));
        $('#item_variance').val(row.find("td:eq(4)").html().replace('AED ', ''));
        $('#item_payment_status').val(row.find("td:eq(5)").html());
        $('#item_budget_status').val(row.find("td:eq(6)").html());
        $('textarea#item_remarks').val(row.find("td:eq(7)").html());
        $('#addNewItemModel').modal('show');
    });
    $('.removeItemBtn').on('click', function (e) {
        var id = $(this).data('id');

        Swal.fire({
            title: "Are you sure?",
            text: "You wont be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "DELETE",
                    enctype: 'multipart/form-data',
                    url: "/event-planner/event/{{ $event->id }}/budgeters/" + id,
                    data: {
                        'id': id,
                    },
                    success: function (data) {
                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    },
                    error: function (e) {

                    }
                });
            } else if (result.dismiss === "cancel") {
                Swal.fire(
                    "Cancelled",
                    "Your imaginary file is safe :)",
                    "error"
                )
            }
        });

    });
    $('#item_name').on('change', function () {
        var total_budget = $(this).find(':selected').data('total_budget');
        var budget = $(this).find(':selected').data('budget');
        var percentage = $(this).find(':selected').data('percentage');

        $('#item_budget').val(budget);
    });
    $('#item_actual_cost').on('input', function () {
        var cost = $(this).val();
        var budget = $('#item_budget').val();
        var variance = parseFloat(budget) - parseFloat(cost);
        if (variance >= 0) {
            $('#item_budget_status').val('On Budget').trigger('change');
        } else {
            $('#item_budget_status').val('Over Budget').trigger('change');
        }
        $('#item_variance').val(variance);
    });
    $('#btnAddItem').on('click', function () {
        if (jsFieldsValidator(['item_name'])) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/event-planner/event/{{ $event->id }}/budgeters",
                data: {
                    'item_name': $('#item_name').val(),
                    'item_budget': $('#item_budget').val(),
                    'item_actual_cost': $('#item_actual_cost').val(),
                    'item_variance': $('#item_variance').val(),
                    'item_payment_status': $('#item_payment_status').val(),
                    'item_budget_status': $('#item_budget_status').val(),
                    'item_remarks': $('#item_remarks').val(),
                    'type': 0,
                },
                success: function (data) {
                    $('#addNewItemModel').modal('hide');
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                },
                error: function (e) {

                }
            });
        }
    });
</script>
@endsection
