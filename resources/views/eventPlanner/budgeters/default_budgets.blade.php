<!-- Modal-->
<div class="modal fade" id="defaultBudgetsModel" role="model" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Askdeema Budgets</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="__budget wd100 mt-10">
                    <div class="__budgeter wd100 ">
                        <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Item Name</th>
                                    <th scope="col" style="display: none;">#Category id</th>
                                    <th scope="col">Budget</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($default_budget as $row)
                                    <tr>
                                        <th scope="col">{{ $loop->iteration }}</th>
                                        <td>{{ $row['category'] }}</td>
                                        <td style="display: none;">{{ $row['category_id'] }}</td>
                                        <td>{{$row['budget']}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="button" id="btnSetDefaultBudget" class="btn btn-primary font-weight-bold">Set Default Budget</button>
            </div>
        </div>
    </div>
</div>
