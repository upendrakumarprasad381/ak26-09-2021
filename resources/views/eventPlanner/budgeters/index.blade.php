@extends('layout.default')
@section('content')

    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container-fluid">
            <!--begin::Dashboard-->
            <div class="__budgeterWrp wd100">
                <div class="row __budgeterFlterWp">
                    <div class="col-xl-12">
                        <!--begin::Stats Widget 16-->
                        <div href="#" class="card card-custom card-stretch">
                            <!--begin::Body-->
                            <div class="card-body">
                                <div class="grid">
                                    <section class="grid-one">
                                        <h2>Total Budget</h2>
                                        <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="120"
                                            height="120">
                                            <circle class="circle-chart__background" stroke="#efefef" stroke-width="1"
                                                fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                            <circle class="circle-chart__circle" stroke="#92B8C0" stroke-width="1"
                                                stroke-dasharray="100,100" stroke-linecap="round" fill="none"
                                                cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                            <g class="circle-chart__info">
                                                <text class="circle-chart__percent" x="16.91549431" y="15.5"
                                                    alignment-baseline="central" text-anchor="middle"
                                                    font-size="6">AED</text>
                                                <text class="circle-chart__subline" x="16.91549431" y="20.5"
                                                    alignment-baseline="central" text-anchor="middle"
                                                    font-size="5">{{ $details['total_budget'] }}</text>
                                            </g>
                                        </svg>
                                    </section>

                                    <section class="grid-one">
                                        <h2>Actual Cost</h2>
                                        <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="120"
                                            height="120">
                                            <circle class="circle-chart__background" stroke="#efefef" stroke-width="1"
                                                fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                            <circle class="circle-chart__circle circle-chart__circle--cost" stroke="#7A5C66"
                                                stroke-width="1" stroke-dasharray="80,100" stroke-linecap="round"
                                                fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                            <g class="circle-chart__info">
                                                <text class="circle-chart__percent" x="16.91549431" y="15.5"
                                                    alignment-baseline="central" text-anchor="middle"
                                                    font-size="8">AED</text>
                                                <text class="circle-chart__subline" x="16.91549431" y="20.5"
                                                    alignment-baseline="central" text-anchor="middle"
                                                    font-size="2">{{ $details['actual_cost'] }}</text>
                                            </g>
                                        </svg>
                                    </section>
                                    <section class="grid-one">
                                        <h2>Total Paid</h2>
                                        <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="120"
                                            height="120">
                                            <circle class="circle-chart__background" stroke="#efefef" stroke-width="1"
                                                fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                            <circle class="circle-chart__circle circle-chart__circle--cost" stroke="#7A5C66"
                                                stroke-width="1" stroke-dasharray="65,100" stroke-linecap="round"
                                                fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                            <g class="circle-chart__info">
                                                <text class="circle-chart__percent" x="16.91549431" y="15.5"
                                                    alignment-baseline="central" text-anchor="middle"
                                                    font-size="8">AED</text>
                                                <text class="circle-chart__subline" x="16.91549431" y="20.5"
                                                    alignment-baseline="central" text-anchor="middle"
                                                    font-size="2">{{ $details['total_paid'] }}</text>
                                            </g>
                                        </svg>
                                    </section>

                                    <!-- grid two-->
                                    <section class="grid-two">
                                        <h2>To Be Paid</h2>
                                        <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="120"
                                            height="120">
                                            <circle class="circle-chart__background" stroke="#efefef" stroke-width="1"
                                                fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                            <circle class="circle-chart__circle" stroke="#92B8C0" stroke-width="1"
                                                stroke-dasharray="100,100" stroke-linecap="round" fill="none"
                                                cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                            <g class="circle-chart__info">
                                                <text class="circle-chart__percent" x="16.91549431" y="15.5"
                                                    alignment-baseline="central" text-anchor="middle"
                                                    font-size="6">AED</text>
                                                <text class="circle-chart__subline" x="16.91549431" y="20.5"
                                                    alignment-baseline="central" text-anchor="middle"
                                                    font-size="5">{{ $details['actual_cost'] - $details['total_paid'] }}</text>
                                            </g>
                                        </svg>
                                    </section>

                                    <section class="grid-two">
                                        <h2>Over Due</h2>
                                        <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="120"
                                            height="120">
                                            <circle class="circle-chart__background" stroke="#efefef" stroke-width="1"
                                                fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                            <circle class="circle-chart__circle circle-chart__circle--cost" stroke="#7A5C66"
                                                stroke-width="1" stroke-dasharray="65,100" stroke-linecap="round"
                                                fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                            <g class="circle-chart__info">
                                                <text class="circle-chart__percent" x="16.91549431" y="15.5"
                                                    alignment-baseline="central" text-anchor="middle"
                                                    font-size="8">AED</text>
                                                <text class="circle-chart__subline" x="16.91549431" y="20.5"
                                                    alignment-baseline="central" text-anchor="middle"
                                                    font-size="2">{{ $details['total_budget'] - $details['actual_cost'] < 0 ? ($details['total_budget'] - $details['actual_cost']) * -1 : 0 }}
                                                </text>
                                            </g>
                                        </svg>
                                    </section>
                                    <!--// End Grid -->

                                    <!--  <section class="grid-one">-->
                                    <!--  <h2>Total Budget</h2>-->
                                    <!--  <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="120" height="120">-->
                                    <!--    <circle class="circle-chart__background" stroke="#efefef" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />-->
                                    <!--    <circle class="circle-chart__circle" stroke="#92B8C0" stroke-width="1" stroke-dasharray="100,100" stroke-linecap="round" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />-->
                                    <!--    <g class="circle-chart__info">-->
                                    <!--      <text class="circle-chart__percent" x="16.91549431" y="15.5" alignment-baseline="central" text-anchor="middle" font-size="6">AED</text>-->
                                    <!--      <text class="circle-chart__subline" x="16.91549431" y="20.5" alignment-baseline="central" text-anchor="middle" font-size="5">50,0000</text>-->
                                    <!--    </g>-->
                                    <!--  </svg>-->
                                    <!--</section>-->

                                    <section class="grid-one">
                                        <h2>Balance Amount</h2>
                                        <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="120"
                                            height="120">
                                            <circle class="circle-chart__background" stroke="#efefef" stroke-width="1"
                                                fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                            <circle class="circle-chart__circle circle-chart__circle--cost" stroke="#7A5C66"
                                                stroke-width="1" stroke-dasharray="80,100" stroke-linecap="round"
                                                fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                                            <g class="circle-chart__info">
                                                <text class="circle-chart__percent" x="16.91549431" y="15.5"
                                                    alignment-baseline="central" text-anchor="middle"
                                                    font-size="8">AED</text>
                                                <text class="circle-chart__subline" x="16.91549431" y="20.5"
                                                    alignment-baseline="central" text-anchor="middle"
                                                    font-size="2">{{ $details['total_budget'] - $details['actual_cost'] > 0 ? $details['total_budget'] - $details['actual_cost'] : 0 }}</text>
                                            </g>
                                        </svg>
                                    </section>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 16-->
                    </div>
                </div>
                <div class="wd100 __tpExpBtnWrp __ckLstFtre">
                    <div class="d-flex">

                        {{-- <div class="__ppserch d-flex align-items-center py-3 py-sm-0 px-sm-3 ">

                            <input type="text" class="form-control border-0 font-weight-bold pl-2" placeholder="Search...">
                            <span class="svg-icon svg-icon-lg">

                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"></rect>
                                        <path
                                            d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                            fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                        <path
                                            d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                            fill="#000000" fill-rule="nonzero"></path>
                                    </g>
                                </svg>

                            </span>
                        </div>


                        <div class="dropdown __topSwitchBtn __addItemDrop">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Sort&nbsp;By
                            </button>
                            <div class="dropdown-menu " aria-labelledby="dropdownMenuButton">
                                <a href="#" class="dropdown-item"> 1</a>
                                <a href="#" class="dropdown-item"> 1</a>
                                <a href="#" class="dropdown-item"> 54 </a>
                            </div>
                        </div>
                        <div class="dropdown __topSwitchBtn __addItemDrop">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Filter&nbsp;By
                            </button>
                            <div class="dropdown-menu " aria-labelledby="dropdownMenuButton">
                                <a href="#" class="dropdown-item"> 1</a>
                                <a href="#" class="dropdown-item"> 1</a>
                                <a href="#" class="dropdown-item"> 54 </a>
                            </div>
                        </div> --}}




                    </div>
                </div>


            </div>
            <!--end::Container-->
            <div class="wd100 __bgtTabtfrWrap">

                <div class="card card-custom __enrtTabwgt  __bgtTabtfr">
                    <div class="card-header">

                        <div class="card-toolbar wd100">
                            {{-- <ul class="nav nav-bold nav-pills">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_7_1">Wedding </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_7_2">DJ</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_7_3">Birthday</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_7_3">Birthday</a>
                                </li>

                            </ul> --}}
                        </div>
                        <div class="__crneBtn"><a href="/{{$url}}/event/{{$event->id}}/budgeter/create"><button type="button" class="btn btn-sm btn-outline-primary">Create&nbsp;New </button></a></div>
                    </div>

                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="kt_tab_pane_7_1" role="tabpanel"
                                aria-labelledby="kt_tab_pane_7_1">
                                <div class="wd100  __bgtTabtfrListWrp">
                                    <div class="row">
                                        @foreach ($budgets as $budget)
                                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                                                <div class="__grvwBgttm">
                                                    <div class="__grvwBgttmImg">
                                                        @if ($budget->is_default == 1)
                                                            <div class="pointer">Default </div>
                                                        @endif
                                                        <a href="#"><img class="img-fluid"
                                                                @if ($budget->event['image'] != null) src="{{ asset('storage/' . $budget->event['image'] . '') }}" @else src="{{ asset('storage/' . $budget->event->occasion['image'] . '') }}" @endif /></a>
                                                    </div>
                                                    <h5> <a href="#">{{$budget->event['event_name']}}</a></h5>
                                                    <div class="__wgwtTotLab">Total Budget : {{$event->final_budget}}</div>
                                                    <div class="__wgwtTotLab">Total Used : {{$budget->items->sum('budget')}}</div>
                                                    <a href="/{{$url}}/event/{{$event->id}}/budgeter/{{$budget->id}}"><button type="button"
                                                        class="btn btn-sm btn-outline-primary">View</button></a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>

    </script>
@endsection
