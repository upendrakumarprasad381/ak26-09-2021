<!DOCTYPE html>
<html>

<head>
    <title>{{ $request->contract_name }}</title>
</head>

<body>
    <div class="d-flex flex-column-fluid">
        <div class="container-fluid">
            <div class="__invoicWrp wd100">
                <div class="__viewIncWoz" style="width:720px; margin: 0 auto;">
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"
                        style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
                        <tr id="pdf_header">
                            <td width="60%" align="left" valign="middle">
                                <img src="storage/{{ $party1->bussinessDetails['logo'] }}" width="120px" />
                            </td>
                            <td width="40%" align="left" valign="middle" style="font-size:14px; line-height:25px;">
                                <h3 style="font-size:18px; margin:0px 0px; padding:0 0;">
                                    {{ $party1->first_name }} {{ $party1->last_name }}</h3>
                                {{ $party1->bussinessDetails['address'] }}<br>
                                Contract Name &nbsp;&nbsp; : {{ $request->contract_name }} <br>
                                Date : {{ $request->date }}
                            </td>
                        </tr>
                    </table>

                    <table width="100%" border="0" cellspacing="0" cellpadding="10" style="margin-top:20px;border:#8A8A8A 1px solid;
                font-family:Arial, Helvetica, sans-serif;
                font-size:13px;
                ">
                        <tr>
                            <th width="36%" align="left" valign="middle"
                                style="color:#7A5C66; border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                Events Details</th>
                            <th width="30%" align="left" valign="middle"
                                style="color:#7A5C66; border-bottom:#8A8A8A 1px solid;  border-right:#8A8A8A 1px solid;">
                                Vendor Details</th>
                            <th width="33%" align="left" valign="middle"
                                style="color:#7A5C66; border-bottom:#8A8A8A 1px solid; ">Customer Details</th>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style=" border-right:#8A8A8A 1px solid;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="4" style="font-family:Arial, Helvetica, sans-serif;
                font-size:13px;">
                                    <tr>
                                        <td width="35%"><strong>Event Name</strong></td>
                                        <td width="5%" style="color:#000">:</td>
                                        <td width="60%" style="color:#8A8A8A"> {{ $event->event_name }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Event Date </strong></td>
                                        <td style="color:#000">:</td>
                                        <td style="color:#8A8A8A">{{ $event->event_date }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Guest </strong></td>
                                        <td style="color:#000">:</td>
                                        <td style="color:#8A8A8A">{{ $event->number_of_attendees }}</td>
                                    </tr>
                                </table>
                            </td>

                            <td align="left" valign="top" style=" border-right:#8A8A8A 1px solid;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="4" style="font-family:Arial, Helvetica, sans-serif;
                font-size:13px;">
                                    <tr>
                                        <td><strong>Name</strong></td>
                                        <td style="color:#000">:</td>
                                        <td style="color:#8A8A8A">{{ $party1['first_name'] }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Address </strong></td>
                                        <td style="color:#000">:</td>
                                        <td style="color:#8A8A8A">{{ $party1['email'] }}
                                        </td>
                                    </tr>
                                </table>

                            </td>
                            <td align="left" valign="top">
                                <table width="100%" border="0" cellspacing="0" cellpadding="4" style="font-family:Arial, Helvetica, sans-serif;
                font-size:13px;">
                                    <tr>
                                        <td><strong>Name</strong></td>
                                        <td style="color:#000">:</td>
                                        <td style="color:#8A8A8A">@if($event->customer) {{ $event->customer['first_name'] }} @endif</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Address </strong></td>
                                        <td style="color:#000">:</td>
                                        <td style="color:#8A8A8A">@if($event->customer) {{ $event->customer['email'] }} @endif</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br /><br />


                    <h5
                        style="color:#7A5C66; margin:0 0 10px; font-size:16px; font-family:Arial, Helvetica, sans-serif; ">
                        Scope of work</h5>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5"
                        style=" border:#8A8A8A 1px solid; border-bottom:none;  font-family:Arial, Helvetica, sans-serif;  font-size:13px;  ">
                        <tr>
                            <th width="6%" align="center" valign="middle"
                                style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">Sl.No</th>
                            <th width="9%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                Item Name</th>
                            <th width="9%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                Service
                                Type</th>
                            <th width="26%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                Description</th>
                            <th width="8%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                Quantity</th>
                            <th width="8%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                Amount
                            </th>
                            <th width="8%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">Tax
                            </th>
                            <th width="8%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                Remark
                            </th>
                            <th width="11%" style=" border-bottom:#8A8A8A 1px solid; ">Total</th>
                        </tr>
                        @php
                            $sub_total = 0;
                            $tax_total = 0;
                        @endphp
                        @foreach ($items as $key => $item)
                            @php
                                $total = $item->amount * $item->quantity;
                                $tax_amount = ($total / 100) * $item->tax;

                                $sub_total += $total;
                                $tax_total += $tax_amount;
                            @endphp
                            <tr>
                                <td align="center" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $key + 1 }}</td>
                                <td align="center" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item->event_name }}</td>
                                <td align="center" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ \App\Models\Admin\Master\Category::find($item->service_type)->name }}</td>
                                <td align="left" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item->description }}</td>
                                <td align="center" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item->quantity }}</td>
                                <td align="right" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item->amount }}</td>
                                <td align="center" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item->tax }} %</td>
                                <td align="left" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item->remarks }}</td>
                                <td align="right" valign="middle" style=" border-bottom:#8A8A8A 1px solid;">AED
                                    {{ $tax_amount + $total }}</td>
                            </tr>
                        @endforeach

                        <tr>
                            <td colspan="8" align="right" valign="middle"
                                style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;"><strong>Sub
                                    Total</strong></td>
                            <td align="right" valign="middle" style=" border-bottom:#8A8A8A 1px solid;  "><strong>AED
                                    {{ $sub_total }}</strong></td>
                        </tr>
                        <tr>
                            <td colspan="8" align="right" valign="middle"
                                style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;"><strong>Tax
                                    Total</strong></td>
                            <td align="right" valign="middle" style=" border-bottom:#8A8A8A 1px solid;  "><strong>AED
                                    {{ $tax_total }}</strong>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="8" align="right" valign="middle"
                                style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;"><strong>Total
                                </strong></td>
                            <td align="right" valign="middle" style=" border-bottom:#8A8A8A 1px solid;  "><strong>AED
                                    {{ $tax_total + $sub_total }}</strong>
                            </td>
                        </tr>
                    </table>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif;
                font-size:12px;
                margin-top:50px;
                border-top: #8A8A8A 1px solid;
                color:#000">
                        <tr>
                            <td style="padding-top:15px;"><strong>Terms Of Agreement:</strong>
                                <ol style="    margin: 10px 14px;  padding: 0; ">
                                    @foreach ($terms as $term)
                                        <li>
                                            {{ $term->text }}
                                        </li>
                                    @endforeach

                                </ol>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"
                        style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
                        <tr>
                            <td width="40%" align="left" valign="middle">
                            </td>
                            <td width="30%" align="left" valign="middle" style="font-size:14px; line-height:25px;">
                                {{ $party1->first_name }} {{ $party1->last_name }} <br> Signature
                            </td>
                            <td width="30%" align="left" valign="middle" style="font-size:14px; line-height:25px;">
                                {{ $party2->first_name }} {{ $party2->last_name }} <br> Signature
                            </td>
                        </tr>
                        <tr>
                            <td width="40%" align="left" valign="middle">
                            </td>
                            <td width="30%" align="left" valign="middle" style="font-size:14px; line-height:25px;">
                                @isset($contract)
                                    @if($contract->signature_1_type == 'Digital' && $contract->signature_1)
                                    <img src="storage/{{ $contract->signature_1 }}" width="150px"/>
                                    @endif
                                @endisset
                            </td>
                            <td width="30%" align="left" valign="middle" style="font-size:14px; line-height:25px;">
                                @isset($contract)
                                @if($contract->signature_2_type == 'Digital' && $contract->signature_2)
                                <img src="storage/{{ $contract->signature_2 }}" width="120px"/>
                                @endif
                            @endisset
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--end::Container-->
        </div>
    </div>
</body>

</html>
