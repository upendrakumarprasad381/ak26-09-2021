{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/leads/') }}" class="text-muted">{{$page_title}}</a>
    </li>
@endsection
{{-- Content --}}
@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container-fluid">
		<!--begin::Dashboard-->
	<div class="row __mainblk">
		<div class="col-xl-3">
			<a href="{{route('event-planner.leads')}}" class="card card-custom card-stretch __active">
				<div class="card-body">
 					<div class="__textLebs font-weight-bolder font-size-h3">Received Leads</div>
				</div>
			</a>
		</div>
	<div class="col-xl-3">
			<a href="{{route('event-planner.leads')}}" class="card card-custom card-stretch">
				<div class="card-body">
 					<div class="__textLebs font-weight-bolder font-size-h3">Look For Leads</div>
				</div>
			</a>
		</div>
	<div class="col-xl-3">
			<a href="{{route('event-planner.leads')}}" class="card card-custom card-stretch">
				<div class="card-body">
 					<div class="__textLebs font-weight-bolder font-size-h3">Suggest Leads</div>
				</div>
			</a>
		</div>
	</div>
    <div class="row __sublk">
        <div class="col-xl-3">
            <!--begin::Stats Widget 16-->
            <a href="{{route('event-planner.recieved-leads')}}" class="card card-custom card-stretch  @if($page_title == 'Received Leads') __active @endif">
                <!--begin::Body-->
                <div class="card-body">
                    <div class="font-weight-bold ">
                        <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{$leads_counts['received']}}</span>
                    </div>

                    <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Received Leads</div>

                </div>
                <!--end::Body-->
            </a>
            <!--end::Stats Widget 16-->
        </div>

        <div class="col-xl-3">
            <!--begin::Stats Widget 16-->
            <a href="{{route('event-planner.recieved-leads')}}" class="card card-custom card-stretch @if($page_title == 'Opened Leads') __active @endif">
                <!--begin::Body-->
                <div class="card-body">
                    <div class="font-weight-bold ">
                        <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{$leads_counts['opened']}}</span>
                    </div>

                    <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Opened Leads</div>

                </div>
                <!--end::Body-->
            </a>
            <!--end::Stats Widget 16-->
        </div>

        <div class="col-xl-3">
            <!--begin::Stats Widget 16-->
            <a href="{{route('event-planner.ongoing-leads')}}" class="card card-custom card-stretch @if($page_title == 'Ongoing Leads') __active @endif">
                <!--begin::Body-->
                <div class="card-body">
                    <div class="font-weight-bold ">
                        <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{$leads_counts['ongoing']}}</span>
                    </div>

                    <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Ongoing Leads</div>

                </div>
                <!--end::Body-->
            </a>
            <!--end::Stats Widget 16-->
        </div>

        <div class="col-xl-3">
            <!--begin::Stats Widget 16-->
            <a href="{{route('event-planner.declined-leads')}}" class="card card-custom card-stretch @if($page_title == 'Declined Leads') __active @endif">
                <!--begin::Body-->
                <div class="card-body">
                    <div class="font-weight-bold ">
                        <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{$leads_counts['declined']}}</span>
                    </div>

                    <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Declined Leads</div>

                </div>
                <!--end::Body-->
            </a>
            <!--end::Stats Widget 16-->
        </div>
    </div>
		<!--end::Dashboard-->
	</div>
	<!--end::Container-->
</div>
<!--end::Entry-->
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
@endsection
