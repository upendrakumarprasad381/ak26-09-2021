{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/my-events') }}" class="text-muted">{{ $page_title }}</a>
    </li>
@endsection
{{-- Content --}}
@section('content')
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container-fluid">
            <!--begin::Dashboard-->
            <div class="row __cmFltTp __lddelbk ">
                <div class="col">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 mb-3">
                            <label class="form-label">Occasions</label>
                            <select class="form-control" id="filter_occasion" name="filter_occasion">
                                <option value>All</option>
                                @foreach ($occasions as $occasion)
                                    <option value="{{ $occasion['id'] }}" @if ($request->occasion) @if ($request->occasion == $occasion->id) selected @endif @endif>
                                        {{ $occasion['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 mb-3">
                            <label class="form-label">Event Type</label>
                            <select class="form-control" id="filter_type" name="filter_type">
                                <option value>All</option>
                                <option value="Indoor" @if ($request->type) @if ($request->type == 'Indoor') selected @endif @endif>In Door</option>
                                <option value="Outdoor" @if ($request->type) @if ($request->type == 'Outdoor') selected @endif @endif>Out Door</option>
                            </select>
                        </div>
                        {{-- <div class="col-lg-4 col-md-4 col-sm-12 mb-3">
                            <label class="form-label">Status</label>
                            <select class="form-control" id="exampleSelect1">
                                <option>Select your status</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div> --}}
                    </div>
                </div>

                <div class="col-md-auto">
                    <label class="form-label wd100 __cmoemynone">&nbsp; </label>
                    {{-- <button type="button" class="btn btn-primary">Advance Option</button> --}}
                    {{-- <button type="button" class="btn btn-secondary">Sort By</button> --}}
                    <div class="__droStrwrap accordion accordion-light accordion-toggle-arrow mt-7" id="accordionExample2">
                        <div class="card">
                            <div class="card-header" id="headingOne2">
                                <div class="card-title" data-toggle="collapse" data-target="#collapseOne2"
                                    style="width: 100%;">
                                    Advanced&nbsp;Search
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>


            <div class="wd100 __advFltrRut ">

                <div id="collapseOne2" class="collapse __droStr wd100 mb-5 @if ($request->daterange || $request->attendees) collapse show @endif"
                    data-parent="#accordionExample2">

                    <div class="wd100 ">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 mb-5">
                                <label class="form-label">Date</label>
                                <div class='input-group' id='kt_daterangepicker_6'>
                                    <input type='text' class="form-control" readonly="readonly"
                                        placeholder="Select date range" id="filter_date" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="col-lg-4 col-md-4 col-sm-12 mb-5">
                                <label class="form-label">Venue</label>
                                <select class="form-control" id="exampleSelect1">
                                    <option>Select your location</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div> --}}
                            <div class="col-lg-4 col-md-4 col-sm-12 mb-5">
                                <label class="form-label">Number of Attendees</label>
                                <select class="form-control" id="filter_attendees">
                                    <option value>All</option>
                                    <option value="0,100" @isset($request->attendees) @if ($request->attendees == '0,100') selected @endif @endisset>0 -
                                        100</option>
                                    <option value="101,500" @isset($request->attendees) @if ($request->attendees == '101,500') selected @endif
                                        @endisset>101 - 500</option>
                                    <option value="501,1000" @isset($request->attendees) @if ($request->attendees == '501,1000') selected @endif
                                        @endisset>501 - 1000</option>
                                    <option value="1001,0" @isset($request->attendees) @if ($request->attendees == '1001,0') selected @endif
                                        @endisset>1000+</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <!--begin::Dashboard-->
            <div class="row __sublk">
                <div class="col-xl-3">
                    <!--begin::Stats Widget 16-->
                    <a href="" class="card card-custom card-stretch">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span
                                    class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{ $event_counts['total'] }}</span>
                            </div>

                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Total Events</div>

                        </div>
                        <!--end::Body-->
                    </a>
                    <!--end::Stats Widget 16-->
                </div>

                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                    <!--begin::Stats Widget 16-->
                    <a href="" class="card card-custom card-stretch">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span
                                    class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{ $event_counts['active'] }}</span>
                            </div>

                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Active Events</div>

                        </div>
                        <!--end::Body-->
                    </a>
                    <!--end::Stats Widget 16-->
                </div>

                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                    <!--begin::Stats Widget 16-->
                    <a href="" class="card card-custom card-stretch">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span
                                    class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{ $event_counts['proposed'] }}</span>
                            </div>

                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Proposed Events</div>

                        </div>
                        <!--end::Body-->
                    </a>
                    <!--end::Stats Widget 16-->
                </div>

                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                    <!--begin::Stats Widget 16-->
                    <a href="" class="card card-custom card-stretch">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span
                                    class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{ $event_counts['archived'] }}</span>
                            </div>

                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Archived Events</div>

                        </div>
                        <!--end::Body-->
                    </a>
                    <!--end::Stats Widget 16-->
                </div>
            </div>
            <!--end::Dashboard-->

            <div class="row __ledBlkWrp mb-5">
                @foreach ($events as $row)
                    <div class="col-lg-3 col-md-6 col-sm-12 __ledBlk">
                        <div class="wd100 __ledBlkInr">
                            <div class="__ledBlkImg">
                                <a href="{{ route('' . $url . '.event', $row->id) }}"><img class="img-fluid"
                                        @if ($row['image'] != null) src="{{ asset('storage/' . $row['image'] . '') }}" @else src="{{ asset('storage/' . $row->occasion['image'] . '') }}" @endif></a>
                            </div>
                            <div class="__ledBlkDicp wd100">
                                <div class="__noticotr">
                                    <div class="__yolboll" data-toggle="tooltip" data-theme="dark" title="Proposals">
                                        {{ $row->counts()['proposals'] }}</div>
                                    <div class="__reddboll" data-toggle="tooltip" data-theme="dark" title="Invoices">
                                        {{ $row->counts()['invoices'] }}</div>
                                </div>
                                <h4 class="text-truncate"><a
                                        href="{{ route('' . $url . '.event', $row->id) }}">{{ $row['event_name'] }}</a>
                                </h4>
                                <h5><a
                                        href="{{ route('' . $url . '.event', $row->id) }}">{{ date('d M Y', strtotime($row['event_date'])) }}</a>
                                </h5>
                                <h6 class="text-truncate"><a
                                        href="{{ route('' . $url . '.event', $row->id) }}">{{ $row['event_location'] }}</a>
                                </h6>
                            </div>
                            <div class="__ledBlkBtn wd100">
                                <a href="javascript:void(0);" class="__ldBBtn eventRemoveBtn" data-id="{{$row->id}}">
                                    {{-- <i class="fa fa-trash-o" aria-hidden="true"></i> --}}
                                    <img src="{{ asset('img/cancel.svg') }}">
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            @role('event_planner' && Auth::user()->parent_id == null)
            <div class="wd100 mb-5">
                <a href="{{ route('' . $url . '.create-event') }}"><button type="button"
                        class="btn btn-primary float-right __create_eventBtn">Create a Event</button></a>
            </div>
            @endrole
            <!--end::Container-->
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        function filter(occasion = null, type = null, date = null, attendees = null) {
            var url = '/{{ $url }}/my-events?';
            if (occasion) {
                url += 'occasion=' + occasion + '&';
            }
            if (type) {
                url += 'type=' + type + '&';
            }
            if (date) {
                url += 'daterange=' + date + '&';
            }
            if (attendees) {
                url += 'attendees=' + attendees + '&';
            }
            location.replace(url);
        }

        $('#filter_occasion').on('change', function() {
            var filter_type = $('#filter_type').val();
            var filter_attendees = $('#filter_attendees').val();
            var filter_occasions = $('#filter_occasion').val();
            filter($(this).val(), filter_type, null, filter_attendees);
        });
        $('#filter_type').on('change', function() {
            var filter_type = $('#filter_type').val();
            var filter_attendees = $('#filter_attendees').val();
            var filter_occasions = $('#filter_occasion').val();
            filter(filter_occasions, filter_type, null, filter_attendees);
        });

        function changeDate(start, end) {
            var filter_type = $('#filter_type').val();
            var filter_attendees = $('#filter_attendees').val();
            var filter_occasions = $('#filter_occasion').val();
            filter(filter_occasions, filter_type, [start, end], filter_attendees);
        }
        $('#filter_attendees').on('change', function() {
            var filter_occasions = $('#filter_occasion').val();
            var filter_type = $('#filter_type').val();
            filter(filter_occasions, filter_type, null, $(this).val());
        });
        $('.btnSearch').on('click', function() {
            var filter_status = $('.filter_status_parent').find('.active').data('filter');
            var filter_search = $('#filter_search').val();
            filter(filter_status, $('#filter_occasion').find(':selected').val(), filter_search);
        });


        // predefined ranges
        var start = moment();
        var end = moment();
        // $('#kt_daterangepicker_6 .form-control').val(start.format('MM/DD/YYYY') + ' / ' + end.format(
        //         'MM/DD/YYYY'));
        var date_range = "{{ $request->daterange }}";
        if (date_range) {
            $('#kt_daterangepicker_6 .form-control').val(date_range);
        }
        $('#kt_daterangepicker_6').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',

            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf(
                    'month')]
            }
        }, function(start, end, label) {
            $('#kt_daterangepicker_6 .form-control').val(start.format('MM/DD/YYYY') + ' / ' + end.format(
                'MM/DD/YYYY'));
            changeDate(start.format('MM/DD/YYYY'), end.format(
                'MM/DD/YYYY'));
        });

        $('.eventRemoveBtn').on('click', function(e) {
            var id = $(this).data('id');
            Swal.fire({
                title: "Are you sure?",
                text: "You wont be able to revert this!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function(result) {
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "DELETE",
                        enctype: 'multipart/form-data',
                        url: "/event-planner/events/"+id,
                        success: function(data) {
                            setTimeout(function() {
                                location.reload();
                            }, 1000);
                        },
                        error: function(e) {

                        }
                    });
                } else if (result.dismiss === "cancel") {
                    Swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                    )
                }
            });

        });
    </script>

@endsection
