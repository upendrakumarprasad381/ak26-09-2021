{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/events/' . $event->id . '/edit') }}" class="text-muted">{{ $page_title }}</a>
    </li>
@endsection
{{-- Content --}}
@section('content')
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container-fluid">
            <!--begin::Dashboard-->
            <form id="my-form">
                @csrf
                @method('PATCH')
                <div class="row __lddelbk">
                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                        <label>Event Name</label>
                        <input type="text" class="form-control" name="event_name" placeholder="Name of Event"
                            value="{{ $event->event_name }}">
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 form-group">
                        <label>Events Requirments</label>
                        <select class="form-control select2" multiple="multiple" id="categories_select" name="categories[]">
                            <option value></option>
                            @foreach ($categories as $category)
                                <option value="{{ $category['id'] }}" @if (in_array($category['id'], $categories_ids)) selected @endif>{{ $category['name'] }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 form-group">
                        <label>Select Customer</label>
                        <div class="input-group">
                            <select class="form-control select2" id="customer_select" name="user_id">
                                <option value></option>
                                @foreach ($customers as $customer)
                                    <option value="{{ $customer['id'] }}" @if ($event->user_id == $customer['id']) selected="selected" @endif>
                                        {{ $customer['first_name'] }} {{ $customer['last_name'] }}</option>
                                @endforeach
                            </select>
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button" data-toggle="modal"
                                    data-target="#customerModel">New</button>
                            </div>
                        </div>
                    </div>



                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                        <label>Type Of Event</label>
                        <select class="form-control" id="exampleSelect1" name="occasion">
                            <option value>Select Event Type</option>
                            @foreach ($occasions as $occasion)
                                <option value="{{ $occasion['id'] }}" @if ($event->occasion_id == $occasion['id']) selected @endif>{{ $occasion['name'] }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                        <label>Budget</label>
                        <select class="form-control" id="price_range" name="price_range">
                            <option value>Select Budget Range</option>
                            <option value="[50000,0]" @if ($event['min_budget'] == 50000)
                                selected @endif>Over AED 50,000</option>
                            <option value="[40000,50000]" @if ($event['min_budget'] == 6000) selected @endif>AED 40,000 - AED 50,000
                            </option>
                            <option value="[30000,40000]" @if ($event['min_budget'] == 5000) selected @endif>AED 30,000 - AED 40,000
                            </option>
                            <option value="[20000,30000]" @if ($event['min_budget'] == 4000) selected @endif>AED 20,000 - AED 30,000
                            </option>
                            <option value="[10000,20000]" @if ($event['min_budget'] == 3000) selected @endif>AED 10,000 - AED 20,000
                            </option>
                            <option value="[5000,10000]" @if ($event['min_budget'] == 2000) selected @endif>AED 5,000 - AED 10,000
                            </option>
                            <option value="[0,5000]" @if ($event['min_budget'] == 0)
                                selected @endif>Less than AED 5,000
                            </option>
                        </select>
                    </div>
                    {{-- <div class="col-lg-3 col-md-3 col-sm-12 form-group">
				<label>Final Budget</label>
                <input type="text" class="form-control numeric" name="final_budget" placeholder="Final Budget" value="{{$event->final_budget}}">
			</div> --}}
                    <div class="col-lg-3 col-md-3 col-sm-12 form-group">
                        <label>Number of Attendees</label>
                        <select class="form-control select2" id="number_of_attendees" name="number_of_attendees">
                            <option value>Select No Of Attendees</option>
                            @foreach ($attendees as $attendees_)
                                <option value="{{ $attendees_->value }}" @if ($event->number_of_attendees == $attendees_->value) selected @endif>
                                    {{ $attendees_->value }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12 form-group">
                        <label>Event Location</label>
                        <select class="form-control select2" id="event_location" name="event_location">
                            <option value>Event Location</option>
                            @foreach ($emirates as $emirate)
                                <option value="{{ $emirate->emirate }}" @if ($event->event_location == $emirate->emirate) selected @endif>
                                    {{ $emirate->emirate }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 form-group">
                        <label>Planning Date</label>
                        <div class="input-group date" id="kt_datetimepicker_4" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" placeholder="Select date "
                                data-target="#kt_datetimepicker_4" name="planning_date" />
                            <div class="input-group-append" data-target="#kt_datetimepicker_4" data-toggle="datetimepicker">
                                <span class="input-group-text">
                                    <i class="ki ki-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 form-group">
                        <label>Event Date</label>
                        <div class="input-group date" id="kt_datetimepicker_3" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" placeholder="Select date "
                                data-target="#kt_datetimepicker_3" name="event_date" />
                            <div class="input-group-append" data-target="#kt_datetimepicker_3" data-toggle="datetimepicker">
                                <span class="input-group-text">
                                    <i class="ki ki-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 form-group">
                        <label>Time</label>
                        <input class="form-control" type="time" name="event_time" id="example-time-input"
                            value="{{ $event->event_time }}" />
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 form-group">
                        <label>Upload Event Image</label>
                        <ul id="media-list" class="clearfix">
                            <li class="myupload" @if ($event->image) style="display:none;" @endif>
                                <span><i class="fa fa-plus" aria-hidden="true"></i>
                                    <input type="file" click-type="single" id="picupload" name="image"
                                        value="{{ url('/storage') }}/{{ $event->image }}" class="picupload">
                                </span>
                            </li>
                            @if ($event->image)
                                <li>
                                    <img src="/storage/{{ $event->image }}" title="" />
                                    <div class='post-thumb'>
                                        <div class='inner-post-thumb'>
                                            <a href='javascript:void(0);' data-id='' class='remove-pic'>
                                                <i class='fa fa-times' aria-hidden='true'>
                                                </i>
                                            </a>
                                        </div>
                                    </div>
                                    <input type="text" name="old_image" value="{{ $event->image }}"
                                        style="display: none;">
                                </li>
                            @endif
                        </ul>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12 form-group mt-5">
                        <div class="checkbox-inline">
                            <label class="checkbox checkbox-outline">
                                <input type="checkbox" class="type_checkbox" value="Indoor" name="type_of_event"
                                    @if ($event->type_of_event == 'Indoor') checked @endif />
                                <span></span>
                                Indoor
                            </label>
                            <label class="checkbox checkbox-outline">
                                <input type="checkbox" class="type_checkbox" value="Outdoor" name="type_of_event"
                                    @if ($event->type_of_event == 'Outdoor') checked @endif />
                                <span></span>
                                Outdoor
                            </label>
                        </div>
                    </div>

                </div>
                <h4>Event Permissions</h4>
                <br>
                <div class="row">
                    @foreach ($permissions as $permission)
                        <div class="col-lg-4 col-md-4 col-sm-12 form-group">
                            <div class="checkbox-inline">
                                <label class="checkbox checkbox-outline">
                                    <input type="checkbox" value="{{ $permission['id'] }}" name="event_permissions[]"
                                        @if (count($event_permissions) > 0)@if (in_array($permission['id'], $event_permissions)) checked @endif @endif />
                                    <span></span>
                                    {{ $permission['display_name'] }}
                                </label>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="wd100 mb-5">
                    <button type="button" id="btnSubmit" class="btn btn-primary float-right __create_eventBtn">Update
                        Event</button>
                </div>
            </form>
            <!--end::Container-->
        </div>
    </div>

    <!-- Modal-->
    <div class="modal fade" id="customerModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create New Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="new-customer-form">
                        @csrf
                        <div class="row __lddelbk">
                            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                <label>First Name</label>
                                <input type="text" class="form-control" name="first_name" placeholder="First Name">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                <label>Last Name</label>
                                <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" placeholder="Customer Email">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                <label>Phone</label>
                                <input type="text" class="form-control" name="phone" placeholder="Customer Phone">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                    <button type="button" id="btnCustomerSave" class="btn btn-primary font-weight-bold">Save</button>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        $("#btnSubmit").click(function(event) {
            event.preventDefault();
            var form = $('#my-form')[0];
            var data = new FormData(form);
            $("#btnSubmit").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/{{ $url }}/events/{{ $event->id }}",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    $("#btnSubmit").prop("disabled", false);
                    setTimeout(function() {
                        location.replace('/{{ $url }}/event/{{ $event->id }}');
                    }, 1000);
                },
                error: function(e) {
                    $("#btnSubmit").prop("disabled", false);
                }
            });
        });

        $("#btnCustomerSave").click(function(event) {
            event.preventDefault();
            var form = $('#new-customer-form')[0];
            var data = new FormData(form);
            $("#btnCustomerSave").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/vendor_create_customer",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    $("#btnCustomerSave").prop("disabled", false);
                    $('#customer_select').append('<option value=' + data.user.id + '>' + data.user
                        .first_name + ' ' + data.user.last_name + '</option>');
                    $('#customer_select').val(data.user.id);
                    $('#customerModel').modal('hide');
                },
                error: function(e) {
                    $("#btnCustomerSave").prop("disabled", false);
                }
            });
        });

        var event_date = "{{ $event['event_date'] }}";
        if (event_date != '0000-00-00') {
            $('#kt_datetimepicker_3').datetimepicker({
                defaultDate: event_date,
                format: 'MM/D/yyyy'
            });
        }
        else
        {
            $('#kt_datetimepicker_3').datetimepicker({
                defaultDate: new Date(),
                format: 'MM/D/yyyy'
            });
        }
        // $('#kt_datetimepicker_3').datetimepicker({
        //     defaultDate: "{{ $event['event_date'] }}",
        //     format: 'MM/D/yyyy'
        // });
        var planning_date = "{{ $event['planning_date'] }}";
        if (planning_date != '0000-00-00') {
            $('#kt_datetimepicker_4').datetimepicker({
                defaultDate: planning_date,
                format: 'MM/D/yyyy'
            });
        }
        else
        {
            $('#kt_datetimepicker_4').datetimepicker({
                defaultDate: new Date(),
                format: 'MM/D/yyyy'
            });
        }




        $('#customer_select').select2({
            placeholder: "Select a Customer",
        });
        // $('#customer_select').val("{{ $event->user_id }}");
        // $('#customer_select').trigger('change');
        $('.type_checkbox').click(function() {
            $('.type_checkbox').not(this).prop('checked', false);
        });

        $('#categories_select').select2({
            placeholder: "Events Requirments",
        });
        $('#number_of_attendees').select2({
            placeholder: "Number of Attendees",
        });
        $('#event_location').select2({
            placeholder: "Event Location",
        });
    </script>
    <!--end::Page Scripts-->
@endsection
