{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/event/' . $event->id . '') }}" class="text-muted">{{ $page_title }}</a>
    </li>
@endsection
{{-- Content --}}
@section('content')
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container-fluid">
            <!--begin::Dashboard-->
            <div class="wd100 __topSwitch">
                <div class="d-flex float-right">
                    <div class="dropdown __topSwitchBtn">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Switch Event
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @foreach ($events as $switch_event)
                                @if ($switch_event->id != $event->id)
                                    <a class="dropdown-item"
                                        href="{{ route('' . $url . '.event', $switch_event->id) }}">{{ $switch_event->event_name }}</a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="wd100 __subbnrWrp d-flex">
                <div class="__subbnrTxpt">
                    <h2>{{ $event->event_name }}</h2>
                    <h3>{{ date('d M Y', strtotime($event->event_date)) }}</h3>
                    <h4>{{ $event->event_location }}</h4>
                    <h5>@if ($event->event_time){{ date('h:i A', strtotime($event->event_time)) }} @endif</h5>
                </div>
                <div class="__subbnrImgpt" @if ($event->image != null) style="background: url({{ asset('storage/' . $event->image . '') }}) center" @else style="background: url({{ asset('storage/' . $event->occasion['image'] . '') }}) center" @endif>
                </div>
            </div>
            <div class="wd100 __tpExpBtnWrp">
                <div class="d-flex float-right">
                    @if ($event->vendor_id == Auth::user()->id || Auth::user()->hasRole('event_planner'))
                        <a href="{{ url('' . $url . '/events/' . $event->id . '/edit') }}"><button type="button"
                                class="btn btn-primary">Edit Event</button></a>
                    @endif
                    <button type="button" class="btn btn-secondary btnExport">Export Event</button>
                    <button type="button" class="btn btn-primary btnDuplicate">Duplicate Event</button>
                </div>
            </div>

            <div class="row __sublk">
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <!--begin::Stats Widget 16-->
                    <div href="#" class="card card-custom card-stretch ">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="font-weight-bold">
                                <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans" tabId="all"
                                    id="all-activities">0</span>
                            </div>
                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2"><a
                                    onclick="ToDolistGoTo('all-activities');" href="javascript:void(0)">All Activities</a>
                            </div>
                            <div class="wd100 __bozflddrp" style="display: none;">
                                <select class="form-control form-control-sm" onchange="activitiesData('all-activities');"
                                    id="all-activities-days">
                                    <option value="0">All</option>
                                    {{-- <option value="{{ date('Y-m-d', strtotime('-90 day')) }}">Last 90 Days</option>
                                    <option value="{{ date('Y-m-d', strtotime('-60 day')) }}">Last 60 Days</option>
                                    <option value="{{ date('Y-m-d', strtotime('-30 day')) }}">Last 30 Days</option> --}}
                                </select>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 16-->
                </div>


                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <!--begin::Stats Widget 16-->
                    <div href="#" class="card card-custom card-stretch ">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans"
                                    tabId='todays-overdue' id="overdue">0</span>
                            </div>

                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 "><a
                                    onclick="ToDolistGoTo('overdue');" href="javascript:void(0)">Overdue</a></div>
                            <div style="display: none;" class="wd100 __bozflddrp">
                                <select class="form-control form-control-sm" onchange="activitiesData('overdue');"
                                    id="overdue-days">
                                    <option value="0">All</option>
                                    {{-- <option value="{{ date('Y-m-d', strtotime('-90 day')) }}">Last 90 Days</option>
                                    <option value="{{ date('Y-m-d', strtotime('-60 day')) }}">Last 60 Days</option>
                                    <option value="{{ date('Y-m-d', strtotime('-30 day')) }}">Last 30 Days</option> --}}
                                </select>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 16-->
                </div>

                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <!--begin::Stats Widget 16-->
                    <div href="#" class="card card-custom card-stretch ">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans" tabId='upcoming'
                                    id="upcoming">0</span>
                            </div>

                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 "><a
                                    onclick="ToDolistGoTo('upcoming');" href="javascript:void(0)">Upcoming</a></div>

                            <div style="display: none;" class="wd100 __bozflddrp">
                                <select class="form-control form-control-sm" onchange="activitiesData('upcoming');"
                                    id="upcoming-days">
                                    <option value="0">All</option>
                                    {{-- <option value="{{ date('Y-m-d', strtotime('+90 day')) }}">Next 90 Days</option>
                                    <option value="{{ date('Y-m-d', strtotime('+60 day')) }}">Next 60 Days</option>
                                    <option value="{{ date('Y-m-d', strtotime('+30 day')) }}">Next 30 Days</option> --}}
                                </select>
                            </div>

                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 16-->
                </div>

                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <!--begin::Stats Widget 16-->
                    <div class="card card-custom card-stretch ">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans" tabId='completed'
                                    id="completed">0</span>
                            </div>

                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 "><a
                                    onclick="ToDolistGoTo('completed');" href="javascript:void(0)">Completed</a></div>
                            <div style="display: none;" class="wd100 __bozflddrp">
                                <select class="form-control form-control-sm" onchange="activitiesData('completed');"
                                    id="completed-days">
                                    <option value="0">All</option>
                                    {{-- <option value="{{ date('Y-m-d', strtotime('-90 day')) }}">Last 90 Days</option>
                                    <option value="{{ date('Y-m-d', strtotime('-60 day')) }}">Last 60 Days</option>
                                    <option value="{{ date('Y-m-d', strtotime('-30 day')) }}">Last 30 Days</option> --}}
                                </select>
                            </div>

                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 16-->
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-lg-8 col-md-8 col-sm-12">
                    <div class="row __srvBzWrp">
                        @role('event_planner')
                            @if (in_array('view-vendor-manager', $permissions))
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <a href="/vendor-manager/{{ $event->id }}/view-all-request" class="wd100 __srvBzbk">
                                        <div class="__evtPlnCount">{{ $event->counts()['ep_requests'] }}</div>
                                        <div class="__sbkIcon">
                                            <i class="flaticon-user"></i>
                                        </div>
                                        <h5>Vendor Manager</h5>
                                    </a>
                                </div>
                            @endif
                        @endrole
                        @if (in_array('view-calendar', $permissions))
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <a href="/calender/{{ $event->id }}" class="wd100 __srvBzbk">
                                    <div class="__sbkIcon">
                                        <i class="flaticon-calendar"></i>
                                    </div>

                                    <h5>Calender</h5>
                                </a>
                            </div>
                        @endif
                        @role('event_planner')
                            @if (in_array('view-budgeter', $permissions))
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <a href="{{ url('' . $url . '/event/' . $event->id . '/budgeter') }}"
                                        class="wd100 __srvBzbk">
                                        <div class="__sbkIcon">
                                            <i class="flaticon-budget"></i>
                                        </div>
                                        <h5>Budgeter</h5>
                                    </a>
                                </div>
                            @endif
                        @endrole
                        @if (in_array('view-contracts', $permissions))
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <a href="{{ url('' . $url . '/event/' . $event->id . '/contracts?status=Pending') }}"
                                    class="wd100 __srvBzbk">
                                    <div class="__evtPlnCount">{{ $event->counts()['contracts'] }}</div>
                                    <div class="__sbkIcon">
                                        <i class="flaticon-contract"></i>
                                    </div>
                                    <h5>Contracts</h5>
                                </a>
                            </div>
                        @endif
                        @if (in_array('view-checklist', $permissions))
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <a href="/checklist/{{ $event->id }}" class="wd100 __srvBzbk">
                                    <div class="__sbkIcon">
                                        <i class="flaticon-clipboard"></i>
                                    </div>
                                    <h5>Checklist</h5>
                                </a>
                            </div>
                        @endif
                        @if (in_array('view-proposals', $permissions))
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <a href="{{ url('' . $url . '/event/' . $event->id . '/proposals?status=Send/Recieved') }}"
                                    class="wd100 __srvBzbk">
                                    <div class="__evtPlnCount">{{ $event->counts()['proposals'] }}</div>
                                    <div class="__sbkIcon">
                                        <i class="flaticon-receipt"></i>
                                    </div>
                                    <h5>Proposals</h5>
                                </a>
                            </div>
                        @endif
                        @role('event_planner')
                            @if (in_array('view-create-a-web_page', $permissions))
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <a href="/create-web-page/{{ $event->id }}" class="wd100 __srvBzbk">
                                        <div class="__sbkIcon">
                                            <i class="flaticon-networking"></i>
                                        </div>
                                        <h5>Create a Web Page</h5>
                                    </a>
                                </div>
                            @endif
                            @if (in_array('view-floor-plans', $permissions))
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <a href="#" class="wd100 __srvBzbk">
                                        <div class="__sbkIcon">
                                            <i class="flaticon-blueprint"></i>
                                        </div>
                                        <h5>Floor Plans</h5>
                                    </a>
                                </div>
                            @endif
                        @endrole
                        @if (in_array('view-invoices', $permissions))
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <a href="{{ url('' . $url . '/event/' . $event->id . '/invoices') }}"
                                    class="wd100 __srvBzbk">
                                    <div class="__evtPlnCount">{{ $event->counts()['invoices'] }}</div>
                                    <div class="__sbkIcon">
                                        <i class="flaticon-report"></i>
                                    </div>
                                    <h5>Invoices</h5>
                                </a>
                            </div>
                        @endif
                        @if (in_array('view-conversations', $permissions))
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <a href="/conversations" target="_blank" class="wd100 __srvBzbk">
                                    <div class="__sbkIcon">
                                        <i class="flaticon-chat"></i>
                                    </div>
                                    <h5>Conversations</h5>
                                </a>
                            </div>
                        @endif
                        @role('event_planner')
                            @if (in_array('view-guest-list', $permissions))
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <a href="/guest-list/{{ $event->id }}" class="wd100 __srvBzbk">
                                        <div class="__sbkIcon">
                                            <i class="flaticon-guest-list"></i>
                                        </div>
                                        <h5>Guest List</h5>
                                    </a>
                                </div>
                            @endif
                        @endrole
                        {{-- @if (in_array('view-form-builder', $permissions))
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <a href="#" class="wd100 __srvBzbk">
                                    <div class="__sbkIcon">
                                        <i class="flaticon-contact-form"></i>
                                    </div>
                                    <h5>Form Builder</h5>
                                </a>
                            </div>
                        @endif --}}
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 ">

                    <div class="card card-custom __enrtTabwgt">
                        <div class="card-header">

                            <div class="card-toolbar wd100">
                                <ul class="nav nav-bold nav-pills  wd100">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_7_1">Vendors</a>
                                    </li>
                                    {{-- <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_7_2">Event
                                            Planner</a>
                                    </li> --}}
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_7_3">Venue</a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="kt_tab_pane_7_1" role="tabpanel"
                                    aria-labelledby="kt_tab_pane_7_1">
                                    <div class="wd100 __tbRultContacts">
                                        @foreach ($chat_list as $key => $vendor)
                                            <div class="d-flex align-items-center __rtUrclibz">
                                                <div class="symbol symbol-50 symbol-light mr-5 __taurpic">
                                                    <img src="@if ($vendor['image'] == null){{ asset('media/users/100_8.jpg') }} @else {{ asset('/storage/' . $vendor['image'] . '') }} @endif" class="h-75 align-self-end" alt="">
                                                </div>
                                                <div>
                                                    <a href="/vendor-manager/vendor-details/{{ $key }}/{{ $event->categories[0]['category_id'] }}/{{ $event->id }}"
                                                        class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font_size_18">{{ $vendor['name'] }}</a>
                                                    <a href="/vendor-manager/vendor-details/{{ $key }}/{{ $event->categories[0]['category_id'] }}/{{ $event->id }}"
                                                        class="text-muted font-weight-bold d-block">{{ $vendor['name'] }}</a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="kt_tab_pane_7_2" role="tabpanel"
                                    aria-labelledby="kt_tab_pane_7_2">
                                    <div class="wd100 __tbRultVenue">
                                        @foreach ($chat_list as $vendor)
                                            @if ($vendor['type'] == 16)
                                                <div class="d-flex align-items-center __rtUrclibz">
                                                    <div class="symbol symbol-50 symbol-light mr-5 __taurpic">
                                                        <img src="@if ($vendor['image'] == null){{ asset('media/users/100_8.jpg') }} @else {{ asset('/storage/' . $vendor['image'] . '') }} @endif" class="h-75 align-self-end"
                                                            alt="">
                                                    </div>
                                                    <div>
                                                        <a href="/vendor-manager/vendor-details/{{ $key }}/{{ $event->categories[0]['category_id'] }}/{{ $event->id }}"
                                                            class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font_size_18">{{ $vendor['name'] }}</a>
                                                        <a href="/vendor-manager/vendor-details/{{ $key }}/{{ $event->categories[0]['category_id'] }}/{{ $event->id }}"
                                                            class="text-muted font-weight-bold d-block">{{ $vendor['name'] }}</a>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>




            </div>
            <!--end::Container-->
        </div>
        <!-- Modal-->
        <div class="modal fade" id="budgetEntryModel" role="model" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Final Budget</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <form method="POST" action="/{{ $url }}/event/{{ $event->id }}/save-final-budget">
                        <div class="modal-body">
                            @csrf
                            <div class="wd100 __proslAddWrp __addscpWrkPg">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4 __ctndrofid">
                                        <label class="form-label">Budget Amount</label>
                                        <input type="text" class="form-control numeric" name="final_amount"
                                            placeholder="Amount">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light-primary font-weight-bold"
                                data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary font-weight-bold">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{-- @if (Session::get('show_buget_model'))
            <script>

            </script>
        @endif --}}
    @endsection

    {{-- Scripts Section --}}
    @section('scripts')
        <script>
            if ("{{ Session::get('show_buget_model') }}")
                $('#budgetEntryModel').modal('show');
        </script>
        <script>
            $(document).ready(function() {
                setTimeout(function() {
                    activitiesData('all-activities');
                    activitiesData('overdue');
                    activitiesData('upcoming');
                    activitiesData('completed');
                }, 500);

            });

            function activitiesData(counterId) {
                var fromDays = $('#' + counterId + '-days').val();
                var tabId = $('#' + counterId).attr('tabId');

                var data = {
                    _token: "{{ csrf_token() }}",
                    autoload: true,
                    eventDashboard: true,
                    eventId: '{{ $event->id }}',
                    tabId: tabId,
                    fromDays: fromDays
                };
                $.ajax({
                    url: "{{ url('/to-do-list') }}",
                    cache: false,
                    data: data,
                    async: true,
                    type: 'POST',
                    success: function(res) {
                        res = jQuery.parseJSON(res);
                        if (res.status == true) {
                            $('#' + counterId).html(res.dataCount);
                        }
                    }
                });

            }

            function ToDolistGoTo(counterId) {
                var fromDays = $('#' + counterId + '-days').val();
                var tabId = $('#' + counterId).attr('tabId');
                var Url = "{{ url('/to-do-list') }}/?tabId=" + tabId + "&fromdate=" + fromDays +
                    '&eventId={{ $event->id }}';
                window.location = Url;
            }

            $('.btnDuplicate').on('click', function() {
                Swal.fire({
                    title: "Are you sure?",
                    text: "You want to duplicate this event!",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                }).then(function(result) {
                    if (result.value) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url: "/{{ $url }}/event/{{ $event->id }}/create-duplicate",
                            success: function(data) {
                                setTimeout(function() {
                                    location.replace('/{{ $url }}/event/' + data
                                        .event_id)
                                }, 1000);
                            },
                            error: function(e) {}
                        });
                    }
                });
            });

            $('.btnExport').on('click', function() {
                $('.btnExport').text('Please Wait...');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/{{ $url }}/event/{{ $event->id }}/export-event",
                    success: function(data) {
                        $('.btnExport').text('Event Export');
                        window.open('/storage/pdf/'+data);
                    },
                    error: function(e) {}
                });
            });
        </script>
    @endsection
