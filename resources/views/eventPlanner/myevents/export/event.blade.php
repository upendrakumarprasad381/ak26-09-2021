<!DOCTYPE html>
<html>
<head>
    <title>{{ $event->event_name }}</title>
</head>
<body>
    <div class="d-flex flex-column-fluid">
        <div class="container-fluid">
            <div class="__invoicWrp wd100">
                <div class="__viewIncWoz" style="width:720px; margin: 0 auto;">
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"
                        style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
                        <tr id="pdf_header">
                            <td width="55%" align="left" valign="middle">
                                <img src="https://askdeema.granddubai.com/user/images/logo.png" width="250px" />
                            </td>
                            <td width="45%" align="left" valign="middle" style="font-size:14px; line-height:25px;">
                                <h3 style="font-size:18px; margin:0px 0px; padding:0 0;">
                                    {{ $event->event_name }}</h3>
                                Customer&nbsp;Name : @if ($event->customer) {{ $event->customer['first_name'] }} @endif<br>
                                Email &nbsp;&nbsp; : @if ($event->customer) {{ $event->customer['email'] }} @endif <br>
                                Date : {{ $event->event_date }} <br>
                                Location : {{ $event->event_location }}
                            </td>
                        </tr>
                    </table>
                    <hr>
                    <br>
                    <h5
                        style="color:#7A5C66; margin:0 0 10px; font-size:16px; font-family:Arial, Helvetica, sans-serif; ">
                        Event Details</h5>
                    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0"
                        style="font-family:Arial, Helvetica, sans-serif; font-size:14px; border:#000 1px solid;">
                        <tr>
                            <td width="33%" align="left" valign="top"
                                style="border-right: #000 1px solid; font-family: Arial, Helvetica, sans-serif;">

                                <table width="100% " border="0" align="center" cellpadding="3" cellspacing="0"
                                    style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <tr>
                                        <td width="50%">Occasion</td>
                                        <td width="50%">: {{ $event->occasion['name'] }}</td>
                                    </tr>
                                    <tr>
                                        <td width="50%">Venue</td>
                                        <td width="50%">: {{ $event->event_location ?: 'Not Specified' }} </td>
                                    </tr>
                                    <tr>
                                        <td width="50%">Planning Date</td>
                                        <td width="50%">: {{ $event->planning_date }} </td>
                                    </tr>
                                    <tr>
                                        <td>Budget</td>
                                        <td>:
                                            {{ $event->min_budget > 0 ? 'AED ' . $event->min_budget : '' . ($event->max_budget > 0 ? ' -' . ' AED ' . $event->max_budget : '') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Photography Start Time</td>
                                        <td>: {{ date('H:i A', strtotime($event->event_time)) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Events Category</td>
                                        <td>: @if ($event->categoriesModel)
                                                @foreach ($event->categoriesModel as $category)
                                                    {{ $category['name'] }}
                                                @endforeach
                                            @endif
                                        </td>
                                    </tr>
                                    {{-- <tr>
                                  <td>Would you like to meet this vendor over video chat?</td>
                                  <td>: Maybe later</td>
                                </tr> --}}
                                </table>
                            </td>
                            <td width="33%" align="left" valign="top" style=" ">

                                <table width="100% " border="0" align="center" cellpadding="3" cellspacing="0"
                                    style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                                    <tr>
                                        <td>Event Type</td>
                                        <td>: {{ $event->type_of_event }}</td>
                                    </tr>
                                    <tr>
                                        <td>Location</td>
                                        <td>: {{ $event->event_location ?: 'Not Specified' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Event Date</td>
                                        <td>: {{ $event->event_date }}</td>
                                    </tr>
                                    <tr>
                                        <td>Final Budget</td>
                                        <td>: AED {{ $event->final_budget }}</td>
                                    </tr>
                                    <tr>
                                        <td>Guest</td>
                                        <td>: {{ $event->number_of_attendees }}</td>
                                    </tr>
                                    <tr>
                                        <td>Photography Hours</td>
                                        <td>: {{ $event->number_of_hours }} Hours</td>
                                    </tr>

                                </table>
                            </td>

                        </tr>

                    </table>
                    <br>
                    <h5
                        style="color:#7A5C66; margin:0 0 10px; font-size:16px; font-family:Arial, Helvetica, sans-serif; ">
                        Note</h5>
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"
                        style="font-family:Arial, Helvetica, sans-serif; font-size:14px; margin-top:18px">
                        <tr>
                            <td>&nbsp; {{ $event->quick_note }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div style="page-break-after: always;"></div>

    <div class="d-flex flex-column-fluid">
        <div class="container-fluid">
            <div class="__invoicWrp wd100">
                <div class="__viewIncWoz" style="width:720px; margin: 0 auto;">
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"
                        style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
                        <tr id="pdf_header">
                            <td width="55%" align="left" valign="middle">
                                <img src="https://askdeema.granddubai.com/user/images/logo.png" width="250px" />
                            </td>
                            <td width="45%" align="left" valign="middle" style="font-size:14px; line-height:25px;">
                                <h3 style="font-size:18px; margin:0px 0px; padding:0 0;">
                                    {{ $event->event_name }}</h3>
                                Customer&nbsp;Name : @if ($event->customer) {{ $event->customer['first_name'] }} @endif<br>
                                Email &nbsp;&nbsp; : @if ($event->customer) {{ $event->customer['email'] }} @endif <br>
                                Date : {{ $event->event_date }} <br>
                                Location : {{ $event->event_location }}
                            </td>
                        </tr>
                    </table>
                    <hr>
                    <br>
                    <h5
                        style="color:#7A5C66; margin:0 0 10px; font-size:16px; font-family:Arial, Helvetica, sans-serif; ">
                        Proposals</h5>
                    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0"
                        style="font-family:Arial, Helvetica, sans-serif; font-size:14px; border:#000 1px solid; border-bottom:none;">
                        <tr>
                            <th align="center" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">SL
                            </th>
                            <th align="center" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">Date
                            </th>
                            <th align="left" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">
                                Category</th>
                            <th align="left" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">
                                Vendor Name</th>

                            <th align="left" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">
                                Status</th>
                            <th align="center" valign="middle" style="border-bottom: #000 1px solid;   " scope="col">
                                Total Amount</th>
                        </tr>
                        @if ($proposals)
                            @foreach ($proposals as $proposal)
                                <tr>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $loop->iteration }}</td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $proposal->date }}
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $proposal->request->category['name'] }}
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        @if (Auth::user()->hasRole('vendor'))
                                            {{ $proposal->party2['first_name'] }}
                                            {{ $proposal->party2['last_name'] }}
                                        @else
                                            {{ $proposal->party1['first_name'] }}
                                            {{ $proposal->party1['last_name'] }}
                                        @endif
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $proposal->status }}
                                    </td>
                                    <td align="right" valign="middle" style="border-bottom: #000 1px solid;   ">AED
                                        {{ $proposal->items->sum('total') }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                    <br>
                    <h5
                        style="color:#7A5C66; margin:0 0 10px; font-size:16px; font-family:Arial, Helvetica, sans-serif; ">
                        Note</h5>
                </div>
            </div>
        </div>
    </div>
    <div style="page-break-after: always;"></div>
    <div class="d-flex flex-column-fluid">
        <div class="container-fluid">
            <div class="__invoicWrp wd100">
                <div class="__viewIncWoz" style="width:720px; margin: 0 auto;">
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"
                        style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
                        <tr id="pdf_header">
                            <td width="55%" align="left" valign="middle">
                                <img src="https://askdeema.granddubai.com/user/images/logo.png" width="250px" />
                            </td>
                            <td width="45%" align="left" valign="middle" style="font-size:14px; line-height:25px;">
                                <h3 style="font-size:18px; margin:0px 0px; padding:0 0;">
                                    {{ $event->event_name }}</h3>
                                Customer&nbsp;Name : @if ($event->customer) {{ $event->customer['first_name'] }} @endif<br>
                                Email &nbsp;&nbsp; : @if ($event->customer) {{ $event->customer['email'] }} @endif <br>
                                Date : {{ $event->event_date }} <br>
                                Location : {{ $event->event_location }}
                            </td>
                        </tr>
                    </table>
                    <hr>
                    <br>
                    <h5
                        style="color:#7A5C66; margin:0 0 10px; font-size:16px; font-family:Arial, Helvetica, sans-serif; ">
                        Contracts</h5>
                    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0"
                        style="font-family:Arial, Helvetica, sans-serif; font-size:14px; border:#000 1px solid; border-bottom:none;">
                        <tr>
                            <th align="center" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">SL
                            </th>
                            <th align="center" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">Date
                            </th>
                            <th align="left" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">
                                Category</th>
                            <th align="left" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">
                                Vendor Name</th>

                            <th align="left" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">
                                Status</th>
                            <th align="center" valign="middle" style="border-bottom: #000 1px solid;   " scope="col">
                                Total Amount</th>
                        </tr>
                        @if ($contracts)
                            @foreach ($contracts as $contract)
                                <tr>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $loop->iteration }}</td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $contract->date }}
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $contract->proposal->request->category['name'] }}
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        @if (Auth::user()->hasRole('vendor'))
                                            {{ $contract->party1['first_name'] }}
                                            {{ $contract->party1['last_name'] }}
                                        @else
                                            {{ $contract->party2['first_name'] }}
                                            {{ $contract->party2['last_name'] }}
                                        @endif
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $contract->status }}
                                    </td>
                                    <td align="right" valign="middle" style="border-bottom: #000 1px solid;   ">AED
                                        {{ $contract->items->sum('total') }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                    <br>
                    <h5
                        style="color:#7A5C66; margin:0 0 10px; font-size:16px; font-family:Arial, Helvetica, sans-serif; ">
                        Note</h5>
                </div>
            </div>
        </div>
    </div>
    <div style="page-break-after: always;"></div>
    <div class="d-flex flex-column-fluid">
        <div class="container-fluid">
            <div class="__invoicWrp wd100">
                <div class="__viewIncWoz" style="width:720px; margin: 0 auto;">
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"
                        style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
                        <tr id="pdf_header">
                            <td width="55%" align="left" valign="middle">
                                <img src="https://askdeema.granddubai.com/user/images/logo.png" width="250px" />
                            </td>
                            <td width="45%" align="left" valign="middle" style="font-size:14px; line-height:25px;">
                                <h3 style="font-size:18px; margin:0px 0px; padding:0 0;">
                                    {{ $event->event_name }}</h3>
                                Customer&nbsp;Name : @if ($event->customer) {{ $event->customer['first_name'] }} @endif<br>
                                Email &nbsp;&nbsp; : @if ($event->customer) {{ $event->customer['email'] }} @endif <br>
                                Date : {{ $event->event_date }} <br>
                                Location : {{ $event->event_location }}
                            </td>
                        </tr>
                    </table>
                    <hr>
                    <br>
                    <h5
                        style="color:#7A5C66; margin:0 0 10px; font-size:16px; font-family:Arial, Helvetica, sans-serif; ">
                        Invoices</h5>
                    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0"
                        style="font-family:Arial, Helvetica, sans-serif; font-size:14px; border:#000 1px solid; border-bottom:none;">
                        <tr>
                            <th align="center" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">SL
                            </th>
                            <th align="center" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">Date
                            </th>
                            <th align="left" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">
                                Category</th>
                            <th align="left" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">
                                Vendor Name</th>

                            <th align="left" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">
                                Status</th>
                            @if($invoices)
                            <th align="left" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">
                                Payment Status</th>
                            @endif
                            <th align="center" valign="middle" style="border-bottom: #000 1px solid;   " scope="col">
                                Total Amount</th>
                        </tr>
                        @if ($invoices)
                            @foreach ($invoices as $invoice)
                                <tr>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $loop->iteration }}</td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $invoice->date }}
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $invoice->contract->proposal->request->category['name'] }}
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        @if (Auth::user()->hasRole('vendor'))
                                            {{ $invoice->party2['first_name'] }}
                                            {{ $invoice->party2['last_name'] }}
                                        @else
                                            {{ $invoice->party1['first_name'] }}
                                            {{ $invoice->party1['last_name'] }}
                                        @endif
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $invoice->status }}
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $invoice->payment_status }}
                                    </td>
                                    <td align="right" valign="middle" style="border-bottom: #000 1px solid;   ">AED
                                        {{ $invoice->items->sum('total') }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                    <br>
                    <h5
                        style="color:#7A5C66; margin:0 0 10px; font-size:16px; font-family:Arial, Helvetica, sans-serif; ">
                        Note</h5>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
