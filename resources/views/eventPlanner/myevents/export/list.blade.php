<!DOCTYPE html>
<html>
<head>
    <title>{{ $event->event_name }}</title>
</head>

<body style="page-break-after: always;">
    <div class="d-flex flex-column-fluid">
        <div class="container-fluid">
            <div class="__invoicWrp wd100">
                <div class="__viewIncWoz" style="width:720px; margin: 0 auto;">
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"
                        style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
                        <tr id="pdf_header">
                            <td width="55%" align="left" valign="middle">
                                <img src="https://askdeema.granddubai.com/user/images/logo.png" width="250px" />
                            </td>
                            <td width="45%" align="left" valign="middle" style="font-size:14px; line-height:25px;">
                                <h3 style="font-size:18px; margin:0px 0px; padding:0 0;">
                                    {{ $event->event_name }}</h3>
                                Customer&nbsp;Name : @if ($event->customer) {{ $event->customer['first_name'] }} @endif<br>
                                Email &nbsp;&nbsp; : @if ($event->customer) {{ $event->customer['email'] }} @endif <br>
                                Date : {{ $event->event_date }} <br>
                                Location : {{ $event->event_location }}
                            </td>
                        </tr>
                    </table>
                    <hr>
                    <br>
                    <h5
                        style="color:#7A5C66; margin:0 0 10px; font-size:16px; font-family:Arial, Helvetica, sans-serif; ">
                        {{ $title }}</h5>
                    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0"
                        style="font-family:Arial, Helvetica, sans-serif; font-size:14px; border:#000 1px solid; border-bottom:none;">
                        <tr>
                            <th align="center" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">SL
                            </th>
                            <th align="center" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">Date
                            </th>
                            <th align="left" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">
                                Category</th>
                            <th align="left" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">
                                Vendor Name</th>

                            <th align="left" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">
                                Status</th>
                            @if($invoices)
                            <th align="left" valign="middle"
                                style="border-bottom: #000 1px solid; border-right: #000 1px solid; " scope="col">
                                Payment Status</th>
                            @endif
                            <th align="center" valign="middle" style="border-bottom: #000 1px solid;   " scope="col">
                                Total Amount</th>
                        </tr>
                        @if ($proposals)
                            @foreach ($proposals as $proposal)
                                <tr>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $loop->iteration }}</td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $proposal->date }}
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $proposal->request->category['name'] }}
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        @if (Auth::user()->hasRole('vendor'))
                                            {{ $proposal->party2['first_name'] }}
                                            {{ $proposal->party2['last_name'] }}
                                        @else
                                            {{ $proposal->party1['first_name'] }}
                                            {{ $proposal->party1['last_name'] }}
                                        @endif
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $proposal->status }}
                                    </td>
                                    <td align="right" valign="middle" style="border-bottom: #000 1px solid;   ">AED
                                        {{ $proposal->items->sum('total') }}</td>
                                </tr>
                            @endforeach
                        @endif
                        @if ($contracts)
                            @foreach ($contracts as $contract)
                                <tr>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $loop->iteration }}</td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $contract->date }}
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $contract->proposal->request->category['name'] }}
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        @if (Auth::user()->hasRole('vendor'))
                                            {{ $contract->party1['first_name'] }}
                                            {{ $contract->party1['last_name'] }}
                                        @else
                                            {{ $contract->party2['first_name'] }}
                                            {{ $contract->party2['last_name'] }}
                                        @endif
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $contract->status }}
                                    </td>
                                    <td align="right" valign="middle" style="border-bottom: #000 1px solid;   ">AED
                                        {{ $contract->items->sum('total') }}</td>
                                </tr>
                            @endforeach
                        @endif
                        @if ($invoices)
                            @foreach ($invoices as $invoice)
                                <tr>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $loop->iteration }}</td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $invoice->date }}
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $invoice->contract->proposal->request->category['name'] }}
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        @if (Auth::user()->hasRole('vendor'))
                                            {{ $invoice->party2['first_name'] }}
                                            {{ $invoice->party2['last_name'] }}
                                        @else
                                            {{ $invoice->party1['first_name'] }}
                                            {{ $invoice->party1['last_name'] }}
                                        @endif
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $invoice->status }}
                                    </td>
                                    <td align="left" valign="middle"
                                        style="border-bottom: #000 1px solid; border-right: #000 1px solid; ">
                                        {{ $invoice->payment_status }}
                                    </td>
                                    <td align="right" valign="middle" style="border-bottom: #000 1px solid;   ">AED
                                        {{ $invoice->items->sum('total') }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                    <br>
                    <h5
                        style="color:#7A5C66; margin:0 0 10px; font-size:16px; font-family:Arial, Helvetica, sans-serif; ">
                        Note</h5>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
