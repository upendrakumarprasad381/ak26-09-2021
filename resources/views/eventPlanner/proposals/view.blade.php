@extends('layout.default')
@section('sub-header')
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/event/' . $event->id . '') }}" class="text-muted">{{ $event->event_name }}</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/event/' . $event->id . '/proposals') }}" class="text-muted">Proposals</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/event/' . $event->id . '/proposals/' . $proposal->id . '') }}"
            class="text-muted">{{ $page_title }}</a>
    </li>
@endsection
@section('content')
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <div class="container-fluid">
            <div class="__invoicWrp wd100">
                <div class="d-flex" style=" justify-content: flex-end;">
                    @if ($proposal->status != 'Accepted')
                        <button class="btn btn-primary pl-10 pr-10" id="btnAccept" type="button">
                            Accept
                        </button> &nbsp;
                        <a href="/event-planner/event/{{ $event->id }}/proposals/{{ $proposal->id }}/edit?negotiation=1"><button
                                class="btn btn-primary pl-10 pr-10" id="btnNegotiate" type="button">
                                Negotiate
                            </button></a> &nbsp;
                        <button class="btn btn-primary pl-10 pr-10" id="btnDecline" type="button">
                            Decline
                        </button>
                    @endif
                </div>
                <br>
                <div class="__viewIncWoz" style="width:800px; margin: 0 auto;">

                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"
                        style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
                        <tr>
                            <td width="70%" align="left" valign="middle">
                                <img src="/storage/{{ $proposal->party1->bussinessDetails['logo'] }}" width="120px" />
                            </td>
                            <td width="30%" align="left" valign="middle" style="font-size:14px; line-height:25px;">

                                <h3 style="font-size:18px; margin:0px 0px; padding:0 0;">
                                    {{ $proposal->party1->first_name }}
                                    {{ $proposal->party1->last_name }}</h3>
                                {{ $proposal->party1->bussinessDetails['address'] }}<br>
                                Date : {{ $proposal->date }}
                            </td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="10" style="margin-top:20px;border:#8A8A8A 1px solid;
                            font-family:Arial, Helvetica, sans-serif;
                            font-size:13px;
                            ">
                        <tr>
                            <th width="33%" align="left" valign="middle"
                                style="color:#7A5C66; border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                Events Details</th>
                            <th width="33%" align="left" valign="middle"
                                style="color:#7A5C66; border-bottom:#8A8A8A 1px solid;  border-right:#8A8A8A 1px solid;">
                                Event Planner Details</th>
                            <th width="33%" align="left" valign="middle"
                                style="color:#7A5C66; border-bottom:#8A8A8A 1px solid; ">Customer Details</th>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style=" border-right:#8A8A8A 1px solid;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="4" style="font-family:Arial, Helvetica, sans-serif;
                            font-size:13px;">
                                    <tr>
                                        <td width="35%"><strong>Event Name</strong></td>
                                        <td width="5%" style="color:#000">:</td>
                                        <td width="60%" style="color:#8A8A8A"> {{ $proposal->event['event_name'] }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Event Date </strong></td>
                                        <td style="color:#000">:</td>
                                        <td style="color:#8A8A8A">{{ $proposal->event['event_date'] }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Guest </strong></td>
                                        <td style="color:#000">:</td>
                                        <td style="color:#8A8A8A">{{ $proposal->event['number_of_attendees'] }}</td>
                                    </tr>
                                </table>
                            </td>
                            <td align="left" valign="top" style=" border-right:#8A8A8A 1px solid;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="4" style="font-family:Arial, Helvetica, sans-serif;
                            font-size:13px;">
                                    <tr>
                                        <td><strong>Name</strong></td>
                                        <td style="color:#000">:</td>
                                        <td style="color:#8A8A8A">{{ $proposal->party2['first_name'] }}
                                            {{ $proposal->party2['last_name'] }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Address </strong></td>
                                        <td style="color:#000">:</td>
                                        <td style="color:#8A8A8A">{{ $proposal->party2->bussinessDetails['address'] }}
                                        </td>
                                    </tr>
                                </table>

                            </td>
                            <td align="left" valign="top">
                                <table width="100%" border="0" cellspacing="0" cellpadding="4" style="font-family:Arial, Helvetica, sans-serif;
                            font-size:13px;">
                                    <tr>
                                        <td><strong>Name</strong></td>
                                        <td style="color:#000">:</td>
                                        <td style="color:#8A8A8A">@if($proposal->event['customer']) {{ $proposal->event['customer']['first_name'] }}
                                            {{ $proposal->event['customer']['last_name'] }} @endif</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Address </strong></td>
                                        <td style="color:#000">:</td>
                                        <td style="color:#8A8A8A">@if($proposal->event['customer'])  {{ $proposal->event['customer']['email'] }} @endif</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br /><br />


                    <h5 style="color:#7A5C66; margin:0 0 10px; font-size:16px; font-family:Arial, Helvetica, sans-serif; ">
                        Scope of work</h5>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5"
                        style=" border:#8A8A8A 1px solid; border-bottom:none;  font-family:Arial, Helvetica, sans-serif;  font-size:13px;  ">
                        <tr>
                            <th width="6%" align="center" valign="middle"
                                style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">Sl.No</th>
                            <th width="9%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">Item
                                Name</th>
                            <th width="9%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">Service
                                Type</th>
                            <th width="9%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">Sub
                                Service</th>

                            <th width="26%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                Description</th>
                            <th width="8%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                {{ $proposal->request->category['qty_label'] ?: 'Quantity' }}</th>
                            <th width="8%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">Amount
                            </th>
                            <th width="8%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">Tax
                            </th>
                            <th width="8%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">Remark
                            </th>
                            <th width="11%" style=" border-bottom:#8A8A8A 1px solid; ">Total</th>
                        </tr>
                        @foreach ($proposal->items as $key => $item)
                            <tr>
                                <td align="center" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $key + 1 }}</td>
                                <td align="center" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item['item_name'] }}</td>
                                <td align="center" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item->category['name'] }}</td>
                                <td align="center" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item->subCategory?$item->subCategory['name']:'' }}</td>
                                <td align="left" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item['description'] }}</td>
                                <td align="center" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item['quantity'] }}</td>
                                <td align="right" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item['amount'] }}</td>
                                <td align="center" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item['tax'] }}</td>
                                <td align="left" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item['remarks'] }}</td>
                                <td align="right" valign="middle" style=" border-bottom:#8A8A8A 1px solid;">AED
                                    {{ $item['total'] }}</td>
                            </tr>
                        @endforeach


                        <tr>
                            <td colspan="9" align="right" valign="middle"
                                style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;"><strong>Total
                                    Amount</strong></td>
                            <td align="right" valign="middle" style=" border-bottom:#8A8A8A 1px solid;  "><strong>AED
                                    {{ $proposal->items->sum('total') }}</strong></td>
                        </tr>
                    </table>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif;
                            font-size:12px;
                            margin-top:50px;
                            border-top: #8A8A8A 1px solid;
                            color:#000">
                        <tr>
                            <td style="padding-top:15px;"><strong>Terms and Conditions:</strong>
                                <ol style="    margin: 10px 14px;  padding: 0; ">
                                    @foreach (json_decode($proposal->terms_of_agreement) as $key => $terms)
                                        <li data-text="{{ $terms->text }}" data-row="{{ $key }}">
                                            {{ $terms->text }}
                                        </li>
                                    @endforeach
                                </ol>

                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--end::Container-->
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $("#btnAccept").click(function(event) {
            event.preventDefault();
            $("#btnSubmit").prop("disabled", true);
            Swal.fire({
                title: "Are you sure?",
                text: "You Want To Accept This Proposal.",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, Accept!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function(result) {
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "PATCH",
                        enctype: 'multipart/form-data',
                        url: "/event-planner/event/{{ $event->id }}/proposals/{{ $proposal->id }}",
                        data: {
                            editable: 0
                        },
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function(data) {
                            $("#btnAccept").prop("disabled", false);
                            setTimeout(function() {
                                location.replace(
                                    '/event-planner/event/{{ $event->id }}/proposals'
                                );
                            }, 1000);
                        },
                        error: function(e) {
                            $("#btnAccept").prop("disabled", false);
                        }
                    });
                } else if (result.dismiss === "cancel") {
                    // Swal.fire(
                    //     "Cancelled",
                    //     "",
                    //     "error"
                    // )
                }
            });
        });
    </script>
@endsection
