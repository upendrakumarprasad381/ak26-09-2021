{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')

@section('sub-header')
<?php
$event = \App\Helpers\LibHelper::GetvendoreventsById($eventId);
?>
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/event/' . $eventId . '') }}" class="text-muted">{{ $event->event_name }}</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="<?= url("vendor-manager/$eventId/view-all-request") ?>" class="text-muted">Vendor Manager</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="" class="text-muted">Vendor List</a>
</li>
@endsection
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->

        <form method="get">
            <div class="row __cmFltTp __lddelbk mb-10"> 

                <div class="col-lg-4 col-md-4 col-sm-12 mb-3">
                    <label class="form-label">Search</label>
                    <input name="search" type="text" required class="form-control">
                </div>

                <div class="col-md-auto">
                    <label class="form-label wd100 __cmoemynone">&nbsp; </label>

                    <button type="submit" class="btn btn-secondary">Search</button>
                </div>





            </div>
        </form>


        <?php
        $condSql2 = "";
        $catAr = App\Helpers\LibHelper::GetcategoriesById($categoryId);

        if (isset($_GET['search'])) {
            $condSql2 = $condSql2 . " AND  U.first_name LIKE '%" . $_GET['search'] . "%'";
        }
        ?>
        <div class="wd100 __eBk __djs  __listviewevMg">
            <h2><?= !empty($catAr->name) ? $catAr->name : '' ?></h2>
            <div class="row">
                <?php
                $baseDir = Config::get('constants.HOME_DIR');

                $join = "LEFT JOIN users U ON U.id=VC.user_id LEFT JOIN role_user RU ON RU.user_id=U.id LEFT JOIN vendor_details VD ON VD.user_id=VC.user_id";
                $Sql = "SELECT VC.*,U.first_name,U.profile_picture,VD.whatsapp  FROM `vendor_category` VC  $join WHERE VC.`category_id` = $categoryId AND RU.role_id='2' AND U.parent_id IS NULL AND U.status=0 $condSql2";
                $ulist = \App\Database::select($Sql);
                if (!empty($ulist)) {
                    for ($j = 0; $j < count($ulist); $j++) {
                        $d = $ulist[$j];

                        $baseFile = "storage/" . $d->profile_picture;
                        if (is_file($baseDir . $baseFile)) {
                            $d->profile_picture = url($baseFile);
                        } else {
                            $d->profile_picture = url(Config::get('constants.DEFAULT_LOGO_VENDOR'));
                        }
                        $url = url('vendor-manager/vendor-details/' . $d->user_id . '/' . $categoryId . '/' . $eventId);
                        $attr = "url='$url'";
                        $Sql = "SELECT * FROM `event_planner_request` WHERE event_id='$eventId' AND vendor_id='$d->user_id' AND category_id='$categoryId'";
                        $data = \App\Database::selectSingle($Sql);
                        ?>
                        <div class="col-lg-3 col-md-4 col-sm-6 mb-7"> 
                            <div class="wd100 __eBkBz"> 
                                <a href="{{$url}}">
                                    <div class=" __eBkBzImg wd100">
                                        <img class="img-fluid" src="<?= $d->profile_picture ?>">
                                    </div>
                                </a>
                                <div class="__eBkBzDcrp wd100 p-4">
                                    <h5 class="text-center "><a href="{{$url}}"><?= $d->first_name ?></a></h5>
                                    <div class="wd100 __drosmllboz text-center">
                                        
                                        <div class="__eBkBzDrop __bth50">
                                            <button type="button" class="btn btn-secondary btn-sm ml-1 mr-1"><?= empty($data) ? '<a ' . $attr . ' onclick="sendrRFQ(this);" href="javascript:void(0)">Send RFQ</a>' : (!empty($data->proposal_status) ? 'Proposal created' : App\Helpers\CommonHelper::eventPlannerEventstatus($data->status)) ?></button>
                                            <div class="__bntWhts" title="Whatsapp"><a target="_blank" href="https://web.whatsapp.com/send?phone=<?= $d->whatsapp ?>&text=Hello"><img src="<?= url('user/images/whatsapp_w.svg') ?>"></a> </div>                                        
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                } else {
                    ?><h5 class="text-center" style="margin-left: 49px;padding: 20px;">data not found.</h5><?php
                }
                ?>
            </div>
        </div>











        <!--end::Container-->
    </div>
</div>
<!--begin::Entry-->

<modalPopup></modalPopup>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
    function sendrRFQ(e) {
        var data = {
            _token: "{{ csrf_token() }}",
            showRFQModal: true,
        };
        $.ajax({
            url: $(e).attr('url'),
            cache: false,
            data: data,
            async: false,
            type: 'POST',
            success: function (res) {
                res = jQuery.parseJSON(res);
                if (res.status == true) {
                    $('modalPopup').html(res.HTML);
                    $('#exampleModal').modal('show');
                }
            }
        });
    }
     function  sendRFQ() {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var json = {};
        json['vendor_id'] = $('#vendor_id').val();
        json['event_id'] = $('#event_id').val();
        json['category_id'] = $('#category_id').val();


        json['event_time'] = $('#start_time').val();
        json['number_of_hours'] = $('#no_of_hours').val();


        json['budget'] = $('input[name="budget"]:checked').attr('minbudget');
        json['max_budget'] = $('input[name="budget"]:checked').attr('maxbudget');
        json['video_chat'] = $('input[name="video_chat"]:checked').val();
        json['notes'] = $('#rfq_quick_note').val();
//        if ($('#budget').val() == '') {
//            $('#budget').focus();
//            return false;
//        }
        if ($('#rfq_quick_note').val() == '') {
            $('#rfq_quick_note').focus();
            return false;
        }
        var data = {_token: CSRF_TOKEN, json: json};
        $.ajax({
            type: "POST",
            url: '/vendor-manager/saveRFQ',
            data: data,
            success: function (msg) {
                $('#exampleModal').modal('hide');
                location.replace("{{url('/')}}/vendor-manager/" + json['event_id'] + '/view-all-request');
            }
        });
    }
</script>
@endsection
