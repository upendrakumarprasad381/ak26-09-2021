{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

<?php
$message = "";
if (isset($_POST['submitreviews'])) {
    $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
    $json['created_at'] = date('Y-m-d H:i:s');
    $json['updated_at'] = $json['created_at'];
    App\Database::insert('user_reviews', $json);
    $message = "thank you for your support";
}
if (isset($_POST['contacsussubmit'])) {
    $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
    $json['created_at'] = date('Y-m-d H:i:s');
    $json['updated_at'] = $json['created_at'];
    App\Database::insert('user_contact_us', $json);
    $message = "thank you for your enquiry";
}




$event = \App\Helpers\LibHelper::GetvendoreventsById($eventId);

$Sql = "SELECT * FROM `event_planner_request` WHERE event_id='$eventId' AND vendor_id='$vendorId' AND category_id='$categoryId'";
$alreadySend = \App\Database::selectSingle($Sql);

$vendor = App\Helpers\LibHelper::GetusersBy($vendorId);
$vendorDetails = App\Helpers\LibHelper::GetvendordetailsBy($vendorId);
if (empty($event) || empty($vendorDetails)) {
    echo 'Invalid Url';
    exit;
}
?>

@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/event/' . $eventId . '') }}" class="text-muted">{{ $event->event_name }}</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="<?= url("vendor-manager/$eventId/view-all-request") ?>" class="text-muted">Vendor Manager</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="" class="text-muted">Vendor Details</a>
</li>
@endsection
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">

        <?php if (!empty($message)) { ?>
            <div class="alert alert-success" role="alert">
                {{$message}}
            </div>
        <?php } ?>

       	<div class="__evmTpBr __enFrInBk">



            <div class="wd100">
                <div class="row">

                    <div class="col-lg-9 col-md-8 col-sm-12">
                        <div class="card card-custom __evDpgPg">

                            <ul class="nav nav-bold nav-pills  wd100">
                                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_7_1">About</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#kt_tab_pane_7_2">Photos</a></li>
                                <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#kt_tab_pane_7_4">Reviews</a></li>
                                <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#kt_tab_pane_7_5">Contact</a></li>
                                <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#kt_tab_pane_7_6">Team</a></li>
                            </ul>


                            <div class="tab-content">

                                <div class="tab-pane fade active show" id="kt_tab_pane_7_1" role="tabpanel" aria-labelledby="kt_tab_pane_7_1">
                                    <div class="wd100 __evpabcotWboz">
                                        <div class="wd100 __evpabbzinfo">
                                            <h4> <?= $vendor->first_name . ' ' . $vendor->last_name ?>
                                                <div class="__epd_wishliico">
                                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                                </div>
                                            </h4>
                                            <div class="wd100 __epdloction">
                                                <?= !empty($vendorDetails->address) ? $vendorDetails->address : 'location not updated' ?>
                                            </div>
                                        </div>


                                        <div class="wd100 __epdsAbout">
                                            <h2>About</h2>
                                            <p><?= !empty($vendorDetails->about) ? $vendorDetails->about : 'About us not updated by vendor.' ?></p>
                                        </div>

                                        <!--                                        <div class="wd100 __epdsDetails">
                                                                                    <h3>Details</h3>
                                                                                    <p>Fashion Services</p>
                                                                                    <h3>Dry Cleaning and Preservations</h3>
                                                                                </div>-->

                                    </div>

                                </div>

                                <div class="tab-pane fade" id="kt_tab_pane_7_2" role="tabpanel" aria-labelledby="kt_tab_pane_7_2">

                                    <div class="__ptolibzWr wd100">

                                        <div class="gallery clear">
                                            <?php
                                            $Sql = "SELECT * FROM `vendor_images` WHERE user_id='$vendorId'";
                                            $img = App\Database::select($Sql);
                                            $baseDir = Config::get('constants.HOME_DIR');
                                            if (!empty($img)) {
                                                for ($i = 0; $i < count($img); $i++) {
                                                    $d = $img[$i];
                                                    $baseFile = "storage/" . $d->image;
                                                    if (is_file($baseDir . $baseFile)) {
                                                        $d->image = url($baseFile);
                                                    } else {
                                                        $d->image = url(Config::get('constants.DEFAULT_LOGO_VENDOR'));
                                                    }
                                                    ?>
                                                    <div class="bg" style="background-image: url(<?= $d->image ?>);"></div>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <div class="bg" style="background-image: url(https://demo.softwarecompany.ae/ask_deema/images/light_4.jpg);"></div>
                                                <div class="bg" style="background-image: url(https://demo.softwarecompany.ae/ask_deema/images/light_5.jpg);"></div>
                                                <div class="bg" style="background-image: url(https://demo.softwarecompany.ae/ask_deema/images/light_6.jpg);"></div>
                                                <div class="bg" style="background-image: url(https://demo.softwarecompany.ae/ask_deema/images/light_7.png);"></div>
                                                <div class="bg" style="background-image: url(https://demo.softwarecompany.ae/ask_deema/images/light_4.jpg);"></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>



                                <div class="tab-pane fade " id="kt_tab_pane_7_4" role="tabpanel" aria-labelledby="kt_tab_pane_7_4">
                                    <div class="wd100 __rateExperience">

                                        <h2>Rate your experience</h2>
                                        <div class="wd100 __starRate">
                                            <div class="___staricon">
                                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                            </div>

                                        </div>

                                        <div class="wd100 __write_review">

                                            <form method="post">
                                                @csrf
                                                <div class="row">
                                                    <input type="hidden" name="json[user_id]" value="<?= Auth::user()->id ?>">
                                                    <input type="hidden" name="json[vendor_id]" value="<?= $vendorId ?>">
                                                    <div class="mb-3 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <label for="exampleFormControlInput1" class="form-label">Name</label>
                                                        <input type="text" class="form-control" value="<?= Auth::user()->first_name ?>" name="json[name]" required placeholder="">
                                                    </div>

                                                    <div class="mb-3 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <label for="exampleFormControlInput1" class="form-label">Email</label>
                                                        <input type="email" class="form-control" value="<?= Auth::user()->email ?>" name="json[email]" required placeholder="">
                                                    </div>

                                                    <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <label for="exampleFormControlInput1" class="form-label">Comment</label>
                                                        <textarea class="form-control" rows="3" name="json[comment]" required placeholder=""></textarea>
                                                    </div>

                                                    <div class="mb-3 mt-2 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <button type="submit" name="submitreviews" class="btn btn-secondary">Submit</button>
                                                    </div>

                                                </div>
                                            </form>


                                            <!--                                            <div class="wd100">

                                                                                            <div class="d-flex __wrirewInfo align-items-center">
                                                                                                <div class="flex-shrink-0">
                                                                                                    <img src="https://demo.softwarecompany.ae/ask_deema/images/male-avatar.jpg">
                                                                                                </div>
                                                                                                <div class="flex-grow-1  __dcrpgltetr">
                                                                                                    <h6>John Martin</h6>
                                                                                                    <span>johnmartin345@gmail.com</span>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>

                                                                                        <div class="wd100">
                                                                                            <div class="wd100 __starRate">
                                                                                                <div class="___staricon">
                                                                                                    <a href="#"><i class="fa fa-star __acv" aria-hidden="true"></i></a>
                                                                                                    <a href="#"><i class="fa fa-star __acv" aria-hidden="true"></i></a>
                                                                                                    <a href="#"><i class="fa fa-star __acv" aria-hidden="true"></i></a>
                                                                                                    <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                                                    <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                                                </div>
                                                                                                <div class="__starRtvlu">Review On Monday 12/09/21</div>
                                                                                            </div>

                                                                                            <div class="wd100 ___rulreviewWrp">
                                                                                                <div class=" __rulreview">
                                                                                                    <p>Lorem Ipsum Dolor Sit Amet, Consetetur Sadipscing Elitr, Sed Diam Nonumy Eirmod Tempor Invidunt Ut Labore Et Dolore Magna Aliquyam Erat, Sed Diam Voluptua. At Vero Eos Et Accusam Et Justo Duo Dolores Et Ea Rebum. Stet Clita Kasd Gubergren, No Sea Takimata Sanctus Est Lorem Ipsum Dolor Sit Amet. </p>
                                                                                                </div>

                                                                                                <div class="__rulreview_bl2">
                                                                                                    <h5>Response From Vendor on 12/09/21</h5>
                                                                                                    <p>Lorem Ipsum Dolor Sit Amet, Consetetur Sadipscing Elitr, Sed Diam Nonumy Eirmod Tempor Invidunt Ut Labore Et Dolore Magna Aliquyam Erat, Sed Diam Voluptua. At Vero Eos Et Accusam Et Justo Duo Dolores Et Ea Rebum. Stet Clita Kasd Gubergren, No Sea Takimata Sanctus Est Lorem Ipsum Dolor Sit Amet. Lorem Ipsum Dolor Sit Amet, Consetetur Sadipscing Elitr,</p>
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>-->



                                        </div>

                                    </div>
                                </div>

                                <div class="tab-pane fade " id="kt_tab_pane_7_5" role="tabpanel" aria-labelledby="kt_tab_pane_7_5">
                                    <div class="wd100 __epd_contact">
                                        <div class="wd100 __epd_contact_boz">
                                            <h3><?= $vendor->first_name . ' ' . $vendor->last_name ?></h3>

                                            <div class="wd100 d-flex __liloctnWrp">
                                                <div class="flex-shrink-0"> <img src="" class="mr-2"> </div>
                                                <div class="flex-grow-1  "> <a href="javascript:void(0)"> <?= !empty($vendorDetails->address) ? $vendorDetails->address : 'location not updated' ?></a> </div>
                                            </div>

                                            <div class="wd100 d-flex __liloctnWrp __licallWrp">
                                                <div class="flex-shrink-0"> <img src="https://demo.softwarecompany.ae/ask_deema/images/call_icon.svg" class="mr-2"> </div>
                                                <div class="flex-grow-1 "> <a href="tel:+<?= !empty($vendor->phone) ? $vendor->phone : '' ?>">+<?= !empty($vendor->phone) ? $vendor->phone : '' ?></a> </div>
                                            </div>

                                            <div class="wd100 d-flex __liloctnWrp __licallWrp">
                                                <div class="flex-shrink-0"> <img src="https://demo.softwarecompany.ae/ask_deema/images/call_icon.svg" class="mr-2"> </div>
                                                <div class="flex-grow-1 "> <a href="tel:+<?= !empty($vendor->phone_office) ? $vendor->phone_office : '' ?>">+<?= !empty($vendor->phone_office) ? $vendor->phone_office : '' ?></a> </div>
                                            </div>
                                        </div>
                                        <div class="wd100 __epd_contact_froBz __write_review ">
                                            <form method="post">
                                                @csrf
                                                <div class="row">
                                                    <input type="hidden" name="json[vendor_id]" value="<?= $vendorId ?>">
                                                    <div class="mb-3 col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                        <label for="exampleFormControlInput1" class="form-label">Name</label>
                                                        <input type="text" class="form-control" value="<?= Auth::user()->first_name ?>" required name="json[name]" placeholder="">
                                                    </div>
                                                    <div class="mb-3 col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                        <label for="exampleFormControlInput1" class="form-label">Email</label>
                                                        <input type="email" class="form-control" value="<?= Auth::user()->email ?>" required name="json[email]" placeholder="">
                                                    </div>
                                                    <div class="mb-3 col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                        <label for="exampleFormControlInput1" class="form-label">Phone</label>
                                                        <input type="number" class="form-control" value="<?= Auth::user()->phone ?>" required name="json[phone]" placeholder="">
                                                    </div>


                                                    <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <label for="exampleFormControlInput1" class="form-label">Comment</label>
                                                        <textarea class="form-control" rows="3" required name="json[comment]" placeholder=""></textarea>
                                                    </div>

                                                    <div class="mb-4 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <button type="submit" name="contacsussubmit" class="btn btn-secondary float-end">Submit</button>
                                                    </div>

                                                </div>
                                            </form>

                                        </div>


                                    </div>
                                </div>

                                <div class="tab-pane fade " id="kt_tab_pane_7_6" role="tabpanel" aria-labelledby="kt_tab_pane_7_6">
                                    <div class="wd100  __epdteamWrp">

                                        <div class="__rluLGrid row row-cols-2 row-cols-sm-3 row-cols-md-4 row-cols-lg-6">
                                            <?php
                                            $baseDir = Config::get('constants.HOME_DIR');
                                            $Sql = "SELECT first_name,last_name,profile_picture FROM `users`  WHERE parent_id='$vendorId'";
                                            $team = App\Database::select($Sql);
                                            if (!empty($team)) {
                                                for ($i = 0; $i < count($team); $i++) {
                                                    $d = $team[$i];

                                                    $baseFile = "storage/" . $d->profile_picture;
                                                    if (is_file($baseDir . $baseFile)) {
                                                        $d->profile_picture = url($baseFile);
                                                    } else {
                                                        $d->profile_picture = url(Config::get('constants.DEFAULT_LOGO_VENDOR'));
                                                    }
                                                    ?>
                                                    <div class="__teaminfoBz">
                                                        <img class="img-fluid" src="<?= $d->profile_picture ?>">
                                                        <h6><?= $d->first_name . ' ' . $d->last_name ?></h6>
                                                    </div>
                                                <?php
                                                }
                                            } else {
                                                ?><div class="__teaminfoBz"><h6>Team not found.</h6></div><?php }
                                            ?>


                                        </div>

                                    </div>
                                </div>

                            </div>



                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-sm-12">
                        <div class="wd100 __evpRtewgtInfoB">
                            <?php if (empty($alreadySend)) { ?>
                                <a href="javascript:void(0)"  class="__bntCscy __bntsendfq"  onclick="showRFQModal();" >Send RFQ</a>
                            <?php } else {
                                ?> <a href="javascript:void(0)"  class="__bntCscy __bntsendfq"   >Already Sent</a><?php
                            }
                            ?>
                            <a href="/conversations" target="_blank"  class="__bntCpry __bntsendfq">
                                Contact Vendor
                            </a>

                            <div class="__bntWhts" title="Whatsapp">
                                <a target="_blank" href="https://web.whatsapp.com/send?phone=<?= $vendorDetails->whatsapp ?>text=Hello">
                                    <img src="<?= url('user/images/whatsapp_w.svg') ?>">  Whatsapp
                                </a>
                            </div>

                        </div>

                    </div>

                </div>
            </div>










        </div>

    </div>
</div>

<!-- Button trigger modal -->


<!-- Modal -->
<?=App\Helpers\HtmlHelper::showRFQModal($vendorId, $categoryId, $eventId); ?>


@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
    $("input[name='venue_picked']").change(function () {
        var val = $(this).val();
        if (val == 'Yes, I have picked a venue') {
            $('#venue_name_div').show();
        } else {
            $('#venue_name_div').hide();
        }
    });
    function showRFQModal() {
        $('#exampleModal').modal('show');
    }
    function  sendRFQ() {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var json = {};
        json['vendor_id'] = $('#vendor_id').val();
        json['event_id'] = $('#event_id').val();
        json['category_id'] = $('#category_id').val();

        json['event_date'] = $('#rfq_wedding_date').val();
        json['event_time'] = $('#start_time').val();
        json['number_of_hours'] = $('#no_of_hours').val();


        json['budget'] = $('input[name="budget"]:checked').attr('minbudget');
        json['max_budget'] = $('input[name="budget"]:checked').attr('maxbudget');
        json['video_chat'] = $('input[name="video_chat"]:checked').val();
        json['notes'] = $('#rfq_quick_note').val();
//        if ($('#budget').val() == '') {
//            $('#budget').focus();
//            return false;
//        }
        if ($('#rfq_quick_note').val() == '') {
            $('#rfq_quick_note').focus();
            return false;
        }
        var data = {_token: CSRF_TOKEN, json: json};
        $.ajax({
            type: "POST",
            url: '/vendor-manager/saveRFQ',
            data: data,
            success: function (msg) {
                $('#exampleModal').modal('hide');
                location.replace("{{url('/')}}/vendor-manager/" + json['event_id'] + '/view-all-request');
            }
        });
    }
</script>
@endsection
