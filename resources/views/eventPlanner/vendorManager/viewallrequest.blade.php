{{-- Extends layout --}}
@extends('layout.default')

@section('sub-header')
<?php
  $event = \App\Helpers\LibHelper::GetvendoreventsById($eventId);
?>
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/event/' . $eventId . '') }}" class="text-muted">{{$event->event_name}}</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="" class="text-muted">Vendor Manager</a>
</li>
@endsection

{{-- Content --}}
@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->

        <?php

        $Sql = "SELECT VEC.*,C.name AS cat_name FROM `vendor_event_category` VEC LEFT JOIN categories C ON C.id=VEC.category_id WHERE VEC.vendor_event_id='$eventId'";
        $cateGroup = App\Database::select($Sql);
        if (!empty($cateGroup)) {
            for ($k = 0; $k < count($cateGroup); $k++) {
                $r = $cateGroup[$k];
                ?>
                <div class="wd100 __minTilbz">
                    <h2><?= $r->cat_name ?>

                    </h2>
                </div>
                <div class="wd100 __eBk __djs">
                    <div class="row">

                        <div class="col-lg-3 col-md-4 col-sm-6 mb-5">
                            <a href="<?= url("vendor-manager/$eventId/$r->category_id/get-vendor") ?>"  class="__cateringBozAdd">
                                <i class=" flaticon-plus"></i> <br/>
                                Get a <?= $r->cat_name ?>
                            </a>
                        </div>


                        <?php
                        $baseDir = Config::get('constants.HOME_DIR');


                        $select = ",U.first_name,U.profile_picture,VD.whatsapp";
                        $join = "LEFT JOIN users U ON U.id=EPR.vendor_id LEFT JOIN vendor_details VD ON VD.user_id=EPR.vendor_id";
                        $Sql = "SELECT EPR.* $select FROM `event_planner_request` EPR $join WHERE EPR.event_id='$eventId' AND EPR.category_id='$r->category_id'";
                        $pdata = \App\Database::select($Sql);

//                        echo '<pre>';
//                        print_r($pdata);
//                        echo '</pre>';
                        if (!empty($pdata)) {

                            for ($i = 0; $i < count($pdata); $i++) {
                                $d = $pdata[$i];

                                $baseFile = "storage/" . $d->profile_picture;
                                if (is_file($baseDir . $baseFile)) {
                                    $d->profile_picture = url($baseFile);
                                } else {
                                    $d->profile_picture = url(Config::get('constants.DEFAULT_LOGO_VENDOR'));
                                }


                                $url = url('vendor-manager/vendor-details/' . $d->vendor_id . '/' . $d->category_id . '/' . $eventId);
                                ?>
                                <div class="col-lg-3 col-md-4 col-sm-6 mb-5">
                                    <div class="wd100 __eBkBz">
                                        <div class=" __eBkBzImg wd100">
                                            <a href="<?= $url ?>"> <img class="img-fluid" src="<?= $d->profile_picture ?>"></a>
                                        </div>
                                        <div class="__eBkBzDcrp wd100 p-4">
                                            <h5 class="text-center "><a href="<?= $url ?>"><?= $d->first_name ?></a></h5>
                                            <div class="wd100 __drosmllboz text-center">
                                                <div class="__eBkBzDrop">


                                                    <span style="cursor: inherit;"
                                                class="__unlinkStatus btn @if($d->status == 2) btn-outline-success @elseif($d->status == 3) btn-outline-danger  @else btn-outline-primary @endif  __unlinkStatus btn-sm __drosml_Awaiting wd100 "> @if($d->status == 2) RFQ Accepted @elseif($d->status == 3) RFQ Rejected  @else Awating Quotation @endif</span>


                                                </div>
                                            </div>
                                            <div class="wd100 __rstsumry text-center">
                                                Request sent on <?= date('d M-Y', strtotime($d->created_at)) ?>
                                            </div>
                                            <div class="wd100">
                                                <div class="__eBkBzDrsecAct __bth50vewlrqt">
                                                    <button receiverId='<?= $d->vendor_id ?>' msg='Remider From <?= Auth::user()->first_name ?> for <?= $event->event_name ?> (RFQ Remider)' onclick="sendReminderallrequest(this);" style="width: 100%;" type="button" class="btn __sentReminder btn-primary mr-1"><i class="fa fa-bell" aria-hidden="true"></i> </button>

                                                    <div class="__bntWhts" title="Whatsapp"><a target="_blank" href="https://web.whatsapp.com/send?phone=<?= $d->whatsapp ?>&text=Hello"><img src="https://askdeema.granddubai.com/user/images/whatsapp_w.svg"> </a></div>


                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        } else {
                            ?> <h2 style="font-size: 16px;text-align: center;display: none;margin-left: 30%;">No request found.</h2><?php
                        }
                        ?>
                    </div>
                </div>


                <?php
            }
        } else {
            ?> <h2 style="font-size: 16px;text-align: center;">Please update your event category services.<br>No event category found</h2><?php
            }
            ?>


    </div>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
    function sendReminderallrequest(e) {
        var receiverId = $(e).attr('receiverId');
        var msg = $(e).attr('msg');
        var data = {sendReminderallrequest: true, receiverId: receiverId, msg: msg};
        Swal.fire({
            title: "Send Reminder",
            text: "Are you sure want to send reminder?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes",
            cancelButtonText: "No, cancel!",
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    data: data,
                    url: '',
                    type: 'POST',
                    async: false,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function (data) {
                        Swal.fire(
                                "Reminder RFQ",
                                "Successfully sent.",
                                "success"
                                )
                        setTimeout(function () {
                            //location.reload();
                        }, 1000);
                    },
                    error: function (e) {
                    }
                });
            } else if (result.dismiss === "cancel") {
                Swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                        )
            }
        });
    }
</script>
@endsection
