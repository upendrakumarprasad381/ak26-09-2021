<div id="kt_header" class="header {{ Metronic::printClasses('header', false) }}"
    {{ Metronic::printAttrs('header') }}>

    <div class="container-fluid d-flex align-items-center justify-content-between">
        @role('vendor|event_planner|shop_admin|user')
        <!--begin::Header Menu Wrapper-->
        <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
            <!--begin::Header Menu-->
            <div id="kt_header_menu" class="__headerCmMen header-menu header-menu-mobile header-menu-layout-default">

                <!--begin::Header Nav-->
                <ul class="menu-nav">

                    <li class="menu-item">
                        <a href="/" class="menu-link">
                            <span class="menu-text">Planning Tools</span>
                        </a>
                        {{-- <div class="menu-submenu menu-submenu-classic menu-submenu-left">
                            <ul class="menu-subnav">



                                <li class="menu-item " aria-haspopup="true">
                                    <a href="<?= url('customer-login') ?>" class="menu-link">
                                        <span class="menu-text">Budgeter</span>
                                    </a>
                                </li>

                                <li class="menu-item " aria-haspopup="true">
                                    <a href="<?= url('customer-login') ?>" class="menu-link">
                                        <span class="menu-text">Vendor Manager</span>
                                    </a>
                                </li>
                                <li class="menu-item  " aria-haspopup="true">
                                    <a href="<?= url('customer-login') ?>" class="menu-link">
                                        <span class="menu-text">Events</span>
                                    </a>
                                </li>

                                <li class="menu-item " aria-haspopup="true">
                                    <a href="<?= url('customer-login') ?>" class="menu-link">
                                        <span class="menu-text">Conversations</span>
                                    </a>
                                </li>

                                <li class="menu-item  " aria-haspopup="true">
                                    <a href="<?= url('customer-login') ?>" class="menu-link">
                                        <span class="menu-text">Invoices/Contracts</span>
                                    </a>
                                </li>



                            </ul>

                        </div> --}}


                    </li>

                    <li class="menu-item">
                        <a href="<?= url('/vendors-list') ?>" class="menu-link">
                            <span class="menu-text">Vendors </span>
                        </a>
                        {{-- <div class="menu-submenu menu-submenu-classic menu-submenu-left">
                            <ul class="menu-subnav">


                                <?php
                                $servc = App\Helpers\LibHelper::GetvendorCategory();
                                for ($i = 0; $i < count($servc); $i++) {
                                    $d = $servc[$i];
                                    ?>
                                <li class="menu-item" aria-haspopup="true"><a class="menu-link"
                                        href="<?= url("/vendors-list?catId=$d->id") ?>"><span class="menu-text"><?= $d->name ?></span></a></li>
                                <?php
                                }
                                ?>

                                <li class="menu-item" aria-haspopup="true">
                                    <a href="<?= url("/vendors-list?event-planer=1") ?>" class="menu-link">
                                        <span class="menu-text">Event Planners</span>
                                    </a>
                                </li>



                            </ul>

                        </div> --}}
                    </li>

                    <li class="menu-item">
                        <a href="<?= url('/shop') ?>" class="menu-link">
                            <span class="menu-text">Shop</span>
                        </a>
                    </li>

                    <li class="menu-item">
                        <a href="/" class="menu-link">
                            <span class="menu-text">Ask Deema Services</span>
                        </a>
                    </li>


                    <li class="menu-item">
                        <a href="/" class="menu-link">
                            <span class="menu-text">Blog</span>
                        </a>
                    </li>


                    <li class="menu-item">
                        <a href="<?= url('about-us') ?>" class="menu-link">
                            <span class="menu-text">About Us</span>
                        </a>
                    </li>

                    <li class="menu-item">
                        <a href="<?= url('vendors-portal') ?>" class="menu-link">
                            <span class="menu-text">Vendors Portal</span>
                        </a>
                    </li>



                </ul>
                <!--end::Header Nav-->
            </div>
            <!--end::Header Menu-->
        </div>
        <!--end::Header Menu Wrapper-->
        @endrole
        @role('superadministrator')
        @if (config('layout.header.self.display'))

            @php
                $kt_logo_image = 'logo-light.png';
            @endphp

            @if (config('layout.header.self.theme') === 'light')
                @php $kt_logo_image = 'logo-dark.png' @endphp
            @elseif (config('layout.header.self.theme') === 'dark')
                @php $kt_logo_image = 'logo-light.png' @endphp
            @endif

            <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                @if (config('layout.aside.self.display') == false)
                    <div class="header-logo">
                        <a href="{{ url('/') }}">
                            <img alt="Logo" src="{{ asset('media/logos/' . $kt_logo_image) }}" />
                        </a>
                    </div>
                @endif

                <div id="kt_header_menu"
                    class="header-menu header-menu-mobile {{ Metronic::printClasses('header_menu', false) }}"
                    {{ Metronic::printAttrs('header_menu') }}>
                    <ul class="menu-nav {{ Metronic::printClasses('header_menu_nav', false) }}">
                        {{ Menu::renderHorMenu(config('menu_header.items')) }}
                    </ul>
                </div>
            </div>

        @else
            <div></div>
        @endif

        @include('layout.partials.extras._topbar')
        @endrole
    </div>
</div>
