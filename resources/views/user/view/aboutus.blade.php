@include('user.view.includes.header')

<?php
$CONTACT_US_DETAILS = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'CONTACT_US_DETAILS');
?>


<section class="inner-banner wd100">
    <div class="breadcrumb-area" style="background-image: url(<?= url('/img/vendors-bg.jpg') ?>)" data-overlay="dark"
         data-opacity="7">
        <div class="container pt-150 pb-150 position-relative">
            <div class="row justify-content-center">
                <div class="col-xl-12">
                    <div class="breadcrumb-title">

                        <h3 class="title">Creative & Elegant Event<br>
                            Design & planning.</h3>
                        <span class="sub-title">Showcase your signature services set to go.</span>
                    </div>
                </div>
            </div>
            <div class="breadcrumb-nav">
                <ul>
                    <li><a href="<?= url('/') ?>">Home</a></li>
                    <li class="active">About us</li>
                </ul>
            </div>
            <div class="social-links">
                <ul class="social-icon-three">


                    <li><a href="https://www.facebook.com/Ask-Deema-100964511965038/"><i class="fab fab-facebook-f"></i></a></li>


                    <li><a href="https://twitter.com/askdeema"><i class="fab fab-twitter"></i></a></li>


                    <li><a href="https://www.instagram.com/askdeema/"><i class="fab fab-instagram"></i></a></li>


                    <li><a href="https://www.linkedin.com/company/askdeema"><i class="fab fab-linkedin-in"></i></a></li>

                </ul>
            </div>
        </div>
    </div>
</section>

<!-- ABOUT -->

<!-- END ABOUT -->
<section class="about-one">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section-title-5 text-center mb-60">
                    <h3 class="title">About Us</h3>
                </div>
            </div>
            <div class="col-lg-6 d-flex justify-content-end">
                <div class="about-one__image-block">

                    <div class="inner wow fadeInUp animated" data-wow-delay="200ms" data-wow-duration="1200ms" style="visibility: visible; animation-duration: 1200ms; animation-delay: 200ms; animation-name: fadeInUp;">

                        <img src="https://demo.softwarecompany.ae/ask_deema/images/gallery/about-1-1.jpg" class="img-fluid about-one__image" alt="Awesome Image" />

                    </div><!-- /.inner -->
                </div><!-- /.about-one__image-block -->
            </div><!-- /.col-lg-6 -->




            <div class="col-lg-6 d-flex">
                <div class="about-one__content my-auto">
                    <div class="block-title">
                        <h2 class="block-title__title text-capitalize">Our Story</h2>
                        <p class="block-title__tag-line block-title__tag-line-has-line text-capitalize">Where it all began</p>

                        <!-- /.block-title__title -->
                    </div><!-- /.block-title -->
                    <p class="about-one__text">Deema and Abdullah started working on AskDeema platform in July 2019. The concept they worked on was to find a platform to ease the event planning experience to make it a better and enjoyable one. They made AskDeema as a platform to be a one stop shop for events planning, basically they connected the event planner or event enthusiast with all the vendors, shops, and planning tools required for him/her to make the event happen. Deema here is represented as the cool aunt who’s experienced enough to give you the right advice and cool enough that you would go to her for help..</p><!-- /.about-one__text -->

                </div><!-- /.about-one__content -->
            </div><!-- /.col-lg-6 -->

        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.about-one -->



<!--Start Team Area-->
<section class="team-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section-title-5 text-center mb-60">
                    <h3 class="title-meet">Meet Our Team</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <!--Start Single Team Member-->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="single-team-member">
                    <div class="img-holder">
                        <img src="https://demo.softwarecompany.ae/ask_deema/images/gallery/team-01.jpg" alt="Awesome Image">
                        <!-- <ul class="sociallinks">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                        </ul> -->
                        <div class="overlay">
                            <div class="box">
                                <div class="link">
                                    <a class="btn-two" href="#">View Profile<span class="flaticon-next"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="name text-center">
                        <p><span>CEO & Founder</span></p>
                        <h3>Alison Fletcher</h3>
                    </div>
                </div>
            </div>
            <!--End Single Team Member-->
            <!--Start Single Team Member-->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="single-team-member">
                    <div class="img-holder">
                        <img src="https://demo.softwarecompany.ae/ask_deema/images/gallery/team-02.jpg" alt="Awesome Image">
                        <!-- <ul class="sociallinks">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                        </ul> -->
                        <div class="overlay">
                            <div class="box">
                                <div class="link">
                                    <a class="btn-two" href="#">View Profile<span class="flaticon-next"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="name text-center">
                        <p><span>Cofounder & CEO</span></p>
                        <h3>Joe Wilson</h3>
                    </div>
                </div>
            </div>
            <!--End Single Team Member-->
            <!--Start Single Team Member-->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="single-team-member">
                    <div class="img-holder">
                        <img src="https://demo.softwarecompany.ae/ask_deema/images/gallery/team-01.jpg" alt="Awesome Image">
                        <!-- <ul class="sociallinks">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                        </ul> -->
                        <div class="overlay">
                            <div class="box">
                                <div class="link">
                                    <a class="btn-two" href="#">View Profile<span class="flaticon-next"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="name text-center">
                        <p><span>Shop Manager</span></p>
                        <h3>White Grey</h3>
                    </div>
                </div>
            </div>
            <!--End Single Team Member-->
            <!--Start Single Team Member-->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="single-team-member">
                    <div class="img-holder">
                        <img src="https://demo.softwarecompany.ae/ask_deema/images/gallery/team-02.jpg" alt="Awesome Image">
                        <!-- <ul class="sociallinks">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                        </ul> -->
                        <div class="overlay">
                            <div class="box">
                                <div class="link">
                                    <a class="btn-two" href="#">View Profile<span class="flaticon-next"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="name text-center">
                        <p><span>Manager</span></p>
                        <h3>Kevin Smith</h3>
                    </div>
                </div>
            </div>
            <!--End Single Team Member-->
        </div>
    </div>
</section>
<!--End Team Area-->

<!-- Ask Deema Contact information -->
<section class="askdeema-contact-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content">
                    <div class="title">
                        <h1>Contact Information For Ask Deema</span></h1>
                    </div>
                    <div class="button">

                        <a class="btn-one call-us" href="tel:+<?= $CONTACT_US_DETAILS->column_3 ?>"><i class="fa fa-phone"></i><?= $CONTACT_US_DETAILS->column_3 ?></a>

                        <a class="btn-one mail-us call-us" href="mailto:<?= $CONTACT_US_DETAILS->column_2 ?>"><i class="flaticon-mail"></i><?= $CONTACT_US_DETAILS->column_2 ?><span class="flaticon-next"></span></a>

                        <a class="btn-one call-us" href="https://wa.me/<?= $CONTACT_US_DETAILS->column_4 ?>/?text=Hello sir" target="_blank"><i class="fab fab-whatsapp"></i><?= $CONTACT_US_DETAILS->column_4 ?><span class="flaticon-next"></span></a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--End Ask Deema Contact information -->



@include('user.view.includes.footer')
