@include('user.view.includes.header')


<div class="__shareFixd">
    <a href="https://www.linkedin.com/company/askdeema"><img src="https://demo.softwarecompany.ae/ask_deema/images/rtfixd1.svg"></a>
    <a href="https://twitter.com/askdeema"><img src="https://demo.softwarecompany.ae/ask_deema/images/rtfixd2.svg"></a>
    <a href="https://www.facebook.com/Ask-Deema-100964511965038/"><img src="https://demo.softwarecompany.ae/ask_deema/images/rtfixd3.svg"></a>
    <a href="https://www.instagram.com/askdeema/"><img src="https://demo.softwarecompany.ae/ask_deema/images/rtfixd4.svg"></a>
</div>

<section class="section __innerbanner __blogWpPg" style="background-image: url(https://demo.softwarecompany.ae/ask_deema/images/blog_bnr.jpg)" data-overlay="dark"
         data-opacity="4">
    <div class="container position-relative">
        <h2>Always use Active Voice<br />
            Over The Passive One</h2>
        <h6>Make it attention grabbing</h6>




    </div>
</section>

<section class="section breadcrumb_wrap">

    <div class="container position-relative">
        <div class="breadcrumb-nav">
            <ul>
                <li><a href="index.php">Home</a></li>
                <li class="active">Blog</li>
            </ul>
        </div>
    </div>

</section>

<section class="section __blog_wrp">

    <div class="container">

        <h2>Ask Deema Blog</h2>

        <div class="wd100 __blog_tabwp">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <?php
                    $Sql = "SELECT * FROM `blog_categories` WHERE status=1";
                    $cat = \App\Database::select($Sql);
                    for ($i = 0; $i < count($cat); $i++) {
                        $d = $cat[$i];
                        $baseFile = "storage/" . $d->image;
                        if (is_file(Config::get('constants.HOME_DIR') . $baseFile)) {
                            $d->image_url = url($baseFile);
                        } else {
                            $d->image_url = url(Config::get('constants.DEFAULT_LOGO_VENDOR'));
                        }
                        ?>
                        <button class="nav-link active" id="nav-deema_advice-tab" data-bs-toggle="tab"
                                data-bs-target="#nav-deema_advice" type="button" role="tab" aria-controls="nav-deema_advice"
                                aria-selected="true">
                            <div class="__blgtbbz">
                                <img class="img-fluid" src="<?= $d->image_url ?>" />
                                <h5><?= $d->name ?></h5>
                            </div>
                        </button>
                    <?php } ?>


                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-deema_advice" role="tabpanel"
                     aria-labelledby="nav-deema_advice-tab">

                    <div class="__blgCtLIvw wd100">

                        <div class="row">

                            <?php
                            $Sql = "SELECT * FROM `blogs` WHERE status=1";
                            $blogs = \App\Database::select($Sql);
                            for ($i = 0; $i < count($blogs); $i++) {
                                $d = $blogs[$i];

                                $baseFile = "storage/" . $d->image;
                        if (is_file(Config::get('constants.HOME_DIR') . $baseFile)) {
                            $d->image_url = url($baseFile);
                        } else {
                            $d->image_url = url(Config::get('constants.DEFAULT_LOGO_VENDOR'));
                        }
                                ?>
                                <div class="col-lg-4 col-md-12 col-sm-12">
                                    <div class="__blgGdVw wd100">
                                        <div class="__blgGdVImg wd100">
                                            <img class="img-fluid" src="<?= $d->image_url ?>" />
                                        </div>

                                        <div class="__blgGddcrop wd100">
                                            <div class="__blgGddcrop_Inr wd100">
                                                <h2><?= $d->title ?></h2>

                                                <div class="__blgGddcrop_InrInfoWrp wd100">
                                                    <div class="__blgadmin">By Admin</div>
                                                    <div class="__blgdte"><?= date('d M, Y',  strtotime($d->created_at)) ?></div>
                                                </div>

                                                <div class="__blgdcrlist wd100">
                                                   <?= $d->description ?>
                                                </div>
                                                <div class="wd100 text-center">
                                                    <a href="#" xxhref="<?= url("blog/$d->slug") ?>" class="__blgMoreBtn">Read More</a>

                                                </div>


                                            </div>
                                        </div>

                                    </div>


                                </div>
                            <?php } ?>




                        </div>

<!--                        <div class="wd100 text-end mt-3 ">
                            <button type="submit" class="btn btn-secondary  me-3">More Articles</button>

                        </div>-->


                    </div>



                </div>

                <div class="tab-pane fade" id="nav-photos" role="tabpanel" aria-labelledby="nav-photos-tab">


                    <div class="__blgCtLIvw wd100">

                        <div class="row">


                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="__blgGdVw wd100">
                                    <div class="__blgGdVImg wd100">
                                        <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/images/blog1.jpg" />
                                    </div>

                                    <div class="__blgGddcrop wd100">
                                        <div class="__blgGddcrop_Inr wd100">
                                            <h2>Happy Wedding Couple Walk Together</h2>

                                            <div class="__blgGddcrop_InrInfoWrp wd100">
                                                <div class="__blgadmin">By Admin</div>
                                                <div class="__blgdte">08 May, 2019</div>
                                            </div>

                                            <div class="__blgdcrlist wd100">
                                                <p>In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate</p>
                                            </div>
                                            <div class="wd100 text-center">
                                                <a href="#" class="__blgMoreBtn">Read More</a>

                                            </div>


                                        </div>
                                    </div>

                                </div>


                            </div>

                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="__blgGdVw wd100">
                                    <div class="__blgGdVImg wd100">
                                        <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/images/blog1.jpg" />
                                    </div>

                                    <div class="__blgGddcrop wd100">
                                        <div class="__blgGddcrop_Inr wd100">
                                            <h2>Happy Wedding Couple Walk Together</h2>

                                            <div class="__blgGddcrop_InrInfoWrp wd100">
                                                <div class="__blgadmin">By Admin</div>
                                                <div class="__blgdte">08 May, 2019</div>
                                            </div>

                                            <div class="__blgdcrlist wd100">
                                                <p>In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate</p>
                                            </div>
                                            <div class="wd100 text-center">
                                                <a href="#" class="__blgMoreBtn">Read More</a>

                                            </div>


                                        </div>
                                    </div>

                                </div>


                            </div>

                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="__blgGdVw wd100">
                                    <div class="__blgGdVImg wd100">
                                        <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/images/blog1.jpg" />
                                    </div>

                                    <div class="__blgGddcrop wd100">
                                        <div class="__blgGddcrop_Inr wd100">
                                            <h2>Happy Wedding Couple Walk Together</h2>

                                            <div class="__blgGddcrop_InrInfoWrp wd100">
                                                <div class="__blgadmin">By Admin</div>
                                                <div class="__blgdte">08 May, 2019</div>
                                            </div>

                                            <div class="__blgdcrlist wd100">
                                                <p>In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate</p>
                                            </div>
                                            <div class="wd100 text-center">
                                                <a href="#" class="__blgMoreBtn">Read More</a>

                                            </div>


                                        </div>
                                    </div>

                                </div>


                            </div>



                        </div>

                        <div class="wd100 text-end mt-3 ">
                            <button type="submit" class="btn btn-secondary  me-3">More Articles</button>

                        </div>


                    </div>



                </div>

                <div class="tab-pane fade" id="nav-details" role="tabpanel" aria-labelledby="nav-details-tab">


                    <div class="__blgCtLIvw wd100">

                        <div class="row">


                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="__blgGdVw wd100">
                                    <div class="__blgGdVImg wd100">
                                        <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/images/blog1.jpg" />
                                    </div>

                                    <div class="__blgGddcrop wd100">
                                        <div class="__blgGddcrop_Inr wd100">
                                            <h2>Happy Wedding Couple Walk Together</h2>

                                            <div class="__blgGddcrop_InrInfoWrp wd100">
                                                <div class="__blgadmin">By Admin</div>
                                                <div class="__blgdte">08 May, 2019</div>
                                            </div>

                                            <div class="__blgdcrlist wd100">
                                                <p>In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate</p>
                                            </div>
                                            <div class="wd100 text-center">
                                                <a href="#" class="__blgMoreBtn">Read More</a>

                                            </div>


                                        </div>
                                    </div>

                                </div>


                            </div>

                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="__blgGdVw wd100">
                                    <div class="__blgGdVImg wd100">
                                        <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/images/blog1.jpg" />
                                    </div>

                                    <div class="__blgGddcrop wd100">
                                        <div class="__blgGddcrop_Inr wd100">
                                            <h2>Happy Wedding Couple Walk Together</h2>

                                            <div class="__blgGddcrop_InrInfoWrp wd100">
                                                <div class="__blgadmin">By Admin</div>
                                                <div class="__blgdte">08 May, 2019</div>
                                            </div>

                                            <div class="__blgdcrlist wd100">
                                                <p>In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate</p>
                                            </div>
                                            <div class="wd100 text-center">
                                                <a href="#" class="__blgMoreBtn">Read More</a>

                                            </div>


                                        </div>
                                    </div>

                                </div>


                            </div>

                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="__blgGdVw wd100">
                                    <div class="__blgGdVImg wd100">
                                        <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/images/blog1.jpg" />
                                    </div>

                                    <div class="__blgGddcrop wd100">
                                        <div class="__blgGddcrop_Inr wd100">
                                            <h2>Happy Wedding Couple Walk Together</h2>

                                            <div class="__blgGddcrop_InrInfoWrp wd100">
                                                <div class="__blgadmin">By Admin</div>
                                                <div class="__blgdte">08 May, 2019</div>
                                            </div>

                                            <div class="__blgdcrlist wd100">
                                                <p>In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate</p>
                                            </div>
                                            <div class="wd100 text-center">
                                                <a href="#" class="__blgMoreBtn">Read More</a>

                                            </div>


                                        </div>
                                    </div>

                                </div>


                            </div>



                        </div>

                        <div class="wd100 text-end mt-3 ">
                            <button type="submit" class="btn btn-secondary  me-3">More Articles</button>

                        </div>


                    </div>



                </div>

                <div class="tab-pane fade" id="nav-reviews" role="tabpanel" aria-labelledby="nav-reviews-tab">

                    <div class="__blgCtLIvw wd100">

                        <div class="row">


                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="__blgGdVw wd100">
                                    <div class="__blgGdVImg wd100">
                                        <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/images/blog1.jpg" />
                                    </div>

                                    <div class="__blgGddcrop wd100">
                                        <div class="__blgGddcrop_Inr wd100">
                                            <h2>Happy Wedding Couple Walk Together</h2>

                                            <div class="__blgGddcrop_InrInfoWrp wd100">
                                                <div class="__blgadmin">By Admin</div>
                                                <div class="__blgdte">08 May, 2019</div>
                                            </div>

                                            <div class="__blgdcrlist wd100">
                                                <p>In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate</p>
                                            </div>
                                            <div class="wd100 text-center">
                                                <a href="#" class="__blgMoreBtn">Read More</a>

                                            </div>


                                        </div>
                                    </div>

                                </div>


                            </div>

                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="__blgGdVw wd100">
                                    <div class="__blgGdVImg wd100">
                                        <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/images/blog1.jpg" />
                                    </div>

                                    <div class="__blgGddcrop wd100">
                                        <div class="__blgGddcrop_Inr wd100">
                                            <h2>Happy Wedding Couple Walk Together</h2>

                                            <div class="__blgGddcrop_InrInfoWrp wd100">
                                                <div class="__blgadmin">By Admin</div>
                                                <div class="__blgdte">08 May, 2019</div>
                                            </div>

                                            <div class="__blgdcrlist wd100">
                                                <p>In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate</p>
                                            </div>
                                            <div class="wd100 text-center">
                                                <a href="#" class="__blgMoreBtn">Read More</a>

                                            </div>


                                        </div>
                                    </div>

                                </div>


                            </div>

                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="__blgGdVw wd100">
                                    <div class="__blgGdVImg wd100">
                                        <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/images/blog1.jpg" />
                                    </div>

                                    <div class="__blgGddcrop wd100">
                                        <div class="__blgGddcrop_Inr wd100">
                                            <h2>Happy Wedding Couple Walk Together</h2>

                                            <div class="__blgGddcrop_InrInfoWrp wd100">
                                                <div class="__blgadmin">By Admin</div>
                                                <div class="__blgdte">08 May, 2019</div>
                                            </div>

                                            <div class="__blgdcrlist wd100">
                                                <p>In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate</p>
                                            </div>
                                            <div class="wd100 text-center">
                                                <a href="#" class="__blgMoreBtn">Read More</a>

                                            </div>


                                        </div>
                                    </div>

                                </div>


                            </div>



                        </div>

                        <div class="wd100 text-end mt-3 ">
                            <button type="submit" class="btn btn-secondary  me-3">More Articles</button>

                        </div>


                    </div>


                </div>

                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">

                    <div class="__blgCtLIvw wd100">

                        <div class="row">


                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="__blgGdVw wd100">
                                    <div class="__blgGdVImg wd100">
                                        <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/images/blog1.jpg" />
                                    </div>

                                    <div class="__blgGddcrop wd100">
                                        <div class="__blgGddcrop_Inr wd100">
                                            <h2>Happy Wedding Couple Walk Together</h2>

                                            <div class="__blgGddcrop_InrInfoWrp wd100">
                                                <div class="__blgadmin">By Admin</div>
                                                <div class="__blgdte">08 May, 2019</div>
                                            </div>

                                            <div class="__blgdcrlist wd100">
                                                <p>In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate</p>
                                            </div>
                                            <div class="wd100 text-center">
                                                <a href="#" class="__blgMoreBtn">Read More</a>

                                            </div>


                                        </div>
                                    </div>

                                </div>


                            </div>

                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="__blgGdVw wd100">
                                    <div class="__blgGdVImg wd100">
                                        <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/images/blog1.jpg" />
                                    </div>

                                    <div class="__blgGddcrop wd100">
                                        <div class="__blgGddcrop_Inr wd100">
                                            <h2>Happy Wedding Couple Walk Together</h2>

                                            <div class="__blgGddcrop_InrInfoWrp wd100">
                                                <div class="__blgadmin">By Admin</div>
                                                <div class="__blgdte">08 May, 2019</div>
                                            </div>

                                            <div class="__blgdcrlist wd100">
                                                <p>In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate</p>
                                            </div>
                                            <div class="wd100 text-center">
                                                <a href="#" class="__blgMoreBtn">Read More</a>

                                            </div>


                                        </div>
                                    </div>

                                </div>


                            </div>

                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="__blgGdVw wd100">
                                    <div class="__blgGdVImg wd100">
                                        <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/images/blog1.jpg" />
                                    </div>

                                    <div class="__blgGddcrop wd100">
                                        <div class="__blgGddcrop_Inr wd100">
                                            <h2>Happy Wedding Couple Walk Together</h2>

                                            <div class="__blgGddcrop_InrInfoWrp wd100">
                                                <div class="__blgadmin">By Admin</div>
                                                <div class="__blgdte">08 May, 2019</div>
                                            </div>

                                            <div class="__blgdcrlist wd100">
                                                <p>In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate</p>
                                            </div>
                                            <div class="wd100 text-center">
                                                <a href="#" class="__blgMoreBtn">Read More</a>

                                            </div>


                                        </div>
                                    </div>

                                </div>


                            </div>



                        </div>

                        <div class="wd100 text-end mt-3 ">
                            <button type="submit" class="btn btn-secondary  me-3">More Articles</button>

                        </div>


                    </div>


                </div>


                <div class="tab-pane fade" id="nav-team" role="tabpanel" aria-labelledby="nav-team-tab">

                    <div class="__blgCtLIvw wd100">

                        <div class="row">


                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="__blgGdVw wd100">
                                    <div class="__blgGdVImg wd100">
                                        <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/images/blog1.jpg" />
                                    </div>

                                    <div class="__blgGddcrop wd100">
                                        <div class="__blgGddcrop_Inr wd100">
                                            <h2>Happy Wedding Couple Walk Together</h2>

                                            <div class="__blgGddcrop_InrInfoWrp wd100">
                                                <div class="__blgadmin">By Admin</div>
                                                <div class="__blgdte">08 May, 2019</div>
                                            </div>

                                            <div class="__blgdcrlist wd100">
                                                <p>In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate</p>
                                            </div>
                                            <div class="wd100 text-center">
                                                <a href="#" class="__blgMoreBtn">Read More</a>

                                            </div>


                                        </div>
                                    </div>

                                </div>


                            </div>

                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="__blgGdVw wd100">
                                    <div class="__blgGdVImg wd100">
                                        <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/images/blog1.jpg" />
                                    </div>

                                    <div class="__blgGddcrop wd100">
                                        <div class="__blgGddcrop_Inr wd100">
                                            <h2>Happy Wedding Couple Walk Together</h2>

                                            <div class="__blgGddcrop_InrInfoWrp wd100">
                                                <div class="__blgadmin">By Admin</div>
                                                <div class="__blgdte">08 May, 2019</div>
                                            </div>

                                            <div class="__blgdcrlist wd100">
                                                <p>In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate</p>
                                            </div>
                                            <div class="wd100 text-center">
                                                <a href="#" class="__blgMoreBtn">Read More</a>

                                            </div>


                                        </div>
                                    </div>

                                </div>


                            </div>

                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="__blgGdVw wd100">
                                    <div class="__blgGdVImg wd100">
                                        <img class="img-fluid" src="https://demo.softwarecompany.ae/ask_deema/images/blog1.jpg" />
                                    </div>

                                    <div class="__blgGddcrop wd100">
                                        <div class="__blgGddcrop_Inr wd100">
                                            <h2>Happy Wedding Couple Walk Together</h2>

                                            <div class="__blgGddcrop_InrInfoWrp wd100">
                                                <div class="__blgadmin">By Admin</div>
                                                <div class="__blgdte">08 May, 2019</div>
                                            </div>

                                            <div class="__blgdcrlist wd100">
                                                <p>In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate In publishing and graphic design,
                                                    Lorem ipsum is a placeholder text
                                                    commonly used to demonstrate</p>
                                            </div>
                                            <div class="wd100 text-center">
                                                <a href="#" class="__blgMoreBtn">Read More</a>

                                            </div>


                                        </div>
                                    </div>

                                </div>


                            </div>



                        </div>

                        <div class="wd100 text-end mt-3 ">
                            <button type="submit" class="btn btn-secondary  me-3">More Articles</button>

                        </div>


                    </div>



                </div>

            </div>
        </div>

        <!--        <div class="wd100 text-center">
                    <button type="submit" class="btn btn-secondary __blogWriteBtn">Write Our Blog</button>

                </div>-->


    </div>





</section>



@include('user.view.includes.footer')
