@include('user.view.includes.header')
<!--inner section -->
<section class="inner-banner wd100" style="display: none;">
    <div class="breadcrumb-area" style="background-image: url({{url('/user/images/bg/about-us.jpg')}})" data-overlay="dark"
         data-opacity="7">
        <div class="container pt-150 pb-150 position-relative">
            <div class="row justify-content-center">
                <div class="col-xl-12">
                    <div class="breadcrumb-title">
                        <h3 class="title">Creative & Elegant Event<br>
                            Design & planning.
                        </h3>
                        <span class="sub-title">Showcase your signature services set to go.</span>
                    </div>
                </div>
            </div>
            <div class="breadcrumb-nav">
                <ul>
                    <li><a href="<?= url('/') ?>">Home</a></li>
                    <li class="active">Log in</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--End inner Section -->
<!-- sign area start -->


 
<!--inner section -->
<section class="sign-up-area inner-banner wd100 pb-0">
    <div class="breadcrumb-area" style="background-image: url({{url('/user/images/bg/about-us.jpg')}})" data-overlay="dark"
         data-opacity="7">
        <div class="container pt-150 pb-150 position-relative">
           <div class="row justify-content-center">
               
                
                 <div class="col-xl-6 __brtTxPart">
                    <div class="breadcrumb-title">
                        <h3 class="title">Creative & Elegant Event<br>
                            Design & planning.
                        </h3>
                        <span class="sub-title">Showcase your signature services set to go.</span>
                    </div>
                </div>
               
              <div class="col-xl-6 col-lg-7">
                <div class="sign__wrapper white-bg">
                    <div class="sign__header mb-35">
                        <div class="sign__in sign-breadcrumb-inner">
                            <h3>Login to your Account</h3>
                            <!-- <a href="#" class="sign__social sign__social-2 mb-15"><i class="fab fa-google-plus-g"></i>Sign Up with Google</a>
                               <p> <span>........</span> Or, <a href="sign-in.html">sign in</a> with your email<span> ........</span> </p> -->
                        </div>
                    </div>
                    <div class="sign__form">

                        <form action="<?= url('customerloginTest') ?>" method="post">
                            @if(session()->has('message'))
                            <div class="alert alert-danger" role="alert">
                                {{ session()->get('message') }}
                            </div>
                            @endif

                            @csrf
                            <div class="sign__input-wrapper mb-25">
                              
                                <h5>E-mail</h5>
                                <div class="sign__input">
                                    <input type="email" name="email" value="<?= !empty($_POST['json']['message']) ? $_POST['json']['message'] : '' ?>" id="email" placeholder="Enter Your E-mail" required>
                                    <i class="fal fa-envelope"></i>
                                </div>
                            </div>

                            <div class="sign__input-wrapper mb-25">
                                <h5>Password</h5>
                                <div class="sign__input">
                                    <input type="password" name="password" id="password" placeholder="Password" required>
                                    <i class="fal fa-lock"></i>
                                    
                                    <div class="wd100">
                                         <span toggle="#user_password" class="fa fa-fw field-icon toggle-password fa-eye" aria-hidden="true"></span>
                                         <span toggle="#user_password" class="fa fa-fw field-icon toggle-password fa-eye-slash" aria-hidden="true"></span>
                                    </div>
                                    
                                </div>
                                 <a class="__forgotpassword" href="<?= url('password/reset') ?>"> Forgot password ? </a>  
                            </div>

                            <div class="sign__action d-flex justify-content-between mb-25">
                                <p>Don't have an account yet? 
                                    <a href="<?= url('customer-signup') ?>">Click here to Signup</a></p>
                            </div>
                            <div class="sign__up__button">
                                <button class="m-btn m-btn-4 w-30"  type="submit"> <a >Sign-In</a></button>
                                
                                
                           
                                <div class="  __linecs"> 
                                    <a class="__vendorsloginLink" href="<?= url('login') ?>">   Vendors Login </a>
                                </div>
                                
                            </div>
                            <div class="sign__new text-center mt-20" style="display: none;">
                                <div class="social_icons">
                                    <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
                                    <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
                                    <a target="_blank" href="#"><i class="fab fa-linkedin-in"></i></a>
                                    <a target="_blank" href="#"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </form>

                    </div>
                    </di>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- sign area end -->
@include('user.view.includes.footer')

<script>
    var isredirect = $('#isredirect').val();
    if (isredirect == '1') {
        setTimeout(function () {
            window.location = '<?= url('/') ?>';
        }, 500);
    }

    $('#customer-login').click(function () {

        var form = new FormData();
        form.append('_token', CSRF_TOKEN);

        form.append('json[email]', $('#email').val());
        form.append('json[password]', $('#password').val());

        if (!validateEmail($('#email').val()) || $('#email').val() == '') {
            $('#email').css('border-color', 'red');
            $('#email').focus();
            return false;
        } else {
            $('#email').css('border-color', '');
        }

        if ($('#password').val() == '') {
            $('#password').css('border-color', 'red');
            $('#password').focus();
            return false;
        } else {
            $('#password').css('border-color', '');
        }


        var json = ajaxpost(form, "/customer-login");
        try {
            var json = jQuery.parseJSON(json);
            alertSimple(json.messages);
            if (json.status == true) {
                setTimeout(function () {
                    window.location = json.url;
                }, 500);
            }
        } catch (e) {
            alert(e);
        }

    });
</script>