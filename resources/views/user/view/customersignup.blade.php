@include('user.view.includes.header')
<!--inner section -->
<input id="username" style="display:none" type="text" name="fakeusernameremembered">
<input id="password" style="display:none" type="password" name="fakepasswordremembered">
<section class="inner-banner wd100 pb-0">
    <div class="breadcrumb-area" style="background-image: url({{url('/user/images/bg/about-us.jpg')}})" data-overlay="dark"
         data-opacity="7">
        <div class="container pt-150 pb-150 position-relative">
            <div class="row justify-content-center">


                <div class="col-xl-4 __brtTxPart __customerSignupTx">
                    <div class="breadcrumb-title">
                        <h3 class="title">Creative & Elegant Event<br>
                            Design & planning.
                        </h3>
                        <span class="sub-title">Showcase your signature services set to go.</span>
                    </div>
                </div>

                <div class="col-xl-8">

                    <div class="sign__wrapper white-bg">
                        <div class="sign__header mb-25">

                            <div class="sign__in sign-breadcrumb-inner">
                                <h3>CREATE NEW CUSTOMER ACCOUNT </h3>
                                <!-- <a href="#" class="sign__social sign__social-2 mb-15"><i class="fab fa-google-plus-g"></i>Sign Up with Google</a>
                                   <p> <span>........</span> Or, <a href="sign-in.html">sign in</a> with your email<span> ........</span> </p> -->
                            </div>
                        </div>

                        <div class="sign__form">
                            <form onsubmit="return false;">

                                <div class="row">
                                    <div class="col-lg-6 col-md-12 col-sm-12 sign__input-wrapper mb-25">
                                        <h5>First Name</h5>
                                        <div class="sign__input">
                                            <input type="text" id="first_name" placeholder="Enter Your First Name" autocomplete="off">
                                            <i class="fal fa-user"></i>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-12 col-sm-12 sign__input-wrapper mb-25">
                                        <h5>Last Name</h5>
                                        <div class="sign__input">
                                            <input type="text" id="last_name" placeholder="Enter Your Last Name" autocomplete="off">
                                            <i class="fal fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 sign__input-wrapper mb-25">
                                        <h5>Email</h5>
                                        <div class="sign__input">
                                            <input type="text" id="email" autocomplete="nope" placeholder="Enter Your E-mail">
                                            <i class="fal fa-envelope"></i>
                                        </div>
                                    </div>





                                    <div class="col-lg-6 col-md-12 col-sm-12 sign__input-wrapper mb-25">
                                        <div class="sign__input-wrapper mb-25">
                                            <h5>Phone Number</h5>
                                            <div class="__telcoddro"> 
                                                <div class="__codrop sign__input">
                                                    <select name="countryCode" id="phone_code">
                                                        <?php
                                                        $Sql = "SELECT countryname,dial_code FROM `country` GROUP BY dial_code ORDER BY dial_code ASC";
                                                        $data = App\Database::select($Sql);
                                                        for ($i = 0; $i < count($data); $i++) {
                                                            $d = $data[$i];
                                                            ?>
                                                            <option <?= $d->dial_code == '971' ? 'selected' : '' ?> value="<?= $d->dial_code ?>">+<?= $d->dial_code ?></option>
                                                        <?php } ?>

                                                    </select>
                                                </div>
                                                <div class="__telrop sign__input">
                                                    <input type="number" id="phone" placeholder="Enter Your Phone Number" autocomplete="off">
                                                    <i class="fal fa-phone"></i>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-12 col-sm-12 sign__input-wrapper mb-25">
                                        <h5>Password</h5>
                                        <div class="sign__input">
                                            <input type="password" autocomplete="new-password" id="password-new" placeholder="Password">
                                            <i class="fal fa-lock"></i>

                                            <div class="wd100">
                                                <span onclick="showmypassword();" toggle="#user_password" class="fa fa-fw field-icon toggle-password fa-eye-slash" aria-hidden="true"></span>
                                                <span onclick="showmypassword();" toggle="#user_password" class="fa fa-fw field-icon toggle-password fa-eye-slash" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-12 col-sm-12 sign__input-wrapper mb-25">
                                        <h5>Confirm password</h5>
                                        <div class="sign__input">
                                            <input type="password" id="cpassword" placeholder="Password">
                                            <i class="fal fa-lock"></i>

                                            <div class="wd100">
                                                <span onclick="showmypassword();" toggle="#user_password" class="fa fa-fw field-icon toggle-password fa-eye-slash" aria-hidden="true"></span>
                                                <span onclick="showmypassword();" toggle="#user_password" class="fa fa-fw field-icon toggle-password fa-eye-slash" aria-hidden="true"></span>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-lg-6 col-md-12 col-sm-12 sign__input-wrapper mb-25" id="otpcode" style="display: none;">
                                        <h5>OTP Code <a id="resendBtn" style="float: right;" onclick="resentOtp();" href="javascript:void(0)">Resend</a></h5>
                                        <div class="sign__input">
                                            <input type="number" id="otpcodeVal" placeholder="">
                                            <i class="fal fa-lock"></i>
                                        </div>
                                    </div>

                                </div>

                                <div class="sign__action d-flex justify-content-between mb-15">
                                    <p>Already Registered User? 
                                        <a href="<?= url('customer-login') ?>">Click here to login</a></p>
                                </div>

                                <div class="sign__up__button">
                                    <button type="button" class="m-btn m-btn-4 w-30" id="customer-singup"> <a href="javascript:void(0)">Sign up</a></button>

                                </div>


                                <div class="  __linecs"> 
                                    <a class="__vendorsloginLink" href="<?= url('vendor-signup') ?>"> Vendors Signup</a>

                                </div>



                                <div class="sign__new text-center mt-20" style="display: none;">
                                    <div class="social_icons" >
                                        <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
                                        <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
                                        <a target="_blank" href="#"><i class="fab fa-linkedin-in"></i></a>
                                        <a target="_blank" href="#"><i class="fab fa-instagram"></i></a>
                                    </div>
                                </div>


                            </form>

                        </div>
                        </di>
                    </div>
                </div> 


            </div>

            <!-- <div class="breadcrumb-nav">
                <ul>
                    <li><a href="<?= url('/') ?>">Home</a></li>
                    <li class="active">Sign Up</li>
                </ul>
            </div> -->


        </div>
    </div>
</section>
<!--End inner Section -->
<!-- sign area start -->
<div class="sign-up-area pt-100 fix">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-7">
            </div>
        </div>
    </div>
</div>
<!-- sign area end -->
@include('user.view.includes.footer')

<script>
    var isValidate = 0;
    var optCode = '';
    var emailId = '';
    var phone = '';
    $('#customer-singup').click(function () {

        var form = new FormData();
        form.append('_token', CSRF_TOKEN);
        var phoneWithcode = $('#phone_code').val() + $("#phone").val();

        form.append('json[first_name]', $('#first_name').val());
        form.append('json[last_name]', $('#last_name').val());
        form.append('json[email]', $('#email').val());
        form.append('json[phone]', phoneWithcode);
        form.append('json[password]', $('#password-new').val());


        if ($('#vendor_category').val() == '') {
            $('#vendor_category').css('border-color', 'red');
            $('#vendor_category').focus();
            return false;
        } else {
            $('#vendor_category').css('border-color', '');
        }

        if ($('#first_name').val() == '') {
            $('#first_name').css('border-color', 'red');
            $('#first_name').focus();
            return false;
        } else {
            $('#first_name').css('border-color', '');
        }

        if (!validateEmail($('#email').val()) || $('#email').val() == '') {
            $('#email').css('border-color', 'red');
            $('#email').focus();
            return false;
        } else {
            $('#email').css('border-color', '');
        }
        if ($('#phone').val() == '') {
            $('#phone').css('border-color', 'red');
            $('#phone').focus();
            return false;
        } else {
            $('#phone').css('border-color', '');
        }
        if ($('#password-new').val() == '') {
            $('#password-new').css('border-color', 'red');
            $('#password-new').focus();
            return false;
        } else {
            $('#password-new').css('border-color', '');
        }
        if ($('#cpassword').val() != $('#password-new').val()) {
            $('#cpassword').css('border-color', 'red');
            $('#cpassword').focus();
            return false;
        } else {
            $('#cpassword').css('border-color', '');
        }
        if (optCode) {
            var user_code = parseInt($('#otpcodeVal').val());
            var code = parseInt(atob(optCode)) - 777;
            if (user_code != code) {
                alertSimple('Invalid otp code');
                $('#otpcodeVal').focus();
                return false;
            } else if (user_code == code) {
                form.append('json[email]', emailId);
                form.append('json[phone]', phone);
                isValidate = 1;
            }
        }
        form.append('isValidate', isValidate);
        var json = ajaxpost(form, "/customer-signup");
        try {
            var json = jQuery.parseJSON(json);
            alertSimple(json.messages);
            if (json.status == true) {
                if (isValidate == false) {
                    optCode = json.otp;
                    emailId = $('#email').val();
                    phone = phoneWithcode;
                    $('#resendBtn').hide();
                    setTimeout(function () {
                        $('#resendBtn').show();
                    }, 30000);
                    $('#email,#phone').attr('disabled', true);
                    $('#otpcode').show();
                } else {
                    setTimeout(function () {
                        window.location = '';
                    }, 1000);
                }
            }
        } catch (e) {
            alert(e);
        }

    });
    function resentOtp() {
        optCode = 0;
        $('#customer-singup').click();
        $('#resendBtn').hide();
        setTimeout(function () {
            $('#resendBtn').show();
        }, 30000);
    }
    function showmypassword() {
        var passwordType = $('#password-new').attr('type');
        if (passwordType == 'password') {
            $('.fa-eye-slash').addClass('fa-eye').removeClass('fa-eye-slash');
            $('#password-new,#cpassword').attr('type', 'text');
        } else {
            $('#password-new,#cpassword').attr('type', 'password');
            $('.fa-eye').addClass('fa-eye-slash').removeClass('fa-eye');
        }
    }
</script>