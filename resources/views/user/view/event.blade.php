{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->


        <!--begin::Dashboard-->






        <div class="row __ledBlkWrp mb-5">
            <?php
            $roleId = Auth::user()->roles()->first()->id;
            if ($roleId == '6') {
                $select = ",TL.event_id,VE.event_name,VE.event_date,VE.event_location,VE.document";
                $join = " LEFT JOIN todo_list TL ON TL.id=TLG.todo_id LEFT JOIN vendor_events VE ON VE.id=TL.event_id";
                $Sql = "SELECT TLG.*$select  FROM `todo_list_guest` TLG $join WHERE TLG.`user_id` = '".Auth::user()->id."'";
                $dataList = \App\Database::select($Sql);
            } else {
                $select = ",O.name AS occasion_name,VE.document";
                $join = " LEFT JOIN occasions O ON O.id=VE.occasion_id";
                $Sql = "SELECT VE.*,VE.id AS event_id $select FROM `vendor_events` VE $join WHERE VE.user_id='" . Auth::user()->id . "' ORDER BY VE.id DESC";
                $dataList = \App\Database::select($Sql);
            }

            $baseDir = Config::get('constants.HOME_DIR');
            if (!empty($dataList)) {
                for ($i = 0; $i < count($dataList); $i++) {
                    $d = $dataList[$i];
                    $url = url('user/event/' . $d->event_id);

                    $baseFile = "storage/" . $d->document;
                    if (is_file($baseDir . $baseFile)) {
                        $d->document = url($baseFile);
                    } else {
                        $d->document = url(Config::get('constants.DEFAULT_LOGO_VENDOR'));
                    }
                    ?>
                    <div class="col-lg-3 col-md-6 col-sm-12 __ledBlk">
                        <div class="wd100 __ledBlkInr">
                            <div class="__ledBlkImg">
                                <a href="<?= $url ?>"><img class="img-fluid" src="<?= $d->document ?>"></a>
                            </div>
                            <div class="__ledBlkDicp wd100">

                                <h4 class="text-truncate"><a href="<?= $url ?>"><?= $d->event_name ?></a></h4>
                                <h5><a href="<?= $url ?>"><?= date('F d Y', strtotime($d->event_date)) ?></a></h5>
                                <h6 class="text-truncate"><a href="<?= $url ?>"><?= $d->event_location ?></a></h6>
                            </div>

                        </div>
                    </div>
                    <?php
                }
            }
            ?>


        </div>

        <div class="wd100 mb-5">
            <!--<button type="button" class="btn btn-primary float-right __create_eventBtn">Create a Event</button>-->
        </div>


        <!--end::Container-->




        <!--end::Container-->
    </div>
</div>
<!--begin::Entry-->

@endsection

{{-- Scripts Section --}}
@section('scripts')

@endsection
