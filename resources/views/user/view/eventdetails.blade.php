{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->
<?php
$event=App\Helpers\LibHelper::GetvendoreventsById($eventId);
?>


        <div class="wd100 __subbnrWrp d-flex">
            <div class="__subbnrTxpt">
                <h2><?= $event->event_name ?></h2>
                <h3><?= date('F d Y', strtotime($event->event_date)) ?></h3>
                <h4><?= $event->event_location ?></h4>
            
            </div>
            <div class="__subbnrImgpt" style="background: url('<?= $event->event_logo ?>') center"> </div>
        </div>








        <div class="row mt-10">

            <div class="col-lg-12 col-md-12 col-sm-12">

                <div class="row __srvBzWrp">


                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <a href="javascript:void(0)" class="wd100 __srvBzbk">
                            <div class="__sbkIcon">
                                <i class="flaticon-budget"></i>
                            </div>
                            <h5>Budgeter</h5>
                        </a> 
                    </div> 




                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <a href="<?= url("checklist/$eventId") ?>" class="wd100 __srvBzbk">
                            <div class="__sbkIcon">
                                <i class="flaticon-clipboard"></i>
                            </div>
                            <h5>Checklist</h5>
                        </a> 
                    </div> 





                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <a href="javascript:void(0)" class="wd100 __srvBzbk">
                            <div class="__sbkIcon">
                                <i class="flaticon-chat"></i>
                            </div>
                            <h5>Conversations</h5>
                        </a> 
                    </div> 

                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <a href="<?= url("create-web-page/$eventId") ?>" class="wd100 __srvBzbk">
                            <div class="__sbkIcon">
                                <i class="flaticon-networking"></i>
                            </div>
                            <h5>Create a Web Page</h5>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <a href="<?= url("guest-list/$eventId") ?>" class="wd100 __srvBzbk">
                            <div class="__sbkIcon">
                                <i class="flaticon-guest-list"></i>
                            </div>
                            <h5>Guest List</h5>
                        </a> 
                    </div> 


                </div>

            </div>







        </div>



        <!--end::Container-->
    </div>
</div>
<!--begin::Entry-->

@endsection

{{-- Scripts Section --}}
@section('scripts')

@endsection
