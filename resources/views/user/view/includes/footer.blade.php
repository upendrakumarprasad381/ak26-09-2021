<?php
$CONTACT_US_DETAILS = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'CONTACT_US_DETAILS');
?>
<footer class="footer wd100">
    <div class="container">
        <div class="row">

            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 __ftFtext">

                <img class="img-fluid" src="{{asset('user/images/logo.png')}}" alt="Ask Deema">

                  <p>Deema and Abdullah started working
                    onAskDeema platform in July 2019.
                    The concept they worked on was to find
                    a platform to ease the event planning
                    experience to make it a better and
                    enjoyable one.</p>



                <div class="_social_icons_top wd100">
                    <a target="_blank" href="https://www.facebook.com/Ask-Deema-100964511965038/"><i class="fab fa-facebook-f"></i></a>
                    <a target="_blank" href="https://twitter.com/askdeema"><i class="fab fa-twitter"></i></a>
                    <a target="_blank" href="https://www.linkedin.com/company/askdeema"><i class="fab fa-linkedin-in"></i></a>
                    <a target="_blank" href="#"><i class="fab fa-youtube"></i></a>
                    <a target="_blank" href="https://www.instagram.com/askdeema/"><i class="fab fa-instagram"></i></a>
                </div>


            </div>

            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 ft_menu">
                <h5>Services</h5>
                <ul>
                    <li><a href="<?= url('customer-login') ?>">Planning Tools </a></li>
                    <li><a href="<?= url('vendors-list') ?>">Vendor Finder</a></li>
                    <li><a href="<?= url('shop') ?>">Ask Deema Shop</a></li>
                    <li><a href="<?= url('my-services') ?>">Ask Deema’s Services</a></li>
                     <li><a href="<?= url('customer-login') ?>">Customer Login</a></li>

                </ul>


            </div>

            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 __ftinstagram">
                <h5>Instagram</h5>

                <div class="wd100 __inwrap">
<script src="https://apps.elfsight.com/p/platform.js" defer></script>
<div class="elfsight-app-6d566419-cea2-4ea9-9384-c33c49edee69"></div>
<!--                    <div class="__instBoz">
                        <a href="#"><img class="img-fluid" src="{{asset('user/images/Instagram1.png')}}">	</a>
                    </div>

                    <div class="__instBoz">
                        <a href="#"><img class="img-fluid" src="{{asset('user/images/Instagram2.png')}}">	</a>
                    </div>


                    <div class="__instBoz">
                        <a href="#"><img class="img-fluid" src="{{asset('user/images/Instagram3.png')}}">	</a>
                    </div>

                    <div class="__instBoz">
                        <a href="#"><img class="img-fluid" src="{{asset('user/images/Instagram4.png')}}">	</a>
                    </div>

                    <div class="__instBoz">
                        <a href="#"><img class="img-fluid" src="{{asset('user/images/Instagram5.png')}}">	</a>
                    </div>

                    <div class="__instBoz">
                        <a href="#"><img class="img-fluid" src="{{asset('user/images/Instagram6.pn')}}g"></a>
                    </div>-->

                </div>

            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 ft_menu">
                <h5>Contact Us</h5>


                <div class="wd100 mb-3 f_info">
                    <div class="d-flex">
                        <div class="flex-shrink-0"> <img src="{{asset('user/images/ft_pin.svg')}}"> </div>
                        <div class="flex-grow-1 ms-3">
                            <!--							<b>Address</b>-->
                            <?= $CONTACT_US_DETAILS->column_1 ?> </div>
                    </div>
                </div>


                <div class="wd100 mb-3 f_info">
                    <div class="d-flex">
                        <div class="flex-shrink-0">
                            <img src="{{asset('user/images/ft_mail.svg')}}">
                        </div>
                        <div class="flex-grow-1 ms-3">

                            <a href="mailto:<?= $CONTACT_US_DETAILS->column_2 ?>"><?= $CONTACT_US_DETAILS->column_2 ?></a>
                        </div>
                    </div>

                </div>

                <div class="wd100 mb-3 f_info">
                    <div class="d-flex">
                        <div class="flex-shrink-0">
                            <img src="{{asset('user/images/ft_call.svg')}}">
                        </div>

                        <div class="flex-grow-1 ms-3">
                            <a href="tel:+<?= $CONTACT_US_DETAILS->column_3 ?>">+<?= $CONTACT_US_DETAILS->column_3 ?></a>
                        </div>
                    </div>
                </div>

                <div class="wd100 __appdowWrp">
                    <h6>Download App</h6>

                    <div class="wd100 __storeIc">

                        <a target="_blank" href="#"><img class="img-fluid" src="{{asset('user/images/android-1.png')}}" /></a>
                        <a target="_blank" href="#"><img class="img-fluid" src="{{asset('user/images/apple-1.png')}}" /></a>

                    </div>
                </div>


            </div>
        </div>
    </div>

    <div class="wd100 copy_right">
        <div class="container">
            <div class="wd100 __ftsubNav">

                <ul>
                    <li> <a href="<?= url('customer-login') ?>">Planning Tools </a></li>
                    <li><a href="<?= url('vendors-list') ?>">Vendors </a></li>
                    <li><a href="<?= url('shop') ?>">Shop</a></li>
                    <li><a href="<?= url('my-services') ?>">Ask Deema  Services</a> </li>
                    <li><a href="<?= url('blog') ?>">Blog</a></li>
                    <li><a href="<?= url('about-us') ?>">About Us</a></li>
                    <li><a href="<?= url('vendors-portal') ?>">Vendors Portal</a></li>
                    <li><a href="<?= url('contact-us') ?>">Contact Us</a></li>
                </ul>


            </div>
            Copyright © <?= date('Y') ?> Ask Deema All right reserved. <a target="_blank" href="https://www.alwafaagroup.com/"> Web Design | Alwafaa Group, Dubai</a>


            <img class="pay_icon" src="{{asset('user/images/pay_icon.png')}}">


        </div>
    </div>
</footer>

<a id="back-to-top" href="#" class="back-to-top" role="button"><i class="fas fa-chevron-up"></i></a>
    <?php
    $roughtName = Route::getCurrentRoute()->getActionMethod();
    ?>
<script>
    var base_url = '<?= url('/'); ?>';
    var CSRF_TOKEN = "{{ csrf_token() }}";
    var method = '{{$roughtName}}';
</script>
<script src="{{asset('user/js/jquery.min.js')}}"></script>
<script src="{{asset('user/js/bootstrap.bundle.js')}}"></script>
<script src="{{asset('user/js/swiper-bundle.min.js')}}"></script>
<script src="{{asset('user/js/dzsparallaxer.js')}}"></script>
<link href="{{asset('user/js/lib/select2.min.css')}}" rel="stylesheet" />
<script src="{{asset('user/js/lib/select2.min.js')}}"></script>
{{-- <script src="{{asset('user/js/lib/appuser.js')}}"></script> --}}
<script src="{{asset('user/js/lib/jquery-confirm.min.js')}}"></script>
<script src="{{asset('user/js/common.js')}}"></script>
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function () {

        window.addEventListener('scroll', function () {

            if (window.scrollY > 200) {
                document.getElementById('navbar_top').classList.add('fixed-top');
                // add padding top to show content behind navbar
                navbar_height = document.querySelector('.navbar').offsetHeight;
                document.body.style.paddingTop = navbar_height + 'px';
            } else {
                document.getElementById('navbar_top').classList.remove('fixed-top');
                // remove padding top from body
                document.body.style.paddingTop = '0';
            }
        });
    });
    // DOMContentLoaded  end
</script>

