<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="{{asset('user/css/bootstrap.css')}}">
        <!--	<link rel="stylesheet" href="{{asset('user/css/style.css')}}">
                <link rel="stylesheet" href="{{asset('user/css/style2.css')}}">
                <link rel="stylesheet" href="{{asset('user/css/responsive.css')}} -->

        <link rel="stylesheet" href="{{asset('user/css/demo')}}/style.css?v=1.0.1" >
        <link rel="stylesheet" href="{{asset('user/css/demo')}}/style2.css">
        <link rel="stylesheet" href="{{asset('user/css/demo')}}/responsive.css">

        <link rel="stylesheet" href="{{asset('user/css/swiper.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/spacing.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/flaticon.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/fontawesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/icons.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('user/css/dzsparallaxer.css')}}">
        <link rel="stylesheet" href="{{asset('user/js/lib/jquery-confirm.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('user/wedding-icon-font/flaticon.css')}}">
        <title>Ask Deema </title>
        <link rel="apple-touch-icon" sizes="57x57" href="{{asset('user/images/favicons/apple-icon-57x57.png')}}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{asset('user/images/favicons/apple-icon-60x60.png')}}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{asset('user/images/favicons/apple-icon-72x72.png')}}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{asset('user/images/favicons/apple-icon-76x76.png')}}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{asset('user/images/favicons/apple-icon-114x114.png')}}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{asset('user/images/favicons/apple-icon-120x120.png')}}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{asset('user/images/favicons/apple-icon-144x144.png')}}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{asset('user/images/favicons/apple-icon-152x152.png')}}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{asset('user/images/favicons/apple-icon-180x180.png')}}">
        <link rel="icon" type="image/png" sizes="192x192" href="{{asset('user/images/favicons/android-icon-192x192.png')}}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{asset('user/images/favicons/favicon-32x32.png')}}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{asset('user/images/favicons/favicon-96x96.png')}}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{asset('user/images/favicons/favicon-16x16.png')}}">
        <link rel="manifest" href="{{asset('user/images/favicons/manifest.json')}}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{asset('user/images/favicons/ms-icon-144x144.png')}}">
        <meta name="theme-color" content="#ffffff">
        <script src="https://kit.fontawesome.com/9cea2b8228.js" crossorigin="anonymous"></script>
    </head>

    <body>


        <?php
        $session = App\Helpers\CommonHelper::GetSessionuser();
        ?>
        <style>
            /* Chrome, Safari, Edge, Opera */
            input::-webkit-outer-spin-button,
            input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            /* Firefox */
            input[type=number] {
                -moz-appearance: textfield;
            }
        </style>
        <header class="header" id="navbar_top">
            <div class="container __cctest">


                <div class="logo">
                    <a href="<?= url('/') ?>"><img class="img-fluid" src="{{asset('user/images/logo.png')}}" alt="Ask Deema"></a>
                </div>
                <div class="__hdrtpart">

                    <div class="__sbrasct">
                        <div class="menu_wrap navbar-light">
                            <nav class="navbar navbar-expand-lg  ">

                                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main_nav" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                                <div class="collapse navbar-collapse" id="main_nav">
                                    <ul class="navbar-nav">
                                        <li class="nav-item dropdown  "><a class="nav-link dropdown-toggle" href="javascript:void(0)"  >Planning Tools </a>
                                            <ul class="dropdown-menu __megMnLis">
                                                <li><a class="dropdown-item" href="<?= url('customer-login') ?>">Checklist </a></li>
                                                <li><a class="dropdown-item" href="<?= url('customer-login') ?>">Vendor Manager</a></li>
                                                <li><a class="dropdown-item" href="<?= url('customer-login') ?>">Budgetor</a></li>
                                                <li><a class="dropdown-item" href="<?= url('customer-login') ?>">Converstions </a></li>
                                                <li><a class="dropdown-item" href="<?= url('customer-login') ?>">Event Website </a></li>
                                                <li><a class="dropdown-item" href="<?= url('customer-login') ?>">Guest List </a></li>

                                                <li><a class="dropdown-item" href="<?= url('customer-login') ?>">Invoices  </a></li>
                                                <li><a class="dropdown-item" href="<?= url('customer-login') ?>">Proposals </a></li>
                                                <li><a class="dropdown-item" href="<?= url('customer-login') ?>">Contracts </a></li>
                                                <li><a class="dropdown-item" href="<?= url('customer-login') ?>">Registry  </a></li>
                                                <li><a class="dropdown-item" href="<?= url('customer-login') ?>">Financial Tool </a></li>
                                                <li><a class="dropdown-item" href="<?= url('customer-login') ?>">Calender </a></li>

                                                <li><a class="dropdown-item" href="<?= url('customer-login') ?>">To Do List   </a></li>
                                                <li><a class="dropdown-item" href="<?= url('customer-login') ?>">Floor Plans  </a></li>
                                                <li><a class="dropdown-item" href="<?= url('customer-login') ?>">Shop Front </a></li>


                                            </ul>
                                        </li>
                                        <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="<?= url("/vendors-list") ?>"  >Vendors </a>
                                            <ul class="dropdown-menu">
                                                <?php
                                                $servc = App\Helpers\LibHelper::GetvendorCategory();
                                                for ($i = 0; $i < count($servc); $i++) {
                                                    $d = $servc[$i];
                                                    ?>
                                                    <li><a class="dropdown-item" href="<?= url("/vendors-list?catId=$d->id") ?>"><?= $d->name ?></a></li>
                                                    <?php
                                                }
                                                ?>
                                                <li><a class="dropdown-item" href="<?= url("/vendors-list?event-planer=1") ?>">Event Planners</a></li>
                                            </ul>
                                        </li>
                                        <li class="nav-item"><a class="nav-link" href="<?= url('/shop') ?>"> Shop </a></li>

                                        <!--
                                                                                        <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">PT Programs</a>
                                                                                                <ul class="dropdown-menu">
                                                                                                        <li><a class="dropdown-item" href="product_details.php">Construction Material Testing</a></li>
                                                                                                        <li><a class="dropdown-item" href="product_details.php">Water Testing</a></li>
                                                                                                        <li><a class="dropdown-item" href="product_details.php">Environmental Testing</a></li>
                                                                                                        <li><a class="dropdown-item" href="product_details.php">Oil Testing</a></li>
                                                                                                        <li><a class="dropdown-item" href="product_details.php">Medical Testing</a></li>
                                                                                                </ul>
                                                                                        </li>
                                        -->
                                        <li class="nav-item"><a class="nav-link" href="<?= url('my-services') ?>">Ask Deema Services</a></li>

                                        <li class="nav-item"><a class="nav-link" href="<?= url('blog') ?>">Blog</a></li>
                                        <li class="nav-item"><a class="nav-link" href="<?= url('about-us') ?>">About Us</a></li>
                                        <li class="nav-item"><a class="nav-link" href="<?= url('vendors-portal') ?>">Vendors Portal</a></li>
                                        <!--						<li class="nav-item"><a class="nav-link" href="#">Contact Us</a></li>-->
                                    </ul>
                                </div>
                                <!-- navbar-collapse.// -->

                            </nav>
                        </div>

                        <?php if (empty($session)) { ?>
                            <div class="__logregdrp">
                                <div class="__logretbn">
                                    <a class="__login" href="<?= url('login') ?>"> Log In </a>

                                    <a class="__sign_up" href="<?= url('vendor-signup') ?>">Sign Up</a>
                                </div>
                                <a href="cart.php" class="__cart">
                                    <div class="_gtag cart_count" style=" ">10</div> <img src="{{url('/user')}}/images/cart.svg"> <span>My Cart</span> </a>
                            </div>
                        <?php } else { ?>
                            <div class="dash">
                                <ul class="navbar-nav after_login">
                                    <li class="nav-item dropdown dmenu">

                                        <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">
                                            <img width="24" height="24" class="img-fluid" src="<?= url('img/user-icon.png') ?>">
                                            <span class="lim"><?= $session->first_name ?></span>
                                        </a>

                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="<?= url('user/dashboard') ?>">Dashboard</a></li>
                                            <li><a class="dropdown-item" href="<?= url('user/personal_information') ?>">My Account </a></li>
                                            <li><a class="dropdown-item" href="<?= url('customer-logout') ?>">Logout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </header>
