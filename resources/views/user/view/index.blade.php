@include('user.view.includes.header')
<section class="section __banner">

    <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <?php
            $Sql = "SELECT * FROM `banner_management`";
            $banner = App\Database::select($Sql);

            for ($i = 0; $i < count($banner); $i++) {
                $d = $banner[$i];
                ?>
                <button type="button" data-bs-target="#carouselExampleCaptions" class="<?= empty($i) ? 'active' : '' ?>" data-bs-slide-to="<?= $i + 1 ?>" aria-label="Slide <?= $i + 1 ?>"></button>
                <?php
            }
            ?>


        </div>
        <div class="carousel-inner">
            <?php
            $baseDir = Config::get('constants.HOME_DIR') . "image-list/";
            for ($i = 0; $i < count($banner); $i++) {
                $d = $banner[$i];
                $file = is_file($baseDir . $d->image) ? url("image-list/" . $d->image) : url('/user/images/banner1.jpg');
                ?>
                <div class="carousel-item <?= empty($i) ? 'active' : '' ?>">
                    <img src="{{$file}}" class="d-block w-100" alt="...">

                    <div class="carousel-caption d-md-block">
                        <h5><?= !empty($d->title) ? $d->title : '' ?></h5>
                        <p><?= !empty($d->description) ? $d->description : '' ?></p>
                    </div>

                </div>
            <?php } ?>

        </div>

    </div>


</section>

<section class="section __bnrbtmscrwrp">
    <div class="container">
        <div class="__brtabbz">

            <div class="__bnr_txtfxd wd100">
                <h5><?= !empty($banner[0]->title) ? $banner[0]->title : '' ?></h5>
                <p><?= !empty($banner[0]->description) ? $banner[0]->description : '' ?></p>
            </div>


            <nav class="wd100">
                <div class="nav nav-tabs servicelink" id="nav-tab" role="tablist">
                    <?php
                    $servc = App\Helpers\LibHelper::GetvendorCategory();
                    for ($i = 0; $i < count($servc); $i++) {
                        $d = $servc[$i];
                        $file = url('/') . "/storage/" . (!empty($d->icon) ? $d->icon : '');
                        ?>
                        <button class="nav-link <?= empty($i) ? 'active' : '' ?>" catId="{{$d->id}}" id="nav-wedding_venue-tab" data-bs-toggle="tab" data-bs-target="#nav-wedding_venue" type="button" role="tab" aria-controls="nav-wedding_venue" aria-selected="true">
                            <div style="background: url({{$file}})" class="__icon"> </div>
                            <?= $d->name ?>
                        </button>
                    <?php } ?>
                    <button class="nav-link" id="nav-event_planners-tab" catId="EVENT_PLANER" data-bs-toggle="tab" data-bs-target="#nav-event_planners" type="button" role="tab" aria-controls="nav-event_planners" aria-selected="false">
                        <div class="__icon"> </div>Event Planners
                    </button>
                </div>
            </nav>

            <div class="tab-content wd100" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-wedding_venue" role="tabpanel" aria-labelledby="nav-wedding_venue-tab">

                </div>




            </div>
        </div>

    </div>
</section>


<section class="section  __fr2bk">
    <div class="container">
        <h2>Services</h2>
        <h3>Ask Deema Offers</h3>
        <div class="wd100">
            <div class="row">


                <div class="col __bxzsr">
                    <div class="__slrp">

                        <div class="__simgwp">
                            <img class="img-fluid" src="{{url('/user')}}/images/planning_tool.svg">
                        </div>

                        <div class="wd100 ">
                            <h4> PLANNING TOOLS </h4>
                        </div>
                        <div class="wd100 __slrptx">
                            <p>Use the planning tool to transform
                                all your event planning experience
                                into an easy and up to date online
                                experience.</p>
                        </div>
                        <div class="wd100 __rdnhfj">
                            <a href="<?= url('customer-login') ?>" class="__btn">Read More </a>
                        </div>
                    </div>
                </div>



                <div class="col __bxzsr">
                    <div class="__slrp">

                        <div class="__simgwp">
                            <img class="img-fluid" src="{{url('/user')}}/images/vendor_finder.svg">
                        </div>

                        <div class="wd100  ">
                            <h4> VENDOR FINDER</h4>
                        </div>
                        <div class="wd100 __slrptx">
                            <p>Use the planning tool to transform
                                all your event planning experience
                                into an easy and up to date online
                                experience.</p>
                        </div>
                        <div class="wd100 __rdnhfj">
                            <a href="<?= url('vendors-list') ?>" class="__btn">Read More </a>
                        </div>
                    </div>
                </div>



                <div class="col __bxzsr">
                    <div class="__slrp">

                        <div class="__simgwp">
                            <img class="img-fluid" src="{{url('/user')}}/images/ask_deema_Shop.svg">
                        </div>

                        <div class="wd100  ">
                            <h4>ASK DEEMA SHOP</h4>
                        </div>
                        <div class="wd100 __slrptx">
                            <p>Use the planning tool to transform
                                all your event planning experience
                                into an easy and up to date online
                                experience.</p>
                        </div>
                        <div class="wd100 __rdnhfj">
                            <a href="<?= url('shop') ?>" class="__btn">Read More </a>
                        </div>
                    </div>
                </div>



                <div class="col __bxzsr">
                    <div class="__slrp">

                        <div class="__simgwp">
                            <img class="img-fluid" src="{{url('/user')}}/images/services_ask_deema.svg">
                        </div>

                        <div class="wd100  ">
                            <h4> ASK DEEMA SERVICE</h4>
                        </div>
                        <div class="wd100 __slrptx">
                            <p>Use the planning tool to transform
                                all your event planning experience
                                into an easy and up to date online
                                experience.</p>
                        </div>
                        <div class="wd100 __rdnhfj">
                            <a href="<?= url('my-services') ?>" class="__btn">Read More </a>
                        </div>
                    </div>
                </div>



            </div>
        </div>
        <div class="wd100 __blksubbtnwp"> </div>
    </div>
</section>

@include('user.view.includes.footer')
<script>
    $(document).ready(function () {
       
        $('.servicelink .nav-link').click(function () {
            var form = new FormData();
            var catId = $(this).attr('catId');
            form.append('_token', CSRF_TOKEN);
            form.append('function', 'homepageSearchLoad');
            form.append('helper', 'Html');
            form.append('catId', catId);

            var json = ajaxpost(form, "/helper");
            try {
                var json = jQuery.parseJSON(json);
                if (json.status == true) {
                    $('#nav-wedding_venue').html(json.HTML);
                    $('.select2').select2();
                }
            } catch (e) {
                alert(e);
            }
        });
        $('#nav-wedding_venue-tab').click();
        $('body').on('click', '#homepageSearchBtn', function () {
            var catId = $('.servicelink .nav-link.active').attr('catId');
            catId = catId == null ? '' : catId;
            var occasions = $("#occasions").select2('val');
            var location = $("#location").select2('val');
            var noofguest = $("#noofguest").select2('val');
            var search = $("#search").val();
            var url = '';
            if (catId == 'EVENT_PLANER') {
                catId = $("#vendor_category").select2('val');
                catId = catId == null ? '' : catId;

                url = url + 'catId=' + catId;
                url = url + '&event-planer=1';
            } else {
                url = url + 'catId=' + catId;
            }
            if (occasions) {
                url = url + '&occasions=' + occasions;
            }
            if (location) {
                url = url + '&location=' + location;
            }
            if (noofguest) {
                url = url + '&noofguest=' + noofguest;
            }
            if (search) {
                url = url + '&search=' + search;
            }
            window.location = base_url + '/vendors-list?' + url;
        });
    });

</script>