@include('user.view.includes.header')

<?php
$CONTACT_US_DETAILS = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'CONTACT_US_DETAILS');
?>

<!--Inner Banner -->

<section class="inner-banner wd100">
    <div class="breadcrumb-area" style="background-image: url(<?= url('/img/vendors-bg.jpg') ?>)" data-overlay="dark"
         data-opacity="7">
        <div class="container pt-150 pb-150 position-relative">
            <div class="row">
                <div class="col-xl-6">
                    <div class="breadcrumb-title">

                        <h3 class="title">Ask Deema’s Services Tools
                            For Everyone
                        </h3>

                        <span class="sub-title">DEEMA’s speciality in the event industry is translated in the ASKDEEMA Services we offer.<br>
                            These services serve all customers to provide a unique experience in all the event
                            planning journey. Below are the services offered.</span>
                        <div class="col-sm-6 button-ask">
                            <div class="button">
                                <a class="btn-one ask-ad" href="<?= url('customer-login') ?>">Start Your Journy With AD</a>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="breadcrumb-nav">
                <ul>
                    <li><a href="<?= url('/') ?>">Home</a></li>
                    <li class="active">Services</li>
                </ul>
            </div>
            <div class="social-links">
                <ul class="social-icon-three">


                    <li><a href="https://www.facebook.com/Ask-Deema-100964511965038/"><i class="fab fab-facebook-f"></i></a></li>


                    <li><a href="https://twitter.com/askdeema"><i class="fab fab-twitter"></i></a></li>


                    <li><a href="https://www.instagram.com/askdeema/"><i class="fab fab-instagram"></i></a></li>


                    <li><a href="https://www.linkedin.com/company/askdeema"><i class="fab fab-linkedin-in"></i></a></li>

                </ul>
            </div>
        </div>

    </div>

</section>

<!-- End Inner Banner -->

<!-- Services -->
<section class="service-one service-one__home-two thm-gray-bg service-one__service-page">
    <div class="service-one__block">
        <div class="block-title text-center">
            <h2 class="block-title__title">What Can We Offer You?</h2>
            <div class="block-title__line"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="flex-fill  service-thumbnail-second-icon">
                        <div class="service-thumbnail-second-icon-circle ">
                            <i class="flaticon-calendar copy 2"></i>
                        </div>
                        <h3 class="service-one__title"><a href="#">Event Consultation</a></h3><!-- /.service-one__title -->
                        <p class="service-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->
                        <!-- <a href="#" class="thm-btn service-one__btn">Read More</a> -->
                    </div><!-- /.service-one__single -->
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <div class="flex-fill  service-thumbnail-second-icon">
                        <div class="service-thumbnail-second-icon-circle ">
                            <i class="flaticon-user copy 2"></i>
                        </div>

                        <h3 class="service-one__title"><a href="#">Beyond Event Management</a></h3><!-- /.service-one__title -->
                        <p class="service-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->
                        <!-- <a href="#" class="thm-btn service-one__btn">Read More</a> -->
                    </div><!-- /.service-one__single -->
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <div class="flex-fill  service-thumbnail-second-icon">
                        <div class="service-thumbnail-second-icon-circle ">
                            <i class="flaticon-operator copy 2"></i>
                        </div>
                        <h3 class="service-one__title"><a href="#">On Ground Assistance</a></h3><!-- /.service-one__title -->
                        <p class="service-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->
                        <!-- <a href="#" class="thm-btn service-one__btn">Read More</a> -->
                    </div><!-- /.service-one__single -->
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <div class="flex-fill  service-thumbnail-second-icon">
                        <div class="service-thumbnail-second-icon-circle">
                            <i class="flaticon-networking copy 2"></i>
                        </div>
                        <h3 class="service-one__title"><a href="#">Event Concept Development</a></h3><!-- /.service-one__title -->
                        <p class="service-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->
                        <!-- <a href="#" class="thm-btn service-one__btn">Read More</a> -->
                    </div><!-- /.service-one__single -->
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <div class="flex-fill  service-thumbnail-second-icon">
                        <div class="service-thumbnail-second-icon-circle ">
                            <i class="flaticon-group copy 2"></i>
                        </div>
                        <h3 class="service-one__title"><a href="#">Event Planning Services</a></h3><!-- /.service-one__title -->
                        <p class="service-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->
                        <!-- <a href="#" class="thm-btn service-one__btn">Read More</a> -->
                    </div><!-- /.service-one__single -->
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <div class="flex-fill  service-thumbnail-second-icon">
                        <div class="service-thumbnail-second-icon-circle ">
                            <i class="flaticon-eye copy 2"></i>
                        </div>
                        <h3 class="service-one__title"><a href="#">Wedding Vision</a></h3><!-- /.service-one__title -->
                        <p class="service-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->
                        <!-- <a href="#" class="thm-btn service-one__btn">Read More</a> -->
                    </div><!-- /.service-one__single -->
                </div><!-- /.col-lg-4 -->


            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.service-one__block -->
</section><!-- /.service-one -->
<!-- End Service -->

<!-- Ask Deema Contact Us -->

<section class="askdeema-contact-service  __serblNew">
    <div class="container">
        <div class="row">


            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="inner-content wd100">

                    <div class="title wd100">
                        <h1>Contact Information For Ask Deema</span></h1>
                    </div>

                    <div class="__dzsfwrap wd100">

                        <div class="button">
                            <a class="btn-one call-us" href="tel:+<?= $CONTACT_US_DETAILS->column_3 ?>"><i class="flaticon-call"></i><?= $CONTACT_US_DETAILS->column_3 ?></a>

                            <a class="btn-one mail-us" href="mailto:<?= $CONTACT_US_DETAILS->column_2 ?> "><i class="flaticon-mail"></i><?= $CONTACT_US_DETAILS->column_2 ?><span class="flaticon-next"></span></a>




                            <a class="btn-one whats-app call-us" href="https://wa.me/<?= $CONTACT_US_DETAILS->column_4 ?>/?text=Hello sir" target="_blank"><i class="flaticon-whatsapp"></i><?= $CONTACT_US_DETAILS->column_4 ?><span class="flaticon-next"></span></a>
                        </div>

                         <div class="__dfxgvs">
                            <div class="button">
                                <a class="btn-one ask-deema-start" xxdata-bs-toggle="modal" xxdata-bs-target="#exampleModal" href="<?= url('customer-login') ?>">Start Your Journey With Ask Deema </a>

                            </div>
                        </div>

                    </div>



                </div>




            <!-- <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">

            </div>

            --->




        </div>
    </div>
</section>


<!--<div class="modal fade __service-popup" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <h1 class="modal-title" id="exampleModalLabel">What Can We Offer You? </h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h5></h5>

                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="sign__input-wrapper mb-25">
                            <h5>First Name</h5>
                            <div class="sign__input-form">
                                <input type="text" class="form-control" placeholder="Enter Your First Name">

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="sign__input-wrapper mb-25">
                            <h5>Last Name</h5>
                            <div class="sign__input-form">
                                <input type="text" class="form-control" placeholder="Enter Your Last Name">

                            </div>
                        </div>
                    </div>
                </div>  End Row

                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="sign__input-wrapper mb-25">
                        <h5>E-mail Address</h5>
                        <div class="sign__input-form">
                            <input type="text" class="form-control" placeholder="Enter Your E-mail">

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="sign__input-wrapper mb-25">
                        <h5>Phone Number</h5>
                        <div class="sign__input-form">
                            <input type="text" class="form-control" placeholder="Enter your Number">

                        </div>
                    </div>
                     End Row



                    <div class="col-lg-12 col-md-6 col-sm-6">
                        <div class="sign__input-wrapper mb-25">
                            <h5>What Can We Offer You?</h5>
                            <div class="sign__input-form">
                                <select id="choices-multiple-remove-button" multiple>

                                    <option value="Event Consultation">Event Consultation</option>
                                    <option value="Beyond Event Managemen">Beyond Event Management</option>
                                    <option value="On Ground Assistance">On Ground Assistance</option>
                                    <option value="Event Concept Development">Event Concept Development</option>
                                    <option value="Event Planning Services">Event Planning Services</option>
                                    <option value="Wedding Vision">Wedding Vision</option>

                                </select>
                            </div>
                        </div>
                    </div>

                     End Row

                    <div class="col-lg-12 col-md-6 col-sm-6">
                        <div class="sign__input-wrapper mb-25">
                            <h5>Events Details</h5>

                            <div class="sign__input-form">
                                <textarea type="text"  class="form-control"placeholder="Events Details">
                                </textarea>


                            </div>
                        </div>
                    </div>

                     End Row


                    <div class="col-lg-12 col-md-6 col-sm-6">

                        <label class="check-remember container1 mb-3">
                            <input type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                        <p class="remember mb-3">Registered Store
                        </p>
                    </div>

                     End Row
                    <p class="remember-sign mb-3">By Clicking <a
                            href="#url">‘Sign Up’  I Agree To   Privacy Policy  And Terms Of Use. </a>
                    </p>

                    <button type="submit" class="btn btn-style btn-primary w-100">Sign Up
                    </button>

                    <div class="sign__new text-center mt-20">
                        <h5>Or Signup</h5>
                        <div class="social_icons">
                            <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
                            <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
                            <a target="_blank" href="#"><i class="fab fa-linkedin-in"></i></a>
                            <a target="_blank" href="#"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                    <p class="remember-sign-login mt-3 text-center"><a
                            href="#url">Already a Member Of  Ask Deema Log In </a>
                    </p>
                </div>


            </div>

        </div>
    </div>
</div>-->




@include('user.view.includes.footer')
