{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <!--begin::Dashboard-->

       	<div class="__toListWrp wd100"> 

            

            <div class="wd100 __tpExpBtnWrp" style="margin-top: 0;">
                <form method="post" onsubmit="return false;">
                    @csrf
                    <div class="d-flex">
                        <div class="__tolistserch d-flex align-items-center py-3 py-sm-0 px-sm-3 ">
                            <input name="search" id="search" type="text" class="form-control border-0 font-weight-bold pl-2" required placeholder="Search...">

                        </div>
                        <div class="dropdown __topSwitchBtn">
                            <button onclick="autoLoad();" name="searcthis" class="btn btn-primary" type="button" >
                                Serach
                            </button>
                        </div>
                        <div class="dropdown __topSwitchBtn">
                            <a href="<?= url('to-do-list/create') ?>"><button class="btn btn-secondary" type="button" >Create New </button></a>

                        </div>
                    </div>
                </form>
            </div> 


            


            <div class="  _todolist-1" style="margin-top: 50px;">



                <div class="__todolist_TabWrp wd100">
                    <div class="row">
                        <ul class="nav nav-tabs nav nav-pills nav-fill alltab">
                            <li><a  class="nav-to active" onclick="autoLoad();" tabId='all' data-toggle="tab" href="#home">All</a></li>
                            <li><a class="nav-to" onclick="autoLoad();" tabId='todays-overdue' data-toggle="tab" href="#menu1">Today + Overdue</a></li>
                            <li><a class="nav-to" onclick="autoLoad();" tabId='upcoming' data-toggle="tab" href="#menu2">Upcoming</a></li>
                            <li><a class="nav-to" onclick="autoLoad();" tabId='completed' data-toggle="tab" href="#menu3">Completed</a></li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active show">

                            <div class="__srvBzWrp __todSubRwp"> 
                                
                                <div class="__toderptrep"> 
                                    <div class="__toListTavw __scopeWorkTbvw  wd100"> 
                                         <?php
                                      
                                         ddlist($type = 'all');
                                        ?>
                                    </div>
                                </div>

                            </div> 
                        </div>

                        <div id="menu1" class="tab-pane fade">
                            <div class="  __srvBzWrp __todSubRwp">
                               
                                <div class="__toderptrep"> 
                                    <div class="__toListTavw __scopeWorkTbvw  wd100"> 
                                        <?php
                                        ddlist($type = 'todays-overdue');
                                        ?>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div id="menu2" class="tab-pane fade">
                            <div class="  __srvBzWrp __todSubRwp">
                                

                                <div class="__toderptrep"> 
                                    <div class="__toListTavw __scopeWorkTbvw  wd100"> 
                                        <?php
                                        ddlist($type = 'upcoming');
                                        ?>
                                    </div>
                                </div>

                            </div> 
                        </div>
                        <div id="menu3" class="tab-pane fade">
                            <div class="__srvBzWrp __todSubRwp">
                                
                                <div class="__toderptrep"> 
                                    <div class="__toListTavw __scopeWorkTbvw  wd100"> 
                                        <?php
                                        ddlist($type = 'completed');
                                        ?>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div> 


            </div>









            <div class="wd100 text-right mt-5">
                <a href="<?= url('to-do-list/create') ?>"><button type="button" class="btn btn-primary __guestLisBnt">Add New</button></a>

            </div>

        </div>



        <!--end::Container-->
    </div>
</div>
<!--begin::Entry-->
<?php

function ddlist($type) {
    ?>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col" class="text-center __brdsLt">S.N</th>
                <th scope="col" class="text-center">Event Name</th>
                <th scope="col" class="text-center">List Name</th>
                <th scope="col" class="text-center">Date</th>

                <th scope="col" class="text-center">Items Name</th>
              

                <th scope="col" class="text-center" width="20%">Status</th>
                <th scope="col" class="text-center __brdsRt">Action</th> 
            </tr>
        </thead>
        <tbody id="tbody-<?= $type ?>">


        </tbody>
    </table> 
    <?php
}
?>

@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
   
    var base_url="{{url('/')}}";
</script>











<script>
    function autoLoad() {
        setTimeout(function () {
            var tabId = $('.alltab .active').attr('tabid');
            var loading = '<tr><td colspan="8">Wait data is loading...</td></tr>';
            $('#tbody-' + tabId).html(loading);

            b();
        }, 250);

        function b() {
            var tabId = $('.alltab .active').attr('tabid');
            var data = {_token: "{{ csrf_token() }}", autoload: true, tabId: tabId};
            var occasions = [];
            $('.occasions:checked').each(function () {
                occasions.push($(this).attr('cond'));
            });
            var filter_list = [];
            $('.filter_list:checked').each(function () {
                filter_list.push($(this).attr('cond'));
            });
            var vendorCategory = [];
            $('.vendorCategory:checked').each(function () {
                vendorCategory.push($(this).attr('cond'));
            });
            data['occasions'] = occasions;
            data['filter_list'] = filter_list;
            data['vendorCategory'] = vendorCategory;
            data['search']=$('#search').val();
            $.ajax({
                url: "",
                cache: false,
                data: data,
                async: false,
                type: 'POST',
                success: function (res) {
                    res = jQuery.parseJSON(res);
                    $('#tbody-' + tabId).html(res.HTML);
                }
            });
        }
    }
    autoLoad();
    function clearall() {
        $('.vendorCategory,.filter_list,.occasions').prop('checked', false);

    }
    function deleteTodo(e) {
        Swal.fire({
            title: "Are you sure?",
            text: "You wont be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                var data = {_token: "{{ csrf_token() }}", todo_id: $(e).attr('todoId')};
                $.ajax({
                    url: "{{url('/to-do-list/delete-todo')}}",
                    cache: false,
                    data: data,
                    async: false,
                    type: 'POST',
                    success: function (res) {
                        location.reload();
                    }
                });
            } else if (result.dismiss === "cancel") {
                Swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                        )
            }
        });
    }



    
   
</script>
@endsection
