@include('user.view.includes.header')
<?php
$vendorId = !empty($_REQUEST['vendorId']) ? base64_decode($_REQUEST['vendorId']) : '';
$catId = !empty($_REQUEST['catId']) ? explode(',', $_REQUEST['catId']) : [];
$catId = !empty($catId) && is_array($catId) ? $catId : [];
$session = \App\Helpers\CommonHelper::GetSessionuser();


$vendor = App\Helpers\LibHelper::GetusersBy($vendorId);
$vendorDetails = App\Helpers\LibHelper::GetvendordetailsBy($vendorId);

if (isset($_POST['submitreviews'])) {
    $json = !empty($_POST['jsonR']) && is_array($_POST['jsonR']) ? $_POST['jsonR'] : [];
    $json['created_at'] = date('Y-m-d H:i:s');
    $json['updated_at'] = $json['created_at'];
    App\Database::insert('user_reviews', $json);
    $message = "thank you for your support";
}
if (isset($_POST['contacsussubmit'])) {
    $json = !empty($_POST['jsonR']) && is_array($_POST['jsonR']) ? $_POST['jsonR'] : [];
    $json['created_at'] = date('Y-m-d H:i:s');
    $json['updated_at'] = $json['created_at'];
    App\Database::insert('user_contact_us', $json);
    $message = "thank you for your enquiry";
}
?>
<div class="__shareFixd">
    <?php if (!empty($vendorDetails->linkedin)) { ?>
        <a target="_blank" href="<?= $vendorDetails->linkedin ?>"><img src="{{url('/user')}}/images/rtfixd1.svg"></a>
    <?php } if (!empty($vendorDetails->twiter)) { ?>
        <a target="_blank" href="<?= $vendorDetails->twiter ?>"><img src="{{url('/user')}}/images/rtfixd2.svg"></a>
    <?php } if (!empty($vendorDetails->facebook)) { ?>
        <a target="_blank" href="<?= $vendorDetails->facebook ?>"><img src="{{url('/user')}}/images/rtfixd3.svg"></a>
    <?php } if (!empty($vendorDetails->instagram)) { ?>
        <a target="_blank" href="<?= $vendorDetails->instagram ?>"><img src="{{url('/user')}}/images/rtfixd4.svg"></a>
    <?php } ?>
</div>


<input type="hidden" id="vendorId" value="<?= $vendorId ?>">


<section class="section __b2svSlir">
    <div class="inside_box wd100">
        <!-- Swiper S-->
        <div class="__epdSlr  wd100">
            <div class="swiper-wrapper">
                <?php
                $Sql = "SELECT * FROM `vendor_images` WHERE user_id='$vendorId'"; //
                $img = App\Database::select($Sql);
                $baseDir = Config::get('constants.HOME_DIR');
                if (!empty($img)) {
                    for ($i = 0; $i < count($img); $i++) {
                        $d = $img[$i];
                        $baseFile = "storage/" . $d->image;
                        if (is_file($baseDir . $baseFile)) {
                            $d->image = url($baseFile);
                        } else {
                            $d->image = url(Config::get('constants.DEFAULT_LOGO_VENDOR'));
                        }
                        ?>
                        <div class="swiper-slide">
                            <img class="img-fluid" src="{{$d->image}}">
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <div class="swiper-slide">
                        <img class="img-fluid" src="{{url('/user')}}/images/Event-management-companies1.jpg">
                    </div>
                    <div class="swiper-slide">
                        <img class="img-fluid" src="{{url('/user')}}/images/best-wedding-planners-In-Hyderabad.jpg">
                    </div>

                    <div class="swiper-slide">
                        <img class="img-fluid" src="{{url('/user')}}/images/Event-management-companies1.jpg">
                    </div>

                    <div class="swiper-slide">
                        <img class="img-fluid" src="{{url('/user')}}/images/best-wedding-planners-In-Hyderabad.jpg">
                    </div>
                <?php } ?>

            </div>
        </div>
        <!-- Add Arrows -->
        <!--
<div class="swiper-button-next __srbk2-next"></div>
<div class="swiper-button-prev __srbk2-prev"></div>
        -->
    </div>
</section>

<section class="section __evpllWp">
    <div class="container">
        <div class="wd100 __evpllbwzWp">

            <div class="__evpLewd">



                <div class="wd100 __evp_tabwp">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <button class="nav-link active" id="nav-about-tab" data-bs-toggle="tab" data-bs-target="#nav-about" type="button" role="tab" aria-controls="nav-about" aria-selected="true">About</button>
                            <button class="nav-link" id="nav-photos-tab" data-bs-toggle="tab" data-bs-target="#nav-photos" type="button" role="tab" aria-controls="nav-photos" aria-selected="false">Photos</button>
                            <button class="nav-link" id="nav-reviews-tab" data-bs-toggle="tab" data-bs-target="#nav-reviews" type="button" role="tab" aria-controls="nav-reviews" aria-selected="false">Reviews</button>
                            <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</button>
                            <button class="nav-link" id="nav-team-tab" data-bs-toggle="tab" data-bs-target="#nav-team" type="button" role="tab" aria-controls="nav-team" aria-selected="false">Team</button>

                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
                            <div class="wd100 __evpabcotWboz">
                                <div class="wd100 __evpabbzinfo">
                                    <h4><?= $vendor->first_name . ' ' . $vendor->last_name ?>
                                        <div class="__epd_wishliico">
                                            <i class="fa fa-heart" aria-hidden="true"></i>
                                        </div>
                                    </h4>
                                    <div class="wd100 __epdloction">
                                        <?= !empty($vendorDetails->address) ? $vendorDetails->address : 'location not updated' ?>
                                    </div>
                                </div>

                                <div class="wd100 __epdsAbout">
                                    <h2>About</h2>
                                    <p><?= !empty($vendorDetails->about) ? $vendorDetails->about : 'About us not updated by vendor.' ?></p>
                                </div>
                                <!--
                                                                <div class="wd100 __epdsDetails">
                                                                    <h3>Details</h3>
                                                                    <p>Fashion Services</p>
                                                                    <h3>Dry Cleaning and Preservations</h3>

                                                                </div>-->
                            </div>

                        </div>

                        <div class="tab-pane fade" id="nav-photos" role="tabpanel" aria-labelledby="nav-photos-tab">

                            <div class="__ptolibzWr wd100">

                                <div class="gallery clear">
                                    <?php
                                    $Sql = "SELECT * FROM `vendor_images` WHERE user_id='$vendorId'"; //
                                    $img = App\Database::select($Sql);
                                    $baseDir = Config::get('constants.HOME_DIR');
                                    if (!empty($img)) {
                                        for ($i = 0; $i < count($img); $i++) {
                                            $d = $img[$i];
                                            $baseFile = "storage/" . $d->image;
                                            if (is_file($baseDir . $baseFile)) {
                                                $d->image = url($baseFile);
                                            } else {
                                                $d->image = url(Config::get('constants.DEFAULT_LOGO_VENDOR'));
                                            }
                                            ?> <div class="bg" style="background-image: url({{$d->image}});"></div>  <?php
                                        }
                                    } else {
                                        ?>
                                        <div class="bg" style="background-image: url({{url('/user')}}/images/light_1.jpg);"></div>
                                        <div class="bg" style="background-image: url({{url('/user')}}/images/light_3.jpg);"></div>
                                        <div class="bg" style="background-image: url({{url('/user')}}/images/light_4.jpg);"></div>

                                        <div class="bg" style="background-image: url({{url('/user')}}/images/light_5.jpg);"></div>
                                        <div class="bg" style="background-image: url({{url('/user')}}/images/light_6.jpg);"></div>

                                        <div class="bg" style="background-image: url({{url('/user')}}/images/light_7.png);"></div>
                                        <div class="bg" style="background-image: url({{url('/user')}}/images/light_4.jpg);"></div>
                                    <?php } ?>
                                </div>
                            </div>

                        </div>



                        <div class="tab-pane fade" id="nav-reviews" role="tabpanel" aria-labelledby="nav-reviews-tab">
                            <!--	reviews-->
                            <div class="wd100 __rateExperience" >

                                <h2>Rate your experience</h2>
                                <div class="wd100 __starRate">
                                    <div class="___staricon">
                                        <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                    </div>

                                </div>

                                <div class="wd100 __write_review">
                                    <form method="post">
                                        @csrf
                                        <div class="row">

                                            <div class="mb-3 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <label for="exampleFormControlInput1" class="form-label">Name</label>
                                                <input type="text" class="form-control" value="<?= !empty(Auth::user()->first_name) ? Auth::user()->first_name : '' ?>" name="jsonR[name]"  placeholder="" required>
                                            </div>

                                            <div class="mb-3 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <label for="exampleFormControlInput1" class="form-label">Email</label>
                                                <input type="email" class="form-control" value="<?= !empty(Auth::user()->email) ? Auth::user()->email : '' ?>" name="jsonR[email]" placeholder="" required>
                                            </div>

                                            <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <label for="exampleFormControlInput1" class="form-label">Comment</label>
                                                <textarea class="form-control" name="jsonR[comment]" rows="3" placeholder="" required></textarea>
                                            </div>

                                            <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <?php
                                                if (!empty($session)) {
                                                    ?>
                                                    <input type="hidden" name="jsonR[user_id]" value="<?= Auth::user()->id ?>">
                                                    <input type="hidden" name="jsonR[vendor_id]" value="<?= $vendorId ?>">
                                                    <button type="submit" name="submitreviews" class="btn btn-secondary">Submit</button><?php
                                                } else {
                                                    ?>
                                                    <a href="<?= url('customer-login') ?>"><button type="button" class="btn btn-secondary">Login and submit</button></a>
                                                <?php } ?>
                                            </div>

                                        </div>
                                    </form>

                                    <!--                                    <div class="wd100">

                                                                            <div class="d-flex __wrirewInfo align-items-center">
                                                                                <div class="flex-shrink-0">
                                                                                    <img src="{{url('/user')}}/images/male-avatar.jpg"> </div>
                                                                                <div class="flex-grow-1 ms-3 __dcrpgltetr">
                                                                                    <h6>John Martin</h6>
                                                                                    <span>johnmartin345@gmail.com</span>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                        <div class="wd100">
                                                                            <div class="wd100 __starRate">
                                                                                <div class="___staricon">
                                                                                    <a href="#"><i class="fa fa-star __acv" aria-hidden="true"></i></a>
                                                                                    <a href="#"><i class="fa fa-star __acv" aria-hidden="true"></i></a>
                                                                                    <a href="#"><i class="fa fa-star __acv" aria-hidden="true"></i></a>
                                                                                    <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                                    <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                                </div>
                                                                                <div class="__starRtvlu">Review On Monday 12/09/21</div>
                                                                            </div>

                                                                            <div class="wd100 ___rulreviewWrp">
                                                                                <div class=" __rulreview">
                                                                                    <p>Lorem Ipsum Dolor Sit Amet, Consetetur Sadipscing Elitr, Sed Diam Nonumy Eirmod Tempor Invidunt Ut Labore Et Dolore Magna Aliquyam Erat, Sed Diam Voluptua. At Vero Eos Et Accusam Et Justo Duo Dolores Et Ea Rebum. Stet Clita Kasd Gubergren, No Sea Takimata Sanctus Est Lorem Ipsum Dolor Sit Amet. </p>
                                                                                </div>

                                                                                <div class="__rulreview_bl2">
                                                                                    <h5>Response From Vendor on 12/09/21</h5>
                                                                                    <p>Lorem Ipsum Dolor Sit Amet, Consetetur Sadipscing Elitr, Sed Diam Nonumy Eirmod Tempor Invidunt Ut Labore Et Dolore Magna Aliquyam Erat, Sed Diam Voluptua. At Vero Eos Et Accusam Et Justo Duo Dolores Et Ea Rebum. Stet Clita Kasd Gubergren, No Sea Takimata Sanctus Est Lorem Ipsum Dolor Sit Amet. Lorem Ipsum Dolor Sit Amet, Consetetur Sadipscing Elitr,</p>
                                                                                </div>
                                                                            </div>

                                                                        </div>-->



                                </div>

                            </div>
                        </div>

                        <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">

                            <div class="wd100 __epd_contact">
                                <div class="wd100 __epd_contact_boz">
                                    <h3><?= $vendor->first_name . ' ' . $vendor->last_name ?></h3>

                                    <div class="wd100 d-flex __liloctnWrp">
                                        <div class="flex-shrink-0"> <img src="{{url('/user')}}/images/location.svg" class="mr-2"> </div>
                                        <div class="flex-grow-1  "> <a href="javascript:void(0)"> <?= !empty($vendorDetails->address) ? $vendorDetails->address : 'location not updated' ?></a> </div>
                                    </div>

                                    <div class="wd100 d-flex __liloctnWrp __licallWrp">
                                        <div class="flex-shrink-0"> <img src="{{url('/user')}}/images/call_icon.svg" class="mr-2"> </div>
                                        <div class="flex-grow-1 "> <a href="tel:+<?= !empty($vendor->phone) ? $vendor->phone : '' ?>">+<?= !empty($vendor->phone) ? $vendor->phone : '' ?></a> </div>
                                    </div>

                                    <div class="wd100 d-flex __liloctnWrp __licallWrp">
                                        <div class="flex-shrink-0"> <img src="{{url('/user')}}/images/call_icon.svg" class="mr-2"> </div>
                                        <div class="flex-grow-1 "> <a href="tel:+<?= !empty($vendor->phone_office) ? $vendor->phone_office : '' ?>">+<?= !empty($vendor->phone_office) ? $vendor->phone_office : '' ?></a> </div>
                                    </div>
                                </div>


                                <div class="wd100 __epd_contact_froBz __write_review ">
                                    <form method="post">
                                        @csrf
                                        <div class="row">
                                            <input type="hidden" name="jsonR[vendor_id]" value="<?= $vendorId ?>">
                                            <div class="mb-3 col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                <label for="exampleFormControlInput1" class="form-label">Name</label>
                                                <input type="text" name="jsonR[name]" value="<?= !empty(Auth::user()->first_name) ? Auth::user()->first_name : '' ?>" class="form-control" required="" placeholder="">
                                            </div>

                                            <div class="mb-3 col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                <label for="exampleFormControlInput1" class="form-label">Email</label>
                                                <input type="email" name="jsonR[email]" value="<?= !empty(Auth::user()->email) ? Auth::user()->email : '' ?>" class="form-control" required="" placeholder="">
                                            </div>
                                            <div class="mb-3 col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                <label for="exampleFormControlInput1" class="form-label">Phone</label>
                                                <input type="number" name="jsonR[phone]" value="<?= !empty(Auth::user()->phone) ? Auth::user()->phone : '' ?>" class="form-control" required="" placeholder="">
                                            </div>


                                            <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <label for="exampleFormControlInput1" class="form-label">Comment</label>
                                                <textarea class="form-control" name="jsonR[comment]" rows="3" required="" placeholder=""></textarea>
                                            </div>

                                            <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <button type="submit" name="contacsussubmit" class="btn btn-secondary float-end">Submit</button>
                                            </div>

                                        </div>
                                    </form>

                                </div>


                            </div>
                        </div>


                        <div class="tab-pane fade" id="nav-team" role="tabpanel" aria-labelledby="nav-team-tab">

                            <div class="wd100  __epdteamWrp">

                                <div class="__rluLGrid row row-cols-1 row-cols-sm-3 row-cols-md-4 row-cols-lg-6">
                                    <?php
                                    $baseDir = Config::get('constants.HOME_DIR');
                                    $Sql = "SELECT first_name,last_name,profile_picture FROM `users`  WHERE parent_id='$vendorId'";
                                    $team = App\Database::select($Sql);
                                    if (!empty($team)) {
                                        for ($i = 0; $i < count($team); $i++) {
                                            $d = $team[$i];

                                            $baseFile = "storage/" . $d->profile_picture;
                                            if (is_file($baseDir . $baseFile)) {
                                                $d->profile_picture = url($baseFile);
                                            } else {
                                                $d->profile_picture = url(Config::get('constants.DEFAULT_LOGO_VENDOR'));
                                            }
                                            ?>
                                            <div class="__teaminfoBz">
                                                <img class="img-fluid" src="{{$d->profile_picture}}">
                                                <h6>Kendal Gay</h6>
                                            </div>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <div style="width: 100%;" class="__teaminfoBz">
                                            <h6>Team not found.</h6>
                                        </div>
                                    <?php } ?>






                                </div>

                            </div>

                        </div>






                    </div>

                </div>



            </div>

            <div class="__evpRtewgt">

                <div class="wd100 __evpRtewgtInfoB">
                    <?php if (empty($session)) {
                        ?>
                        <a href="javascript:void(0)" onclick="window.location = '<?= url('customer-login') ?>'" class="__bntCscy __bntsendfq" >Send RFQ</a>
                        <a style="background: #009688;" href="javascript:void(0)" onclick="window.location = '<?= url('customer-login') ?>'" class="__bntCscy __bntsendfq" >Create Event</a>
                        <?php
                    } else {
                        ?>
                        <a href="javascript:void(0)" class="__bntCscy __bntsendfq" data-bs-toggle="modal" data-bs-target="#exampleModal">Send RFQ</a>
                        <a style="background: #009688;" href="javascript:void(0)" class="__bntCscy __bntsendfq" data-bs-toggle="modal" data-bs-target="#createNewEventModal">Create Event</a>
                    <?php } ?>


                    <a href="javascript:void(0)" onclick="document.getElementById('nav-contact-tab').click();" class="__bntCpry __bntsendfq"> Message Vendor</a>
                </div>



                <div class="wd100 __evpRtewgtInfo2Sx">

                    <div class="wd100 __rluLGdrps">

                        <div class="d-flex align-items-center">
                            <div class="flex-shrink-0"> <img src="{{$vendor->profile_picture_logo}}" class="__brnd mr-3"> </div>
                            <div class="flex-grow-1 ms-3">
                                <h5><a href="javascript:void(0)"><?= $vendor->first_name . ' ' . $vendor->last_name ?></a></h5> </div>
                        </div>
                        <div class="wd100 d-flex __liloctnWrp ">
                            <div class="flex-shrink-0"> <img src="{{url('/user')}}/images/location.svg" class="mr-2"> </div>
                            <div class="flex-grow-1  "> <a href="javascript:void(0)"> <?= !empty($vendorDetails->address) ? $vendorDetails->address : 'location not updated' ?></a> </div>
                        </div>
                        <div class="wd100 d-flex __liloctnWrp __licallWrp">
                            <div class="flex-shrink-0"> <img src="{{url('/user')}}/images/call_icon.svg" class="mr-2"> </div>
                            <div class="flex-grow-1 "> <a href="tel:+<?= !empty($vendor->phone) ? $vendor->phone : '' ?>">+<?= !empty($vendor->phone) ? $vendor->phone : '' ?> </div>
                        </div>
                    </div>

                    <div class="_social_icons_top wd100 __evpRtewgtInfo2social">
                        <?php if (!empty($vendorDetails->facebook)) { ?>
                            <a target="_blank" href="<?= $vendorDetails->facebook ?>"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
                        <?php }if (!empty($vendorDetails->twiter)) { ?>
                            <a target="_blank" href="<?= $vendorDetails->twiter ?>"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                        <?php }if (!empty($vendorDetails->linkedin)) { ?>
                            <a target="_blank" href="<?= $vendorDetails->linkedin ?>"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a>
                        <?php } if (!empty($vendorDetails->linkedin)) { ?>
                            <a target="_blank" href="<?= $vendorDetails->business_website ?>"><i class="fab fa-youtube" aria-hidden="true"></i></a>
                        <?php } if (!empty($vendorDetails->instagram)) { ?>
                            <a target="_blank" href="<?= $vendorDetails->instagram ?>"><i class="fab fa-instagram" aria-hidden="true"></i></a>
                        <?php } ?>
                    </div>


                </div>



            </div>



        </div>


    </div>

</section>
<!-- Modal -->
<?php if (!empty($session)) { ?>
    <div class="modal fade __sendrfqPop" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> </h5>

                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <h5>Get the most accurate pricing and availability info by sharing some of your wedding details with this vendor.</h5>

                    <div class="row">
                        <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <a style="background: #009688;width: 15%;float: right;"  href="javascript:void(0)" class="__bntCscy __bntsendfq" id="frommodalcreateeventBtion" xxdata-bs-toggle="modal" xxdata-bs-target="#createNewEventModal">Create Event</a>
                        </div>
                        <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label">Event Name</label>
                            <select class="form-control " id="event_id" >
                                <option json="" value="">--select--</option>
                                <?php
                                $Sql = "SELECT * FROM `vendor_events` WHERE status =0 AND user_id='$session->id'";
                                $dArray = App\Database::select($Sql);
                                for ($i = 0; $i < count($dArray); $i++) {
                                    $d = $dArray[$i];
                                    ?>
                                    <option json="<?= base64_encode(json_encode($d)) ?>" value="<?= $d->id ?>"><?= $d->event_name . ' - ' . date('d M-Y', strtotime($d->event_date)) ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label">Event Name</label>
                            <input type="email" class="form-control" id="rfq_event_name" readonly value="" placeholder="">
                        </div>
                        <div class="mb-3 col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label">What's your name?</label>
                            <input type="email" class="form-control" id="rfq_first_name" readonly value="<?= $session->first_name ?>" placeholder="First Name">
                        </div>

                        <div class="mb-3 col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label">&nbsp;</label>
                            <input type="email" class="form-control" id="rfq_second_name" readonly value="<?= $session->last_name ?>" placeholder="Second Name">
                        </div>

                        <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label">What's your email?</label>
                            <input type="email" class="form-control" id="rfq_email" readonly value="<?= $session->email ?>" readonly placeholder="Email">
                        </div>

                        <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label">What's Your Event Date?</label>
                            <input type="date" class="form-control" id="rfq_wedding_date" readonly placeholder="Wedding Data">
                        </div>




                        <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label"> How many guests do you plan to have?</label>
                            <input type="number" class="form-control" id="rfq_no_of_guests" readonly placeholder="Number Of Guests">
                        </div>
                        <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label">Note</label>
                            <textarea class="form-control" id="rfq_quick_note" placeholder="Hello! My partner and I are getting married and we're interested in learning more about pricing and services." rows="3"></textarea>
                        </div>


                    </div>



                </div>
                <div class="modal-footer">

                    <button id="usersendRFQ" type="button" class="btn btn-primary __outlinbnt">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade __sendrfqPop" id="createNewEventModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <h5>Get the most accurate pricing and availability info by sharing some of your wedding details with this vendor.</h5>

                    <div class="row">


                        <div class="mb-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label">Event Name</label>
                            <input type="email" class="form-control" id="event_name" value="" placeholder="">
                        </div>

                        <div class="mb-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label">Occasions</label>
                            <select class="form-control " id="occasion_id" >

                                <?php
                                //  $Sql = "SELECT VC.*,O.name AS occasions_name FROM `vendor_category` VC LEFT JOIN occasions O ON O.id=VC.category_id WHERE VC.user_id='$vendorId' AND O.name IS NOT NULL";
                                // $Sql = "SELECT VC.*,O.name AS occasions_name FROM `vendor_occasions` VC LEFT JOIN occasions O ON O.id=VC.occasion_id WHERE VC.user_id='$vendorId'";
                                $Sql = "SELECT * FROM `occasions` WHERE 1"; //
                                $dArray = App\Database::select($Sql);
                                for ($i = 0; $i < count($dArray); $i++) {
                                    $d = $dArray[$i];
                                    ?>
                                    <option value="<?= $d->id ?>"><?= $d->name ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="mb-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label">Services</label>
                            <select class="form-control selectpicker" multiple="multiple" data-live-search="true" id="cat_sel_id" >
                                <?php
                               // $Sql = "SELECT VC.*,C.name AS category_name  FROM `vendor_category` VC LEFT JOIN categories C ON C.id=VC.category_id WHERE `user_id` = '$vendorId'";
                                //$dArray = App\Database::select($Sql);
                              $cattttt=  \App\Helpers\LibHelper::GetvendorCategory();
                                for ($i = 0; $i < count($cattttt); $i++) {
                                    $d = $cattttt[$i];
                                    ?>
                                    <option <?= in_array($d->id, $catId) ? 'selected' : '' ?> value="<?= $d->id ?>"><?= $d->name ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="mb-3 col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label">What's your name?</label>
                            <input type="email" class="form-control" value="<?= $session->first_name ?>" readonly placeholder="First Name">
                        </div>

                        <div class="mb-3 col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label">&nbsp;</label>
                            <input type="email" class="form-control" value="<?= $session->last_name ?>" readonly placeholder="Second Name">
                        </div>

                        <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label">What's your email?</label>
                            <input type="email" class="form-control" id="email" value="<?= $session->email ?>" readonly placeholder="Email">
                        </div>

                        <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label">What's your event date?</label>
                            <input type="date" class="form-control" id="wedding_date" placeholder="Wedding Data">
                        </div>


                        <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-check">
                                <input id="flexible_wedding_date" class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                <label class="form-check-label" for="flexCheckDefault">
                                    My Event Date Is Flexible
                                </label>
                            </div>
                        </div>

                        <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label"> How many guests do you plan to have?</label>
                            <input type="number" class="form-control" id="no_of_guests" placeholder="Number Of Guests">
                        </div>


                        <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label"> Do you have a venue picked out?</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" value="Yes, I have picked a venue" checked name="venue_picked" id="venuepicked">
                                <label class="form-check-label" for="venuepicked">
                                    Yes, I have picked a venue
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" value="No, I am still looking for a venue" name="venue_picked" id="venuepicked-1">
                                <label class="form-check-label" for="venuepicked-1">
                                    No, I'm still looking for a venue
                                </label>
                            </div>
                        </div>

                        <div class="mb-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" id="venue_name_div">
                            <label for="exampleFormControlInput1" class="form-label"> Venue Name</label>
                            <input type="text" class="form-control" id="venue_name" placeholder="Venue Name">
                        </div>
                    </div>

                    <div class="row __anrQrusl" id="morequestiondiv" style="display: none;">

                        <div class="mb-3 col-lg-6 col-md-12 col-sm-12 col-xs-12">

                            <label for="exampleFormControlInput1" class="form-label">How long do you need wedding photography?</label>
                            <select class="form-select" id="start_time" aria-label="Default select example">
                                <option value="">Event Start Time</option>
                                <option value="10:00">10:00</option>
                                <option value="11:00">11:00</option>
                                <option value="12:00">12:00</option>
                                <option value="13:00">13:00</option>
                                <option value="14:00">14:00</option>
                                <option value="15:00">15:00</option>
                                <option value="16:00">16:00</option>

                            </select>
                        </div>
                        <div class="mb-3 col-lg-6 col-md-12 col-sm-12 col-xs-12">

                            <label for="exampleFormControlInput1" class="form-label">&nbsp;</label>
                            <select class="form-select" id="no_of_hours" aria-label="Default select example">
                                <option value="" selected>Number Of Hours</option>
                                <option value="5">05</option>
                                <option value="6">06</option>
                                <option value="7">07</option>
                                <option value="8">08</option>
                                <option value="9">09</option>
                                <option value="10">10 </option>
                            </select>
                        </div>



                        <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label">Which additional services are you interested in?</label>

                            <div class="form-check">
                                <input class="form-check-input  additional_services" type="checkbox" value="1" title="Second Photographer (another photographer to provide additional coverage on your big day)" id="flexCheckDefault1a">
                                <label class="form-check-label" for="flexCheckDefault1a">
                                    Second Photographer (another photographer to provide additional coverage on your big day)
                                </label>
                            </div>

                            <div class="form-check">
                                <input class="form-check-input additional_services" type="checkbox" title="Nondigital Products (wedding album, parent's album, heirloom boxes, prints, etc.)" value="2" id="flexCheckDefault2b">
                                <label class="form-check-label" for="flexCheckDefault2b">
                                    Nondigital Products (wedding album, parent's album, heirloom boxes, prints, etc.)
                                </label>
                            </div>


                            <div class="form-check">
                                <input class="form-check-input additional_services" type="checkbox" value="3" title="Engagement Session" id="flexCheckDefault2c">
                                <label class="form-check-label" for="flexCheckDefault2c">
                                    Engagement Session
                                </label>
                            </div>

                        </div>


                        <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label">What is your estimated budget for photography services?</label>

                            <div class="form-check">
                                <input class="form-check-input" minbudget="7000" maxbudget="0" name="budget" type="radio" checked value="1" id="flexCheckDefault11a">
                                <label class="form-check-label" for="flexCheckDefault11a">
                                    Over AED 7,000
                                </label>
                            </div>

                            <div class="form-check">
                                <input class="form-check-input" minbudget="6000" maxbudget="7000" name="budget" type="radio" value="2" id="flexCheckDefault12b">
                                <label class="form-check-label" for="flexCheckDefault12b">
                                    AED 6,000 - AED 7,000
                                </label>
                            </div>


                            <div class="form-check">
                                <input class="form-check-input" minbudget="5000" maxbudget="6000" name="budget" type="radio" value="3" id="flexCheckDefault12c">
                                <label class="form-check-label" for="flexCheckDefault12c">
                                    AED 5,000 - AED 6,000
                                </label>
                            </div>


                            <div class="form-check">
                                <input class="form-check-input" minbudget="4000" maxbudget="5000" name="budget" type="radio" value="4" id="flexCheckDefault12d">
                                <label class="form-check-label" for="flexCheckDefault12d">
                                    AED 4,000 - AED 5,000
                                </label>
                            </div>



                            <div class="form-check">
                                <input class="form-check-input" minbudget="3000" maxbudget="4000" name="budget" type="radio" value="5" id="flexCheckDefault12e">
                                <label class="form-check-label" for="flexCheckDefault12e">
                                    AED 3,000 - AED 4,000
                                </label>
                            </div>


                            <div class="form-check">
                                <input class="form-check-input" minbudget="2000" maxbudget="3000" name="budget" type="radio" value="6" id="flexCheckDefault12f">
                                <label class="form-check-label" for="flexCheckDefault12f">
                                    AED 2,000 - AED 3,000
                                </label>
                            </div>

                            <div class="form-check">
                                <input class="form-check-input" minbudget="0" maxbudget="2000" name="budget" type="radio" value="7" id="flexCheckDefault12o">
                                <label class="form-check-label" for="flexCheckDefault12o">
                                    Less than AED 2,000
                                </label>
                            </div>

                            <div class="form-check">
                                <input class="form-check-input" minbudget="0" maxbudget="0" name="budget" type="radio" value="8" id="flexCheckDefault12x">
                                <label class="form-check-label" for="flexCheckDefault12x">
                                    I'm not sure yet
                                </label>
                            </div>

                        </div>


                        <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label">Would you like to meet this vendor over video chat?</label>

                            <div class="form-check">
                                <input class="form-check-input" name="video_chat" type="radio" value="Yes please" id="flexCheckDefault1x1">
                                <label class="form-check-label" for="flexCheckDefault1x1">
                                    Yes please!
                                </label>
                            </div>

                            <div class="form-check">
                                <input class="form-check-input" name="video_chat" checked type="radio" value="Maybe later" id="flexCheckDefault1x2">
                                <label class="form-check-label" for="flexCheckDefault1x2">
                                    Maybe later
                                </label>
                            </div>

                        </div>
                        <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <p>Video chats can help you easily connect with wedding vendors during these uncertain times. <br />
                                Once you submit your request, this vendor will reach out to schedule your chat.</p>
                        </div>


                        <div class="mb-3 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="exampleFormControlInput1" class="form-label">Add a quick note</label>
                            <p>Vendors love learning more about what you're looking for. </p>
                            <textarea class="form-control" id="quick_note" placeholder="Hello! My partner and I are getting married and we're interested in learning more about pricing and services." rows="3"></textarea>
                            <p>By using this service, you agree that your email and other information may be shared with the vendor.
                                Please see our Privacy Policy and Terms of Use for details.</p>
                        </div>


                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary __anermoreQs " onclick="document.getElementById('morequestiondiv').style.display = 'inline-flex';
                                $(this).hide();">Answer More Questions </button>
                    <button id="addnewwvwntbtn" type="button" class="btn btn-primary __outlinbnt">Create Event</button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
@include('user.view.includes.footer')

<style>
    .modal-body {

        overflow-y: auto;
    }
</style>


<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>



<script>
                        $("input[name='venue_picked']").change(function () {
                            var val = $(this).val();
                            if (val == 'Yes, I have picked a venue') {
                                $('#venue_name_div').show();
                            } else {
                                $('#venue_name_div').hide();
                            }
                        });
                        $('#frommodalcreateeventBtion').click(function () {
                            $('#exampleModal').modal('hide');
                            setTimeout(function () {
                                $('#createNewEventModal').modal('show');
                            }, 250);


                        });
                        $('#event_id').on('change', function () {
                            var json = $('#event_id option:selected').attr('json');
                            var data = jQuery.parseJSON(atob(json));
                            $('#rfq_event_name').val(data.event_name);
                            $('#rfq_wedding_date').val(data.event_date);
                            $('#rfq_no_of_guests').val(data.number_of_attendees);

                        });
                        $('#usersendRFQ').click(function () {
                            var form = new FormData();
                            form.append('_token', CSRF_TOKEN);
                            form.append('json[event_id]', $('#event_id').val());
                            form.append('json[quick_note]', $('#rfq_quick_note').val());
                            form.append('json[vendor_id]', $('#vendorId').val());

                            if ($('#event_id').val() == '') {
                                $('#event_id').css('border-color', 'red');
                                $('#event_id').focus();
                                return false;
                            } else {
                                $('#event_id').css('border-color', '');
                            }
                            if ($('#rfq_quick_note').val() == '') {
                                $('#rfq_quick_note').css('border-color', 'red');
                                $('#rfq_quick_note').focus();
                                return false;
                            } else {
                                $('#rfq_quick_note').css('border-color', '');
                            }
                            $.confirm({
                                title: 'Are You Sure You Want To Submit ?',
                                content: false,
                                type: 'green',
                                typeAnimated: true,
                                buttons: {
                                    confirm: {
                                        text: 'Submit',
                                        btnClass: 'btn-secondary',
                                        action: function () {
                                            var json = ajaxpost(form, "/senduserRFQ");
                                            try {
                                                var json = jQuery.parseJSON(json);
                                                alertSimple(json.messages);
                                                if (json.status == true) {
                                                    setTimeout(function () {
                                                        window.location = json.url;
                                                    }, 1000);
                                                }
                                            } catch (e) {
                                                alert(e);
                                            }
                                        }
                                    },
                                    cancel: {
                                        text: 'Cancel',
                                        btnClass: 'btn-danger',
                                        action: function () {
                                        }
                                    },
                                }
                            });
                        });
                        $('#addnewwvwntbtn').click(function () {

                            var form = new FormData();
                            form.append('_token', CSRF_TOKEN);
                            form.append('catId', $("#cat_sel_id").val());

                            form.append('json[vendor_id]', 0);


                            form.append('json[event_name]', $('#event_name').val());
                            form.append('json[occasion_id]', $('#occasion_id').val());

                            form.append('json[event_date]', $("#wedding_date").val());
                            form.append('json[date_flexible]', $('#flexible_wedding_date').is(":checked") ? 'Yes' : 'No');
                            form.append('json[number_of_attendees]', $('#no_of_guests').val());
                            form.append('json[picked_a_venue]', $('input[name="venue_picked"]:checked').val());
                            form.append('json[event_location]', $('#venue_name').val());

                            form.append('json[event_time]', $('#start_time').val());
                            form.append('json[number_of_hours]', $('#no_of_hours').val());



                            $('.additional_services:checked').each(function (i) {
                                form.append('additional_services[' + i + '][service_id]', $(this).val());
                                form.append('additional_services[' + i + '][title]', $(this).attr('title'));

                            });
                            if ($('#cat_sel_id').val() == null) {
                                $('.dropdown-toggle').css('border-color', 'red');
                                $('#cat_sel_id').focus();
                                return false;
                            } else {
                                $('.dropdown-toggle').css('border-color', '');
                            }
//        form.append('json[budget]', $('input[name="budget"]:checked').val());
                            form.append('json[min_budget]', $('input[name="budget"]:checked').attr('minbudget'));
                            form.append('json[max_budget]', $('input[name="budget"]:checked').attr('maxbudget'));
                            form.append('json[video_chat]', $('input[name="video_chat"]:checked').val());
                            form.append('json[quick_note]', $('#quick_note').val());
                            if ($('#event_name').val() == '') {
                                $('#event_name').css('border-color', 'red');
                                $('#event_name').focus();
                                return false;
                            } else {
                                $('#event_name').css('border-color', '');
                            }


                            if ($('#wedding_date').val() == '') {
                                $('#wedding_date').css('border-color', 'red');
                                $('#wedding_date').focus();
                                return false;
                            } else {
                                $('#wedding_date').css('border-color', '');
                            }
                            if ($('#no_of_guests').val() == '') {
                                $('#no_of_guests').css('border-color', 'red');
                                $('#no_of_guests').focus();
                                return false;
                            } else {
                                $('#no_of_guests').css('border-color', '');
                            }


                            $.confirm({
                                title: 'Are You Sure You Want To Submit ?',
                                content: false,
                                type: 'green',
                                typeAnimated: true,
                                buttons: {
                                    confirm: {
                                        text: 'Submit',
                                        btnClass: 'btn-secondary',
                                        action: function () {
                                            var json = ajaxpost(form, "/vendor-details");
                                            try {
                                                var json = jQuery.parseJSON(json);
                                                alertSimple(json.messages);
                                                if (json.status == true) {
                                                    setTimeout(function () {
                                                        window.location = json.url;
                                                    }, 1000);
                                                }
                                            } catch (e) {
                                                alert(e);
                                            }
                                        }
                                    },
                                    cancel: {
                                        text: 'Cancel',
                                        btnClass: 'btn-danger',
                                        action: function () {
                                        }
                                    },
                                }
                            });
                        });</script>



<script>
    var swiper = new Swiper('.__epdSlr', {
        slidesPerView: 5,
        spaceBetween: 15,
        loop: true,
        navigation: {
            nextEl: '.__epdSlr-next',
            prevEl: '.__epdSlr-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 2,
                spaceBetween: 15,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 15,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10,
            }
        },
        autoplay: {
            delay: 2000,
        }
    });</script>
<script>
    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        $('#back-to-top').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 400
                    )
                    ;
            return false;
        });
    });
</script>

<!--<link rel='stylesheet' href='{{url('/public/user')}}/css/jquery.fancybox.min.css'>
<script src='{{url('/public/user')}}/js/jquery.fancybox.min.js'></script>-->

<script>
    $(document).ready(function () {

        $('.gallery > div.bg').each(function () {
            $(this).wrapAll('<a href="" data-fancybox="gallery"></a>');
        });
        $('.gallery a').each(function () {
            var link = $(this).children('.bg').css('background-image');
            console.log(link);
            link = link.replace(/(url\(|\)|")/g, '');
            $(this).attr('href', link);
        });
//        $("[data-fancybox]").fancybox({
//            loop: true,
//            buttons: [
//                "zoom",
//                //  "share",
//                "slideShow",
//                "fullScreen",
//                //  "download",
//                "thumbs",
//                "close"
//            ]
//        });
    });

</script>
