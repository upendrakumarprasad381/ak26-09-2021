@include('user.view.includes.header')
<?php
$type = !empty($_GET['type']) ? $_GET['type'] : '';
if ($type == '3') {
    $title = "Event Planner";
    $desc = "Planning an event for any event planner is a long and hectic procedure.The online feature of the event planner account can make your life easier as an event planner regardless of your grade in the market.";
} else if ($type == '4') {
    $title = "Ask Deema’s Shop For Everyone";
    $desc = "Ask Deema shop is created to support event home businesses and minimize their hassle to join the ecommerce world. All you need as features to manage and run an ecommerce shop are provided by Askdeema's You can review the detailed features below:";
} else {
    $title = "Ask Deema’s Vendors For Everyone";
    $desc = "Ask Deema shop is created to support event home businesses and minimize their hassle to join the ecommerce world. All you need as features to manage and run an ecommerce shop are provided by ASKDEEMA. You can review the detailed features below:";
}
?>
<!--inner section -->
<!--Inner Banner -->
<input id="username" style="display:none" type="text" name="fakeusernameremembered">
<input id="password" style="display:none" type="password" name="fakepasswordremembered">
<section class="inner-banner wd100">
    <div class="breadcrumb-area" style="background-image: url({{url('/user/images/bg/vendors-bg.jpg')}})" data-overlay="dark"
         data-opacity="7">
        <div class="container pt-150 pb-150 position-relative">
            <div class="row justify-content-center">

                <div class="col-xl-4">
                    <div class="breadcrumb-title">
                        <h3 class="title">{{$title}}</h3>
                        <span class="sub-title">{{$desc}}</span>

                    </div>
                </div>

                <div class="col-lg-8">
                    <div class="banner-form-w3 wd100">

                        <!-- Register form -->

                        <form action="" onsubmit="return false;" autocomplete="off" method="post">
                            <h5 class="mb-4 text-center">Would you like to connect with us</h5>
                            <div class="form-style">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="sign__input-wrapper mb-25">
                                            <h5>Account Type</h5>
                                            <div class="sign__input-form">
                                                <select id="roll_id" class="form-control">
                                                    <option  value="">--select--</option>
                                                    <option <?= $type == '2' ? 'selected' : '' ?> value="2">Event's Vendor</option>
                                                    <option <?= $type == '3' ? 'selected' : '' ?> value="3">Event Planner</option>
                                                    <option <?= $type == '4' ? 'selected' : '' ?> value="4">Shop Owner</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div class="sign__input-wrapper mb-25">
                                            <h5>First Name</h5>
                                            <div class="sign__input-form">
                                                <input type="text" id="first_name" class="form-control" placeholder="Enter Your First Name">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div class="sign__input-wrapper mb-25">
                                            <h5>Last Name</h5>
                                            <div class="sign__input-form">
                                                <input type="text" id="last_name" class="form-control" placeholder="Enter Your Last Name">

                                            </div>
                                        </div>
                                    </div>






                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div class="sign__input-wrapper mb-25">
                                            <h5>E-mail Address</h5>
                                            <div class="sign__input-form">
                                                <input type="text" id="email" class="form-control"  autocomplete="nope" value="" placeholder="Enter Your E-mail">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div class="sign__input-wrapper mb-25">
                                            <h5>Phone Number</h5>
                                            <div class="__telcoddro"> 
                                                <div class="__codrop sign__input">
                                                    <select name="countryCode" id="phone_code">
                                                        <?php
                                                        $Sql = "SELECT countryname,dial_code FROM `country` GROUP BY dial_code ORDER BY dial_code ASC";
                                                        $data = App\Database::select($Sql);
                                                        for ($i = 0; $i < count($data); $i++) {
                                                            $d = $data[$i];
                                                            ?>
                                                            <option <?= $d->dial_code == '971' ? 'selected' : '' ?> value="<?= $d->dial_code ?>">+<?= $d->dial_code ?></option>
                                                        <?php } ?>

                                                    </select>
                                                </div>
                                                <div class="__telrop sign__input">
                                                    <input type="number" id="phone" placeholder="Enter Your Phone Number" autocomplete="off">
                                                    <i class="fal fa-phone"></i>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>





                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div class="sign__input-wrapper mb-25">
                                            <h5>Office Number</h5>
                                            <div class="__telcoddro"> 
                                                <div class="__codrop sign__input">
                                                    <select name="countryCode" id="phone_office_code">
                                                        <?php
                                                        $Sql = "SELECT countryname,dial_code FROM `country` GROUP BY dial_code ORDER BY dial_code ASC";
                                                        $data = App\Database::select($Sql);
                                                        for ($i = 0; $i < count($data); $i++) {
                                                            $d = $data[$i];
                                                            ?>
                                                            <option <?= $d->dial_code == '971' ? 'selected' : '' ?> value="<?= $d->dial_code ?>">+<?= $d->dial_code ?></option>
                                                        <?php } ?>

                                                    </select>
                                                </div>
                                                <div class="__telrop sign__input">
                                                    <input type="number" id="phone_office" class="form-control" placeholder="Enter Office Number">

                                                </div>
                                            </div> 
                                        </div>
                                    </div>








                                    <div class="col-lg-6 col-md-12 col-sm-12" id="categoryDiv" style="display: <?= $type == '3' ? 'none' : 'block' ?>">
                                        <div class="sign__input-wrapper mb-25">
                                            <h5>Let us know your services categories</h5>
                                            <div class="sign__input-form">
                                                <select id="vendor_category" data-placeholder="Select one" class="form-control select2" multiple="">
                                                    <?php
                                                    $servc = App\Helpers\LibHelper::GetvendorCategory();
                                                    for ($i = 0; $i < count($servc); $i++) {
                                                        $d = $servc[$i];
                                                        ?>  <option value="<?= $d->id ?>"><?= $d->name ?></option><?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-6 col-md-12 col-sm-12" id="occasionsDiv" style="display: <?= $type == '3' ? 'block' : 'none' ?>">
                                        <div class="sign__input-wrapper mb-25">
                                            <h5>Let us know the evets you conduct.</h5>
                                            <div class="sign__input-form">
                                                <select id="vendor_occasions" data-placeholder="Select one" class="form-control select2" multiple="">
                                                    <?php
                                                    $servc = App\Helpers\LibHelper::Getoccasions();
                                                    for ($i = 0; $i < count($servc); $i++) {
                                                        $d = $servc[$i];
                                                        ?>  <option value="<?= $d->id ?>"><?= $d->name ?></option><?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div class="sign__input-wrapper mb-25">
                                            <h5>Choose available emirates</h5>
                                            <div class="sign__input-form">
                                                <select id="emirates" multiple="" class="form-control select2">
                                                    <?php
                                                    $Sql = "SELECT * FROM `emirates`";
                                                    $data = App\Database::select($Sql);
                                                    for ($i = 0; $i < count($data); $i++) {
                                                        $d = $data[$i];
                                                        ?>
                                                        <option <?= $d->id == '2' ? 'selected' : ' ' ?>  value="<?= $d->id ?>"><?= $d->emirate ?></option>
                                                    <?php } ?>

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div> <!-- End Row -->





                                <div class="row">
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div class="sign__input-wrapper mb-25">
                                            <h5>Password</h5>
                                            <div class=" sign__input">
                                                <input type="password" id="password-new" autocomplete="new-password" class="form-control" placeholder="Enter your Password">
                                                <i class="fal fa-lock"></i>

                                                <div class="wd100">
                                                    <span onclick="showmypassword();" toggle="#user_password" class="fa fa-fw field-icon toggle-password fa-eye" aria-hidden="true"></span>
                                                    <span onclick="showmypassword();" toggle="#user_password" class="fa fa-fw field-icon toggle-password fa-eye-slash" aria-hidden="true"></span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 sign__input-wrapper mb-25">
                                        <h5>Confirm password</h5>
                                        <div class="sign__input">
                                            <input type="password" id="cpassword" placeholder="Password">
                                            <i class="fal fa-lock"></i>

                                            <div class="wd100">
                                                <span onclick="showmypassword();" toggle="#user_password" class="fa fa-fw field-icon toggle-password fa-eye" aria-hidden="true"></span>
                                                <span onclick="showmypassword();" toggle="#user_password" class="fa fa-fw field-icon toggle-password fa-eye-slash" aria-hidden="true"></span>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-lg-6 col-md-12 col-sm-12 sign__input-wrapper mb-25" id="otpcode" style="display: none;">
                                        <h5>OTP Code <a id="resendBtn" style="float: right;" onclick="resentOtp();" href="javascript:void(0)">Resend</a></h5>
                                        <div class="sign__input">
                                            <input type="number" id="otpcodeVal" placeholder="">
                                            <i class="fal fa-lock"></i>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 sign__input-wrapper mb-25">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" onclick="additionalInformation();" id="showpsw">
                                            <label class="form-check-label" for="showpsw">
                                                Additional Information
                                            </label>
                                        </div>
                                    </div> 

                                    <div class="row" id="additionalInformationdiv" style="display: none;">

                                        <div class="col-lg-6 col-md-12 col-sm-12">
                                            <div class="sign__input-wrapper mb-25">
                                                <h5>Business website  </h5>
                                                <div class="sign__input-form">
                                                    <input type="text" id="business_website" class="form-control" placeholder="">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12">
                                            <div class="sign__input-wrapper mb-25">
                                                <h5>Instagram  </h5>
                                                <div class="sign__input-form">
                                                    <input type="text" id="instagram" class="form-control" placeholder="">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12">
                                            <div class="sign__input-wrapper mb-25">
                                                <h5>Facebook</h5>
                                                <div class="sign__input-form">
                                                    <input type="text" id="facebook" class="form-control" placeholder="">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12">
                                            <div class="sign__input-wrapper mb-25">
                                                <h5>Linkedin</h5>
                                                <div class="sign__input-form">
                                                    <input type="text" id="linkedin" class="form-control" placeholder="">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12">
                                            <div class="sign__input-wrapper mb-25">
                                                <h5>Twiter</h5>
                                                <div class="sign__input-form">
                                                    <input type="text" id="twiter" class="form-control" placeholder="">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12">
                                            <div class="sign__input-wrapper mb-25">
                                                <h5>Address</h5>
                                                <div class="sign__input-form">
                                                    <input type="text" id="address" class="form-control"placeholder="">
                                                </div>
                                            </div>
                                        </div>

                                    </div> <!-- End Row -->


                                    <div class="col-lg-12 col-md-12 col-sm-12 sign__input-wrapper mb-25">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox"  id="tpolicy">
                                            <label class="form-check-label" for="tpolicy">
                                                <a target="_blank" href="<?= url('privacy-policy') ?>"> I Agree To   Privacy Policy  And Terms Of Use. </a></p>
                                            </label>
                                        </div>
                                    </div>






                                </div>



                                <div class="wd100 text-center">
                                    <button type="button" id="vendor-signup" class="btn btn-style btn-primary __singvnder">Sign Up </button>
                                </div>

                                <div class="wd100">
                                    <div class="sign__new text-center mt-20" style="display: none;">
                                        <h5>Or Signup</h5>
                                        <div class="social_icons">
                                            <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
                                            <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
                                            <a target="_blank" href="#"><i class="fab fa-linkedin-in"></i></a>
                                            <a target="_blank" href="#"><i class="fab fa-instagram"></i></a>
                                        </div>
                                    </div> 
                                </div>

                                <div class="wd100">
                                    <p class="remember-sign-login mt-3 text-center"><a href="<?= url('/login') ?>">Already a Member Of  Ask Deema Log In </a>
                                    </p>
                                </div>
                                
                                <div class="wd100 text-center">
						        	<a href="{{url('customer-signup')}}" class="__custmLoginBtnN">Create New Customer Account</a>
        						</div>
        				
                                
                                
                            </div>
                        </form>
                        <!-- //Register form -->

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

</section>
<div class="clearfix"></div>

<!-- End Inner Banner -->


<!-- Login Form -->



<!--// End Login Form -->

<!-- Event Planners-->
<section class="eventplanner-one eventplanner--one__home-two thm-gray-bg service-one__service-page wd100">
    <div class="vendor-one__block">
        <!-- <div class="block-title text-center">
          <h2 class="block-title__title text-uppercase">Top Wedding Vendor Categories</h2>
            <div class="block-title__line"></div>
        </div> -->
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-user"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Vendor Manager</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->

                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-calendar"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Calender</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->

                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-budget"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Budgeter</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-contract"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Contracts</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-clipboard"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Checklist</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-recruitment"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Leads</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-contract-1"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Proposals</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-browser"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Create Event Web Page</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-blueprint"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Floor Plans</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-receipt"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Invoices</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-chat"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Conversations</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-guest-list"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Guest List</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
                <div class="col-lg-3">
                    <div class="flex-fill  eventplanner-thumbnail-second-icon">
                        <div class="eventplanner-thumbnail-second-icon-circle  ">
                            <i class="flaticon-contact-form"></i>
                        </div>
                        <h3 class="eventplanner-one__title"><a href="#">Form Builder</a></h3>
                        <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                    </div>
                </div><!-- /.col-lg-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.service-one__block -->
</section><!-- /.service-one -->
















<style>

</style>





@include('user.view.includes.footer')
<script>
    $('#roll_id').change(function () {
        var roll_id = $('#roll_id').val();
        if (roll_id == '3') {
            $('#occasionsDiv').show();
            $('#categoryDiv').hide();
        } else {
            $('#occasionsDiv').hide();
            $('#categoryDiv').show();
        }
    });
    var isValidate = 0;
    var optCode = '';
    var emailId = '';
    var phone = '';
    $('#vendor-signup').click(function () {

        var form = new FormData();
        form.append('_token', CSRF_TOKEN);

        var roll_id = $('#roll_id').val();
        var phoneWithcode = $('#phone_code').val() + $("#phone").val();
        form.append('roll_id', roll_id);

        form.append('json[first_name]', $('#first_name').val());
        form.append('json[last_name]', $('#last_name').val());
        form.append('json[email]', $('#email').val());
        form.append('json[phone]', phoneWithcode);
        form.append('json[phone_office]', ($('#phone_office_code').val() + $("#phone_office").val()));
        form.append('json[password]', $('#password-new').val());



        var emirates = $('#emirates').select2('val');
        form.append('details[emirates]', emirates);

        form.append('details[address]', $('#address').val());
        form.append('details[business_website]', $('#business_website').val());
        form.append('details[instagram]', $('#instagram').val());
        form.append('details[facebook]', $('#facebook').val());
        form.append('details[linkedin]', $('#linkedin').val());
        form.append('details[twiter]', $('#twiter').val());



        if ($('#roll_id').val() == '') {
            $('#roll_id').css('border-color', 'red');
            $('#roll_id').focus();
            return false;
        } else {
            $('#roll_id').css('border-color', '');
        }

        if ($('#first_name').val() == '') {
            $('#first_name').css('border-color', 'red');
            $('#first_name').focus();
            return false;
        } else {
            $('#first_name').css('border-color', '');
        }

        if (!validateEmail($('#email').val()) || $('#email').val() == '') {
            $('#email').css('border-color', 'red');
            $('#email').focus();
            return false;
        } else {
            $('#email').css('border-color', '');
        }
        if ($('#phone').val() == '') {
            $('#phone').css('border-color', 'red');
            $('#phone').focus();
            return false;
        } else {
            $('#phone').css('border-color', '');
        }
      

        if (roll_id == '3') {
            var vendor_occasions = $("#vendor_occasions").select2('val');
            form.append('json[vendor_occasions]', vendor_occasions);
            if (vendor_occasions == '' || vendor_occasions == null) {
                $('#vendor_occasions').css('border-color', 'red');
                $('#vendor_occasions').focus();
                return false;
            } else {
                $('#vendor_occasions').css('border-color', '');
            }
        } else {
            var vendor_category = $("#vendor_category").select2('val');
            form.append('json[vendor_category]', vendor_category);
            if (vendor_category == '' || vendor_category == null) {
                $('#vendor_category').css('border-color', 'red');
                $('#vendor_category').focus();
                return false;
            } else {
                $('#vendor_category').css('border-color', '');
            }
        }


        if (emirates == '' || emirates == null) {
            $('#emirates').css('border-color', 'red');
            $('#emirates').focus();
            return false;
        } else {
            $('#emirates').css('border-color', '');
        }

        if ($('#password-new').val() == '') {
            $('#password-new').css('border-color', 'red');
            $('#password-new').focus();
            return false;
        } else {
            $('#password-new').css('border-color', '');
        }

        if ($('#cpassword').val() != $('#password-new').val()) {
            $('#cpassword').css('border-color', 'red');
            $('#cpassword').focus();
            return false;
        } else {
            $('#cpassword').css('border-color', '');
        }
        if (!$('#tpolicy').is(":checked")) {
            alertSimple('Please accept terms and conditions.');
            return false;
        }


        if (optCode) {
            var user_code = parseInt($('#otpcodeVal').val());
            var code = parseInt(atob(optCode)) - 777;
            if (user_code != code) {
                alertSimple('Invalid otp code');
                $('#otpcodeVal').focus();
                return false;
            } else if (user_code == code) {
                form.append('json[email]', emailId);
                form.append('json[phone]', phone);
                isValidate = 1;
            }
        }
        form.append('isValidate', isValidate);
        $.confirm({
            title: 'Are You Sure You Want To Submit ?',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-secondary',
                    action: function () {
                        var json = ajaxpost(form, "/vendor-signup");
                        if(jQuery.parseJSON(json)){
                            json=jQuery.parseJSON(json);
                        }
                        try {
                        console.log(json);
                            alertSimple(json.messages);
                            if (json.status == true) {
                                if (isValidate == false) {
                                    optCode = json.otp;
                                    emailId = $('#email').val();
                                    phone = phoneWithcode;
                                    $('#resendBtn').hide();
                                    setTimeout(function () {
                                        $('#resendBtn').show();
                                    }, 30000);
                                    $('#email,#phone').attr('disabled', true);
                                    $('#otpcode').show();
                                } else {
                               
                                    setTimeout(function () {
                                        if (json.GO_URL) {
                                           window.location = json.GO_URL;
                                        } else {
                                            window.location = '';
                                        }
                                    }, 1000);
                                }
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-danger',
                    action: function () {
                    }
                },
            }
        });

    });
    function showmypassword() {
        var passwordType = $('#password-new').attr('type');
        if (passwordType == 'password') {
            $('.fa-eye-slash').addClass('fa-eye').removeClass('fa-eye-slash');
            $('#password-new,#cpassword').attr('type', 'text');
        } else {
            $('#password-new,#cpassword').attr('type', 'password');
            $('.fa-eye').addClass('fa-eye-slash').removeClass('fa-eye');
        }
    }
    function additionalInformation() {
        if ($('#showpsw').is(':checked')) {
            $('#additionalInformationdiv').show();
        } else {
            $('#additionalInformationdiv').hide();
        }
    }
    function resentOtp() {
        optCode = 0;
        $('#customer-singup').click();
        $('#resendBtn').hide();
        setTimeout(function () {
            $('#resendBtn').show();
        }, 30000);
    }
</script>