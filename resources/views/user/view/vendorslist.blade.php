@include('user.view.includes.header')
<?php
$REDIRECT_QUERY_STRING = !empty($_SERVER['REDIRECT_QUERY_STRING']) ? $_SERVER['REDIRECT_QUERY_STRING'] : '';

$cond = "";
$catId = isset($_GET['catId']) ? $_GET['catId'] : '';
$location = isset($_GET['location']) ? $_GET['location'] : '';
$noofguest = isset($_GET['noofguest']) ? $_GET['noofguest'] : '';
$search = isset($_GET['search']) ? $_GET['search'] : '';

$catIdAr = !empty($catId) ? explode(',', $catId) : [];
$catIdAr = !empty($catIdAr) && is_array($catIdAr) ? $catIdAr : [];
$catIdAr = array_filter($catIdAr);

$occasions = isset($_GET['occasions']) ? $_GET['occasions'] : '';
$eventplaner = isset($_GET['event-planer']) ? $_GET['event-planer'] : '';
?>


<section class="section __innerbanner __event_planners_search" data-overlay="dark" data-opacity="5">
    <div class="container position-relative">
        <h2>Event Planners</h2>
        <h5>What are you looking for ?</h5>
    </div> 
</section>

<?php /*
  <section class="section __inrFlterWrp" >

  <div class="container">
  <form method="get">
  <div class="row">
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 __inrFlrbz">
  <select class="form-select" name="occasions" aria-label="Default select example">
  <option value="">Occasions</option>
  <?php
  $Sql = "SELECT * FROM `occasions` WHERE 1"; //
  $oclist = App\Database::select($Sql);
  for ($i = 0; $i < count($oclist); $i++) {
  $d = $oclist[$i];
  ?>
  <option <?= $occasions == $d->id ? 'selected' : '' ?> value="<?= $d->id ?>"><?= $d->name ?></option>
  <?php } ?>
  </select>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 __inrFlrbz">
  <input type="text" class="form-select" placeholder="Location">
  </div>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 __inrFlrbz">
  <select class="form-select" name="event-planer" aria-label="Default select example">
  <option  value="0">Vendor</option>
  <option <?= $eventplaner == '1' ? 'selected' : '' ?> value="1">Event Planer</option>
  </select>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 __inrFlrbz">
  <button type="submit" class="btn btn-primary">Search</button>
  </div>
  </div>
  </form>
  </div>

  </section>
 */ ?>


<section class="section __rlutlistWrp"> 
    <div class="container">
        <div class="row"> 

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"> 

                <form method="get" >
                    <div class="__rlutLtwigWrp">

                        <div class="mb-3"> 
                            <label>Occasions</label>  
                            <select class="form-select" name="occasions" aria-label="Default select example">
                                <option value="">--select--</option>
                                <?php
                                $Sql = "SELECT * FROM `occasions` WHERE 1"; // 
                                $oclist = App\Database::select($Sql);
                                for ($i = 0; $i < count($oclist); $i++) {
                                    $d = $oclist[$i];
                                    ?>
                                    <option <?= $occasions == $d->id ? 'selected' : '' ?> value="<?= $d->id ?>"><?= $d->name ?></option>
                                <?php } ?>
                            </select> 
                        </div>

                        <div class="mb-3"> 
                            <label>Type</label>  
                            <select class="form-select" name="event-planer" aria-label="Default select example">
                                <option  value="0">Vendor</option>
                                <option <?= $eventplaner == '1' ? 'selected' : '' ?> value="1">Event Planer</option>
                            </select> 
                        </div>

                        <div class="mb-3">
                            <label>Location</label>
                            <select class="form-select" name="location" aria-label="Default select example">
                                <option  value="">--select--</option>
                                <?php
                                $Sql = "SELECT * FROM `emirates`";
                                $data = App\Database::select($Sql);
                                for ($i = 0; $i < count($data); $i++) {
                                    $d = $data[$i];
                                    ?>
                                    <option <?= $d->id == $location ? 'selected' : ' ' ?>  value="<?= $d->id ?>"><?= $d->emirate ?></option>
                                <?php } ?>

                            </select> 
                        </div>

                        <div class="mb-3">
                            <label>No of Guest </label>
                            <input type="number" class="form-control" value="<?= $noofguest ?>" autocomplete="off" placeholder=""> 
                        </div>
                        <input type="hidden" id="catId" name="catId" value="<?= $catId ?>">
                        <div class="mb-3">
                            <label>Category</label> 
                            <select class="form-select select2" id="catIdUpdate" multiple=""  aria-label="Default select example">
                                <?php
                                $cattttt = \App\Helpers\LibHelper::GetvendorCategory();
                                for ($i = 0; $i < count($cattttt); $i++) {
                                    $d = $cattttt[$i];
                                    ?>
                                    <option <?= in_array($d->id, $catIdAr) ? 'selected' : '' ?> value="<?= $d->id ?>"><?= $d->name ?></option>
                                <?php } ?>

                            </select> 
                        </div>
                        <div class="mb-3"> 
                            <label>Budget</label>  
                            <select class="form-select" aria-label="Default select example">
                                <option selected="">Select Budget</option>
                                <option value="1">Budget 0-10000</option>
                                <option value="2">Budget 10000-20000</option>
                                <option value="3">Budget 20000-50000</option>
                                <option value="3">Budget 50000-100000</option>
                                <option value="3">Budget more then 100000</option>
                            </select>
                        </div>



                        <div class="mb-3"> 
                            <label>Vendor/Event Planer Name</label>  
                            <input type="text" class="form-control" value="<?= $search ?>" autocomplete="off" name="search" placeholder=""> 

                        </div>
                        <div class="mb-3"> 
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>






                    </div>
                </form>
            </div> 

            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 __rluListWrp"> 
                <?php
                if (!empty($catIdAr)) {
                    $string = implode(',', $catIdAr);

                    $cond = $cond . " AND U.id IN (SELECT user_id FROM `vendor_category` WHERE category_id IN ($string))";
                }

                if ($eventplaner == '1') {
                    $cond = $cond . " AND RU.role_id = '3'";
                } else {
                    $cond = $cond . " AND RU.role_id = '2'";
                }

                if (!empty($occasions)) {
                    $cond = $cond . " AND U.id IN (SELECT user_id FROM `vendor_occasions` WHERE occasion_id='$occasions')";
                }
                if (!empty($location)) {
                    $cond = $cond . " AND U.id IN (SELECT user_id FROM `vendor_emirate` WHERE emirate_id='$location')";
                }
                if (!empty($search)) {
                     $cond = $cond . " AND (U.first_name  LIKE '%$search%' || U.last_name  LIKE '%$search%')";
                }
             
                $select = "U.id AS user_id,U.first_name,VD.emirates,U.phone,U.profile_picture";
                $join = "  LEFT JOIN vendor_details VD ON VD.user_id=U.id LEFT JOIN role_user RU ON RU.user_id=U.id";
                $Sql = "SELECT  $select FROM `users` U $join WHERE U.status=0 $cond AND U.parent_id IS NULL GROUP BY U.id";
                $list = \App\Database::select($Sql);
                ?>
                <div class="wd100 __subrelttxt">
                    <?= count($list) ?> result found.
                </div> 

                <div class="wd100"> 

                    <div class="__rluLGrid row row-cols-1 row-cols-sm-1 row-cols-md-2 row-cols-lg-3">


                        <?php
                        $baseDir = Config::get('constants.HOME_DIR');
                        for ($i = 0; $i < count($list); $i++) {
                            $d = $list[$i];
                            $baseFile = "storage/" . $d->profile_picture;
                            if (is_file($baseDir . $baseFile)) {
                                $d->profile_picture = url($baseFile);
                            } else {
                                $d->profile_picture = url(Config::get('constants.DEFAULT_LOGO_VENDOR'));
                            }
                            $url = url('vendor-details?vendorId=' . base64_encode($d->user_id) . "&" . $REDIRECT_QUERY_STRING);
                            ?>
                            <div class="__rluLGridBoz">
                                <div class="wd100 __rluLGridBozInrp">

                                    <div class="wd100 __rluLGridBozImg">
                                        <a href="<?= $url ?>"><img class="img-fluid" src="{{$d->profile_picture}}"></a>
                                    </div>
                                    <div class="wd100 __rluLGdrps"> 
                                        <div class="__wishliico">
                                            <i class="fa fa-heart" aria-hidden="true"></i>
                                        </div>
                                        <div class="d-flex">
                                            <div class="flex-shrink-0">
                                                <img src="{{$d->profile_picture}}" class="__brnd mr-3">
                                            </div>
                                            <div class="flex-grow-1 ms-3">
                                                <h5><a href="<?= $url ?>"><?= $d->first_name ?></a></h5>
                                                <div class="wd100 __starRate" style="display: none;">
                                                    <div class="___staricon">
                                                        <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                        <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                        <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                        <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                        <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                    </div>
                                                    <div class="__starRtvlu">4.5</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wd100 d-flex __liloctnWrp"> 
                                            <div class="flex-shrink-0">
                                                <img src="{{url('/user')}}/images/location.svg" class="mr-2">
                                            </div>
                                            <div class="flex-grow-1  ">
                                                <a href="<?= $url ?>"><?= $d->emirates ?></a>  
                                            </div> 
                                        </div>
                                        <div class="wd100 d-flex __liloctnWrp __licallWrp"> 
                                            <div class="flex-shrink-0">
                                                <img src="{{url('/user')}}/images/call_icon.svg" class="mr-2">
                                            </div>
                                            <div class="flex-grow-1 ">
                                                <a href="tel:+9710502468979"><?= $d->phone ?></a>  
                                            </div> 
                                        </div>
                                        <a href="<?= $url ?>" class="__ListBnt">Request a Quote</a>

                                    </div>

                                </div>
                            </div>
                        <?php } ?>



                    </div>


                </div>



            </div>
        </div>
    </div>
</section>
@include('user.view.includes.footer')

<script>
    $('#catIdUpdate').change(function () {
        var catId = $('#catIdUpdate').val();
        $('#catId').val(catId);
    });
</script>