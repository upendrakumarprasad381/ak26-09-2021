@include('user.view.includes.header')

<!--Inner Banner -->

<section class="inner-banner wd100">
    <div class="breadcrumb-area" style="background-image: url(<?= url('/user') ?>/images/bg/vendor-portal-bg.jpg)" data-overlay="dark"
         data-opacity="7">
        <div class="container pt-150 pb-150 position-relative">
            <div class="row">
                <div class="col-xl-6">
                    <div class="breadcrumb-title">

                        <h3 class="title">Make their Dream day <br>
                            With your delight team.
                        </h3>
                        <span class="sub-title">Showcase your signature  services set to go.</span>
                    </div>
                </div>
            </div>
            <div class="breadcrumb-nav">
                <ul>
                    <li><a href="<?= url('/') ?>">Home</a></li>
                    <li class="active">Vendors Portal</li>
                </ul>
            </div>
            <div class="social-links">
                <ul class="social-icon-three">


                    <li><a href="https://www.facebook.com/Ask-Deema-100964511965038/"><i class="fab fab-facebook-f"></i></a></li>


                    <li><a href="https://twitter.com/askdeema"><i class="fab fab-twitter"></i></a></li>


                    <li><a href="https://www.instagram.com/askdeema/"><i class="fab fab-instagram"></i></a></li>


                    <li><a href="https://www.linkedin.com/company/askdeema"><i class="fab fab-linkedin-in"></i></a></li>

                </ul>
            </div>
        </div>

    </div>

</section>

<!-- End Inner Banner -->

<!-- END ABOUT -->
<section class="vendor-portal-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 d-flex justify-content-end">
                <div class="vendorportal__image-block">
                    <div class="inner wow fadeInUp animated" data-wow-delay="200ms" data-wow-duration="1200ms" style="visibility: visible; animation-duration: 1200ms; animation-delay: 200ms; animation-name: fadeInUp;">
                        <img src="<?= url('/user') ?>/images/gallery/vendor-portal.png" class="vendor-portal_image" alt="Awesome Image" />

                    </div><!-- /.inner -->
                </div><!-- /.about-one__image-block -->
            </div><!-- /.col-lg-6 -->




            <div class="col-lg-6 d-flex">
                <div class="vendor-portal_content my-auto">
                    <div class="block-title">
                        <h2 class="block-title__title text-capitalize">Supplier's Portal </h2>

                        <!-- /.block-title__title -->
                    </div><!-- /.block-title -->
                    <p class="vendorportal__text">Deema and Abdullah started working on AskDeema platform in July 2019. The concept they worked on was to find a platform to ease the event planning experience to make it a better and enjoyable one. They made AskDeema as a platform to be a one stop shop for events planning, basically they connected the event planner or event enthusiast with all the vendors, shops, and planning tools required for him/her to make the event happen. Deema here is represented as the cool aunt who’s experienced enough to give you the right advice and cool enough that you would go to her for help..</p><!-- /.about-one__text -->

                </div><!-- /.about-one__content -->
            </div><!-- /.col-lg-6 -->

        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.about-one -->
<!-- End Portal -->

<!-- Portal -->
<section class="vendor-portal-one vendor-portal__home-two thm-gray-bg vendor-portal-page__service-page">
    <div class="vendor-portal-one__block">
        <div class="block-title text-center">
            <h2 class="block-title__title text-capitalize">What Can We Offer You?</h2>
            <div class="block-title__line">Showcase your signature  services set to go.</div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="vendor-portal-one__single">

                        <h3 class="vendor-portal-text__title"><a href="<?= url('vendors-portal/event-planner?type=3') ?>">Event Planner</a></h3><!-- /.service-one__title -->
                        <p class="vendor-portal-one__text">Planning an event for any event planner is a long and hectic procedure. The online feature.</p><!-- /.service-one__text -->
                        <a href="<?= url('vendors-portal/event-planner?type=3') ?>" class="thm-btn vendor-portal__btn">Read More</a>
                    </div><!-- /vendor-portal-one__single -->
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <div class="vendor-portal-one__single">
                        <h3 class="vendor-portal-text__title"><a href="<?= url('vendors-portal/ask-deema-vendors?type=2') ?>">Ask Deema’s Vendors</a></h3><!-- /.service-one__title -->
                        <p class="vendor-portal-one__text">Managing to survive through multiple events hassle and respond to your new inquiries is on.</p><!-- /.service-one__text -->
                        <a href="<?= url('vendors-portal/ask-deema-vendors?type=2') ?>" class="thm-btn vendor-portal__btn">Read More</a>
                    </div><!-- /vendor-portal-one__single-->
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <div class="vendor-portal-one__single">
                        <h3 class="vendor-portal-text__title"><a href="<?= url('vendors-portal/shop-owner?type=2') ?>">Shop Owner</a></h3><!-- /.service-one__title -->
                        <p class="vendor-portal-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->
                        <a href="<?= url('vendors-portal/shop-owner?type=4') ?>" class="thm-btn vendor-portal__btn">Read More</a>
                    </div><!-- /vendor-portal-one__single -->
                </div><!-- /.col-lg-4 -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.service-one__block -->
</section><!-- /.service-one -->
<!-- End Portal -->

@include('user.view.includes.footer')
