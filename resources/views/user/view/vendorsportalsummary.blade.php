@include('user.view.includes.header')
<?php
$type = !empty($_REQUEST['type']) ? $_REQUEST['type'] : '2';
if ($type == '2') {
    ?>
    <!--Inner Banner -->
    <section class="inner-banner wd100">
        <div class="breadcrumb-area" style="background-image: url(<?= url('/user') ?>/images/bg/vendors-bg.jpg)" data-overlay="dark"data-opacity="7">
            <div class="container pt-150 pb-150 position-relative">
                <div class="row">
                    <div class="col-xl-5">
                        <div class="breadcrumb-title">
                            <h3 class="title">Ask Deema’s Vendors
                                For Everyone</h3>
                            <span class="sub-title">Ask Deema shop is created to support event home businesses and
                                minimize their hassle to join the ecommerce world. All you need as
                                features to manage and run an ecommerce shop are provided by
                                ASKDEEMA. You can review the detailed features below:</span>
                            <div class="button-ask">
                                <div class="button">
                                    <a class="btn-one ask-ad" href="<?= url('vendor-signup?type=' . $type) ?>">Start Your Journy With AD</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="breadcrumb-nav">
                    <ul>
                        <li><a href="<?= url('/') ?>">Home</a></li>
                        <li class="active">Vendors Login</li>
                    </ul>
                </div>

            </div>
        </div>
    </section>
    <!-- End Inner Banner -->

    <!-- Event Planners-->
    <section class="eventplanner-one eventplanner--one__home-two thm-gray-bg service-one__service-page">
        <div class="vendor-one__block">
            <!-- <div class="block-title text-center">
              <h2 class="block-title__title text-uppercase">Top Wedding Vendor Categories</h2>
                <div class="block-title__line"></div>
            </div> -->
            <div class="container">
                <div class="row">

                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-calendar"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Calender</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->

                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-budget"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Budgeter</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-contract"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Contracts</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-clipboard"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Checklist</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-recruitment"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Leads</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-contract-1"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Proposals</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-contact-form"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Form Builder</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->

                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-receipt"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Invoices</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-chat"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Conversations</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->


                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.service-one__block -->
    </section><!-- /.service-one -->

    <?php
} else if ($type == '3') {
    ?>
    <!--Inner Banner -->

    <section class="inner-banner wd100">
        <div class="breadcrumb-area" style="background-image: url(<?= url('/user') ?>/images/bg/vendors-bg.jpg)" data-overlay="dark"
             data-opacity="7">
            <div class="container pt-150 pb-150 position-relative">
                <div class="row">
                    <div class="col-xl-6">
                        <div class="breadcrumb-title">

                            <h3 class="title">Event Planner 
                            </h3>
                            <span class="sub-title">Planning an event for any event planner is a long and hectic 
                                procedure.The online feature of the event planner account 
                                can make your life easier as an event planner regardless of 
                                your grade in the market.</span>
                            <div class="col-sm-6 button-ask">
                                <div class="button">
                                    <a class="btn-one ask-ad" href="<?= url('vendor-signup?type=' . $type) ?>">Start Your Journy With AD</a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="breadcrumb-nav">
                    <ul>
                        <li><a href="<?= url('/') ?>">Home</a></li>
                        <li class="active">Event Planner</li>
                    </ul>
                </div>

            </div>

        </div>

    </section>

    <!-- End Inner Banner -->

    <!-- Event Planners-->
    <section class="eventplanner-one eventplanner--one__home-two thm-gray-bg service-one__service-page wd100">
        <div class="vendor-one__block">
            <!-- <div class="block-title text-center">
              <h2 class="block-title__title text-uppercase">Top Wedding Vendor Categories</h2>
                <div class="block-title__line"></div>
            </div> -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-user"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Vendor Manager</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->

                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-calendar"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Calender</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->

                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-budget"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Budgeter</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-contract"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Contracts</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-clipboard"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Checklist</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-recruitment"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Leads</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-contract-1"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Proposals</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-browser"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Create Event Web Page</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-blueprint"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Floor Plans</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-receipt"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Invoices</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-chat"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Conversations</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-guest-list"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Guest List</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-contact-form"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Form Builder</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.service-one__block -->
    </section><!-- /.service-one -->

    <?php
} else if ($type == '4') {
    ?>
    <!--Inner Banner -->

    <section class="inner-banner wd100">
        <div class="breadcrumb-area" style="background-image: url(<?= url('/user') ?>/images/bg/vendors-bg.jpg)" data-overlay="dark"data-opacity="7">
            <div class="container pt-150 pb-150 position-relative">
                <div class="row">
                    <div class="col-xl-5">
                        <div class="breadcrumb-title">
                            <h3 class="title">Ask Deema’s Shop
                                For Everyone</h3>
                            <span class="sub-title">Ask Deema shop is created to support event home businesses and 
                                minimize their hassle to join the ecommerce world. All you need as 
                                features to manage and run an ecommerce shop are provided by 
                                Askdeema's You can review the detailed features below:</span>
                            <div class="button-ask">
                                <div class="button">
                                    <a class="btn-one ask-ad" href="<?= url('vendor-signup?type=' . $type) ?>">Start Your Journy With AD</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="breadcrumb-nav">
                    <ul>
                        <li><a href="<?= url('/') ?>">Home</a></li>
                        <li class="active">Shop Login</li>
                    </ul>
                </div>

            </div>
        </div>
    </section>


    <!-- End Inner Banner -->

    <!-- Event Planners-->
    <section class="eventplanner-one eventplanner--one__home-two thm-gray-bg service-one__service-page">
        <div class="vendor-one__block">
            <!-- <div class="block-title text-center">
              <h2 class="block-title__title text-uppercase">Top Wedding Vendor Categories</h2>
                <div class="block-title__line"></div>
            </div> -->
            <div class="container">
                <div class="row">

                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-finance"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Finance Tool</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->

                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-calendar"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Calender</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-relationship"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Customer Relationships</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-shopping-bag"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Products</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-recruitment"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Shop Front</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-report"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Reports</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-shipped"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Delivery Cycle </a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->

                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-receipt"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">My Orders</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-chat"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Conversations</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-market"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Commercial</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3">
                        <div class="flex-fill  eventplanner-thumbnail-second-icon">
                            <div class="eventplanner-thumbnail-second-icon-circle  ">
                                <i class="flaticon-contract"></i>
                            </div>
                            <h3 class="eventplanner-one__title"><a href="#">Contracts</a></h3>
                            <p class="eventplanner-one__text">Lorem ipsum dolor sit amet constur elit sed do eiusm tempor incidet dolore magna ex aliqu enim ad minim.</p><!-- /.service-one__text -->

                        </div>
                    </div><!-- /.col-lg-3 -->

                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.service-one__block -->
    </section><!-- /.service-one -->

    <?php
}
?>
@include('user.view.includes.footer')