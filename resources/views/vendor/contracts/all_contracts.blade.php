{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="{{url(''.$url.'/contracts')}}" class="text-muted">{{$page_title}}</a>
</li>
@endsection
{{-- Content --}}
@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container-fluid">
		<!--begin::Dashboard-->
        @php
        if ($role == 'vendor') {
            $url = 'vendor';
        } else {
            $url = 'event-planner';
        }
        @endphp
		<div class="__contractsWrp wd100">

			<div class="row ">
								<div class="__vtrTpBtnWrp d-flex wd100">
                    <a href="{{url(''.$url.'/contracts')}}" class="__vrttbtn @if($page_title == 'Contracts') active @endif">All Contracts <div class="__countCustom">{{ $counts->Pending + $counts->Signed + $counts->OnHold + $counts->Declined }}</div></a>
					<a href="{{url(''.$url.'/contracts?status=Pending')}}" class="__vrttbtn @if($page_title == 'Pending Contracts') active @endif">Pending Contracts  <div class="__countCustom">{{ $counts->Pending }}</div></a>
					<a href="{{url(''.$url.'/contracts?status=Signed')}}" class="__vrttbtn @if($page_title ==  'Signed Contracts') active @endif">Signed Contracts <div class="__countCustom">{{ $counts->Signed }}</div></a>
					<a href="{{url(''.$url.'/contracts?status=Declined')}}" class="__vrttbtn @if($page_title == 'Declined Contracts') active @endif">Declined/On Hold <div class="__countCustom">{{$counts->OnHold + $counts->Declined}}</div></a>

				</div>
			</div>

			<div class="wd100 __tpExpBtnWrp __ckLstFtre">
				<div class="d-flex">

					<div class="__ppserch d-flex align-items-center py-3 py-sm-0 px-sm-3 ">

						<input type="text" class="form-control border-0 font-weight-bold pl-2" placeholder="Search...">
						<span class="svg-icon svg-icon-lg">

							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<rect x="0" y="0" width="24" height="24"></rect>
									<path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
									<path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"></path>
								</g>
							</svg>

						</span>
					</div>


					<div class="dropdown __topSwitchBtn __addItemDrop">
						<button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Sort&nbsp;By
						</button>
						<div class="dropdown-menu " aria-labelledby="dropdownMenuButton">
						 	<a href="#" class="dropdown-item" > 1</a>
							<a href="#" class="dropdown-item" > 1</a>
							<a href="#" class="dropdown-item" > 54 </a>
						</div>
				 	</div>

					<div class="dropdown __topSwitchBtn">
						<button class="btn btn-primary " type="button">
							Export
						</button>
					</div>


					<div class="dropdown __topSwitchBtn">
						<button class="btn btn-secondary" type="button">
							Edit
						</button>
					</div>



				</div>
			</div>


			<div class="row __vtrSrBz">
            @foreach ($contracts as $row)
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        				<div class="wd100 __cotrLboz ">
        					<div class="__cotrLbozHeadcrd mb-2">
        						<h4 class="font-weight-bolder mt-2 mb-2 mr-5 text-dark">{{$row->proposal->event['event_name']}}</h4>

        			 	 	</div>
        					<div class="__cotrLbozbodycrd">
        						<table width="100%"  >
        						  <tr>
        							<td class="__tabhd">Vendor Name</td>
        							<td class="__tabresut">@if($row->party1_id == Auth::user()->id) {{$row->party2['first_name']}} @else {{$row->party1['first_name']}} @endif</td>
        						  </tr>
        						  <tr>
        							<td class="__tabhd">Contract Value</td>
        							<td class="__tabresut">AED {{ $row->items->sum('total') }}</td>
        						  </tr>
        						  <tr>
        							<td class="__tabhd">Vendor Category</td>
        							<td class="__tabresut">{{ $row->proposal->request['category']['name'] }}</td>
        						  </tr>
        						  <tr>
        							<td class="__tabhd">Contract Status</td>
        							<td class="__tabresut">
        						 		<button type="button" class="btn @if($row->status == 'Pending') btn-outline-primary @elseif($row->status == 'Signed') btn-outline-success @else btn-outline-danger  @endif btn-sm pt-1 pb-1">{{ $row->status }}</button>
        						  	</td>
        						  </tr>
        						</table>
                                <div class="__rtBtnWrp">
        						    @if ($row->party1_id == Auth::user()->id)
        						    <a class="__viewEditBtn btn-sm btn btn-primary float-end mt-3 ml-2" href="{{url(''.$url.'/event/'.$row->event->id.'/contracts/'.$row->id.'/edit')}}">View / Edit</a>
                                    @else
                                    <a class="__viewEditBtn btn-sm btn btn-primary float-end mt-3 ml-2" href="{{url(''.$url.'/event/'.$row->event->id.'/contracts/'.$row->id)}}">View</a>
                                    @endif
        						</div>
        					</div>
        				</div>
        		  	</div>
            @endforeach


		</div>





		</div>

		<!--end::Container-->
	</div>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>

</script>
@endsection
