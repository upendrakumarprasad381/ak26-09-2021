{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/event/' . $event->id . '') }}" class="text-muted">{{ $event->event_name }}</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/event/' . $event->id . '/contracts') }}" class="text-muted">Contracts</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/event/' . $event->id . '/contracts/' . $contract->id . '/edit') }}"
            class="text-muted">{{ $page_title }}</a>
    </li>
@endsection
{{-- Content --}}
@section('content')
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container-fluid">
            <div class="__scopeWorkHdprt d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex __proslAddLtPrt">
                    {{-- <h4 class="__fontWtbld mt-2 mb-2 mr-5 text-primary">Scope Of Work</h4> --}}
                </div>
                <div class="__proslAddRtPrt">
                    @if ($contract->signature_2_type == 'Written' && $contract->signature_2)
                        <a href="{{ url('/storage') }}/{{ $contract->signature_2 }}" target="_blank">
                            <button class="btn btn-primary btn-primary" type="button"> Print </button></a>
                    @elseif($contract->signature_1_type == 'Written' && $contract->signature_1)
                        <a href="{{ url('/storage') }}/{{ $contract->signature_1 }}" target="_blank">
                            <button class="btn btn-primary btn-primary" type="button"> Print </button></a>
                    @else
                        <button class="btn btn-primary btn-primary printBtn" type="button"> Print </button>
                    @endif
                </div>
            </div>
            <form id="proposals-form">
                @csrf
                <div class="_method">
                @method('PATCH')
                </div>
                <!--begin::Dashboard-->
                <div class="wd100 __proslAddWrp ">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Proposal</label>
                            <select class="form-control select2" id="proposal_select" name="proposal_id">
                                <option value></option>
                                @foreach ($proposals as $proposal)
                                    <option value="{{ $proposal['id'] }}" @if ($contract->proposal_id == $proposal->id) selected="selected" @endif
                                        data-category_name="{{ $proposal->request->category['name'] }}"
                                        data-items="{{ $proposal->items }}"
                                        data-category_items="{{ $proposal->request->category['items'] }}"
                                        data-category_id="{{ $proposal->request->category['id'] }}"
                                        data-vendor_id="{{ $proposal->request->vendor['id'] }}"
                                        data-vendor_name="{{ $proposal->request->vendor['first_name'] }} {{ $proposal->request->vendor['last_name'] }}">
                                        {{ $proposal->request->vendor['first_name'] }}
                                        {{ $proposal->request->vendor['last_name'] }} -
                                        {{ $proposal->request->category['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Contract Name</label>
                            <input type="text" class="form-control" placeholder="Contract Name" name="contract_name"
                                value="{{ $contract->contract_name }}">
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Event Name</label>
                            <input type="text" class="form-control" placeholder="Wedding"
                                value="{{ $event->event_name }}">
                            <input type="hidden" name="event_id" value="{{ $event->id }}">
                        </div>

                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Name of Party 1</label>
                            <input type="text" class="form-control" placeholder="Name of Party"
                                value="{{ $contract->party1['first_name'] }} {{ $contract->party1['last_name'] }}">
                            <input type="hidden" name="party1_id" value="{{ $contract->party1_id }}">
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Name of Party 2</label>
                            <input type="text" class="form-control" placeholder="Name of Party"
                                value="{{ $contract->party2['first_name'] }} {{ $contract->party2['last_name'] }}"
                                id="name_of_party2">
                            <input type="hidden" name="party2_id" value="{{ $contract->party2_id }}"
                                id="name_of_party2_id">
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Date</label>
                            <div class="input-group date" id="kt_datetimepicker_3" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" placeholder="Select date "
                                    data-target="#kt_datetimepicker_3" name="date" />
                                <div class="input-group-append" data-target="#kt_datetimepicker_3"
                                    data-toggle="datetimepicker">
                                    <span class="input-group-text">
                                        <i class="ki ki-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label"> </label>
                            <div class="checkbox-inline checkbox-lg">
                                <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-lg checkbox-primary">
                                    <input type="checkbox" name="approved_digital_written_signature" value="1"
                                        @if ($contract->approved_digital_written_signature == 1) checked="checked" @endif />
                                    <span></span>
                                    Approved digital(e-sign) or written signature
                                </label>
                            </div>

                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-12 col-xs-12 mb-4 __cutmSwitch">
                            <label class="form-label">Written&nbsp;|&nbsp;Digital</label>
                            <span class="switch switch-primary">
                                <label>
                                    <input type="checkbox" name="type" value="Digital" id="writen-digital-sign"
                                        @if ($contract->signature_1_type == 'Digital') checked="checked" @endif />
                                    <span></span>
                                </label>
                            </span>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Party 1 Signature / File</label>
                            <ul id="media-list" class="clearfix">
                                <li class="myupload" @if ($contract->signature_1) style="display:none;" @endif>
                                    <span><i class="fa fa-plus" aria-hidden="true"></i>
                                        <input type="file" click-type="single" id="picupload" name="signature_1"
                                            value="{{ url('/storage') }}/{{ $contract->signature_2 }}"
                                            class="picupload">
                                    </span>
                                </li>
                                @if ($contract->signature_1)
                                    @if ($contract->signature_1_type == 'Written')
                                        <li>
                                            <img src="{{ asset('assets/icons/docs.png') }}" title="" />
                                            @if ($editable == 1)
                                                <div class='post-thumb'>
                                                    <div class='inner-post-thumb'>
                                                        <a href='javascript:void(0);' data-id='' class='remove-pic'>
                                                            <i class='fa fa-times' aria-hidden='true'>
                                                            </i>
                                                        </a>
                                                    </div>
                                                </div>
                                            @endif
                                            <input type="text" name="old_signature_1"
                                                value="{{ $contract->signature_1 }}" style="display: none;">
                                        </li>
                                    @else
                                        <li>
                                            <img src="/storage/{{ $contract->signature_1 }}" title="" />
                                            @if ($editable == 1)
                                                <div class='post-thumb'>
                                                    <div class='inner-post-thumb'>
                                                        <a href='javascript:void(0);' data-id='' class='remove-pic'>
                                                            <i class='fa fa-times' aria-hidden='true'>
                                                            </i>
                                                        </a>
                                                    </div>
                                                </div>
                                            @endif
                                            <input type="text" name="old_signature_1"
                                                value="{{ $contract->signature_1 }}" style="display: none;">
                                        </li>
                                    @endif
                                @endif
                            </ul>
                            <input type="hidden" class="form-control esign" name="signature_1_esign" id="signature_1_esign"
                                value="">
                        </div>

                        <div class="col-lg-2 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Party 2 Signature / File</label>
                            <ul id="media-list" class="clearfix">
                                <li class="myupload" @if ($contract->signature_2) style="display:none;" @endif>
                                    <span><i class="fa fa-plus" aria-hidden="true"></i>
                                        <input type="file" click-type="single" id="picupload" name="signature_2"
                                            value="{{ url('/storage') }}/{{ $contract->signature_2 }}"
                                            class="picupload">
                                    </span>
                                </li>
                                @if ($contract->signature_2)
                                    @if ($contract->signature_2_type == 'Written')
                                        <li>
                                            <img src="{{ asset('assets/icons/docs.png') }}" title="" />
                                            @if ($editable == 1)
                                                <div class='post-thumb'>
                                                    <div class='inner-post-thumb'>
                                                        <a href='javascript:void(0);' data-id='' class='remove-pic'>
                                                            <i class='fa fa-times' aria-hidden='true'>
                                                            </i>
                                                        </a>
                                                    </div>
                                                </div>
                                            @endif
                                            <input type="text" name="old_signature_1"
                                                value="{{ $contract->signature_2 }}" style="display: none;">
                                        </li>
                                    @else
                                        <li>
                                            <img src="/storage/{{ $contract->signature_2 }}" title="" />
                                            @if ($editable == 1)
                                                <div class='post-thumb'>
                                                    <div class='inner-post-thumb'>
                                                        <a href='javascript:void(0);' data-id='' class='remove-pic'>
                                                            <i class='fa fa-times' aria-hidden='true'>
                                                            </i>
                                                        </a>
                                                    </div>
                                                </div>
                                            @endif
                                            <input type="text" name="old_signature_1"
                                                value="{{ $contract->signature_2 }}" style="display: none;">
                                        </li>
                                    @endif
                                @endif
                            </ul>
                            <input type="hidden" class="form-control esign" name="signature_2_esign" id="signature_2_esign"
                                value="">
                        </div>
                    </div>
                </div>

                <div class="__scopeWorkWrp wd100">
                    <div
                        class="__scopeWorkHdprt d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                        <div class="d-flex __proslAddLtPrt">
                            <h4 class="__fontWtbld mt-2 mb-2 mr-5 text-primary">Scope Of Work</h4>
                        </div>
                        <div class="__proslAddRtPrt">
                            @if ($editable == 1)
                                <button class="btn btn-primary btn-outline-primary addNewItemBtn" type="button"
                                    data-toggle="modal" data-target="#addNewItemModel"> Add Item </button>
                            @endif
                        </div>
                    </div>
                    <div class="__scopeWorkTbvw wd100 ">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col" class="text-center">Sl.No</th>
                                    <th scope="col" class="text-center">Name</th>
                                    <th scope="col" class="text-center">Service Type</th>
                                    <th scope="col" class="text-center">Description</th>
                                    <th scope="col" class="text-right">Amount</th>
                                    <th scope="col" class="text-center">
                                        {{ $contract->proposal->request->category['qty_label'] ?: 'Quantity' }}</th>
                                    <th scope="col" class="text-center">Tax</th>
                                    <th scope="col" class="text-right">Total</th>
                                    <th class="text-center" scope="col" style="display: none;">Remarks</th>
                                    <th class="text-center" scope="col" style="display: none;">Service Type Id</th>
                                    <th class="text-center" scope="col">Upload</th>
                                    <th class="text-center" scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody id="itemsTableBody">
                                @foreach ($contract->items as $item)
                                    <tr>
                                        <td class="text-center td-index">{{ $loop->iteration }}</td>
                                        <td class="text-center">{{ $item->item_name }}</td>
                                        <td class="text-center">{{ $item->category['name'] }}</td>
                                        <td class="text-center">{{ $item->description }}</td>
                                        <td class="text-right">{{ number_format((float) $item->amount, 2, '.', '') }}
                                        </td>
                                        <td class="text-center">{{ $item->quantity }}</td>
                                        <td class="text-center">{{ $item->tax }}%</td>
                                        <td class="text-right">{{ number_format((float) $item->total, 2, '.', '') }}
                                        </td>
                                        <td class="text-center" style="display: none;">{{ $item->remarks }}</td>
                                        <td class="text-center" style="display: none;">{{ $item->service_type_id }}
                                        </td>
                                        <td class="text-center">
                                            <input type="hidden" value="{{ $item->attachment }}" class="image_url">
                                            @if ($item->attachment)
                                                <div class="__scpTbImg">
                                                    <img class="img-fluid item_image"
                                                        src="/storage/{{ $item->attachment }}">
                                                </div>
                                            @endif
                                        </td>
                                        <td class="text-right">
                                            @if ($editable == 1)
                                                <a href="javascript:void(0)" title="Edit"
                                                    class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary editItemBtn">
                                                    <i class="far fa-edit"></i>
                                                </a>
                                            @else
                                                <a href="javascript:void(0)" title="View"
                                                    class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary editItemBtn">
                                                    <i class="flaticon-file-2"></i>
                                                </a>
                                            @endif
                                            @if ($editable == 1)
                                                <a href="javascript:void(0);" title="Delete"
                                                    class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary removeItemBtn">
                                                    <i class="fas fa-trash-alt"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="8" align="right" valign="middle" class="__tbbodr">Subtotal</td>
                                    <td colspan="1" align="right" class="text-right __tbbodr" id="sub_total">
                                        {{ number_format((float) $contract->items->sum('sub_total'), 2, '.', '') }}</td>
                                    <td class="__tbbodr"></td>
                                </tr>
                                <tr>
                                    <td colspan="8" align="right" valign="middle" class="__tbbodr">Tax</td>
                                    <td colspan="1" align="right" class="text-right __tbbodr" id="tax_total">
                                        {{ number_format((float) $contract->items->sum('tax_total'), 2, '.', '') }}</td>
                                    <td class="__tbbodr"></td>
                                </tr>
                                <tr>
                                    <td colspan="8" align="right" valign="middle" class="__tbbodr font-weight-bolder">
                                        <strong>Total </strong>
                                    </td>
                                    <td colspan="1" align="right" class="text-right __tbbodr font-weight-bolder"
                                        id="total">
                                        {{ number_format((float) $contract->items->sum('total'), 2, '.', '') }}</td>
                                    <td class="__tbbodr"></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="__terAgntWrp  wd100">
                    <div class="__terAgntWrp  wd100">
                        <div
                            class="__scopeWorkHdprt d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                            <div class="d-flex __proslAddLtPrt">
                                <h4 class="__fontWtbld mt-2 mb-2 mr-5 text-primary">Terms Of Agreement</h4>
                            </div>
                            @if ($editable == 1)
                                <div class="__proslAddRtPrt">
                                    <button class="btn btn-primary btnTCAddNew" type="button"> Add New</button>
                                    <button class="btn btn-secondary btnApplyDflt" type="button"> Select Default</button>
                                </div>
                            @endif
                        </div>
                        @if ($editable == 1)
                            <div class="__terAgntWrpBz">
                                <ol id="tc_list">
                                    @foreach (json_decode($contract->terms_of_agreement) as $key => $terms)
                                        <li data-text="{{ $terms->text }}" data-row="{{ $key }}">
                                            {{ $terms->text }} <a href="javascript:;"
                                                class="btn btn-sm btn-clean btn-icon TCeditBtn" title="Edit"><i
                                                    class="la la-pencil"></i></a>
                                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon TCdeleteBtn"
                                                title="Delete"><i class="la la-trash"></i></a>
                                        </li>
                                    @endforeach
                                </ol>
                            </div>
                        @else
                            <div class="__terAgntWrpBz">
                                <ol>
                                    @foreach (json_decode($contract->terms_of_agreement) as $key => $terms)
                                        <li>{{ $terms->text }}</li>
                                    @endforeach
                                </ol>
                            </div>

                        @endif
                        <div class="wd100 text-right mt-6">
                            @if ($contract->status == 'Signed')
                                <button type="button" disabled
                                    class="btn btn-primary pl-15 pr-15 pt-5 pb-5 font-weight-bolder">Contract
                                    Signed</button>
                            @elseif($contract->status == 'Ammended')
                                <button type="button" disabled
                                    class="btn btn-primary pl-15 pr-15 pt-5 pb-5 font-weight-bolder">Contract
                                    Ammended</button>
                            @else
                                <button type="button" id="btnSubmit"
                                    class="btn btn-primary pl-15 pr-15 pt-5 pb-5 font-weight-bolder">Accept
                                    Contract</button>
                            @endif
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Container-->
        </div>
        @include('vendor.proposals.default_tc')
        @include('vendor.proposals.add_tc_item')
        @include('vendor.contracts.add_item')
        @include('vendor.proposals.e_sign_canvas')
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        var tc_items = [];
        tc_items = "{{ $contract->terms_of_agreement }}";
        localStorage.setItem("tc_items", (tc_items.replace(/&quot;/g, '"')));
        $('#kt_datetimepicker_3').datetimepicker({
            defaultDate: "{{ $contract->date }}",
            format: 'MM/D/yyyy'
        });
        $('#proposal_select').select2({
            placeholder: "Select a Proposal",
        });
        $('.summernote_terms_and_conditions').summernote({
            height: 200,
        });
        $('.summernote_terms_and_conditions').summernote('code', '{!! $contract->terms_of_agreement !!}');
        $('#item_name').select2({
            tags: true,
            placeholder: "Select Item"
        });
        $('.btnApplyDflt').on('click', function() {
            var terms = $(this).data('terms');
            $('.summernote_terms_and_conditions').summernote('code', terms);
        });
        editRow();
        $('#proposal_select').on('change', function() {
            var category_id = $(this).find(':selected').data('category_id');
            var category_text = $(this).find(':selected').data('category_name');
            var category_items = $(this).find(':selected').data('category_items');
            var items = $(this).find(':selected').data('items');

            $('#name_of_party2').val($(this).find(':selected').data('vendor_name'));
            $('#name_of_party2_id').val($(this).find(':selected').data('vendor_id'));
            $('#item_service_type').empty();
            $('#item_service_type').append('<option value="' + category_id + '" selected>' + category_text +
                '</option>');

            $('#item_name').empty();
            $('#item_name').append('<option value>Select Item</option>');
            category_items.forEach(element => {
                $('#item_name').append('<option value="' + element.name + '">' + element.name +
                    '</option>');
            });

            $('#itemsTableBody').empty();
            items.forEach(element => {
                var qty = element.quantity;
                var amount = element.amount;
                var tax = element.tax;
                var sub_total = qty * amount;
                var tax_amount = (sub_total / 100) * tax;
                var row = '<tr>';
                row += '<td class="text-center td-index">1</td>';
                row += '<td class="text-center">' + element.item_name + '</td>';
                row += '<td class="text-center">' + element.category.name + '</td>';
                row += '<td class="text-center">' + element.description + '</td>';
                row += '<td class="text-right">' + amount + '</td>';
                row += '<td class="text-center">' + qty + '</td>';
                row += '<td class="text-right">' + tax + '%</td>';
                row += '<td class="text-right">' + (sub_total + tax_amount).toFixed(2) + '</td>';
                row += '<td class="text-right" style="display:none;">' + element.remarks + '</td>';
                row += '<td class="text-right" style="display:none;">' + element.service_type_id + '</td>';
                if (element.attachment) {
                    row += '<td class="text-center"><input type="hidden" value="' + element.attachment +
                        '" class="image_url"><div class="__scpTbImg"><img class="img-fluid item_image" src="/storage/' +
                        element.attachment + '" ></div></td>';
                    row +=
                        '<td class="text-right"><a href="javascript:void(0)" title="Edit" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary editItemBtn"><i class="far fa-edit"></i></i></a><a href="javascript:void(0);" title="Delete" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary removeItemBtn"><i class="fas fa-trash-alt"></i></a></td></tr>';
                } else {
                    row += '<td></td>';
                    row +=
                        '<td class="text-right"><a href="javascript:void(0)" title="Edit" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary editItemBtn"><i class="far fa-edit"></i></i></a><a href="javascript:void(0);" title="Delete" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary removeItemBtn"><i class="fas fa-trash-alt"></i></a></td></tr>';
                }
                $('#itemsTableBody').append(row);
                editRow();
                setIndex();
            });

        });

        $("#btnSubmit").click(function(event) {
            event.preventDefault();
            var items = [];
            $('#itemsTableBody > tr').each(function() {
                var row = {
                    event_name: $(this).find('td').eq(1).text(),
                    service_type: parseInt($(this).find('td').eq(9).text()),
                    description: $(this).find('td').eq(3).text(),
                    quantity: parseFloat($(this).find('td').eq(5).text()),
                    tax: parseFloat($(this).find('td').eq(6).text()),
                    amount: parseFloat($(this).find('td').eq(4).text()),
                    remarks: $(this).find('td').eq(8).text(),
                    file: $(this).find('td').eq(10).find('.image_url').val() ? $(this).find('td').eq(10)
                        .find('.image_url').val() : null,
                }
                items.push(row);
            });
            if (jsFieldsValidator(['request_id'])) {
                if (items.length > 0) {
                    var form = $('#proposals-form')[0];
                    var data = new FormData(form);
                    var editable = "{{ $editable }}";
                    var terms_of_agreement = $('.summernote_terms_and_conditions').summernote('code');
                    data.append('items', JSON.stringify(items));
                    data.append('terms_of_agreement', terms_of_agreement);
                    data.append('editable', editable);
                    $("#btnSubmit").prop("disabled", true);
                    $.ajax({
                        type: "POST",
                        enctype: 'multipart/form-data',
                        url: "/vendor/event/{{ $event->id }}/contracts/{{ $contract->id }}",
                        data: data,
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function(data) {
                            $("#btnSubmit").prop("disabled", false);
                            setTimeout(function() {
                                location.replace(
                                    '/vendor/event/{{ $event->id }}/contracts');
                            }, 1000);
                        },
                        error: function(e) {
                            $("#btnSubmit").prop("disabled", false);
                        }
                    });
                } else {
                    toastr.error('Add items to proposal.', 'Error');
                }
            }

        });
        var parent_tag = '';
        $('.picupload').on('click', function(e) {
            if ($(this).data('type') == 'item_file') {

            } else {
                if ($('#writen-digital-sign:checkbox:checked').length > 0) {
                    e.preventDefault();
                    var canvas = document.getElementById("sig-canvas");
                    canvas.width = canvas.width;
                    $('#eSignModel').modal('show');
                    parent_tag = $(this).parent().parent();
                }
            }
        });

        function clearFields() {
            $('#item_name').select2("val", "");
            $('textarea#item_description').val('');
            $('#item_quantity').val('');
            // $('#item_tax').val('');
            $('#item_amount').val('');
            $('textarea#item_remarks').val('');
            $('#item_editable').val(null);
            var image =
                `<li class="myupload"><span><i class="fa fa-plus" aria-hidden="true"></i><input type="file" click-type="single" id="item_file" name="item_file" class="picupload" data-type="item_file"></span></li>`;
            $('.item_image_panel').html(image);
            editable_row = null;
        }
        var editable_row = null;
        $('#btnAddItem').on('click', function() {
            if ($('#item_name').find(':selected').data('select2-tag')) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: "/vendor_create_item",
                    data: {
                        'name': $('#item_name').val(),
                        'category_id': $('#item_service_type').val()
                    },
                    success: function(data) {},
                    error: function(e) {}
                });
            }
            if (jsFieldsValidator(['item_name', 'item_amount', 'item_tax', 'item_quantity'])) {
                if (editable_row != null) {
                    var row = editable_row;

                    var qty = parseFloat($('#item_quantity').val());
                    var amount = parseFloat($('#item_amount').val());
                    var tax = parseFloat($('#item_tax').val());
                    var sub_total = qty * amount;
                    var tax_amount = (sub_total / 100) * tax;

                    row.find("td:eq(1)").text($('#item_name').val());
                    row.find("td:eq(3)").text($('textarea#item_description').val());
                    row.find("td:eq(4)").text(amount.toFixed(2));
                    row.find("td:eq(5)").text($('#item_quantity').val());
                    row.find("td:eq(6)").text($('#item_tax').val() + '%');
                    row.find("td:eq(7)").text((sub_total + tax_amount).toFixed(2));
                    row.find("td:eq(8)").text($('#item_remarks').val());
                    if ($('#item_old_image').val()) {} else {
                        var files = $('#item_file')[0].files;
                        if (files.length > 0) {
                            uploadImage(files, true); // file,editable
                        } else {
                            row.find("td:eq(10)").empty();
                        }
                    }
                    $('#addNewItemModel').modal('hide');
                    setIndex();
                } else {
                    var files = $('#item_file')[0].files;
                    if (files.length > 0) {
                        var image_url = uploadImage(files);
                        $('.item_image_panel').find('.myupload').show();
                        $('.item_image_panel')[0].children[0].remove();
                    } else {
                        updateRow();
                    }
                }
            }
        });

        $('.addNewItemBtn').on('click', function() {
            clearFields();
        });

        function uploadImage(files, editable = null) {
            var image_url;
            var fd = new FormData();
            fd.append('file', files[0]);
            fd.append('path', 'vendor/contracts/{{ $event->id }}/items');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/upload-file",
                data: fd,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    image_url = data;
                    updateRow(image_url, editable);
                },
                error: function(e) {

                }
            });

            return image_url;
        }

        function updateRow(image_url = null, editable = null) {
            if (editable) {
                editable_row.find("td:eq(9)").html('<input type="hidden" value="' + image_url +
                    '" class="image_url"><div class="__scpTbImg"><img class="img-fluid item_image" src="/storage/' +
                    image_url + '" ></div>');
            } else {
                var qty = parseFloat($('#item_quantity').val());
                var amount = parseFloat($('#item_amount').val());
                var tax = parseFloat($('#item_tax').val());
                var sub_total = qty * amount;
                var tax_amount = (sub_total / 100) * tax;
                var row = '<tr>';
                row += '<td class="text-center td-index">1</td>';
                row += '<td class="text-center">' + $('#item_name').val() + '</td>';
                row += '<td class="text-center">' + $('#item_service_type').text() + '</td>';
                row += '<td class="text-center">' + $('textarea#item_description').val() + '</td>';
                row += '<td class="text-right">' + amount.toFixed(2) + '</td>';
                row += '<td class="text-center">' + $('#item_quantity').val() + '</td>';
                row += '<td class="text-right">' + $('#item_tax').val() + '%</td>';
                row += '<td class="text-right">' + (sub_total + tax_amount).toFixed(2) + '</td>';
                row += '<td class="text-right" style="display:none;">' + $('#item_remarks').val() + '</td>';
                row += '<td class="text-right" style="display:none;">' + $('#item_service_type').val() + '</td>';
                if (image_url) {
                    row += '<td class="text-center"><input type="hidden" value="' + image_url +
                        '" class="image_url"><div class="__scpTbImg"><img class="img-fluid item_image" src="/storage/' +
                        image_url + '" ></div></td>';
                    row +=
                        '<td class="text-right"><a href="javascript:void(0)" title="Edit" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary editItemBtn"><i class="far fa-edit"></i></i></a><a href="javascript:void(0);" title="Delete" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary removeItemBtn"><i class="fas fa-trash-alt"></i></a></td></tr>';
                } else {
                    row += '<td></td>';
                    row +=
                        '<td class="text-right"><a href="javascript:void(0)" title="Edit" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary editItemBtn"><i class="far fa-edit"></i></i></a><a href="javascript:void(0);" title="Delete" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary removeItemBtn"><i class="fas fa-trash-alt"></i></a></td></tr>';
                }
                $('#itemsTableBody').append(row);
                editRow();
                $('#addNewItemModel').modal('hide');
            }
            setIndex();
            clearFields();
        }

        function setIndex() {
            var total_amunt = 0;
            var tax_amunt = 0;
            var sub_total = 0;
            $(".td-index").each(function(index) {
                $(this).text(++index);
                var row = $(this).closest("tr");
                var item_sub_total = parseFloat(row.find("td:eq(5)").html()) * parseFloat(row.find("td:eq(4)")
                    .html());
                sub_total += item_sub_total;
                tax_amunt += (item_sub_total / 100) * parseFloat((row.find("td:eq(6)").html()).replace('%', ''));
                total_amunt += parseFloat(row.find("td:eq(7)").html());
            });

            $('#sub_total').text(sub_total.toFixed(2));
            $('#tax_total').text(tax_amunt.toFixed(2));
            $('#total').text(total_amunt.toFixed(2));

        }

        function editRow() {
            $('.editItemBtn').on('click', function(e) {
                var row = $(this).closest("tr");
                $('#item_name').val(row.find("td:eq(1)").html()).trigger('change');
                $('textarea#item_description').val(row.find("td:eq(3)").html());
                $('#item_amount').val(row.find("td:eq(4)").html());
                $('#item_tax').val(row.find("td:eq(6)").html().replace('%', ''));
                $('#item_quantity').val(row.find("td:eq(5)").html());
                $('textarea#item_remarks').val(row.find("td:eq(8)").html());
                editable_row = row;
                if (row.find("td:eq(10)").html() != '') {
                    var image = `<li class="myupload" style="display:none;"><span><i class="fa fa-plus" aria-hidden="true"></i><input type="file" click-type="single" id="item_file" name="item_file" class="picupload" data-type="item_file"></span></li>
                            <li>
                            <img src="` + row.find("td:eq(10)").find('.item_image').attr('src') + `" title=""/>
                            <div class='post-thumb'>
                                <div class='inner-post-thumb'>
                                    <a href='javascript:void(0);' data-id='' class='remove-pic'>
                                        <i class='fa fa-times' aria-hidden='true'>
                                        </i>
                                    </a>
                                </div>
                            </div>
                            <input type="text" name="old_image" id="item_old_image" value="` + row.find("td:eq(10)")
                        .find('.item_image').attr('src') + `" style="display: none;">
                        </li>`;
                    $('.item_image_panel').html(image);
                }
                $('#addNewItemModel').modal('show');
            });

            $('.removeItemBtn').on('click', function(e) {
                var row = $(this).closest("tr");
                Swal.fire({
                    title: "Are you sure?",
                    text: "You wont be able to revert this!",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                }).then(function(result) {
                    if (result.value) {
                        row.remove();
                        setIndex();
                    } else if (result.dismiss === "cancel") {
                        Swal.fire(
                            "Cancelled",
                            "Your imaginary file is safe :)",
                            "error"
                        )
                    }
                });
            });
        }

        $('.printBtn').on('click', function(event) {
            event.preventDefault();
            $('._method').empty();
            var items = [];
            $('#itemsTableBody > tr').each(function() {
                var row = {
                    event_name: $(this).find('td').eq(1).text(),
                    service_type: parseInt($(this).find('td').eq(9).text()),
                    description: $(this).find('td').eq(3).text(),
                    quantity: parseFloat($(this).find('td').eq(5).text()),
                    tax: parseFloat($(this).find('td').eq(6).text()),
                    amount: parseFloat($(this).find('td').eq(4).text()),
                    remarks: $(this).find('td').eq(8).text(),
                    file: $(this).find('td').eq(10).find('.image_url').val() ? $(this).find('td').eq(10)
                        .find('.image_url').val() : null,
                }
                items.push(row);
            });
            if (jsFieldsValidator(['request_id'])) {
                if (items.length > 0) {
                    var tc_items = localStorage["tc_items"];
                    var form = $('#proposals-form')[0];
                    var data = new FormData(form);

                    data.append('items', JSON.stringify(items));
                    data.append('terms_of_agreement', tc_items);
                    data.append('contract',"{{$contract->id}}");
                    $.ajax({
                        type: "POST",
                        enctype: 'multipart/form-data',
                        url: "/event-planner/event/{{ $event->id }}/contracts/generate-pdf",
                        data: data,
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function(data) {
                            $('._method').append('@method('PATCH')');
                            $("#btnSubmit").prop("disabled", false);
                            window.open('/storage/pdf/contract.pdf');

                        },
                        error: function(e) {
                            $('._method').append('@method('PATCH')');
                            $("#btnSubmit").prop("disabled", false);
                        }
                    });
                } else {
                    $('._method').append('@method('PATCH')');
                    toastr.error('Add items to contracts.', 'Error');
                }
            }
        });
    </script>
@endsection
