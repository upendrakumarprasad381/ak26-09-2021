{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="{{ url('' . $url . '/financial-tool') }}" class="text-muted">Financial Tool</a>
</li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/financial-tool/create') }}" class="text-muted">{{ $page_title }}</a>
    </li>
@endsection
{{-- Content --}}
@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid">
        <form id="proposals-form">
            @csrf
            <!--begin::Dashboard-->
            <div class="wd100 __proslAddWrp ">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Events</label>
                        <select class="form-control" id="request_select" name="request_id">
                            <option value>Select Events</option>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Contracts</label>
                        <select class="form-control" id="request_select" name="request_id">
                            <option value>Select Contracts</option>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Item</label>
                        <input type="text" class="form-control" placeholder="Item"
                            value="">
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Sub Item</label>
                        <input type="text" class="form-control" placeholder="Sub Item"
                            value="">
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Actual Cost</label>
                        <input type="text" class="form-control" placeholder="Actual Cost"
                            value="">
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                        <label class="form-label">Customer Invoice Amount</label>
                        <input type="text" class="form-control" placeholder="Invoice Amt"
                            value="">
                    </div>
                    <div class="wd100 text-right mt-6">
                        <button type="button" id="btnSubmit"
                            class="btn btn-primary pl-15 pr-15 pt-5 pb-5 font-weight-bolder">Save</button>
                    </div>
                </div>
            </div>


        </form>
        <!--end::Container-->
    </div>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>

@endsection
