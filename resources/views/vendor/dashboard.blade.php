{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<!--begin::Dashboard-->
<div class="row __sublk">
      <div class="col-sm-12">
        <div class="row">
     <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
        <!--begin::Stats Widget 16-->
        <a href="/{{$url}}/leads" class="card card-custom card-stretch">
            <!--begin::Body-->
            <div class="card-body">
                <div class="font-weight-bold ">
                    <span
                        class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{$total_leads}}</span>
                </div>

                <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Total Leads</div>

            </div>
            <!--end::Body-->
        </a>
        <!--end::Stats Widget 16-->
    </div>

    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
        <!--begin::Stats Widget 16-->
        <a href="/{{$url}}/proposals" class="card card-custom card-stretch">
            <!--begin::Body-->
            <div class="card-body">
                <div class="font-weight-bold ">
                    <span
                        class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{$total_proposals}}</span>
                </div>

                <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Total Proposals</div>

            </div>
            <!--end::Body-->
        </a>
        <!--end::Stats Widget 16-->
    </div>

     <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
        <!--begin::Stats Widget 16-->
        <a href="/{{$url}}/contracts" class="card card-custom card-stretch">
            <!--begin::Body-->
            <div class="card-body">
                <div class="font-weight-bold ">
                    <span
                        class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{$total_contracts}}</span>
                </div>

                <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Total Contracts</div>

            </div>
            <!--end::Body-->
        </a>
        <!--end::Stats Widget 16-->
    </div>

    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
        <!--begin::Stats Widget 16-->
        <a href="/{{$url}}/invoices" class="card card-custom card-stretch">
            <!--begin::Body-->
            <div class="card-body">
                <div class="font-weight-bold ">
                    <span
                        class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{$total_invoices}}</span>
                </div>

                <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Total Invoices</div>

            </div>
            <!--end::Body-->
        </a>
        <!--end::Stats Widget 16-->
    </div>
        </div>
      </div>









<div class="col-sm-12">

<h4 class="font-weight-bolder mt-10" style="padding: 5px 0;">All Activity  Details</h4>

</div>



    <div class="col-sm-12">
        <div class="row">
           <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <!--begin::Stats Widget 16-->
                <a href="javascript:void(0)" onclick="ToDolistGoTo('all-activities');" class="card card-custom card-stretch">
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="font-weight-bold ">
                            <span
                                class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans" tabId="all"
                                  id="all-activities">0</span>
                        </div>
                        <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">All Activity</div>
                    </div>
                    <!--end::Body-->
                </a>
                <!--end::Stats Widget 16-->
            </div>

          <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <!--begin::Stats Widget 16-->
                <a href="javascript:void(0)" onclick="ToDolistGoTo('overdue');" class="card card-custom card-stretch">
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="font-weight-bold ">
                            <span
                                class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans" tabId='todays-overdue' id="overdue">0</span>
                        </div>
                        <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Today + Overdue</div>
                    </div>
                    <!--end::Body-->
                </a>
                <!--end::Stats Widget 16-->
            </div>

          <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <!--begin::Stats Widget 16-->
                <a href="javascript:void(0)" onclick="ToDolistGoTo('upcoming');" class="card card-custom card-stretch">
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="font-weight-bold ">
                            <span
                                class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans" tabId='upcoming'
                                  id="upcoming">0</span>
                        </div>
                        <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Upcoming</div>
                    </div>
                    <!--end::Body-->
                </a>
                <!--end::Stats Widget 16-->
            </div>

            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <!--begin::Stats Widget 16-->
                <a href="javascript:void(0)" onclick="ToDolistGoTo('completed');" class="card card-custom card-stretch">
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="font-weight-bold ">
                            <span
                                class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans" tabId='completed'
                                  id="completed">0</span>
                        </div>
                        <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Completed</div>
                    </div>
                    <!--end::Body-->
                </a>
                <!--end::Stats Widget 16-->
            </div>

        </div>
    </div>




</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>


<script>
      $(document).ready(function () {

            setTimeout(function () {
                activitiesData('all-activities');
                activitiesData('overdue');
                activitiesData('upcoming');
                activitiesData('completed');
            }, 500);

        });

        function activitiesData(counterId) {
            var fromDays = 0;
            var tabId = $('#' + counterId).attr('tabId');

            var data = {
                _token: "{{ csrf_token() }}",
                autoload: true,
                eventDashboard: true,
                eventId: 0,
                tabId: tabId,
                fromDays: fromDays
            };
            $.ajax({
                url: "{{ url('/to-do-list') }}",
                cache: false,
                data: data,
                async: true,
                type: 'POST',
                success: function (res) {
                    res = jQuery.parseJSON(res);
                    if (res.status == true) {
                        $('#' + counterId).html(res.dataCount);
                    }
                }
            });

        }
         function ToDolistGoTo(counterId) {

            var tabId = $('#' + counterId).attr('tabId');
            var Url="{{url('/to-do-list')}}/?tabId="+tabId+"&fromdate=0";
            window.location=Url;
        }
</script>
@endsection
