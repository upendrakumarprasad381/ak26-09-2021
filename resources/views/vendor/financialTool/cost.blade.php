@extends('layout.default')
@section('sub-header')
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/financial-tool/') }}" class="text-muted">Financial Tool</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/financial-tool/financial-cost') }}" class="text-muted">{{ $page_title }}</a>
    </li>
@endsection
@section('content')
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container-fluid">
            <!--begin::Dashboard-->
            <div class="row __sublk">
                <div class="col-xl-4">
                    <!--begin::Stats Widget 16-->
                    <a href="" class="card card-custom card-stretch">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span
                                    class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{ $data->sum('actual_cost') }}</span>
                            </div>

                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Actual Cost</div>

                        </div>
                        <!--end::Body-->
                    </a>
                    <!--end::Stats Widget 16-->
                </div>

                <div class="col-xl-4">
                    <!--begin::Stats Widget 16-->
                    <a href="" class="card card-custom card-stretch">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span
                                    class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{ $data->sum('invoice_amount') }}</span>
                            </div>

                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Total Customer Invoice</div>

                        </div>
                        <!--end::Body-->
                    </a>
                    <!--end::Stats Widget 16-->
                </div>

                <div class="col-xl-4">
                    <!--begin::Stats Widget 16-->
                    <a href="" class="card card-custom card-stretch">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span
                                    class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{ $data->sum('invoice_amount') - $data->sum('actual_cost') }}</span>
                            </div>
                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Total Profit</div>
                        </div>
                        <!--end::Body-->
                    </a>
                    <!--end::Stats Widget 16-->
                </div>
            </div>
            <br>
            <!--end::Dashboard-->
            <div class="__budget wd100 mt-10">
                <div class="__budgeter wd100 ">
                    <div class="__bud-btn">
                        <a href="{{ url('' . $url . '/financial-tool/financial-cost/create') }}"><button
                                class="btn btn-primary btn-outline-primary " type="button"> Add Cost </button></a>
                    </div>
                    <table class="table table-head-custom table-head-bg table-borderless table-vertical-center mt-5">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Event</th>
                                <th scope="col">Contract</th>
                                <th scope="col">Actual Cost</th>
                                <th scope="col">Customer Invoice</th>
                                <th scope="col">Profit</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->event['event_name'] }}</td>
                                    <td>{{ $item->contract_id }}</td>
                                    <td>AED {{ $item->actual_cost }}</td>
                                    <td>AED {{ $item->invoice_amount }}</td>
                                    <td>AED {{ $item->invoice_amount - $item->actual_cost }}</td>
                                    <td><a href="/{{$url}}/financial-tool/financial-cost/{{$item->id}}/edit" title="Edit"
                                            class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary editItemBtn">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <a href="javascript:void(0)" title="Delete" data-id="{{$item->id}}"
                                            class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary deleteItemBtn">
                                            <i class="fas fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('.deleteItemBtn').on('click', function(e) {
            var id = $(this).data('id');
            Swal.fire({
                title: "Are you sure?",
                text: "You wont be able to revert this!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function(result) {
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "DELETE",
                        enctype: 'multipart/form-data',
                        url: "/{{$url}}/financial-tool/financial-cost/" + id,
                        data: {
                            'id': id,
                        },
                        success: function(data) {
                            setTimeout(function() {
                                location.reload();
                            }, 1000);
                        },
                        error: function(e) {

                        }
                    });
                } else if (result.dismiss === "cancel") {
                    // Swal.fire(
                    //     "Cancelled",
                    //     "Your imaginary file is safe :)",
                    //     "error"
                    // )
                }
            });

        });
    </script>
@endsection
