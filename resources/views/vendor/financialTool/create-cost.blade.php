{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/financial-tool/') }}" class="text-muted">Financial Tool</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/financial-tool/financial-cost') }}" class="text-muted">Financial Cost</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/financial-tool/financial-cost/create') }}"
            class="text-muted">{{ $page_title }}</a>
    </li>
@endsection
{{-- Content --}}
@section('content')
    @php
    if (isset($editable)) {
        $url_ = '/' . $url . '/financial-tool/financial-cost/' . $editable->id . '';
    } else {
        $url_ = '/' . $url . '/financial-tool/financial-cost';
    }
    @endphp
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container-fluid">
            <form id="cost-form">
                @isset($editable)
                    @method('PATCH')
                @endisset
                @csrf
                <!--begin::Dashboard-->
                <div class="wd100 __proslAddWrp ">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Events</label>
                            <select class="form-control select2" id="event_select" name="event_id">
                                <option value></option>
                                @foreach ($events as $event)
                                    <option value="{{ $event['id'] }}" @isset($editable) @if ($editable->event_id == $event['id']) selected @endif
                                        @endisset>{{ $event['event_name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Contracts</label>
                            <select class="form-control" id="contract_select" name="contract_id">
                                <option value>Select Contracts</option>
                                @isset($editable)
                                    @foreach ($contracts as $contract)
                                        <option value="{{ $contract->id }}" @if ($editable->contract_id == $contract->id) selected @endif>{{ $contract->id }}
                                        </option>
                                    @endforeach
                                @endisset
                            </select>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Date</label>
                            <div class="input-group date" id="kt_datetimepicker_3" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" placeholder="Select date "
                                    data-target="#kt_datetimepicker_3" name="date" />
                                <div class="input-group-append" data-target="#kt_datetimepicker_3"
                                    data-toggle="datetimepicker">
                                    <span class="input-group-text">
                                        <i class="ki ki-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="items_row" class="row">
                        @isset($editable)
                            @foreach ($editable->items as $item)
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-4">
                                    <label class="form-label">Item Name</label>
                                    <input type="text" class="form-control" placeholder="Item" name="item_name[]"
                                        value="{{ $item['item_name'] }}">
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-4">
                                    <label class="form-label">Actual Cost</label>
                                    <input type="text" class="form-control" placeholder="Actual Cost" name="actual_cost[]"
                                        value="{{ $item['actual_cost'] }}">
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-4">
                                    <label class="form-label">Customer Invoice Amount</label>
                                    <input type="text" class="form-control" name="invoice_amount[]" placeholder="Invoice Amt"
                                        value="{{ $item['invoice_amount'] }}">
                                </div>
                            @endforeach
                        @endisset
                    </div>
                    <div class="row">
                        <div class="wd100 text-right mt-6">
                            <button type="button" id="btnSubmit"
                                class="btn btn-primary pl-15 pr-15 pt-5 pb-5 font-weight-bolder">Save</button>
                        </div>
                    </div>
                </div>


            </form>
            <!--end::Container-->
        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        $('#event_select').select2({
            placeholder: "Select a Event",
        });
        $('#kt_datetimepicker_3').datetimepicker({
            defaultDate: new Date(),
            format: 'MM/D/yyyy',
        });
        $('#event_select').on('change', function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "GET",
                enctype: 'multipart/form-data',
                url: "/{{ $url }}/get-contracts",
                data: {
                    event_id: $('#event_select').val()
                },
                success: function(data) {
                    $('#contract_select').empty();
                    if (data) {
                        var row = '<option value="">Select Contracts</option>';
                        data.forEach(element => {
                            row += '<option value="' + element.id + '">' + element.id +
                                '</option>';
                        });
                        $('#contract_select').append(row);
                    }
                },
                error: function(e) {

                }
            });
        });
        $('#contract_select').on('change', function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "GET",
                enctype: 'multipart/form-data',
                url: "/{{ $url }}/get-contract-items",
                data: {
                    contract_id: $('#contract_select').val()
                },
                success: function(data) {
                    if (data) {
                        console.log(data);
                        var row = ``;
                        $('#items_row').empty();
                        data.forEach(element => {
                            row += `<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Item Name</label>
                            <input type="text" class="form-control" placeholder="Item" name="item_name[]" value="` +
                                element.item_name +
                                `">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Actual Cost</label>
                            <input type="text" class="form-control" placeholder="Actual Cost" name="actual_cost[]" value="` +
                                element
                                .total + `">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Customer Invoice Amount</label>
                            <input type="text" class="form-control" name="invoice_amount[]" placeholder="Invoice Amt" value="">
                        </div>`;
                        });
                        $('#items_row').append(row);
                    }
                },
                error: function(e) {

                }
            });
        });
        $("#btnSubmit").click(function(event) {
            event.preventDefault();
            var form = $('#cost-form')[0];
            var data = new FormData(form);
            $("#btnSubmit").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "{{ $url_ }}",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    $("#btnSubmit").prop("disabled", false);
                    setTimeout(function() {
                        location.replace(
                            '/{{ $url }}/financial-tool/financial-cost');
                    }, 1000);
                },
                error: function(e) {
                    $("#btnSubmit").prop("disabled", false);
                }
            });
        });
    </script>
@endsection
