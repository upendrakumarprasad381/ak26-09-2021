{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/financial-tool') }}" class="text-muted">{{ $page_title }}</a>
    </li>
@endsection
{{-- Content --}}
@section('content')
    <div class="wd100 text-right mt-6">
        <a href="{{ url('' . $url . '/financial-tool/financial-cost') }}"> <button type="button" id="btnSubmit" class="btn btn-primary pl-15 pr-15 pt-5 pb-5 font-weight-bolder">
            Cost</button></a>
    </div>
    <!--begin::Dashboard-->
    <div class="row __sublk">
        <div class="col-xl-4">
            <!--begin::Stats Widget 16-->
            <a href="" class="card card-custom card-stretch">
                <!--begin::Body-->
                <div class="card-body">
                    <div class="font-weight-bold ">
                        <span
                            class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{ $invoice_count['total'] }}</span>
                    </div>

                    <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Total Invoiced Amount</div>

                </div>
                <!--end::Body-->
            </a>
            <!--end::Stats Widget 16-->
        </div>

        <div class="col-xl-4">
            <!--begin::Stats Widget 16-->
            <a href="" class="card card-custom card-stretch">
                <!--begin::Body-->
                <div class="card-body">
                    <div class="font-weight-bold ">
                        <span
                            class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{ $invoice_count['paid'] }}</span>
                    </div>

                    <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Total Collected</div>

                </div>
                <!--end::Body-->
            </a>
            <!--end::Stats Widget 16-->
        </div>

        <div class="col-xl-4">
            <!--begin::Stats Widget 16-->
            <a href="" class="card card-custom card-stretch">
                <!--begin::Body-->
                <div class="card-body">
                    <div class="font-weight-bold ">
                        <span
                            class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{ $invoice_count['pending'] }}</span>
                    </div>
                    <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Total Pending</div>
                </div>
                <!--end::Body-->
            </a>
            <!--end::Stats Widget 16-->
        </div>
    </div>
    <br>
    <!--end::Dashboard-->
    <div class="row">
        <div class="col-lg-6">
            <!--begin::Card-->
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Financial Graph</h3>
                    </div>
                </div>
                <div class="card-body">
                    <!--begin::Chart-->
                    <div id="chart_3_"></div>
                    <!--end::Chart-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <div class="col-lg-6">
            <!--begin::Card-->
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Profit Graph</h3>
                    </div>
                </div>
                <div class="card-body">
                    <!--begin::Chart-->
                    <div id="chart_4_"></div>
                    <!--end::Chart-->
                </div>
            </div>
            <!--end::Card-->
        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script>
        var financial_report = JSON.parse(("{{$financial_report}}").replace(/&quot;/g, '"'));
        var profit_report = JSON.parse(("{{$profit_report}}").replace(/&quot;/g, '"'));

        const primary = '#6993FF';
        const success = '#1BC5BD';
        const info = '#8950FC';
        const warning = '#FFA800';
        const danger = '#F64E60';
        const apexChart = "#chart_3_";
        var options = {
            series: [{
                name: 'Total Invoice',
                data: financial_report.total
            }, {
                name: 'Total Collected',
                data: financial_report.paid
            }, {
                name: 'Total Pending',
                data: financial_report.unpaid
            }],
            chart: {
                type: 'bar',
                height: 350
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: ['Jan','Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct','Nov','Dec'],
            },
            yaxis: {
                title: {
                    text: '$ (thousands)'
                }
            },
            fill: {
                opacity: 1
            },
            tooltip: {
                y: {
                    formatter: function(val) {
                        return "$ " + val + " thousands"
                    }
                }
            },
            colors: [primary, success, warning]
        };
        var chart = new ApexCharts(document.querySelector(apexChart), options);
        chart.render();


        const apexChart2 = "#chart_4_";
        var options2 = {
            series: [{
                name: 'Actual Cost',
                data: profit_report.actual_cost
            }, {
                name: 'Customer Invoice',
                data: profit_report.invoice_amount
            }, {
                name: 'Profit',
                data: profit_report.profit
            }],
            chart: {
                type: 'bar',
                height: 350
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: ['Jan','Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct','Nov','Dec'],
            },
            yaxis: {
                title: {
                    text: '$ (thousands)'
                }
            },
            fill: {
                opacity: 1
            },
            tooltip: {
                y: {
                    formatter: function(val) {
                        return "$ " + val + " thousands"
                    }
                }
            },
            colors: [primary, success, warning]
        };
        var chart2 = new ApexCharts(document.querySelector(apexChart2), options2);
        chart2.render();
    </script>
@endsection
