<!-- Modal-->
<div class="modal fade" id="acceptInvoiceModel" role="model" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Invoice Payment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="invoice-payment-form">
                    @csrf
                    <div class="wd100 __proslAddWrp __addscpWrkPg">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4 __ctndrofid">
                                <label class="form-label">Payment Status</label>
                                <select class="form-control" name="payment_status" id="payment_status">
                                    <option value="Paid">Paid</option>
                                    <option value="Un Paid">Un Paid</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="button" id="btnPayment" class="btn btn-primary font-weight-bold">Submit</button>
            </div>
        </div>
    </div>
</div>
