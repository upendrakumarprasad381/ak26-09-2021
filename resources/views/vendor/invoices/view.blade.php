@extends('layout.default')
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="{{url(''.$url.'/my-events/')}}" class="text-muted">My Events</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{url(''.$url.'/event/'.$event->id.'')}}" class="text-muted">{{$event->event_name}}</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{url(''.$url.'/event/'.$event->id.'/invoices')}}" class="text-muted">Invoices</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{url(''.$url.'/event/'.$event->id.'/invoices/'.$invoice->id.'')}}" class="text-muted">{{$page_title}}</a>
</li>
@endsection
@section('content')
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <div class="container-fluid">
            <div class="__invoicWrp wd100">
                <div class="d-flex" style=" justify-content: flex-end;">
                    @if ($invoice->party1_id != Auth::user()->id && $invoice->status == 'Pending')
                    <button class="btn btn-success pl-10 pr-10" id="btnAccept" data-toggle="modal"
                        data-target="#acceptInvoiceModel" type="button">
                        Accept
                    </button>
                    &nbsp;
                    <button class="btn btn-danger pl-10 pr-10" id="btnReject" type="button">
                        Reject
                    </button>
                    @else
                    @if($invoice->party1_id == Auth::user()->id && $invoice->payment_status != 'Paid')
                    <a href="{{ url(''.$url.'/event/' . $event->id . '/invoices/' . $invoice->id . '/edit') }}"><button class="btn btn-primary pl-10 pr-10" type="button">
                        Edit
                    </button></a>
                    @endif
                    @endif
                    @if ($invoice->party1_id == Auth::user()->id && $invoice->payment_status == 'Paid')
                    &nbsp;
                    <button class="btn btn-success pl-10 pr-10" id="btnCollect" data-toggle="modal"
                    data-target="#collectStatusModel" type="button">
                        Update Collect Status
                    </button>
                    @endif
                </div>
                <br>
                <div class="__viewIncWoz" style="width:800px; margin: 0 auto;">
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"
                        style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
                        <tr>
                            <td width="70%" align="left" valign="middle">
                                <img src="/storage/{{ $invoice->party1->bussinessDetails['logo'] }}" width="120px" />
                            </td>
                            <td width="30%" align="left" valign="middle" style="font-size:14px; line-height:25px;">

                                <h3 style="font-size:18px; margin:0px 0px; padding:0 0;">
                                    {{ $invoice->party1->first_name }}
                                    {{ $invoice->party1->last_name }}</h3>
                                {{ $invoice->party1->bussinessDetails['address'] }}<br>
                                Invoice No &nbsp;&nbsp; : {{ $invoice->invoice_sn }} <br>
                                Invoice Date : {{ $invoice->date }} <br> Due Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                {{ $invoice->due_date }} <br>
                                Status &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                {{ $invoice->status }}
                            </td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="10" style="margin-top:20px;border:#8A8A8A 1px solid;
                font-family:Arial, Helvetica, sans-serif;
                font-size:13px;
                ">
                        <tr>
                            <th width="33%" align="left" valign="middle"
                                style="color:#7A5C66; border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                Events Details</th>
                            <th width="33%" align="left" valign="middle"
                                style="color:#7A5C66; border-bottom:#8A8A8A 1px solid;  border-right:#8A8A8A 1px solid;">
                                Event Planner Details</th>
                            <th width="33%" align="left" valign="middle"
                                style="color:#7A5C66; border-bottom:#8A8A8A 1px solid; ">Customer Details</th>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style=" border-right:#8A8A8A 1px solid;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="4" style="font-family:Arial, Helvetica, sans-serif;
                font-size:13px;">
                                    <tr>
                                        <td width="35%"><strong>Event Name</strong></td>
                                        <td width="5%" style="color:#000">:</td>
                                        <td width="60%" style="color:#8A8A8A"> {{ $invoice->event['event_name'] }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Event Date </strong></td>
                                        <td style="color:#000">:</td>
                                        <td style="color:#8A8A8A">{{ $invoice->event['event_date'] }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Guest </strong></td>
                                        <td style="color:#000">:</td>
                                        <td style="color:#8A8A8A">{{ $invoice->event['number_of_attendees'] }}</td>
                                    </tr>
                                </table>
                            </td>
                            <td align="left" valign="top" style=" border-right:#8A8A8A 1px solid;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="4" style="font-family:Arial, Helvetica, sans-serif;
                font-size:13px;">
                                    <tr>
                                        <td><strong>Name</strong></td>
                                        <td style="color:#000">:</td>
                                        <td style="color:#8A8A8A">{{ $invoice->party2['first_name'] }}
                                            {{ $invoice->party2['last_name'] }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Address </strong></td>
                                        <td style="color:#000">:</td>
                                        <td style="color:#8A8A8A">{{ $invoice->party2->bussinessDetails['address'] }}
                                        </td>
                                    </tr>
                                </table>

                            </td>
                            <td align="left" valign="top">
                                <table width="100%" border="0" cellspacing="0" cellpadding="4" style="font-family:Arial, Helvetica, sans-serif;
                font-size:13px;">
                                    <tr>
                                        <td><strong>Name</strong></td>
                                        <td style="color:#000">:</td>
                                        <td style="color:#8A8A8A">@if($invoice->event['customer']) {{ $invoice->event['customer']['first_name'] }}
                                            {{ $invoice->event['customer']['last_name'] }} @endif</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Address </strong></td>
                                        <td style="color:#000">:</td>
                                        <td style="color:#8A8A8A">@if($invoice->event['customer']) {{ $invoice->event['customer']['email'] }} @endif </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br /><br />


                    <h5 style="color:#7A5C66; margin:0 0 10px; font-size:16px; font-family:Arial, Helvetica, sans-serif; ">
                        Scope of work</h5>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5"
                        style=" border:#8A8A8A 1px solid; border-bottom:none;  font-family:Arial, Helvetica, sans-serif;  font-size:13px;  ">
                        <tr>
                            <th width="6%" align="center" valign="middle"
                                style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">Sl.No</th>
                            <th width="9%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">Service
                                Type</th>
                            <th width="26%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                Description</th>
                            <th width="8%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                {{$invoice->contract->proposal->request->category['qty_label']?:'Quantity'}}</th>
                            <th width="8%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">Amount
                            </th>
                            <th width="8%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">Tax
                            </th>
                            <th width="8%" style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">Remark
                            </th>
                            <th width="11%" style=" border-bottom:#8A8A8A 1px solid; ">Total</th>
                        </tr>
                        @foreach ($invoice->items as $key => $item)
                            <tr>
                                <td align="center" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $key + 1 }}</td>
                                <td align="center" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item['item_name'] }}</td>
                                <td align="left" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item['description'] }}</td>
                                <td align="center" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item['quantity'] }}</td>
                                <td align="right" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item['amount'] }}</td>
                                <td align="center" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item['tax'] }} %</td>
                                <td align="left" valign="middle"
                                    style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;">
                                    {{ $item['remarks'] }}</td>
                                <td align="right" valign="middle" style=" border-bottom:#8A8A8A 1px solid;">AED
                                    {{ $item['total'] }}</td>
                            </tr>
                        @endforeach


                        <tr>
                            <td colspan="7" align="right" valign="middle"
                                style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;"><strong>Total
                                    Amount</strong></td>
                            <td align="right" valign="middle" style=" border-bottom:#8A8A8A 1px solid;  "><strong>AED
                                    {{ $invoice->total_amount }}</strong></td>
                        </tr>
                        <tr>
                            <td colspan="7" align="right" valign="middle"
                                style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;"><strong>Total
                                    Paid</strong></td>
                            <td align="right" valign="middle" style=" border-bottom:#8A8A8A 1px solid;  "><strong>AED
                                    {{ $invoice->total_paid }}</strong></td>
                        </tr>
                        <tr>
                            <td colspan="7" align="right" valign="middle"
                                style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;"><strong>Invoice
                                    Amount</strong></td>
                            <td align="right" valign="middle" style=" border-bottom:#8A8A8A 1px solid;  "><strong>AED
                                    {{ $invoice->total_payable }}</strong></td>
                        </tr>
                        <tr>
                            <td colspan="7" align="right" valign="middle"
                                style=" border-bottom:#8A8A8A 1px solid; border-right:#8A8A8A 1px solid;"><strong>Balance
                                    Amount</strong></td>
                            <td align="right" valign="middle" style=" border-bottom:#8A8A8A 1px solid;  "><strong>AED
                                    {{ $invoice->total_amount - $invoice->total_paid - $invoice->total_payable }}</strong>
                            </td>
                        </tr>
                    </table>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif;
                font-size:12px;
                margin-top:50px;
                border-top: #8A8A8A 1px solid;
                color:#000">
                        <tr>
                            <td style="padding-top:15px;"><strong>Terms and Conditions:</strong>
                                <ol style="    margin: 10px 14px;  padding: 0; ">
                                    @foreach (json_decode($invoice->terms_of_agreement) as $key => $terms)
                                        <li data-text="{{ $terms->text }}" data-row="{{ $key }}">
                                            {{ $terms->text }}
                                        </li>
                                    @endforeach
                                </ol>

                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--end::Container-->
        </div>
    </div>
    @include('vendor.invoices.accept_invoice')
    @include('vendor.invoices.collect_status')
@endsection

@section('scripts')
    <script>
        $("#btnPayment").click(function(event) {
            event.preventDefault();
            var form = $('#invoice-payment-form')[0];
            var data = new FormData(form);
            $("#btnPayment").prop("disabled", true);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/vendor/event/{{ $event->id }}/accept-invoice/{{ $invoice->id }}",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    $("#btnPayment").prop("disabled", false);
                    setTimeout(function() {
                        location.replace(
                            '/{{$url}}/event/{{ $event->id }}/invoices');
                    }, 1000);
                },
                error: function(e) {
                    $("#btnPayment").prop("disabled", false);
                }
            });
        });

        $("#btnCollectStatus").click(function(event) {
            event.preventDefault();
            var form = $('#collect-status-form')[0];
            var data = new FormData(form);
            $("#btnCollectStatus").prop("disabled", true);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/vendor/event/{{ $event->id }}/invoice-collect-status/{{ $invoice->id }}",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    $("#btnCollectStatus").prop("disabled", false);
                    setTimeout(function() {
                        location.replace(
                            '/{{$url}}/event/{{ $event->id }}/invoices');
                    }, 1000);
                },
                error: function(e) {
                    $("#btnCollectStatus").prop("disabled", false);
                }
            });
        });

        $("#btnReject").click(function(event) {
            event.preventDefault();
            $("#btnReject").prop("disabled", true);
            Swal.fire({
                title: "Are you sure?",
                text: "You Want To Reject This Invoice.",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, Reject!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function(result) {
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        enctype: 'multipart/form-data',
                        url: "/vendor/event/{{ $event->id }}/reject-invoice/{{ $invoice->id }}",
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function(data) {
                            $("#btnReject").prop("disabled", false);
                            setTimeout(function() {
                                location.replace(
                                    '/{{$url}}/event/{{ $event->id }}/invoices');
                            }, 1000);
                        },
                        error: function(e) {
                            $("#btnReject").prop("disabled", false);
                        }
                    });
                } else if (result.dismiss === "cancel") {
                    // Swal.fire(
                    //     "Cancelled",
                    //     "",
                    //     "error"
                    // )
                }
            });
        });
    </script>
@endsection
