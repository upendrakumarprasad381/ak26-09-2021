{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/leads/') }}" class="text-muted">Leads</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/leads/recieved-leads') }}" class="text-muted">Received Leads</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/leads/recieved-leads/' . $recieved_lead->event->id . '') }}"
            class="text-muted">{{ $recieved_lead->event->event_name }}</a>
    </li>
@endsection
@section('content')
    @if ($recieved_lead->event_planner_id)
        @php
            $accept_url = '/vendor/leads/accept-event-planner-lead';
            $decline_url = '/vendor/leads/decline-event-planner-lead';
            $budget = '';
            if ($recieved_lead->budget > 0) {
                $budget = $budget . 'AED ' . $recieved_lead->budget;
            }
            if ($recieved_lead->max_budget > 0) {
                $budget = $recieved_lead->budget > 0 ? $budget . ' - ' : '';
                $budget = $budget . 'AED ' . $recieved_lead->max_budget;
            }
        @endphp
    @else
        @php
            $accept_url = '/vendor/leads/accept-lead';
            $decline_url = '/vendor/leads/decline-lead';
            $budget = '';
            if ($recieved_lead->minbudget > 0) {
                $budget = $budget . 'AED ' . $recieved_lead->minbudget;
            }
            if ($recieved_lead->maxbudget > 0) {
                $budget = $recieved_lead->minbudget > 0 ? $budget . ' - ' : '';
                $budget = $budget . 'AED ' . $recieved_lead->maxbudget;
            }
        @endphp
    @endif
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container-fluid">
            <!--begin::Dashboard-->
            <div class="row __sublk">
                <div class="col-xl-3">
                    <!--begin::Stats Widget 16-->
                    <a href="{{ route('vendor.recieved-leads') }}"
                        class="card card-custom card-stretch  @if ($recieved_lead->status == 0) __active @endif">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span
                                    class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{ $leads_counts['received'] }}</span>
                            </div>

                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Received Leads</div>

                        </div>
                        <!--end::Body-->
                    </a>
                    <!--end::Stats Widget 16-->
                </div>

                <div class="col-xl-3">
                    <!--begin::Stats Widget 16-->
                    <a href="{{ route('vendor.recieved-leads') }}"
                        class="card card-custom card-stretch @if ($recieved_lead->status == 1) __active @endif">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span
                                    class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{ $leads_counts['opened'] }}</span>
                            </div>

                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Opened Leads</div>

                        </div>
                        <!--end::Body-->
                    </a>
                    <!--end::Stats Widget 16-->
                </div>

                <div class="col-xl-3">
                    <!--begin::Stats Widget 16-->
                    <a href="{{ route('vendor.ongoing-leads') }}"
                        class="card card-custom card-stretch @if ($recieved_lead->status == 2) __active @endif">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span
                                    class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{ $leads_counts['ongoing'] }}</span>
                            </div>

                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Ongoing Leads</div>

                        </div>
                        <!--end::Body-->
                    </a>
                    <!--end::Stats Widget 16-->
                </div>

                <div class="col-xl-3">
                    <!--begin::Stats Widget 16-->
                    <a href="{{ route('vendor.declined-leads') }}"
                        class="card card-custom card-stretch @if ($recieved_lead->status == 3) __active @endif">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span
                                    class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{ $leads_counts['declined'] }}</span>
                            </div>

                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Declined Leads</div>

                        </div>
                        <!--end::Body-->
                    </a>
                    <!--end::Stats Widget 16-->
                </div>
            </div>
            <div class="row __ledBlkWrp mb-1">
                <div class="col-lg-4 col-md-4 col-sm-12 __lddelbk">
                    <div class="__lddelbkImg">
                        <a href="#"><img class="img-fluid" @if ($recieved_lead->event['image'] != null) src="{{ asset('storage/' . $recieved_lead->event['image'] . '') }}" @else src="{{ asset('storage/' . $recieved_lead->event->occasion['image'] . '') }}" @endif></a>
                    </div>
                    <h3 class="text-center font-weight-bolder mt-3 mb-3">@if ($recieved_lead->event['customer']){{ $recieved_lead->event['customer']['first_name'] }} {{ $recieved_lead->event['customer']['last_name'] }} @endif @if ($recieved_lead->event_planner_id) - {{ $recieved_lead->category['name'] }} @else - @foreach ($recieved_lead->categoriesList as $category) {{ $category['name'] }} @if ($loop->iteration != count($recieved_lead->categoriesList)) | @endif @endforeach @endif </h3>
                </div>

                <div class="col-lg-5 col-md-5 col-sm-12 __lddelbk_REblk">
                    <div class="wd100 __ldde_REblk">
                        <h3 class="font-weight-bolder mt-5">Requirement of Event</h3>

                        <div class="wd100">

                            <div class="__REbntGrid row row-cols-1 row-cols-sm-1 row-cols-md-3 row-cols-lg-3">
                                <div class="__REbnt tiles">
                                    <div class="wd100 __REbntboz tile">
                                        <i class="flaticon-calendar"></i>
                                        <div class="details">
                                            <span class="title">Date</span>
                                            <span
                                                class="info">@if($recieved_lead['event_date']) {{ date('d/m/Y', strtotime($recieved_lead['event_date'])) }} @else {{ date('d/m/Y', strtotime($recieved_lead->event['event_date'])) }} @endif</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="__REbnt tiles">
                                    <div class="wd100 __REbntboz tile">
                                        <i class="flaticon-budget"></i>
                                        <div class="details">
                                            <span class="title">Budget</span>
                                            <span class="info">{{ $budget }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="__REbnt tiles">
                                    <div class="wd100 __REbntboz tile">
                                        <img src="{{ asset('img/Venue.svg') }}" />
                                        <div class="details">
                                            <span class="title">Venue</span>
                                            <span class="info"> @if ($recieved_lead->event['event_location']) {{ $recieved_lead->event['event_location'] }} @else Not specified @endif</span>
                                        </div>
                                    </div>
                                </div>


                                <div class="__REbnt tiles">
                                    <div class="wd100 __REbntboz tile">
                                        <i class="flaticon-map-location" style=" margin-top: -15px;"></i>
                                        <div class="details">
                                            <span class="title">Event Location</span>
                                            <span class="info"> @if ($recieved_lead->event['event_location']) {{ $recieved_lead->event['event_location'] }}  @endif</span>
                                        </div>
                                    </div>
                                </div>

                                @if ($recieved_lead->event['type_of_event'] == 'Indoor')
                                    <div class="__REbnt tiles">
                                        <div class="wd100 __REbntboz tile">
                                            <img src="{{ asset('img/indoor.svg') }}" />
                                            <div class="details">
                                                <span class="title">Indoor</span>
                                                <span class="info"> </span>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="__REbnt tiles">
                                        <div class="wd100 __REbntboz tile">
                                            <img src="{{ asset('img/outdoor.svg') }}" />
                                            <div class="details">
                                                <span class="title">Outdoor</span>
                                                <span class="info"> </span>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <div class="__REbnt tiles">
                                    <div class="wd100 __REbntboz tile">
                                        <i style="margin-top: -15px;" class="flaticon-users"></i>
                                        <div class="details">
                                            <span class="title">Guest</span>
                                            <span
                                                class="info">{{ $recieved_lead->event['number_of_attendees'] }}
                                            </span>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>



                    </div>


                </div>

                <div class="col-lg-3 col-md-3 col-sm-12 __ldDPg_actBz">

                    <div class="wd100 __ldDPg_actBzInrp">

                        <a href="#" class="__inrldBBtn approvedBtn @if ($recieved_lead->status == 2) disabled @endif"
                            data-id="{{ $recieved_lead['id'] }}" data-toggle="tooltip" data-theme="dark" title="Accept">
                            <img src="{{ asset('img/tick.svg') }}">
                        </a>

                        <a href="#" class="__inrldBBtn declineBtn @if ($recieved_lead->status == 3) disabled @endif"
                            data-id="{{ $recieved_lead['id'] }}" data-toggle="tooltip" data-theme="dark" title="Decline">
                            <img src="{{ asset('img/cancel.svg') }}">
                        </a>

                        <a href="#" class="__inrldBBtn  @if ($recieved_lead->status == 2) disabled @endif" data-toggle="tooltip" data-theme="dark"
                            title="I will think about it.">
                            <img src="{{ asset('img/information.svg') }}">
                        </a>
                        <a href="/conversations" target="_blank" class="__inrldBBtn @if ($recieved_lead->status == 3) disabled @endif"
                            data-toggle="tooltip" data-theme="dark" title="Comminicate">
                            <img src="{{ asset('img/transfer.svg') }}">
                        </a>
                        @if ($recieved_lead->event_planner_id)
                            <a href="https://web.whatsapp.com/send?phone=<?= $recieved_lead->event_planner->bussinessDetails->whatsapp ?>text=Hello"
                                target="_blank" class="__inrldBBtn" data-toggle="tooltip" data-theme="dark"
                                title="Whatsapp">
                                <img src="{{ asset('img/whatsapp.svg') }}">
                            </a>
                        @else
                            <a href="https://web.whatsapp.com/send?phone=<?= $recieved_lead->vendor->bussinessDetails->whatsapp ?>text=Hello"
                                class="__inrldBBtn" data-toggle="tooltip" data-theme="dark" title="Whatsapp">
                                <img src="{{ asset('img/whatsapp.svg') }}">
                            </a>
                        @endif
                        <a href="javascript:void(0);" class="__inrldBBtn btnSuggestModel @if ($recieved_lead->status == 2) disabled @endif"
                            data-toggle="tooltip" data-theme="dark" title="Suggest Lead">
                            <img src="{{ asset('img/chat.svg') }}">
                        </a>
                    </div>
                </div>
            </div>

            <div class="__droStrwrap accordion accordion-light accordion-toggle-arrow" id="accordionExample2">
                <div class="card">

                    <div class="card-header" id="headingOne2">
                        <div class="card-title" data-toggle="collapse" data-target="#collapseOne2">
                            View&nbsp;More
                        </div>
                    </div>

                    <div id="collapseOne2" class="collapse __droStr wd100" data-parent="#accordionExample2">
                        <div class="card-body  wd100">

                            <div class="wd100 ">
                                <div class="row">

                                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                                        <label>Occasion</label>
                                        <input type="text" readonly class="form-control"
                                            value="@if ($recieved_lead->event['occasion']){{ $recieved_lead->event['occasion']['name'] }} @endif">
                                    </div>
                                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                                        <label>Event Type</label>
                                        <input type="text" readonly class="form-control"
                                            value="@if ($recieved_lead->event['type_of_event'] == 'Indoor') Indoor @else Outdoor @endif">
                                    </div>
                                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                                        <label>Budget</label>
                                        <input type="text" readonly class="form-control" value="{{ $budget }}">
                                    </div>
                                    {{-- <div class="form-group col-lg-4 col-md-6 col-sm-12">
					<label>Do you have a venue picked out?</label>
                    <input type="text" readonly class="form-control" value="{{$recieved_lead->event['picked_a_venue']}}" readonly>
				</div> --}}
                                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                                        <label>Venue</label>
                                        <input type="text" readonly class="form-control"
                                            value="@if ($recieved_lead->event['event_location']) {{ $recieved_lead->event['event_location'] }} @else Not specified @endif">
                                    </div>
                                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                                        <label>Location</label>
                                        <input type="text" readonly class="form-control"
                                            value=" {{ $recieved_lead->event['event_location'] }}">
                                    </div>
                                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                                        <label>Event Date</label>
                                        <input type="text" readonly class="form-control"
                                            value="@if($recieved_lead['event_date']) {{ date('d/m/Y', strtotime($recieved_lead['event_date'])) }} @else {{ date('d/m/Y', strtotime($recieved_lead->event['event_date'])) }} @endif" readonly>

                                    </div>
                                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                                        <label>My Event Date Is Flexible</label>
                                        <input type="text" readonly class="form-control"
                                            value="{{ $recieved_lead->event['date_flexible'] }}" readonly>

                                    </div>
                                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                                        <label>Guest</label>
                                        <input type="text" readonly class="form-control"
                                            value="{{ $recieved_lead->event['number_of_attendees'] }}">
                                    </div>
                                    @if($recieved_lead->event_time)
                                    <div class="form-group col-lg-4 col-md-6 col-sm-12 ">
                                        <label>{{$recieved_lead->category['name']}} Start Time</label>
                                        <input type="text" readonly class="form-control" placeholder=""
                                            value="{{ date('h:i A', strtotime($recieved_lead['event_time'])) }}">
                                    </div>
                                    <div class="form-group col-lg-4 col-md-6 col-sm-12 ">
                                        <label></label>
                                        <label>{{$recieved_lead->category['name']}} Hours</label>
                                        <input type="text" readonly class="form-control" placeholder=""
                                            value="{{ $recieved_lead['number_of_hours'] }} Hours">
                                    </div>
                                    @endif
                                    <div class="form-group col-lg-4 col-md-6 col-sm-12 ">
                                        <label>Photography Start Time</label>
                                        <input type="text" readonly class="form-control" placeholder=""
                                            value="{{ date('h:i A', strtotime($recieved_lead->event['event_time'])) }}">
                                    </div>
                                    <div class="form-group col-lg-4 col-md-6 col-sm-12 ">
                                        <label></label>
                                        <label>Photography Hours</label>
                                        <input type="text" readonly class="form-control" placeholder=""
                                            value="{{ $recieved_lead->event['number_of_hours'] }} Hours">
                                    </div>
                                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                                        <label>Would you like to meet this vendor over video chat?</label>
                                        <input type="text" readonly class="form-control"
                                            value="{{ $recieved_lead->event['video_chat'] }}" readonly>
                                    </div>
                                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                                        <label>Note</label>
                                        <textarea class="form-control" readonly>{{ $recieved_lead->notes }}</textarea>
                                    </div>
                                    @if ($recieved_lead->event->additionalServices)
                                        <div class="form-group col-lg-4 col-md-6 col-sm-12 ">
                                            <label>Which additional services are you interested in?</label>
                                            <div class="checkbox-inline">
                                                @foreach ($recieved_lead->event->additionalServices as $services)
                                                    <label class="checkbox checkbox-outline">
                                                        <input type="checkbox" name="Checkboxes15" checked disabled />
                                                        <span></span>
                                                        {{ $services['title'] }}
                                                    </label>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        @include('vendor.leads.suggest_leads')

    @endsection

    {{-- Scripts Section --}}
    @section('scripts')
        <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
        <script>
            $('.approvedBtn').on('click', function(event) {
                var id = $(this).data('id');
                var url = '{{ $accept_url }}'

                Swal.fire({
                    title: "Are you sure?",
                    text: "You want to accept this lead.",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, Accept!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                }).then(function(result) {
                    if (result.value) {
                        $.ajax({
                            type: "POST",
                            enctype: 'multipart/form-data',
                            url: url,
                            data: {
                                id: id
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(data) {
                                // Swal.fire(
                                //     "Deleted!",
                                //     ""+data.message+"",
                                //     "success"
                                // )
                                setTimeout(function() {
                                    location.replace('/vendor/leads');
                                }, 1000);
                            },
                            error: function(e) {}
                        });
                    } else if (result.dismiss === "cancel") {
                        // Swal.fire(
                        //     "Cancelled",
                        //     "",
                        //     "error"
                        // )
                    }
                });
            });

            $('.declineBtn').on('click', function(event) {
                var id = $(this).data('id');
                var url = '{{ $decline_url }}'

                Swal.fire({
                    title: "Are you sure?",
                    text: "You want to decline this lead.",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, Decline!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                }).then(function(result) {
                    if (result.value) {
                        $.ajax({
                            type: "POST",
                            enctype: 'multipart/form-data',
                            url: url,
                            data: {
                                id: id
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(data) {
                                setTimeout(function() {
                                    location.replace('/vendor/leads');
                                }, 1000);
                            },
                            error: function(e) {}
                        });
                    } else if (result.dismiss === "cancel") {}
                });
            });
        </script>
        <script>
            $(function() {
                $('[data-toggle="tooltip"]').tooltip()
            });
            $('.btnSuggestModel').on('click', function() {
                $('#suggestLeadModel').modal('show');
            });
            $('#suggest_vendor').select2({
                placeholder: "Select a Vendor",
            });
            $('#btnSuggestBtn').on('click', function() {
                var form = $('#suggest-leads-form')[0];
                var data = new FormData(form);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: '/vendor/leads/suggest-lead',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        setTimeout(function() {
                            location.replace('/vendor/suggested-leads');
                        }, 1000);
                    },
                    error: function(e) {}
                });
            });
        </script>
    @endsection
