{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/leads/') }}" class="text-muted">Leads</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/leads/recieved-leads') }}" class="text-muted">{{$page_title}}</a>
    </li>
@endsection
{{-- Content --}}
@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container-fluid">
		<!--begin::Dashboard-->
		<div class="row __sublk">
			<div class="col-xl-3">
				<!--begin::Stats Widget 16-->
				<a href="{{route('vendor.recieved-leads')}}" class="card card-custom card-stretch  @if($page_title == 'Received Leads') __active @endif">
					<!--begin::Body-->
					<div class="card-body">
					 <div class="font-weight-bold ">
							<span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{$leads_counts['received']}}</span>
					</div>

						<div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Received Leads</div>

					</div>
					<!--end::Body-->
				</a>
				<!--end::Stats Widget 16-->
			</div>
				<div class="col-xl-3">
				<!--begin::Stats Widget 16-->
				<a href="{{route('vendor.recieved-leads')}}" class="card card-custom card-stretch @if($page_title == 'Opened Leads') __active @endif">
					<!--begin::Body-->
					<div class="card-body">
					 <div class="font-weight-bold ">
							<span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{$leads_counts['opened']}}</span>
					</div>

						<div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Opened Leads</div>

					</div>
					<!--end::Body-->
				</a>
				<!--end::Stats Widget 16-->
			</div>

				<div class="col-xl-3">
				<!--begin::Stats Widget 16-->
				<a href="{{route('vendor.ongoing-leads')}}" class="card card-custom card-stretch @if($page_title == 'Ongoing Leads') __active @endif">
					<!--begin::Body-->
					<div class="card-body">
					 <div class="font-weight-bold ">
							<span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{$leads_counts['ongoing']}}</span>
					</div>

						<div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Ongoing Leads</div>

					</div>
					<!--end::Body-->
				</a>
				<!--end::Stats Widget 16-->
			</div>

			<div class="col-xl-3">
				<!--begin::Stats Widget 16-->
				<a href="{{route('vendor.declined-leads')}}" class="card card-custom card-stretch @if($page_title == 'Declined Leads') __active @endif">
					<!--begin::Body-->
					<div class="card-body">
					 <div class="font-weight-bold ">
							<span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{$leads_counts['declined']}}</span>
					</div>

						<div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Declined Leads</div>

					</div>
					<!--end::Body-->
				</a>
				<!--end::Stats Widget 16-->
			</div>
		</div>
		<div class="row __ledBlkWrp">
            @foreach ($recieved_leads as $row)
            @if($row->event_planner_id)
            <div class="col-lg-3 col-md-6 col-sm-12 __ledBlk">
				<div class="wd100 __ledBlkInr">
					<div class="__ledBlkImg">
						<a href="{{route('vendor.event-planner-recieved-lead',$row->id)}}"><img class="img-fluid" @if($row->event['image'] != null) src="{{asset('storage/'.$row->event['image'].'')}}" @else src="{{asset('storage/'.$row->event->occasion['image'].'')}}" @endif></a>
					</div>
					<div class="__ledBlkDicp wd100">
						<h4 class="text-truncate"><a href="{{route('vendor.event-planner-recieved-lead',$row->id)}}">{{$row->event['event_name']}} - {{$row->category['name']}}</a></h4>
						<h5><a href="{{route('vendor.event-planner-recieved-lead',$row->id)}}">{{date('d M Y', strtotime($row->event['event_date']))}}</a></h5>
						<h6 class="text-truncate"><a href="#">{{$row['venue_name']}}</a></h6>
                        <p>From Event Planner</p>
					</div>
				</div>
			</div>
            @else
            <div class="col-lg-3 col-md-6 col-sm-12 __ledBlk">
				<div class="wd100 __ledBlkInr">
					<div class="__ledBlkImg">
						<a href="{{route('vendor.recieved-lead',$row->id)}}"><img class="img-fluid" src="{{asset('img/young-muslim-bride-groom-wedding-photos.png')}}"></a>
					</div>
					<div class="__ledBlkDicp wd100">
						<h4 class="text-truncate"><a href="{{route('vendor.recieved-lead',$row->id)}}">{{$row['event_name']}}</a></h4>
						<h5><a href="{{route('vendor.recieved-lead',$row->id)}}">{{date('d M Y', strtotime($row->wedding_date))}}</a></h5>
						<h6 class="text-truncate"><a href="#">{{$row['venue_name']}}</a></h6>
                        <p>From Customer</p>
					</div>
				</div>
			</div>
            @endif
            @endforeach

		</div>

		<!--end::Dashboard-->
	</div>
	<!--end::Container-->
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
@endsection
