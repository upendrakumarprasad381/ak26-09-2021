<!-- Modal-->
<div class="modal fade" id="suggestLeadModel" role="model" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Suggest Leads</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="suggest-leads-form">
                    @csrf
                    <div class="wd100 __proslAddWrp __addscpWrkPg">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group __ctndrofid">
                                <label class="form-label">Vendors</label>
                                <select class="form-control select2" name="vendor" id="suggest_vendor">
                                    <option value></option>
                                    @foreach ($vendors as $vendor)
                                    <option value="{{$vendor['id']}}">{{$vendor['first_name']}}{{$vendor['last_name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                <label class="form-label">Note</label>
                                <textarea class="form-control" rows="3" name="note" id="suggest_note"></textarea>
                            </div>
                            <input type="hidden" name="request_id" id="suggest_request_id"
                                value="{{ $recieved_lead['id'] }}">
                                <input type="hidden" name="type" value="@if($recieved_lead->event_planner_id)Event Planner @else Customer @endif">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="button" id="btnSuggestBtn" class="btn btn-primary font-weight-bold">Suggest</button>
            </div>
        </div>
    </div>
</div>
