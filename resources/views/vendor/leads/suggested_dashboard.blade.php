{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/leads/') }}" class="text-muted">{{$page_title}}</a>
    </li>
@endsection
{{-- Content --}}
@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container-fluid">
		<!--begin::Dashboard-->
	<div class="row __mainblk">
		<div class="col-xl-3">
			<a href="{{route('vendor.leads')}}" class="card card-custom card-stretch">
				<div class="card-body">
 					<div class="__textLebs font-weight-bolder font-size-h3">Received Leads</div>
				</div>
			</a>
		</div>
	<div class="col-xl-3">
			<a href="{{route('vendor.look-for-leads')}}" class="card card-custom card-stretch  @if($page_title == 'Look For Leads') __active @endif">
				<div class="card-body">
 					<div class="__textLebs font-weight-bolder font-size-h3">Look For Leads</div>
				</div>
			</a>
		</div>
	<div class="col-xl-3">
			<a href="{{route('vendor.suggested-leads')}}" class="card card-custom card-stretch @if($page_title == 'Suggested Leads') __active @endif">
				<div class="card-body">
 					<div class="__textLebs font-weight-bolder font-size-h3">Suggest Leads</div>
				</div>
			</a>
		</div>
	</div>
    <div class="row __ledBlkWrp">
        @foreach ($recieved_leads as $row)
        @if($row->event_planner_id)
        <div class="col-lg-3 col-md-6 col-sm-12 __ledBlk">
            <div class="wd100 __ledBlkInr">
                <div class="__ledBlkImg">
                    <a href="{{route('vendor.event-planner-recieved-lead',$row->id)}}"><img class="img-fluid" @if($row->event['image'] != null) src="{{asset('storage/'.$row->event['image'].'')}}" @else src="{{asset('storage/'.$row->event->occasion['image'].'')}}" @endif></a>
                </div>
                <div class="__ledBlkDicp wd100">
                    <h4 class="text-truncate"><a href="{{route('vendor.event-planner-recieved-lead',$row->id)}}">{{$row->event['event_name']}} - {{$row->category['name']}}</a></h4>
                    <h5><a href="{{route('vendor.event-planner-recieved-lead',$row->id)}}">{{date('d M Y', strtotime($row->event['event_date']))}}</a></h5>
                    <h6 class="text-truncate"><a href="#">{{$row['venue_name']}}</a></h6>
                    @if($page_title == 'Look For Leads')
                    <p>Suggested By {{$row->vendor['first_name']}} {{$row->vendor['last_name']}}</p>
                    @else
                    <p>Suggested To {{$row->suggested_to['first_name']}} {{$row->suggested_to['last_name']}}</p>
                    @endif
                </div>
            </div>
        </div>
        @else
        <div class="col-lg-3 col-md-6 col-sm-12 __ledBlk">
            <div class="wd100 __ledBlkInr">
                <div class="__ledBlkImg">
                    <a href="{{route('vendor.recieved-lead',$row->id)}}"><img class="img-fluid" src="{{asset('img/young-muslim-bride-groom-wedding-photos.png')}}"></a>
                </div>
                <div class="__ledBlkDicp wd100">
                    <h4 class="text-truncate"><a href="{{route('vendor.recieved-lead',$row->id)}}">{{$row['event_name']}}</a></h4>
                    <h5><a href="{{route('vendor.recieved-lead',$row->id)}}">{{date('d M Y', strtotime($row->wedding_date))}}</a></h5>
                    <h6 class="text-truncate"><a href="#">{{$row['venue_name']}}</a></h6>
                    @if($page_title == 'Look For Leads')
                    <p>Suggested By {{$row->vendor['first_name']}} {{$row->vendor['last_name']}}</p>
                    @else
                    <p>Suggested To {{$row->suggested_to['first_name']}} {{$row->suggested_to['last_name']}}</p>
                    @endif
                </div>
            </div>
        </div>
        @endif
        @endforeach

    </div>

		<!--end::Dashboard-->
	</div>
	<!--end::Container-->
</div>
<!--end::Entry-->
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
@endsection
