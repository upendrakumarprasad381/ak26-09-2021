{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">

    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Profile Personal Information-->
            <div class="d-flex flex-row">
                <!--begin::Aside-->
                <div class="flex-row-auto offcanvas-mobile w-250px w-xxl-350px" id="kt_profile_aside">
                    <!--begin::Profile Card-->
                    <div class="card card-custom card-stretch">
                        <!--begin::Body-->
                        <div class="card-body pt-4">
                            <!--begin::Nav-->
                            <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                                <div class="navi-item mb-2">
                                    <a href="{{ url('/vendor/default-tax') }}" class="navi-link py-4 @if($page_title == 'Default Tax') active @endif">
                                        <span class="navi-icon mr-2">
                                            <i class="fas fa-percentage"></i>
                                        </span>
                                        <span class="navi-text font-size-lg">Default Tax</span>
                                    </a>
                                </div>
                                <div class="navi-item mb-2">
                                    <a href="{{ url('/vendor/terms-conditions') }}" class="navi-link py-4 @if($page_title == 'Terms & Conditions') active @endif">
                                        <span class="navi-icon mr-2">
                                            <i class="fas fa-list"></i>
                                        </span>
                                        <span class="navi-text font-size-lg">Terms & Conditions</span>
                                    </a>
                                </div>
                                <div class="navi-item mb-2">
                                    <a href="{{ url('/'.$url.'/member-designations') }}" class="navi-link py-4 @if($page_title == 'Designations') active @endif">
                                        <span class="navi-icon mr-2">
                                            <i class="fas fa-user"></i>
                                        </span>
                                        <span class="navi-text font-size-lg">Designations</span>
                                    </a>
                                </div>
                                <div class="navi-item mb-2">
                                    <a href="{{ url('/'.$url.'/no-of-attendees') }}" class="navi-link py-4 @if($page_title == 'No of Attendees') active @endif">
                                        <span class="navi-icon mr-2">
                                            <i class="fas fa-list-ol"></i>
                                        </span>
                                        <span class="navi-text font-size-lg">No of Attendees</span>
                                    </a>
                                </div>
                            </div>
                            <!--end::Nav-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Profile Card-->
                </div>
                <!--end::Aside-->
                @yield('master_settings_content')
            </div>
            <!--end::Profile Personal Information-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

