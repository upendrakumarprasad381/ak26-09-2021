@extends('vendor.masterSettings.master_settings_layout')
@section('master_settings_content')
    <!--begin::Content-->
    <div class="flex-row-fluid ml-lg-8">
        <!--begin::Card-->
        <div class="card card-custom card-stretch">
            <!--begin::Header-->
            <div class="card-header py-3">
                <div class="card-title align-items-start flex-column">
                    <h3 class="card-label font-weight-bolder text-dark">Member Designations
                    </h3>
                </div>
                <div class="card-toolbar">
                    <button type="submit" id="btnSubmit" class="btn btn-success mr-2">Save</button>
                    <a href="{{ url('/'.$url.'/member-designations') }}"><button type="reset"
                            class="btn btn-secondary">Cancel</button></a>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Form-->
            @php
                $url_ = '/'.$url.'/member-designations';
                if (isset($editable)) {
                    $url_ = '/'.$url.'/member-designations/' . $editable->id;
                }
            @endphp
            <form class="form" id="form">
                @csrf
                @if (isset($editable))
                    @method('PATCH')
                @endif
                <!--begin::Body-->
                <div class="card-body">
                    <div class="row">
                        <div class="form-group validated col-md-12">
                            <label class="form-control-label">Name</label>
                            <input class="form-control" id="name" name="name" focus value="@isset($editable){{ $editable->name }}@endisset">
                            </div>
                        </div>
                    </div>

                </form>
                <!--end::Form-->
            </div>
        </div>
        <!--end::Content-->
    @endsection

    {{-- Scripts Section --}}
    @section('scripts')
        <script type="text/javascript">
            var url = "{{ $url_ }}";
            $("#btnSubmit").click(function(event) {
                event.preventDefault();
                var form = $('#form')[0];
                var data = new FormData(form);
                $("#btnSubmit").prop("disabled", true);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: url,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(data) {
                        $("#btnSubmit").prop("disabled", false);
                        setTimeout(function() {
                            location.replace('/{{$url}}/member-designations');
                        }, 1000);

                    },
                    error: function(e) {
                        $("#btnSubmit").prop("disabled", false);
                    }
                });
            });
        </script>
    @endsection
