@extends('vendor.masterSettings.master_settings_layout')
@section('master_settings_content')
                <!--begin::Content-->
                <div class="flex-row-fluid ml-lg-8">
                    <!--begin::Card-->
                    <div class="card card-custom card-stretch">
                        <!--begin::Header-->
                        <div class="card-header py-3">
                            <div class="card-title align-items-start flex-column">
                                <h3 class="card-label font-weight-bolder text-dark">Default Tax</h3>
                                <span class="text-muted font-weight-bold font-size-sm mt-1">Update your Tax.</span>
                            </div>
                            <div class="card-toolbar">
                                <button type="submit" id="btnSubmit" class="btn btn-success mr-2">Save Changes</button>
                                <a href="{{url('/dashboard')}}"><button type="reset" class="btn btn-secondary">Cancel</button></a>
                            </div>
                        </div>
                        <!--end::Header-->
                        <!--begin::Form-->
                        <form class="form" id="form">
                            @csrf
                            <!--begin::Body-->
                            <div class="card-body">
                                <div class="row">
                                <div class="form-group validated col-md-12">
                                    <label class="form-control-label">Default Tax</label>
                                    <select class="form-control" name="tax" id="tax">
                                        <option value>Select Tax</option>
                                        @foreach ($taxes as $tax)
                                                <option value="{{$tax->id}}" @if($data->default_tax == $tax->id) selected @endif>{{$tax->name}}</option>
                                        @endforeach
                                     </select>
                                </div>

                                </div>
                            </div>
                            <!--end::Body-->
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
                <!--end::Content-->

@endsection

{{-- Scripts Section --}}
@section('scripts')
<script type="text/javascript">

$("#btnSubmit").click(function (event) {
    event.preventDefault();
    var form = $('#form')[0];
    var data = new FormData(form);
    $("#btnSubmit").prop("disabled", true);
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "/vendor/master-settings",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
            $("#btnSubmit").prop("disabled", false);
            setTimeout(function () {
                location.reload();
            }, 1000);
        },
        error: function (e) {
            $("#btnSubmit").prop("disabled", false);
        }
    });
});
</script>
@endsection
