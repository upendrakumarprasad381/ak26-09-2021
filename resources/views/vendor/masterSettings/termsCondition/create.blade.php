@extends('vendor.masterSettings.master_settings_layout')
@section('master_settings_content')
    <!--begin::Content-->
    <div class="flex-row-fluid ml-lg-8">
        <!--begin::Card-->
        <div class="card card-custom card-stretch">
            <!--begin::Header-->
            <div class="card-header py-3">
                <div class="card-title align-items-start flex-column">
                    <h3 class="card-label font-weight-bolder text-dark">Terms And Conditions
                    </h3>
                </div>
                <div class="card-toolbar">
                    <button type="submit" id="btnSubmit" class="btn btn-success mr-2">Save</button>
                    <a href="{{ url('/vendor/terms-conditions') }}"><button type="reset"
                            class="btn btn-secondary">Cancel</button></a>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Form-->
            @php
                $url = '/vendor/terms-conditions';
                if (isset($editable)) {
                    $url = '/vendor/terms-conditions/' . $editable->id;
                }
            @endphp
            <form class="form" id="form">
                @csrf
                @if (isset($editable))
                    @method('PATCH')
                @endif
                <!--begin::Body-->
                <div class="card-body">
                    <div class="row">
                        <div class="form-group validated col-md-12">
                            <label class="form-label">Type</label>
                            <select class="form-control" name="type" id="type">
                                <option value>Select Type</option>
                                <option value="Proposal" @isset($editable) @if ($editable->type == 'Proposal') selected @endif @endisset>Proposal
                                </option>
                                <option value="Contract" @isset($editable) @if ($editable->type == 'Contract') selected @endif @endisset>Contract
                                </option>
                                <option value="Invoice" @isset($editable) @if ($editable->type == 'Invoice') selected @endif @endisset>Invoice
                                </option>
                            </select>
                        </div>
                        <div class="form-group validated col-md-12">
                            <label class="form-control-label">Name</label>
                            <input class="form-control" id="name" name="name" focus
                                value="@isset($editable){{ $editable->name }}@endisset">
                            </div>
                            <div class="form-group validated col-md-12">
                                <label class="form-control-label">Terms & Condition</label>
                                <textarea class="form-control" id="terms_and_condition" name="terms_and_condition" focus>@isset($editable){{ $editable->text }}@endisset
                                                                    </textarea>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-control-label">Default</label>
                                    <span class="switch switch-icon">
                                        <label>
                                            <input type="hidden" value="0" name="default" @isset($editable)@if ($editable->default == 0) checked="checked" @endif
                                                    @endisset @empty($editable) checked="checked" @endempty>
                                                <input type="checkbox" value="1" name="default"
                                                    @isset($editable)@if ($editable->default == 1) checked="checked" @endif @endisset @empty($editable)
                                                checked="checked" @endempty />
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>

                    </form>
                    <!--end::Form-->
                </div>

            @endsection

            {{-- Scripts Section --}}
            @section('scripts')
                <script type="text/javascript">
                    var url = "{{ $url }}";
                    $("#btnSubmit").click(function(event) {
                        event.preventDefault();
                        var form = $('#form')[0];
                        var data = new FormData(form);
                        $("#btnSubmit").prop("disabled", true);
                        $.ajax({
                            type: "POST",
                            enctype: 'multipart/form-data',
                            url: url,
                            data: data,
                            processData: false,
                            contentType: false,
                            cache: false,
                            success: function(data) {
                                $("#btnSubmit").prop("disabled", false);
                                setTimeout(function() {
                                    location.replace('/vendor/terms-conditions');
                                }, 1000);

                            },
                            error: function(e) {
                                $("#btnSubmit").prop("disabled", false);
                            }
                        });
                    });
                </script>
            @endsection
