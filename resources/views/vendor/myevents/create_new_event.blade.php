{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
	<!--begin::Container-->
	<div class="container-fluid">
		<!--begin::Dashboard-->
        <form id="my-form">
            @csrf
		<div class="row __lddelbk">
            <div class="col-lg-6 col-md-12 col-sm-12 form-group">
                <label>Select Customer</label>
                <div class="input-group">
                    <select class="form-control select2" id="customer_select" name="user_id">
                        <option value></option>
                        {{-- @foreach ($customers as $customer)
                            <option value="{{$customer['id']}}">{{$customer['first_name']}} {{$customer['last_name']}}</option>
                        @endforeach --}}
                    </select>
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#customerModel">New</button>
                    </div>
                </div>
			</div>
            <div class="col-lg-6 col-md-6 col-sm-12">
            </div>
			<div class="col-lg-6 col-md-6 col-sm-12 form-group">
				<label>Event Name</label>
				<input type="text" class="form-control" name="event_name" placeholder="Name of Event">
			</div>

			<div class="col-lg-6 col-md-6 col-sm-12 form-group">
				<label>Type Of Event</label>
				<select class="form-control" id="exampleSelect1" name="occasion">
					<option value>Select Event Type</option>
                    @foreach ($occasions as $occasion)
                        <option value="{{$occasion['id']}}">{{$occasion['name']}}</option>
                    @endforeach
				</select>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-12 form-group">
				<label>Upload File</label>
				<input class="form-control" type="file" name="document" id="formFile">

			</div>

			<div class="col-lg-6 col-md-6 col-sm-12 form-group mt-5">

				<label>Budget</label>

				<div class="row align-items-center">
					<!--
			  <div class="col-2">

			   <input type="text" class="form-control" id="kt_nouislider_3_input"  placeholder="Quantity"/>

			  </div>
			  <div class="col-2">
			   <input type="text" class="form-control" id="kt_nouislider_3.1_input"  placeholder="Quantity"/>
			  </div>
		-->
					<div class="col-12">
						<div id="kt_nouislider_3" class="nouislider"></div>
					</div>
				</div>
				<div style="display: none;">
					<div class="form-group row mb-6">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Basic Setup</label>
						<div class="col-lg-6 col-md-12 col-sm-12">
							<div class="row align-items-center">
								<div class="col-4">
									<input type="text" class="form-control" name="min_budget" id="kt_nouislider_1_input" placeholder="Quantity" value="100"/>
								</div>
								<div class="col-8">
									<div id="kt_nouislider_1" class="nouislider-drag-danger"></div>
								</div>
							</div>
							<span class="form-text text-muted mt-6">Input control is attached to slider</span>
						</div>
					</div>

					<div class="separator separator-dashed my-10"></div>

					<div class="form-group row mb-6">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Currency Formatting</label>
						<div class="col-lg-6 col-md-12 col-sm-12">
							<div class="row align-items-center">
								<div class="col-4">
									<input type="text" class="form-control" name="max_budget" id="kt_nouislider_2_input" placeholder="Currency" value="100"/>
								</div>
								<div class="col-8">
									<div id="kt_nouislider_2" class="nouislider nouislider-handle-danger"></div>
								</div>
							</div>
							<span class="form-text text-muted mt-6">To format the slider output, noUiSlider offers a format option.</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 form-group">
				<label>Number of Attendees</label>
				<input type="text" class="form-control" name="number_of_attendees" placeholder="Number of Attendees">
			</div>

			<div class="col-lg-3 col-md-3 col-sm-12 form-group">
				<label>Event Location</label>
				<input type="text" class="form-control" name="event_location" placeholder="Event Location">
			</div>
            <div class="col-lg-3 col-md-3 col-sm-12 form-group">
				<label>Planning Date</label>
				<input class="form-control" type="date" value="" name="planning_date" id="example-date-input" />
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 form-group">
				<label>Event Date</label>
				<input class="form-control" type="date" value="" name="event_date" id="example-date-input" />
			</div>
            <div class="col-lg-3 col-md-3 col-sm-12 form-group">
				<label>Time</label>
				<input class="form-control" type="time" value="" name="event_time" id="example-time-input" />
			</div>

			<div class="col-lg-4 col-md-4 col-sm-12 form-group">
				<div class="checkbox-inline">
					<label class="checkbox checkbox-outline">
						<input type="checkbox" class="type_checkbox" value="Indoor" name="type_of_event" checked/>
						<span></span>
						Indoor
					</label>
					<label class="checkbox checkbox-outline">
						<input type="checkbox" class="type_checkbox" value="Outdoor" name="type_of_event"/>
						<span></span>
						Outdoor
					</label>
				</div>
			</div>

		</div>
		<div class="wd100 mb-5">
			<button type="button" id="btnSubmit" class="btn btn-primary float-right __create_eventBtn">Create a Event</button>
		</div>
        </form>

		<!--end::Container-->
	</div>
</div>

<!-- Modal-->
<div class="modal fade" id="customerModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create New Customer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="new-customer-form">
                    @csrf
                    <div class="row __lddelbk">
                        <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" name="first_name" placeholder="First Name">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Customer Email">
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                            <label>Phone</label>
                            <input type="text" class="form-control" name="phone" placeholder="Customer Phone">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="button" id="btnCustomerSave" class="btn btn-primary font-weight-bold">Save</button>
            </div>
        </div>
    </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
    $("#btnSubmit").click(function (event) {
            event.preventDefault();
            var form = $('#my-form')[0];
            var data = new FormData(form);
            $("#btnSubmit").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/vendor/events",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    $("#btnSubmit").prop("disabled", false);
                    setTimeout(function () {
                        location.replace('/vendor/my-events');
                    }, 1000);
                },
                error: function (e) {
                    $("#btnSubmit").prop("disabled", false);
                }
            });
        });

        $("#btnCustomerSave").click(function (event) {
            event.preventDefault();
            var form = $('#new-customer-form')[0];
            var data = new FormData(form);
            $("#btnCustomerSave").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/vendor_create_customer",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    $("#btnCustomerSave").prop("disabled", false);
                    $('#customer_select').append('<option value='+data.user.id+'>'+data.user.first_name+' '+data.user.last_name+'</option>');
                    $('#customer_select').val(data.user.id);
                    $('#customerModel').modal('hide');
                },
                error: function (e) {
                    $("#btnCustomerSave").prop("disabled", false);
                }
            });
        });

        // init slider
        var slider = document.getElementById('kt_nouislider_3');

        noUiSlider.create(slider, {
            start: [0, 10000],
            connect: true,
            tooltips: [true, wNumb({
                decimals: 2
            })],
            range: {
                'min': 0,
                'max': 10000
            }
        });


        // init slider input
        var sliderInput0 = document.getElementById('kt_nouislider_1_input');
        var sliderInput1 = document.getElementById('kt_nouislider_2_input');
        var sliderInputs = [sliderInput1, sliderInput0];

        slider.noUiSlider.on('update', function(values, handle) {
            sliderInputs[handle].value = values[handle];
        });

        $('#customer_select').select2({
            placeholder: "Select a Customer",
        });
        $('.type_checkbox').click(function() {
            $('.type_checkbox').not(this).prop('checked', false);
        });

</script>
<!--end::Page Scripts-->
@endsection
