{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
<!--begin::Container-->
<div class="container-fluid">
<!--begin::Dashboard-->
    <div class="row __cmFltTp __lddelbk ">
    <div class="col">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 mb-3">
                <label class="form-label">Event Type</label>
                <select class="form-control" id="exampleSelect1">
                    <option>Select your Event Type</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 mb-3">
                <label class="form-label">Location</label>
                <select class="form-control" id="exampleSelect1">
                    <option>Select your location</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 mb-3">
                <label class="form-label">Status</label>
                <select class="form-control" id="exampleSelect1">
                    <option>Select your status</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
            </div>
        </div>
    </div>
<div class="col-md-auto">
    <label class="form-label wd100 __cmoemynone">&nbsp; </label>
        <button type="button" class="btn btn-primary">Advance Option</button>
        <button type="button" class="btn btn-secondary">Sort By</button>
</div>

</div>

<!--begin::Dashboard-->
<div class="row __sublk">
<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
    <!--begin::Stats Widget 16-->
    <a href="" class="card card-custom card-stretch">
        <!--begin::Body-->
        <div class="card-body">
            <div class="font-weight-bold ">
                <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{$event_counts['total']}}</span>
            </div>

            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Total Events</div>

        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 16-->
</div>

<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
    <!--begin::Stats Widget 16-->
    <a href="" class="card card-custom card-stretch">
        <!--begin::Body-->
        <div class="card-body">
            <div class="font-weight-bold ">
                <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{$event_counts['active']}}</span>
            </div>

            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Active Events</div>

        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 16-->
</div>

<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
    <!--begin::Stats Widget 16-->
    <a href="" class="card card-custom card-stretch">
        <!--begin::Body-->
        <div class="card-body">
            <div class="font-weight-bold ">
                <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{$event_counts['proposed']}}</span>
            </div>

            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Proposed Events</div>

        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 16-->
</div>

<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
    <!--begin::Stats Widget 16-->
    <a href="" class="card card-custom card-stretch">
        <!--begin::Body-->
        <div class="card-body">
            <div class="font-weight-bold ">
                <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">{{$event_counts['archived']}}</span>
            </div>

            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Archived Events</div>

        </div>
        <!--end::Body-->
    </a>
    <!--end::Stats Widget 16-->
</div>
</div>
<!--end::Dashboard-->

<div class="row __ledBlkWrp mb-5">
    @foreach ($events as $row)

        <div class="col-lg-3 col-md-6 col-sm-12 __ledBlk">
            <div class="wd100 __ledBlkInr">
                <div class="__ledBlkImg">
                    <a href="{{route('vendor.event',$row->id)}}"><img class="img-fluid" src="{{asset('img/young-muslim-bride-groom-wedding-photos.png')}}"></a>
                </div>
                <div class="__ledBlkDicp wd100">
                    <div class="__noticotr">
                        <div class="__yolboll" data-toggle="tooltip" data-theme="dark" title="Proposals">{{$row->counts()['proposals']}}</div>
                        <div class="__reddboll" data-toggle="tooltip" data-theme="dark" title="Invoices">{{$row->counts()['invoices']}}</div>
                    </div>
                    <h4 class="text-truncate"><a href="{{route('vendor.event',$row->id)}}">{{$row['event_name']}}</a></h4>
                    <h5><a href="{{route('vendor.event',$row->id)}}">{{date('d M Y', strtotime($row['event_date']))}}</a></h5>
                    <h6 class="text-truncate"><a href="{{route('vendor.event',$row->id)}}">{{$row['event_location']}}</a></h6>
                </div>

            </div>
        </div>
    @endforeach
</div>
@role('event_planner' && Auth::user()->parent_id == null)
<div class="wd100 mb-5">
    <a href="{{route('vendor.create-event')}}"><button type="button" class="btn btn-primary float-right __create_eventBtn">Create a Event</button></a>
</div>
@endrole
<!--end::Container-->
</div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
@endsection
