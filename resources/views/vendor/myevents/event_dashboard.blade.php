{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container-fluid">
            <!--begin::Dashboard-->
            <div class="wd100 __topSwitch">
                <div class="d-flex float-right">
                    <div class="dropdown __topSwitchBtn">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Switch Event
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wd100 __subbnrWrp d-flex">
                <div class="__subbnrTxpt">
                    <h2>{{ $event->event_name }}</h2>
                    <h3>{{ date('d M Y', strtotime($event->event_date)) }}</h3>
                    <h4>{{ $event->event_location }}</h4>
                    <h5>05.00 PM</h5>
                </div>
                <div class="__subbnrImgpt" style="background: url({{ asset('img/naina_events_bnr.jpg') }}) center">
                </div>
            </div>
            <div class="wd100 __tpExpBtnWrp">
                <div class="d-flex float-right">
                    <button type="button" class="btn btn-primary">Edit Event</button>
                    <button type="button" class="btn btn-secondary">Export Event</button>
                    <button type="button" class="btn btn-secondary">Duplicate Event</button>
                </div>
            </div>



            <div class="row __sublk">
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                    <!--begin::Stats Widget 16-->
                    <div href="#" class="card card-custom card-stretch ">
                        <!--begin::Body-->
                        <div class="card-body">

                            <div class="font-weight-bold">
                                <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">06</span>
                            </div>

                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2">Key Activities</div>

                            <div class="wd100 __bozflddrp">
                                <select class="form-control form-control-sm">
                                    <option>Last 90 Days</option>
                                    <option>Last 60 Days</option>
                                    <option>Last 30 Days</option>
                                </select>
                            </div>

                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 16-->
                </div>


                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                    <!--begin::Stats Widget 16-->
                    <div href="#" class="card card-custom card-stretch ">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">12</span>
                            </div>

                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Overdue</div>
                            <div class="wd100 __bozflddrp">
                                <select class="form-control form-control-sm">
                                    <option>Last 90 Days</option>
                                    <option>Last 60 Days</option>
                                    <option>Last 30 Days</option>
                                </select>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 16-->
                </div>

               <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                    <!--begin::Stats Widget 16-->
                    <div href="#" class="card card-custom card-stretch ">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">10</span>
                            </div>

                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Upcoming</div>

                            <div class="wd100 __bozflddrp">
                                <select class="form-control form-control-sm">
                                    <option>Last 90 Days</option>
                                    <option>Last 60 Days</option>
                                    <option>Last 30 Days</option>
                                </select>
                            </div>

                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 16-->
                </div>

                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                    <!--begin::Stats Widget 16-->
                    <div class="card card-custom card-stretch ">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="font-weight-bold ">
                                <span class="__textCotr font-size-h1 font-weight-bolder mr-2 __open_sans">20</span>
                            </div>

                            <div class="__textLebs font-weight-bolder font-size-h5 mb-2 ">Waiting</div>

                            <div class="wd100 __bozflddrp">
                                <select class="form-control form-control-sm">
                                    <option>Last 90 Days</option>
                                    <option>Last 60 Days</option>
                                    <option>Last 30 Days</option>
                                </select>
                            </div>

                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 16-->
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-lg-8 col-md-7 col-sm-12">
                    <div class="row __srvBzWrp">
                        @if (in_array('view-calendar', $permissions))
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <a href="/calender/{{ $event->id }}" class="wd100 __srvBzbk">
                                    <div class="__sbkIcon">
                                        <i class="flaticon-calendar"></i>
                                    </div>

                                    <h5>Calender</h5>
                                </a>
                            </div>
                        @endif
                        @if (in_array('view-budgeter', $permissions))
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <a href="#" class="wd100 __srvBzbk">
                                    <div class="__sbkIcon">
                                        <i class="flaticon-budget"></i>
                                    </div>
                                    <h5>Budgeter</h5>
                                </a>
                            </div>
                        @endif
                        @if (in_array('view-contracts', $permissions))
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <a href="{{ url('vendor/event/' . $event->id . '/contracts') }}" class="wd100 __srvBzbk">
                                    <div class="__evtPlnCount">{{ $event->counts()['contracts'] }}</div>
                                    <div class="__sbkIcon">
                                        <i class="flaticon-contract"></i>
                                    </div>
                                    <h5>Contracts</h5>
                                </a>
                            </div>
                        @endif
                        @if (in_array('view-checklist', $permissions))
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <a href="/checklist/{{ $event->id }}" class="wd100 __srvBzbk">
                                    <div class="__sbkIcon">
                                        <i class="flaticon-clipboard"></i>
                                    </div>
                                    <h5>Checklist</h5>
                                </a>
                            </div>
                        @endif
                        <!--	@if (in_array('view-leads', $permissions))-->
                        <!--               <div class="col-lg-4 col-md-4 col-sm-12">-->
                        <!--	<a href="#" class="wd100 __srvBzbk">-->
                        <!--	 	<div class="__sbkIcon">-->
                        <!--		   <i class="flaticon-contract-1"></i>-->
                        <!--		</div>-->
                        <!--	 	 <h5>Leads</h5>-->
                        <!--	</a>-->
                        <!--</div>-->
                        @endif
                        @if (in_array('view-proposals', $permissions))
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <a href="{{ url('vendor/event/' . $event->id . '/proposals?status=Send/Recieved') }}"
                                    class="wd100 __srvBzbk">
                                    <div class="__evtPlnCount">{{ $event->counts()['proposals'] }}</div>
                                    <div class="__sbkIcon">
                                        <i class="flaticon-receipt"></i>
                                    </div>
                                    <h5>Proposals</h5>
                                </a>
                            </div>
                        @endif
                        @if (in_array('view-create-a-web_page', $permissions) && Auth::user()->roles[0]->name == 'event_planner')
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <a href="/create-web-page/{{ $event->id }}" class="wd100 __srvBzbk">
                                    <div class="__sbkIcon">
                                        <i class="flaticon-networking"></i>
                                    </div>
                                    <h5>Create a Web Page</h5>
                                </a>
                            </div>
                        @endif
                        @if (in_array('view-floor-plans', $permissions) && Auth::user()->roles[0]->name == 'event_planner')
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <a href="#" class="wd100 __srvBzbk">
                                    <div class="__sbkIcon">
                                        <i class="flaticon-blueprint"></i>
                                    </div>
                                    <h5>Floor Plans</h5>
                                </a>
                            </div>
                        @endif
                        @if (in_array('view-invoices', $permissions))
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <a href="{{ url('vendor/event/' . $event->id . '/invoices') }}" class="wd100 __srvBzbk">
                                    <div class="__evtPlnCount">{{ $event->counts()['invoices'] }}</div>
                                    <div class="__sbkIcon">
                                        <i class="flaticon-report"></i>
                                    </div>
                                    <h5>Invoices</h5>
                                </a>
                            </div>
                        @endif
                        @if (in_array('view-conversations', $permissions))
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <a href="/conversations" target="_blank" class="wd100 __srvBzbk">
                                    <div class="__sbkIcon">
                                        <i class="flaticon-chat"></i>
                                    </div>
                                    <h5>Conversations</h5>
                                </a>
                            </div>
                        @endif
                        @if (in_array('view-guest-list', $permissions) && Auth::user()->roles[0]->name == 'event_planner')
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <a href="/guest-list/{{ $event->id }}" class="wd100 __srvBzbk">
                                    <div class="__sbkIcon">
                                        <i class="flaticon-guest-list"></i>
                                    </div>
                                    <h5>Guest List</h5>
                                </a>
                            </div>
                        @endif
                        {{-- @if (in_array('view-form-builder', $permissions))
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <a href="#" class="wd100 __srvBzbk">
                                    <div class="__sbkIcon">
                                        <i class="flaticon-contact-form"></i>
                                    </div>
                                    <h5>Form Builder</h5>
                                </a>
                            </div>
                        @endif --}}
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 ">

                    <div class="card card-custom __enrtTabwgt">
                        <div class="card-header">

                            <div class="card-toolbar wd100">
                                <ul class="nav nav-bold nav-pills  wd100">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_7_2">Event
                                            Planner</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_7_3">Venue</a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">

                                <div class="tab-pane fade show active" id="kt_tab_pane_7_2" role="tabpanel"
                                    aria-labelledby="kt_tab_pane_7_2">

                                    <div class="wd100 __tbRultEvent_planner">

                                        @foreach ($event->eventPlannerRFQ as $vendors)
                                            <div class="d-flex align-items-center __rtUrclibz">
                                                <div class="symbol symbol-50 symbol-light mr-5 __taurpic">
                                                    <img src="@if ($vendors->event_planner['profile_picture'] == null){{ asset('media/users/100_8.jpg') }} @else {{ asset('/storage/' . $vendors->event_planner['profile_picture'] . '') }} @endif" class="h-75 align-self-end" alt="">
                                                </div>
                                                <div>
                                                    <a href="#"
                                                        class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font_size_18">{{ $vendors->event_planner['first_name'] }}</a>
                                                    <a href="#"
                                                        class="text-muted font-weight-bold d-block">{{ $vendors->event_planner->bussinessDetails['emirates'] }}</a>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>




                                </div>
                                <div class="tab-pane fade" id="kt_tab_pane_7_3" role="tabpanel"
                                    aria-labelledby="kt_tab_pane_7_3">
                                    <div class="wd100 __tbRultVenue">

                                        @foreach ($event->eventPlannerRFQ as $vendors)
                                            @if ($vendors->category_id == 16)
                                                <div class="d-flex align-items-center __rtUrclibz">
                                                    <div class="symbol symbol-50 symbol-light mr-5 __taurpic">
                                                        <img src="@if ($vendors->vendor['profile_picture'] == null){{ asset('media/users/100_8.jpg') }} @else {{ asset('/storage/' . $vendors->vendor['profile_picture'] . '') }} @endif" class="h-75 align-self-end"
                                                            alt="">
                                                    </div>
                                                    <div>
                                                        <a href="/conversations" target="_blank"
                                                            class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font_size_18">{{ $vendors->vendor['first_name'] }}</a>
                                                        <a href="/conversations" target="_blank"
                                                            class="text-muted font-weight-bold d-block">{{ $vendors->vendor->bussinessDetails['emirates'] }}</a>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>

                </div>




            </div>
            <!--end::Container-->
        </div>
    @endsection

    {{-- Scripts Section --}}
    @section('scripts')

    @endsection
