<!-- Modal-->
<div class="modal fade" id="addNewItemModel" role="model" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="new-item-form">
                    @csrf
                    <div class="wd100 __proslAddWrp __addscpWrkPg">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="form-label">Service Type</label>
                                <select class="form-control" name="item_service_type" id="item_service_type">
                                    @if (isset($proposal))
                                        @foreach ($requests as $item)
                                            @if ($item->id == $proposal->request_id)
                                                <option value="{{ $item->category['id'] }}">
                                                    {{ $item->category['name'] }}</option>
                                            @endif
                                        @endforeach
                                    @else
                                        <option value=""></option>
                                    @endif
                                </select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group">
                                <label class="form-label">Sub Service</label>
                                <select class="form-control" name="item_sub_service" id="item_sub_service">
                                    @if (isset($proposal))
                                        <option value>Select Sub Service</option>
                                        @foreach ($proposal->request->category->childs as $item)
                                            <option value="{{ $item->id }}">
                                                {{ $item->name }}</option>
                                        @endforeach
                                    @else
                                        <option value=""></option>
                                    @endif
                                </select>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-4 __ctndrofid">
                                <label class="form-label wd100">Item Name</label>
                                <select class="form-control select2" name="item_name" id="item_name">
                                    @if (isset($proposal))
                                        @foreach ($proposal->request->category->items as $item)
                                            <option value="{{ $item->name }}">{{ $item['name'] }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                <label class="form-label">Description</label>
                                <textarea class="form-control" rows="3" placeholder="Description"
                                    name="item_description" id="item_description"></textarea>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                <label class="form-label">Amount</label>
                                <input type="text" class="form-control numeric" placeholder="Amount" name="item_amount"
                                    id="item_amount">
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                <label class="form-label">Tax</label>
                                <select class="form-control" name="item_tax" id="item_tax">
                                    <option value>Select Tax</option>
                                    @foreach ($taxes as $tax)
                                        <option value="{{ $tax->value }}" @if (Auth::user()->bussinessDetails['default_tax'] == $tax->id) selected @endif>{{ $tax->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                <label class="form-label qty_label_text">@if (isset($proposal)) {{ $proposal->request->category['qty_label'] ?: 'Quantity' }} @else Quantity @endif</label>
                                <input type="text" class="form-control numeric" placeholder="@if (isset($proposal)) {{ $proposal->request->category['qty_label'] ?: 'Quantity' }} @else Quantity @endif"
                                    name="item_quantity" id="item_quantity">
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                <label class="form-label">Attach a File</label>
                                {{-- <div class="custom-file">
                                 <input type="file" class="custom-file-input" name="item_file" id="item_file"/>
                                 <label class="custom-file-label" for="customFile">Choose file</label>
                                </div> --}}
                                <ul id="media-list" class="clearfix item_image_panel">
                                    <li class="myupload">
                                        <span><i class="fa fa-plus" aria-hidden="true"></i>
                                            <input type="file" click-type="single" id="item_file" name="item_file"
                                                class="picupload" data-type="item_file">
                                        </span>
                                    </li>
                                </ul>
                            </div>

                            {{-- <div class="wd100 form-group text-right">
                                <button class="btn btn-secondary mr-3 " type="button">  Add Sub item for cost breakdown </button>
                            </div> --}}


                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group mt-7">
                                <label class="form-label">Remarks</label>
                                <textarea class="form-control" id="item_remarks" rows="3" placeholder="Remarks"
                                    name="item_remarks"></textarea>
                            </div>
                            <input type="hidden" id="item_editable">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                @if ($page_title != 'View Proposal')
                    <button type="button" id="btnAddItem" class="btn btn-primary font-weight-bold">Add Item</button>
                @endif
            </div>
        </div>
    </div>
</div>
