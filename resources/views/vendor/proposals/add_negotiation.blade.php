<!-- Modal-->
<div class="modal fade" id="addNegModel" role="model" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Negotiation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="new-neg-form">
                    @csrf
                    <div class="wd100 __proslAddWrp __addscpWrkPg">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4">
                                <label class="form-label">Item Name</label>
                                <input type="text" class="form-control" placeholder="Item Name" name="neg_name" id="neg_name" readonly>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-group">
                                <label class="form-label">Amount</label>
                                <input type="text" class="form-control numeric" placeholder="Amount" name="neg_amount" id="neg_amount">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                <label class="form-label">Note</label>
                                <textarea class="form-control" rows="3" placeholder="Note" name="neg_note" id="neg_note"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                @if($editable == 0)
                <button type="button" id="btnAddNeg" class="btn btn-primary font-weight-bold">Update Item</button>
                @endif
            </div>
        </div>
    </div>
</div>
