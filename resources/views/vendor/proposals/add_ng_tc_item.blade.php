<!-- Modal-->
<div class="modal fade" id="addNGTCItemModel" role="model" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Terms & Condition</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="new-neg-form">
                    @csrf
                    <div class="wd100 __proslAddWrp __addscpWrkPg">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                <label class="form-label">Terms & Condition</label>
                                <textarea class="form-control" rows="3" name="neg_terms_and_condition"
                                    id="neg_terms_and_condition"></textarea>
                            </div>
                            <input type="hidden" id="current_row">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="button" id="btnAddNGTCItem" class="btn btn-primary font-weight-bold">Add Negotiation</button>
            </div>
        </div>
    </div>
</div>
