{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
<li class="breadcrumb-item text-muted">
    <a href="{{url(''.$url.'/my-events/')}}" class="text-muted">My Events</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{url(''.$url.'/event/'.$event->id.'')}}" class="text-muted">{{$event->event_name}}</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{url(''.$url.'/event/'.$event->id.'/proposals')}}" class="text-muted">Proposals</a>
</li>
<li class="breadcrumb-item text-muted">
    <a href="{{url(''.$url.'/event/'.$event->id.'/proposals/create')}}" class="text-muted">{{$page_title}}</a>
</li>
@endsection
{{-- Content --}}
@section('content')
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container-fluid">
            <form id="proposals-form">
                @csrf
                <!--begin::Dashboard-->
                <div class="wd100 __proslAddWrp ">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">RFQ</label>
                            <select class="form-control select2" id="request_select" name="request_id">
                                <option value></option>
                                @foreach ($requests as $request)
                                    <option value="{{ $request['id'] }}"
                                        data-category_id="{{ $request->category['id'] }}"
                                        data-category_qty="{{ $request->category['qty_label'] }}"
                                        data-vendor_id="{{ $request->event_planner['id'] }}"
                                        data-vendor_name="{{ $request->event_planner['first_name'] }} {{ $request->event_planner['last_name'] }}"
                                        data-items="{{ $request->category['items'] }}"> {{ $request->category['name'] }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Event Name</label>
                            <input type="text" class="form-control" placeholder="Wedding"
                                value="{{ $event->event_name }}">
                            <input type="hidden" name="event_id" value="{{ $event->id }}">
                        </div>

                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Name of Party 1</label>
                            <input type="text" class="form-control" placeholder="Name of Party"
                                value="{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}">
                            <input type="hidden" name="party1_id" value="{{ Auth::user()->id }}">
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Name of Party 2</label>
                            <input type="text" class="form-control" placeholder="Name of Party" value=""
                                id="name_of_party2">
                            <input type="hidden" name="party2_id" value="" id="name_of_party2_id">
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Date</label>
                            <div class="input-group date" id="kt_datetimepicker_3" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" placeholder="Select date "
                                    data-target="#kt_datetimepicker_3" name="date" />
                                <div class="input-group-append" data-target="#kt_datetimepicker_3"
                                    data-toggle="datetimepicker">
                                    <span class="input-group-text">
                                        <i class="ki ki-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Status</label>
                            <select class="form-control" id="status" name="status">
                                <option value="Send/Recieved" selected>Send/Recieved</option>
                                <option value="Pending">Pending</option>
                                <option value="Declined">Declined</option>
                                <option value="On Hold">On Hold</option>
                                <option value="Accepted">Accepted</option>
                                <option value="Draft">Draft</option>
                            </select>
                        </div> --}}
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label"> </label>
                            <div class="checkbox-inline checkbox-lg">
                                <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-lg checkbox-primary">
                                    <input type="checkbox" name="approved_digital_written_signature" value="1" />
                                    <span></span>
                                    Approved digital(e-sign) or written signature
                                </label>
                            </div>

                        </div>
                        <div class="col-lg- col-md-2 col-sm-12 col-xs-12 mb-4 __cutmSwitch">
                            <label class="form-label">Written&nbsp;|&nbsp;Digital</label>
                            <span class="switch switch-primary">
                                <label>
                                    <input type="checkbox" checked="checked" name="type" value="Digital"
                                        id="writen-digital-sign" />
                                    <span></span>
                                </label>
                            </span>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Signature</label>
                            <ul id="media-list" class="clearfix">
                                <li class="myupload">
                                    <span><i class="fa fa-plus" aria-hidden="true"></i>
                                        <input type="file" click-type="single" id="picupload" name="signature_1"
                                            class="picupload">
                                    </span>
                                </li>
                            </ul>
                            <input type="hidden" class="form-control esign" name="signature_1_esign" id="signature_1_esign"
                                value="">
                        </div>

                        {{-- <div class="col-lg-2 col-md-6 col-sm-12 col-xs-12 mb-4">
            <label class="form-label">Party 2 Signature</label>
            <ul id="media-list" class="clearfix">
                <li class="myupload">
                    <span><i class="fa fa-plus" aria-hidden="true"></i>
                        <input type="file" click-type="single" id="picupload" name="signature_2" class="picupload">
                    </span>
                </li>
            </ul>
            <input type="hidden" class="form-control esign" name="signature_2_esign" id="signature_2_esign" value="">
        </div> --}}

                    </div>
                </div>

                <div class="__scopeWorkWrp wd100">
                    <div
                        class="__scopeWorkHdprt d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                        <div class="d-flex __proslAddLtPrt">
                            <h4 class="__fontWtbld mt-2 mb-2 mr-5 text-primary">Scope Of Work</h4>
                        </div>
                        <div class="__proslAddRtPrt">
                            <button class="btn btn-primary btn-outline-primary addNewItemBtn" type="button"
                                data-toggle="modal" data-target="#addNewItemModel"> Add Item </button>
                        </div>
                    </div>
                    <div class="__scopeWorkTbvw wd100 ">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col" class="text-center">Sl.No</th>
                                    <th scope="col" class="text-center">Name</th>
                                    <th scope="col" class="text-center">Service Type</th>
                                    <th scope="col" class="text-center">Sub Service</th>
                                    <th scope="col" class="text-center">Description</th>
                                    <th scope="col" class="text-right">Amount</th>
                                    <th scope="col" class="text-center qty_label_text">Quantity</th>
                                    <th scope="col" class="text-center">Tax</th>
                                    <th scope="col" class="text-right">Total</th>
                                    <th class="text-center" scope="col">Upload</th>
                                    <th class="text-center" scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody id="itemsTableBody">

                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="8" align="right" valign="middle" class="__tbbodr">Subtotal</td>
                                    <td colspan="1" align="right" class="text-right __tbbodr" id="sub_total">0.00</td>
                                    <td class="__tbbodr"></td>
                                </tr>
                                <tr>
                                    <td colspan="8" align="right" valign="middle" class="__tbbodr">Tax</td>
                                    <td colspan="1" align="right" class="text-right __tbbodr" id="tax_total">0.00</td>
                                    <td class="__tbbodr"></td>
                                </tr>
                                <tr>
                                    <td colspan="8" align="right" valign="middle" class="__tbbodr font-weight-bolder">
                                        <strong>Total </strong>
                                    </td>
                                    <td colspan="1" align="right" class="text-right __tbbodr font-weight-bolder" id="total">
                                        0.00</td>
                                    <td class="__tbbodr"></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="__terAgntWrp  wd100">
                    <div
                        class="__scopeWorkHdprt d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                        <div class="d-flex __proslAddLtPrt">
                            <h4 class="__fontWtbld mt-2 mb-2 mr-5 text-primary">Terms And Conditions</h4>
                        </div>
                        <div class="__proslAddRtPrt">
                            <button class="btn btn-primary btnTCAddNew" type="button"> Add New</button>
                            <button class="btn btn-secondary btnApplyDflt" type="button" data-type="Proposal"> Select Default</button>
                        </div>
                    </div>
                    <div class="__terAgntWrpBz">
                        <ol id="tc_list">

                        </ol>
                    </div>
                    <div class="wd100 text-right mt-6">
                        <button type="button" id="btnDraftSubmit"
                            class="btn btn-secondary pl-15 pr-15 pt-5 pb-5 font-weight-bolder">Save to Draft</button>
                        <button type="button" id="btnSubmit"
                            class="btn btn-primary pl-15 pr-15 pt-5 pb-5 font-weight-bolder">Send Proposal</button>
                    </div>
                </div>
            </form>
            <!--end::Container-->
        </div>
        @include('vendor.proposals.default_tc')
        @include('vendor.proposals.add_tc_item')
        @include('vendor.proposals.add_item')
        @include('vendor.proposals.e_sign_canvas')
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        localStorage.setItem("items", JSON.stringify([]));
        localStorage.setItem("tc_items", JSON.stringify([]));
        $('#kt_datetimepicker_3').datetimepicker({
            defaultDate: new Date(),
            format: 'MM/D/yyyy'
        });
        $('#request_select').select2({
            placeholder: "Select a Request",
        });
        $('#item_name').select2({
            tags: true,
            placeholder: "Select Item"
        });
        $('.summernote_terms_and_conditions').summernote({
            height: 200,
        });

        $('#request_select').on('change', function() {
            var category_id = $(this).find(':selected').data('category_id');
            getSubService(category_id);
            var category_text = $(this).find(':selected').text();
            var category_items = $(this).find(':selected').data('items');

            $('#name_of_party2').val($(this).find(':selected').data('vendor_name'));
            $('#name_of_party2_id').val($(this).find(':selected').data('vendor_id'));
            if($(this).find(':selected').data('category_qty'))
            {
                $('.qty_label_text').html($(this).find(':selected').data('category_qty'));
                $('#item_quantity').attr('placeholder',$(this).find(':selected').data('category_qty'));

            }
            $('#item_service_type').empty();
            $('#item_service_type').append('<option value="' + category_id + '" selected>' + category_text +
                '</option>');

            $('#item_name').empty();
            $('#item_name').append('<option value>Select Item</option>');
            category_items.forEach(element => {
                $('#item_name').append('<option value="' + element.name + '">' + element.name +
                    '</option>');
            });

        });
        $('.showCreateItemModel').on('click', function() {
            $('#createNewItemModel').modal('show');
        });


        $("#btnNewItemSave").click(function(event) {
            event.preventDefault();
            var category_id = $('#item_service_type').val();
            var form = $('#create-item-form')[0];
            var data = new FormData(form);
            data.append('category_id', category_id);
            $("#btnNewItemSave").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/vendor_create_item",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    $("#btnNewItemSave").prop("disabled", false);
                    $('#item_name').append('<option value=' + data.name + '>' + data.name +
                        '</option>');
                    $('#item_name').val(data.name).trigger('change');
                    $('#createNewItemModel').modal('hide');
                },
                error: function(e) {
                    $("#btnNewItemSave").prop("disabled", false);
                }
            });
        });
        $("#btnSubmit").click(function(event) {
            event.preventDefault();
            var items = JSON.parse(localStorage["items"]);
            var tc_items = localStorage["tc_items"];
            if (jsFieldsValidator(['request_id'])) {
                if (items.length > 0) {
                    var form = $('#proposals-form')[0];
                    var data = new FormData(form);
                    var terms_of_agreement = $('.summernote_terms_and_conditions').summernote('code');
                    data.append('items', JSON.stringify(items));
                    data.append('terms_of_agreement', tc_items);
                    $("#btnSubmit").prop("disabled", true);
                    $.ajax({
                        type: "POST",
                        enctype: 'multipart/form-data',
                        url: "/vendor/event/{{ $event->id }}/proposals",
                        data: data,
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function(data) {
                            $("#btnSubmit").prop("disabled", false);
                            setTimeout(function() {
                                location.replace(
                                    '/vendor/event/{{ $event->id }}');
                            }, 1000);
                        },
                        error: function(e) {
                            $("#btnSubmit").prop("disabled", false);
                        }
                    });
                } else {
                    toastr.error('Add items to proposal.', 'Error');
                }
            }
        });
        $("#btnDraftSubmit").click(function(event) {
            event.preventDefault();
            var items = JSON.parse(localStorage["items"]);
            var tc_items = localStorage["tc_items"];
            if (jsFieldsValidator(['request_id'])) {
                if (items.length > 0) {
                    var form = $('#proposals-form')[0];
                    var data = new FormData(form);
                    var terms_of_agreement = $('.summernote_terms_and_conditions').summernote('code');
                    data.append('items', JSON.stringify(items));
                    data.append('terms_of_agreement', JSON.stringify(tc_items));
                    data.append('draft', true);
                    $("#btnSubmit").prop("disabled", true);
                    $.ajax({
                        type: "POST",
                        enctype: 'multipart/form-data',
                        url: "/vendor/event/{{ $event->id }}/proposals",
                        data: data,
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function(data) {
                            $("#btnSubmit").prop("disabled", false);
                            setTimeout(function() {
                                location.replace(
                                    '/vendor/event/{{ $event->id }}');
                            }, 1000);
                        },
                        error: function(e) {
                            $("#btnSubmit").prop("disabled", false);
                        }
                    });
                } else {
                    toastr.error('Add items to proposal.', 'Error');
                }
            }
        });
        var parent_tag = '';
        $('.picupload').on('click', function(e) {
            if ($(this).data('type') == 'item_file') {

            } else {
                if ($('#writen-digital-sign:checkbox:checked').length > 0) {
                    e.preventDefault();
                    var canvas = document.getElementById("sig-canvas");
                    canvas.width = canvas.width;
                    $('#eSignModel').modal('show');
                    parent_tag = $(this).parent().parent();
                }
            }
        });

        function clearFields() {
            $('#item_name').val('');
            $('textarea#item_description').val('');
            $('#item_quantity').val('');
            $('#item_amount').val('');
            $('textarea#item_remarks').val('');
            $('#item_editable').val(null);
            var image =
                `<li class="myupload"><span><i class="fa fa-plus" aria-hidden="true"></i><input type="file" click-type="single" id="item_file" name="item_file" class="picupload" data-type="item_file"></span></li>`;
            $('.item_image_panel').html(image);
        }

        $('#btnAddItem').on('click', function() {
            if ($('#item_name').find(':selected').data('select2-tag')) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: "/vendor_create_item",
                    data: {
                        'name': $('#item_name').val(),
                        'category_id': $('#item_service_type').val()
                    },
                    success: function(data) {},
                    error: function(e) {}
                });
            }
            var items = JSON.parse(localStorage["items"]);
            if (jsFieldsValidator(['item_name', 'item_amount', 'item_tax', 'item_quantity'])) {
                var item_name = $('#item_name').val();
                var description = $('textarea#item_description').val()
                var service_type = $('#item_service_type').text();
                var service_type_id = $('#item_service_type').val();
                var sub_service = $('#item_sub_service').find(':selected').val()?$('#item_sub_service').find(':selected').text():'';
                var sub_service_id = $('#item_sub_service').find(':selected').val();
                var qty = parseFloat($('#item_quantity').val());
                var amount = parseFloat($('#item_amount').val());
                var tax = parseFloat($('#item_tax').val());
                var remark = $('#item_remarks').val();
                var sub_total = qty * amount;
                var tax_amount = (sub_total / 100) * tax;
                var total = sub_total + tax_amount;
                var image_url = '';
                if ($('#item_old_image').val()) {
                    image_url = $('#item_old_image').val();
                } else {
                    var files = $('#item_file')[0].files;
                    if (files.length > 0) {
                        image_url = uploadImage(files);
                        $('.item_image_panel').find('.myupload').show();
                        $('.item_image_panel')[0].children[0].remove();
                    } else {
                        image_url = null;
                    }
                }
                var item = {
                    item_name: item_name,
                    description: description,
                    service_type: service_type,
                    service_type_id: service_type_id,
                    sub_service : sub_service,
                    sub_service_id : sub_service_id,
                    qty: qty,
                    amount: amount,
                    tax: tax,
                    remark: remark,
                    sub_total: sub_total,
                    tax_amount: tax_amount,
                    total: total,
                    image_url: image_url
                };
                if (items.length > 0) {
                    var exist = 0;
                    items.forEach(element => {
                        if (element.item_name == item_name) {
                            exist = 1;
                            element.setvice_type = service_type;
                            element.service_type_id = service_type_id;
                            element.sub_service = sub_service;
                            element.sub_service_id = sub_service_id;
                            element.qty = qty;
                            element.amount = amount;
                            element.tax = tax;
                            element.remark = remark;
                            element.sub_total = sub_total;
                            element.tax_amount = tax_amount;
                            element.total = total;
                            element.image_url = image_url
                        }
                    });
                    if (exist == 0) {
                        items.push(item);
                    }
                } else {
                    items.push(item);
                }
                localStorage["items"] = JSON.stringify(items);

                console.log(JSON.parse(localStorage["items"]));
                updateTableRows(items);
                $('#addNewItemModel').modal('hide');
            }

            // if ($('#item_name').find(':selected').data('select2-tag')) {
            //     $.ajaxSetup({
            //         headers: {
            //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //         }
            //     });
            //     $.ajax({
            //         type: "POST",
            //         enctype: 'multipart/form-data',
            //         url: "/vendor_create_item",
            //         data: {
            //             'name': $('#item_name').val(),
            //             'category_id': $('#item_service_type').val()
            //         },
            //         success: function(data) {},
            //         error: function(e) {}
            //     });
            // }
            // if (jsFieldsValidator(['item_name', 'item_amount', 'item_tax', 'item_quantity'])) {
            //     if (editable_row != null) {
            //         var row = editable_row;
            //         var qty = parseFloat($('#item_quantity').val());
            //         var amount = parseFloat($('#item_amount').val());
            //         var tax = parseFloat($('#item_tax').val());
            //         var sub_total = qty * amount;
            //         var tax_amount = (sub_total / 100) * tax;

            //         row.find("td:eq(1)").text($('#item_name').val());
            //         row.find("td:eq(3)").text($('textarea#item_description').val());
            //         row.find("td:eq(4)").text(amount.toFixed(2));
            //         row.find("td:eq(5)").text($('#item_quantity').val());
            //         row.find("td:eq(6)").text($('#item_tax').val() + '%');
            //         row.find("td:eq(7)").text((sub_total + tax_amount).toFixed(2));
            //         row.find("td:eq(8)").text($('#item_remarks').val());
            //         if ($('#item_old_image').val()) {} else {
            //             var files = $('#item_file')[0].files;
            //             if (files.length > 0) {
            //                 uploadImage(files, true); // file,editable
            //             } else {
            //                 row.find("td:eq(10)").empty();
            //             }
            //         }
            //         $('#addNewItemModel').modal('hide');
            //         setIndex();
            //     } else {
            //         var files = $('#item_file')[0].files;
            //         if (files.length > 0) {
            //             var image_url = uploadImage(files);
            //             $('.item_image_panel').find('.myupload').show();
            //             $('.item_image_panel')[0].children[0].remove();
            //         } else {
            //             updateRow();
            //         }
            //     }
            // }
        });

        function updateTableRows(items) {
            $('#itemsTableBody').empty();
            var sub_total = 0;
            var tax_total = 0;
            var total = 0;
            items.forEach((element, key) => {
                var row = '<tr data-row="' + key + '">';
                row += '<td class="text-center td-index">1</td>';
                row += '<td class="text-center">' + element.item_name + '</td>';
                row += '<td class="text-center">' + element.service_type + '</td>';
                row += '<td class="text-center">' + element.sub_service + '</td>';
                row += '<td class="text-center">' + element.description + '</td>';
                row += '<td class="text-right">' + element.amount.toFixed(2) + '</td>';
                row += '<td class="text-center">' + element.qty + '</td>';
                row += '<td class="text-right">' + element.tax + '%</td>';
                row += '<td class="text-right">' + element.total.toFixed(2) + '</td>';
                if (element.image_url) {
                    row += '<td class="text-center"><input type="hidden" value="' + element.image_url +
                        '" class="image_url"><div class="__scpTbImg"><img class="img-fluid item_image" src="/storage/' +
                        element.image_url + '" ></div></td>';
                } else {
                    row += '<td></td>';
                }
                row +=
                    '<td class="text-right"><a href="javascript:void(0)" title="Edit" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary editItemBtn"><i class="far fa-edit"></i></i></a><a href="javascript:void(0);" title="Delete" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary removeItemBtn"><i class="fas fa-trash-alt"></i></a></td></tr>';

                $('#itemsTableBody').append(row);
                editRow();
                sub_total += element.sub_total;
                tax_total += element.tax_amount;
                total += element.total;

                $('#sub_total').text(sub_total.toFixed(2));
                $('#tax_total').text(tax_total.toFixed(2));
                $('#total').text(total.toFixed(2));

            });
        }

        $('.addNewItemBtn').on('click', function() {
            clearFields();
        });

        function uploadImage(files, editable = null) {
            var image_url;
            var fd = new FormData();
            fd.append('file', files[0]);
            fd.append('path', 'vendor/proposals/{{ $event->id }}/items');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/upload-file",
                data: fd,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    image_url = data;
                    // updateRow(image_url, editable);
                },
                error: function(e) {

                }
            });

            return image_url;
        }

        // function updateRow(image_url = null, editable = null) {
        //     if (editable) {
        //         editable_row.find("td:eq(9)").html('<input type="hidden" value="' + image_url +
        //             '" class="image_url"><div class="__scpTbImg"><img class="img-fluid item_image" src="/storage/' +
        //             image_url + '" ></div>');
        //     } else {
        //         var qty = parseFloat($('#item_quantity').val());
        //         var amount = parseFloat($('#item_amount').val());
        //         var tax = parseFloat($('#item_tax').val());
        //         var sub_total = qty * amount;
        //         var tax_amount = (sub_total / 100) * tax;
        //         var row = '<tr';
        //         row += '<td class="text-center td-index">1</td>';
        //         row += '<td class="text-center">' + $('#item_name').val() + '</td>';
        //         row += '<td class="text-center">' + $('#item_service_type').text() + '</td>';
        //         row += '<td class="text-center">' + $('textarea#item_description').val() + '</td>';
        //         row += '<td class="text-right">' + amount.toFixed(2) + '</td>';
        //         row += '<td class="text-center">' + $('#item_quantity').val() + '</td>';
        //         row += '<td class="text-right">' + $('#item_tax').val() + '%</td>';
        //         row += '<td class="text-right">' + (sub_total + tax_amount).toFixed(2) + '</td>';
        //         row += '<td class="text-right" style="display:none;">' + $('#item_remarks').val() + '</td>';
        //         row += '<td class="text-right" style="display:none;">' + $('#item_service_type').val() + '</td>';
        //         if (image_url) {
        //             row += '<td class="text-center"><input type="hidden" value="' + image_url +
        //                 '" class="image_url"><div class="__scpTbImg"><img class="img-fluid item_image" src="/storage/' +
        //                 image_url + '" ></div></td>';
        //             row +=
        //                 '<td class="text-right"><a href="javascript:void(0)" title="Edit" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary editItemBtn"><i class="far fa-edit"></i></i></a><a href="javascript:void(0);" title="Delete" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary removeItemBtn"><i class="fas fa-trash-alt"></i></a></td></tr>';
        //         } else {
        //             row += '<td></td>';
        //             row +=
        //                 '<td class="text-right"><a href="javascript:void(0)" title="Edit" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary editItemBtn"><i class="far fa-edit"></i></i></a><a href="javascript:void(0);" title="Delete" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary removeItemBtn"><i class="fas fa-trash-alt"></i></a></td></tr>';
        //         }
        //         $('#itemsTableBody').append(row);
        //         editRow();
        //         $('#addNewItemModel').modal('hide');
        //     }
        //     setIndex();
        //     clearFields();
        // }

        // function setIndex() {
        //     var total_amunt = 0;
        //     var tax_amunt = 0;
        //     var sub_total = 0;
        //     $(".td-index").each(function(index) {
        //         $(this).text(++index);
        //         var row = $(this).closest("tr");
        //         var item_sub_total = parseFloat(row.find("td:eq(5)").html()) * parseFloat(row.find("td:eq(4)")
        //             .html());
        //         sub_total += item_sub_total;
        //         tax_amunt += (item_sub_total / 100) * parseFloat((row.find("td:eq(6)").html()).replace('%', ''));
        //         total_amunt += parseFloat(row.find("td:eq(7)").html());
        //     });

        //     $('#sub_total').text(sub_total.toFixed(2));
        //     $('#tax_total').text(tax_amunt.toFixed(2));
        //     $('#total').text(total_amunt.toFixed(2));

        // }

        function editRow() {
            $('.editItemBtn').on('click', function(e) {
                var row = $(this).closest("tr");
                var array_index = row.data('row');
                var item = JSON.parse(localStorage["items"])[array_index];
                $('#item_name').val(item.item_name).trigger('change');
                $('textarea#item_description').val(item.description);
                $('#item_amount').val(item.amount);
                $('#item_tax').val(item.tax);
                $('#item_quantity').val(item.qty);
                $('textarea#item_remarks').val(item.remark);
                // if (item.image_url) {
                //     var image = `<li class="myupload" style="display:none;"><span><i class="fa fa-plus" aria-hidden="true"></i><input type="file" click-type="single" id="item_file" name="item_file" class="picupload" data-type="item_file"></span></li>
            //             <li>
            //             <img src="` + row.find("td:eq(10)").find('.item_image').attr('src') + `" title=""/>
            //             <div class='post-thumb'>
            //                 <div class='inner-post-thumb'>
            //                     <a href='javascript:void(0);' data-id='' class='remove-pic'>
            //                         <i class='fa fa-times' aria-hidden='true'>
            //                         </i>
            //                     </a>
            //                 </div>
            //             </div>
            //             <input type="text" name="old_image" id="item_old_image" value="` + row.find("td:eq(10)")
                //         .find('.item_image').attr('src') + `" style="display: none;">
            //         </li>`;
                //     $('.item_image_panel').html(image);
                // }
                $('#addNewItemModel').modal('show');
            });
            $('.removeItemBtn').on('click', function(e) {
                var row = $(this).closest("tr");
                var array_index = row.data('row');
                var items = JSON.parse(localStorage["items"]);

                Swal.fire({
                    title: "Are you sure?",
                    text: "You wont be able to revert this!",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                }).then(function(result) {
                    if (result.value) {
                        items.splice(array_index, 1);
                        localStorage["items"] = JSON.stringify(items);
                        updateTableRows(items);
                    } else if (result.dismiss === "cancel") {
                        Swal.fire(
                            "Cancelled",
                            "Your imaginary file is safe :)",
                            "error"
                        )
                    }
                });

            });

        }
    </script>
@endsection
