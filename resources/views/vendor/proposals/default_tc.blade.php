<!-- Modal-->
<div class="modal fade" id="defaultTCModel" role="model" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Select Default TC</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {{-- <label>Default Checkboxes</label> --}}
                    <div class="checkbox-list default_tc_list">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="button" id="btnAddDefaultTC" class="btn btn-primary font-weight-bold">Apply Default</button>
            </div>
        </div>
    </div>
</div>
