	<!-- Modal-->
<div class="modal fade" id="eSignModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Digital Signature</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
            <!-- Content -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <canvas id="sig-canvas" width="400" height="160">
                            Get a better browser, bro.
                        </canvas>
                    </div>
                </div>
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="sig-clearBtn" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="button" id="sig-submitBtn" class="btn btn-primary font-weight-bold" data-dismiss="modal">Save</button>
            </div>
        </div>
    </div>
</div>

