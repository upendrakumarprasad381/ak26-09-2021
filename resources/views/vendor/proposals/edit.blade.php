{{-- Extends layout --}}
@extends('layout.default')
@section('sub-header')
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/my-events/') }}" class="text-muted">My Events</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/event/' . $event->id . '') }}" class="text-muted">{{ $event->event_name }}</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/event/' . $event->id . '/proposals') }}" class="text-muted">Proposals</a>
    </li>
    <li class="breadcrumb-item text-muted">
        <a href="{{ url('' . $url . '/event/' . $event->id . '/proposals/' . $proposal->id . '/edit') }}"
            class="text-muted">{{ $page_title }}</a>
    </li>
@endsection
{{-- Content --}}
@section('content')
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container-fluid">
            <form id="proposals-form">
                @csrf
                @method('PATCH')
                <!--begin::Dashboard-->
                <div class="wd100 __proslAddWrp ">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">RFQ</label>
                            <select class="form-control select2" id="request_select" name="request_id">
                                <option value></option>
                                @foreach ($requests as $request)
                                    <option value="{{ $request['id'] }}"
                                        data-category_id="{{ $request->category['id'] }}"
                                        data-category_qty="{{ $request->category['qty_label'] }}"
                                        data-vendor_id="{{ $request->event_planner['id'] }}"
                                        data-vendor_name="{{ $request->event_planner['first_name'] }} {{ $request->event_planner['last_name'] }}"
                                        @if ($proposal->request_id == $request->id)) selected="selected" @endif>{{ $request->category['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Event Name</label>
                            <input type="text" class="form-control" placeholder="Wedding"
                                value="{{ $event->event_name }}" @if ($editable == 0) readonly @endif>
                            <input type="hidden" name="event_id" value="{{ $event->id }}">
                            <input type="hidden" name="request_id" value="{{ $request->id }}">
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Name of Party 1</label>
                            <input type="text" class="form-control" placeholder="Name of Party"
                                value="{{ $proposal->party1['first_name'] }} {{ $proposal->party1['last_name'] }}"
                                @if ($editable == 0) readonly @endif>
                            <input type="hidden" name="party1_id" value="{{ $proposal->party1['id'] }}">
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Name of Party 2</label>
                            <input type="text" class="form-control" placeholder="Name of Party" id="name_of_party2"
                                value="{{ $proposal->party2['first_name'] }} {{ $proposal->party2['last_name'] }}"
                                @if ($editable == 0) readonly @endif>
                            <input type="hidden" name="party2_id" value="{{ $proposal->party2['id'] }}"
                                id="name_of_party2_id">
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Date</label>
                            <div class="input-group date" id="kt_datetimepicker_3" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" placeholder="Select date "
                                    data-target="#kt_datetimepicker_3" name="date" @if ($editable == 0) readonly @endif />
                                <div class="input-group-append" data-target="#kt_datetimepicker_3"
                                    data-toggle="datetimepicker">
                                    <span class="input-group-text">
                                        <i class="ki ki-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Status</label>
                            <select class="form-control" id="status" name="status">
                                <option value="Send/Recieved" @if($proposal->status == 'Send/Recieved') selected @endif>Send/Recieved</option>
                                <option value="Pending" @if($proposal->status == 'Pending') selected @endif>Pending</option>
                                <option value="Declined" @if($proposal->status == 'Declined') selected @endif>Declined</option>
                                <option value="On Hold" @if($proposal->status == 'On Hold') selected @endif>On Hold</option>
                                <option value="Accepted" @if($proposal->status == 'Accepted') selected @endif>Accepted</option>
                                <option value="Draft" @if($proposal->status == 'Draft') selected @endif>Draft</option>
                            </select>
                        </div> --}}
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label"> </label>
                            <div class="checkbox-inline checkbox-lg">
                                <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-lg checkbox-primary">
                                    <input type="checkbox" name="approved_digital_written_signature" value="1"
                                        @if ($proposal->approved_digital_written_signature == 1) checked="checked" @endif @if ($editable == 0) readonly @endif />
                                    <span></span>
                                    Approved digital(e-sign) or written signature
                                </label>
                            </div>

                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-12 col-xs-12 mb-4 __cutmSwitch">
                            <label class="form-label">Written&nbsp;|&nbsp;Digital</label>
                            <span class="switch switch-primary">
                                <label>
                                    <input type="checkbox" name="type" value="Digital" id="writen-digital-sign"
                                        @if ($proposal->type == 'Digital') checked="checked" @endif @if ($editable == 0) readonly @endif />
                                    <span></span>
                                </label>
                            </span>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 mb-4">
                            <label class="form-label">Party 1 Signature</label>
                            <ul id="media-list" class="clearfix">
                                <li class="myupload" @if ($proposal->signature_1) style="display:none;" @endif>
                                    <span><i class="fa fa-plus" aria-hidden="true"></i>
                                        <input type="file" click-type="single" id="picupload" name="signature_1"
                                            value="{{ url('/storage') }}/{{ $proposal->signature_2 }}"
                                            class="picupload">
                                    </span>
                                </li>
                                @if ($proposal->signature_1)
                                    <li>
                                        <img src="/storage/{{ $proposal->signature_1 }}" title="" />
                                        @if ($editable == 1)
                                            <div class='post-thumb'>
                                                <div class='inner-post-thumb'>
                                                    <a href='javascript:void(0);' data-id='' class='remove-pic'>
                                                        <i class='fa fa-times' aria-hidden='true'>
                                                        </i>
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        <input type="text" name="old_signature_1" value="{{ $proposal->signature_1 }}"
                                            style="display: none;">
                                    </li>
                                @endif
                            </ul>
                            <input type="hidden" class="form-control esign" name="signature_1_esign" id="signature_1_esign"
                                value="">
                        </div>
                    </div>
                </div>

                <div class="__scopeWorkWrp wd100">
                    <div
                        class="__scopeWorkHdprt d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                        <div class="d-flex __proslAddLtPrt">
                            <h4 class="__fontWtbld mt-2 mb-2 mr-5 text-primary">Scope Of Work</h4>
                        </div>
                        @if ($editable == 1)
                            <div class="__proslAddRtPrt">
                                <button class="btn btn-primary btn-outline-primary addNewItemBtn" type="button"
                                    data-toggle="modal" data-target="#addNewItemModel"> Add Item </button>
                            </div>
                        @endif
                    </div>
                    <div class="__scopeWorkTbvw wd100 ">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col" class="text-center">Sl.No</th>
                                    <th scope="col" class="text-center">Name</th>
                                    <th scope="col" class="text-center">Service Type</th>
                                    <th scope="col" class="text-center">Sub Service</th>
                                    <th scope="col" class="text-center">Description</th>
                                    <th scope="col" class="text-right">Amount</th>
                                    <th scope="col" class="text-center">
                                        {{ $proposal->request->category['qty_label'] ?: 'Quantity' }}</th>
                                    <th scope="col" class="text-center">Tax</th>
                                    <th scope="col" class="text-right">Total</th>
                                    <th class="text-center" scope="col" style="display: none;">Remarks</th>
                                    <th class="text-center" scope="col" style="display: none;">Service Type Id</th>
                                    <th class="text-center" scope="col">Upload</th>
                                    <th class="text-center" scope="col">Action</th>

                                </tr>
                            </thead>
                            <tbody id="itemsTableBody">
                                @php
                                    $neg = false;
                                    $neg_total = 0.0;
                                    $neg_tax_total = 0.0;
                                    $neg_sub_total = 0.0;
                                @endphp
                                @foreach ($items as $key => $item)
                                    <tr data-row="{{ $key }}">
                                        <td class="text-center td-index">{{ $loop->iteration }}</td>
                                        <td class="text-center" data-id="{{ $item['id'] }}">
                                            {{ $item['item_name'] }}</td>
                                        <td class="text-center">{{ $item['service_type'] }}</td>
                                        <td class="text-center">{{ $item['sub_service'] }}</td>
                                        <td class="text-center">{{ $item['description'] }}</td>
                                        <td class="text-right">
                                            {{ number_format((float) $item['amount'], 2, '.', '') }}
                                            @if (isset($item['negotiation']))
                                                @php
                                                    $neg = true;
                                                    $neg_tax_total += $item['negotiation']['tax_total'];
                                                    $neg_sub_total += $item['negotiation']['sub_total'];
                                                    $neg_total += $item['negotiation']['total'];
                                                @endphp
                                                <p style="color: red;">
                                                    ({{ number_format((float) $item['negotiation']['amount'], 2, '.', '') }})
                                                </p>
                                            @else
                                                @php
                                                    $neg_tax_total += $item['tax_amount'];
                                                    $neg_sub_total += $item['sub_total'];
                                                    $neg_total += $item['total'];
                                                @endphp
                                            @endif
                                        </td>
                                        <td class="text-center">{{ $item['qty'] }}</td>
                                        <td class="text-center">{{ $item['tax'] }}%</td>
                                        <td class="text-right">
                                            {{ number_format((float) $item['total'], 2, '.', '') }}
                                            @if (isset($item['negotiation']))
                                                <p style="color: red;">
                                                    ({{ number_format((float) $item['negotiation']['total'], 2, '.', '') }})
                                                </p>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <input type="hidden" value="{{ $item['image_url'] }}" class="image_url">
                                            @if ($item['image_url'])
                                                <div class="__scpTbImg">
                                                    <img class="img-fluid item_image"
                                                        src="/storage/{{ $item['image_url'] }}">
                                                </div>
                                            @endif
                                        </td>
                                        <td class="text-right">
                                            @if ($editable == 1)
                                                @if ($neg_total != 0 && $neg)
                                                    <a href="javascript:void(0)" title="Negotiation"
                                                        class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary negBtn">
                                                        <i class="far fa-sticky-note"></i>
                                                    </a>
                                                @endif
                                                <a href="javascript:void(0)" title="Edit"
                                                    class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary editItemBtn">
                                                    <i class="far fa-edit"></i>
                                                </a>
                                            @else
                                                @if ($proposal->status != 'Accepted')
                                                    <a href="javascript:void(0)" title="Negotiation"
                                                        class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary negBtn">
                                                        <i class="far fa-sticky-note"></i>
                                                    </a>
                                                @endif
                                                <a href="javascript:void(0)" title="View"
                                                    class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary editItemBtn">
                                                    <i class="flaticon-file-2"></i>
                                                </a>
                                            @endif
                                            @if ($editable == 1)
                                                <a href="javascript:void(0);" title="Delete"
                                                    class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary  removeItemBtn">
                                                    <i class="fas fa-trash-alt"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="7" align="right" valign="middle" class="__tbbodr">Subtotal</td>
                                    <td align="right" class="text-right __tbbodr" id="sub_total">
                                        {{ number_format((float) $proposal->items->sum('sub_total'), 2, '.', '') }}</td>
                                    <td align="right" class="text-right __tbbodr" style="color: red;" id="neg_sub_total">
                                        @if ($neg_total > 0 && $neg) {{ number_format((float) $neg_sub_total, 2, '.', '') }} @endif
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="7" align="right" valign="middle" class="__tbbodr">Tax</td>
                                    <td align="right" class="text-right __tbbodr" id="tax_total">
                                        {{ number_format((float) $proposal->items->sum('tax_total'), 2, '.', '') }}</td>
                                    <td align="right" class="text-right __tbbodr" style="color: red;" id="neg_tax_total">

                                        @if ($neg_total > 0 && $neg){{ number_format((float) $neg_tax_total, 2, '.', '') }}@endif
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="7" align="right" valign="middle" class="__tbbodr font-weight-bolder">
                                        <strong>Total </strong>
                                    </td>
                                    <td align="right" class="text-right __tbbodr font-weight-bolder" id="total">
                                        {{ number_format((float) $proposal->items->sum('total'), 2, '.', '') }}</td>
                                    <td align="right" class="text-right __tbbodr" style="color: red;" id="neg_total">
                                        @if ($neg_total > 0 && $neg){{ number_format((float) $neg_total, 2, '.', '') }}@endif
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="__terAgntWrp  wd100">
                    <div
                        class="__scopeWorkHdprt d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                        <div class="d-flex __proslAddLtPrt">
                            <h4 class="__fontWtbld mt-2 mb-2 mr-5 text-primary">Terms And Conditions</h4>
                        </div>
                        @if ($editable == 1)
                            <div class="__proslAddRtPrt">
                                <button class="btn btn-primary btnTCAddNew" type="button"> Add New</button>
                                <button class="btn btn-secondary btnApplyDflt" type="button" data-type="Proposal"> Select
                                    Default</button>
                            </div>
                        @endif
                    </div>
                    @if ($editable == 1)
                        <div class="__terAgntWrpBz">
                            <ol id="tc_list">
                                @if (count($proposal->terms_negotiations) > 0)
                                    @php
                                        $terms_neg = json_decode($proposal->terms_negotiations[0]->terms);
                                    @endphp
                                @endif
                                @foreach (json_decode($proposal->terms_of_agreement) as $key => $terms)
                                    <li data-text="{{ $terms->text }}" data-row="{{ $key }}">
                                        {{ $terms->text }}
                                        @if (count($proposal->terms_negotiations) > 0)
                                            <span class="neg_text_{{ $key }}" style="color: red;">
                                                @foreach ($terms_neg as $term_)
                                                    @if ($term_->row == $key)
                                                        ({{ $term_->text }})
                                                    @endif
                                                @endforeach
                                            </span>
                                        @endif
                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon TCeditBtn"
                                            title="Edit"><i class="la la-pencil"></i></a>
                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon TCdeleteBtn"
                                            title="Delete"><i class="la la-trash"></i></a>
                                    </li>
                                @endforeach
                            </ol>
                        </div>
                    @else
                        <div class="__terAgntWrpBz">
                            <ol>
                                @foreach (json_decode($proposal->terms_of_agreement) as $key => $terms)
                                    <li>{{ $terms->text }}</li>
                                @endforeach
                            </ol>
                        </div>
                    @endif
                    <div class="wd100 text-right mt-6">
                        @if($proposal->negotiation_status == 1)
                        <button type="button" id="btnNeg"
                            class="btn btn-secondary pl-15 pr-15 pt-5 pb-5 font-weight-bolder">Accept Negotiation
                        </button>
                        <button type="button" id="btnNegSubmit"
                            class="btn btn-primary pl-15 pr-15 pt-5 pb-5 font-weight-bolder">Send Revised Proposal
                        </button>
                        @else
                        <button type="button" id="btnSubmit"
                            class="btn btn-primary pl-15 pr-15 pt-5 pb-5 font-weight-bolder"
                            @if ($proposal->status == 'Accepted') disabled @endif> @if ($editable == 1 && $proposal->status == 'Send/Recieved') Update Proposal @elseif($editable == 1 && $proposal->status == 'Draft') Send Proposal @else @if ($proposal->status == 'Send/Recieved' || $proposal->status == 'Pending') Update Proposal @else Proposal Accepted @endif @endif</button>
                        @endif

                    </div>
                </div>
            </form>
            <!--end::Container-->
        </div>
        @include('vendor.proposals.default_tc')
        @include('vendor.proposals.add_tc_item')
        @include('vendor.proposals.add_item')
        @include('vendor.proposals.add_negotiation')
        @include('vendor.proposals.e_sign_canvas')
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        var items = [];
        var tc_items = [];
        var neg_tc_items = [];
        items = "{{ json_encode($items) }}";
        tc_items = "{{ $proposal_tc }}";
        if("{{ $tc_negotiations }}")
        {
            neg_tc_items = "{{ $tc_negotiations }}";
            localStorage.setItem("neg_tc_items", neg_tc_items.replace(/&quot;/g, '"'));
        }
        else
        {
            localStorage.setItem("neg_tc_items",JSON.stringify([]) );
        }

        tc_items = tc_items.toString();
        localStorage.setItem("items", JSON.stringify(JSON.parse(items.replace(/&quot;/g, '"'))));
        localStorage.setItem("tc_items", (tc_items.replace(/&quot;/g, '"')).replace(/\n/g, ' '));
        // console.log(localStorage['tc_items']);
        // var tc_items = localStorage['tc_items'].replace(/\\n/g, '');

        $('.TCeditBtn').on('click', function() {
            $('#btnAddTCItem').text('Update Item');
            tc_editable = $(this).parent().data('text');
            tc_editable_row = $(this).parent().data('row');
            $('textarea#terms_and_condition').val($(this).parent().data('text'));
            $('#addTCItemModel').modal('show');
        });
        $('.TCdeleteBtn').on('click', function() {
            var tc_items = JSON.parse(localStorage["tc_items"]);
            array_index = $(this).parent().data('row');

            Swal.fire({
                title: "Are you sure?",
                text: "You wont be able to revert this!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function(result) {
                if (result.value) {
                    tc_items.splice(array_index, 1);
                    localStorage["tc_items"] = JSON.stringify(tc_items);
                    updateTCRows();
                } else if (result.dismiss === "cancel") {
                    Swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                    )
                }
            });
        });
        $('#kt_datetimepicker_3').datetimepicker({
            defaultDate: "{{ $proposal->date }}",
            format: 'MM/D/yyyy'
        });
        $('#request_select').select2({
            placeholder: "Select a Request",
        });
        $('#item_name').select2({
            tags: true,
            placeholder: "Select Item"
        });
        $('.summernote_terms_and_conditions').summernote({
            height: 200,
        });
        $('.summernote_terms_and_conditions').summernote('code', '{!! $proposal->terms_of_agreement !!}');
        if ("{{ $editable }}" == "0") {
            $('#summernote_terms_and_conditions').summernote('disable');
        }

        $('#request_select').on('change', function() {
            var category_id = $(this).find(':selected').data('category_id');
            var category_text = $(this).find(':selected').text();
            $('#name_of_party2').val($(this).find(':selected').data('vendor_name'));
            $('#name_of_party2_id').val($(this).find(':selected').data('vendor_id'));
            if ($(this).find(':selected').data('category_qty')) {
                $('.qty_label_text').html($(this).find(':selected').data('category_qty'));
                $('#item_quantity').attr('placeholder', $(this).find(':selected').data('category_qty'));

            }
            $('#item_service_type').empty();
            $('#item_service_type').append('<option value="' + category_id + '" selected>' + category_text +
                '</option>');
        });
        $('#btnNeg').on('click', function() {
            var items = JSON.parse(localStorage["items"]);
            var row = 0;
            items.forEach(element => {
                if (element.negotiation) {
                    element.amount = element.negotiation.amount;
                    element.sub_total = element.negotiation.sub_total
                    element.tax_amount = (element.negotiation.sub_total / 100) * parseFloat(element.tax);
                    element.total = element.negotiation.total;
                    element.negotiation = null;
                }
            });
            localStorage.setItem("items", JSON.stringify(items));
            updateTableRows(items);
            var tc_items = JSON.parse(localStorage["tc_items"]);
            var neg_tc_items = JSON.parse(localStorage["neg_tc_items"]);
            var row_ = 0;
            tc_items.forEach(tc_item => {
                neg_tc_items.forEach(neg_tc_item => {
                    if (neg_tc_item.row == row_) {
                        tc_item.text = neg_tc_item.text;
                    }
                });
                row_++;
            });
            localStorage.removeItem("neg_tc_items");
            localStorage.setItem("tc_items", JSON.stringify(tc_items));
            updateTCRows();
        });
        editRow();
        $('.negBtn').on('click', function(e) {
            var row = $(this).closest("tr");
            $('#neg_name').val(row.find("td:eq(1)").html());
            var amount = parseFloat(row.find("td:eq(4)").find('.neg_amount_row').html().replace(')', '').replace(
                '(', ''));
            $('#neg_amount').val(isNaN(amount) ? 0 : amount);
            $('#neg_note').val(row.find("td:eq(4)").find('.neg_amount_row').data('remark'));
            editable_row = row;
            $('#addNegModel').modal('show');
        });
        $('#btnAddNeg').on('click', function() {
            var row = editable_row;
            var amount = parseFloat($('#neg_amount').val());
            var note = $('#neg_note').val();
            if (amount) {
                row.find("td:eq(4)").find('.neg_amount_row').css('display', 'block');
                row.find("td:eq(4)").find('.neg_amount_row').data('remark', note);
                row.find("td:eq(4)").find('.neg_amount_row').html('(' + amount.toFixed(2) + ')');
                var qty = parseFloat(row.find("td:eq(5)").html());
                var tax = parseFloat(row.find("td:eq(6)").html().replace('%', ''));
                var sub_total = qty * amount;
                var tax_amount = (sub_total / 100) * tax;
                row.find("td:eq(7)").find('.neg_total_row').css('display', 'block');
                row.find("td:eq(7)").find('.neg_total_row').html('(' + (tax_amount + sub_total).toFixed(2) + ')');
            } else {
                row.find("td:eq(4)").find('.neg_amount_row').css('display', 'none');
                row.find("td:eq(4)").find('.neg_amount_row').data('remark', '');
                row.find("td:eq(4)").find('.neg_amount_row').html('');
                row.find("td:eq(7)").find('.neg_total_row').html('');
                row.find("td:eq(7)").find('.neg_total_row').css('display', 'none');
            }
            setIndex();
            $('#addNegModel').modal('hide');
        });
        $("#btnNegSubmit").click(function(event) {
            event.preventDefault();
            var items = JSON.parse(localStorage["items"]);
            var tc_items = localStorage["tc_items"];
            var form = $('#proposals-form')[0];
            var data = new FormData(form);
            var editable = "{{ $editable }}";
            data.append('terms_of_agreement', tc_items);
            data.append('items', JSON.stringify(items));
            data.append('editable', editable);
            data.append('negotiation', 1);
            $("#btnSubmit").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/vendor/event/{{ $event->id }}/proposals/{{ $proposal->id }}",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    $("#btnSubmit").prop("disabled", false);
                    localStorage.setItem("neg_tc_items", []);
                    setTimeout(function() {
                        location.replace('/event-planner/event/{{ $event->id }}');
                    }, 1000);
                },
                error: function(e) {
                    $("#btnSubmit").prop("disabled", false);
                }
            });
        });
        $("#btnSubmit").click(function(event) {
            event.preventDefault();
            var items = JSON.parse(localStorage["items"]);
            var tc_items = localStorage["tc_items"];
            if (items.length > 0) {
                var form = $('#proposals-form')[0];
                var data = new FormData(form);
                var editable = "{{ $editable }}";

                var terms_of_agreement = $('.summernote_terms_and_conditions').summernote('code');
                data.append('terms_of_agreement', tc_items);
                data.append('items', JSON.stringify(items));
                data.append('editable', editable);
                $("#btnSubmit").prop("disabled", true);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: "/vendor/event/{{ $event->id }}/proposals/{{ $proposal->id }}",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(data) {
                        $("#btnSubmit").prop("disabled", false);
                        setTimeout(function() {
                            localStorage.setItem("neg_tc_items", []);
                            location.replace('/vendor/event/{{ $event->id }}');
                        }, 1000);
                    },
                    error: function(e) {
                        $("#btnSubmit").prop("disabled", false);
                    }
                });
            } else {
                toastr.error('Add items to proposal.', 'Error');
            }

        });
        var parent_tag = '';
        $('.picupload').on('click', function(e) {
            if ($(this).data('type') == 'item_file') {

            } else {
                if ($('#writen-digital-sign:checkbox:checked').length > 0) {
                    e.preventDefault();
                    var canvas = document.getElementById("sig-canvas");
                    canvas.width = canvas.width;
                    $('#eSignModel').modal('show');
                    parent_tag = $(this).parent().parent();
                }
            }
        });

        function clearFields() {
            $('#item_name').val('');
            $('textarea#item_description').val('');
            $('#item_quantity').val('');
            // $('#item_tax').val('');
            $('#item_amount').val('');
            $('textarea#item_remarks').val('');
            $('#item_editable').val(null);
            var image =
                `<li class="myupload"><span><i class="fa fa-plus" aria-hidden="true"></i><input type="file" click-type="single" id="item_file" name="item_file" class="picupload" data-type="item_file"></span></li>`;
            $('.item_image_panel').html(image);
            editable_row = null;
        }

        $('#btnAddItem').on('click', function() {
            var items = JSON.parse(localStorage["items"]);
            if (jsFieldsValidator(['item_name', 'item_amount', 'item_tax', 'item_quantity'])) {
                var item_name = $('#item_name').val();
                var description = $('textarea#item_description').val()
                var service_type = $('#item_service_type').text();
                var service_type_id = $('#item_service_type').val();
                var sub_service = $('#item_sub_service').find(':selected').val() ? $('#item_sub_service').find(
                    ':selected').text() : '';
                var sub_service_id = $('#item_sub_service').find(':selected').val();
                var qty = parseFloat($('#item_quantity').val());
                var amount = parseFloat($('#item_amount').val());
                var tax = parseFloat($('#item_tax').val());
                var remark = $('#item_remarks').val();
                var sub_total = qty * amount;
                var tax_amount = (sub_total / 100) * tax;
                var total = sub_total + tax_amount;
                var image_url = '';
                if ($('#item_old_image').val()) {
                    image_url = $('#item_old_image').val();
                } else {
                    var files = $('#item_file')[0].files;
                    if (files.length > 0) {
                        image_url = uploadImage(files);
                        $('.item_image_panel').find('.myupload').show();
                        $('.item_image_panel')[0].children[0].remove();
                    } else {
                        image_url = null;
                    }
                }
                var item = {
                    item_name: item_name,
                    description: description,
                    service_type: service_type,
                    service_type_id: service_type_id,
                    sub_service: sub_service,
                    sub_service_id: sub_service_id,
                    qty: qty,
                    amount: amount,
                    tax: tax,
                    remark: remark,
                    sub_total: sub_total,
                    tax_amount: tax_amount,
                    total: total,
                    image_url: image_url
                };
                if (items.length > 0) {
                    var exist = 0;
                    items.forEach(element => {
                        if (element.item_name == item_name) {
                            exist = 1;
                            element.setvice_type = service_type;
                            element.service_type_id = service_type_id;
                            element.sub_service = sub_service;
                            element.sub_service_id = sub_service_id;
                            element.description = description;
                            element.qty = qty;
                            element.amount = amount;
                            element.tax = tax;
                            element.remark = remark;
                            element.sub_total = sub_total;
                            element.tax_amount = tax_amount;
                            element.total = total;
                            element.image_url = image_url
                        }
                    });
                    if (exist == 0) {
                        items.push(item);
                    }
                } else {
                    items.push(item);
                }
                localStorage["items"] = JSON.stringify(items);
                updateTableRows(items);
                $('#addNewItemModel').modal('hide');
            }

        });

        function updateTableRows(items) {

            $('#itemsTableBody').empty();
            var sub_total = 0;
            var tax_total = 0;
            var total = 0;

            var neg_sub_total = 0;
            var neg_tax_total = 0;
            var neg_total = 0;
            items.forEach((element, key) => {
                var row = '<tr data-row="' + key + '">';
                row += '<td class="text-center td-index">1</td>';
                row += '<td class="text-center">' + element.item_name + '</td>';
                row += '<td class="text-center">' + element.service_type + '</td>';
                row += '<td class="text-center">' + element.sub_service + '</td>';
                row += '<td class="text-center">' + element.description + '</td>';
                row += element.negotiation ? '<td class="text-right">' + element.amount.toFixed(2) +
                    '<p style="color: red;">(' + element.negotiation.amount.toFixed(2) + ')</p></td>' :
                    '<td class="text-right">' + element.amount.toFixed(2) + '</td>';
                row += '<td class="text-center">' + element.qty + '</td>';
                row += '<td class="text-right">' + element.tax + '%</td>';
                row += element.negotiation ? '<td class="text-right">' + element.total.toFixed(2) +
                    '<p style="color: red;">(' + element.negotiation.total.toFixed(2) + ')</p></td>' :
                    '<td class="text-right">' + element.total.toFixed(2) + '</td>';
                if (element.image_url) {
                    row += '<td class="text-center"><input type="hidden" value="' + element.image_url +
                        '" class="image_url"><div class="__scpTbImg"><img class="img-fluid item_image" src="/storage/' +
                        element.image_url + '" ></div></td>';
                } else {
                    row += '<td></td>';
                }
                row += '<td class="text-right">';
                if (element.negotiation) {
                    row +=
                        '<a href="javascript:void(0)" title="Negotiation"class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary negBtn"><i class="far fa-sticky-note"></i></a>';
                }
                row +=
                    '<a href="javascript:void(0)" title="Edit" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary editItemBtn"><i class="far fa-edit"></i></i></a><a href="javascript:void(0);" title="Delete" class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary removeItemBtn"><i class="fas fa-trash-alt"></i></a></td></tr>';

                $('#itemsTableBody').append(row);
                editRow();
                sub_total += element.sub_total;
                tax_total += element.tax_amount;
                total += element.total;

                if (element.negotiation) {
                    neg_sub_total += element.negotiation.sub_total;
                    neg_tax_total += element.negotiation.tax_total;
                    neg_total += element.negotiation.total;
                } else {
                    neg_sub_total += element.sub_total;
                    neg_tax_total += element.tax_amount;
                    neg_total += element.total;
                }
                if (element.negotiation) {
                    $('#neg_sub_total').text(neg_sub_total != 0 ? neg_sub_total.toFixed(2) : '');
                    $('#neg_tax_total').text(neg_tax_total != 0 ? neg_tax_total.toFixed(2) : '');
                    $('#neg_total').text(neg_total != 0 ? neg_total.toFixed(2) : '');
                } else {
                    $('#neg_sub_total').text('');
                    $('#neg_tax_total').text('');
                    $('#neg_total').text('');
                }
                $('#sub_total').text(sub_total.toFixed(2));
                $('#tax_total').text(tax_total.toFixed(2));
                $('#total').text(total.toFixed(2));

            });
        }

        $('.addNewItemBtn').on('click', function() {
            clearFields();
        });

        function uploadImage(files, editable = null) {
            var image_url;
            var fd = new FormData();
            fd.append('file', files[0]);
            fd.append('path', 'vendor/proposals/{{ $event->id }}/items');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/upload-file",
                data: fd,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    image_url = data;
                    // updateRow(image_url, editable);
                },
                error: function(e) {

                }
            });

            return image_url;
        }

        function editRow() {
            $('.editItemBtn').on('click', function(e) {
                var row = $(this).closest("tr");
                var array_index = row.data('row');
                var item = JSON.parse(localStorage["items"])[array_index];
                $('#item_name').val(item.item_name).trigger('change');
                $('textarea#item_description').val(item.description);
                $('#item_amount').val(item.amount);
                $('#item_tax').val(item.tax);
                $('#item_quantity').val(item.qty);
                $('textarea#item_remarks').val(item.remark);
                // if (item.image_url) {
                //     var image = `<li class="myupload" style="display:none;"><span><i class="fa fa-plus" aria-hidden="true"></i><input type="file" click-type="single" id="item_file" name="item_file" class="picupload" data-type="item_file"></span></li>
            //             <li>
            //             <img src="` + row.find("td:eq(10)").find('.item_image').attr('src') + `" title=""/>
            //             <div class='post-thumb'>
            //                 <div class='inner-post-thumb'>
            //                     <a href='javascript:void(0);' data-id='' class='remove-pic'>
            //                         <i class='fa fa-times' aria-hidden='true'>
            //                         </i>
            //                     </a>
            //                 </div>
            //             </div>
            //             <input type="text" name="old_image" id="item_old_image" value="` + row.find("td:eq(10)")
                //         .find('.item_image').attr('src') + `" style="display: none;">
            //         </li>`;
                //     $('.item_image_panel').html(image);
                // }
                $('#addNewItemModel').modal('show');
            });
            $('.removeItemBtn').on('click', function(e) {
                var row = $(this).closest("tr");
                var array_index = row.data('row');
                var items = JSON.parse(localStorage["items"]);

                Swal.fire({
                    title: "Are you sure?",
                    text: "You wont be able to revert this!",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: true
                }).then(function(result) {
                    if (result.value) {
                        items.splice(array_index, 1);
                        localStorage["items"] = JSON.stringify(items);
                        updateTableRows(items);
                    } else if (result.dismiss === "cancel") {
                        Swal.fire(
                            "Cancelled",
                            "Your imaginary file is safe :)",
                            "error"
                        )
                    }
                });

            });
            $('.negBtn').on('click', function(e) {
                var row = $(this).closest("tr");
                var array_index = row.data('row');
                var item = JSON.parse(localStorage["items"])[array_index];
                $('#neg_name').val(item.item_name);
                $('#neg_amount').val(item.negotiation ? item.negotiation.amount : 0);
                $('#neg_note').val(item.negotiation ? item.negotiation.note : '');
                $('#addNegModel').modal('show');
            });

        }
    </script>
@endsection
