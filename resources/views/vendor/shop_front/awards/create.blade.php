@extends('admin.accountSettings.account_settings_layout')
@section('account_settings_content')
    <!--begin::Content-->
    <div class="flex-row-fluid ml-lg-8">
        <!--begin::Card-->
        <div class="card card-custom card-stretch">
            <!--begin::Header-->
            <div class="card-header py-3">
                <div class="card-title align-items-start flex-column">
                    <h3 class="card-label font-weight-bolder text-dark">Awards and Affiliations
                    </h3>
                </div>
                <div class="card-toolbar">
                    <button type="submit" id="btnSubmit" class="btn btn-success mr-2">Save</button>
                    <a href="{{ url('/awards-affiliations') }}"><button type="reset"
                            class="btn btn-secondary">Cancel</button></a>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Form-->
            @php
                $url = '/awards-affiliations';
                if (isset($editable)) {
                    $url = '/awards-affiliations/' . $editable->id;
                }
            @endphp
            <form class="form" id="form">
                @csrf
                @if (isset($editable))
                    @method('PATCH')
                @endif
                <!--begin::Body-->
                <div class="card-body">
                    <div class="row">
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Title</label>
                            <input type="text" class="form-control" id="title" name="title" @isset($editable)
                                value="{{ $editable->title }}" @endisset />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Description</label>
                            <input type="text" class="form-control" id="description" name="description" @isset($editable)
                                value="{{ $editable->description }}" @endisset />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Thumbnail</label>
                            @if (isset($editable))
                                <ul id="media-list" class="clearfix">
                                    <li class="myupload" @if ($editable->thumbnail) style="display:none;" @endif>
                                        <span><i class="fa fa-plus" aria-hidden="true"></i>
                                            <input type="file" click-type="single" id="picupload" name="thumbnail"
                                                value="{{ url('/storage') }}/{{ $editable->thumbnail }}"
                                                class="picupload">
                                        </span>
                                    </li>
                                    @if ($editable->thumbnail)
                                        <li>
                                            <img src="/storage/{{ $editable->thumbnail }}" title="" />
                                            <div class='post-thumb'>
                                                <div class='inner-post-thumb'>
                                                    <a href='javascript:void(0);' data-id='' class='remove-pic'>
                                                        <i class='fa fa-times' aria-hidden='true'>
                                                        </i>
                                                    </a>
                                                </div>
                                            </div>
                                            <input type="text" name="old_thumbnail" value="{{ $editable->thumbnail }}"
                                                style="display: none;">
                                        </li>
                                    @endif
                                </ul>
                            @else
                                <ul id="media-list" class="clearfix">
                                    <li class="myupload">
                                        <span><i class="fa fa-plus" aria-hidden="true"></i>
                                            <input type="file" click-type="single" id="picupload" name="thumbnail"
                                                class="picupload">
                                        </span>
                                    </li>
                                </ul>
                            @endif
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Aattachment</label>
                            <input type="file" class="form-control" name="attachment" />
                            @if (isset($editable))
                                @if ($editable->attachment)
                                    <a href="/storage/{{ $editable->attachment }}"
                                        download="{{ $editable->title }}">Download</a>
                                @endif
                            @endif
                        </div>

                    </div>
                </div>

            </form>
            <!--end::Form-->
        </div>
    </div>
    <!--end::Content-->

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script type="text/javascript">
        var url = "{{ $url }}";
        $("#btnSubmit").click(function(event) {
            event.preventDefault();
            var form = $('#form')[0];
            var data = new FormData(form);
            $("#btnSubmit").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: url,
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    $("#btnSubmit").prop("disabled", false);
                    setTimeout(function() {
                        location.replace('/awards-affiliations');
                    }, 1000);

                },
                error: function(e) {
                    $("#btnSubmit").prop("disabled", false);
                }
            });
        });
    </script>
@endsection
