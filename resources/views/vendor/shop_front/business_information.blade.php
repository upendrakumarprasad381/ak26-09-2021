@extends('vendor.shop_front.master')
@section('shop_front_content')
    <!--begin::Content-->
    <div class="flex-row-fluid ml-lg-8">
        <!--begin::Card-->
        <div class="card card-custom card-stretch">
            <!--begin::Header-->
            <div class="card-header py-3">
                <div class="card-title align-items-start flex-column">
                    <h3 class="card-label font-weight-bolder text-dark">Business Information</h3>
                    <span class="text-muted font-weight-bold font-size-sm mt-1">Update your business
                        informaiton</span>
                </div>
                <div class="card-toolbar">
                    <button type="submit" id="btnSubmit" class="btn btn-success mr-2">Save Changes</button>
                    <a href="{{ url('/dashboard') }}"><button type="reset" class="btn btn-secondary">Cancel</button></a>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Form-->
            <form class="form" id="form">
                @csrf
                <!--begin::Body-->
                <div class="card-body">
                    <div class="row">
                        <div class="form-group validated col-md-12">
                            <label class="form-control-label">Logo</label>
                            <ul id="media-list" class="clearfix">
                                <li class="myupload" @if ($user->bussinessDetails['logo'])
                                    style="display:none;" @endif>
                                    <span><i class="fa fa-plus" aria-hidden="true"></i>
                                        <input type="file" click-type="single" id="picupload" name="logo"
                                            value="{{ url('/storage') }}/{{ $user->bussinessDetails['logo'] }}"
                                            class="picupload">
                                    </span>
                                </li>
                                @if ($user->bussinessDetails['logo'])
                                    <li>
                                        <img src="/storage/{{ $user->bussinessDetails['logo'] }}" title="" />
                                        <div class='post-thumb'>
                                            <div class='inner-post-thumb'>
                                                <a href='javascript:void(0);' data-id='' class='remove-pic'>
                                                    <i class='fa fa-times' aria-hidden='true'>
                                                    </i>
                                                </a>
                                            </div>
                                        </div>
                                        <input type="text" name="old_logo" value="{{ $user->bussinessDetails['logo'] }}"
                                            style="display: none;">
                                    </li>
                                @endif
                            </ul>
                        </div>
                        <div class="form-group validated col-md-12">
                            <label class="form-control-label">About</label>
                            <textarea class="form-control" rows="5"
                                name="about">{{ $user->bussinessDetails['about'] }}</textarea>
                        </div>
                        <div class="form-group validated col-md-12">
                            <label class="form-control-label">Occasions</label>
                            <select class="form-control select2" multiple="multiple" id="occasion_select"
                                name="occasions[]">
                                <option value>Select Occasions</option>
                                @foreach ($occasions as $occasion)
                                    <option value="{{ $occasion['id'] }}" @if (in_array($occasion['id'], $user->occasions->pluck('id')->toArray())) selected @endif>
                                        {{ $occasion['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                            <label>Categories</label>
                            <select class="form-control select2" multiple="multiple" id="categories_select"
                                onchange="changeCategories(this)" name="categories[]">
                                <option value></option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category['id'] }}" @if (in_array($category['id'], $user->categories->pluck('id')->toArray())) selected @endif>
                                        {{ $category['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Price Range</label>
                            <select class="form-control" id="price_range" name="price_range">
                                <option value>Select Budget Range</option>
                                <option value="[1000000,0]" @if ($user->bussinessDetails['min_price'] == 1000000)
                                    selected @endif>Over AED 10,00,000</option>
                                <option value="[500000,1000000]" @if ($user->bussinessDetails['min_price'] == 500000) selected @endif>AED 500,000 - AED 10,00,000
                                </option>
                                <option value="[100000,500000]" @if ($user->bussinessDetails['min_price'] == 100000) selected @endif>AED 100,000 - AED 500,000
                                </option>
                                <option value="[50000,100000]" @if ($user->bussinessDetails['min_price'] == 50000) selected @endif>AED 50,000 - AED 100,000
                                </option>
                                <option value="[0,50000]" @if ($user->bussinessDetails['min_price'] == 0)
                                    selected @endif>Less than AED 50,000
                                </option>
                            </select>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                            <label>Emirate Availability</label>
                            <select class="form-control select2" multiple="multiple" id="emirate_select" name="emirates[]">
                                <option value></option>
                                @foreach ($emirates as $emirate)
                                    <option value="{{ $emirate['id'] }}" @if (in_array($emirate['id'], $user->emirates->pluck('id')->toArray())) selected @endif>
                                        {{ $emirate['emirate'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group validated col-md-12">
                            <label class="form-control-label">Address</label>
                            <input type="text" class="form-control" name="address"
                                value="{{ $user->bussinessDetails['address'] }}" />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Secondary Email</label>
                            <input type="text" class="form-control" name="email_2"
                                value="{{ $user->bussinessDetails['email_2'] }}" />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Whatsapp</label>
                            <input type="number" class="form-control" name="whatsapp"
                                value="{{ $user->bussinessDetails['whatsapp'] }}" />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Country</label>
                            <select class="form-control select2" id="country" name="country">
                                <option value></option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}" @if ($user->bussinessDetails['country'] == $country->id) selected @endif>
                                        {{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">City</label>
                            <select class="form-control select2" id="city" name="city">
                                <option value></option>
                                @foreach ($cities as $city)
                                    <option value="{{ $city->id }}" @if ($user->bussinessDetails['city'] == $city->id) selected @endif>
                                        {{ $city->name }}</option>
                                @endforeach
                            </select>
                            {{-- <input type="text" class="form-control" name="city"
                                value="{{ $user->bussinessDetails['city'] }}" /> --}}
                        </div>
                        {{-- <div class="form-group validated col-md-6">
                            <label class="form-control-label">Location</label>
                            <input type="text" class="form-control" name="location"
                                value="{{ $user->bussinessDetails['emirates'] }}" />
                        </div> --}}
                        {{-- <div class="form-group validated col-md-6">
                            <label class="form-control-label">Emirate Availability</label>
                            <input type="text" class="form-control" name="emirates_availability"
                                value="{{ $user->bussinessDetails['emirates_availability'] }}" />
                        </div> --}}

                    </div>
                    <h4>Documents</h4>
                    <hr>
                    <div class="row">
                        <div class="form-group validated col-md-4">
                            <label class="form-control-label">Trade License</label>
                            <input type="file" class="form-control" value="" name="trade_license" />
                            @if ($user->bussinessDetails['trade_license'])
                                <a href="/storage/{{ $user->bussinessDetails['trade_license'] }}"
                                    download="{{ $user->first_name }}{{ $user->last_name }}-Trade License">Download-Trade
                                    License</a>
                            @endif
                        </div>
                        <div class="form-group validated col-md-4">
                            <label class="form-control-label">Identity Proof</label>
                            <input type="file" class="form-control" value="" name="identity_proof" />
                            @if ($user->bussinessDetails['identity_proof'])
                                <a href="/storage/{{ $user->bussinessDetails['identity_proof'] }}"
                                    download="{{ $user->first_name }}{{ $user->last_name }}-Identity Proof">Download-Identity
                                    Proof</a>
                            @endif
                        </div>
                        <div class="form-group validated col-md-4">
                            <label class="form-control-label">Passport</label>
                            <input type="file" class="form-control" value="" name="passport" />
                            @if ($user->bussinessDetails['passport'])
                                <a href="/storage/{{ $user->bussinessDetails['passport'] }}"
                                    download="{{ $user->first_name }}{{ $user->last_name }}-Passport">Download-Passport</a>
                            @endif
                        </div>
                    </div>
                    <h4>Social Media</h4>
                    <hr>
                    <div class="row">
                        <div class="form-group validated col-md-12">
                            <label class="form-control-label">Website</label>
                            <input type="text" class="form-control" name="business_website"
                                value="{{ $user->bussinessDetails['business_website'] }}" />
                        </div>
                        {{-- <div class="form-group validated col-md-6">
                            <label class="form-control-label">Social Media Link</label>
                            <input type="text" class="form-control" name="social_media_link"
                                value="{{ $user->bussinessDetails['social_media_link'] }}" />
                        </div> --}}
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Instagram</label>
                            <input type="text" class="form-control" name="instagram"
                                value="{{ $user->bussinessDetails['instagram'] }}" />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Facebook</label>
                            <input type="text" class="form-control" name="facebook"
                                value="{{ $user->bussinessDetails['facebook'] }}" />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Linkedin</label>
                            <input type="text" class="form-control" name="linkedin"
                                value="{{ $user->bussinessDetails['linkedin'] }}" />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Twitter</label>
                            <input type="text" class="form-control" name="twiter"
                                value="{{ $user->bussinessDetails['twiter'] }}" />
                        </div>
                        {{-- <div class="form-group validated col-md-6">
                            <label class="form-control-label">Focus</label>
                            <input type="text" class="form-control" name="focus"
                                value="{{ $user->bussinessDetails['focus'] }}" />
                        </div> --}}
                        {{-- <div class="form-group validated col-md-6">
                            <label class="form-control-label">Cuisine & Focus</label>
                            <input type="text" class="form-control" name="cuisine_focus"
                                value="{{ $user->bussinessDetails['cuisine_focus'] }}" />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Menu Type</label>
                            <input type="text" class="form-control" name="menu_type"
                                value="{{ $user->bussinessDetails['menu_type'] }}" />
                        </div>
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Experience & Focus</label>
                            <input type="text" class="form-control" name="experience_focus"
                                value="{{ $user->bussinessDetails['instagram'] }}" />
                        </div>

                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Capacity</label>
                            <input type="text" class="form-control" name="capacity"
                                value="{{ $user->bussinessDetails['capacity'] }}" />
                        </div> --}}
                    </div>
                    <h4>Filters</h4>
                    <hr>
                    <div class="row filters_container">

                    </div>
                </div>
                <!--end::Body-->
                <div class="card-footer">
                    <a href="{{ url('/awards-affiliations') }}"><button type="button" class="btn btn-primary">Awards and
                            Affiliations</button></a>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
    <!--end::Content-->
@endsection
{{-- Scripts Section --}}
@section('scripts')
    <script type="text/javascript">
        $("#btnSubmit").click(function(event) {
            event.preventDefault();
            var form = $('#form')[0];
            var data = new FormData(form);
            $("#btnSubmit").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/update_business_information",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    $("#btnSubmit").prop("disabled", false);
                    setTimeout(function() {
                        location.reload();
                    }, 1000);

                },
                error: function(e) {
                    $("#btnSubmit").prop("disabled", false);
                }
            });
        });

        $('#country').on('change', function() {
            var country_id = $(this).val();
            $.ajax({
                type: "GET",
                url: "/get-cities",
                data: {
                    country_id: country_id
                },
                success: function(data) {
                    var row = '<option value></option>';
                    $('#city').empty();
                    data.forEach(element => {
                        row += '<option value="' + element.id + '">' + element.name +
                            '</option>'
                    });
                    $('#city').append(row);
                },
                error: function(e) {}
            });
        });

        $('#occasion_select').select2({
            placeholder: "Select Occasions",
        });
        $('#emirate_select').select2({
            placeholder: "Select Emirates",
        });
        $('#categories_select').select2({
            placeholder: "Select Categories",
        });
        $('#country').select2({
            placeholder: "Select Country",
        });
        $('#city').select2({
            placeholder: "Select City",
        });
        var categories = "{{ json_encode($user->categories->pluck('id')->toArray()) }}";
        updateFilters(JSON.parse(categories));

        function changeCategories(event) {
            updateFilters($(event).val());
        }

        function updateFilters(categories = null) {
            // var categories = $("input[name='categories[]']")
            //   .map(function(){return $(this).val();}).get();
            // console.log(categories);
            $.ajax({
                type: "GET",
                url: "/get-filters",
                data: {
                    categories: categories
                },
                success: function(data) {
                    $('.filters_container').empty();
                    var html = ``;
                    if (data.length > 0) {
                        data.forEach(element => {
                            if (element.filter.type == 'Text Box') {
                                var text_box_value = '';
                                if (element.filter_value.length > 0) {
                                    text_box_value = element.filter_value[0].value;
                                }
                                html += `<div class="form-group validated col-md-6">
                                            <label class="form-control-label">` + element.filter.name + `</label>
                                            <input type="text" value="` + text_box_value +
                                    `" class="form-control" name="filters[` + element.filter
                                    .id + `]"/>
                                        </div>`;
                            } else if (element.filter.type == 'Drop Down') {
                                var options = ``;
                                element.filter.items.forEach(element_ => {
                                    var selected = '';
                                    if (element.filter_value.length > 0) {
                                        if (element.filter_value[0].filter_item_id == element_
                                            .value) {
                                            selected = 'selected';
                                        }
                                    }
                                    options += `<option value="` + element_.value + `" ` +
                                        selected + `>` +
                                        element_.text + `</option>`;
                                });
                                html += `<div class="form-group validated col-md-6">
                                            <label class="form-control-label">` + element.filter.name + `</label>
                                            <select class="form-control" name="filters[` + element.filter.id + `][]">
                                            <option value>Select ` + element.filter.name + `</option>
                                            ` + options + `
                                        </select></div>`;
                            } else if (element.filter.type == 'Check Box') {
                                var options = ``;
                                element.filter.items.forEach(element__ => {
                                    var checked = '';
                                    if (element.filter_value.length > 0) {
                                        element.filter_value.forEach(filter_element => {
                                            if (filter_element.filter_item_id ==
                                                element__.value) {
                                                checked = 'checked';
                                            }
                                        });
                                    }
                                    options += `<label class="checkbox checkbox-outline">
                                        <input type="checkbox" value="` + element__.value + `" ` + checked +
                                        ` name="filters[` +
                                        element.filter.id + `][]">
                                        <span></span> &nbsp;
                                        ` + element__.text + `
                                    </label>`;
                                });
                                html += `<div class="form-group validated col-md-6">
                                            <label class="form-control-label">` + element.filter.name + `</label>
                                                ` + options + `
                                            </div>`;
                            } else {
                                var options = ``;
                                element.filter.items.forEach(element__ => {
                                    var checked_radio = '';
                                    if (element.filter_value.length > 0) {
                                        if (element.filter_value[0].filter_item_id == element__
                                            .value) {
                                            checked_radio = 'checked';
                                        }
                                    }
                                    options += `<label class="checkbox checkbox-outline">
                                        <input type="radio" value="` + element__.value + `" ` + checked_radio +
                                        ` name="filters[` +
                                        element.filter.id + `][]">
                                        <span></span> &nbsp;
                                        ` + element__.text + `
                                    </label>`;
                                });
                                html += `<div class="form-group validated col-md-6">
                                            <label class="form-control-label">` + element.filter.name + `</label>
                                                ` + options + `
                                            </div>`;
                            }
                        });
                        $('.filters_container').append(html);
                    }
                    // var row = '<option value></option>';
                    // $('#city').empty();
                    // data.forEach(element => {
                    //     row += '<option value="' + element.id + '">' + element.name +
                    //         '</option>'
                    // });
                    // $('#city').append(row);
                },
                error: function(e) {}
            });
        }
    </script>
@endsection
