@extends('vendor.shop_front.master')
@section('shop_front_content')
    <!--begin::Content-->
    <div class="flex-row-fluid ml-lg-8">
        <!--begin::Card-->
        <div class="card card-custom card-stretch">
            <!--begin::Header-->
            <div class="card-header py-3">
                <div class="card-title align-items-start flex-column">
                    <h3 class="card-label font-weight-bolder text-dark">Gallary
                    </h3>
                </div>
                <div class="card-toolbar">
                    <button type="submit" id="btnSubmit" class="btn btn-success mr-2">Save</button>
                    <a href="{{ url('/' . $url . '/gallary') }}"><button type="reset"
                            class="btn btn-secondary">Cancel</button></a>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Form-->
            @php
                $url_ = '/' . $url . '/gallary';
                if (isset($editable)) {
                    $url_ = '/' . $url . '/gallary/' . $editable->id;
                }
            @endphp
            <form class="form" id="form">
                @csrf
                @if (isset($editable))
                    @method('PATCH')
                @endif
                <!--begin::Body-->
                <div class="card-body">
                    <div class="row">
                        <div class="form-group validated col-md-6">
                            <label class="form-control-label">Image</label>
                            {{-- <ul id="media-list" class="clearfix">
                                <li class="myupload">
                                    <span><i class="fa fa-plus" aria-hidden="true"></i>
                                        <input type="file" click-type="single" id="picupload" name="image"
                                            class="picupload">
                                    </span>
                                </li>
                            </ul> --}}
                            <ul id="media-list" class="clearfix">
                                <li class="myupload" @isset($editable) @if($editable->image) style="display:none;" @endif @endisset>
                                    <span><i class="fa fa-plus" aria-hidden="true"></i>
                                        <input type="file" click-type="single" id="picupload" name="image" @isset($editable) value="{{url('/storage')}}/{{$editable->image}}" @endisset class="picupload">
                                    </span>
                                </li>
                                @isset($editable)
                                @if($editable->image)
                                <li>
                                    <img src="/storage/{{$editable->image}}" title=""/>
                                    <div class='post-thumb'>
                                        <div class='inner-post-thumb'>
                                            <a href='javascript:void(0);' data-id='' class='remove-pic'>
                                                <i class='fa fa-times' aria-hidden='true'>
                                                </i>
                                            </a>
                                        </div>
                                    </div>
                                    <input type="text" name="old_image" value="{{$editable->image}}" style="display: none;">
                                </li>
                                @endif
                                @endisset
                            </ul>
                        </div>
                    </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
    <!--end::Content-->
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script type="text/javascript">
        var url = "{{ $url_ }}";
        $("#btnSubmit").click(function(event) {
            event.preventDefault();
            var form = $('#form')[0];
            var data = new FormData(form);
            $("#btnSubmit").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: url,
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    $("#btnSubmit").prop("disabled", false);
                    setTimeout(function() {
                        location.replace('/{{ $url }}/gallary');
                    }, 1000);

                },
                error: function(e) {
                    $("#btnSubmit").prop("disabled", false);
                }
            });
        });
    </script>
@endsection
