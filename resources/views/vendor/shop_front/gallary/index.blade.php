@extends('vendor.shop_front.master')
@section('shop_front_content')
    <!--begin::Content-->
    <div class="flex-row-fluid ml-lg-8">
        <!--begin::Card-->
        <div class="card card-custom card-stretch">
            <!--begin::Header-->
            <div class="card-header py-3">
                <div class="card-title align-items-start flex-column">
                    <h3 class="card-label font-weight-bolder text-dark">Gallary
                    </h3>
                </div>
                <div class="card-toolbar">
                    <a href="{{ url('/'.$url.'/gallary/create') }}"><button type="submit" id="btnSubmit"
                            class="btn btn-success mr-2">Add New</button></a>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Form-->
            <form class="form" id="form">
                @csrf
                <!--begin::Body-->
                <div class="card-body">
                    <table class="table table-bordered table-hover table-checkable" id="kt_datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($rows as $row)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td><div class="symbol symbol-60 symbol-2by3 flex-shrink-0 mr-4">
                                        <div class="symbol-label" style="background-image: url('/storage/{{$row->image}}')"></div>
                                    </div></td>
                                    <td nowrap="nowrap">
                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Edit details">
                                            <i class="las la-search"></i>
                                        </a>
                                        <a href="/{{$url}}/gallary/{{ $row->id }}/edit"
                                            class="btn btn-sm btn-clean btn-icon" title="Edit details">
                                            <i class="la la-edit"></i>
                                        </a>
                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon deleteBtn"
                                            data-id="{{ $row->id }}" title="Delete">
                                            <i class="la la-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--end::Body-->
            </form>
            <!--end::Form-->
        </div>
    </div>
    <!--end::Content-->

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script type="text/javascript">
        var table = $('#kt_datatable').DataTable();
        $('.deleteBtn').on('click', function(event) {
            var id = $(this).data('id');
            Swal.fire({
                title: "Are you sure?",
                text: "You wont be able to revert this!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        type: "DELETE",
                        enctype: 'multipart/form-data',
                        url: "/{{$url}}/gallary/" + id,
                        processData: false,
                        contentType: false,
                        cache: false,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            Swal.fire(
                                "Deleted!",
                                "Your file has been deleted.",
                                "success"
                            )
                            setTimeout(function() {
                                location.reload();
                            }, 1000);
                        },
                        error: function(e) {}
                    });
                } else if (result.dismiss === "cancel") {
                    Swal.fire(
                        "Cancelled",
                        "Your imaginary file is safe :)",
                        "error"
                    )
                }
            });
        });
    </script>
@endsection
