{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Profile Personal Information-->
            <div class="d-flex flex-row">
                <!--begin::Aside-->
                <div class="flex-row-auto offcanvas-mobile w-250px w-xxl-350px" id="kt_profile_aside">
                    <!--begin::Profile Card-->
                    <div class="card card-custom card-stretch">
                        <!--begin::Body-->
                        <div class="card-body pt-4">
                            <!--begin::Nav-->
                            <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                                @if($user->parent_id == null)
                                <div class="navi-item mb-2">
                                    <a href="{{ url('/'.$url.'/shop-front/basic-information') }}" class="navi-link py-4 @if($page_title == 'Basic Informations') active @endif">
                                        <span class="navi-icon mr-2">
                                            <i class="fas fa-info"></i>
                                        </span>
                                        <span class="navi-text font-size-lg">Basic Information</span>
                                    </a>
                                </div>
                                <div class="navi-item mb-2">
                                    <a href="{{ url('/'.$url.'/shop-front') }}" class="navi-link py-4 @if($page_title == 'Business Informations') active @endif">
                                        <span class="navi-icon mr-2">
                                            <i class="fas fa-briefcase"></i>
                                        </span>
                                        <span class="navi-text font-size-lg">Business Information</span>
                                    </a>
                                </div>
                                @endif
                                {{-- <div class="navi-item mb-2">
                                    <a href="{{ url('/'.$url.'/shop-front/team-members') }}" class="navi-link py-4 @if($page_title == 'Team Members') active @endif">
                                        <span class="navi-icon mr-2">
                                            <i class="fas fa-users"></i>
                                        </span>
                                        <span class="navi-text font-size-lg">Team Members</span>
                                    </a>
                                </div>

                                <div class="navi-item mb-2">
                                    <a href="{{ url('/'.$url.'/shop-front/awards') }}" class="navi-link py-4 @if($page_title == 'Awards') active @endif">
                                        <span class="navi-icon mr-2">
                                            <i class="fas fa-users"></i>
                                        </span>
                                        <span class="navi-text font-size-lg">Awards</span>
                                    </a>
                                </div> --}}
                                <div class="navi-item mb-2">
                                    <a href="{{ url('/'.$url.'/gallary') }}" class="navi-link py-4 @if($page_title == 'Gallary') active @endif">
                                        <span class="navi-icon mr-2">
                                            <i class="fas fa-file"></i>
                                        </span>
                                        <span class="navi-text font-size-lg">Gallary</span>
                                    </a>
                                </div>
                            </div>
                            <!--end::Nav-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Profile Card-->
                </div>
                <!--end::Aside-->
                @yield('shop_front_content')
            </div>
            <!--end::Profile Personal Information-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->
@endsection

