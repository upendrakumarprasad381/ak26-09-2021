<?php

//use Illuminate\Support\Facades\Route;

Route::get('/guest', [App\Http\Controllers\user\UserHomeController::class, 'index'])->name('user.dashboard');



Route::group(['prefix' => '/', 'as' => 'user'], function() {

    Route::get('/', 'UserController@index')->name('home_page');
    Route::any('/helper', 'UserController@helper');
    Route::any('/customer-signup', 'RegistrationController@customersignup');
    Route::any('/customer-login', 'RegistrationController@customerlogin');
    Route::any('/vendor-signup', 'RegistrationController@vendorsignup');
    Route::any('/customer-logout', 'RegistrationController@customerlogout');
    Route::post('/customerloginTest', 'RegistrationController@customerloginTest');


    Route::any('/test1', 'RegistrationController@test1');
    Route::any('/test2', 'RegistrationController@test2');

    Route::any('/vendors-list', 'UserController@vendorslist');
    Route::any('/vendor-details', 'UserController@vendordetails');
    Route::post('/senduserRFQ', 'UserController@senduserRFQ');

    Route::any('/my-services', 'UserController@myservices');
    Route::any('/about-us', 'UserController@aboutus');
    Route::any('/contact-us', 'UserController@contactus');
    Route::any('/privacy-policy', 'UserController@privacypolicy');

    Route::any('/blog', 'UserController@blog');
        Route::any('/blog/{blogId}', 'UserController@blogdetails');


    Route::group(['prefix' => '/vendors-portal', 'as' => 'vendors-portal'], function() {
        Route::any('/', 'UserController@vendorsportal');
        Route::any('/event-planner', 'UserController@vendorsportalsummary');
        Route::any('/ask-deema-vendors', 'UserController@vendorsportalsummary');
        Route::any('/shop-owner', 'UserController@vendorsportalsummary');
    });
});

Route::group(['prefix' => '/shop', 'as' => 'shop'], function() {
    Route::get('/', 'ShopController@index');
});
Route::group(['prefix' => 'user', 'middleware' => ['role:user|guest'], 'as' => 'user'], function() {
    Route::any('/event', 'UserController@event');
    Route::any('/event/{eventId}', 'UserController@eventdetails');
    Route::any('/to-do-list', 'UserController@todolist');
});
?>