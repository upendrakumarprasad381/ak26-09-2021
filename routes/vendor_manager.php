<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/vendor-manager', 'as' => 'VendorManager'], function() {
    Route::get('/{eventId}/{categoryId}/get-vendor', 'EventPlanner\VendorManagerController@index');
    Route::any('/vendor-details/{vendorId}/{categoryId}/{eventId}', 'EventPlanner\VendorManagerController@vendordetails');
    Route::post('/saveRFQ', 'EventPlanner\VendorManagerController@saveRFQ');
    Route::any('/{eventId}/view-all-request', 'EventPlanner\VendorManagerController@viewallrequest');
});
Route::group(['prefix' => '/event-planner', 'as' => 'EventPlanner'], function() {
    Route::get('/proposals/{eventId}', 'EventPlanner\VendorManagerController@proposals');
});
Route::group(['prefix' => '/to-do-list', 'as' => 'ToDoList'], function() {
    Route::any('/', 'ToDoListController@index');
    Route::any('/create/{Id?}', 'ToDoListController@create')->where(['Id' => '[0-9]+']);
    Route::post('/store', 'ToDoListController@store');

    Route::post('/getGuestlistForToDo', 'ToDoListController@getGuestlistForToDo');


    Route::post('/getsubList', 'ToDoListController@getsubList');
    Route::post('/delete-todo', 'ToDoListController@deletetodo');

    Route::any('/addnewcontact', 'ToDoListController@addnewcontact');
});
Route::group(['prefix' => '/guest-list', 'as' => 'GuestList'], function() {
    Route::get('/{eventId}', 'GuestListController@index')->where(['eventId' => '[0-9]+']);
    Route::any('/{eventId}/create-event', 'GuestListController@createevent')->where(['eventId' => '[0-9]+']);
    Route::any('/{eventId}/sub-event/{subeventId}', 'GuestListController@subevent')->where(['eventId' => '[0-9]+']);
    Route::any('/{eventId}/sub-event/{subeventId}/export', 'GuestListController@subeventExport')->where(['eventId' => '[0-9]+']);
    Route::any('/{eventId}/sub-event/{subeventId}/create/{Id?}', 'GuestListController@subeventcreate')->where(['eventId' => '[0-9]+']);
    Route::any('/{eventId}/layout-settings/{subeventId}', 'GuestListController@layoutsettings')->where(['eventId' => '[0-9]+']);
    Route::any('/{eventId}/layout-settings-content/{subeventId}/{layoutId}', 'GuestListController@layoutsettingscontent')->where(['eventId' => '[0-9]+']);
});
Route::group(['prefix' => '/calender', 'as' => 'calender'], function() {
    Route::any('/settings', 'calenderController@settings');
    Route::get('/{eventId?}', 'calenderController@index')->where(['eventId' => '[0-9]+']);

    Route::any('/googleUpdatelist', 'calenderController@googleUpdatelist');
    Route::any('/googleUpdateEventId', 'calenderController@googleUpdateEventId');
    Route::any('/eventList/{eventId?}', 'calenderController@eventList');
});
Route::group(['prefix' => '/checklist', 'as' => 'checklist'], function() {
    Route::any('/{eventId}/original-checklist', 'checklistController@originalchecklist')->where(['eventId' => '[0-9]+']);
    Route::any('/{eventId}/customized-checklist', 'checklistController@customizedchecklist')->where(['eventId' => '[0-9]+']);
    Route::any('/{eventId}/{filterId?}', 'checklistController@index')->where(['eventId' => '[0-9]+']);
});
Route::any('create-web-page/{eventId}', 'createWebPageController@index');
Route::any('/guest-list/{invitationId}/event-invitation-confirmation', 'UserController@eventInvitationConfirmation')->where(['invitationId' => '[0-9]+']);
Route::get('/web/{eventId}', 'createWebPageController@webPagepreview');
Route::group(['prefix' => '/web-page', 'as' => 'createWebPage'], function() {

    Route::any('/settings/{eventId}', 'createWebPageController@webPagesettings');
    Route::get('/view/{eventId}', 'createWebPageController@webPagepreview');
});
