<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MeetingController;
use App\Http\Controllers\ReportUserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Demo routes
Route::get('/datatables', 'PagesController@datatables');
Route::get('/ktdatatables', 'PagesController@ktDatatables');
Route::get('/select2', 'PagesController@select2');
Route::get('/jquerymask', 'PagesController@jQueryMask');
Route::get('/icons/custom-icons', 'PagesController@customIcons');
Route::get('/icons/flaticon', 'PagesController@flaticon');
Route::get('/icons/fontawesome', 'PagesController@fontawesome');
Route::get('/icons/lineawesome', 'PagesController@lineawesome');
Route::get('/icons/socicons', 'PagesController@socicons');
Route::get('/icons/svg', 'PagesController@svg');

// Quick search dummy route to display html elements in search dropdown (header search)
Route::get('/quick-search', 'PagesController@quickSearch')->name('quick-search');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/dashboard','HomeController@index');


Route::get('/personal_information', [App\Http\Controllers\HomeController::class, 'personalInformation'])->name('user.personal_information');
Route::get('/business_information', [App\Http\Controllers\HomeController::class, 'businessInformation'])->name('user.business_information');
Route::get('/change_password', [App\Http\Controllers\HomeController::class, 'changePassword'])->name('user.change_password');
Route::post('/update_user_information', [App\Http\Controllers\HomeController::class, 'updateUserInformation'])->name('user.update_user_information');
Route::post('/update_business_information', 'Admin\ManageVendorsController@updateBusinessInformation')->name('user.update_business_information');
Route::post('/update_user_password', [App\Http\Controllers\HomeController::class, 'updateUserPassword'])->name('user.update_user_password');
Route::post('/vendor_create_customer','HomeController@createCustomer')->name('create-customer');
Route::post('/vendor_create_item','HomeController@createVendorItem');
Route::post('/upload-file','HomeController@uploadFile');
Route::resource('awards-affiliations', 'Vendor\VendorAwardController');
Route::resource('team-members', 'Vendor\TeamMemberController');
Route::get('get-cities','Admin\ManageVendorsController@getCities');
Route::get('get-filters','Admin\Master\VendorFilterController@getFilters');

Route::group(['prefix' => 'admin', 'middleware' => ['role:superadministrator']], function() {
    Route::get('/', [App\Http\Controllers\Admin\AdminHomeController::class, 'index'])->name('admin.dashboard');
    Route::group(['prefix' => 'master'],function(){
        Route::get('/get-categories',[App\Http\Controllers\Admin\Master\VendorsCategoryController::class,'getData']);
        Route::resource('vendors-categories',Admin\Master\VendorsCategoryController::class);
        Route::resource('event-planners-categories',Admin\Master\EventPlannersCategoryController::class);
        Route::resource('shop-admins-categories',Admin\Master\ShopAdminsCategoryController::class);
        Route::resource('occasions',Admin\Master\OccasionController::class);
        Route::resource('vendor-items',Admin\Master\VendorItemController::class);
        Route::resource('vendor-taxes',Admin\Master\VendorTaxController::class);
        Route::resource('budget-settings',Admin\Master\BudgetSettingsController::class);
        Route::resource('vendor-filters',Admin\Master\VendorFilterController::class);
    });
    Route::group(['prefix' => 'web'],function(){
        Route::resource('our-teams',Admin\Web\OurTeamController::class);
        Route::resource('blogs',Admin\Web\BlogController::class);
        Route::resource('blogs-categories',Admin\Web\BlogCategoryController::class);
        Route::resource('web-settings',Admin\Web\WebSettingsController::class);
        Route::resource('sliders',Admin\Web\WebSliderController::class);
    });
     Route::group(['prefix' => 'cms'],function(){
            Route::any('banner','Admin\Web\CMSController@banner');
            Route::any('addbanner/{bannerId?}','Admin\Web\CMSController@addbanner');
            Route::any('bannerRemove/{bannerId}','Admin\Web\CMSController@bannerRemove');
            Route::any('contactus','Admin\Web\CMSController@contactus');
     });
    Route::get('event-planners','Admin\ManageVendorsController@eventPlannersIndex');
    Route::get('vendors','Admin\ManageVendorsController@vendorsIndex');
    Route::get('event-planner/{id}','Admin\ManageVendorsController@eventPlannerShow');
    Route::get('vendor/{id}','Admin\ManageVendorsController@vendorShow');
    Route::post('vendor-approval/{id}','Admin\ManageVendorsController@vendorApproval');
    Route::post('update-vendor/{id}','Admin\ManageVendorsController@updateVendor');
    Route::get('import-vendors','Admin\ManageVendorsController@importVendors');
    Route::post('import-vendors','Admin\ManageVendorsController@importExcel');

    Route::get('permission-reset',function()
    {
        Artisan::call('migrate:refresh --path=database/migrations/2021_08_22_071110_update_permissions.php');
        dd("Permission Reset successfully.");
    });
});

Route::group(['prefix' => 'vendor', 'middleware' => ['role:vendor|event_planner']], function() {
    Route::get('/',function(){
        return redirect('/dashboard');
    });
    Route::get('/dashboard','Vendor\VendorHomeController@index')->name('vendor.dashboard');
    Route::get('/leads','Vendor\ManageLeadsController@index')->name('vendor.leads');
    Route::get('/suggested-leads','Vendor\ManageLeadsController@suggestedLeads')->name('vendor.suggested-leads');
    Route::get('/look-for-leads','Vendor\ManageLeadsController@lookForLeads')->name('vendor.look-for-leads');
    Route::get('/leads/recieved-leads','Vendor\ManageLeadsController@recievedLeads')->name('vendor.recieved-leads');
    Route::get('/leads/recieved-lead/{id}','Vendor\ManageLeadsController@showRcievedLead')->name('vendor.recieved-lead');
    Route::get('/leads/ongoing-leads','Vendor\ManageLeadsController@ongoingLeads')->name('vendor.ongoing-leads');
    Route::get('/leads/declined-leads','Vendor\ManageLeadsController@declinedLeads')->name('vendor.declined-leads');
    Route::post('/leads/accept-lead','Vendor\ManageLeadsController@acceptLead')->name('vendor.accept-lead');
    Route::post('/leads/decline-lead','Vendor\ManageLeadsController@declineLead')->name('vendor.decline-lead');
    Route::post('/leads/suggest-lead','Vendor\ManageLeadsController@suggestLead')->name('vendor.suggest-lead');

    Route::get('/leads/event-planner-recieved-lead/{id}','Vendor\ManageLeadsController@showEventPlannerRcievedLead')->name('vendor.event-planner-recieved-lead');
    Route::post('/leads/accept-event-planner-lead','Vendor\ManageLeadsController@acceptEventPlannerLead')->name('vendor.accept-event-planner-lead');
    Route::post('/leads/decline-event-planner-lead','Vendor\ManageLeadsController@declineEventPlannerLead')->name('vendor.decline-event-planner-lead');


    Route::get('/my-events','EventPlanner\ManageEventsController@index')->name('vendor.my-events');
    // Route::get('/create-event','Vendor\ManageEventsController@create')->name('vendor.create-event');
    Route::get('/create-event','EventPlanner\ManageEventsController@create')->name('vendor.create-event');
    Route::get('/event/{id}','EventPlanner\ManageEventsController@showEvent')->name('vendor.event');
    Route::resource('/events', 'Vendor\ManageEventsController');
    Route::resource('/event/{event_id}/proposals', 'Vendor\ManageProposalsController');
    Route::get('/event/{event_id}/accepted-proposals','Vendor\ManageProposalsController@acceptedProposals');
    Route::get('/event/{event_id}/pending-proposals','Vendor\ManageProposalsController@pendingProposals');
    Route::resource('/event/{event_id}/contracts', 'Vendor\ManageContractsController');
    Route::get('/event/{event_id}/signed-contracts','Vendor\ManageContractsController@signedContracts');
    Route::post('/event/{event_id}/export-event','EventPlanner\ManageEventsController@exportEvent')->name('event-planner.export-event');
    Route::resource('/event/{event_id}/budgeter', 'Vendor\VendorBudgeterController');

    Route::resource('/event/{event_id}/invoices', 'Vendor\ManageInvoiceController');
    Route::get('/event/{event_id}/invoices/create/{contract_id}', 'Vendor\ManageInvoiceController@create');
    Route::get('/event/{event_id}/accepted-invoices', 'Vendor\ManageInvoiceController@acceptedInvoices');
    Route::get('/event/{event_id}/paid-invoices', 'Vendor\ManageInvoiceController@paidInvoices');
    Route::get('/event/{event_id}/unpaid-invoices', 'Vendor\ManageInvoiceController@unPaidInvoices');
    Route::get('/event/{event_id}/rejected-invoices', 'Vendor\ManageInvoiceController@rejectedInvoices');
    Route::get('/event/{event_id}/all-invoices', 'Vendor\ManageInvoiceController@allInvoices');

    Route::post('/event/{event_id}/reject-invoice/{invoice_id}', 'Vendor\ManageInvoiceController@rejectInvoice');
    Route::post('/event/{event_id}/accept-invoice/{invoice_id}', 'Vendor\ManageInvoiceController@acceptInvoice');
    Route::post('/event/{event_id}/invoice-collect-status/{invoice_id}', 'Vendor\ManageInvoiceController@invoiceCollectStatus');

    Route::resource('/master-settings', 'Vendor\MasterSettingsController');
    Route::resource('/terms-conditions', 'Vendor\VendorTermsConditionController');
    Route::get('/get_vendor_terms_conditions','Vendor\VendorTermsConditionController@getTC');
    Route::get('/default-tax','Vendor\MasterSettingsController@defaultTax');

    Route::get('/invoices','Vendor\VendorHomeController@allInvoices');
    Route::get('/proposals','Vendor\VendorHomeController@allProposals');
    Route::get('/contracts','Vendor\VendorHomeController@allContracts');

    Route::get('/financial-tool','Vendor\VendorHomeController@financialTool');
    Route::get('/financial-tool/create','Vendor\VendorHomeController@createFinancialTool');
    Route::resource('/financial-tool/financial-cost', 'Vendor\FinancialCostController');
    Route::get('/get-contracts','Vendor\FinancialCostController@getContracts');
    Route::resource('member-designations', 'Vendor\MemberDesignationController');
    Route::resource('no-of-attendees', 'Vendor\NoOfAttendeesController');
    Route::get('/shop-front', 'Vendor\ManageVendorImagesController@index');
    Route::get('/shop-front/basic-information', 'Vendor\ManageShopFrontController@basicInformation');
    Route::resource('gallary', 'Vendor\ManageVendorImagesController');
    Route::get('/get-sub-services/{id}', 'Admin\Master\VendorsCategoryController@getSubServices');

    Route::get('/get-checklists/{occasion_id}','checklistController@getChecklists');
});

Route::group(['prefix' => 'event-planner', 'middleware' => ['role:event_planner|vendor']], function() {

    // Route::get('/','Vendor\VendorHomeController@index')->name('vendor.dashboard');
    Route::get('/dashboard','Vendor\VendorHomeController@index')->name('vendor.dashboard');
    // Route::get('/','EventPlanner\EventPlannerHomeController@index')->name('event-planner.dashboard');
    Route::resource('/master-settings', 'Vendor\MasterSettingsController');
    Route::get('/default-tax','Vendor\MasterSettingsController@defaultTax');
    Route::get('/leads','EventPlanner\ManageLeadsController@index')->name('event-planner.leads');

    Route::get('/leads/recieved-leads','EventPlanner\ManageLeadsController@recievedLeads')->name('event-planner.recieved-leads');
    Route::get('/leads/recieved-lead/{id}','EventPlanner\ManageLeadsController@showRcievedLead')->name('event-planner.recieved-lead');
    Route::get('/leads/ongoing-leads','EventPlanner\ManageLeadsController@ongoingLeads')->name('event-planner.ongoing-leads');
    Route::get('/leads/declined-leads','EventPlanner\ManageLeadsController@declinedLeads')->name('event-planner.declined-leads');

    Route::post('/leads/accept-lead','EventPlanner\ManageLeadsController@acceptLead')->name('event-planner.accept-lead');
    Route::post('/leads/decline-lead','EventPlanner\ManageLeadsController@declineLead')->name('event-planner.decline-lead');

    Route::get('/my-events','EventPlanner\ManageEventsController@index')->name('event-planner.my-events');
    Route::get('/create-event','EventPlanner\ManageEventsController@create')->name('event-planner.create-event');
    Route::get('/event/{id}','EventPlanner\ManageEventsController@showEvent')->name('event-planner.event');
    Route::post('/event/{event_id}/create-duplicate','EventPlanner\ManageEventsController@createDuplicate')->name('event-planner.create-duplicate-event');
    Route::post('/event/{event_id}/export-event','EventPlanner\ManageEventsController@exportEvent')->name('event-planner.export-event');
    Route::resource('/events', 'EventPlanner\ManageEventsController');

    Route::resource('/event/{event_id}/proposals', 'EventPlanner\ManageProposalsController');
    Route::get('/event/{event_id}/accepted-proposals','EventPlanner\ManageProposalsController@acceptedProposals');
    Route::get('/event/{event_id}/pending-proposals','EventPlanner\ManageProposalsController@pendingProposals');

    Route::post('/event/{event_id}/contracts/generate-pdf','EventPlanner\ManageContractsController@generatePDF');
    Route::resource('/event/{event_id}/contracts', 'EventPlanner\ManageContractsController');
    Route::get('/event/{event_id}/contracts/create/{proposal_id}', 'EventPlanner\ManageContractsController@create');
    Route::get('/event/{event_id}/signed-contracts','EventPlanner\ManageContractsController@signedContracts');
    Route::post('/event/{event_id}/contracts/ammend','EventPlanner\ManageContractsController@ammendContract');
    // Route::resource('/event/{event_id}/invoices', 'EventPlanner\ManageInvoiceController');
    Route::resource('/event/{event_id}/invoices', 'Vendor\ManageInvoiceController');

    Route::resource('/event/{event_id}/budgeters', 'EventPlanner\ManageBudgeterController');
    Route::post('/set-as-default-budget/{id}','EventPlanner\ManageBudgetController@setAsDefault');
    Route::get('/event/{event_id}/own-budgeters', 'EventPlanner\ManageBudgeterController@ownBudgets');

    Route::resource('/event/{event_id}/budgeter', 'EventPlanner\ManageBudgetController');
    Route::post('/event/{event_id}/store-budget-item', 'EventPlanner\ManageBudgetController@BudgetItemStore');
    Route::post('/event/{event_id}/save-final-budget', 'EventPlanner\ManageBudgetController@saveFinalBudget');


    Route::get('/invoices','Vendor\VendorHomeController@allInvoices');
    Route::get('/proposals','Vendor\VendorHomeController@allProposals');
    Route::get('/contracts','Vendor\VendorHomeController@allContracts');

    Route::get('/financial-tool','Vendor\VendorHomeController@financialTool');
    Route::resource('/financial-tool/financial-cost', 'Vendor\FinancialCostController');
    Route::get('/get-contracts','Vendor\FinancialCostController@getContracts');
    Route::get('/get-contract-items','Vendor\FinancialCostController@getContractItems');
    Route::resource('member-designations', 'Vendor\MemberDesignationController');
    Route::resource('no-of-attendees', 'Vendor\NoOfAttendeesController');
    Route::get('/shop-front', 'Vendor\ManageShopFrontController@index');
    Route::get('/shop-front/basic-information', 'Vendor\ManageShopFrontController@basicInformation');
    // Route::get('/shop-front/awards', 'Vendor\ManageShopFrontController@awards');
    Route::resource('gallary', 'Vendor\ManageVendorImagesController');

});

Route::group(['prefix' => 'shop-admin', 'middleware' => ['role:shop_admin']], function() {
    Route::get('/', [App\Http\Controllers\shopAdmin\ShopAdminHomeController::class, 'index'])->name('shop_admin.dashboard');
});

Route::group(['prefix' => 'user', 'middleware' => ['role:user|guest']], function() {
    Route::get('/', [App\Http\Controllers\user\UserHomeController::class, 'index'])->name('user.dashboard');
    Route::get('/personal_information', [App\Http\Controllers\user\UserAccountController::class, 'personalInformation'])->name('user.personal_information');
    Route::get('/change_password', [App\Http\Controllers\user\UserAccountController::class, 'changePassword'])->name('user.change_password');
    Route::post('/update_user_information', [App\Http\Controllers\user\UserAccountController::class, 'updateUserInformation'])->name('user.update_user_information');
    Route::post('/update_user_password', [App\Http\Controllers\user\UserAccountController::class, 'updateUserPassword'])->name('user.update_user_password');
});



//chat
Route::group(['middleware' => 'auth'], function () {
    //view routes
    Route::get('/conversations',
        [ChatController::class, 'index'])->name('conversations');
    Route::get('profile', [UserController::class, 'getProfile']);
    Route::get('logout', [\App\Http\Controllers\Auth\LoginController::class, 'logout']);

    //get all user list for chat
    Route::get('users-list', [API\UserAPIController::class, 'getUsersList']);
    Route::get('get-users', [API\UserAPIController::class, 'getUsers']);
    Route::delete('remove-profile-image', [API\UserAPIController::class, 'removeProfileImage']);
    /** Change password */
    Route::post('change-password', [API\UserAPIController::class, 'changePassword']);
    Route::get('conversations/{ownerId}/archive-chat', [API\UserAPIController::class, 'archiveChat']);

    Route::get('get-profile', [API\UserAPIController::class, 'getProfile']);
    Route::post('profile', [API\UserAPIController::class, 'updateProfile'])->name('update.profile');
    Route::post('update-last-seen', [API\UserAPIController::class, 'updateLastSeen']);

    Route::post('send-message',
        [API\ChatAPIController::class, 'sendMessage'])->name('conversations.store')->middleware('sendMessage');
    Route::get('users/{id}/conversation', [API\UserAPIController::class, 'getConversation']);
    Route::get('conversations-list', [API\ChatAPIController::class, 'getLatestConversations']);
    Route::get('archive-conversations', [API\ChatAPIController::class, 'getArchiveConversations']);
    Route::post('read-message', [API\ChatAPIController::class, 'updateConversationStatus']);
    Route::post('file-upload', [API\ChatAPIController::class, 'addAttachment'])->name('file-upload');
    Route::post('image-upload', [API\ChatAPIController::class, 'imageUpload'])->name('image-upload');
    Route::get('conversations/{userId}/delete', [API\ChatAPIController::class, 'deleteConversation']);
    Route::post('conversations/message/{conversation}/delete', [API\ChatAPIController::class, 'deleteMessage']);
    Route::post('conversations/{conversation}/delete', [API\ChatAPIController::class, 'deleteMessageForEveryone']);
    Route::get('/conversations/{conversation}', [API\ChatAPIController::class, 'show']);
    Route::post('send-chat-request', [API\ChatAPIController::class, 'sendChatRequest'])->name('send-chat-request');
    Route::post('accept-chat-request',
        [API\ChatAPIController::class, 'acceptChatRequest'])->name('accept-chat-request');
    Route::post('decline-chat-request',
        [API\ChatAPIController::class, 'declineChatRequest'])->name('decline-chat-request');

    /** Web Notifications */
    Route::put('update-web-notifications', [API\UserAPIController::class, 'updateNotification']);

    /** BLock-Unblock User */
    Route::put('users/{user}/block-unblock', [API\BlockUserAPIController::class, 'blockUnblockUser']);
    Route::get('blocked-users', [API\BlockUserAPIController::class, 'blockedUsers']);

    /** My Contacts */
    Route::get('my-contacts', [API\UserAPIController::class, 'myContacts'])->name('my-contacts');

    /** Groups API */
    Route::post('groups', [API\GroupAPIController::class, 'create']);
    Route::post('groups/{group}', [API\GroupAPIController::class, 'update']);
    Route::get('groups', [API\GroupAPIController::class, 'index']);
    Route::get('groups/{group}', [API\GroupAPIController::class, 'show']);
    Route::put('groups/{group}/add-members', [API\GroupAPIController::class, 'addMembers']);
    Route::delete('groups/{group}/members/{user}', [API\GroupAPIController::class, 'removeMemberFromGroup']);
    Route::delete('groups/{group}/leave', [API\GroupAPIController::class, 'leaveGroup']);
    Route::delete('groups/{group}/remove', [API\GroupAPIController::class, 'removeGroup']);
    Route::put('groups/{group}/members/{user}/make-admin', [API\GroupAPIController::class, 'makeAdmin']);
    Route::put('groups/{group}/members/{user}/dismiss-as-admin', [API\GroupAPIController::class, 'dismissAsAdmin']);
    Route::get('users-blocked-by-me', [API\BlockUserAPIController::class, 'blockUsersByMe']);

    Route::get('notification/{notification}/read', [API\NotificationController::class, 'readNotification']);
    Route::get('notification/read-all', [API\NotificationController::class, 'readAllNotification']);

    /** Web Notifications */
    Route::put('update-web-notifications', [API\UserAPIController::class, 'updateNotification']);
    Route::put('update-player-id', [API\UserAPIController::class, 'updatePlayerId']);
    //set user custom status route
    Route::post('set-user-status', [API\UserAPIController::class, 'setUserCustomStatus'])->name('set-user-status');
    Route::get('clear-user-status', [API\UserAPIController::class, 'clearUserCustomStatus'])->name('clear-user-status');

    //report user
    Route::post('report-user', [API\ReportUserController::class, 'store'])->name('report-user.store');
});
